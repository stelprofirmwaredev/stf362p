/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2019, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    HAL_VoltageSensing.c
* @date    2019/07/05
* @authors Jean-Fran�ois Many
* @brief   Voltage Sensing Module based on ZC pulse duration
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include "typedef.h"
#include "HAL_Interrupt.h"
#include "stm32f4xx_hal.h"
#include ".\HAL_VoltageSensing.h"
#include ".\Sensors\inc\THREAD_Sensors.h"
#include <math.h>
#include ".\HAL\inc\HAL_LoadManagement.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define INVALID_MAX_ZC_DURATION_MEASURE 16000
#define INVALID_MIN_ZC_DURATION_MEASURE 100
#define MAXIMUM_240V_DURATION           10000

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
uint32_t ZC_High_Duration = 0;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
TIM_HandleTypeDef htim4;


/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/


/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void start_voltage_sensing(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    /* Enable TIM4 clock */
    __TIM4_CLK_ENABLE();

    /* Initialize TIM4 */
    htim4.Instance = TIM4;

    /* Initialize TIMx peripheral as follow:
    + Period = 20000
    + Prescaler = 30
    + ClockDivision = 0
    + Counter direction = Up
    */
    htim4.Init.Period = 20000;
    htim4.Init.Prescaler = 30;
    htim4.Init.ClockDivision = 0;
    htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
    if (HAL_TIM_Base_Init(&htim4) == HAL_OK)
    {
        ;
    }
    __HAL_TIM_ENABLE(&htim4);
    __HAL_RCC_GPIOC_CLK_ENABLE();

    GPIO_InitStruct.Pin = ZC_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(ZC_GPIO_Port, &GPIO_InitStruct);

    HAL_NVIC_SetPriority(EXTI15_10_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

    __HAL_GPIO_EXTI_CLEAR_FLAG(ZC_Pin);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void end_voltage_sensing(void)
{
    HAL_NVIC_DisableIRQ(EXTI15_10_IRQn);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void VoltageDetection_Callback(void)
{
    static uint32_t ZC_edge = 1;
    GPIO_InitTypeDef GPIO_InitStruct;

    if (ZC_edge == 1)
    {
        __HAL_RCC_GPIOC_CLK_ENABLE();

        __HAL_TIM_SET_COUNTER(&htim4, 0);
        GPIO_InitStruct.Pin = ZC_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
        GPIO_InitStruct.Pull = GPIO_PULLUP;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
        HAL_GPIO_Init(ZC_GPIO_Port, &GPIO_InitStruct);

        HAL_NVIC_SetPriority(EXTI15_10_IRQn, 5, 0);
        HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

        __HAL_GPIO_EXTI_CLEAR_FLAG(ZC_Pin);

        ZC_edge = 0;
    }
    else
    {
        //Capture the timer count
        ZC_High_Duration = __HAL_TIM_GET_COUNTER(&htim4);

        //If duration is too large, perform another sampling
        if ((ZC_High_Duration < INVALID_MAX_ZC_DURATION_MEASURE) && (ZC_High_Duration > INVALID_MIN_ZC_DURATION_MEASURE))
        {
            if (ZC_High_Duration <= MAXIMUM_240V_DURATION)
            {
                Set_DetectedVoltage(240);
            }
            else
            {
                Set_DetectedVoltage(120);
            }
            end_voltage_sensing();
            ZC_edge = 1;
        }
        else
        {
            GPIO_InitStruct.Pin = ZC_Pin;
            GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
            GPIO_InitStruct.Pull = GPIO_PULLUP;
            GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
            HAL_GPIO_Init(ZC_GPIO_Port, &GPIO_InitStruct);

            HAL_NVIC_SetPriority(EXTI15_10_IRQn, 5, 0);
            HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

            __HAL_GPIO_EXTI_CLEAR_FLAG(ZC_Pin);
            ZC_edge = 1;
        }
    }
}


/*******************************************************************************
* @brief  Voltage Sensing monitoring function
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2019/02/21
*******************************************************************************/
void StartVoltageSensing(void)
{
    start_voltage_sensing();
}

/** Copyright(C) 2019 Stelpro Design, All Rights Reserved**/
