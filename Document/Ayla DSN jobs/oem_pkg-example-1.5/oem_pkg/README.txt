Release notes for OEM scripts

Package Part Number: AY004CSU0

This package contains scripts to help customers set up Ayla modules
in their devices.

The Ayla module console should be connected via a serial port on the PC.

For install and usage instructions, see 

	Ayla OEM Package Installation and User Guide, AY006DSU0.

Change log:

Version 1.5: 2015-07-21
	ayla_verify ver 0.10:
		Indicate to cloud that the connection is for test purposes,
		and doesn't indicate the first connection after sale of dev.

Version 1.4: 2015-07-06
	ayla-config ver 0.9:
		DEV-1738: fix bad log file when airkiss key present.

Version 1.0: 2014-05-28
	ayla_config ver 0.5:
		Allow reset to be disabled at end of test.
		Add configurable pause before opening terminal in case it is 
		still open after module update.
		script now exits with a specific error code if test fails
		
	ayla_verify ver 0.8:
		Use factory reset before and after tests by default.
		Add RSSI test.
		Disable client if not testing connectivity to service.
		Use 'wifi profile erase' instead of 'wifi profile disable'.

Version 0.1: 2013-01-06
	Initial release.

--- End
