#
# Copyright 2014 Ayla Networks, Inc.  All rights reserved.
#
# ayla_lib: common functions used in Ayla TCL scripts
# 

set mod_fd ""

#
# Seed the pseudo-random number generator
#
proc set_rand_seed {} {
	set seed [expr [clock clicks] + [pid] + [clock seconds]]
	set seed [expr $seed - [expr $seed * 2]]
	expr (srand ($seed))
}

#
# Generate a random AES key
# This code assumes the generator has been seeded, above.
#
proc rand_aes_key {} {
	set keylen 16
	set key ""

	# This code could be more efficient, but it doesn't rely on
	# word size or the precision of rand() (or floating point numbers)
	while {$keylen > 0} {
		set keypart [format %02X [expr int(rand() * 256)]]
		append key $keypart
		incr keylen -1
	}
	return $key
}

#
# Find the Windows serial port to use to talk to the Ayla device.
# Searches the registry by running DOS command "reg query".
# Uses temp file from global temp_file
# Returns the name of the port.
#
proc find_port {} {
	global conf
	global cmdname
	global temp_file
	global conf_file

	set ports 0
	set port ""

	if [catch {
		set key {HKEY_LOCAL_MACHINE\HARDWARE\DEVICEMAP\SERIALCOMM}
		exec reg query $key > $temp_file
	}] {
		puts "$cmdname: error getting com port with regedit"
		exit 1
	}
	if [catch {
		set fd [open $temp_file r]
	}] {
		puts "$cmdname: error getting com port: can't open $temp_file"
		exit 2
	}
	set data [read $fd]
	set lines [split $data "\n\r"]

	foreach line $lines {
		set token [split [regsub -all {[ 	]+} $line " "]]
		set argc [llength $token]
		if {$argc == 4 && [regexp {\\Device\\VCP*} [lindex $token 1]]} {
			set port [lindex $token 3]
			incr ports
		}
	}
	if {$ports > 1} {
		puts "$cmdname: error: found $ports possible com ports to use"
		puts "$cmdname: Please configure com port in $conf_file."
		exit 2
	}
	return $port
}

#
# read conf() array from file
#
proc conf_read {file} {
	global conf
	global cmds
	global cmds_nosave
	global cmdname

	if [catch {
		set fd [open $file r]
	}] {
		puts "$cmdname: error: cannot open configuration file: $file"
		puts "$cmdname: please create $file based on oem_pkg\\$file"
		exit 2
	}
	set data [read $fd]
	set lines [split $data "\n\r"]

	set cmd_mode 0

	foreach line $lines {
		if {[string index "$line" 0] == "#"} {
			continue
		}
		set token [split [regsub -all {[ 	]+} $line " "]]
		set argc [llength $token]
		if {$argc == 0} {
			continue
		}
		set cmd [lindex $token 0]

		if {$argc == 1 && $cmd == "commands"} {
			set cmd_mode 1
		} elseif {$argc == 2 && $cmd == "commands" &&
		    [lindex $token 1] == "nosave"} {
			set cmd_mode 2
		} elseif {$argc == 2 && $cmd == "commands" &&
		    [lindex $token 1] == "end"} {
			set cmd_mode 0
		} elseif {$cmd_mode == 1} {
			#
			# capture expected OEM ID and model from commands
			#
			if {$cmd == "oem" && $argc > 1} {
				set arg1 [lindex $token 1]
				if {$argc == 3 && $arg1 == "model"} {
					set conf(oem_model) [lindex $token 2]
				} elseif {$argc == 2} {
					set conf(oem) $arg1
				}
			}

			#
			# capture WiFi Airkiss parameters
			#
			# wifi setup_mode airkiss key <val>
			#
			if {$cmd == "wifi" && $argc >= 4} {
				set arg1 [lindex $token 1]
				set arg2 [lindex $token 2]
				set arg3 [lindex $token 3]
				if {$arg1 == "setup_mode" &&
				    $arg2 == "airkiss" &&
				    $arg3 == "key"} {
					if {$argc == 4} {
						lappend token [rand_aes_key]
						set line [join $token " "]
						set conf(airkiss_key) \
						    [lindex $token 4]
					} elseif {$argc == 5 &&
					    [lindex $token 4] == "genkey"} {
						set token [lreplace $token 4 4 \
						    [rand_aes_key]]
						set line [join $token " "]
						set conf(airkiss_key) \
						    [lindex $token 4]
					} elseif {$argc == 5} {
						set conf(airkiss_key) \
						    [lindex $token 4]
					}
				}
			}

			lappend cmds $line
		} elseif {$cmd_mode == 2} {
			lappend cmds_nosave $line
		} else {
			set conf([lindex $token 0]) [lindex $token 1]
		}
	}
	close $fd
}

#
# Update log file
#
proc log_ayla {status {errcode 0} {msg ""}} {
	global conf
	global assign
	global err
	global log_ayla_service_status

	if {![info exists log_ayla_service_status]} {
		set log_ayla_service_status ""
	}
	# replace any commas in the text message with spaces
	regsub -all "," $msg " " msg

	set log_file $conf(log_file)

	set time [clock seconds]
	set timestamp "[clock format $time -gmt 1 -format "20%y/%m/%d %T UTC"]"

	#
	# Comma-separated-values (CSV) log entry.
	# The first field is a format designator in case we want to
	# change it later or have multiple formats.
	#
	set entry "4,$time,$timestamp,$status,$errcode,[assign_get model]"
	set entry "$entry,[assign_get serial],[assign_get mac]"
	set entry "$entry,[assign_get mfg_model],[assign_get mfg_serial]"
	set entry "$entry,[assign_get stm32_sig],$msg"
	set entry "$entry,$conf(oem),$conf(oem_model)"
	set entry "$entry,$log_ayla_service_status,[conf_get odm]"
	set entry "$entry,[conf_get mod_version]"
	set entry "$entry,[conf_get airkiss_key]"

	if [catch {
		set fd [open $log_file a]
		puts $fd $entry
		close $fd
	} rc errinfo ] {
		send_user "log $log_file write error code $rc\n"
		send_user "log $log_file write error info \
			[dict get $errinfo -errorinfo]"
	}
}

#
# Give pass/fail status on a test.
# stat should be "pass", "fail", or "timeout"
# Optionally, provide a value for tests like RSSI.
#
proc step_status {name stat {value ""}} {
	global start_time

	set elapsed [expr [clock seconds] - $start_time]
	set mins [expr $elapsed / 60]
	set secs [expr $elapsed % 60]
	set timestamp [format "%3d:%2.2d" $mins $secs]

	if {$stat != "start" && $stat != "pass"} {
		send_user "\n"
	}
	set stat [format "%-7s" "$stat:"]
	if {$value != ""} {
		send_user --  "$timestamp $stat $name: $value\n"
	} else {
		send_user --  "$timestamp $stat $name\n"
	}
}

#
# Send a command to the module with a carriage return, using Expect.
#
proc send_mod {cmd} {
	global mod

	send -i $mod "$cmd\r"
}

proc assign_get {name} {
	global assign

	if {[info exists assign($name)]} {
		return $assign($name)
	}
	return ""
}

proc conf_get {name} {
	global conf

	if {[info exists conf($name)]} {
		return $conf($name)
	}
	return ""
}

proc conf_get_int {name} {
	global conf

	if {[info exists conf($name)]} {
		return $conf($name)
	}
	return 0
}
 
#
# reset to transition to BC
# wait for module reset by user if it doesn't respond right away
#
proc mod_reset {{mode ""} {recurse 0} {factory 0}} {
	global mod
	global err
	global conf
	global prompt
	global spawn_id

	set spawn_id $mod

	if {$mode != ""} {
		set mode_req "$mode"
	} else {
		set mode_req $conf(req_mode)
	}
	if {!$recurse} {
		step_status mod_reset start
	}
	set version unknown
	log_user $conf(log_user)

	if {[conf_get_int factory_reset] || $factory} {
		set reset_cmd "reset factory"
	} else {
		set reset_cmd "reset"
	}
	set cmd $reset_cmd

	set try 0
	while {$version == "unknown"} {
		if {$try > 1} {
			send_user "\nModule reset timeout: \
			   Try manual reset.\n"
			send_user "\nRetrying reset\n\n"
			set cmd $reset_cmd
			log_user 1
		}
		incr try
		if {$try >= 4} {
			log_user $conf(log_user)
			step_status mod_reset timeout
			error "mod startup timed out" "" $err(mod_start)
		}

		send_mod $cmd
		set spawn_id $mod
		set mod_mode user
		set bc "bc\[a-z\]*"

		expect_before "$cmd\n"

		set timeout 10
		expect {
			-re ".*($bc) (\[^ \]*) .*mfg mode.*$bc init done\n.*" {
				set program $expect_out(1,string)
				set version $expect_out(2,string)
				set mod_mode mfg
				set prompt "mfg-> "
			} -re ".*($bc) (\[^ \]*) .*setup mode.*$bc init done\n.*" {
				set program $expect_out(1,string)
				set version $expect_out(2,string)
				set mod_mode setup
				set prompt "setup-> "
			} -re ".*($bc) (\[^ \]*) .*$bc init done\n.*" {
				set program $expect_out(1,string)
				set version $expect_out(2,string)
			} eof {
				# console closed, probably due to break cond.
				console_open
				set cmd "show version"
			} timeout {
				set cmd $reset_cmd
			}
		}
	}
	expect_before
	log_user $conf(log_user)

	set req_program [conf_get mod_program]
	if {$program != $req_program && $req_program != ""} {
		send_user "\tmodule has firmware program $program\n"
		send_user "\tconfiguration needs program $req_program\n"
		step_status mod_reset fail "module has wrong program $program\n"
		error "mod has wrong program $program" "" $err(mod_program)
	}
	set req_version [conf_get mod_version]
	if {$version != $req_version && $req_version != ""} {
		send_user "\tmodule has firmware version $version\n"
		send_user "\tconfiguration needs version $req_version\n"
		step_status mod_reset fail "module has wrong version $version\n"
		error "mod has wrong version $version" "" $err(mod_version)
	}

	if {$mod_mode != $mode_req} {
		send_user "\tmodule is in $mod_mode mode\n"
		set key [conf_get oem_setup_key]
		if {!$recurse && $mode_req == "setup" && $mod_mode == "user" &&
		    "$key" != ""} {
			send_user "\tattempting to re-enable setup mode\n"
			send_user "\tresetting to previous factory settings\n"
			mod_reset user 1 1
			mod_cmd ""
			set prompt "setup-> "
			mod_cmd "setup_mode enable $key"
			mod_cmd "save"
			mod_reset setup 1
		} else {
			step_status mod_reset fail \
			    "module is not in $mode_req mode"
			error "mod not in $mode_req mode" "" $err(mod_start_mfg)
		}
	}
	if {!$recurse} {
		step_status mod_reset pass
	}
}

#
# send a command to BC and look at result
#
proc mod_cmd cmd {
	global err
	global mod
	global prompt

	set spawn_id $mod

	send_mod "$cmd"
	expect {
		"*FAIL:" {
			error "mod cmd $cmd failed" ""  $err(mod_cmd)
		} "usage:" {
			error "mod cmd $cmd failed usage" ""  $err(mod_cmd)
		} "unrecognized command" {
			error "mod cmd $cmd failed" ""  $err(mod_cmd)
		} -re ".*$prompt" {
		} -re "--> " {
		} timeout {
			error "mod cmd $cmd timed out" "" \
				$err(mod_cmd_time)
		}
	}
}

#
# open or re-open the console
#
proc console_open {} {
	global conf
	global mod
	global mod_fd
	global console_settings
	global err

	if { $mod_fd != "" } {
		# try to close, just in case. This is expected to fail.
		if [catch { 
			close -i $mod
		} rc] {
			# send_user "close caught error: $rc\n"
		}
		spawn -noecho -leaveopen $mod_fd
		set mod $spawn_id
		return	
	}
	
	set op "open" 
	if [catch {
		set fd [open "\\\\.\\$conf(com_port)" r+]

		set op "config $console_settings"
		fconfigure $fd -mode $console_settings -handshake none

		set op "set buffers"
		fconfigure $fd -sysbuffer {4096 4096} -pollinterval 10

		set op "spawn"
		spawn -noecho -leaveopen $fd

		set mod $spawn_id
		set mod_fd $fd
	}] {
		error "failed to $op $conf(com_port)" "" $err(console_connect)
	}
}

proc console_start {} {
	global err
	global conf
	global console_settings

	if {$conf(com_port) == ""} {
		set conf(com_port) [find_port]
		if {$conf(com_port) == ""} {
			step_status test_connect fail dev_not_found
			error "no com port found for Ayla board" "" \
				$err(console_port)
		}
	}

	# Pause in case TeraTerm still has serial line open for ayla_update
	after [conf_get_int com_port_delay]
	console_open
	send_user "using port $conf(com_port) settings $console_settings\n"
	log_user $conf(log_user)
}

proc mod_show_label "" {
	global mod
	global err
	global assign

	step_status show_label start

	set spawn_id $mod
	set cmd "show id"
	send_mod "show id"
	expect_before "show id\n"

	set assign(mac_addr) ""
	set assign(serial) ""

	while 1 {
		expect {
			"*FAIL:" {
				error "mod cmd $cmd failed" ""  $err(mod_cmd)
			} "unrecognized command" {
				error "mod cmd $cmd failed" ""  $err(mod_cmd)
			} -re "id: (\[\^\"\n\]*) \"(\[\^\"\n\]*)\"\n" {
				set name $expect_out(1,string)
				set val $expect_out(2,string)
				set assign($name) $val
			} -re "->" {
				break
			} timeout {
				error "mod cmd $cmd timed out" "" \
					$err(mod_cmd_time)
			}
		}
		expect_before
	}
	expect_before

	set assign(dsn) "$assign(serial)"
	set assign(mac) "$assign(mac_addr)"

	if {$assign(dsn) == ""} {
		step_status show_label fail "expected DSN not seen"
		return
	}
	step_status show_label pass "DSN $assign(dsn) MAC $assign(mac)"
}

proc console_detach "" {
	global mod
	global mod_fd

	if {$mod >= 0} {
		close $mod_fd
		close -i $mod
		wait -i $mod -nowait
		set mod -1
	}
}

# end of ayla_lib.tcl
