#
# Copyright 2011-2013 Ayla Networks, Inc.  All rights reserved.
#
# ayla_verify: verification of OEM setup for Ayla modules
# 
set cmdname ayla_verify
set version "0.10 2015-07-21"
#
package require Expect
package require xml

send_user "$cmdname version $version\n"
set start_time [clock seconds]

# note: for trace use: exp_internal 1

#
# Include common functions
#
source [file join [file dirname [info script]] ayla_lib.tcl]

#
# Configuration settings
#
set conf_file "verify_config.txt"

set conf(test_wifi_ssid) ayla
set conf(test_wifi_key)	aylatest
set conf(test_wifi_sec)	WPA2_Personal
set conf(test_wifi_prof) 9
set conf(wifi_profiles)	10
set conf(com_port)	""
set conf(log_user)	0
set conf(factory_reset) 1
set conf(log_file)	log.txt
set conf(mod_version)	""

set conf(test_service)	0

# set to 1 to run thruput tests
set conf(test_thruput)	1	
# set to 1 to repeat thruput tests
set conf(test_thruput_loop) 0
# length for thruput tests
set conf(test_thruput_len) 100000	
# min bandwidth
set conf(test_thruput_min) 200000
# length for first dry-run trial 
set conf(test_thruput_dry_len) 1000	

set conf(req_mode) 	user

set conf(oem) ""
set conf(oem_model) ""

set conf(ads_user) ""
set conf(ads_passwd) ""
set conf(app_id) ""
set conf(app_secret) ""
set conf(auth_host) "user.aylanetworks.com"
set conf(ads_host) "device.aylanetworks.com"
set conf(ads_timeout) 40

set prompt "--> "
set assign(dsn) ""
set mod_key ""
set auth_token ""

set conf(mod_ip_addr) "192.168.0.1"

set max_limit 5

# workaround to get forward slashes in DOS home directory path
set home [string map {"\\" "/"} $env(HOME)]
set path .

# executables used

set curl "$path/oem_pkg/curl/curl.exe"
set null_file NUL
set temp_file curl_scratch.txt

set cmd [file tail $argv0]

#
# error codes
#
set err(stram_timeout)	1
# reserved		2
set err(usage)		4
set err(conf_odm)	5
set err(flash_setup)	11
set err(flash_setup_time) 12
set err(load_flash)	30
set err(load_flash_time) 31
set err(mfg_dl) 	32
set err(mfg_dl_time) 	33
set err(ram_start)	34
set err(mod_start_mfg) 	49
set err(mod_start) 	50
set err(mod_cmd) 	51
set err(mod_cmd_time) 	52
set err(mod_test) 	53
set err(serial)		60
set err(serial_old)	61
set err(serial_old_time) 62
set err(serial_old_conf) 63
set err(serial_get)	64
set err(serial_none)	65
set err(serial_write)	66
set err(serial_parse)	67
set err(serial_not_mfg) 68
set err(serial_not_passed) 69
set err(serial_test_time) 70
set err(serial_no_mac)	71
set err(file_crc)	72
set err(console_port)		73
set err(console_connect)	74
set err(mac_from_mod)		75
set err(flash_erase)		81
set err(flash_erase_time) 	82
set err(flash_init) 		91
set err(flash_init_time) 	92
set err(unknown) 		99
set err(auth_open)		101
set err(auth_curl)		102
set err(props_curl)		103
set err(dsn_curl)		104
set err(conn_time)		105
set err(temp_open)		106
set err(props_missing)		107
set err(ads_timeout)		108
set err(auth_fail)		112
set err(auth_config)		113
set err(mod_thruput)		120
set err(oem_id)			131
set err(oem_model)		132
set err(oem_key)		133

set console_settings 115200,n,8,1

set wifi(mode) unknown
set wifi(ssid) unknown
set log_ayla_service_status	""

#
# Handle test failure
#
proc test_fail msg {
	global err
	global mod

	send_user "\nmodule test failed: $msg\n"
	if {$mod != -1} {
		send_mod "reset"
	}
	error "module test failed: $msg\n" "" $err(mod_test)
	exit 1
}

#
# Setup Wi-Fi for OEM verification test
#
proc mod_test_setup {} {
	global mod

	set spawn_id $mod
	set timeout 5

	#
	# Set temporary profile for testing
	#
	step_status test_setup start
	mod_cmd "wifi enable 0"
	mod_cmd "wifi commit"
	step_status test_setup pass
}

proc mod_test_connect {} {
	global mod
	global err
	global conf
	global wifi
	global mod_key
	global log_ayla_service_status

	step_status test_connect start
	set spawn_id $mod
	set timeout 20

	#
	# Set an indication to ADS that this is a test connection.
	#
	mod_cmd "client test"

	for {set prof 0} {$prof < $conf(wifi_profiles)} {incr prof} {
		mod_cmd "wifi profile $prof profile erase"
	}
	mod_cmd "wifi profile $conf(test_wifi_prof)"
	mod_cmd "wifi ssid $conf(test_wifi_ssid)"
	mod_cmd "wifi security $conf(test_wifi_sec)"
	if {$conf(test_wifi_sec) != "none"} {
		mod_cmd "wifi key $conf(test_wifi_key)"
	}
	mod_cmd "wifi profile enable"

	if {$conf(test_service) == 0} {
		mod_cmd "client disable"
	}

	#
	# Turn off save_on* so we don't do a save.
	# be sure not to do a manual save either.
	# Eventually have a mode that disables all saves.
	#
	mod_cmd "wifi save_on_ap_connect 0"
	mod_cmd "wifi save_on_server_connect 0"
	mod_cmd "wifi enable 1"
	mod_cmd "wifi commit"
	mod_cmd "wifi scan"
	step_status test_connect pass

	#
	# Wait for join to succeed
	#
	step_status test_join start
	expect {
		"*wifi: join succeeded" {
			step_status test_join pass
		} timeout {
			step_status test_join timeout
			test_fail "waiting for wifi join"
		}
	}

	#
	# Wait for DHCP to succeed and capture IP address
	#
	step_status test_dhcp start
	set conf(mod_ip_addr) unknown
	expect {
		-re "up using IP (\[0-9\.\]*)\n" {
			set conf(mod_ip_addr) $expect_out(1,string)
			step_status test_dhcp pass $conf(mod_ip_addr)
		} timeout {
			step_status test_dhcp fail timeout
			test_fail "waiting for DHCP"
		}
	}

	#
	# Wait for connection to service
	#
	if {$conf(test_service)} {
		set log_ayla_service_status 0
		set timeout $conf(ads_timeout)
		step_status test_link start
		set mod_key ""
		expect {
		   -re {.* i client: module name "[^"]*" key ([^.]*)\.} {
				set mod_key $expect_out(1,string)
				step_status test_link pass
				set log_ayla_service_status 1
			} timeout {
				mod_show_wifi
				step_status test_link timeout
				error "test_link timeout" "" $err(ads_timeout)
			}
		}
	}

	#
	# Get RSSI
	# 
	step_status test_rssi start
	mod_cmd "rssi"
	expect {
	   -re "RSSI average (-?\[0-9\]*)\n" {
			step_status test_rssi pass $expect_out(1,string)
		} timeout {
			step_status test_rssi fail timeout
			test_fail "waiting for RSSI"
		}
	}
}

proc mod_show_oem "" {
	global mod
	global err
	global conf
	global prompt

	step_status show_oem start

	set spawn_id $mod
	set cmd "show oem"
	send_mod $cmd
	set oem ""
	set oem_model ""
	set oem_key ""

	while 1 {
		expect {
			-re "\noem: \"(\[^\"\]*)\"\n" {
				set oem $expect_out(1,string)
			} -re "oem_model: \"(\[^\"\]*)\"\n" {
				set oem_model $expect_out(1,string)
			} "oem_key: \(is set\)\n" {
				set oem_key set
			} "oem_key: \(not set\)\n" {
				set oem_key "not set"
			} -re ".*$prompt" {
				break
			} timeout {
				error "mod cmd $cmd timed out" "" \
					$err(mod_cmd_time)
			}
		}
	}

	if {$oem != $conf(oem)} {
		step_status show_oem fail "incorrect OEM value: $oem"
		error "OEM ID mismatch" "" $err(oem_id)
	}
	if {$oem_model != $conf(oem_model)} {
		step_status show_oem fail "incorrect OEM model: $oem_model"
		error "OEM model mismatch" "" $err(oem_model)
	}
	if {$oem_key != "set"} {
		step_status show_oem fail "OEM key not set"
		error "OEM key not set" "" $err(oem_key)
	}
	step_status show_oem pass
}

proc mod_show_wifi "" {
	global mod
	global err
	global conf
	global prompt
	global wifi

	set spawn_id $mod
	log_user 1
	set cmd "show wifi"
	send_mod $cmd

	set ssid "unknown"
	set mode "unknown"

	while 1 {
		expect {
			-re "\nWi-Fi associated with SSID (\[\^\n\]*)\n" {
				set ssid $expect_out(1,string)
				set mode STA
			} -re "\nWi-Fi AP mode ssid (\[\^\n]*)\n" {
				set ssid $expect_out(1,string)
				set mode AP
			} -re "\nWi-Fi AP mode SSID (\[\^\n]*)\n" {
				set ssid $expect_out(1,string)
				set mode AP
			} -re "\nWi-Fi disabled\n" {
				set mode down
			} -re "\nWi-Fi idle\n" {
				set mode down
			} -re "\nWi-Fi idle (not )?scanning\n" {
				set mode down
			} -re "\nWi-Fi associating with SSID \[\^\n]\n" {
				set ssid $expect_out(1,string)
				set mode down
			} -re ".*$prompt" {
				break
			} timeout {
				error "mod cmd $cmd timed out" "" \
					$err(mod_cmd_time)
			}
		}
	}
	set wifi(mode) $mode
	set wifi(ssid) $ssid
	log_user $conf(log_user)

	set rc "mode $mode SSID $ssid"
}

#
# XML parser for authentication response
#
set xml_token ""
set xml_token_stack [list]
set xml_path "/"

set xml_value(access-token) ""
set xml_value(refresh-token) ""

proc xml_cdata {data args} {
	global xml_value
	global xml_token

	append xml_value($xml_token) "$data"
}

proc xml_elem_start {name attlist args} {
	global xml_value
	global xml_token
	global xml_token_stack

	lappend xml_token_stack $xml_token
	set xml_token $name
	set xml_value($xml_token) ""
}

proc xml_elem_end {name args} {
	global xml_token_stack
	global xml_token

	set xml_token_stack [lrange $xml_token_stack 0 end-1]
	set xml_token [lindex $xml_token_stack end]
}

set prop_value(version) ""
set prop_type(version) ""

proc xml_prop_end {name args} {
	global prop_value
	global prop_type
	global xml_value

	xml_elem_end $name $args
	if {$name == "property" && [info exists xml_value(name)]} {
		set prop $xml_value(name)

		if {[info exists xml_value(value)]} {
			set prop_value($prop) $xml_value(value)
		} else {
			set prop_value($prop) ""
		}
		if {[info exists xml_value(base-type)]} {
			set prop_type($prop) $xml_value(base-type)
		} else {
			set prop_type($prop) unknown
		}
		if {[info exists xml_value(key)]} {
			set prop_key($prop) $xml_value(key)
		} else {
			set prop_key($prop) null
		}
		array unset xml_value
	}
}

proc token {name value} {
	set rc "<$name>$value</$name>"
}

proc user_sign_in {} {
	global err
	global conf
	global temp_file
	global curl
	global xml_value
	global auth_token

	step_status user_sign_in start

	foreach item [list test_app_email test_app_passwd \
	   test_app_id test_app_secret] {
		if {![info exists conf($item)] || $conf($item) == ""} {
			step_status user_sign_in fail "no $item in config"
			error "missing configuration item $item" "" \
				$err(auth_config)
		}
	}

	set xml ""
	append xml [token email $conf(test_app_email)]
	append xml [token password $conf(test_app_passwd)]

	set app [token app_id $conf(test_app_id)]
	append app [token app_secret $conf(test_app_secret)]
	append xml [token application $app]

	set xml [token user $xml]

	if [catch {
		exec "$curl" -k -X POST -d \"$xml\" \
		    -H "Content-Type: application/xml" -o $temp_file \
		    https://$conf(auth_host)/users/sign_in.xml 2> curl_out.txt
	}] {
		error "Auth request for $conf(test_app_email) failed" "" \
		    $err(auth_curl)
	}

	if [catch {set fd [open "$temp_file" r]}] {
		error "read of auth sign_in response failed:\ 
			empty data" "" $err(auth_open)
	}

	#
	# parse XML data
	#
	array unset xml_value
	set parser [::xml::parser \
		-characterdatacommand xml_cdata \
		-elementstartcommand xml_elem_start \
		-elementendcommand xml_elem_end]

	$parser parse [read $fd]
	close $fd
	# file delete $temp_file

	if {![info exists xml_value(access-token)]} {
		step_status user_sign_in fail "no access-token"
		error "user login failed" "" $err(auth_fail)
	}

	# send_user "access_token $xml_value(access-token)\n"
	# send_user "refresh_token $xml_value(refresh-token)\n"
	set auth_token $xml_value(access-token)

	# --- XXX TBD
	step_status user_sign_in pass
}

#
# get last connect time for the module
#
proc ads_conn_time {{old ""}} {
	global err
	global conf
	global temp_file
	global curl
	global xml_value
	global assign
	global auth_token

	set host $conf(ads_host)
	# send_user "using host $host\n"

	step_status conn_time start

	if [catch {
		exec "$curl" -k -o $temp_file \
		    -H "Authorization: auth_token $auth_token" \
		    https://$host/apiv1/dsns/$assign(dsn).xml \
		    2> curl_out.txt
	}] {
		step_status conn_time fail "curl error"
		error "info request for $assign(dsn) failed" "" \
		    $err(dsn_curl)
	}

	if [catch {set fd [open "$temp_file" r]}] {
		error "read of auth sign_in response failed:\ 
			empty data" "" $err(temp_open)
	}

	#
	# parse XML data
	#
	array unset xml_value
	set parser [::xml::parser \
		-characterdatacommand xml_cdata \
		-elementstartcommand xml_elem_start \
		-elementendcommand xml_elem_end]

	$parser parse [read $fd]
	close $fd
	# file delete $temp_file

	#
	# List properties we have
	#
	if {![info exists xml_value(connected-at)]} {
		error "parse error or missing connected-at token" "" \
			$err(conn_time)
	}
	set mod_conn_time $xml_value(connected-at)

	if {$old != "" && $old == $mod_conn_time} {
		step_status conn_time fail "time didn't change"
		error "module connect time didn't change\n" $old $err(conn_time)
	}
	step_status conn_time pass $mod_conn_time
	set rc $mod_conn_time
}

proc ads_props_get {} {
	global err
	global conf
	global temp_file
	global curl
	global xml_value
	global assign
	global auth_token
	global prop_value
	global prop_type

	set host $conf(ads_host)
	# send_user "using host $host\n"

	step_status props_get start

	if [catch {
		exec "$curl" -k -o $temp_file \
		    -H "Authorization: auth_token $auth_token" \
		    https://$host/apiv1/dsns/$assign(dsn)/properties.xml \
		    2> curl_out.txt
	}] {
		error "Property request for $assign(dsn) failed" "" \
		    $err(props_curl)
	}

	if [catch {set fd [open "$temp_file" r]}] {
		error "read of auth sign_in response failed:\ 
			empty data" "" $err(auth_open)
	}

	#
	# parse XML data
	#
	array unset xml_value
	array unset prop_value
	set parser [::xml::parser \
		-characterdatacommand xml_cdata \
		-elementstartcommand xml_elem_start \
		-elementendcommand xml_prop_end]

	$parser parse [read $fd]
	close $fd
	# file delete $temp_file

	#
	# If desired for debugging, list properties we have
	#
	if {0} {
		foreach prop [array names prop_value] {
			send_user "property $prop value \"$prop_value($prop)\"\
				$prop_type($prop)\n"
		}
	}
	step_status props_get pass "[array size prop_type] properties"
}

#
# Test that required properties exist
#
proc ads_props_exist {} {
	global conf
	global err
	global prop_type

	set fail 0

	step_status props_exist start
	set props [split $conf(test_props) ","]
	
	foreach prop $props {
		if {![info exists prop_type($prop)]} {
			incr fail
			send_user "property $prop not on ADS\n"
		}
	}
	if {$fail != 0} {
		step_status props_exist fail "$fail properties not found"
		error "some properties missing" "" $err(props_missing)
	}
	step_status props_exist pass "[llength $props] properties"
}

#
# Module test and setup
#
proc oem_verify "" {
	global conf
	global err

	if {[conf_get odm] == ""} {
		error "odm name must be set in config file" "" $err(conf_odm)
	}

	mod_reset
	mod_show_label
	mod_test_setup

	if {$conf(test_service) != 0} {
		mod_test_service
	} else {
		mod_test_connect
	}

	if {$conf(test_thruput) != 0} {
		mod_test_thruput $conf(mod_ip_addr)
	}

	#
	# Reset to clear test configuration
	#
	mod_reset
}

proc mod_test_service "" {

	#
	# Now that we know the DSN, find out when it last connected to ADS
	#
	user_sign_in

	mod_show_oem
	mod_test_connect

	#
	# Wait for properties to be created on the server
	#
	after 500
	ads_conn_time
	ads_props_get
	ads_props_exist
}

#
# Run thruput test
# Return speed on success, zero if speed too slow, negative on error
#
proc thruput_test1 {ip len min_speed dummy} {
	global curl
	global null_file

	spawn "$curl" -o $null_file \
		-w "dl %{size_download} tt %{time_total}\
			st %{time_starttransfer} sd %{speed_download}\n\n" \
		http://$ip/test?len=$len

	set timeout 20
	set size 0
	set speed 0
	set start_time 0
	step_status test_tput start
	expect {
		-re "dl (\[0-9\]*) tt (\[0-9\.\]*) st (\[0-9\.\]*) sd" {
			set size $expect_out(1,string)
			set total_time $expect_out(2,string)
			set start_time $expect_out(3,string)
		} timeout {
			step_status test_tput timeout
			test_fail "waiting for download len $len"
			return -1
		}
	}
	if {$size != $len} {
		step_status test_tput fail "expected $len but received $size"
		test_fail "expected size $len but downloaded $size"
	}
	if {$total_time <= $start_time} {
		set total_time [expr $start_time + 0.001]
	}
	set speed [expr $size / ($total_time - $start_time)]
	send_user "\nspeed $speed\n"
	if {$speed < $min_speed} {
		send_user "download speed too slow. \
			saw $speed min $min_speed\n"
		return 0
	}
	close -i $spawn_id
	wait -i $spawn_id -nowait
	set mbytes [expr round($speed * 100 / (1024 * 1024)) / 100.0]
	set mbits [expr $mbytes * 8]
	step_status test_tput pass "$mbits mbit/s"
	send_user \
	    "\ndownloaded $size bytes at $mbytes Mbytes/sec $mbits mbit/s\n\n"
	return $mbits
}

#
# test code
#
proc mod_test_thruput ip {
	global conf
	global err

	set dl_test_min($conf(test_thruput_len)) $conf(test_thruput_min)
	set lengths [list $conf(test_thruput_len)]

	# dummy run to grease the wheels - don't report errors except timeout
	thruput_test1 $ip $conf(test_thruput_dry_len) 0 1

	set errs 0
	set try_limit 4
	for {set go 1} {$go} {set go $conf(test_thruput_loop)} {
		foreach len $lengths {
			for {set try 0} {$try < $try_limit} {incr try} {
				set rc [thruput_test1 $ip $len \
				    $dl_test_min($len) 0]
				if {$rc > 0} {
					break
				}
				incr errs
				if {$rc < 0} {
					break
				}
			}
		}
	}
	if {$errs} {
		step_status test_tput fail
		if {$errs >= $try_limit} {
			error "thruput test downloads failed" "" \
				$err(mod_thruput)
		}
		return
	}
	step_status test_tput pass
}

proc usage "" {
	global cmd

	send_user "usage: $cmd \[options...\]\n \
	   \n \
	   Options: \n \
	     --help             show this help text\n \
             --log_user         show module interactions\n \
             --loop         	repeat thruput tests forever\n \
	"
	exit 1
}

#
# Main procedure
#
set setup_cmd oem_verify
set prev_cmd ""
set skip_console 0

#
# Handle command-line options
#
set argi 0
while {$argi < $argc} {
	set arg [lindex $argv $argi]
	incr argi
	if {$argi < $argc} {
		set optarg [lindex $argv $argi]
	} else {
		set optarg ""
	}
	set cmd_arg ""
	switch -- $arg {
		"--help" {
			usage
		} "--log_user" {
			set conf(log_user) 1
		} "--loop" {
			set conf(test_thruput_loop) 1
		} default {
			send_user "invalid argument: $arg\n"
			usage
		}
	}
	if {$cmd_arg != ""} {
		if {$prev_cmd != ""} {
			send_user "\n$cmd: $cmd_arg conflicts with $prev_cmd.\n"
			exit 1
		}
		set prev_cmd $cmd_arg
	}
}

conf_read $conf_file

#
# Start terminal session to the module before any download
# otherwise we could miss version message and early prompts
#
set mod -1
if {$skip_console == 0 && [catch console_start rc errinfo]} {
	send_user "$cmd: $rc\r"
	exit 1
}

set exit_status 0
set status PASS
if { [catch "$setup_cmd" rc errinfo] } {
	set err_code [dict get $errinfo -errorcode]
	set err_info [dict get $errinfo -errorinfo]

	if {$err_code != "NONE"} {
		send_user "$cmd: error code $err_code\n"
		send_user "$cmd: rc $rc\n"
		# uncomment the following for a traceback
		# send_user "$cmd: Error info [dict get $errinfo -errorinfo]\n"
		step_status test_summary fail $err_code
	} else {
		send_user "$cmd: caught error code $err_code $rc\n"
		send_user "$cmd: Error info [dict get $errinfo -errorinfo]\n"
		step_status test_summary fail
		set_err_code $err(unknown)
	}
	log_ayla verify $err_code $rc
	set status FAIL
	set exit_status $err_code
} else {
	log_ayla verify
}
console_detach

if { $assign(dsn) != "" } {
	send_user "\n\n*** $status DSN $assign(dsn) ***\n\n"
} else {
	send_user "\n\n*** $status ***\n\n"
}	
exit $exit_status

# end of script
