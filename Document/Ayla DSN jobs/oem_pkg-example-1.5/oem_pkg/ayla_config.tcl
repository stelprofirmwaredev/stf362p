#
# Copyright 2011-2013 Ayla Networks, Inc.  All rights reserved.
#
# ayla_verify: verification of OEM setup for Ayla modules
# 
set cmdname ayla_config
set version "0.9 2015-07-16"
#
package require Expect
package require xml

send_user "$cmdname version $version\n"
set start_time [clock seconds]

# note: for trace use: exp_internal 1

#
# Include common functions
#
source [file join [file dirname [info script]] ayla_lib.tcl]

#
# Configuration settings
#
set conf_file "config.txt"
set temp_file "reg_out.txt"

#
# configuration default settings
#
set conf(com_port)	""
set conf(req_mode) 	setup
set conf(log_user)	0
set conf(com_port_delay) 500
set conf(reset)		enable
set conf(log_file)	log.txt
set conf(oem)		""
set conf(oem_model)	""
set conf(airkiss_key)	""
set conf(mod_version) ""

#
# default ID settings for log, to avoid errors if 'show id' changes
#
set assign(model) ""
set assign(serial) ""
set assign(mac) ""
set assign(mfg_model) ""
set assign(mfg_serial) ""
set assign(stm32_sig) ""
set assign(dsn) ""

#
# list of commands to run - specified in config file
#
list cmds
set cmds {}
list cmds_nosave
set cmds_nosave {}

set prompt "--> "
set dryrun 0

set max_limit 5

# workaround to get forward slashes in DOS home directory path
set home [string map {"\\" "/"} $env(HOME)]
set path .

# executables used

set cmd [file tail $argv0]
set repair_mode 0
set mod_fd ""

#
# error codes
#
set err(stram_timeout)	1
# reserved		2
set err(usage)		4
set err(flash_setup)	11
set err(flash_setup_time) 12
set err(load_flash)	30
set err(load_flash_time) 31
set err(mfg_dl) 	32
set err(mfg_dl_time) 	33
set err(ram_start)	34
set err(mod_start_mfg) 	49
set err(mod_start) 	50
set err(mod_cmd) 	51
set err(mod_cmd_time) 	52
set err(mod_test) 	53
set err(serial)		60
set err(serial_old)	61
set err(serial_old_time) 62
set err(serial_old_conf) 63
set err(serial_get)	64
set err(serial_none)	65
set err(serial_write)	66
set err(serial_parse)	67
set err(serial_not_mfg) 68
set err(serial_not_passed) 69
set err(serial_test_time) 70
set err(serial_no_mac)	71
set err(file_crc)	72
set err(console_port)		73
set err(console_connect)	74
set err(mac_from_mod)		75
set err(flash_erase)		81
set err(flash_erase_time) 	82
set err(flash_init) 		91
set err(flash_init_time) 	92
set err(unknown) 		99
set err(auth_open)		101
set err(auth_curl)		102
set err(props_curl)		103
set err(dsn_curl)		104
set err(conn_time)		105
set err(temp_open)		106
set err(props_missing)		107
set err(ads_timeout)		108
set err(mod_version)		109
set err(mod_program)		110

set console_settings 115200,n,8,1

proc mod_send_cmds {cmds} {
	global prompt

	foreach cmd $cmds {
		mod_cmd $cmd
	}
}

#
# Module test and setup
#
proc ayla_oem_config "" {
	global dryrun
	global cmds
	global cmds_nosave
	global conf

	mod_reset setup
	mod_show_label
	step_status mod_send_cmds start
	mod_send_cmds $cmds
	step_status mod_send_cmds pass

	if {!$dryrun} {
		step_status "saving" start
		mod_cmd "setup_mode disable"
		mod_cmd "save"
		step_status "saving" pass
		log_ayla config
	} else {
		log_ayla config_test
		send_user "dryrun: module left in setup mode\n"
	}
	step_status "test commands" start
	mod_send_cmds $cmds_nosave
	step_status "test commands" pass

	#
	# Reset to clear test configuration
	#
	if {$conf(reset) != "disable"} {
		mod_reset user
	}
}

proc usage "" {
	global cmd

	send_user "usage: $cmd \[options...\]\n \
	   \n \
	   Options: \n \
	     --dryrun           do not save or clear setup mode at end\n \
	     --help             show this help text\n \
	     --log_user         show module interactions\n \
	"
	exit 1
}

#
# Main procedure
#
set setup_cmd ayla_oem_config
set prev_cmd ""
set skip_console 0

#
# Handle command-line options
#
set argi 0
while {$argi < $argc} {
	set arg [lindex $argv $argi]
	incr argi
	if {$argi < $argc} {
		set optarg [lindex $argv $argi]
	} else {
		set optarg ""
	}
	set cmd_arg ""
	switch -- $arg {
		"--help" {
			usage
		} "--dryrun" {
			set dryrun 1
		} "--log_user" {
			set conf(log_user) 1
		} default {
			send_user "invalid argument: $arg\n"
			usage
		}
	}
	if {$cmd_arg != ""} {
		if {$prev_cmd != ""} {
			send_user "\n$cmd: $cmd_arg conflicts with $prev_cmd.\n"
			exit 1
		}
		set prev_cmd $cmd_arg
	}
}

conf_read $conf_file

#
# Start terminal session to the module
#
set mod -1
if {$skip_console == 0 && [catch console_start rc errinfo]} {
	send_user "$cmd: $rc\r"
	exit 1
}

set exit_status 0
set status PASS
if { [catch "$setup_cmd" rc errinfo] } {
	set err_code [dict get $errinfo -errorcode]
	set err_info [dict get $errinfo -errorinfo]

	if {$err_code != "NONE"} {
		send_user "$cmd: error code $err_code\n"
		send_user "$cmd: rc $rc\n"
		# uncomment the following for a traceback
		# send_user "$cmd: Error info [dict get $errinfo -errorinfo]\n"
		step_status test_summary fail $err_code
	} else {
		send_user "$cmd: caught error code $err_code $rc\n"
		send_user "$cmd: Error info [dict get $errinfo -errorinfo]\n"
		step_status test_summary fail
		set err_code $err(unknown)
	}
	log_ayla config $err_code $rc
	set status FAIL
	set exit_status $err_code
}
console_detach

if { $assign(dsn) != "" } {
	send_user "\n\n*** $status DSN $assign(dsn) ***\n\n"
} else {
	send_user "\n\n*** $status ***\n\n"
}	
exit $exit_status

# end of script
