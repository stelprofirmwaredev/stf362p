/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2017, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    HAL_BitCloud_ConsoleInterface.c
* @date    2017/02/23
* @authors J-F. Simard
* @brief   
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <string.h>

#include ".\HAL\BitCloud\inc\HAL_BitCloud_ConsoleInterface.h"
#include ".\console\inc\console.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/

extern const ConsoleCommand_t IncomingBitCloudMsg[];

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/02/24
*******************************************************************************/
void ZGB_InitSerialInterface (void)
{
    
}



/** Copyright(C) 2017 Stelpro Design, All Rights Reserved**/
