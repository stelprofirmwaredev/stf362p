/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2017, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    HAL_IncomingBitCloudMsg.c
* @date    2017/02/23
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include ".\HAL\BitCloud\inc\HAL_IncomingBitCloudMsg.h"
#include ".\HAL\BitCloud\inc\HAL_OutgoingBitCloudMsg.h"

#include ".\console\inc\console.h"
#include ".\Application\inc\ZigBeeStateMachines.h"
#include ".\Application\inc\ZigBee_ClustersEmulation.h"
#include ".\HAL\inc\HAL_ZigBeeInterface.h"

#include ".\DB\classes\inc\class_Thermostats.h"

#include ".\DB\inc\THREAD_DB.h"
#include ".\Console\inc\THREAD_Console.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
void processReceiveCommissioningStatus(const ScanValue_t *args);
void processCommissioningRequestAck(const ScanValue_t *args);
void processInstallCodeUsageAck(const ScanValue_t *args);
void processInstallCodeAck(const ScanValue_t *args);
void processGetNetworkChannel(const ScanValue_t *args);
void processGetNetworkAddress(const ScanValue_t *args);
void processPanIdResponse(const ScanValue_t *args);
void processMatchDescriptorResponse(const ScanValue_t *args);
void processConfigureReportingResponse(const ScanValue_t *args);
void processBindingResponse(const ScanValue_t *args);
void processReadStringAttributeResponse(const ScanValue_t *args);
void processReadAttributeResponse(const ScanValue_t *args);
void processReadAttributeResponseFailure(const ScanValue_t *args);
void processDeviceAnnceReceived(const ScanValue_t *args);
void processActiveEndpointResponse(const ScanValue_t *args);
void processAttributesReportIndication(const ScanValue_t *args);
void processLeaveResp(const ScanValue_t *args);
void processPong(const ScanValue_t *args);
void processAddGroupRespIndication(const ScanValue_t *args);
void processSimpleDescResp(const ScanValue_t *args);
void processZigBeeStackVersionResp(const ScanValue_t *args);
void processDeviceVerifyKeyInd(const ScanValue_t *args);
void processSecondaryMaskSetInd(const ScanValue_t *args);
void processPrimaryMaskSetInd(const ScanValue_t *args);
void processDeviceLeftInd(const ScanValue_t *args);
void processFrameCounterInd(const ScanValue_t *args);
void processIeeeAddressResp(const ScanValue_t *args);

extern void ZigBeeModuleResetOccurs (void);

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
const ConsoleCommand_t IncomingBitCloudMsg [ZB_INMSG_MAX] =
{
    [ZB_INMSG_COMMISSION_STATUS] =
    {
        .name = "CommissioningStatus",
        .fmt = "sd",                            //s: '='
                                                //d:status
        .handler = processReceiveCommissioningStatus,
    },
    [ZB_INMSG_COMMISSION_REQUEST_ACK] =
    {
        .name = "CommissioningRequestAck",
        .fmt = "",
        .handler = processCommissioningRequestAck,
    },
    [ZB_INMSG_INSTALL_CODE_USAGE_ACK] =
    {
        .name = "InstallCodeUsageAck",
        .fmt = "d",
        .handler = processInstallCodeUsageAck,
    },
    [ZB_INMSG_INSTALL_CODE_ACK] =
    {
        .name = "InstallCodeAck",
        .fmt = "d",
        .handler = processInstallCodeAck,
    },
    [ZB_INMSG_NETWORK_CHANNEL] =
    {
        .name = "NetworkChannel",
        .fmt = "d",
        .handler = processGetNetworkChannel,
    },
    [ZB_INMSG_NETWORK_ADDRESS] =
    {
        .name = "NetworkAddress",
        .fmt = "d",
        .handler = processGetNetworkAddress,
    },
    [ZB_INMSG_PAN_ID] =
    {
        .name = "PanId",
        .fmt = "d",
        .handler = processPanIdResponse,
    },
    [ZB_INMSG_MATCH_DESC_RESP] =
    {
        .name = "MatchDescResponse",
        .fmt = "dd",
        .handler = processMatchDescriptorResponse,
    },
    [ZB_INMSG_CONFIG_REPORT_RESP] =
    {
        .name = "ConfigureReportingResponse",
        .fmt = "d",
        .handler = processConfigureReportingResponse,
    },
    [ZB_INMSG_BIND_RESP] =
    {
        .name = "BindRsp",
        .fmt = "d",
        .handler = processBindingResponse,
    },
    [ZB_INMSG_READ_ATTR_RESP_STRING] =
    {
        .name = "ReadAttributeRespString",
        .fmt = "ddds",
        .handler = processReadStringAttributeResponse,
    },
    [ZB_INMSG_READ_ATTR_RESP] =
    {
        .name = "ReadAttributeResp",
        .fmt = "dddd",
        .handler = processReadAttributeResponse,
    },
    [ZB_INMSG_READ_ATTR_RESP_FAIL] =
    {
        .name = "ReadAttributeResp_Fail",
        .fmt = "dddd",
        .handler = processReadAttributeResponseFailure,
    },
    [ZB_INMSG_DEV_ANNCE] =
    {
        .name = "DeviceAnnouncementReceived",
        .fmt = "ddd",
        .handler = processDeviceAnnceReceived,
    },
    [ZB_INMSG_ATTR_REPORT_IND] =
    {
        .name = "AttrReportIndication",
        .fmt = "dddd",
        .handler = processAttributesReportIndication,
    },
    [ZB_INMSG_LEAVE_RESP] =
    {
        .name = "LeaveRsp",
        .fmt = "dd",
        .handler = processLeaveResp,
    },
    [ZB_INMSG_PONG] =
    {
        .name = "pong",
        .fmt = "d",
        .handler = processPong,
    },
    [ZB_INMSG_ADD_GROUP_RESP_IND] =
    {
        .name = "addGroupResponseInd()",
        .fmt = "",
        .handler = processAddGroupRespIndication,
    },
    [ZB_INMSG_SIMPLE_DESC_RESP] =
    {
        .name = "SimpleDescResp",
        .fmt = "dddsds",
        .handler = processSimpleDescResp,
    },

    [ZB_INMSG_STACK_VERSION] =
    {
        .name = "ZigBeeStackVersion",
        .fmt = "d",
        .handler = processZigBeeStackVersionResp,
    },

    [ZB_INMSG_VERIFY_KEY] =
    {
        .name = "DeviceKeyVerification",
        .fmt = "",
        .handler = processDeviceVerifyKeyInd,
    },

    [ZB_INMSG_PRIMARYMASKSET] =
    {
        .name = "PrimaryMaskSet",
        .fmt = "",
        .handler = processPrimaryMaskSetInd,
    },

    [ZB_INMSG_SECONDARYMASKSET] =
    {
        .name = "SecondaryMaskSet",
        .fmt = "",
        .handler = processSecondaryMaskSetInd,
    },

    [ZB_INMSG_DEVICELEFT] =
    {
        .name = "DeviceLeftReceived",
        .fmt = "dd",
        .handler = processDeviceLeftInd,
    },

    [ZB_INMSG_FRAMECOUNTER] =
    {
        .name = "FrameCounter",
        .fmt = "d",
        .handler = processFrameCounterInd,
    },

    [ZB_INMSG_IEEE_ADDR_RESP] =
    {
        .name = "IeeeAddrResponse",
        .fmt = "ddd",
        .handler = processIeeeAddressResp,
    },

    [ZB_INMSG_WRITE_ATTR_RESP] =
    {
        .name = "WriteAttributeResponse:Success",
        .fmt = "",
        .handler = NULL,
    },

    [ZB_INMSG_DEFAULT] =
    /*  Last entry, must be kept all at NULL    */
    {
        .name = NULL,
        .fmt = NULL,
        .handler = NULL,
    },
};

static char incoming_msg_string_buffer[256];

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
extern TimerHandle_t ZigBeePingTimeout;
extern uint8_t ack_Module_Reset;

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void processPong(const ScanValue_t *args)
{
    if (args[0].uint8 != 0)
    {
        ZigBeeModuleResetOccurs();
        ZGB_PostEvents(ZBEVENT_PONG_RESET_RECEIVED);
        ack_Module_Reset = 1;
    }
    xTimerReset(ZigBeePingTimeout, 0);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void processLeaveResp(const ScanValue_t *args)
{
    uint8_t statId;

    if (args[1].uint8 == 0)
    {
        DBTHRD_GetData(DBTYPE_INDEX_FROM_ZBSHORTADDR, (void *)&statId, args[0].uint16);
    }
    else
    {
        statId = GetStatIdToRemove();
    }

    ZGB_PostEventsWithStatId(ZBEVENT_REMOVESTAT_RESP,statId);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F.Simard
* @date   2017/01/03
*******************************************************************************/
void processAttributesReportIndication(const ScanValue_t *args)
{
    ZigBeeIncomingAttributeMngmt(args[0].uint16,args[1].uint16, args[2].uint16, (void*)&args[3]);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F.Simard
* @date   2017/01/03
*******************************************************************************/
void processReadStringAttributeResponse(const ScanValue_t *args)
{
    ZigBeeIncomingAttributeMngmt(args[0].uint16,args[1].uint16, args[2].uint16, args[3].str);
    ZGB_PostEvents(ZBEVENT_READATTRIBUTERESPONSE);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F.Simard
* @date   2017/01/03
*******************************************************************************/
void processReadAttributeResponse(const ScanValue_t *args)
{
    ZigBeeIncomingAttributeMngmt(args[0].uint16,args[1].uint16, args[2].uint16, (void*)&args[3]);
    ZGB_PostEvents(ZBEVENT_READATTRIBUTERESPONSE);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F.Simard
* @date   2017/01/03
*******************************************************************************/
void processReadAttributeResponseFailure(const ScanValue_t *args)
{
    ZGB_PostEvents(ZBEVENT_READATTRIBUTERESPONSE);
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/**************************************************************************//**
\brief Processes command

\param[in] Str - string with command
******************************************************************************/
void processZBCommand(char *Str)
{
#if (MAX_NUM_OF_ARGS > 15)
  static ScanStack_t stk;
#else
  ScanStack_t stk;
#endif

  if (strcmp(Str, "pong 0") != 0)
  {
      memset(incoming_msg_string_buffer,0,256);
      sprintf(incoming_msg_string_buffer,
              "Incoming Zigbee msg <-%s",Str);
      CONSOLE_RUNTIME_MESSAGE(incoming_msg_string_buffer);
//    CONSOLE_RUNTIME_MESSAGE("Incoming Zigbee msg ->");
//    CONSOLE_MESSAGE_APPEND_STRING(Str);
  }
  xTimerReset(ZigBeePingTimeout, 0);

  if (!tokenizeStr(Str, &stk))
    return;

  if (stk.top == stk.args)                        // No command name
    return;

  /** Seek for a matching command */
  for (uint8_t i = 0; i < ZB_INMSG_MAX-1; i++)      //ZB_INMSG_MAX-1 => we don't want to parse the default data struct.
  {
    if (strcmp(stk.args[0].str, IncomingBitCloudMsg[i].name) == 0)    // Command match
    {
      if (unpackArgs(&IncomingBitCloudMsg[i], &stk))
      {
        FreeLastSentMsg(0, (zbIncomingMsg_t)i);
        if (IncomingBitCloudMsg[i].handler != NULL)
        {
            IncomingBitCloudMsg[i].handler(stk.args + 1);
        }
      }
      return;
    }
  }
}

/** Copyright(C) 2017 Stelpro Design, All Rights Reserved**/
