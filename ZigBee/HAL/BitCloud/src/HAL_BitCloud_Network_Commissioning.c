/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2017, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    HAL_BitCloud_Network_Commissioning.c
* @date    2017/02/27
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <string.h>

#include ".\console\inc\console.h"
#include ".\Application\inc\ZigBeeStateMachines.h"
#include ".\DB\classes\inc\class_Thermostats.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\DB\inc\THREAD_DBDataTypes.h"
#include ".\ZigBee\inc\THREAD_ZigBee.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/



/*******************************************************************************
*    Public variables definitions
*******************************************************************************/


/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/02/24
*******************************************************************************/
void processReceiveCommissioningStatus(const ScanValue_t *args)
{
    ZGB_PostEvents(ZBEVENT_COMMISSIONINGSTATUSOK);
}

/*******************************************************************************
* @brief Commissionning Request acknowledged by the radio module
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/11/08
*******************************************************************************/
void processCommissioningRequestAck(const ScanValue_t *args)
{

}

/*******************************************************************************
* @brief Install Code usage acknowledged by the radio module
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2018/10/29
*******************************************************************************/
void processInstallCodeUsageAck(const ScanValue_t *args)
{
    if (args[0].uint8 != 0)
    {
        ZBTHRD_SetInstallCode();
    }
}

/*******************************************************************************
* @brief Install Code acknowledged by the radio module
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2018/10/29
*******************************************************************************/
void processInstallCodeAck(const ScanValue_t *args)
{
    if (args[0].uint8 != 0)
    {
        //Install code not set
        DBTHRD_SetData(DBTYPE_INSTALLCODE_FAILED,(void*)0, INDEX_DONT_CARE,DBCHANGE_ZIGBEE);
    }
    else
    {
        DBTHRD_SetData(DBTYPE_INSTALLCODE_SUCCESS,(void*)0, INDEX_DONT_CARE,DBCHANGE_ZIGBEE);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/02/24
*******************************************************************************/
void processGetNetworkChannel(const ScanValue_t *args)
{
    dbType_ZigBeeNetworkInfo_t nwInfo;

    DBTHRD_GetData(DBTYPE_ZIGBEE_NETWORK_INFO,(void*)&nwInfo, INDEX_DONT_CARE);

    nwInfo.ActiveChannel = args[0].uint8;
    DBTHRD_SetData(DBTYPE_ZIGBEE_NETWORK_INFO,(void*)&nwInfo, INDEX_DONT_CARE,DBCHANGE_NETWORK_IS_SET);
    ZGB_PostEvents(ZBEVENT_NETWORKCHANNELRECEIVED);
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/02/24
*******************************************************************************/
void processGetNetworkAddress(const ScanValue_t *args)
{
    dbType_ZigBeeNetworkInfo_t nwInfo;

    DBTHRD_GetData(DBTYPE_ZIGBEE_NETWORK_INFO,(void*)&nwInfo, INDEX_DONT_CARE);

    nwInfo.ShortAdd = args[0].uint16;
    if (0xffff != nwInfo.ShortAdd)
    {
        nwInfo.InANetwork = 1;
    }
    else
    {
        nwInfo.InANetwork = 0;
    }

    DBTHRD_SetData(DBTYPE_ZIGBEE_NETWORK_INFO,(void*)&nwInfo, INDEX_DONT_CARE,DBCHANGE_ZIGBEE);

    ZGB_PostEvents(ZBEVENT_NETWORKADDRESSRECEIVED);
}

/*******************************************************************************
* @brief  Process the Pan Id response
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/04/14
*******************************************************************************/
void processPanIdResponse(const ScanValue_t *args)
{
    dbType_ZigBeeNetworkInfo_t nwInfo;

    DBTHRD_GetData(DBTYPE_ZIGBEE_NETWORK_INFO,(void*)&nwInfo, INDEX_DONT_CARE);

    if ((args[0].uint16 != 0) && (args[0].uint16 != 0xFFFF))
    {
        nwInfo.PanID = args[0].uint16;
        DBTHRD_SetData(DBTYPE_ZIGBEE_NETWORK_INFO,(void*)&nwInfo, INDEX_DONT_CARE,DBCHANGE_ZIGBEE);
    }
    ZGB_PostEvents(ZBEVENT_PANIDRECEIVED);
}

/*******************************************************************************
* @brief  Process the ZigBee Stack Version response
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/04/13
*******************************************************************************/
void processZigBeeStackVersionResp(const ScanValue_t *args)
{
    uint8_t zigbeeVersion;

    zigbeeVersion = args[0].uint8;
    DBTHRD_SetData(DBTYPE_ZIGBEE_VERSION,
                (void*)&zigbeeVersion,
                THIS_THERMOSTAT,
                DBCHANGE_ZIGBEE);
}

/*******************************************************************************
* @brief  Process the Verify Key
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/04/13
*******************************************************************************/
void processDeviceVerifyKeyInd(const ScanValue_t *args)
{
    ZGB_PostEvents(ZBEVENT_VERIFYKEY);
}

/*******************************************************************************
* @brief  Process the Primary channel mask set
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/04/13
*******************************************************************************/
void processPrimaryMaskSetInd(const ScanValue_t *args)
{
    ZGB_PostEvents(ZBEVENT_SETPRIMARYCHANNELMASK_IND);
}

/*******************************************************************************
* @brief  Process the Secondary channel mask set
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/04/13
*******************************************************************************/
void processSecondaryMaskSetInd(const ScanValue_t *args)
{
    ZGB_PostEvents(ZBEVENT_SETSECONDARYCHANNELMASK_IND);
}

/*******************************************************************************
* @brief  Process the Device Left message
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/11/06
*******************************************************************************/
void processDeviceLeftInd(const ScanValue_t *args)
{
    uint64_t extAddress;
    uint8_t statIndex;
    char model[33];

    extAddress = (uint64_t)args[0].uint32<<32 | args[1].uint32;
    DBTHRD_GetData(DBTYPE_INDEX_FROM_ZBEXTADDR, &statIndex, extAddress);
    if ((statIndex != THIS_THERMOSTAT) && (statIndex < THERMOSTAT_INSTANCES))
    {
        DBTHRD_GetData(DBTYPE_TSTAT_MODEL,(void*)&model, statIndex);
        if ((0 != strcmp("ST218",model)) && (0 != strcmp("STZB402+",model)))
        {
            classTH_DeleteThermostat(statIndex, DELETE_ONE_TERMOSTAT);
        }
    }
}

/*******************************************************************************
* @brief  Process the Frame Counter message
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/11/20
*******************************************************************************/
void processFrameCounterInd(const ScanValue_t *args)
{
    dbType_ZigBeeNetworkInfo_t nwInfo;
    uint32_t OutgoingFrameCounter;

    DBTHRD_GetData(DBTYPE_ZIGBEE_NETWORK_INFO,(void*)&nwInfo, INDEX_DONT_CARE);

    OutgoingFrameCounter = args[0].uint32;
    if (0 != OutgoingFrameCounter)
    {
        nwInfo.FrameCounter = OutgoingFrameCounter;
    }

    DBTHRD_SetData(DBTYPE_ZIGBEE_NETWORK_INFO,(void*)&nwInfo, INDEX_DONT_CARE,DBCHANGE_ZIGBEE);

    ZGB_PostEvents(ZBEVENT_FRAMECOUNTER_RECEIVED);


}

/*******************************************************************************
* @brief  Process rediscovered extended address (Ieee addr) from an unknown
          short address
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2018/01/30
*******************************************************************************/
void processIeeeAddressResp(const ScanValue_t *args)
{
    zigBeeDescriptor_t zbDescriptor;
    uint16_t rcvShortAddress;
    uint64_t rcvExtAddress;
    uint8_t i;

    rcvShortAddress = args[0].uint16;
    rcvExtAddress = (uint64_t)args[1].uint32<<32 | args[2].uint32;
    for (i = 1; i < THERMOSTAT_INSTANCES; i++)
    {
        DBTHRD_GetData(DBTYPE_ZIGBEE_DESCRIPTOR,(void*)&zbDescriptor, i);
        //Check if the device is already known
        if (zbDescriptor.extAddress == rcvExtAddress)
        {
            zbDescriptor.extAddress = rcvExtAddress;
            zbDescriptor.shortAddress = rcvShortAddress;
            DBTHRD_SetData(DBTYPE_ZIGBEE_DESCRIPTOR,(void*)&zbDescriptor, rcvExtAddress,DBCHANGE_LOCAL);
            break;
        }
    }
}

/** Copyright(C) 2017 Stelpro Design, All Rights Reserved**/
