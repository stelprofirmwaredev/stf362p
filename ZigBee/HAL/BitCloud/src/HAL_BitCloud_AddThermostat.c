/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2017, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    HAL_BitCloud_AddThermostat.c
* @date    2017/02/27
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <string.h>

#include ".\console\inc\console.h"
#include ".\Application\inc\ZigBeeStateMachines.h"
#include ".\Application\inc\ZigBee_ClustersEmulation.h"
#include ".\HAL\inc\HAL_ZigBeeInterface.h"

#include ".\DB\inc\THREAD_DB.h"



/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
typedef struct
{
    uint16_t clusterId;
    uint16_t attrId;
    ZCL_AttributeType_t attrType;
    uint16_t minTime;
    uint16_t maxTime;
    uint16_t change;
} confReport_t;

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
extern zbDeviceToAdd_t currentZigBeeDevice;

confReport_t confReportTable[] =
{
    /****************************/
    /*  Local Temperature       */
    /****************************/
    [CONFREPORT_ATTR_LOCALTEMP] =
    {
        .clusterId = 0x0201,
        .attrId = 0x0000,
        .attrType = ZCL_S16BIT_DATA_TYPE_ID,
        .minTime = 60,
        .maxTime = 300,
        .change = 50,
    },
    /****************************/
    /*  Heat setpoint           */
    /****************************/
    [CONFREPORT_ATTR_HEATSETPOINT] =
    {
        .clusterId = 0x0201,
        .attrId = 0x0012,
        .attrType = ZCL_S16BIT_DATA_TYPE_ID,
        .minTime = 1,
        .maxTime = 300,
        .change = 50,
    },
    /****************************/
    /*  Heat Demand             */
    /****************************/
    [CONFREPORT_ATTR_HEATDEMAND] =
    {
        .clusterId = 0x0201,
        .attrId = 0x0008,
        .attrType = ZCL_U8BIT_DATA_TYPE_ID,
        .minTime = 1,
        .maxTime = 300,
        .change = 1,
    },
    /****************************/
    /*  Power consumption       */
    /****************************/
    [CONFREPORT_ATTR_POWERCONSUMPTION] =
    {
        .clusterId = 0x0201,
        .attrId = 0x4002,
        .attrType = ZCL_U16BIT_DATA_TYPE_ID,
        .minTime = 360,
        .maxTime = 3600,
        .change = 1,
    },
    /****************************/
    /*  Alert                   */
    /****************************/
    [CONFREPORT_ATTR_ALERT] =
    {
        .clusterId = 0x0201,
        .attrId = 0x4003,
        .attrType = ZCL_U8BIT_DATA_TYPE_ID,
        .minTime = 1,
        .maxTime = 3600,
        .change = 1,
    },
    /****************************/
    /*  Floor Mode              */
    /****************************/
    [CONFREPORT_ATTR_FLOORMODE] =
    {
        .clusterId = 0x0201,
        .attrId = 0x4006,
        .attrType = ZCL_U8BIT_DATA_TYPE_ID,
        .minTime = 1,
        .maxTime = 0,
        .change = 1,
    },
    /****************************/
    /*  Relay Cycle Count       */
    /****************************/
    [CONFREPORT_ATTR_RELAYCYCLECOUNT] =
    {
        .clusterId = 0x0201,
        .attrId = 0x4007,
        .attrType = ZCL_U32BIT_DATA_TYPE_ID,
        .minTime = 10,
        .maxTime = 3600,
        .change = 1,
    },
    /****************************/
    /*  Temp. Display Mode      */
    /****************************/
    [CONFREPORT_ATTR_TEMPERATUREDISPLAYMODE] =
    {
        .clusterId = 0x0204,
        .attrId = 0x0000,
        .attrType = ZCL_8BIT_ENUM_DATA_TYPE_ID,
        .minTime = 1,
        .maxTime = 0,
        .change = 1,
    },
    /****************************/
    /*  Keypad lock             */
    /****************************/
    [CONFREPORT_ATTR_KEYPADLOCKOUT] =
    {
        .clusterId = 0x0204,
        .attrId = 0x0001,
        .attrType = ZCL_8BIT_ENUM_DATA_TYPE_ID,
        .minTime = 1,
        .maxTime = 0,
        .change = 1,
    },
    /****************************/
    /*  Humidity                */
    /****************************/
    [CONFREPORT_ATTR_LOCALHUMIDITY] =
    {
        .clusterId = 0x0405,
        .attrId = 0x0000,
        .attrType = ZCL_U16BIT_DATA_TYPE_ID,
        .minTime = 60,
        .maxTime = 300,
        .change = 500,
    },
};

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/24/03
*******************************************************************************/
void ReadRemoteAttributes(zbDeviceToAdd_t tStat, zigBeeClusterId_t clusterId, zigBeeAttrId_t attrId)
{
    char tmpBuffer [100];

    sprintf (tmpBuffer,"readAttribute 0 %d %d %d %d\n",
             tStat.zbDescriptor.shortAddress,
             tStat.zbDescriptor.thermostatEndPoint,
             clusterId,
             attrId);

    /* TODO : There is 3 kind of possible response : string, integer or fail;
    Which one to put here?  */
    SendZigBeeCmd(tmpBuffer,0,ZB_INMSG_DONT_CARE);

}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/03/23
*******************************************************************************/
void ConfigureReport (zbDeviceToAdd_t tStat, confReportTableAttrib_t tableIndex)
{
    char tmpBuffer [100];

    if (tableIndex < (sizeof(confReportTable)/sizeof(confReport_t)))
    {
        sprintf(tmpBuffer, "configureReportingWRC 0 %d %d %d %d %d %d %d %d\n",
                tStat.zbDescriptor.shortAddress,
                tStat.zbDescriptor.thermostatEndPoint,
                confReportTable[tableIndex].clusterId,          //cluster ID,
                confReportTable[tableIndex].attrId,         //attribute id,
                confReportTable[tableIndex].attrType,        //attribute type,
                confReportTable[tableIndex].minTime,//min time
                confReportTable[tableIndex].maxTime,//max time
                confReportTable[tableIndex].change);//change

        SendZigBeeCmd(tmpBuffer,0,ZB_INMSG_DONT_CARE);
    }

}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/03/23
*******************************************************************************/
void BindClusters (zbDeviceToAdd_t tStat, zigBeeClusterId_t cluster)
{
    char tmpBuffer [100];

    sprintf (tmpBuffer, "bindReq 0 %d %d %d %d 0x%04x\n",
             tStat.zbDescriptor.shortAddress,
             (uint32_t)((tStat.zbDescriptor.extAddress&0xffffffff00000000)>>32),
             (uint32_t)(tStat.zbDescriptor.extAddress&0x00000000ffffffff),
             tStat.zbDescriptor.thermostatEndPoint,
             cluster);

    SendZigBeeCmd(tmpBuffer,0,ZB_INMSG_DONT_CARE);
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/03/28
*******************************************************************************/
void Request_SimpleDescriptor(void)
{
    char tmpBuffer [100];

    sprintf(tmpBuffer,"simpleDescReq %d 25\n",
            currentZigBeeDevice.zbDescriptor.shortAddress);
    SendZigBeeCmd(tmpBuffer,0,ZB_INMSG_DONT_CARE);
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/03/28
*******************************************************************************/
void Request_MatchDescriptor(void)
{
    char tmpBuffer [100];

    sprintf(tmpBuffer,"matchDescReq %d 20\n",
            currentZigBeeDevice.zbDescriptor.shortAddress);
    SendZigBeeCmd(tmpBuffer,0,ZB_INMSG_DONT_CARE);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/03/23
*******************************************************************************/
void processSimpleDescResp(const ScanValue_t *args)
{
    uint16_t shortAddress = args[0].uint16;
    uint8_t endpoint = args[1].uint8;
    char *inClusterList;

    if (shortAddress == currentZigBeeDevice.zbDescriptor.shortAddress)
    {
        if (args[2].uint8 > 0)              //if inClusterCnt > 0
        {
            inClusterList = strtok(args[3].str,",");

            while (NULL != inClusterList)
            {
                if (0 == strcmp(inClusterList, "513"))      //cluster thermostat
                {
                    currentZigBeeDevice.deviceIsTstat = 1;
                    currentZigBeeDevice.zbDescriptor.thermostatEndPoint = endpoint;
                    currentZigBeeDevice.clusterToBind |= CLUSTERTOBIND_THERMOSTAT;
                }
                else if (0 == strcmp(inClusterList, "516"))      //cluster thermostat UI Conf
                {
                    currentZigBeeDevice.clusterToBind |= CLUSTERTOBIND_THERMOSTATUI;
                }
                else if (0 == strcmp(inClusterList, "1029"))      //cluster humidity
                {
                    currentZigBeeDevice.clusterToBind |= CLUSTERTOBIND_HUMIDITY;
                }
                else
                {
                }
                inClusterList = strtok(NULL,",");
            }
        }

        if (0 != currentZigBeeDevice.deviceIsTstat)
        {
            DBTHRD_SetData(DBTYPE_ZIGBEE_DESCRIPTOR,
                           (void*)&currentZigBeeDevice.zbDescriptor,
                           currentZigBeeDevice.zbDescriptor.extAddress,
                           DBCHANGE_ZIGBEE);

        }
        else
        {
            currentZigBeeDevice.clusterToBind = 0;
            DBTHRD_SetData(DBTYPE_OTHER_DEVICE_ADDED,(void*)0, INDEX_DONT_CARE, DBCHANGE_LOCAL);
        }
        ZGB_PostEvents(ZBEVENT_SIMPLEDESCRESP);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/02/27
*******************************************************************************/
void processMatchDescriptorResponse(const ScanValue_t *args)
{
    uint16_t shortAddress = args[0].uint16;
    uint8_t endpoint = args[1].uint8;

    if (shortAddress == currentZigBeeDevice.zbDescriptor.shortAddress)
    {
        if ((endpoint != 0) && (endpoint != 255))
        {
            currentZigBeeDevice.deviceIsTstat = 1;
        }
        currentZigBeeDevice.zbDescriptor.thermostatEndPoint = endpoint;
        currentZigBeeDevice.clusterToBind |= CLUSTERTOBIND_THERMOSTAT;
        currentZigBeeDevice.clusterToBind |= CLUSTERTOBIND_THERMOSTATUI;
        currentZigBeeDevice.clusterToBind |= CLUSTERTOBIND_HUMIDITY;

        if (0 != currentZigBeeDevice.deviceIsTstat)
        {
            DBTHRD_SetData(DBTYPE_ZIGBEE_DESCRIPTOR,
                           (void*)&currentZigBeeDevice.zbDescriptor,
                           currentZigBeeDevice.zbDescriptor.extAddress,
                           DBCHANGE_ZIGBEE);
        }
        else
        {
            currentZigBeeDevice.clusterToBind = 0;
            DBTHRD_SetData(DBTYPE_OTHER_DEVICE_ADDED,(void*)0, INDEX_DONT_CARE, DBCHANGE_LOCAL);
        }
        ZGB_PostEvents(ZBEVENT_MATCHDESCRESP);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/02/28
*******************************************************************************/
void processConfigureReportingResponse(const ScanValue_t *args)
{
    if (0 == args[0].int8)
    {
        ZGB_PostEvents(ZBEVENT_CONFIGUREREPORTINGRESPONSE);
    }
    else
    {
        ZGB_PostEvents(ZBEVENT_CONFIGUREREPORTINGTERROR);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/02/28
*******************************************************************************/
void processBindingResponse(const ScanValue_t *args)
{
    if (0 == args[0].int8)           // 0 = success
    {
        ZGB_PostEvents(ZBEVENT_BINDRESPONSE);
    }
    else
    {
        ZGB_PostEvents(ZBEVENT_BINDREQUESTERROR);
    }

}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/02/28
*******************************************************************************/
void processAddGroupRespIndication(const ScanValue_t *args)
{
    ZGB_PostEvents(ZBEVENT_UPDATEGROUP_COMPLETED);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/03/01
*******************************************************************************/
void processDeviceAnnceReceived(const ScanValue_t *args)
{
    currentZigBeeDevice.zbDescriptor.shortAddress = args[0].uint16;
    currentZigBeeDevice.zbDescriptor.extAddress = (uint64_t)args[1].uint32<<32 | args[2].uint32;
    ZGB_PostEvents(ZBEVENT_DEVICEANNCERECEIVED);
}

/** Copyright(C) 2017 Stelpro Design, All Rights Reserved**/
