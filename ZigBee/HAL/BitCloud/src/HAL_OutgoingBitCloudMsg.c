/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    HAL_OutgoingBitCloudMsg.c
* @date    2017/06/12
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include ".\HAL\BitCloud\inc\HAL_OutgoingBitCloudMsg.h"
#include ".\HAL\inc\HAL_ZigBeeInterface.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define FRAME_COUNTER_OFFSET    (0x10000)

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/



/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
void ZbSetPrimaryChannelMask (uint8_t channel)
{
    char tmpBuffer[35];

    sprintf(tmpBuffer,"setPrimaryChannelMask %d \r\n", (1 << channel));
    SendZigBeeCmd(tmpBuffer,3,ZB_INMSG_PRIMARYMASKSET);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
void ZbSetSecondaryChannelMask (uint8_t channel)
{
    char tmpBuffer[35];

    sprintf(tmpBuffer,"setSecondaryChannelMask %d \r\n", 0/*(1 << channel)*/);
    SendZigBeeCmd(tmpBuffer,3,ZB_INMSG_SECONDARYMASKSET);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
void ZbReadStackVersion (void)
{
    SendZigBeeCmd("readStackVersion\r\n",9,ZB_INMSG_STACK_VERSION);
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
void ZbGetNetworkChannel (void)
{
    SendZigBeeCmd("getChannel\n",-1,ZB_INMSG_NETWORK_CHANNEL);
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
void ZbGetDeviceNetworkAddress(void)
{
    SendZigBeeCmd("getNetworkAddress\n",-1,ZB_INMSG_NETWORK_ADDRESS);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
void ZbFormNewNetwork(void)
{
    SendZigBeeCmd("invokeCommissioning 4 0\n",5,ZB_INMSG_COMMISSION_REQUEST_ACK);       //form network (4)
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2018/10/29
*******************************************************************************/
void ZbSetUseOfInstallCode(void)
{
    SendZigBeeCmd("SetbdbJoinUsesInstallCodeKey 1\n",5,ZB_INMSG_INSTALL_CODE_USAGE_ACK);  //Use install code
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2018/10/29
*******************************************************************************/
void ZbClearUseOfInstallCode(void)
{
    SendZigBeeCmd("SetbdbJoinUsesInstallCodeKey 0\n",5,ZB_INMSG_INSTALL_CODE_USAGE_ACK);  //Do not use install code
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2018/10/29
*******************************************************************************/
void ZbSetInstallCode(uint8_t* pMacAddress, uint8_t* pInstallCode)
{
    char tmpBuffer[80];
    char str1[9];
    char str2[9];
    uint32_t mac1;
    uint32_t mac2;
    char* pEnd;
    uint8_t iCodeReversed[37];
    uint8_t i;

    memcpy(&str1, &pMacAddress[0], 8);
    str1[8] = 0;
    memcpy(&str2, &pMacAddress[8], 8);
    str2[8] = 0;
    mac1 = strtoul(str1, &pEnd, 16);
    mac2 = strtoul(str2, &pEnd, 16);
    for (i = 0; i < 32; i+=2)
    {
        iCodeReversed[34-i] = *(pInstallCode+i);
        iCodeReversed[35-i] = *(pInstallCode+i+1);
    }
    iCodeReversed[0] = *(pInstallCode+34);
    iCodeReversed[1] = *(pInstallCode+35);
    iCodeReversed[2] = *(pInstallCode+32);
    iCodeReversed[3] = *(pInstallCode+33);
    iCodeReversed[36] = 0;
    sprintf(tmpBuffer,"SetInstallCode 0x%08x 0x%08x %s\r\n", mac1, mac2, &iCodeReversed[0]);
    SendZigBeeCmd(tmpBuffer,5,ZB_INMSG_INSTALL_CODE_ACK);   //Extended address + Install Code
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/08/16
*******************************************************************************/
void ZbSetPanId (uint16_t panid)
{
    char strBuf[35];

    sprintf (strBuf, "setPanId 0x%x\r\n", panid);
    SendZigBeeCmd(strBuf, 0, ZB_INMSG_DONT_CARE);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
void ZbNetworkSteering(void)
{
    SendZigBeeCmd("invokeCommissioning 2 0\n",5,ZB_INMSG_COMMISSION_REQUEST_ACK);       //steer network (2)
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
void ZbNetworkSteeringAndForm(void)
{
    SendZigBeeCmd("invokeCommissioning 6 0\n",5,ZB_INMSG_COMMISSION_REQUEST_ACK);       //steer form network (2 + 4)
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
void ZbGetPanId(void)
{
    SendZigBeeCmd("getPanId\r\n",-1,ZB_INMSG_PAN_ID);
}

/*******************************************************************************
* @brief Set Permit Join for {duration}
* @inputs duration of the permit join; expressed in seconds
* @retval None
* @author J-F. Simard
* @date   2017/03/23
*******************************************************************************/
void SetPermitJoin (uint8_t duration)
{
    char tmpBuffer[35];
//   {"sendMgmtPermitJoin", "ddd", processMgmtSendPermitJoinCmd, "[dstAddr][dur][tcSig]\r\n"},

    sprintf(tmpBuffer,"sendMgmtPermitJoin 0xffff %d 1\n",duration);
    SendZigBeeCmd(tmpBuffer,0,ZB_INMSG_DONT_CARE);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/16
*******************************************************************************/
void StartFindAndBind(void)
{
    SendZigBeeCmd("invokeCommissioning 8 0\n",0,ZB_INMSG_DONT_CARE); //8 : start find and bind procedure
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/20
*******************************************************************************/
void ResettoFactoryNew(void)
{
    SendZigBeeCmd("resetToFN\n",0,ZB_INMSG_DONT_CARE);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/20
*******************************************************************************/
void SoftReset(void)
{
    SendZigBeeCmd("reset\n",0,ZB_INMSG_DONT_CARE);
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/07/11
*******************************************************************************/
void ZbBootLoaderHandShakeRequest(void)
{
    SendZigBeeMsg((char[])ZB_BOOTLOADER_HANDSHAKE_REQ,ZB_BOOTLOADER_CMD_LENGTH,0,ZB_INMSG_DONT_CARE);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/07/11
*******************************************************************************/
void ZbBootLoaderSendRecord (uint8_t * buf, uint8_t length)
{
    SendZigBeeMsg ((char*)buf, length, 0, ZB_INMSG_DONT_CARE);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/11/20
*******************************************************************************/
void ZbRequestFrameCounter(void)
{
    SendZigBeeCmd("getFrameCounter\n",0,ZB_INMSG_DONT_CARE);
}

/*******************************************************************************
* @brief
* @inputs uint32_t frameCounter
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/11/20
*******************************************************************************/
void ZbSetFrameCounter(uint32_t frameCounter)
{
    char strBuf[35];

    sprintf (strBuf, "setFrameCounter %d\n", frameCounter + FRAME_COUNTER_OFFSET);
    SendZigBeeCmd(strBuf, 3, ZB_INMSG_FRAMECOUNTER);
}

/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
