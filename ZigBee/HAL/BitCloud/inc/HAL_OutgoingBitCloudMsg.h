/*******************************************************************************
* @file    HAL_OutgoingBitCloudMsg.h
* @date    2017/06/12
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef HAL_OutgoingBitCloudMsg_H
#define HAL_OutgoingBitCloudMsg_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>
#include ".\Application\inc\ZigBee_ClustersEmulation.h"
#include ".\Application\inc\ZigBeeStateMachines.h"

/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define ZB_BOOTLOADER_CMD_LENGTH            4
#define ZB_BOOTLOADER_HANDSHAKE_REQ         {0xb2,0xa5,0x65,0x4b,0}
#define ZB_BOOTLOADER_HANDSHAKE_CONF        {0x69,0xD3,0xD2,0x26,0}
#define ZB_BOOTLOADER_ACK                   {0x4D,0x5A,0x9A,0xB4,0}
#define ZB_BOOTLOADER_NACK                  {0x2D,0x59,0x5A,0xB2,0}

/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void ZbReadStackVersion (void);
void ZbGetNetworkChannel (void);
void ZbGetDeviceNetworkAddress(void);
void ZbFormNewNetwork(void);
void ZbGetPanId(void);
void SetPermitJoin (uint8_t duration);

void BindClusters (zbDeviceToAdd_t tStat, zigBeeClusterId_t cluster);
void ConfigureReport (zbDeviceToAdd_t tStat, confReportTableAttrib_t tableIndex);
void ReadRemoteAttributes(zbDeviceToAdd_t tStat, zigBeeClusterId_t clusterId, zigBeeAttrId_t attrId);
void Request_SimpleDescriptor(void);
void Request_MatchDescriptor(void);
void StartFindAndBind(void);
void ResettoFactoryNew(void);
void SoftReset(void);
void ZbNetworkSteering(void);
void ZbNetworkSteeringAndForm(void);
void ZbSetPrimaryChannelMask (uint8_t channel);
void ZbSetSecondaryChannelMask (uint8_t channel);
void ZbSetPanId (uint16_t panid);
void ZbRequestFrameCounter(void);
void ZbSetFrameCounter(uint32_t frameCounter);
void ZbSetUseOfInstallCode(void);
void ZbClearUseOfInstallCode(void);
void ZbSetInstallCode(uint8_t* pMacAddress, uint8_t* pInstallCode);

void ZbBootLoaderHandShakeRequest(void);
void ZbBootLoaderSendRecord (uint8_t * buf, uint8_t length);

#endif /* HAL_OutgoingBitCloudMsg_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
