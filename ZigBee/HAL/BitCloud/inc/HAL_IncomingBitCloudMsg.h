/*******************************************************************************
* @file    HAL_IncomingBitCloudMsg.h
* @date    2017/02/23
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef HAL_IncomingBitCloudMsg_H
#define HAL_IncomingBitCloudMsg_H


/*******************************************************************************
* Includes
*******************************************************************************/



/*******************************************************************************
* Public constants definitions
*******************************************************************************/
typedef enum
{
    ZB_INMSG_COMMISSION_STATUS = 0,
    ZB_INMSG_NETWORK_CHANNEL,
    ZB_INMSG_NETWORK_ADDRESS,
    ZB_INMSG_MATCH_DESC_RESP,
    ZB_INMSG_CONFIG_REPORT_RESP,
    ZB_INMSG_BIND_RESP,
    ZB_INMSG_READ_ATTR_RESP_STRING,
    ZB_INMSG_READ_ATTR_RESP,
    ZB_INMSG_READ_ATTR_RESP_FAIL,
    ZB_INMSG_DEV_ANNCE,
    ZB_INMSG_ATTR_REPORT_IND,
    ZB_INMSG_LEAVE_RESP,
    ZB_INMSG_PONG,
    ZB_INMSG_ADD_GROUP_RESP_IND,
    ZB_INMSG_SIMPLE_DESC_RESP,
    ZB_INMSG_STACK_VERSION,
    ZB_INMSG_PAN_ID,
    ZB_INMSG_VERIFY_KEY,
    ZB_INMSG_PRIMARYMASKSET,
    ZB_INMSG_SECONDARYMASKSET,
    ZB_INMSG_DEVICELEFT,
    ZB_INMSG_FRAMECOUNTER,
    ZB_INMSG_COMMISSION_REQUEST_ACK,
    ZB_INMSG_INSTALL_CODE_USAGE_ACK,
    ZB_INMSG_INSTALL_CODE_ACK,
    ZB_INMSG_IEEE_ADDR_RESP,
    ZB_INMSG_WRITE_ATTR_RESP,

    ZB_INMSG_DEFAULT,

    ZB_INMSG_MAX,

    ZB_INMSG_DONT_CARE = -1,
} zbIncomingMsg_t;


/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void processZBCommand(char *str);



#endif /* HAL_IncomingBitCloudMsg_H */
/** Copyright(C) 2017 Stelpro Design, All Rights Reserved**/
