/*******************************************************************************
* @file    HAL_ZigBeeInterface.h
* @date    2016/09/20
* @authors Jean-Fran�ois Many
* @brief
*******************************************************************************/

#ifndef HAL_ZigBeeInterface_H
#define HAL_ZigBeeInterface_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include "cmsis_os.h"
#include ".\HAL\BitCloud\inc\HAL_IncomingBitCloudMsg.h"


/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define CONSOLE_RX_BUFFER_LENGTH            256
#define MAX_ZB_MSG_LENGTH                   100

/*******************************************************************************
* Public structures definitions
*******************************************************************************/
typedef enum
{
    ZBMSG_NEW       = 0,
    ZBMSG_IN_TRANSFER,
    ZBMSG_SENT,
    ZBMSG_RETRY,
} zbSerialMsgState_t;

typedef struct _zbSerialMsg_t
{
    char str[MAX_ZB_MSG_LENGTH];
    uint8_t msgLength;
    uint8_t retries;
    zbSerialMsgState_t msgState;
    zbIncomingMsg_t expectedRsp;
    struct _zbSerialMsg_t *next;
} zbSerialMsg_t;


/*******************************************************************************
* Public variables declarations
*******************************************************************************/
#ifdef FREERTOS_SUPPORT
    #include "trcRecorder.h"
    extern char* zbEventChannel;
#endif

/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void InitZigBeeInterface(void);
uint8_t UART_Init(void);
void HAL_WriteUART(const char *, uint8_t length);
uint16_t HAL_ReadUART(uint8_t *);
void ZigBeeUartTx_Callback(UART_HandleTypeDef *);
void ZigBeeUartRx_Callback(UART_HandleTypeDef *);
void ZigBeeUartError_Callback(UART_HandleTypeDef *);
void ZigBeeUartHalfRx_Callback(UART_HandleTypeDef *);
void TurnOnZigBeeModule(void);
void TurnOffZigBeeModule(void);

void HAL_SetupDMA_Rx (uint16_t );
void ZigBeeUartRx_PeriodicBufferRead (TimerHandle_t );
void ZigBeeModule_HardReset(void);
void SendZigBeeMsg(char *, uint8_t, uint8_t , zbIncomingMsg_t);
void SendZigBeeCmd(char *, uint8_t , zbIncomingMsg_t);
void FreeLastSentMsg(uint8_t, zbIncomingMsg_t);
void SendNextMsg(void);
void ResendLastMsg (void);
void RxBootloaderMsg(uint8_t incomingByte);
uint8_t isZigbeeUpAndRunning(void);
uint8_t isZigbeeUartInError(void);

#endif /* HAL_ZigBeeInterface_H */
/** Copyright(C) 2014 Stelpro Design, All Rights Reserved**/
