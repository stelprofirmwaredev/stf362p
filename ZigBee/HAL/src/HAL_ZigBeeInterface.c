/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2015, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    HAL_ZigBeeInterface.c
* @date    2016/09/20
* @authors Jean-Fran�ois Many
* @brief   HAL ZigBee Interface module
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdio.h>
#include <string.h>
#include ".\HAL\inc\HAL_ZigBeeInterface.h"
#include ".\HAL\BitCloud\inc\HAL_OutgoingBitCloudMsg.h"


#include "stm32f4xx_hal.h"
#include "cmsis_os.h"
#include "HAL_ZigBeeInterface.h"
#include "LeaveNetworkMenu.h"
#include ".\ZigBee\inc\THREAD_ZigBee.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\Console\inc\THREAD_Console.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/


/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
static DMA_HandleTypeDef hdma_uart_rx;
static DMA_HandleTypeDef hdma_uart_tx;
static uint8_t halZigBeeRxBuffer[CONSOLE_RX_BUFFER_LENGTH];
static char outgoing_msg_string_buffer[256];
uint16_t halRxBufferPoR;

zbSerialMsg_t * zbMsg_Head = NULL;
zbSerialMsg_t * zbMsg_Tail = NULL;

TimerHandle_t ZigBeeMessagesTimeout = NULL;

extern osThreadId zigBeeTaskHandle;
extern TimerHandle_t ZigBeePingTrigger;
extern TimerHandle_t ZigBeePingTimeout;
extern uint8_t ack_Module_Reset;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
UART_HandleTypeDef ZigBeeUART;
uint8_t ZigbeeIsUpAndRunning;
volatile uint8_t ZigBeeUartInError;

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
extern uint8_t isZigBeeModuleUnusable(void);


/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval 0 : success
*         !0 : some error
* @author J-F. Simard
* @date   2016/08/17
*******************************************************************************/
uint8_t UART_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;

#ifdef BIS_CONTROLLER
    __HAL_RCC_GPIOD_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOE_CLK_ENABLE();
    __HAL_RCC_UART5_CLK_ENABLE();
    __HAL_RCC_DMA1_CLK_ENABLE();

    ZigBeeUART.Instance = UART5;
    GPIO_InitStruct.Alternate = GPIO_AF8_UART5;
    hdma_uart_rx.Instance = DMA1_Stream0;
    hdma_uart_rx.Init.Channel = DMA_CHANNEL_4;
    hdma_uart_tx.Instance = DMA1_Stream7;
    hdma_uart_tx.Init.Channel = DMA_CHANNEL_4;


#else
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOE_CLK_ENABLE();
    __HAL_RCC_UART4_CLK_ENABLE();
    __HAL_RCC_DMA1_CLK_ENABLE();

    ZigBeeUART.Instance = UART4;
    GPIO_InitStruct.Alternate = GPIO_AF8_UART4;
    hdma_uart_rx.Instance = DMA1_Stream2;
    hdma_uart_rx.Init.Channel = DMA_CHANNEL_4;
    hdma_uart_tx.Instance = DMA1_Stream4;
    hdma_uart_tx.Init.Channel = DMA_CHANNEL_4;
#endif

    /* Peripheral clock enable */
    HAL_UART_DeInit(&ZigBeeUART);


    HAL_GPIO_WritePin(ZIGBEE_TX_GPIO_Port, ZIGBEE_TX_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(ZIGBEE_UART_RX_GPIO_Port, ZIGBEE_UART_RX_Pin, GPIO_PIN_RESET);

    GPIO_InitStruct.Pin = ZIGBEE_TX_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;

    HAL_GPIO_Init(ZIGBEE_TX_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = ZIGBEE_UART_RX_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(ZIGBEE_UART_RX_GPIO_Port, &GPIO_InitStruct);


    ZigBeeUART.Init.BaudRate = 38400;
    ZigBeeUART.Init.WordLength = UART_WORDLENGTH_8B;
    ZigBeeUART.Init.StopBits = UART_STOPBITS_1;
    ZigBeeUART.Init.Parity = UART_PARITY_NONE;
    ZigBeeUART.Init.Mode = UART_MODE_TX_RX;
    ZigBeeUART.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    ZigBeeUART.Init.OverSampling = UART_OVERSAMPLING_16;

    hdma_uart_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
    hdma_uart_rx.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_uart_rx.Init.MemInc = DMA_MINC_ENABLE;
    hdma_uart_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma_uart_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    hdma_uart_rx.Init.Mode = DMA_CIRCULAR;
    hdma_uart_rx.Init.Priority = DMA_PRIORITY_HIGH;
    hdma_uart_rx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;

    hdma_uart_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
    hdma_uart_tx.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_uart_tx.Init.MemInc = DMA_MINC_ENABLE;
    hdma_uart_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma_uart_tx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    hdma_uart_tx.Init.Mode = DMA_NORMAL;
    hdma_uart_tx.Init.Priority = DMA_PRIORITY_HIGH;
    hdma_uart_tx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;



    HAL_DMA_Init(&hdma_uart_rx);
    __HAL_LINKDMA(&ZigBeeUART,hdmarx,hdma_uart_rx);

    HAL_DMA_Init(&hdma_uart_tx);
    __HAL_LINKDMA(&ZigBeeUART,hdmatx,hdma_uart_tx);

    HAL_UART_Init(&ZigBeeUART);
#ifdef BIS_CONTROLLER
    /* UART5_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(UART5_IRQn, 6, 0);
    HAL_NVIC_EnableIRQ(UART5_IRQn);

    HAL_NVIC_SetPriority(DMA1_Stream7_IRQn, 6, 0);
    HAL_NVIC_EnableIRQ(DMA1_Stream7_IRQn);
#else
    /* UART4_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(UART4_IRQn, 6, 0);
    HAL_NVIC_EnableIRQ(UART4_IRQn);

    HAL_NVIC_SetPriority(DMA1_Stream4_IRQn, 6, 0);
    HAL_NVIC_EnableIRQ(DMA1_Stream4_IRQn);
#endif
    /**UART4 GPIO Configuration
    PA0/WKUP     ------> UART4_TX
    PC11     ------> UART4_RX
    */

//    HAL_GPIO_WritePin(ZIGBEE_TX_GPIO_Port, ZIGBEE_TX_Pin, GPIO_PIN_RESET);
//    HAL_GPIO_WritePin(ZIGBEE_UART_RX_GPIO_Port, ZIGBEE_UART_RX_Pin, GPIO_PIN_RESET);
//
//    GPIO_InitStruct.Pin = ZIGBEE_TX_Pin;
//    GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
//    GPIO_InitStruct.Pull = GPIO_PULLUP;
//    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
//    GPIO_InitStruct.Alternate = GPIO_AF8_UART4;
//    HAL_GPIO_Init(ZIGBEE_TX_GPIO_Port, &GPIO_InitStruct);
//
//    GPIO_InitStruct.Pin = ZIGBEE_UART_RX_Pin;
//    GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
//    GPIO_InitStruct.Pull = GPIO_NOPULL;
//    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
//    GPIO_InitStruct.Alternate = GPIO_AF8_UART4;
//    HAL_GPIO_Init(ZIGBEE_UART_RX_GPIO_Port, &GPIO_InitStruct);


    if (0 != (ZigBeeUART.Instance->SR & 0x100))
    {
        ZigBeeUART.Instance->SR &= ~0x100;
        return 1;
    }
    else if (0 != HAL_GPIO_ReadPin(ZIGBEE_UART_RX_GPIO_Port, ZIGBEE_UART_RX_Pin))
    {
        extern TimerHandle_t bBitCloudPeriodicRx;

        HAL_SetupDMA_Rx(CONSOLE_RX_BUFFER_LENGTH);
        ZigbeeIsUpAndRunning = 1;
        //Clear ZigBeeUartInError only if the Rx DMA state is "normal"
        switch(ZigBeeUART.hdmarx->State)
        {
            case HAL_DMA_STATE_RESET:
            case HAL_DMA_STATE_READY:
            case HAL_DMA_STATE_BUSY:
                ZigBeeUartInError = 0;
                break;

            default:
                break;
        }
        xTimerStart(bBitCloudPeriodicRx, 0);

        return 0;
    }
    else
    {
        return 1;
    }
}

/*******************************************************************************
* @brief
* @inputs xTimer: timer instance
* @retval None
* @author J-F. Simard
* @date 2017/04/04
*******************************************************************************/
void OnZigBeeMsgTimeout( TimerHandle_t xTimer )
{
    xTaskNotify(zigBeeTaskHandle, ZBTHRD_MSG_TIMEOUT, eSetBits);
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void ZigBeeModule_HardReset(void)
{
    extern TimerHandle_t bBitCloudPeriodicRx;

    if (NULL != bBitCloudPeriodicRx)
    {
        xTimerStop(bBitCloudPeriodicRx, 0);
    }
    ZigbeeIsUpAndRunning = 0;

    xTimerStop(ZigBeePingTrigger, 0);
    xTimerStop(ZigBeePingTimeout, 0);
    ack_Module_Reset = 0;

    TurnOffZigBeeModule();
    TurnOnZigBeeModule();
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void InitZigBeeInterface(void)
{
//    TurnOffZigBeeModule();
//    TurnOnZigBeeModule();

/************************************************************************/
/*  Because this function can be called from another context than a POR,*/
/*  heap needs to be free in case some messages are still queued at the */
/*  moment of doing the Init();                                         */
/************************************************************************/
/*	Critical section to avoid another thread requests this heap memory  */
/*  before the end of the free operations. The data pointed by          */
/*  zbMsg_Head must remains integral until the end of the loop.		    */
/************************************************************************/
    taskENTER_CRITICAL();

    while (NULL != zbMsg_Head)
    {
        vPortFree(zbMsg_Head);
        zbMsg_Head = zbMsg_Head->next;
    }

    taskEXIT_CRITICAL();
/********************************************/
    zbMsg_Head = NULL;
    zbMsg_Tail = NULL;

    if (NULL == ZigBeeMessagesTimeout)
    {
        ZigBeeMessagesTimeout = xTimerCreate ("ZigBee Msg Timeout",
                                           pdMS_TO_TICKS(1000),
                                           pdFALSE,     //one-shot
                                           (void*)0,
                                           OnZigBeeMsgTimeout);
        configASSERT(ZigBeeMessagesTimeout);
    }
//    InitUartDma(&ZigBeeUART);
//    UART_Init();
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void TurnOnZigBeeModule(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    __HAL_RCC_GPIOE_CLK_ENABLE();

    HAL_GPIO_WritePin(ZIGBEE_POWER_GPIO_Port, ZIGBEE_POWER_Pin, GPIO_PIN_RESET);
//    HAL_Delay(100);

    GPIO_InitStruct.Pin = ZIGBEE_POWER_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;//GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(ZIGBEE_POWER_GPIO_Port, &GPIO_InitStruct);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void TurnOffZigBeeModule(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;
//    GPIO_PinState ZBBypassState;

    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOE_CLK_ENABLE();

//    //Check if ZigBee module should be left powered
//    ZBBypassState = HAL_GPIO_ReadPin(ZIGBEE_BYPASS_GPIO_Port, ZIGBEE_BYPASS_Pin);
//    if (ZBBypassState != 0)
//    {
        HAL_GPIO_WritePin(ZIGBEE_POWER_GPIO_Port, ZIGBEE_POWER_Pin, GPIO_PIN_SET);
//    }
    HAL_GPIO_WritePin(ZIGBEE_TX_GPIO_Port, ZIGBEE_TX_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(ZIGBEE_UART_RX_GPIO_Port, ZIGBEE_UART_RX_Pin, GPIO_PIN_RESET);

    GPIO_InitStruct.Pin = ZIGBEE_POWER_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;//GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(ZIGBEE_POWER_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = ZIGBEE_TX_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(ZIGBEE_TX_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = ZIGBEE_UART_RX_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(ZIGBEE_UART_RX_GPIO_Port, &GPIO_InitStruct);


    HAL_UART_DeInit(&ZigBeeUART);
    if (NULL != hdma_uart_rx.Instance)
    {
        HAL_DMA_DeInit(&hdma_uart_rx);
    }

    if (NULL != hdma_uart_tx.Instance)
    {
        HAL_DMA_DeInit(&hdma_uart_tx);
    }
    HAL_Delay(50);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void HAL_WriteUART(const char *buffer, uint8_t length)
{
    if (0 == isInUpdateZbModule())
    {
        if (0 != strncmp("ping 0\r\n", buffer, length))
        {
              memset(outgoing_msg_string_buffer,0,256);
              sprintf(outgoing_msg_string_buffer,
                      "Outgoing Zigbee msg ->%s",buffer);
              CONSOLE_RUNTIME_MESSAGE(outgoing_msg_string_buffer);
//            CONSOLE_RUNTIME_MESSAGE("Outgoing Zigbee msg ->");
//            CONSOLE_MESSAGE_APPEND_STRING((char*)buffer);
        }
    }
    HAL_UART_Transmit_DMA(&ZigBeeUART, (uint8_t *)buffer, length);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
uint16_t HAL_ReadUART(uint8_t *buffer)
{
    uint8_t i ;
    uint32_t length = (CONSOLE_RX_BUFFER_LENGTH - ZigBeeUART.hdmarx->Instance->NDTR - halRxBufferPoR) % CONSOLE_RX_BUFFER_LENGTH;
    for (i = 0; (i < length) && (halRxBufferPoR < ZigBeeUART.RxXferSize ); i++)
    {
        buffer[i] = halZigBeeRxBuffer[halRxBufferPoR];
        halZigBeeRxBuffer[halRxBufferPoR] = 0xfd;
        halRxBufferPoR = (halRxBufferPoR+1)%CONSOLE_RX_BUFFER_LENGTH;
    }
    buffer[i] = 0;
    return i;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void ZigBeeUartTx_Callback(UART_HandleTypeDef *huart)
{
    xTaskNotifyFromISR(zigBeeTaskHandle, ZBTHRD_MSG_SENT, eSetBits, &(BaseType_t){0});
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void ZigBeeUartRx_Callback(UART_HandleTypeDef *huart)
{
    halRxBufferPoR = 0;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void ZigBeeUartError_Callback(UART_HandleTypeDef *huart)
{
    ZigBeeUartInError = 1;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/11/04
*******************************************************************************/
void ZigBeeUartRx_PeriodicBufferRead (TimerHandle_t xTimer)
{
    uint8_t byteAmount = (CONSOLE_RX_BUFFER_LENGTH - ZigBeeUART.hdmarx->Instance->NDTR - halRxBufferPoR) % CONSOLE_RX_BUFFER_LENGTH;

//    if (0 != (ZigBeeUART.Instance->SR & 0x100))
//    {
////        ZigBeeUART.Instance->SR &= ~0x100;
////        ZBTHRD_ReinitUart();
//    }
//    else if (byteAmount > 0)
    {
        xTaskNotify(zigBeeTaskHandle, ZBTHRD_INCOMING_MSG, eSetBits);
    }
}



/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/11/03
*******************************************************************************/
void HAL_SetupDMA_Rx (uint16_t numberOfBytes)
{
    HAL_UART_Receive_DMA(&ZigBeeUART, halZigBeeRxBuffer, numberOfBytes);
    halRxBufferPoR = 0;
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void SendNextMsg(void)
{
    if ((NULL != zbMsg_Head) && (0 != ZigbeeIsUpAndRunning))
    {
        if ((ZBMSG_NEW == zbMsg_Head->msgState) ||
            (ZBMSG_RETRY == zbMsg_Head->msgState))
        {
            HAL_WriteUART(zbMsg_Head->str, zbMsg_Head->msgLength);
            xTimerStart(ZigBeeMessagesTimeout, 0);
            zbMsg_Head->msgState = ZBMSG_IN_TRANSFER;
            if ((zbMsg_Head->retries > 0) && (-1 != (int8_t)zbMsg_Head->retries))
            {
                zbMsg_Head->retries--;
            }
        }
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void FreeLastSentMsg(uint8_t force, zbIncomingMsg_t inMsg)
{
/************************************************************************/
/*	Critical section to avoid another thread requests this heap memory  */
/*  before the end of the free operations. The data pointed by          */
/*  zbMsg_Head must remains integral until the end of the loop.		    */
/************************************************************************/
    taskENTER_CRITICAL();
    if (NULL != zbMsg_Head)
    {
        if ((0 != force) ||
            ((ZBMSG_SENT == zbMsg_Head->msgState) &&
                ((0 == zbMsg_Head->retries) ||
                 (zbMsg_Head->expectedRsp == inMsg))))
        {
            xTimerStop(ZigBeeMessagesTimeout, 0);
            vPortFree(zbMsg_Head);
            zbMsg_Head = zbMsg_Head->next;
            xTaskNotify(zigBeeTaskHandle, ZBTHRD_SENDNEXTMSG, eSetBits);
        }
    }

    taskEXIT_CRITICAL();
/********************************************/

}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void ResendLastMsg (void)
{
    if (NULL != zbMsg_Head)
    {
        if (0 == zbMsg_Head->retries)
        {
            FreeLastSentMsg(0,ZB_INMSG_DONT_CARE);
        }
        else
        {
            zbMsg_Head->msgState = ZBMSG_RETRY;
        }
        SendNextMsg();
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void SendZigBeeCmd(char * str, uint8_t retries, zbIncomingMsg_t expectedRsp)
{
    if ((0 == isInUpdateZbModule()) && (0 == isZigBeeModuleUnusable()))
    {
        SendZigBeeMsg(str, -1, retries, expectedRsp);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
#pragma optimize=none
void SendZigBeeMsg(char * str, uint8_t length, uint8_t retries, zbIncomingMsg_t expectedRsp)
{
    zbSerialMsg_t *newMsg = NULL;

    /* To make sure the zigbee message will be sent, the calling thread will
        blocked here until the memory needed for the message can be allocated */
    while (NULL == newMsg)
    {
        newMsg = pvPortMalloc(sizeof(zbSerialMsg_t));

        if (NULL == newMsg)
        {
            osDelay(1);
        }
    }

    if (NULL != newMsg)
    {
        if (MAX_ZB_MSG_LENGTH >= ((uint8_t)-1!=length)?length:strlen(str))
        {
            if ((uint8_t)-1 != length)
            {
                memcpy(newMsg->str,str, length);
                newMsg->msgLength = length;
            }
            else
            {
                strcpy(newMsg->str,str);
                newMsg->msgLength = strlen(str);
            }
            newMsg->retries = retries;
            newMsg->next = NULL;
            newMsg->msgState = ZBMSG_NEW;
            newMsg->expectedRsp = expectedRsp;

            if (NULL == zbMsg_Head)
            {
                zbMsg_Head = newMsg;
                zbMsg_Tail = zbMsg_Head;
            }
            else
            {
                zbMsg_Tail->next = newMsg;
                zbMsg_Tail = zbMsg_Tail->next;
            }
            xTaskNotify(zigBeeTaskHandle, ZBTHRD_OUTGOING_MSG, eSetBits);
        }
        else
        {
            vPortFree(newMsg);
        }
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2017/07/12
*******************************************************************************/
void RxBootloaderMsg(uint8_t incomingByte)
{
    static uint8_t incomingBuffer[10] = {0,0,0,0,0,0,0,0,0,0};
    static uint8_t incomingByteCnt = 0;
    uint8_t clearbuffer = 0;

    if (0 != incomingByte)
    {
        incomingBuffer[incomingByteCnt++] = incomingByte;
    }

//    if (incomingByteCnt >= ZB_BOOTLOADER_CMD_LENGTH)
    {
        if (0 != strstr((char *)incomingBuffer, (char[])ZB_BOOTLOADER_HANDSHAKE_CONF))
        {
            ZGB_PostEvents(ZBEVENT_UPDATEZB_HANDSHAKE_CONF);
            clearbuffer = 1;
        }
        else if (0 != strstr((char *)incomingBuffer, (char[])ZB_BOOTLOADER_ACK))
        {
            ZGB_PostEvents(ZBEVENT_UPDATEZB_RECORD_ACK);
            clearbuffer = 1;
        }
        else if (0 != strstr((char *)incomingBuffer, (char[])ZB_BOOTLOADER_NACK))
        {
            ZGB_PostEvents(ZBEVENT_UPDATEZB_RECORD_NACK);
            clearbuffer = 1;
        }
        else
        {
        }

        if ((0 != clearbuffer) || (incomingByteCnt > sizeof(incomingBuffer)))
        {
            incomingByteCnt = 0;
            memset (incomingBuffer, 0, sizeof(incomingBuffer));
        }
    }

}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
uint8_t isZigbeeUpAndRunning(void)
{
    return ZigbeeIsUpAndRunning;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
uint8_t isZigbeeUartInError(void)
{
    return ZigBeeUartInError;
}

/** Copyright(C) 2014 Stelpro Design, All Rights Reserved**/
