/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    ZigBee_Notifications.c
* @date    2016/10/31
* @authors Jean-Fran�ois Many
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>

#include "ZigBee_Notifications.h"
#include ".\Application\inc\ZigBee_ClustersEmulation.h"

#include ".\DB\inc\THREAD_DB.h"
#include ".\ZigBee\inc\THREAD_ZigBee.h"
#include "HAL_ZigBeeInterface.h"
#include "StringObjects.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
extern QueueHandle_t  zigBeeQhandle;


/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
DB_NOTIFICATION(ZigBee_NotifyZigBeeAssociationPermit);
DB_NOTIFICATION(ZigBee_NotifyZigBeeDeviceType);
DB_NOTIFICATION(NotifyZBStat_UpdateGroupId);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/10/31
*******************************************************************************/
DB_NOTIFICATION(ZigBee_NotifyZigBeeAssociationPermit)
{
    uint8_t permit;

    DBTHRD_GetData(DBTYPE_ZIGBEE_ASSOCIATION_PERMIT, &permit, INDEX_DONT_CARE);
    ZBTHRD_AddZBStat(permit);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/11/02
*******************************************************************************/
DB_NOTIFICATION(ZigBee_NotifyZigBeeDeviceType)
{
//TODO - enable router mode
    static uint8_t oldType = 0/*DEVICE_TYPE_COORDINATOR*/;
    dbType_ZigBeeJoinInfo_t joinInfo;

    DBTHRD_GetData(DBTYPE_ZIGBEE_JOIN_INFO, &joinInfo, INDEX_DONT_CARE);
    if (joinInfo.DeviceType != oldType)
    {
//        appRestart(false);
    }
    oldType = joinInfo.DeviceType;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/12/05
*******************************************************************************/
DB_NOTIFICATION(NotifyZBStat_UpdateGroupId)
{
    if (DBERR_OK == status)
    {
        UpdateTstatGroupId(instance);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/12/05
*******************************************************************************/
DB_NOTIFICATION(NotifyZBStat_GroupSetpoint)
{
    SendOccupiedHeatGroupSetpointAttribute(instance);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/12/05
*******************************************************************************/
DB_NOTIFICATION(NotifyZBStat_AmbientSetpoint)
{
    SendOccupiedHeatSetpointAttribute(instance);
}

/*******************************************************************************
* @brief  Send Unit Format on ZigBee network when there is a change
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/18
*******************************************************************************/
DB_NOTIFICATION(NotifyZBStat_UnitFormat)
{
    SendTemperatureDisplayModeAttribute();
}

/*******************************************************************************
* @brief  Send Outdoor Temperature on ZigBee network when there is a change
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/18
*******************************************************************************/
DB_NOTIFICATION(NotifyZBStat_OutdoorTemperature)
{
    SendOutdoorTemperatureAttribute(BROADCAST_UPDATE);
}

/*******************************************************************************
* @brief  Send Lock State on ZigBee network when there is a change
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/18
*******************************************************************************/
DB_NOTIFICATION(NotifyZBStat_LockState)
{
    SendSystemKeypadLockoutAttribute();
}

/*******************************************************************************
* @brief  Send Thermostat Name on ZigBee network when there is a change
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/18
*******************************************************************************/
DB_NOTIFICATION(NotifyZBStat_TstatName)
{
    SendLocationDescriptionAttribute(instance);
}

/*******************************************************************************
* @brief  Send Time Format on ZigBee network when there is a change
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/18
*******************************************************************************/
DB_NOTIFICATION(NotifyZBStat_TimeFormat)
{
    SendTimeFormatAttribute();
}

/*******************************************************************************
* @brief  Send Local Time on ZigBee network when there is a change
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/18
*******************************************************************************/
DB_NOTIFICATION(NotifyZBStat_LocalTime)
{
    SendTimeAttribute();
    SendOutdoorTemperatureAttribute(INTERNAL_UPDATE);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/07/14
*******************************************************************************/
DB_NOTIFICATION(NotifyZBStat_TstatAdded)
{
    SendOccupiedHeatSetpointAttribute(instance);
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  Register all ZigBee notifications
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/10/31
*******************************************************************************/
void RegisterZigBeeNotifications(void)
{
    DBTHRD_SubscribeToNotification( DBTYPE_ZIGBEE_ASSOCIATION_PERMIT,
                                   ZigBee_NotifyZigBeeAssociationPermit,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification( DBTYPE_ZIGBEE_JOIN_INFO,
                                   ZigBee_NotifyZigBeeDeviceType,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification( DBTYPE_AMBIENT_SETPOINT,
                                   NotifyZBStat_AmbientSetpoint,
                                   (dbChangeSource_t)(DBCHANGE_ALLCHANGES&~(DBCHANGE_ZIGBEE)),
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification( DBTYPE_FLOOR_SETPOINT,
                                   NotifyZBStat_AmbientSetpoint,
                                   (dbChangeSource_t)(DBCHANGE_ALLCHANGES&~(DBCHANGE_ZIGBEE)),
                                   INDEX_DONT_CARE);
//    DBTHRD_SubscribeToNotification( DBTYPE_GROUP_SETPOINT,
//                                   NotifyZBStat_GroupSetpoint,
//                                   (dbChangeSource_t)(DBCHANGE_ALLCHANGES&~DBCHANGE_ZIGBEE),
//                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification( DBTYPE_TEMPERATURE_FORMAT,
                                   NotifyZBStat_UnitFormat,
                                   (dbChangeSource_t)(DBCHANGE_ALLCHANGES&~DBCHANGE_ZIGBEE),
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification( DBTYPE_OUTDOOR_TEMPERATURE,
                                   NotifyZBStat_OutdoorTemperature,
                                   (dbChangeSource_t)(DBCHANGE_ALLCHANGES&~DBCHANGE_ZIGBEE),
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification( DBTYPE_SYSTEMLOCK,
                                   NotifyZBStat_LockState,
                                   (dbChangeSource_t)(DBCHANGE_ALLCHANGES&~DBCHANGE_ZIGBEE),
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification( DBTYPE_TSTAT_NAME,
                                   NotifyZBStat_TstatName,
                                   (dbChangeSource_t)(DBCHANGE_CLOUD),
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification( DBTYPE_TIME_FORMAT,
                                   NotifyZBStat_TimeFormat,
                                   (dbChangeSource_t)(DBCHANGE_ALLCHANGES&~DBCHANGE_ZIGBEE),
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification( DBTYPE_MINUTE_CHANGE,
                                   NotifyZBStat_LocalTime,
                                   (dbChangeSource_t)(DBCHANGE_ALLCHANGES&~DBCHANGE_ZIGBEE),
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification( DBTYPE_TSTAT_PARENT_GROUPID,
                                   NotifyZBStat_UpdateGroupId,
                                   (dbChangeSource_t)(DBCHANGE_ALLCHANGES&~DBCHANGE_ZIGBEE),
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification( DBTYPE_THERMOSTAT_ADDED,
                                   NotifyZBStat_TstatAdded,
                                   (dbChangeSource_t)(DBCHANGE_ALLCHANGES),
                                   INDEX_DONT_CARE);
}
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
