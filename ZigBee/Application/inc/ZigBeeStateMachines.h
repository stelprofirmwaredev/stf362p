/*******************************************************************************
* @file    ZigBeeStateMachines.h
* @date    2017/02/24
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef _ZigBeeStateMachines_H
#define _ZigBeeStateMachines_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>
#include "cmsis_os.h"


/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define ZIGBEE_TSTAT_TIMEOUT                (30*60*1000)    //30 minutes * 60 seconds/minute * 1000 ms/second
#define ZIGBEE_PING_PERIOD                  (1*1000)        //1 second * 1000 ms/second
#define ZIGBEE_PING_TIMEOUT                 (5*1000)        //5 seconds * 1000 ms/second

#define STACKVERSION_STATE_TIMEOUT          (3*1000)        //3 seconds * 1000 ms/second
#define NETWORKADDRESS_STATE_TIMEOUT        (10*1000)       //10 seconds * 1000 ms/second
#define NETWORKCHANNEL_STATE_TIMEOUT        (10*1000)       //10 seconds * 1000 ms/second
#define PANID_STATE_TIMEOUT                 (3*1000)        //3 seconds * 1000 ms/second
#define MAIN_INIT_STATE_TIMEOUT             (5*1000)

typedef enum
{
    ZBEVENT_STARTINITIALIZATION = 0,

    ZBEVENT_RESETRADIO_TO_FACTORYNEW,
    ZBEVENT_RESETRADIO,
    ZBEVENT_ENTERTESTMODE,
    ZBEVENT_UPDATEZIGBEEMODULE,
    ZBEVENT_RESET_ZIGBEEMODULE,
    ZBEVENT_PONG_RESET_RECEIVED,
    ZBEVENT_RESUME,

    ZBEVENT_INITNETWORK,
    ZBEVENT_MODULE_HAS_REBOOTED,
    ZBEVENT_PONG_TIMEOUT,
    ZBEVENT_COMMISSIONINGSTATUSOK,
    ZBEVENT_NETWORKDESTROYED,
    ZBEVENT_WAITBOOTLOADER_TIMEOUT,
    ZBEVENT_GETFRAMECOUNTER,
    ZBEVENT_FRAMECOUNTER_RECEIVED,
    ZBEVENT_FRAMECOUNTER_TIMEOUT,
    ZBEVENT_MODULEHARDRESET,

    ZBEVENT_REQUESTNETWORKCHANNEL,
    ZBEVENT_NETWORKCHANNELRECEIVED,
    ZBEVENT_NETWORKCHANNELTIMEOUT,

    ZBEVENT_REQUESTPANID,
    ZBEVENT_PANIDRECEIVED,
    ZBEVENT_PANIDTIMEOUT,

    ZBEVENT_REQUESTOWNNWKADDRESS,
    ZBEVENT_NETWORKADDRESSRECEIVED,
    ZBEVENT_NETWORKADDRESSTIMEOUT,

    ZBEVENT_REQUESTSTACKVERSION,
    ZBEVENT_STACKVERSIONRECEIVED,
    ZBEVENT_STACKVERSIONTIMEOUT,


    ZBEVENT_ADDTSTAT,
    ZBEVENT_UPDATEGROUP,
    ZBEVENT_STARTCONFIGUREREPORTING,
    ZBEVENT_CONFIGUREREPORTINGRESPONSE,
    ZBEVENT_CONFIGUREREPORTINGTERROR,
    ZBEVENT_STARTCLUSTERBINDING,
    ZBEVENT_BINDRESPONSE,
    ZBEVENT_BINDREQUESTERROR,
    ZBEVENT_STARTREADATTRIBUTE,
    ZBEVENT_READATTRIBUTERESPONSE,
    ZBEVENT_DEVICEANNCERECEIVED,
    ZBEVENT_VERIFYKEY,
    ZBEVENT_VERIFYKEY_TIMEOUT,
    ZBEVENT_REQUESTSIMPLEDESCRIPTOR,
    ZBEVENT_REQUESTMATCHDESCRIPTOR,
    ZBEVENT_MATCHDESCRESP,
    ZBEVENT_MATCHDESCRESPERROR,
    ZBEVENT_SIMPLEDESCRESP,
    ZBEVENT_STOPADDTSTAT,
    ZBEVENT_RECONFIGURE,
    ZBEVENT_RECONFIGURETSTAT,
    ZBEVENT_STARTCOORDINATORMODE,
    ZBEVENT_JOINPERMITTIMEOUT,
    ZBEVENT_ADDINGTSTATHASENDED,

    ZBEVENT_DEVICEDESCRIPTOR_RECEIVED,
    ZBEVENT_UPDATEGROUP_COMPLETED,
    ZBEVENT_BINDINGCLUSTER_COMPLETED,
    ZBEVENT_CONFIGUREREPORTING_COMPLETED,
    ZBEVENT_READATTRIBUTES_COMPLETED,
    ZBEVENT_UPDATEATTRIBUTES_COMPLETED,

    ZBEVENT_UPDATEGROUP_STATE_TIMEOUT,
    ZBEVENT_ADDSTAT_STATE_TIMEOUT,
    ZBEVENT_ADDSTAT_SM_TIMEOUT,
    ZBEVENT_DEVICEDESCRIPTOR_SM_TIMEOUT,
    ZBEVENT_DEVICEDESCRIPTOR_STATE_TIMEOUT,
    ZBEVENT_BINDINGCLUSTER_STATE_TIMEOUT,
    ZBEVENT_BINDINGCLUSTER_SM_TIMEOUT,
    ZBEVENT_CONFIGUREREPORTING_STATE_TIMEOUT,
    ZBEVENT_CONFIGUREREPORTING_SM_TIMEOUT,
    ZBEVENT_READATTRIBUTES_STATE_TIMEOUT,
    ZBEVENT_READATTRIBUTES_SM_TIMEOUT,
    ZBEVENT_UPDATEATTRIBUTES_STATE_TIMEOUT,
    ZBEVENT_UPDATEATTRIBUTES_SM_TIMEOUT,

    ZBEVENT_DESTROYCURRENTNETWORK,
    ZBEVENT_SETPRIMARYCHANNELMASK,
    ZBEVENT_SETPRIMARYCHANNELMASK_IND,
    ZBEVENT_CHANNELMASK_STATE_TIMEOUT,
    ZBEVENT_SETSECONDARYCHANNELMASK,
    ZBEVENT_SETSECONDARYCHANNELMASK_IND,
    ZBEVENT_INIT_IN_FACTORY_MODE,
    ZBEVENT_FACTORYTESTDEVICE_JOINED,

    ZBEVENT_REMOVESTAT,
    ZBEVENT_REMOVESTAT_RESP,
    ZBEVENT_REMOVESTAT_TIMEOUT,
    ZBEVENT_SEND_RESET_TO_FN,
    ZBEVENT_WAIT_ZIGBEE_MODULE,
    ZBEVENT_RESET_TO_FN_COMPLETED,

    ZBEVENT_UPDATEZB_HANDSHAKE_REQUEST,
    ZBEVENT_UPDATEZB_HANDSHAKE_CONF,
    ZBEVENT_UPDATEZB_HANDSHAKE_TIMEOUT,
    ZBEVENT_UPDATEZB_READ_NEXT_RECORD_FROM_FLASH,
    ZBEVENT_UPDATEZB_WRITE_RECORD_TO_MODULE,
    ZBEVENT_UPDATEZB_RECORD_ACK,
    ZBEVENT_UPDATEZB_RECORD_NACK,
    ZBEVENT_UPDATEZB_RECORD_TIMEOUT,
    ZBEVENT_UPDATEZB_UPDATE_COMPLETED,
    ZBEVENT_UPDATEZB_UPDATE_FAILED,
    ZBEVENT_FINALIZE_TIMEOUT,


    ZB_NOEVENT = -1,
} zigBeeSMEventsEnum_t;

typedef enum
{
    ZBSM_INITIALSTATE = 0,

    ZBSM_REINITZIGBEEMODULE,

    ZBSM_MAIN_JOINING_NETWORK,
    ZBSM_MAIN_RUNNING_COORDINATOR,
    ZBSM_MAIN_ADDINGTSTAT,
    ZBSM_MAIN_RECONFIGURETSTAT,
    ZBSM_MAIN_TESTMODE,
    ZBSM_MAIN_REMOVETSTAT,
    ZBSM_MAIN_UPDATEZIGBEE_MODULE,
    ZBSM_MAIN_MODULE_IS_DEAD,

    ZBSM_INIT_WAIT_MODULE_REBOOT,
    ZBSM_INIT_STACKVERSION,
    ZBSM_INIT_NETWORKCHANNEL,
    ZBSM_INIT_OWNNETWORKADDRESS,
    ZBSM_INIT_FRAMECOUNTER,
    ZBSM_INIT_PANID,

    ZBSM_ADDSTAT_GETDEVICEDESCRIPTOR,
    ZBSM_ADDSTAT_UPDATEGROUP,
    ZBSM_ADDSTAT_BINDINGCLUSTERS,
    ZBSM_ADDSTAT_CONFIGUREREPORTING,
    ZBSM_ADDSTAT_READATTRIBUTES,
    ZBSM_ADDSTAT_UPDATEATTRIBUTES,

    ZBSM_DEVICEDESCRIPTOR_WAITSIMPLEDESCR,
    ZBSM_DEVICEDESCRIPTOR_WAITMATCHDESCR,

    ZBSM_BIND_THERMOSTAT_CLUSTER,
    ZBSM_BIND_THERMOSTATUI_CLUSTER,
    ZBSM_BIND_HUMIDITY_CLUSTER,

    ZBSM_CONFREPORT_LOCALTEMP,
    ZBSM_CONFREPORT_HEATSETPOINT,
    ZBSM_CONFREPORT_HEATDEMAND,
    ZBSM_CONFREPORT_POWERCONSUMPTION,
    ZBSM_CONFREPORT_ALERT,
    ZBSM_CONFREPORT_FLOORMODE,
    ZBSM_CONFREPORT_RELAYCYCLECOUNT,
    ZBSM_CONFREPORT_TEMPERATUREDISPLAYMODE,
    ZBSM_CONFREPORT_KEYPADLOCKOUT,
    ZBSM_CONFREPORT_LOCALHUMIDITY,

    ZBSM_READATTR_FLOORMODE,
    ZBSM_READATTR_LOCALTEMP,
    ZBSM_READATTR_HEATSETPOINT,
    ZBSM_READATTR_HEATDEMAND,
    ZBSM_READATTR_POWERCONSUMPTION,
    ZBSM_READATTR_ALERT,
    ZBSM_READATTR_KEYPADLOCKOUT,
    ZBSM_READATTR_LOCALHUMIDITY,
    ZBSM_READATTR_APPLICATIONVERSION,
    ZBSM_READATTR_STACKVERSION,
    ZBSM_READATTR_DATECODE,
    ZBSM_READATTR_MODELID,
    ZBSM_READATTR_LOCATIONDESC,

    ZBSM_TESTMODE_DESTROYCURRENTNETWORK,
    ZBSM_TESTMODE_SETPRIMARYCHANNELMASK,
    ZBSM_TESTMODE_SETSECONDARYCHANNELMASK,
    ZBSM_TESTMODE_JOINNETWORK,
    ZBSM_TESTMODE_ADDBENCHTESTDEVICE,

    ZBSM_DESTROYNWK_REMOVETSTAT,
    ZBSM_DESTROYNWK_WAIT_LEAVERSP,
    ZBSM_DESTROYNWK_FORCE_ERASE,
    ZBSM_DESTROYNWK_WAIT_ZIGBEE_MODULE,

    ZBSM_UPDATEZB_RESET_MODULE,
    ZBSM_UPDATEZB_ENTER_BOOTLOADER,
    ZBSM_UPDATEZB_READ_RECORD_FROM_FLASH,
    ZBSM_UPDATEZB_WRITE_RECORD_TO_MODULE,
    ZBSM_UPDATEZB_WAIT_ACK,
    ZBSM_WAIT_FINALIZE,

    ZBSM_EXIT_STATEMACHINE = -2,
    ZBSM_NO_NEXTSTATE = -1,
} zigBeeSMStatesEnum_t;

typedef enum
{
    SM_SUCCESS = 0,
    SM_SETNEXTSTATE,
    SM_ENDSTATEMACHINE,             //regular finialization of the SM
    SM_TERMINATESTATEMACHINE,       //force termination (e.g. due to a timeout)
    SM_RESTARTSTATEMACHINE,
    SM_RESTARTSTATETIMEOUT,


    SM_ERRSAMESTATE = -4,
    SM_ERRINVALIDSTATE = -3,
    SM_ERRINVALIDEVENT = -2,
    SM_ERRCANNOTEXECUTE = -1,
} stateMachineErrorCode_t;

typedef uint8_t statID_t;
typedef struct
{
    zigBeeSMEventsEnum_t eventId;
    statID_t statId;
    uint16_t zbShortAddress;
    uint8_t channelNumber;
    uint8_t resume;
} zigBeeSMEvents_t;

#define ZBSM_NoEvent()             ((zigBeeSMEvents_t){.eventId = ZB_NOEVENT, \
                                                        .statId = -1,          \
                                                        .zbShortAddress = -1,  \
                                                        .channelNumber = -1,   \
                                                        .resume = 0})
#define ZBSM_SetEvent(event)        ((zigBeeSMEvents_t){.eventId = event, \
                                                        .statId = -1,          \
                                                        .zbShortAddress = -1,  \
                                                        .channelNumber = -1,   \
                                                        .resume = 0})
#define ZBSM_SetEventWIdx(event, idx) ((zigBeeSMEvents_t){.eventId = event, \
                                                        .statId = idx,          \
                                                        .zbShortAddress = -1,  \
                                                        .channelNumber = -1,   \
                                                        .resume = 0})
#define ZBSM_SetEventWChnl(event, chnl) ((zigBeeSMEvents_t){.eventId = event, \
                                                        .statId = -1,          \
                                                        .zbShortAddress = -1,  \
                                                        .channelNumber = chnl,   \
                                                        .resume = 0})


typedef enum
{
    CONFREPORT_ATTR_LOCALTEMP = 0,
    CONFREPORT_ATTR_HEATSETPOINT,
    CONFREPORT_ATTR_HEATDEMAND,
    CONFREPORT_ATTR_POWERCONSUMPTION,
    CONFREPORT_ATTR_ALERT,
    CONFREPORT_ATTR_FLOORMODE,
    CONFREPORT_ATTR_RELAYCYCLECOUNT,
    CONFREPORT_ATTR_TEMPERATUREDISPLAYMODE,
    CONFREPORT_ATTR_KEYPADLOCKOUT,
    CONFREPORT_ATTR_LOCALHUMIDITY,
} confReportTableAttrib_t;



#define CLUSTERTOBIND_THERMOSTAT            (0x01<<0)
#define CLUSTERTOBIND_THERMOSTATUI          (0x01<<1)
#define CLUSTERTOBIND_HUMIDITY              (0x01<<2)


#define SM_NO_TIMEOUT                       ((uint16_t)-1)

#define SM_REGULAR_END                      0
#define SM_FORCE_END                        1
/*******************************************************************************
* Public structures definitions
*******************************************************************************/

typedef struct
{
    zigBeeSMEvents_t nextEvent;
    zigBeeSMStatesEnum_t nextState;
} smNextOperation_t;

typedef stateMachineErrorCode_t (*eventCore_t)(zigBeeSMEvents_t, smNextOperation_t *);
#define ZIGBEE_EVENTCORE_FUNC(name)         static stateMachineErrorCode_t name(zigBeeSMEvents_t event, smNextOperation_t *nextOp)


typedef struct
{
    const uint32_t smTimeoutLength;               //expressed in ms;-1 == infinite
    TimerHandle_t smTimeoutHandler;
    void (*smTimeout_cb)(TimerHandle_t xTimer);
} smTimeouts_t;


typedef struct
{
    zigBeeSMEventsEnum_t eventId;
    eventCore_t eventCore;
} supportedStateEvents_t;
//
///*******************************************************************************
//* @brief
//* @inputs
//*   - zigBeeSMEvents_t event : definition of the incoming event
//*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
//*                   the next event (for restart state machine) or next state (for
//*                   SM_SETNEXTSTATE)
//* @retval
//*   stateMachineErrorCode_t
//* @author J-F. Simard
//* @date   2017/06/12
//*******************************************************************************/
//ZIGBEE_EVENTCORE_FUNC(eventCoreFunc_template)
//{
//    (void)event;
//    (void)nextOp;
//
//
//    return SM_SUCCESS;
//}



typedef struct
{
    const zigBeeSMStatesEnum_t stateId;
    const supportedStateEvents_t * supportedEvents;
    const uint8_t numberOfSupportedEvents;
    smTimeouts_t stateTimeout;
} supportedStateMachineStates_t;

typedef struct _stateMachineDefinition_t
{
    void (*SM_Start)(void);
    void (*SM_End)(uint8_t forceEnd);
    supportedStateMachineStates_t * supportedStates;
    const uint8_t numberOfSupportedStates;
    uint8_t currentStateIndex;
    zigBeeSMStatesEnum_t currentState;
    const zigBeeSMStatesEnum_t initialState;
    struct _stateMachineDefinition_t *parentStateMachine;
    struct _stateMachineDefinition_t *activeSubStateMachine;
    smTimeouts_t stateMachineTimeout;
    char sm_name[30];
} stateMachineDefinition_t;


/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void InitZigBeeStateMachines (void);
void ZGB_PostEvents (zigBeeSMEventsEnum_t eventId);
void ZGB_PostEventsWithStatId (zigBeeSMEventsEnum_t eventId, statID_t statId);
void ZGB_PostEventsWithChannelNumber (zigBeeSMEventsEnum_t eventId, uint8_t channel);
void ZGB_tstatReconfigure(uint8_t statIndex);
void StartZigBeeAssociationTest(uint8_t channel);
uint8_t GetAssociationTestStatus(void);
uint8_t isZbInAddingState(void);
void ZGB_ReinitAndResume (void);
uint8_t IsStatInReconfiguration(uint8_t statIndex);


void StartStateMachine (stateMachineDefinition_t * self, zigBeeSMEvents_t startingEvent);
void EndStateMachine (stateMachineDefinition_t * self, uint8_t forceEnd);
void StartStateMachineTimeout (stateMachineDefinition_t * self);
void StopStateMachineTimeout (stateMachineDefinition_t * self);
void ExecuteEvent (void);
void SuspendSM (stateMachineDefinition_t * self);
void ResumeSM (void);

void SetTestModeChannel(uint8_t channel);
uint8_t isInUpdateZbModule (void);


#endif /* _ZigBeeStateMachines_H */
/** Copyright(C) 2017 Stelpro Design, All Rights Reserved**/
