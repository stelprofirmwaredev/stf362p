/*******************************************************************************
* @file    ZigBee_ClustesrEmulation.h
* @date    2017/03/02
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef ZigBee_ClustesrEmulation_H
#define ZigBee_ClustesrEmulation_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>
#include "common_types.h"
#include ".\DB\inc\THREAD_DB.h"

/*******************************************************************************
* Public constants definitions
*******************************************************************************/
typedef enum
{
  //Null
  ZCL_NO_DATA_TYPE_ID                       = 0x00,

  //General data
  ZCL_8BIT_DATA_TYPE_ID                     = 0x08,
  ZCL_16BIT_DATA_TYPE_ID                    = 0x09,
  ZCL_24BIT_DATA_TYPE_ID                    = 0x0a,
  ZCL_32BIT_DATA_TYPE_ID                    = 0x0b,
  ZCL_40BIT_DATA_TYPE_ID                    = 0x0c,
  ZCL_48BIT_DATA_TYPE_ID                    = 0x0d,
  ZCL_56BIT_DATA_TYPE_ID                    = 0x0e,
  ZCL_64BIT_DATA_TYPE_ID                    = 0x0f,

  //Logical
  ZCL_BOOLEAN_DATA_TYPE_ID                  = 0x10,

  //Bitmap
  ZCL_8BIT_BITMAP_DATA_TYPE_ID              = 0x18,
  ZCL_16BIT_BITMAP_DATA_TYPE_ID             = 0x19,
  ZCL_24BIT_BITMAP_DATA_TYPE_ID             = 0x1a,
  ZCL_32BIT_BITMAP_DATA_TYPE_ID             = 0x1b,
  ZCL_40BIT_BITMAP_DATA_TYPE_ID             = 0x1c,
  ZCL_48BIT_BITMAP_DATA_TYPE_ID             = 0x1d,
  ZCL_56BIT_BITMAP_DATA_TYPE_ID             = 0x1e,
  ZCL_64BIT_BITMAP_DATA_TYPE_ID             = 0x1f,

  //Unsigned integer
  ZCL_U8BIT_DATA_TYPE_ID                    = 0x20,
  ZCL_U16BIT_DATA_TYPE_ID                   = 0x21,
  ZCL_U24BIT_DATA_TYPE_ID                   = 0x22,
  ZCL_U32BIT_DATA_TYPE_ID                   = 0x23,
  ZCL_U40BIT_DATA_TYPE_ID                   = 0x24,
  ZCL_U48BIT_DATA_TYPE_ID                   = 0x25,
  ZCL_U56BIT_DATA_TYPE_ID                   = 0x26,
  ZCL_U64BIT_DATA_TYPE_ID                   = 0x27,

  //Signed integer
  ZCL_S8BIT_DATA_TYPE_ID                    = 0x28,
  ZCL_S16BIT_DATA_TYPE_ID                   = 0x29,
  ZCL_S24BIT_DATA_TYPE_ID                   = 0x2a,
  ZCL_S32BIT_DATA_TYPE_ID                   = 0x2b,
  ZCL_S40BIT_DATA_TYPE_ID                   = 0x2c,
  ZCL_S48BIT_DATA_TYPE_ID                   = 0x2d,
  ZCL_S56BIT_DATA_TYPE_ID                   = 0x2e,
  ZCL_S64BIT_DATA_TYPE_ID                   = 0x2f,

  //Enumeration
  ZCL_8BIT_ENUM_DATA_TYPE_ID                = 0x30,
  ZCL_16BIT_ENUM_DATA_TYPE_ID               = 0x31,

  //Floating point
  ZCL_FSEMI_PRECISION_DATA_TYPE_ID          = 0x38,
  ZCL_FSINGLE_PRECISION_DATA_TYPE_ID        = 0x39,
  ZCL_FDOUBLE_PRECISION_DATA_TYPE_ID        = 0x3a,

  //String
  ZCL_OCTET_STRING_DATA_TYPE_ID             = 0x41,
  ZCL_CHARACTER_STRING_DATA_TYPE_ID         = 0x42,
  ZCL_LONG_OCTET_STRING_DATA_TYPE_ID        = 0x43,
  ZCL_LONG_CHARACTER_STRING_DATA_TYPE_ID    = 0x44,

  //Ordered sequence
  ZCL_ARRAY_DATA_TYPE_ID                    = 0x48,
  ZCL_STRUCTURE_DATA_TYPE_ID                = 0x4c,

  //Collection
  ZCL_SET_DATA_TYPE_ID                      = 0x50,
  ZCL_BAG_DATA_TYPE_ID                      = 0x51,

  //Time
  ZCL_TIME_OF_DAY_DATA_TYPE_ID              = 0xe0,
  ZCL_DATE_DATA_TYPE_ID                     = 0xe1,
  ZCL_UTC_TIME_DATA_TYPE_ID                 = 0xe2,

  //Identifier
  ZCL_CLUSTER_ID_DATA_TYPE_ID               = 0xe8,
  ZCL_ATTRIBUTE_ID_DATA_TYPE_ID             = 0xe9,
  ZCL_BACNET_OID_DATA_TYPE_ID               = 0xea,

  //Misscellaneous
  ZCL_IEEE_ADDRESS_DATA_TYPE_ID             = 0xf0,
  ZCL_128BIT_SECURITY_KEY_DATA_TYPE_ID      = 0xf1,
} ZCL_AttributeType_t;

typedef enum
{
    ZBCLUSTER_THERMOSTAT                = 0x0201,
    ZBCLUSTER_THERMOSTAT_UI_CONF        = 0x0204,
    ZBCLUSTER_HUMIDITYMEAS              = 0x0405,
    ZBCLUSTER_BASIC                     = 0x0000,
    ZBCLUSTER_GROUPS                    = 0x0004,
    ZBCLUSTER_IDENTIFY                  = 0x0003,
    ZBCLUSTER_TIME                      = 0x000a,
} zigBeeClusterId_t;


typedef enum
{
    ZBATTRIBUTE_LOCALTEMPERATURE        = 0x0000,
    ZBATTRIBUTE_OCCHEATSETPOINT         = 0x0012,
    ZBATTRIBUTE_PIHEATDEMAND            = 0x0008,
    ZBATTRIBUTE_POWERCONSUMPTION        = 0x4002,
    ZBATTRIBUTE_ALERT                   = 0x4003,
    ZBATTRIBUTE_OPEN_WINDOW             = 0x4004,
    ZBATTRIBUTE_PIR                     = 0x4005,
    ZBATTRIBUTE_FLOOR_MODE              = 0x4006,
    ZBATTRIBUTE_RELAY_CYCLE_COUNT       = 0x4007,
    ZBATTRIBUTE_KEYPADLOCKOUT           = 0x0001,
    ZBATTRIBUTE_LOCALHUMIDITY           = 0x0000,
    ZBATTRIBUTE_MODELID                 = 0x0005,
    ZBATTRIBUTE_LOCATIONDESC            = 0x0010,
    ZBATTRIBUTE_APPLICATIONVERSION      = 0x0001,
    ZBATTRIBUTE_STACKVERSION            = 0x0002,
    ZBATTRIBUTE_DATECODE                = 0x0006,
} zigBeeAttrId_t;

#define BROADCAST_UPDATE    0
#define INTERNAL_UPDATE     1

/*******************************************************************************
* Public structures definitions
*******************************************************************************/
typedef struct
{
    zigBeeDescriptor_t zbDescriptor;
    uint8_t deviceIsTstat;
    uint8_t clusterToBind;
} zbDeviceToAdd_t;


/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void ZigBeeIncomingAttributeMngmt (uint16_t shortAddress, uint16_t clusterId, uint16_t dataId, void * data);
void SendOccupiedHeatSetpointAttribute (uint8_t thermsotatIndex);
void SendTemperatureDisplayModeAttribute(void);
void SendOutdoorTemperatureAttribute(uint8_t update);
void SendKeypadLockoutAttribute(uint8_t thermsotatIndex);
void SendLocationDescriptionAttribute(uint8_t thermsotatIndex);
void SendTimeFormatAttribute(void);
void SendTimeAttribute(void);
uint8_t tstatNetworkLeaveReq (uint8_t tstatId);
void SendOccupiedHeatGroupSetpointAttribute (uint8_t groupIdx);
void UpdateTstatGroupId (uint8_t tStatIdx);
void SendSystemKeypadLockoutAttribute(void);
uint8_t GetStatIdToRemove(void);
void SendIeeeAddrReq(uint16_t shortAddress);

#endif /* ZigBee_ClustesrEmulation_H */
/** Copyright(C) 2017 Stelpro Design, All Rights Reserved**/
