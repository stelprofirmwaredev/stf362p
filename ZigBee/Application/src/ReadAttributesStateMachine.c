/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    ReadAttributesStateMachine.c
* @date    2017/06/19
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include ".\Application\inc\ZigBeeStateMachines.h"
#include ".\HAL\BitCloud\inc\HAL_OutgoingBitCloudMsg.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define READATTRIBUTE_STATEMACHINE_TIMEOUT     5000        //5sec
#define READATTRIBUTE_STATE_TIMEOUT            1000        //1sec

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static void stateTimeout(TimerHandle_t xTimer);
static void smTimeout(TimerHandle_t xTimer);

void end(uint8_t forceEnd);

ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_FloorMode);
ZIGBEE_EVENTCORE_FUNC(floorMode_ReadResponse);
ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_LocalTemperature);
ZIGBEE_EVENTCORE_FUNC(localTemperature_ReadResponse);
ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_HeatSetpoint);
ZIGBEE_EVENTCORE_FUNC(heatSetpoint_ReadResponse);
ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_HeatDemand);
ZIGBEE_EVENTCORE_FUNC(heatDemand_ReadResponse);
ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_PowerConsumption);
ZIGBEE_EVENTCORE_FUNC(powerConsumption_ReadResponse);
ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_Alert);
ZIGBEE_EVENTCORE_FUNC(alert_ReadResponse);
ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_KeypadLockout);
ZIGBEE_EVENTCORE_FUNC(keypadLockout_ReadResponse);
ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_LocalHumidity);
ZIGBEE_EVENTCORE_FUNC(localHumidity_ReadResponse);
ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_ModelId);
ZIGBEE_EVENTCORE_FUNC(modelId_ReadResponse);
ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_LocationDescription);
ZIGBEE_EVENTCORE_FUNC(locationDescription_ReadResponse);
ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_ApplicationVersion);
ZIGBEE_EVENTCORE_FUNC(applicationVersion_ReadResponse);
ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_StackVersion);
ZIGBEE_EVENTCORE_FUNC(stackVersion_ReadResponse);
ZIGBEE_EVENTCORE_FUNC(dateCode_ReadResponse);
ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_DateCode);

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
supportedStateMachineStates_t readAttributesSM_SupportedStates [] =
{
    /*****************************************************************/
    /*  read attribute state Machine : floor mode state       */
    /*****************************************************************/
    {
        .stateId = ZBSM_READATTR_FLOORMODE,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTREADATTRIBUTE, .eventCore = readRemoteAttribute_FloorMode},
            {.eventId = ZBEVENT_READATTRIBUTES_STATE_TIMEOUT, .eventCore = readRemoteAttribute_FloorMode},
            {.eventId = ZBEVENT_READATTRIBUTERESPONSE, .eventCore = floorMode_ReadResponse},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = READATTRIBUTE_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  read attribute state Machine : local temperature state       */
    /*****************************************************************/
    {
        .stateId = ZBSM_READATTR_LOCALTEMP,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTREADATTRIBUTE, .eventCore = readRemoteAttribute_LocalTemperature},
            {.eventId = ZBEVENT_READATTRIBUTES_STATE_TIMEOUT, .eventCore = readRemoteAttribute_LocalTemperature},
            {.eventId = ZBEVENT_READATTRIBUTERESPONSE, .eventCore = localTemperature_ReadResponse},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = READATTRIBUTE_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  read attribute state Machine : heat setpoint state          */
    /*****************************************************************/
    {
        .stateId = ZBSM_READATTR_HEATSETPOINT,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTREADATTRIBUTE, .eventCore = readRemoteAttribute_HeatSetpoint},
            {.eventId = ZBEVENT_READATTRIBUTES_STATE_TIMEOUT, .eventCore = readRemoteAttribute_HeatSetpoint},
            {.eventId = ZBEVENT_READATTRIBUTERESPONSE, .eventCore = heatSetpoint_ReadResponse},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = READATTRIBUTE_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  read attribute state Machine : heat demand state          */
    /*****************************************************************/
    {
        .stateId = ZBSM_READATTR_HEATDEMAND,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTREADATTRIBUTE, .eventCore = readRemoteAttribute_HeatDemand},
            {.eventId = ZBEVENT_READATTRIBUTES_STATE_TIMEOUT, .eventCore = readRemoteAttribute_HeatDemand},
            {.eventId = ZBEVENT_READATTRIBUTERESPONSE, .eventCore = heatDemand_ReadResponse},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = READATTRIBUTE_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  read attribute state Machine : power consumption state       */
    /*****************************************************************/
    {
        .stateId = ZBSM_READATTR_POWERCONSUMPTION,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTREADATTRIBUTE, .eventCore = readRemoteAttribute_PowerConsumption},
            {.eventId = ZBEVENT_READATTRIBUTES_STATE_TIMEOUT, .eventCore = readRemoteAttribute_PowerConsumption},
            {.eventId = ZBEVENT_READATTRIBUTERESPONSE, .eventCore = powerConsumption_ReadResponse},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = READATTRIBUTE_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  read attribute state Machine : alert state          */
    /*****************************************************************/
    {
        .stateId = ZBSM_READATTR_ALERT,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTREADATTRIBUTE, .eventCore = readRemoteAttribute_Alert},
            {.eventId = ZBEVENT_READATTRIBUTES_STATE_TIMEOUT, .eventCore = readRemoteAttribute_Alert},
            {.eventId = ZBEVENT_READATTRIBUTERESPONSE, .eventCore = alert_ReadResponse},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = READATTRIBUTE_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  read attribute state Machine : keypad lockout state          */
    /*****************************************************************/
    {
        .stateId = ZBSM_READATTR_KEYPADLOCKOUT,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTREADATTRIBUTE, .eventCore = readRemoteAttribute_KeypadLockout},
            {.eventId = ZBEVENT_READATTRIBUTES_STATE_TIMEOUT, .eventCore = readRemoteAttribute_KeypadLockout},
            {.eventId = ZBEVENT_READATTRIBUTERESPONSE, .eventCore = keypadLockout_ReadResponse},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = READATTRIBUTE_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  read attribute state Machine : local humidity state          */
    /*****************************************************************/
    {
        .stateId = ZBSM_READATTR_LOCALHUMIDITY,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTREADATTRIBUTE, .eventCore = readRemoteAttribute_LocalHumidity},
            {.eventId = ZBEVENT_READATTRIBUTES_STATE_TIMEOUT, .eventCore = readRemoteAttribute_LocalHumidity},
            {.eventId = ZBEVENT_READATTRIBUTERESPONSE, .eventCore = localHumidity_ReadResponse},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = READATTRIBUTE_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

        /*****************************************************************/
    /*  read attribute state Machine : application version state     */
    /*****************************************************************/
    {
        .stateId = ZBSM_READATTR_APPLICATIONVERSION,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTREADATTRIBUTE, .eventCore = readRemoteAttribute_ApplicationVersion},
            {.eventId = ZBEVENT_READATTRIBUTES_STATE_TIMEOUT, .eventCore = readRemoteAttribute_ApplicationVersion},
            {.eventId = ZBEVENT_READATTRIBUTERESPONSE, .eventCore = applicationVersion_ReadResponse},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = READATTRIBUTE_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  read attribute state Machine : stack version state          */
    /*****************************************************************/
    {
        .stateId = ZBSM_READATTR_STACKVERSION,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTREADATTRIBUTE, .eventCore = readRemoteAttribute_StackVersion},
            {.eventId = ZBEVENT_READATTRIBUTES_STATE_TIMEOUT, .eventCore = readRemoteAttribute_StackVersion},
            {.eventId = ZBEVENT_READATTRIBUTERESPONSE, .eventCore = stackVersion_ReadResponse},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = READATTRIBUTE_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  read attribute state Machine : date code state          */
    /*****************************************************************/
    {
        .stateId = ZBSM_READATTR_DATECODE,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTREADATTRIBUTE, .eventCore = readRemoteAttribute_DateCode},
            {.eventId = ZBEVENT_READATTRIBUTES_STATE_TIMEOUT, .eventCore = readRemoteAttribute_DateCode},
            {.eventId = ZBEVENT_READATTRIBUTERESPONSE, .eventCore = dateCode_ReadResponse},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = READATTRIBUTE_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  read attribute state Machine : model id state          */
    /*****************************************************************/
    {
        .stateId = ZBSM_READATTR_MODELID,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTREADATTRIBUTE, .eventCore = readRemoteAttribute_ModelId},
            {.eventId = ZBEVENT_READATTRIBUTES_STATE_TIMEOUT, .eventCore = readRemoteAttribute_ModelId},
            {.eventId = ZBEVENT_READATTRIBUTERESPONSE, .eventCore = modelId_ReadResponse},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = READATTRIBUTE_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  read attribute state Machine : location description state    */
    /*****************************************************************/
    {
        .stateId = ZBSM_READATTR_LOCATIONDESC,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTREADATTRIBUTE, .eventCore = readRemoteAttribute_LocationDescription},
            {.eventId = ZBEVENT_READATTRIBUTES_STATE_TIMEOUT, .eventCore = readRemoteAttribute_LocationDescription},
            {.eventId = ZBEVENT_READATTRIBUTERESPONSE, .eventCore = locationDescription_ReadResponse},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = READATTRIBUTE_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },
};

stateMachineDefinition_t ReadAddtributesStateMachine =
{
    .SM_Start = NULL,
    .SM_End = end,
    .supportedStates = readAttributesSM_SupportedStates,
    .numberOfSupportedStates = sizeof(readAttributesSM_SupportedStates)/sizeof(supportedStateMachineStates_t),
    .initialState = ZBSM_READATTR_FLOORMODE,
    .stateMachineTimeout.smTimeoutLength = READATTRIBUTE_STATEMACHINE_TIMEOUT,
    .stateMachineTimeout.smTimeout_cb = smTimeout,
    .parentStateMachine = NULL,
    .activeSubStateMachine = NULL,
    .sm_name = "Read attr. "
};


extern zbDeviceToAdd_t currentZigBeeDevice;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void end(uint8_t forceEnd)
{
    if (SM_REGULAR_END == forceEnd)
    {
        ZGB_PostEvents(ZBEVENT_READATTRIBUTES_COMPLETED);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void stateTimeout(TimerHandle_t xTimer)
{
    ZGB_PostEvents(ZBEVENT_READATTRIBUTES_STATE_TIMEOUT);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void smTimeout(TimerHandle_t xTimer)
{
    ZGB_PostEvents(ZBEVENT_READATTRIBUTES_SM_TIMEOUT);
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_FloorMode)
{
    ReadRemoteAttributes(currentZigBeeDevice, ZBCLUSTER_THERMOSTAT, ZBATTRIBUTE_FLOOR_MODE);

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(floorMode_ReadResponse)
{
    nextOp->nextState = ZBSM_READATTR_LOCALTEMP;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_STARTREADATTRIBUTE);

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_LocalTemperature)
{
    ReadRemoteAttributes(currentZigBeeDevice, ZBCLUSTER_THERMOSTAT, ZBATTRIBUTE_LOCALTEMPERATURE);

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(localTemperature_ReadResponse)
{
    nextOp->nextState = ZBSM_READATTR_HEATSETPOINT;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_STARTREADATTRIBUTE);

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_HeatSetpoint)
{
    ReadRemoteAttributes(currentZigBeeDevice, ZBCLUSTER_THERMOSTAT, ZBATTRIBUTE_OCCHEATSETPOINT);

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(heatSetpoint_ReadResponse)
{
    nextOp->nextState = ZBSM_READATTR_HEATDEMAND;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_STARTREADATTRIBUTE);

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_HeatDemand)
{
    ReadRemoteAttributes(currentZigBeeDevice, ZBCLUSTER_THERMOSTAT, ZBATTRIBUTE_PIHEATDEMAND);

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(heatDemand_ReadResponse)
{
    nextOp->nextState = ZBSM_READATTR_POWERCONSUMPTION;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_STARTREADATTRIBUTE);

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_PowerConsumption)
{
    ReadRemoteAttributes(currentZigBeeDevice, ZBCLUSTER_THERMOSTAT, ZBATTRIBUTE_POWERCONSUMPTION);

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(powerConsumption_ReadResponse)
{
    nextOp->nextState = ZBSM_READATTR_ALERT;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_STARTREADATTRIBUTE);

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_Alert)
{
    ReadRemoteAttributes(currentZigBeeDevice, ZBCLUSTER_THERMOSTAT, ZBATTRIBUTE_ALERT);

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(alert_ReadResponse)
{
    nextOp->nextState = ZBSM_READATTR_KEYPADLOCKOUT;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_STARTREADATTRIBUTE);

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_KeypadLockout)
{
    ReadRemoteAttributes(currentZigBeeDevice, ZBCLUSTER_THERMOSTAT_UI_CONF, ZBATTRIBUTE_KEYPADLOCKOUT);

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(keypadLockout_ReadResponse)
{
    nextOp->nextState = ZBSM_READATTR_LOCALHUMIDITY;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_STARTREADATTRIBUTE);

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_LocalHumidity)
{
    ReadRemoteAttributes(currentZigBeeDevice, ZBCLUSTER_HUMIDITYMEAS, ZBATTRIBUTE_LOCALHUMIDITY);

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(localHumidity_ReadResponse)
{
    nextOp->nextState = ZBSM_READATTR_APPLICATIONVERSION;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_STARTREADATTRIBUTE);

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_ApplicationVersion)
{
    ReadRemoteAttributes(currentZigBeeDevice, ZBCLUSTER_BASIC, ZBATTRIBUTE_APPLICATIONVERSION);

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(applicationVersion_ReadResponse)
{
    nextOp->nextState = ZBSM_READATTR_STACKVERSION;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_STARTREADATTRIBUTE);

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_StackVersion)
{
    ReadRemoteAttributes(currentZigBeeDevice, ZBCLUSTER_BASIC, ZBATTRIBUTE_STACKVERSION);

    return SM_RESTARTSTATETIMEOUT;
}


/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(stackVersion_ReadResponse)
{
    nextOp->nextState = ZBSM_READATTR_DATECODE;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_STARTREADATTRIBUTE);

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_DateCode)
{
    ReadRemoteAttributes(currentZigBeeDevice, ZBCLUSTER_BASIC, ZBATTRIBUTE_DATECODE);

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(dateCode_ReadResponse)
{
    nextOp->nextState = ZBSM_READATTR_MODELID;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_STARTREADATTRIBUTE);

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_ModelId)
{
    ReadRemoteAttributes(currentZigBeeDevice, ZBCLUSTER_BASIC, ZBATTRIBUTE_MODELID);

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(modelId_ReadResponse)
{
    nextOp->nextState = ZBSM_READATTR_LOCATIONDESC;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_STARTREADATTRIBUTE);

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(readRemoteAttribute_LocationDescription)
{
    ReadRemoteAttributes(currentZigBeeDevice, ZBCLUSTER_BASIC, ZBATTRIBUTE_LOCATIONDESC);

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(locationDescription_ReadResponse)
{
    return SM_ENDSTATEMACHINE;
}
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
