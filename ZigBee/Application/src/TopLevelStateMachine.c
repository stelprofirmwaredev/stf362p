/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2017, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    TopLevelStateMachine.c
* @date    2017/07/17
* @authors J-F. Simard
* @brief

*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include ".\Application\inc\ZigBeeStateMachines.h"
#include ".\Application\inc\ZigBee_ClustersEmulation.h"
#include ".\HAL\BitCloud\inc\HAL_OutgoingBitCloudMsg.h"

#include ".\ZigBee\inc\THREAD_ZigBee.h"
#include ".\DB\inc\THREAD_DB.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define MAX_CONSECUTIVE_ZIGBEE_UPDATE   3

#include "trcRecorder.h"
    extern char* zbEventChannel;

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/

void start(void);
ZIGBEE_EVENTCORE_FUNC(radioSoftReset);
ZIGBEE_EVENTCORE_FUNC(radioResetToFactoryNew);
ZIGBEE_EVENTCORE_FUNC(enterTestMode);
ZIGBEE_EVENTCORE_FUNC(udpateZigbeeModule);
ZIGBEE_EVENTCORE_FUNC(zigBeeModule_UpdateCompleted);
ZIGBEE_EVENTCORE_FUNC(zigbeeModuleHardReset);
ZIGBEE_EVENTCORE_FUNC(pongResetReceived);


/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
static uint8_t zigbeeModuleUpdateCnt;
static uint8_t zigbeeModuleIsUnusable;

extern stateMachineDefinition_t MainStateMachine;
extern stateMachineDefinition_t initializationStateMachine;
extern stateMachineDefinition_t addStatStateMachine;
extern stateMachineDefinition_t DestroyZbNwkStateMachine;
extern stateMachineDefinition_t FactoryTestStateMachine;
extern stateMachineDefinition_t UpdateZigbeeModuleStateMachine;

supportedStateMachineStates_t topLevelSM_SupportedStates [] =
{
    /*****************************************************************/
    /*  top level state Machine : Initialization state        */
    /*****************************************************************/
    {
        .stateId = ZBSM_INITIALSTATE,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_RESETRADIO_TO_FACTORYNEW, .eventCore = radioResetToFactoryNew},
            {.eventId = ZBEVENT_RESETRADIO, .eventCore = radioSoftReset},
            {.eventId = ZBEVENT_ENTERTESTMODE, .eventCore = enterTestMode},
            {.eventId = ZBEVENT_UPDATEZIGBEEMODULE, .eventCore = udpateZigbeeModule},
            {.eventId = ZBEVENT_UPDATEZB_UPDATE_COMPLETED, .eventCore = zigBeeModule_UpdateCompleted},
            {.eventId = ZBEVENT_UPDATEZB_UPDATE_FAILED, .eventCore = zigBeeModule_UpdateCompleted},
            {.eventId = ZBEVENT_RESET_ZIGBEEMODULE, .eventCore = zigbeeModuleHardReset},
            {.eventId = ZBEVENT_PONG_RESET_RECEIVED, .eventCore = pongResetReceived},
        },
        .numberOfSupportedEvents = 8,
        .stateTimeout.smTimeoutLength = SM_NO_TIMEOUT,
        .stateTimeout.smTimeout_cb = NULL,
        .stateTimeout.smTimeoutHandler = NULL,
    },
};

stateMachineDefinition_t topLevelStateMachine =
{
    .SM_Start = start,
    .SM_End = NULL,
    .supportedStates = topLevelSM_SupportedStates,
    .numberOfSupportedStates = sizeof(topLevelSM_SupportedStates)/sizeof(supportedStateMachineStates_t),
    .initialState = ZBSM_INITIALSTATE,
    .stateMachineTimeout.smTimeoutLength = SM_NO_TIMEOUT,
    .stateMachineTimeout.smTimeout_cb = NULL,
    .parentStateMachine = NULL,
    .activeSubStateMachine = NULL,
    .sm_name = "Top Level ",
};

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/


/*******************************************************************************
*    Private functions definitions
*******************************************************************************/


/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(radioResetToFactoryNew)
{
    EndStateMachine (&MainStateMachine, SM_FORCE_END);
    StartStateMachine (&DestroyZbNwkStateMachine, ZBSM_SetEvent(ZBEVENT_REMOVESTAT));
    return SM_SUCCESS;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(radioSoftReset)
{
    SoftReset();
    return SM_SUCCESS;
}


/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(enterTestMode)
{
    EndStateMachine (&MainStateMachine, SM_FORCE_END);

    StartStateMachine (&FactoryTestStateMachine, ZBSM_SetEvent(ZBEVENT_DESTROYCURRENTNETWORK));
    SetTestModeChannel (event.channelNumber);

    return SM_SUCCESS;
}


/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(udpateZigbeeModule)
{
    EndStateMachine(topLevelStateMachine.activeSubStateMachine, SM_FORCE_END);
    if (zigbeeModuleUpdateCnt < MAX_CONSECUTIVE_ZIGBEE_UPDATE )
    {
        //StartStateMachine(&UpdateZigbeeModuleStateMachine, ZBSM_SetEvent(ZBEVENT_MODULEHARDRESET));
        StartStateMachine(&UpdateZigbeeModuleStateMachine, ZBSM_SetEvent(ZBEVENT_GETFRAMECOUNTER));
    }
    else
    {
        zigbeeModuleIsUnusable = 1;
        StartStateMachine(&MainStateMachine, ZBSM_SetEvent(ZBEVENT_STARTINITIALIZATION));
    }

    return SM_SUCCESS;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(zigBeeModule_UpdateCompleted)
{
    StartStateMachine (&MainStateMachine, ZBSM_SetEvent(ZBEVENT_STARTINITIALIZATION));
    zigbeeModuleUpdateCnt++;
    return SM_SUCCESS;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/07/18
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(zigbeeModuleHardReset)
{
    EndStateMachine(topLevelStateMachine.activeSubStateMachine, SM_FORCE_END);
    StartStateMachine (&MainStateMachine, ZBSM_SetEvent(ZBEVENT_STARTINITIALIZATION));

    vTracePrint(zbEventChannel, "Zigbee hard reset");
    return SM_SUCCESS;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/09/27
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(pongResetReceived)
{
    if (NULL != DestroyZbNwkStateMachine.parentStateMachine)
    {
        ZGB_PostEvents(ZBEVENT_RESET_TO_FN_COMPLETED);
    }
    else
    {
        StartStateMachine (&initializationStateMachine, ZBSM_SetEvent(ZBEVENT_MODULE_HAS_REBOOTED));
    }

    return SM_SUCCESS;
}


/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
void start(void)
{
    zigbeeModuleUpdateCnt = 0;
    zigbeeModuleIsUnusable = 0;
    StartStateMachine (&MainStateMachine, ZBSM_SetEvent(ZBEVENT_STARTINITIALIZATION));
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
uint8_t isZigBeeModuleUnusable(void)
{
    return zigbeeModuleIsUnusable;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void SetZigBeeModuleUnusable(uint8_t unusable)
{
    zigbeeModuleIsUnusable = unusable;
}

/** Copyright(C) 2017 Stelpro Design, All Rights Reserved**/
