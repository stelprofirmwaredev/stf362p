/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    DeviceDescriptorStateMachine.c
* @date    2017/06/16
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include ".\Application\inc\ZigBeeStateMachines.h"
#include ".\HAL\BitCloud\inc\HAL_OutgoingBitCloudMsg.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define DEVICEDESCRIPTOR_SM_TIMEOUT         5000    //5sec = 5000ms
#define DEVICEDESCRIPTOR_REPEAT_PERIOD      1000    //1sec



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static void stateTimeout(TimerHandle_t xTimer);
static void smTimeout(TimerHandle_t xTimer);

static void end(uint8_t forceEnd);

ZIGBEE_EVENTCORE_FUNC(DescriptorReceived);
ZIGBEE_EVENTCORE_FUNC(requestSimpleDescriptor);
ZIGBEE_EVENTCORE_FUNC(requestMatchDescriptor);
ZIGBEE_EVENTCORE_FUNC(matchDescriptorInitialRequest);
ZIGBEE_EVENTCORE_FUNC(simpleDescriptorInitialRequest);
ZIGBEE_EVENTCORE_FUNC(terminateDeviceDescriptor);

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
supportedStateMachineStates_t DeviceDescriptorSM_SupportedStates [] =
{
    /*****************************************************************/
    /*  deviceDescriptor state Machine : Initialization state        */
    /*****************************************************************/
    {
        .stateId = ZBSM_INITIALSTATE,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_REQUESTSIMPLEDESCRIPTOR, .eventCore = simpleDescriptorInitialRequest},
            {.eventId = ZBEVENT_REQUESTMATCHDESCRIPTOR, .eventCore = matchDescriptorInitialRequest},
        },
        .numberOfSupportedEvents = 2,
        .stateTimeout.smTimeoutLength = SM_NO_TIMEOUT,
        .stateTimeout.smTimeout_cb = NULL,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  deviceDescriptor state Machine : wait simple descrip. state  */
    /*****************************************************************/
    {
        .stateId = ZBSM_DEVICEDESCRIPTOR_WAITSIMPLEDESCR,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_SIMPLEDESCRESP, .eventCore = DescriptorReceived},
            {.eventId = ZBEVENT_DEVICEDESCRIPTOR_STATE_TIMEOUT, .eventCore = requestSimpleDescriptor},
            {.eventId = ZBEVENT_DEVICEDESCRIPTOR_SM_TIMEOUT, .eventCore = matchDescriptorInitialRequest},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = DEVICEDESCRIPTOR_REPEAT_PERIOD,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  deviceDescriptor state Machine : wait match descrip. state   */
    /*****************************************************************/
    {
        .stateId = ZBSM_DEVICEDESCRIPTOR_WAITMATCHDESCR,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_MATCHDESCRESP, .eventCore = DescriptorReceived},
            {.eventId = ZBEVENT_DEVICEDESCRIPTOR_STATE_TIMEOUT, .eventCore = requestMatchDescriptor},
            {.eventId = ZBEVENT_DEVICEDESCRIPTOR_SM_TIMEOUT, .eventCore = terminateDeviceDescriptor},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = DEVICEDESCRIPTOR_REPEAT_PERIOD,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },
};

stateMachineDefinition_t DeviceDescriptorStateMachine =
{
    .SM_Start = NULL,
    .SM_End = end,
    .supportedStates = DeviceDescriptorSM_SupportedStates,
    .numberOfSupportedStates = sizeof(DeviceDescriptorSM_SupportedStates)/sizeof(supportedStateMachineStates_t),
    .initialState = ZBSM_INITIALSTATE,
    .stateMachineTimeout.smTimeoutLength = DEVICEDESCRIPTOR_SM_TIMEOUT,
    .stateMachineTimeout.smTimeout_cb = smTimeout,
    .parentStateMachine = NULL,
    .activeSubStateMachine = NULL,
    .sm_name = "Device Descr."
};



/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void end(uint8_t forceEnd)
{
    if (SM_REGULAR_END == forceEnd)
    {
        ZGB_PostEvents(ZBEVENT_DEVICEDESCRIPTOR_RECEIVED);
    }
    else
    {
        ZGB_PostEvents(ZBEVENT_ADDSTAT_STATE_TIMEOUT);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void stateTimeout(TimerHandle_t xTimer)
{
    ZGB_PostEvents(ZBEVENT_DEVICEDESCRIPTOR_STATE_TIMEOUT);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void smTimeout(TimerHandle_t xTimer)
{
    ZGB_PostEvents(ZBEVENT_DEVICEDESCRIPTOR_SM_TIMEOUT);
}


/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/13
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(DescriptorReceived)
{
    return SM_ENDSTATEMACHINE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/13
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(terminateDeviceDescriptor)
{
    return SM_TERMINATESTATEMACHINE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/13
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(requestMatchDescriptor)
{
    Request_MatchDescriptor();

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/13
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(requestSimpleDescriptor)
{
    Request_SimpleDescriptor();

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/13
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(matchDescriptorInitialRequest)
{
    Request_MatchDescriptor();
    nextOp->nextState = ZBSM_DEVICEDESCRIPTOR_WAITMATCHDESCR;

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/13
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(simpleDescriptorInitialRequest)
{
    Request_SimpleDescriptor();
    nextOp->nextState = ZBSM_DEVICEDESCRIPTOR_WAITSIMPLEDESCR;

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/



/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
