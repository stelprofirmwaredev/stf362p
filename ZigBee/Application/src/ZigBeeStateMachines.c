/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2017, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    ZigBeeStateMachines.c
* @date    2017/02/24
* @authors J-F. Simard
* @brief
*       This file implement state machine APIs for use with the ZigBee interface
*   of the Maestro (TUX) Controller device.  It implement ways to initialize,
*   reset and configure a remote Zigbee module, along with adding and configuring
*   zigbee devices on the network
*
*   The APIs implement parent-child relationship between different state machine,
*   timeout management for both state machines and states and table style
*   configuration methods for readability and maintenance.
*
*   Basics :
*   - State Machine (stateMachineDefinition_t) :
*       # A state machine has :
*           ## 1 to many states
*           ## 1 timeout (optional)
*           ## 1 parent state machine
*           ## 1 child state machine
*           ## A start function (optional)
*           ## An end function (optional)
*
*   - State (supportedStateMachineStates_t) :
*       # A state has :
*           ## A human readable Id
*           ## 0 to many events
*           ## 1 timeout (optional)
*
*   - Event (supportedStateEvents_t) :
*       # An event has :
*           ## A human readable Id
*           ## a core function
*
*   Using :
*   - Starting a state machine (StartStateMachine()) :
*       # When starting, a state machine will :
*           1. Run the optional start function;
*           2. Establish a parent-child relastionship with the calling SM
*           3. Set the initial (hard-config.) state
*           4. Execute the starting event (if any)
*           5. Start the timeout timer (if any)
*
*   - Executing an event
*       # When executing an event, the system will first look at the registered
*           events for the current state of the top level active state machine
*       # If a match is found in the current state/state machine combination,
*           the core function is executed.  Once the event is executed, the
*           systems returns
*       # If no match is found in the current state/state machine combination,
*           the system will look in the next child state machine for a match,
*           and so forth until the last active state have been parsed.
*       # If no match is ever found, the system returns without further processing
*
*   - Working with timeouts
*       # State machine timeout
*           ## A state machine timeout will execute a callback function upon timer
*               expiration.  The timer is one shot, thus required to be explicitly
*               restarted if needed, altough it has been designed to supervise
*               the proper execution of a state machine and end the execution
*               upon timeout
*           ## A state machine timeout will be refreshed only when the state machine
*               successfully changes state.
*
*       # State timeout
*           ## A state timeout will execute a callback function upon timer
*               expiration.  The timer is one shot, thus required to be explicitly
*               restarted if needed.
*           ## The timer is automatically created on the activation of the state
*               and automatically destroyed on the deactivation of the state
*
*   - Ending a state machine (EndStateMachine())
*       # When ending a state machine, the system will :
*           1. End recursively all child state machine
*           2. Destroy the current state timeout timer
*           3. Destroy the state machine timeout timer
*           4. Run the optional end function
*       # There is 2 path for ending a state machine
*           1. Regular :
*               # The regular path is meant to end a state machine normally,
*                   when all steps have executed without problem
*           2. Force end :
*               # This path has been designed to allow a state machine to terminate
*                   suddenly, due to a process error or a timeout.  Using this path,
*                   the end function will receive indication of such a path and
*                   allow the developper to execute or not some of the post processing
*                   normally executed
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <string.h>
#include ".\Application\inc\ZigBeeStateMachines.h"
#include ".\Application\inc\ZigBee_ClustersEmulation.h"
#include ".\HAL\BitCloud\inc\HAL_OutgoingBitCloudMsg.h"

#include ".\ZigBee\inc\THREAD_ZigBee.h"
#include ".\DB\inc\THREAD_DB.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#include "trcRecorder.h"
extern char* zbEventChannel;

typedef struct _eventQ_t
{
    zigBeeSMEvents_t event;
    struct _eventQ_t * next;
} eventQ_t;

#define ZB_EVENT_MAX    5
SemaphoreHandle_t zb_event_cnt;

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
void runEvent (zigBeeSMEvents_t event);
void SetPermitJoin (uint8_t duration);
void notifyNewEvent(void);

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
uint8_t formAndAdd;
uint8_t resumeAddState;

zbDeviceToAdd_t currentZigBeeDevice;

eventQ_t * eventQHead;
eventQ_t * eventQTail;

uint8_t testStatus = 0;

extern stateMachineDefinition_t topLevelStateMachine;
extern osThreadId zigBeeSMTaskHandle;

static stateMachineDefinition_t * suspendedSM;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/


/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/****************************************************************************
                NEW STATE MACHINE IMPLEMENTATION

                            START

****************************************************************************/


/*******************************************************************************
* @brief  Notify new zigbee event
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/07/18
*******************************************************************************/
void notifyNewEvent(void)
{
    xTaskNotify(zigBeeSMTaskHandle, ZBTHRD_EVENT, eSetBits);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/09
*******************************************************************************/
void startTimeoutTimer(smTimeouts_t * timeout)
{
    if (SM_NO_TIMEOUT != timeout->smTimeoutLength)
    {
        if (NULL == timeout->smTimeoutHandler)
        {
            if (NULL != timeout->smTimeout_cb)
            {
                timeout->smTimeoutHandler = xTimerCreate("",
                                        pdMS_TO_TICKS(timeout->smTimeoutLength),
                                        pdFALSE,        //one-shot
                                        (void*)0,
                                        timeout->smTimeout_cb);
            }
        }
        if (NULL != timeout->smTimeoutHandler)
        {
            xTimerStart(timeout->smTimeoutHandler,0);
        }
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/09
*******************************************************************************/
void stopTimeoutTimer(smTimeouts_t * timeout)
{
    if (NULL != timeout->smTimeoutHandler)
    {
        xTimerDelete (timeout->smTimeoutHandler, 0);
    }
    timeout->smTimeoutHandler = NULL;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/09
*******************************************************************************/
void refreshTimeoutTimer(smTimeouts_t * timeout)
{
    if (NULL != timeout->smTimeoutHandler)
    {
        xTimerStart(timeout->smTimeoutHandler,0);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/08
*******************************************************************************/
stateMachineDefinition_t * findNextPotentialParent (stateMachineDefinition_t * self)
{
    stateMachineDefinition_t * potentialParent = &topLevelStateMachine;
    while (NULL != potentialParent->activeSubStateMachine)
    {
        if (self == potentialParent->activeSubStateMachine)
        {
            break;
        }
        potentialParent = potentialParent->activeSubStateMachine;
    }

    return potentialParent;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/08
*******************************************************************************/
stateMachineErrorCode_t setNextState (stateMachineDefinition_t * self, smNextOperation_t nextOp)
{
    const supportedStateMachineStates_t * currentState;

    if ((uint8_t)-1 != self->currentStateIndex)
    {
        currentState = &self->supportedStates[self->currentStateIndex];

        stopTimeoutTimer((smTimeouts_t *)&currentState->stateTimeout);
    }

    if (ZBSM_EXIT_STATEMACHINE == nextOp.nextState)
    {
        return SM_ENDSTATEMACHINE;
    }

    if (nextOp.nextState == self->currentState)
    {
        return SM_ERRSAMESTATE;
    }
    else
    {
        for (uint8_t i = 0; i < self->numberOfSupportedStates; i++)
        {
            if (nextOp.nextState == self->supportedStates[i].stateId)
            {
                self->currentStateIndex = i;
                currentState = &self->supportedStates[self->currentStateIndex];
                self->currentState = currentState->stateId;
                startTimeoutTimer((smTimeouts_t *)&currentState->stateTimeout);
                {
                    if (ZB_NOEVENT != nextOp.nextEvent.eventId)
                    {
                        runEvent (nextOp.nextEvent);
                    }
                }

                return SM_SUCCESS;
            }
        }
    }
    return SM_ERRINVALIDSTATE;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/08
*******************************************************************************/
void refreshStateMachineTimeouts (stateMachineDefinition_t * currentStateMachine)
{
    do
    {
        refreshTimeoutTimer(&currentStateMachine->stateMachineTimeout);
        currentStateMachine = currentStateMachine->parentStateMachine;
    }
    while (NULL != currentStateMachine);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/08
*******************************************************************************/
stateMachineErrorCode_t parseEvent (const supportedStateMachineStates_t * currentState,
                                    zigBeeSMEvents_t event, smNextOperation_t * nextOp)
{
    vTracePrintF(zbEventChannel, "processing event %d", event.eventId);
    for (uint8_t i = 0; i < currentState->numberOfSupportedEvents; i++)
    {
        if (currentState->supportedEvents[i].eventId == event.eventId)
        {
            if (NULL != currentState->supportedEvents[i].eventCore)
            {
                return (currentState->supportedEvents[i].eventCore(event, nextOp));
            }
            return SM_ERRCANNOTEXECUTE;
        }
    }
    return SM_ERRINVALIDEVENT;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/19
*******************************************************************************/
void storeNewEvent (zigBeeSMEvents_t event)
{
    if (pdTRUE == xSemaphoreTake(zb_event_cnt,pdMS_TO_TICKS(1000) ))
    {
        eventQ_t * newEvent = NULL;
        uint8_t retry = 10;
        while ((newEvent == NULL) && (retry-->0))
        {
            newEvent = pvPortMalloc(sizeof(eventQ_t));
            if (newEvent == NULL)
            {
                osDelay(10);
            }
        }

        if (NULL != newEvent)
        {
            newEvent->event = event;
            newEvent->next = NULL;
            if (NULL == eventQHead)
            {
                eventQHead = newEvent;
                eventQTail = eventQHead;
            }
            else
            {
                eventQTail->next = newEvent;
                eventQTail = eventQTail->next;
            }

            notifyNewEvent();
            vTracePrint(zbEventChannel, "new zb event stored");
        }
    }
    else
    {
        vTracePrint(zbEventChannel, "something went wrong. zb event discarded");
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/19
*******************************************************************************/
void runEvent (zigBeeSMEvents_t event)
{
    stateMachineDefinition_t * currentSMLevel = &topLevelStateMachine;


    while (NULL != currentSMLevel)
    {
        stateMachineErrorCode_t status;
        smNextOperation_t nextOp =
        {
            .nextEvent = ZB_NOEVENT,
            .nextState = ZBSM_NO_NEXTSTATE,
        };

        if ((uint8_t)-1 != currentSMLevel->currentStateIndex)
        {
            status = parseEvent (&currentSMLevel->supportedStates[currentSMLevel->currentStateIndex], event, &nextOp);

            if (SM_ERRINVALIDEVENT != status)
            {
                switch (status)
                {
                case SM_SETNEXTSTATE:
                {
                    stateMachineErrorCode_t status;
                    status = setNextState (currentSMLevel, nextOp);
                    if (SM_ENDSTATEMACHINE == status)
                    {
                        EndStateMachine (currentSMLevel, SM_REGULAR_END);
                    }
                    else if (SM_SUCCESS)
                    {
                        refreshStateMachineTimeouts (currentSMLevel);
                    }
                    else
                    {
                    }
                    break;
                }
                case SM_RESTARTSTATETIMEOUT:
                    startTimeoutTimer((smTimeouts_t *)&currentSMLevel->supportedStates[currentSMLevel->currentStateIndex].stateTimeout);
                    break;

                case SM_ENDSTATEMACHINE:
                    EndStateMachine (currentSMLevel, SM_REGULAR_END);
                    break;

                case SM_TERMINATESTATEMACHINE:
                    EndStateMachine (currentSMLevel, SM_FORCE_END);
                    break;

                case SM_RESTARTSTATEMACHINE:
                    EndStateMachine (currentSMLevel, SM_FORCE_END);
                    StartStateMachine (currentSMLevel,nextOp.nextEvent);
                    break;

                default:
                    break;
                }
                break;
            }
        }
        currentSMLevel = currentSMLevel->activeSubStateMachine;
    }
}


/****************************************************************************
                NEW STATE MACHINE IMPLEMENTATION

                            END

****************************************************************************/








/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/02/24
*******************************************************************************/
void InitZigBeeStateMachines (void)
{
    eventQHead = NULL;
    eventQTail = NULL;

    suspendedSM = NULL;

    zb_event_cnt = xSemaphoreCreateCounting(ZB_EVENT_MAX,ZB_EVENT_MAX);
    configASSERT(zb_event_cnt);
    StartStateMachine (&topLevelStateMachine, ZBSM_NoEvent());

}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/02/24
*******************************************************************************/
void ZGB_ReinitAndResume (void)
{
    zigBeeSMEvents_t event;

    event.eventId = ZBEVENT_STARTINITIALIZATION;
    event.statId = -1;
    event.zbShortAddress = -1;
    event.channelNumber = -1;
    event.resume = 1;

    (void)event;  //this removes a warning
    /* TODO :   */
//    MainZigbeeStateMachine (event);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/02/24
*******************************************************************************/
void ZGB_PostEvents (zigBeeSMEventsEnum_t eventId)
{
    storeNewEvent (ZBSM_SetEvent(eventId));
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/02/24
*******************************************************************************/
void ZGB_PostEventsWithStatId (zigBeeSMEventsEnum_t eventId, statID_t statId)
{
    storeNewEvent (ZBSM_SetEventWIdx(eventId, statId));
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/02/24
*******************************************************************************/
void ZGB_PostEventsWithChannelNumber (zigBeeSMEventsEnum_t eventId, uint8_t channel)
{
    storeNewEvent (ZBSM_SetEventWChnl(eventId, channel));
}

/*******************************************************************************
* @brief  Initiate the ZigBee Association Test (production test)
* @inputs channel: channel to be used in network formation
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/05/11
*******************************************************************************/
void StartZigBeeAssociationTest(uint8_t channel)
{
    ZGB_PostEventsWithChannelNumber(ZBEVENT_ENTERTESTMODE, channel);
}

/*******************************************************************************
* @brief  Return the result of the ZigBee Association Test (production test)
* @inputs None
* @retval testStatus: 1=Associated, 0=Not Associated, 0xFF=Not Finished
* @author Jean-Fran�ois Many
* @date   2017/05/11
*******************************************************************************/
uint8_t GetAssociationTestStatus(void)
{
    return testStatus;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
uint8_t isZbInAddingState(void)
{
//    return ((zigBeeMain_States==ZBMAINSTATE_ADDINGTSTAT)?1:0);
    return 0;
}








/****************************************************************************
                NEW STATE MACHINE IMPLEMENTATION

                            START (PUBLIC SERVICES)

****************************************************************************/







/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/08
*******************************************************************************/
void StartStateMachine (stateMachineDefinition_t * self, zigBeeSMEvents_t startingEvent)
{
    if (NULL != self)
    {
        char string_buffer[100] = "Starting ";
        strcat(string_buffer, (const char*)self->sm_name);
        strcat(string_buffer, " state machine");
        vTracePrint(zbEventChannel,string_buffer);

        self->currentStateIndex = -1;
        self->currentState = ZBSM_NO_NEXTSTATE;

        if (NULL != self->activeSubStateMachine) //in case it is a [re]start
        {
            EndStateMachine(self->activeSubStateMachine, SM_FORCE_END);
        }

        if (&topLevelStateMachine != self)
        {
            self->parentStateMachine = findNextPotentialParent(self);
            self->parentStateMachine->activeSubStateMachine = self;
        }

        if (NULL != self->SM_Start)
        {
            self->SM_Start();
        }

        setNextState (self, (smNextOperation_t){.nextState = self->initialState,.nextEvent = startingEvent});

        startTimeoutTimer(&self->stateMachineTimeout);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/08
*******************************************************************************/
void EndStateMachine (stateMachineDefinition_t * self, uint8_t forceEnd)
{
    if (NULL != self)
    {
        char string_buffer[100] = "Ending ";
        strcat(string_buffer, (const char*)self->sm_name);
        strcat(string_buffer, " state machine");
        vTracePrint(zbEventChannel,string_buffer);

        if (NULL != self->activeSubStateMachine)
        {
            EndStateMachine(self->activeSubStateMachine, forceEnd);
        }

        if (NULL != self->parentStateMachine)
        {
            self->parentStateMachine->activeSubStateMachine = NULL;
            self->parentStateMachine = NULL;
        }
        stopTimeoutTimer (&self->stateMachineTimeout);
        if ((uint8_t)-1 != self->currentStateIndex)
        {
            stopTimeoutTimer ((smTimeouts_t*)&self->supportedStates[self->currentStateIndex].stateTimeout);
        }

        if (NULL != self->SM_End)
        {
            self->SM_End(forceEnd);
        }
    }
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void zigbee_event_task(void const * argument)
{
    eventQ_t * event = (eventQ_t*)argument;

    runEvent(event->event);

    vPortFree (event);

    vTaskDelete(NULL);
}



/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/08
*******************************************************************************/
void ExecuteEvent (void)
{
    if (NULL != eventQHead)
    {
        eventQ_t * next;

        runEvent(eventQHead->event);
        next = eventQHead->next;
        vPortFree (eventQHead);
        eventQHead = next;
        xSemaphoreGive(zb_event_cnt);

//        osThreadDef(zbEventTask, zigbee_event_task,osPriorityNormal, 0, 256);
//        if (NULL != osThreadCreate(osThread(zbEventTask), eventQHead))
//        {
//            eventQHead = eventQHead->next;
//            xSemaphoreGive(zb_event_cnt);
//            vTracePrint(zbEventChannel, "Zigbee event dispatched");
//        }
//        else
//        {
//            vTracePrint(zbEventChannel, "cannot create event Task. will retry later...");
//        }
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/08
*******************************************************************************/
void StartStateMachineTimeout (stateMachineDefinition_t * self)
{
    startTimeoutTimer(&self->stateMachineTimeout);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/08
*******************************************************************************/
void StopStateMachineTimeout (stateMachineDefinition_t * self)
{
    stopTimeoutTimer(&self->stateMachineTimeout);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/07/18
*******************************************************************************/
void SuspendSM (stateMachineDefinition_t * self)
{
    suspendedSM = self;
    self->parentStateMachine->activeSubStateMachine = NULL;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/07/18
*******************************************************************************/
void ResumeSM (void)
{
    if (NULL != suspendedSM)
    {
        if (suspendedSM->parentStateMachine->activeSubStateMachine)
        {
            EndStateMachine (suspendedSM->parentStateMachine->activeSubStateMachine, 1);
        }
        suspendedSM->parentStateMachine->activeSubStateMachine = suspendedSM;
        suspendedSM = NULL;
    }
}



/****************************************************************************
                NEW STATE MACHINE IMPLEMENTATION

                            END (PUBLIC SERVICES

****************************************************************************/

/** Copyright(C) 2017 Stelpro Design, All Rights Reserved**/
