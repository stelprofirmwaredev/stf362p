/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    BindingClusterStateMachine.c
* @date    2017/06/14
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include ".\Application\inc\ZigBeeStateMachines.h"
#include ".\HAL\BitCloud\inc\HAL_OutgoingBitCloudMsg.h"
#include ".\DB\inc\THREAD_DB.h"



/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define BINDINGSTATEMACHINE_TIMEOUT     5000        //5sec
#define BINDINGSTATE_TIMEOUT            1000        //1sec

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
void stateTimeout (TimerHandle_t xTimer);
void stateMachineTimeout (TimerHandle_t xTimer);
void endBindingClusterStateMachine(uint8_t forceEnd);

ZIGBEE_EVENTCORE_FUNC(bindThermostatCluster);
ZIGBEE_EVENTCORE_FUNC(bindThermostatUICluster);
ZIGBEE_EVENTCORE_FUNC(bindHumidityCluster);
ZIGBEE_EVENTCORE_FUNC(bindThermostatClusterTimeout);
ZIGBEE_EVENTCORE_FUNC(bindThermostatUIClusterTimeout);
ZIGBEE_EVENTCORE_FUNC(bindHumidityClusterTimeout);
ZIGBEE_EVENTCORE_FUNC(bindingDone);

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
supportedStateMachineStates_t BindingClustersSM_SupportedStates [] =
{
    /**************************************************************/
    /* binding clusters state Machine : thermostat cluster state  */
    /**************************************************************/
    {
        .stateId = ZBSM_BIND_THERMOSTAT_CLUSTER,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTCLUSTERBINDING,            .eventCore = bindThermostatCluster},
            {.eventId = ZBEVENT_BINDRESPONSE,                   .eventCore = bindThermostatUICluster},
            {.eventId = ZBEVENT_BINDREQUESTERROR,               .eventCore = bindThermostatClusterTimeout},
            {.eventId = ZBEVENT_BINDINGCLUSTER_STATE_TIMEOUT,   .eventCore = bindThermostatClusterTimeout},
        },
        .numberOfSupportedEvents = 4,
        .stateTimeout.smTimeoutLength = BINDINGSTATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /* binding clusters state Machine : thermostat UI cluster state  */
    /*****************************************************************/
    {
        .stateId = ZBSM_BIND_THERMOSTATUI_CLUSTER,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTCLUSTERBINDING,            .eventCore = bindThermostatUICluster},
            {.eventId = ZBEVENT_BINDRESPONSE,                   .eventCore = bindHumidityCluster},
            {.eventId = ZBEVENT_BINDREQUESTERROR,               .eventCore = bindThermostatUIClusterTimeout},
            {.eventId = ZBEVENT_BINDINGCLUSTER_STATE_TIMEOUT,   .eventCore = bindThermostatUIClusterTimeout},
        },
        .numberOfSupportedEvents = 4,
        .stateTimeout.smTimeoutLength = BINDINGSTATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /************************************************************/
    /* binding clusters state Machine : Humidity cluster state  */
    /************************************************************/
    {
        .stateId = ZBSM_BIND_HUMIDITY_CLUSTER,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTCLUSTERBINDING,            .eventCore = bindHumidityCluster},
            {.eventId = ZBEVENT_BINDRESPONSE,                   .eventCore = bindingDone},
            {.eventId = ZBEVENT_BINDREQUESTERROR,               .eventCore = bindHumidityClusterTimeout},
            {.eventId = ZBEVENT_BINDINGCLUSTER_STATE_TIMEOUT,   .eventCore = bindHumidityClusterTimeout},
        },
        .numberOfSupportedEvents = 4,
        .stateTimeout.smTimeoutLength = BINDINGSTATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },
};

stateMachineDefinition_t BindingClustersStateMachine =
{
    .SM_Start = NULL,
    .SM_End = endBindingClusterStateMachine,
    .supportedStates = BindingClustersSM_SupportedStates,
    .numberOfSupportedStates = sizeof(BindingClustersSM_SupportedStates)/sizeof(supportedStateMachineStates_t),
    .initialState = ZBSM_BIND_THERMOSTAT_CLUSTER,
    .stateMachineTimeout.smTimeoutLength = BINDINGSTATEMACHINE_TIMEOUT,
    .stateMachineTimeout.smTimeoutHandler = NULL,
    .stateMachineTimeout.smTimeout_cb = stateMachineTimeout,
    .parentStateMachine = NULL,
    .activeSubStateMachine = NULL,
    .sm_name = "Binding",
};

extern zbDeviceToAdd_t currentZigBeeDevice;
extern stateMachineDefinition_t ConfigureReportingStateMachine;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/




/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/15
*******************************************************************************/
void stateMachineTimeout (TimerHandle_t xTimer)
{
    ZGB_PostEvents(ZBEVENT_BINDINGCLUSTER_SM_TIMEOUT);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/15
*******************************************************************************/
void stateTimeout (TimerHandle_t xTimer)
{
    ZGB_PostEvents(ZBEVENT_BINDINGCLUSTER_STATE_TIMEOUT);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/14
*******************************************************************************/
void endBindingClusterStateMachine(uint8_t forceEnd)
{
    if (SM_REGULAR_END == forceEnd)
    {
        ZGB_PostEvents(ZBEVENT_BINDINGCLUSTER_COMPLETED);
    }
}


/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(bindThermostatClusterTimeout)
{
    (void)event;

    BindClusters (currentZigBeeDevice, ZBCLUSTER_THERMOSTAT);

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(bindThermostatUIClusterTimeout)
{
    (void)event;

    BindClusters (currentZigBeeDevice, ZBCLUSTER_THERMOSTAT_UI_CONF);

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(bindHumidityClusterTimeout)
{
    (void)event;

    BindClusters (currentZigBeeDevice, ZBCLUSTER_HUMIDITYMEAS);

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(bindThermostatCluster)
{
    (void)event;

    if (0 != (currentZigBeeDevice.clusterToBind & CLUSTERTOBIND_THERMOSTAT))
    {
        BindClusters (currentZigBeeDevice, ZBCLUSTER_THERMOSTAT);
        nextOp->nextState = ZBSM_BIND_THERMOSTAT_CLUSTER;
    }
    else
    {
        nextOp->nextState = ZBSM_BIND_THERMOSTATUI_CLUSTER;
        nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_STARTCLUSTERBINDING);
    }


    return SM_SETNEXTSTATE;
}


/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(bindThermostatUICluster)
{
    (void)event;

    if (0 != (currentZigBeeDevice.clusterToBind & CLUSTERTOBIND_THERMOSTATUI))
    {
        BindClusters (currentZigBeeDevice, ZBCLUSTER_THERMOSTAT_UI_CONF);

        nextOp->nextState = ZBSM_BIND_THERMOSTATUI_CLUSTER;
    }
    else
    {
        nextOp->nextState = ZBSM_BIND_HUMIDITY_CLUSTER;
        nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_STARTCLUSTERBINDING);
    }

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(bindHumidityCluster)
{
    (void)event;

    if (0 != (currentZigBeeDevice.clusterToBind & CLUSTERTOBIND_HUMIDITY))
    {
        BindClusters (currentZigBeeDevice, ZBCLUSTER_HUMIDITYMEAS);

        nextOp->nextState = ZBSM_BIND_HUMIDITY_CLUSTER;
        return SM_SETNEXTSTATE;
    }
    else
    {
        return SM_ENDSTATEMACHINE;
    }
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(bindingDone)
{
    (void)event;
    (void)nextOp;

    return SM_ENDSTATEMACHINE;
}


/*******************************************************************************
*    Public functions definitions
*******************************************************************************/



/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
