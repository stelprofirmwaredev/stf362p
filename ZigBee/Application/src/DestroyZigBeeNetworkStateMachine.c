/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    DestroyZigBeeStateMachine.c
* @date    2017/07/04
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include ".\ZigBee\inc\THREAD_ZigBee.h"
#include ".\Application\inc\ZigBeeStateMachines.h"
#include ".\HAL\BitCloud\inc\HAL_OutgoingBitCloudMsg.h"

#include ".\DB\classes\inc\class_Thermostats.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define REMOVESTAT_TIMEOUT          (2*1000)            //2 seconds
#define DESTROYSM_TIMEOUT          (50*1000)            //2 seconds
#define FORCE_ERASE_TIMEOUT        (10)
#define WAIT_ZIGBEE_MODULE          (10*1000)

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static void stateTimeout(TimerHandle_t xTimer);
static void smTimeout(TimerHandle_t xTimer);
static void waitZigbeeModule(TimerHandle_t xTimer);

static void SmStart(void);
static void SmEnd(uint8_t forceEnd);

ZIGBEE_EVENTCORE_FUNC(sendLeaveRequest);
ZIGBEE_EVENTCORE_FUNC(LeaveRsp);
ZIGBEE_EVENTCORE_FUNC(LeaveRsp_Timeout);
ZIGBEE_EVENTCORE_FUNC(WaitZigbeeModule_Timeout);
ZIGBEE_EVENTCORE_FUNC(SendResetToFn);

static statID_t statId;

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
supportedStateMachineStates_t DestroyZbNwkSM_SupportedStates [] =
{
    /*****************************************************************/
    /*  destroy zb nwk state Machine : remove stat state        */
    /*****************************************************************/
    {
        .stateId = ZBSM_DESTROYNWK_REMOVETSTAT,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_REMOVESTAT, .eventCore = sendLeaveRequest},
        },
        .numberOfSupportedEvents = 1,
        .stateTimeout.smTimeoutLength = SM_NO_TIMEOUT,
        .stateTimeout.smTimeout_cb = NULL,
        .stateTimeout.smTimeoutHandler = NULL,
    },
    /*****************************************************************/
    /*  destroy zb nwk state Machine : wait leave rsp state        */
    /*****************************************************************/
    {
        .stateId = ZBSM_DESTROYNWK_WAIT_LEAVERSP,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_REMOVESTAT_RESP, .eventCore = LeaveRsp},
            {.eventId = ZBEVENT_REMOVESTAT_TIMEOUT, .eventCore = LeaveRsp_Timeout},
        },
        .numberOfSupportedEvents = 2,
        .stateTimeout.smTimeoutLength = REMOVESTAT_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },
    /*****************************************************************/
    /*  destroy zb nwk state Machine : force flush state        */
    /*****************************************************************/
    {
        .stateId = ZBSM_DESTROYNWK_FORCE_ERASE,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_REMOVESTAT_TIMEOUT, .eventCore = LeaveRsp_Timeout},
        },
        .numberOfSupportedEvents = 1,
        .stateTimeout.smTimeoutLength = FORCE_ERASE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },
    /*****************************************************************/
    /*  destroy zb nwk state Machine : wait zigbee module state      */
    /*****************************************************************/
    {
        .stateId = ZBSM_DESTROYNWK_WAIT_ZIGBEE_MODULE,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_SEND_RESET_TO_FN, .eventCore = SendResetToFn},
            {.eventId = ZBEVENT_WAIT_ZIGBEE_MODULE, .eventCore = WaitZigbeeModule_Timeout},
            {.eventId = ZBEVENT_RESET_TO_FN_COMPLETED, .eventCore = WaitZigbeeModule_Timeout},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = WAIT_ZIGBEE_MODULE,
        .stateTimeout.smTimeout_cb = waitZigbeeModule,
        .stateTimeout.smTimeoutHandler = NULL,
    },

};

stateMachineDefinition_t DestroyZbNwkStateMachine =
{
    .SM_Start = SmStart,
    .SM_End = SmEnd,
    .supportedStates = DestroyZbNwkSM_SupportedStates,
    .numberOfSupportedStates = sizeof(DestroyZbNwkSM_SupportedStates)/sizeof(supportedStateMachineStates_t),
    .initialState = ZBSM_DESTROYNWK_REMOVETSTAT,
    .stateMachineTimeout.smTimeoutLength = DESTROYSM_TIMEOUT,
    .stateMachineTimeout.smTimeout_cb = smTimeout,
    .parentStateMachine = NULL,
    .activeSubStateMachine = NULL,
    .sm_name = "Destroy ZB Net"
};

extern stateMachineDefinition_t MainStateMachine;
extern SemaphoreHandle_t sem_ClearDataBase;
extern uint8_t testStatus;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void SmStart(void)
{
    statId = THERMOSTAT_INSTANCES-1;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void SmEnd(uint8_t forceEnd)
{
    StartStateMachine (&MainStateMachine, ZBSM_NoEvent());

    {
        dbType_ZigBeeNetworkInfo_t nwInfo;

        DBTHRD_GetData(DBTYPE_ZIGBEE_NETWORK_INFO,(void*)&nwInfo, INDEX_DONT_CARE);
        nwInfo.InANetwork = 0;
        if (0 == getKeepSameNetwork())
        {
            nwInfo.ZigBeeAssociationPermit = 0;
            nwInfo.ActiveChannel = 0;
            nwInfo.PanID = 0;
            nwInfo.ShortAdd = -1;
            nwInfo.extPANId = -1;
        }
        DBTHRD_SetData(DBTYPE_ZIGBEE_NETWORK_INFO,(void*)&nwInfo, INDEX_DONT_CARE,DBCHANGE_ZIGBEE);

    }

#ifndef ZIGBEE_CERTIFICATION_INTERFACE
    ZGB_PostEvents(ZBEVENT_NETWORKDESTROYED);
#endif
    if (NULL != sem_ClearDataBase)
    {
        xSemaphoreGive (sem_ClearDataBase);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void stateTimeout(TimerHandle_t xTimer)
{
    ZGB_PostEvents(ZBEVENT_REMOVESTAT_TIMEOUT);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void smTimeout(TimerHandle_t xTimer)
{
    EndStateMachine (&DestroyZbNwkStateMachine, SM_FORCE_END);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void waitZigbeeModule(TimerHandle_t xTimer)
{
    ZGB_PostEvents(ZBEVENT_WAIT_ZIGBEE_MODULE);
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(sendLeaveRequest)
{
    if (0 != tstatNetworkLeaveReq (statId))
    {
        nextOp->nextState = ZBSM_DESTROYNWK_FORCE_ERASE;
    }
    else
    {
        nextOp->nextState = ZBSM_DESTROYNWK_WAIT_LEAVERSP;
    }

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(LeaveRsp)
{
    classTH_DeleteThermostat(event.statId, DELETE_ONE_TERMOSTAT);
    if (event.statId == statId)
    {
        if (statId > 0)
        {
            statId--;
        }
        else
        {
            nextOp->nextState = ZBSM_DESTROYNWK_WAIT_ZIGBEE_MODULE;
            nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_SEND_RESET_TO_FN);
            return SM_SETNEXTSTATE;
        }
    }
    else
    {
        statId = THERMOSTAT_INSTANCES-1;        //sync. problem; restart sequence
    }
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_REMOVESTAT);
    nextOp->nextState = ZBSM_DESTROYNWK_REMOVETSTAT;
    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(LeaveRsp_Timeout)
{
    classTH_DeleteThermostat(statId, DELETE_ONE_TERMOSTAT);
    if (statId > 0)
    {
        statId--;
        nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_REMOVESTAT);
        nextOp->nextState = ZBSM_DESTROYNWK_REMOVETSTAT;
        return SM_SETNEXTSTATE;
    }
    else
    {
        nextOp->nextState = ZBSM_DESTROYNWK_WAIT_ZIGBEE_MODULE;
        nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_SEND_RESET_TO_FN);
        return SM_SETNEXTSTATE;
    }
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(WaitZigbeeModule_Timeout)
{
    return SM_ENDSTATEMACHINE;
}



/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(SendResetToFn)
{
    ResettoFactoryNew();
    testStatus = 0;
    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/



/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
