/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    ConfigureReporting.c
* @date    2017/06/16
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include ".\Application\inc\ZigBeeStateMachines.h"
#include ".\HAL\BitCloud\inc\HAL_OutgoingBitCloudMsg.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define CONFIGUREREPORT_STATEMACHINE_TIMEOUT     5000        //5sec
#define CONFIGUREREPORT_STATE_TIMEOUT            1000        //1sec



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static void stateTimeout(TimerHandle_t xTimer);
static void smTimeout(TimerHandle_t xTimer);

static void end(uint8_t forceEnd);

ZIGBEE_EVENTCORE_FUNC(configure_LocalTemp_Reporting);
ZIGBEE_EVENTCORE_FUNC(localTemp_ConfReportingResponse);
ZIGBEE_EVENTCORE_FUNC(configure_HeatSetpoint_Reporting);
ZIGBEE_EVENTCORE_FUNC(HeatSetpoint_ConfReportingResponse);
ZIGBEE_EVENTCORE_FUNC(configure_HeatDemand_Reporting);
ZIGBEE_EVENTCORE_FUNC(HeatDemand_ConfReportingResponse);
ZIGBEE_EVENTCORE_FUNC(configure_PowerConsumption_Reporting);
ZIGBEE_EVENTCORE_FUNC(powerConsumption_ConfReportingResponse);
ZIGBEE_EVENTCORE_FUNC(configure_Alert_Reporting);
ZIGBEE_EVENTCORE_FUNC(alert_ConfReportingResponse);
ZIGBEE_EVENTCORE_FUNC(configure_FloorMode_Reporting);
ZIGBEE_EVENTCORE_FUNC(floorMode_ConfReportingResponse);
ZIGBEE_EVENTCORE_FUNC(configure_RelayCycleCount_Reporting);
ZIGBEE_EVENTCORE_FUNC(relayCycleCount_ConfReportingResponse);
ZIGBEE_EVENTCORE_FUNC(configure_TemperatureDisplayMode_Reporting);
ZIGBEE_EVENTCORE_FUNC(temperatureDisplayMode_ConfReportingResponse);
ZIGBEE_EVENTCORE_FUNC(configure_KeypadLockout_Reporting);
ZIGBEE_EVENTCORE_FUNC(keypadLockout_ConfReportingResponse);
ZIGBEE_EVENTCORE_FUNC(configure_LocalHumidity_Reporting);
ZIGBEE_EVENTCORE_FUNC(localHumidity_ConfReportingResponse);

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
supportedStateMachineStates_t ConfigureReportingSM_SupportedStates [] =
{
    /*****************************************************************/
    /*  configureReporting state Machine : local temperature state   */
    /*****************************************************************/
    {
        .stateId = ZBSM_CONFREPORT_LOCALTEMP,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTCONFIGUREREPORTING, .eventCore = configure_LocalTemp_Reporting},
            {.eventId = ZBEVENT_CONFIGUREREPORTINGRESPONSE, .eventCore = localTemp_ConfReportingResponse},
            {.eventId = ZBEVENT_CONFIGUREREPORTINGTERROR, .eventCore = configure_LocalTemp_Reporting},
            {.eventId = ZBEVENT_CONFIGUREREPORTING_STATE_TIMEOUT, .eventCore = configure_LocalTemp_Reporting},
        },
        .numberOfSupportedEvents = 4,
        .stateTimeout.smTimeoutLength = CONFIGUREREPORT_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  configureReporting state Machine : heat setpoint state      */
    /*****************************************************************/
    {
        .stateId = ZBSM_CONFREPORT_HEATSETPOINT,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTCONFIGUREREPORTING, .eventCore = configure_HeatSetpoint_Reporting},
            {.eventId = ZBEVENT_CONFIGUREREPORTINGRESPONSE, .eventCore = HeatSetpoint_ConfReportingResponse},
            {.eventId = ZBEVENT_CONFIGUREREPORTINGTERROR, .eventCore = configure_HeatSetpoint_Reporting},
            {.eventId = ZBEVENT_CONFIGUREREPORTING_STATE_TIMEOUT, .eventCore = configure_HeatSetpoint_Reporting},
        },
        .numberOfSupportedEvents = 4,
        .stateTimeout.smTimeoutLength = CONFIGUREREPORT_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  configureReporting state Machine : heat demand state      */
    /*****************************************************************/
    {
        .stateId = ZBSM_CONFREPORT_HEATDEMAND,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTCONFIGUREREPORTING, .eventCore = configure_HeatDemand_Reporting},
            {.eventId = ZBEVENT_CONFIGUREREPORTINGRESPONSE, .eventCore = HeatDemand_ConfReportingResponse},
            {.eventId = ZBEVENT_CONFIGUREREPORTINGTERROR, .eventCore = configure_HeatDemand_Reporting},
            {.eventId = ZBEVENT_CONFIGUREREPORTING_STATE_TIMEOUT, .eventCore = configure_HeatDemand_Reporting},
        },
        .numberOfSupportedEvents = 4,
        .stateTimeout.smTimeoutLength = CONFIGUREREPORT_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  configureReporting state Machine : power consumption state   */
    /*****************************************************************/
    {
        .stateId = ZBSM_CONFREPORT_POWERCONSUMPTION,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTCONFIGUREREPORTING, .eventCore = configure_PowerConsumption_Reporting},
            {.eventId = ZBEVENT_CONFIGUREREPORTINGRESPONSE, .eventCore = powerConsumption_ConfReportingResponse},
            {.eventId = ZBEVENT_CONFIGUREREPORTINGTERROR, .eventCore = configure_PowerConsumption_Reporting},
            {.eventId = ZBEVENT_CONFIGUREREPORTING_STATE_TIMEOUT, .eventCore = configure_PowerConsumption_Reporting},
        },
        .numberOfSupportedEvents = 4,
        .stateTimeout.smTimeoutLength = CONFIGUREREPORT_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  configureReporting state Machine : alert state               */
    /*****************************************************************/
    {
        .stateId = ZBSM_CONFREPORT_ALERT,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTCONFIGUREREPORTING, .eventCore = configure_Alert_Reporting},
            {.eventId = ZBEVENT_CONFIGUREREPORTINGRESPONSE, .eventCore = alert_ConfReportingResponse},
            {.eventId = ZBEVENT_CONFIGUREREPORTINGTERROR, .eventCore = configure_Alert_Reporting},
            {.eventId = ZBEVENT_CONFIGUREREPORTING_STATE_TIMEOUT, .eventCore = configure_Alert_Reporting},
        },
        .numberOfSupportedEvents = 4,
        .stateTimeout.smTimeoutLength = CONFIGUREREPORT_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  configureReporting state Machine : floor mode                */
    /*****************************************************************/
    {
        .stateId = ZBSM_CONFREPORT_FLOORMODE,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTCONFIGUREREPORTING, .eventCore = configure_FloorMode_Reporting},
            {.eventId = ZBEVENT_CONFIGUREREPORTINGRESPONSE, .eventCore = floorMode_ConfReportingResponse},
            {.eventId = ZBEVENT_CONFIGUREREPORTINGTERROR, .eventCore = configure_FloorMode_Reporting},
            {.eventId = ZBEVENT_CONFIGUREREPORTING_STATE_TIMEOUT, .eventCore = configure_FloorMode_Reporting},
        },
        .numberOfSupportedEvents = 4,
        .stateTimeout.smTimeoutLength = CONFIGUREREPORT_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  configureReporting state Machine : relay cycle count         */
    /*****************************************************************/
    {
        .stateId = ZBSM_CONFREPORT_RELAYCYCLECOUNT,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTCONFIGUREREPORTING, .eventCore = configure_RelayCycleCount_Reporting},
            {.eventId = ZBEVENT_CONFIGUREREPORTINGRESPONSE, .eventCore = relayCycleCount_ConfReportingResponse},
            {.eventId = ZBEVENT_CONFIGUREREPORTINGTERROR, .eventCore = configure_RelayCycleCount_Reporting},
            {.eventId = ZBEVENT_CONFIGUREREPORTING_STATE_TIMEOUT, .eventCore = configure_RelayCycleCount_Reporting},
        },
        .numberOfSupportedEvents = 4,
        .stateTimeout.smTimeoutLength = CONFIGUREREPORT_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /************************************************************************/
    /*  configureReporting state Machine : temperature display mode state   */
    /************************************************************************/
    {
        .stateId = ZBSM_CONFREPORT_TEMPERATUREDISPLAYMODE,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTCONFIGUREREPORTING, .eventCore = configure_TemperatureDisplayMode_Reporting},
            {.eventId = ZBEVENT_CONFIGUREREPORTINGRESPONSE, .eventCore = temperatureDisplayMode_ConfReportingResponse},
            {.eventId = ZBEVENT_CONFIGUREREPORTINGTERROR, .eventCore = configure_TemperatureDisplayMode_Reporting},
            {.eventId = ZBEVENT_CONFIGUREREPORTING_STATE_TIMEOUT, .eventCore = configure_TemperatureDisplayMode_Reporting},
        },
        .numberOfSupportedEvents = 4,
        .stateTimeout.smTimeoutLength = CONFIGUREREPORT_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  configureReporting state Machine : keypad lock state      */
    /*****************************************************************/
    {
        .stateId = ZBSM_CONFREPORT_KEYPADLOCKOUT,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTCONFIGUREREPORTING, .eventCore = configure_KeypadLockout_Reporting},
            {.eventId = ZBEVENT_CONFIGUREREPORTINGRESPONSE, .eventCore = keypadLockout_ConfReportingResponse},
            {.eventId = ZBEVENT_CONFIGUREREPORTINGTERROR, .eventCore = configure_KeypadLockout_Reporting},
            {.eventId = ZBEVENT_CONFIGUREREPORTING_STATE_TIMEOUT, .eventCore = configure_KeypadLockout_Reporting},
        },
        .numberOfSupportedEvents = 4,
        .stateTimeout.smTimeoutLength = CONFIGUREREPORT_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  configureReporting state Machine : local humidity state      */
    /*****************************************************************/
    {
        .stateId = ZBSM_CONFREPORT_LOCALHUMIDITY,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTCONFIGUREREPORTING, .eventCore = configure_LocalHumidity_Reporting},
            {.eventId = ZBEVENT_CONFIGUREREPORTINGRESPONSE, .eventCore = localHumidity_ConfReportingResponse},
            {.eventId = ZBEVENT_CONFIGUREREPORTINGTERROR, .eventCore = configure_LocalHumidity_Reporting},
            {.eventId = ZBEVENT_CONFIGUREREPORTING_STATE_TIMEOUT, .eventCore = configure_LocalHumidity_Reporting},
        },
        .numberOfSupportedEvents = 4,
        .stateTimeout.smTimeoutLength = CONFIGUREREPORT_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stateTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

};

stateMachineDefinition_t ConfigureReportingStateMachine =
{
    .SM_Start = NULL,
    .SM_End = end,
    .supportedStates = ConfigureReportingSM_SupportedStates,
    .numberOfSupportedStates = sizeof(ConfigureReportingSM_SupportedStates)/sizeof(supportedStateMachineStates_t),
    .initialState = ZBSM_CONFREPORT_LOCALTEMP,
    .stateMachineTimeout.smTimeoutLength = CONFIGUREREPORT_STATEMACHINE_TIMEOUT,
    .stateMachineTimeout.smTimeout_cb = smTimeout,
    .parentStateMachine = NULL,
    .activeSubStateMachine = NULL,
    .sm_name = "Configure Report"
};

extern zbDeviceToAdd_t currentZigBeeDevice;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void end(uint8_t forceEnd)
{
    if (SM_REGULAR_END == forceEnd)
    {
        ZGB_PostEvents(ZBEVENT_CONFIGUREREPORTING_COMPLETED);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void stateTimeout(TimerHandle_t xTimer)
{
    ZGB_PostEvents(ZBEVENT_CONFIGUREREPORTING_STATE_TIMEOUT);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void smTimeout(TimerHandle_t xTimer)
{
     ZGB_PostEvents(ZBEVENT_CONFIGUREREPORTING_SM_TIMEOUT);
}



/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(configure_LocalTemp_Reporting)
{
    ConfigureReport(currentZigBeeDevice,CONFREPORT_ATTR_LOCALTEMP);

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(localTemp_ConfReportingResponse)
{
    nextOp->nextState = ZBSM_CONFREPORT_HEATSETPOINT;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_STARTCONFIGUREREPORTING);

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(configure_HeatSetpoint_Reporting)
{
    ConfigureReport(currentZigBeeDevice,CONFREPORT_ATTR_HEATSETPOINT);

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(HeatSetpoint_ConfReportingResponse)
{
    nextOp->nextState = ZBSM_CONFREPORT_HEATDEMAND;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_STARTCONFIGUREREPORTING);

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(configure_HeatDemand_Reporting)
{
    ConfigureReport(currentZigBeeDevice,CONFREPORT_ATTR_HEATDEMAND);

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(HeatDemand_ConfReportingResponse)
{
    nextOp->nextState = ZBSM_CONFREPORT_POWERCONSUMPTION;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_STARTCONFIGUREREPORTING);

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(configure_PowerConsumption_Reporting)
{
    ConfigureReport(currentZigBeeDevice,CONFREPORT_ATTR_POWERCONSUMPTION);

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(powerConsumption_ConfReportingResponse)
{
    nextOp->nextState = ZBSM_CONFREPORT_ALERT;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_STARTCONFIGUREREPORTING);

    return SM_SETNEXTSTATE;
}


/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(configure_Alert_Reporting)
{
    ConfigureReport(currentZigBeeDevice,CONFREPORT_ATTR_ALERT);

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(alert_ConfReportingResponse)
{
    nextOp->nextState = ZBSM_CONFREPORT_FLOORMODE;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_STARTCONFIGUREREPORTING);

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(configure_FloorMode_Reporting)
{
    ConfigureReport(currentZigBeeDevice,CONFREPORT_ATTR_FLOORMODE);

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(floorMode_ConfReportingResponse)
{
    nextOp->nextState = ZBSM_CONFREPORT_RELAYCYCLECOUNT;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_STARTCONFIGUREREPORTING);

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(configure_RelayCycleCount_Reporting)
{
    ConfigureReport(currentZigBeeDevice,CONFREPORT_ATTR_RELAYCYCLECOUNT);

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(relayCycleCount_ConfReportingResponse)
{
    nextOp->nextState = ZBSM_CONFREPORT_TEMPERATUREDISPLAYMODE;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_STARTCONFIGUREREPORTING);

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(configure_TemperatureDisplayMode_Reporting)
{
    ConfigureReport(currentZigBeeDevice,CONFREPORT_ATTR_TEMPERATUREDISPLAYMODE);

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(temperatureDisplayMode_ConfReportingResponse)
{
    nextOp->nextState = ZBSM_CONFREPORT_KEYPADLOCKOUT;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_STARTCONFIGUREREPORTING);

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(configure_KeypadLockout_Reporting)
{
    ConfigureReport(currentZigBeeDevice,CONFREPORT_ATTR_KEYPADLOCKOUT);

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(keypadLockout_ConfReportingResponse)
{
    nextOp->nextState = ZBSM_CONFREPORT_LOCALHUMIDITY;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_STARTCONFIGUREREPORTING);

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(configure_LocalHumidity_Reporting)
{
    ConfigureReport(currentZigBeeDevice,CONFREPORT_ATTR_LOCALHUMIDITY);

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(localHumidity_ConfReportingResponse)
{
    return SM_ENDSTATEMACHINE;
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
