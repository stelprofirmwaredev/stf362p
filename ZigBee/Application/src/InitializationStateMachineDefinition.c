/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    InitializationStateMachineDefinition.c
* @date    2017/06/09
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include ".\Application\inc\ZigBeeStateMachines.h"
#include ".\HAL\BitCloud\inc\HAL_OutgoingBitCloudMsg.h"
#include ".\HAL\inc\HAL_ZigBeeInterface.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\ZigBee\inc\THREAD_ZigBee.h"
#include ".\Console\inc\THREAD_Console.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define INIT_MAXHARDRESET_COUNT         3
#define MAX_STACK_VERSION_RETRY     2
#define MAX_PAN_ID_RETRY            2

#define BOOTLOADER_WAITING_DELAY    600         //600ms
#define WAIT_PONG_RESPONSE_TIMEOUT  (10*1000)   //10sec

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/


/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static void initStart(void);
static void initEnd(uint8_t forceEnd);
static void waitBootloader(TimerHandle_t xTimer);
static void stackVersionTimer_cb(TimerHandle_t xTimer);
static void panIdTimer_cb(TimerHandle_t xTimer);
static void networkChannelTimer_cb(TimerHandle_t xTimer);
static void networkAddressTimer_cb(TimerHandle_t xTimer);
static void waitPongRsp(TimerHandle_t xTimer);

ZIGBEE_EVENTCORE_FUNC(stackVersionReceivedTimeout);
ZIGBEE_EVENTCORE_FUNC(uartConfig);
ZIGBEE_EVENTCORE_FUNC(NetworkAddressTimeOut);
ZIGBEE_EVENTCORE_FUNC(zigbeeModule_hardReset);
ZIGBEE_EVENTCORE_FUNC(requestStackVersion);
ZIGBEE_EVENTCORE_FUNC(stackVersionReceived);
ZIGBEE_EVENTCORE_FUNC(NetworkChannelReceived);
ZIGBEE_EVENTCORE_FUNC(NetworkAddressReceived);
ZIGBEE_EVENTCORE_FUNC(requestNetworkChannel);
ZIGBEE_EVENTCORE_FUNC(requestNetworkAddress);
ZIGBEE_EVENTCORE_FUNC(requestPanId);
ZIGBEE_EVENTCORE_FUNC(panIdReceived);
ZIGBEE_EVENTCORE_FUNC(pongReceived);
ZIGBEE_EVENTCORE_FUNC(startTestSequence);

extern void SetZigBeeModuleUnusable(uint8_t unusable);

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
supportedStateMachineStates_t initSM_SupportedStates [] =
{
    /********************************************************/
    /*  initalization state Machine : Initialization state  */
    /********************************************************/
    {
        .stateId = ZBSM_INITIALSTATE,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_INITNETWORK, .eventCore = zigbeeModule_hardReset},
            {.eventId = ZBEVENT_WAITBOOTLOADER_TIMEOUT, .eventCore = uartConfig},
            {.eventId = ZBEVENT_MODULE_HAS_REBOOTED, .eventCore = pongReceived},
            {.eventId = ZBEVENT_INIT_IN_FACTORY_MODE, .eventCore = startTestSequence},
        },
        .numberOfSupportedEvents = 4,
        .stateTimeout.smTimeoutLength = BOOTLOADER_WAITING_DELAY,
        .stateTimeout.smTimeout_cb = waitBootloader,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /********************************************************/
    /*  initalization state Machine : wait module reset(pong)state  */
    /********************************************************/
    {
        .stateId = ZBSM_INIT_WAIT_MODULE_REBOOT,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_MODULE_HAS_REBOOTED, .eventCore = pongReceived},
            {.eventId = ZBEVENT_PONG_TIMEOUT, .eventCore = stackVersionReceivedTimeout},
        },
        .numberOfSupportedEvents = 2,
        .stateTimeout.smTimeoutLength = WAIT_PONG_RESPONSE_TIMEOUT,
        .stateTimeout.smTimeout_cb = waitPongRsp,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /********************************************************/
    /*  initalization state Machine : waiting stack version */
    /********************************************************/
    {
        .stateId = ZBSM_INIT_STACKVERSION,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_REQUESTSTACKVERSION, .eventCore = requestStackVersion},
            {.eventId = ZBEVENT_STACKVERSIONRECEIVED, .eventCore = stackVersionReceived},
            {.eventId = ZBEVENT_STACKVERSIONTIMEOUT, .eventCore = stackVersionReceivedTimeout},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = STACKVERSION_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = stackVersionTimer_cb,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /********************************************************/
    /*  initalization state Machine : waiting stack version */
    /********************************************************/
    {
        .stateId = ZBSM_INIT_NETWORKCHANNEL,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_REQUESTNETWORKCHANNEL, .eventCore = requestNetworkChannel},
            {.eventId = ZBEVENT_NETWORKCHANNELRECEIVED, .eventCore = NetworkChannelReceived},
            {.eventId = ZBEVENT_NETWORKCHANNELTIMEOUT, .eventCore = NetworkChannelReceived},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = NETWORKCHANNEL_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = networkChannelTimer_cb,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /**************************************************************/
    /*  initalization state Machine : waiting network own address */
    /**************************************************************/
    {
        .stateId = ZBSM_INIT_OWNNETWORKADDRESS,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_COMMISSIONINGSTATUSOK, .eventCore = requestNetworkAddress},
            {.eventId = ZBEVENT_REQUESTOWNNWKADDRESS, .eventCore = requestNetworkAddress},
            {.eventId = ZBEVENT_NETWORKADDRESSRECEIVED, .eventCore = NetworkAddressReceived},
            {.eventId = ZBEVENT_NETWORKADDRESSTIMEOUT, .eventCore = NetworkAddressTimeOut},
        },
        .numberOfSupportedEvents = 4,
        .stateTimeout.smTimeoutLength = NETWORKADDRESS_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = networkAddressTimer_cb,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*************************************************************/
    /*  initalization state Machine : waiting network PAN ID     */
    /*************************************************************/
    {
        .stateId = ZBSM_INIT_PANID,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_REQUESTPANID, .eventCore = requestPanId},
            {.eventId = ZBEVENT_PANIDRECEIVED, .eventCore = panIdReceived},
            {.eventId = ZBEVENT_PANIDTIMEOUT, .eventCore = panIdReceived},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = PANID_STATE_TIMEOUT,
        .stateTimeout.smTimeout_cb = panIdTimer_cb,
        .stateTimeout.smTimeoutHandler = NULL,
    },
};

stateMachineDefinition_t initializationStateMachine =
{
    .SM_Start = initStart,
    .SM_End = initEnd,
    .supportedStates = initSM_SupportedStates,
    .numberOfSupportedStates = sizeof(initSM_SupportedStates)/sizeof(supportedStateMachineStates_t),
    .initialState = ZBSM_INITIALSTATE,
    .stateMachineTimeout.smTimeoutLength = -1,
    .stateMachineTimeout.smTimeoutHandler = NULL,
    .stateMachineTimeout.smTimeout_cb = NULL,
    .parentStateMachine = NULL,
    .activeSubStateMachine = NULL,
    .sm_name = "Init. ",
};

static uint8_t hardResetCnt;
static uint8_t tryProgrammingRadioModule;

extern TimerHandle_t ZigBeePingTrigger;
extern TimerHandle_t ZigBeePingTimeout;
extern uint8_t ack_Module_Reset;

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
static void initStart(void)
{
    hardResetCnt = 0;
    tryProgrammingRadioModule = 0;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
static void initEnd(uint8_t forceEnd)
{
    if (0 != forceEnd)
    {
        if (0 != tryProgrammingRadioModule)
        {
            ZGB_PostEvents(ZBEVENT_UPDATEZIGBEEMODULE);
        }
        xTimerStop(ZigBeePingTrigger, 0);
        xTimerStop(ZigBeePingTimeout, 0);
        ack_Module_Reset = 0;


    }
    else
    {
        xTimerStart(ZigBeePingTimeout, 0);
        ZGB_PostEvents(ZBEVENT_STARTCOORDINATORMODE);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void waitBootloader(TimerHandle_t xTimer)
{
    ZGB_PostEvents(ZBEVENT_WAITBOOTLOADER_TIMEOUT);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void waitPongRsp(TimerHandle_t xTimer)
{
    ZGB_PostEvents(ZBEVENT_PONG_TIMEOUT);
}


/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(zigbeeModule_hardReset)
{
    ZigBeeModule_HardReset();

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(uartConfig)
{
    InitZigBeeInterface();
    if (0 != UART_Init())           //error during config.  Reset zigbee module
    {
        ZGB_PostEvents(ZBEVENT_INITNETWORK);
        if (INIT_MAXHARDRESET_COUNT > hardResetCnt++)
        {
            return SM_RESTARTSTATETIMEOUT;
        }
        else
        {
            tryProgrammingRadioModule = 1;
            return SM_TERMINATESTATEMACHINE;
        }
    }
    else
    {
        nextOp->nextState = ZBSM_INIT_WAIT_MODULE_REBOOT;
        xTimerStart(ZigBeePingTrigger, 0);

        return SM_SETNEXTSTATE;
    }
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(startTestSequence)
{
    nextOp->nextState = ZBSM_INIT_STACKVERSION;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_REQUESTSTACKVERSION);

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(requestStackVersion)
{
    ZbReadStackVersion();

    return SM_SUCCESS;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(stackVersionReceived)
{
    nextOp->nextState = ZBSM_INIT_OWNNETWORKADDRESS;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_REQUESTOWNNWKADDRESS);
    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(stackVersionReceivedTimeout)
{
    tryProgrammingRadioModule = 1;
    return SM_TERMINATESTATEMACHINE;
}


/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(requestNetworkChannel)
{
    ZbGetNetworkChannel();
    return SM_SUCCESS;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(NetworkChannelReceived)
{
    nextOp->nextState = ZBSM_INIT_PANID;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_REQUESTPANID);
    return SM_SETNEXTSTATE;
}


/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(requestNetworkAddress)
{
    ZbGetDeviceNetworkAddress();
    return SM_SUCCESS;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(NetworkAddressReceived)
{
    dbType_ZigBeeNetworkInfo_t nwInfo;

    DBTHRD_GetData(DBTYPE_ZIGBEE_NETWORK_INFO,(void*)&nwInfo, INDEX_DONT_CARE);

    if (0xffff == nwInfo.ShortAdd)
    {
        if (0 != getKeepSameNetwork())
        {
            dbType_ZigBeeNetworkInfo_t nwInfo;
            DBTHRD_GetData(DBTYPE_ZIGBEE_NETWORK_INFO,(void*)&nwInfo, INDEX_DONT_CARE);

            if (nwInfo.FrameCounter != 0)
            {
                ZbSetFrameCounter(nwInfo.FrameCounter);
            }
            if (nwInfo.PanID != 0)
            {
                ZbSetPanId(nwInfo.PanID);
            }
            ZbSetPrimaryChannelMask (nwInfo.ActiveChannel);
            ZbSetSecondaryChannelMask(0);
            ZBTHRD_KeepSameNetwork (0);
        }
        ZbFormNewNetwork();
        return (SM_RESTARTSTATETIMEOUT);
    }
    else
    {
        nextOp->nextState = ZBSM_INIT_NETWORKCHANNEL;
        nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_REQUESTNETWORKCHANNEL);
        return SM_SETNEXTSTATE;
    }
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(NetworkAddressTimeOut)
{
    return SM_TERMINATESTATEMACHINE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(requestPanId)
{
    ZbGetPanId();
    return SM_SUCCESS;
}


/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(panIdReceived)
{
    return SM_ENDSTATEMACHINE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(pongReceived)
{
    nextOp->nextState = ZBSM_INIT_STACKVERSION;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_REQUESTSTACKVERSION);
    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief  Callback if the Stack Version is not returned
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/04/13
*******************************************************************************/
static void stackVersionTimer_cb(TimerHandle_t xTimer)
{
    ZGB_PostEvents(ZBEVENT_STACKVERSIONTIMEOUT);
}

/*******************************************************************************
* @brief  Callback if the Pan Id is not returned
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/04/14
*******************************************************************************/
static void panIdTimer_cb(TimerHandle_t xTimer)
{
    ZGB_PostEvents(ZBEVENT_PANIDTIMEOUT);
}

/*******************************************************************************
* @brief  Callback if the network address is not returned
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/04/14
*******************************************************************************/
static void networkAddressTimer_cb(TimerHandle_t xTimer)
{
    ZGB_PostEvents(ZBEVENT_NETWORKADDRESSTIMEOUT);
}

/*******************************************************************************
* @brief  Callback if the channel # is not returned
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/04/14
*******************************************************************************/
static void networkChannelTimer_cb(TimerHandle_t xTimer)
{
    ZGB_PostEvents(ZBEVENT_NETWORKCHANNELTIMEOUT);
}


/*******************************************************************************
*    Public functions definitions
*******************************************************************************/



/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
