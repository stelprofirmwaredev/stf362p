/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    AddStatStateMachineDefinition.c
* @date    2017/06/09
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include ".\Application\inc\ZigBeeStateMachines.h"
#include ".\HAL\BitCloud\inc\HAL_OutgoingBitCloudMsg.h"




/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define ADD_STAT_SM_TIMEOUT         30000           //30sec
#define VERIFYKEY_TIMEOUT           2000            //2sec
#define UPDATEGROUP_TIMEOUT         3100            //3.1 sec; update group command
                                                    // will be repeated 3 times
                                                    // at 1 sec. interval

#define UPDATEGROUP_RETRY_MAX       3
#define MAX_CONFIG_RETRIES          3

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static void init(void);
static void end(uint8_t forceEnd);
static void addTstatTimer_cb(TimerHandle_t xTimer);
static void verifyKeyTimeout (TimerHandle_t xTimer);
static void updateGroupTimer_cb (TimerHandle_t xTimer);


ZIGBEE_EVENTCORE_FUNC(reconfigureTstat);
ZIGBEE_EVENTCORE_FUNC(getDeviceDescriptor);
ZIGBEE_EVENTCORE_FUNC(addStatTimeout);
ZIGBEE_EVENTCORE_FUNC(deviceDescriptorReceived);
ZIGBEE_EVENTCORE_FUNC(bindingClustersCompleted);
ZIGBEE_EVENTCORE_FUNC(updateAttributesCompleted);
ZIGBEE_EVENTCORE_FUNC(readingAttributesCompleted);
ZIGBEE_EVENTCORE_FUNC(configureReportingCompleted);
ZIGBEE_EVENTCORE_FUNC(updateGroupCompleted);
ZIGBEE_EVENTCORE_FUNC(sendUpdateGroupCommand);

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
supportedStateMachineStates_t addStatSM_SupportedStates [] =
{
    /********************************************************/
    /*  addStat state Machine : Initialization state        */
    /********************************************************/
    {
        .stateId = ZBSM_INITIALSTATE,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_VERIFYKEY_TIMEOUT, .eventCore = getDeviceDescriptor},
            {.eventId = ZBEVENT_RECONFIGURETSTAT, .eventCore = reconfigureTstat},
            {.eventId = ZBEVENT_VERIFYKEY, .eventCore = getDeviceDescriptor},
            {.eventId = ZBEVENT_FACTORYTESTDEVICE_JOINED, .eventCore = getDeviceDescriptor},
        },
        .numberOfSupportedEvents = 4,
        .stateTimeout.smTimeoutLength = VERIFYKEY_TIMEOUT,
        .stateTimeout.smTimeout_cb = verifyKeyTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /********************************************************/
    /*  addStat state Machine : wait device descriptor state*/
    /********************************************************/
    {
        .stateId = ZBSM_ADDSTAT_GETDEVICEDESCRIPTOR,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_DEVICEDESCRIPTOR_RECEIVED, .eventCore = deviceDescriptorReceived},
        },
        .numberOfSupportedEvents = 1,
        .stateTimeout.smTimeoutLength = SM_NO_TIMEOUT,
        .stateTimeout.smTimeout_cb = NULL,
        .stateTimeout.smTimeoutHandler = NULL,
    },


    /********************************************************/
    /*  addStat state Machine : binding clusters state     */
    /********************************************************/
    {
        .stateId = ZBSM_ADDSTAT_UPDATEGROUP,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_UPDATEGROUP, .eventCore = sendUpdateGroupCommand},
            {.eventId = ZBEVENT_UPDATEGROUP_COMPLETED, .eventCore = updateGroupCompleted},
            {.eventId = ZBEVENT_UPDATEGROUP_STATE_TIMEOUT, .eventCore = sendUpdateGroupCommand},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = UPDATEGROUP_TIMEOUT,
        .stateTimeout.smTimeout_cb = updateGroupTimer_cb,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /********************************************************/
    /*  addStat state Machine : binding clusters state     */
    /********************************************************/
    {
        .stateId = ZBSM_ADDSTAT_BINDINGCLUSTERS,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_BINDINGCLUSTER_COMPLETED, .eventCore = bindingClustersCompleted},
            {.eventId = ZBEVENT_BINDINGCLUSTER_SM_TIMEOUT, .eventCore = addStatTimeout},
        },
        .numberOfSupportedEvents = 2,
        .stateTimeout.smTimeoutLength = SM_NO_TIMEOUT,
        .stateTimeout.smTimeout_cb = NULL,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /********************************************************/
    /*  addStat state Machine : configure reporting state   */
    /********************************************************/
    {
        .stateId = ZBSM_ADDSTAT_CONFIGUREREPORTING,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_CONFIGUREREPORTING_COMPLETED, .eventCore = configureReportingCompleted},
            {.eventId = ZBEVENT_CONFIGUREREPORTING_SM_TIMEOUT, .eventCore = addStatTimeout},
        },
        .numberOfSupportedEvents = 2,
        .stateTimeout.smTimeoutLength = SM_NO_TIMEOUT,
        .stateTimeout.smTimeout_cb = NULL,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /********************************************************/
    /*  addStat state Machine : read attributes state     */
    /********************************************************/
    {
        .stateId = ZBSM_ADDSTAT_READATTRIBUTES,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_READATTRIBUTES_COMPLETED, .eventCore = readingAttributesCompleted},
            {.eventId = ZBEVENT_READATTRIBUTES_SM_TIMEOUT, .eventCore = addStatTimeout},
        },
        .numberOfSupportedEvents = 2,
        .stateTimeout.smTimeoutLength = SM_NO_TIMEOUT,
        .stateTimeout.smTimeout_cb = NULL,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /********************************************************/
    /*  addStat state Machine : update attributes state     */
    /********************************************************/
    {
        .stateId = ZBSM_ADDSTAT_UPDATEATTRIBUTES,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_UPDATEATTRIBUTES_COMPLETED, .eventCore = updateAttributesCompleted},
            {.eventId = ZBEVENT_UPDATEATTRIBUTES_SM_TIMEOUT, .eventCore = addStatTimeout},

        },
        .numberOfSupportedEvents = 2,
        .stateTimeout.smTimeoutLength = SM_NO_TIMEOUT,
        .stateTimeout.smTimeout_cb = NULL,
        .stateTimeout.smTimeoutHandler = NULL,
    },
};

stateMachineDefinition_t addStatStateMachine =
{
    .SM_Start = init,
    .SM_End = end,
    .supportedStates = addStatSM_SupportedStates,
    .numberOfSupportedStates = sizeof(addStatSM_SupportedStates)/sizeof(supportedStateMachineStates_t),
    .initialState = ZBSM_INITIALSTATE,
    .stateMachineTimeout.smTimeoutLength = ADD_STAT_SM_TIMEOUT,
    .stateMachineTimeout.smTimeout_cb = addTstatTimer_cb,
    .parentStateMachine = NULL,
    .activeSubStateMachine = NULL,
    .sm_name = "Add Stat",
};


extern uint8_t testStatus;

extern stateMachineDefinition_t DeviceDescriptorStateMachine;
extern stateMachineDefinition_t BindingClustersStateMachine;
extern stateMachineDefinition_t ReadAddtributesStateMachine;
extern stateMachineDefinition_t ConfigureReportingStateMachine;

extern zbDeviceToAdd_t currentZigBeeDevice;
static uint8_t idOfStatInReconfig;

static uint8_t groupUpdateRetryCnt;
static uint8_t addStatRetry  = 0;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/13
*******************************************************************************/
static void init(void)
{
    idOfStatInReconfig = -1;
    groupUpdateRetryCnt = 0;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/13
*******************************************************************************/
static void end(uint8_t forceEnd)
{
    ZGB_PostEventsWithStatId (ZBEVENT_ADDINGTSTATHASENDED, idOfStatInReconfig);
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/02/28
*******************************************************************************/
static void addTstatTimer_cb(TimerHandle_t xTimer)
{
    ZGB_PostEvents(ZBEVENT_ADDSTAT_STATE_TIMEOUT);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/02/28
*******************************************************************************/
static void updateGroupTimer_cb(TimerHandle_t xTimer)
{
    ZGB_PostEvents(ZBEVENT_UPDATEGROUP_STATE_TIMEOUT);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-François Many
* @date   2017/03/06
*******************************************************************************/
static void verifyKeyTimeout (TimerHandle_t xTimer)
{
    ZGB_PostEvents(ZBEVENT_VERIFYKEY_TIMEOUT);
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/19
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(reconfigureTstat)
{
    idOfStatInReconfig = event.statId;
    StartStateMachine (&DeviceDescriptorStateMachine,
                       ZBSM_SetEventWIdx(ZBEVENT_REQUESTSIMPLEDESCRIPTOR,event.statId));
    nextOp->nextState = ZBSM_ADDSTAT_GETDEVICEDESCRIPTOR;

    return SM_SETNEXTSTATE;
}


/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/13
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(getDeviceDescriptor)
{
    StartStateMachine (&DeviceDescriptorStateMachine, ZBSM_SetEvent(ZBEVENT_REQUESTSIMPLEDESCRIPTOR));

    testStatus = 1;

    (void)event;
    nextOp->nextState = ZBSM_ADDSTAT_GETDEVICEDESCRIPTOR;


    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/19
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(deviceDescriptorReceived)
{
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_UPDATEGROUP);
    nextOp->nextState = ZBSM_ADDSTAT_UPDATEGROUP;

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/19
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(updateGroupCompleted)
{
    StartStateMachine (&BindingClustersStateMachine, ZBSM_SetEvent(ZBEVENT_STARTCLUSTERBINDING));


    (void)event;
    nextOp->nextState = ZBSM_ADDSTAT_BINDINGCLUSTERS;


    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/19
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(sendUpdateGroupCommand)
{
    uint8_t statId;

    if (groupUpdateRetryCnt < UPDATEGROUP_RETRY_MAX)
    {

        if (DBERR_EXISTING_STAT == DBTHRD_GetData(DBTYPE_INDEX_FROM_ZBSHORTADDR,
                                                    (void *)&statId,
                                                    currentZigBeeDevice.zbDescriptor.shortAddress))
        {
            UpdateTstatGroupId(statId);
            groupUpdateRetryCnt++;
            return SM_RESTARTSTATETIMEOUT;
        }
        else
        {
            return SM_TERMINATESTATEMACHINE;
        }
    }
    else
    {
        return SM_TERMINATESTATEMACHINE;
    }
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/19
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(bindingClustersCompleted)
{
    StartStateMachine (&ConfigureReportingStateMachine, ZBSM_SetEvent(ZBEVENT_STARTCONFIGUREREPORTING));


    (void)event;
    nextOp->nextState = ZBSM_ADDSTAT_CONFIGUREREPORTING;


    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/19
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(configureReportingCompleted)
{
    StartStateMachine (&ReadAddtributesStateMachine, ZBSM_SetEvent(ZBEVENT_STARTREADATTRIBUTE));


    (void)event;
    nextOp->nextState = ZBSM_ADDSTAT_READATTRIBUTES;


    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/19
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(readingAttributesCompleted)
{
    (void)event;
    (void)nextOp;
    addStatRetry = 0;
    DBTHRD_SetData(DBTYPE_THERMOSTAT_FULLY_CONFIGURED,(void*)0, INDEX_DONT_CARE, DBCHANGE_LOCAL);

    return SM_ENDSTATEMACHINE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/19
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(updateAttributesCompleted)
{

    (void)event;
    (void)nextOp;


    return SM_ENDSTATEMACHINE;
}


/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/13
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(addStatTimeout)
{
    uint8_t stat_Id;

    addStatRetry++;
    if (addStatRetry >= MAX_CONFIG_RETRIES)
    {
        addStatRetry = 0;
        return SM_TERMINATESTATEMACHINE;
    }
    else
    {
        if (DBERR_EXISTING_STAT == DBTHRD_GetData(DBTYPE_INDEX_FROM_ZBSHORTADDR,
                                                    (void *)&stat_Id,
                                                    currentZigBeeDevice.zbDescriptor.shortAddress))
        {
            ZGB_PostEventsWithStatId(ZBEVENT_RECONFIGURE, stat_Id);
        }
        return SM_ENDSTATEMACHINE;
    }
}


/*******************************************************************************
*    Public functions definitions
*******************************************************************************/



/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
