/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    UpdateZigbeeModuleStateMachine.c
* @date    2017/07/011
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include ".\Application\inc\ZigBeeStateMachines.h"
#include ".\HAL\BitCloud\inc\HAL_OutgoingBitCloudMsg.h"
#include ".\HAL\inc\HAL_ZigBeeInterface.h"
#include "FlashOps.h"
#include "SREC_Utility.h"
#include ".\Console\inc\THREAD_Console.h"
#include ".\HAL\inc\HAL_Flash.h"
#include ".\ZigBee\inc\THREAD_ZigBee.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define HANDSHAKE_REQUEST_MAX_TIMEOUT           (30*1000)   //30 seconds
#define HANDSHAKE_REQUEST_REPEAT_PERIOD         (200)       //200ms
#define SREC_ACK_TIMEOUT                        (10*1000)   //10 seconds
#define WAIT_UPDATE_FINALIZE                    (1*1000)

#define ENTERBOOTLOADER_WAITING_DELAY                100         //100ms
#define MAX_HARDRESET_RESET                     3
#define FRAMECOUNTER_WAITING_DELAY              1000            //1sec
#define MAX_FRAMECOUNTER_GET_ATTEMPT            3

#define SREC_BUFFER_SIZE                64
#define BIN_BUFFER_SIZE                (SREC_BUFFER_SIZE/2)

#define SREC_HEADER_LENGTH              6           // type : 2bytes; length:2 bytes; \r\n:2bytes
#define SREC_TYPE_POS                   0           // {'S','X'} ; X can be 0-9
#define SREC_LENGTH_POS                 2           // {'2','3'} ; arbitrary exemple of 0x23
#define SREC_ADDRESS_POS                4           //
#define BIN_TYPE_POS                    0           // {'S','X'} ; X can be 0-9
#define BIN_LENGTH_POS                  2           //in binary, lenght is one byte
#define BIN_ADDRESS_POS                 3


typedef enum
{
    SREC_CONVERT_SUCCESS = 0,
    SREC_CONVERT_FAIL,
} srecConvertResult_t;

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
//static void stateTimeout(TimerHandle_t xTimer);
static void smTimeout(TimerHandle_t xTimer);
static void handShakeRepeatTimeout(TimerHandle_t xTimer);
static void srecAckTimeout(TimerHandle_t xTimer);
static void waitBootloader(TimerHandle_t xTimer);
static void finalizationTimeout(TimerHandle_t xTimer);
static void frameCounterTimeout(TimerHandle_t xTimer);

static void SmStart(void);
static void SmEnd(uint8_t forceEnd);

ZIGBEE_EVENTCORE_FUNC(handShakeRequest);
ZIGBEE_EVENTCORE_FUNC(handShakeConfirm);
ZIGBEE_EVENTCORE_FUNC(readZbImageRecord);
ZIGBEE_EVENTCORE_FUNC(writeRecordToModule);
ZIGBEE_EVENTCORE_FUNC(writerecordAcknowledge);
ZIGBEE_EVENTCORE_FUNC(abortUpdate);
ZIGBEE_EVENTCORE_FUNC(requestFrameCounter);
ZIGBEE_EVENTCORE_FUNC(FrameCounterReceived);
ZIGBEE_EVENTCORE_FUNC(zigbeeModule_hardReset);
ZIGBEE_EVENTCORE_FUNC(uartConfig);
ZIGBEE_EVENTCORE_FUNC(finalizeUpdate);


/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
supportedStateMachineStates_t UpdateZigbeeModuleSM_SupportedStates [] =
{
    /********************************************************/
    /*  initalization state Machine : Initialization state  */
    /********************************************************/
    {
        .stateId = ZBSM_INITIALSTATE,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_GETFRAMECOUNTER, .eventCore = requestFrameCounter},
            {.eventId = ZBEVENT_FRAMECOUNTER_RECEIVED, .eventCore = FrameCounterReceived},
            {.eventId = ZBEVENT_FRAMECOUNTER_TIMEOUT, .eventCore = requestFrameCounter},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = FRAMECOUNTER_WAITING_DELAY,
        .stateTimeout.smTimeout_cb = frameCounterTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /********************************************************/
    /*  Reset ZigBee module state                           */
    /********************************************************/
    {
        .stateId = ZBSM_UPDATEZB_RESET_MODULE,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_MODULEHARDRESET, .eventCore = zigbeeModule_hardReset},
            {.eventId = ZBEVENT_WAITBOOTLOADER_TIMEOUT, .eventCore = uartConfig},
        },
        .numberOfSupportedEvents = 2,
        .stateTimeout.smTimeoutLength = ENTERBOOTLOADER_WAITING_DELAY,
        .stateTimeout.smTimeout_cb = waitBootloader,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  update zigbee module state Machine : Initialization state    */
    /*****************************************************************/
    {
        .stateId = ZBSM_UPDATEZB_ENTER_BOOTLOADER,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_UPDATEZB_HANDSHAKE_REQUEST, .eventCore = handShakeRequest},
            {.eventId = ZBEVENT_UPDATEZB_HANDSHAKE_CONF, .eventCore = handShakeConfirm},
            {.eventId = ZBEVENT_UPDATEZB_HANDSHAKE_TIMEOUT, .eventCore = handShakeRequest},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = HANDSHAKE_REQUEST_REPEAT_PERIOD,
        .stateTimeout.smTimeout_cb = handShakeRepeatTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  update zigbee module state Machine : Initialization state    */
    /*****************************************************************/
    {
        .stateId = ZBSM_UPDATEZB_READ_RECORD_FROM_FLASH,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_UPDATEZB_READ_NEXT_RECORD_FROM_FLASH, .eventCore = readZbImageRecord},
        },
        .numberOfSupportedEvents = 1,
        .stateTimeout.smTimeoutLength = SM_NO_TIMEOUT,
        .stateTimeout.smTimeout_cb = NULL,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  update zigbee module state Machine : Initialization state    */
    /*****************************************************************/
    {
        .stateId = ZBSM_UPDATEZB_WRITE_RECORD_TO_MODULE,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_UPDATEZB_WRITE_RECORD_TO_MODULE, .eventCore = writeRecordToModule},
        },
        .numberOfSupportedEvents = 1,
        .stateTimeout.smTimeoutLength = SM_NO_TIMEOUT,
        .stateTimeout.smTimeout_cb = NULL,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  update zigbee module state Machine : Initialization state    */
    /*****************************************************************/
    {
        .stateId = ZBSM_UPDATEZB_WAIT_ACK,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_UPDATEZB_RECORD_ACK, .eventCore = writerecordAcknowledge},
            {.eventId = ZBEVENT_UPDATEZB_RECORD_NACK, .eventCore = abortUpdate},
            {.eventId = ZBEVENT_UPDATEZB_RECORD_TIMEOUT, .eventCore = abortUpdate},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = SREC_ACK_TIMEOUT,
        .stateTimeout.smTimeout_cb = srecAckTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  update zigbee module state Machine : wait update finalization state    */
    /*****************************************************************/
    {
        .stateId = ZBSM_WAIT_FINALIZE,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_FINALIZE_TIMEOUT, .eventCore = finalizeUpdate},
        },
        .numberOfSupportedEvents = 1,
        .stateTimeout.smTimeoutLength = WAIT_UPDATE_FINALIZE,
        .stateTimeout.smTimeout_cb = finalizationTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

};

stateMachineDefinition_t UpdateZigbeeModuleStateMachine =
{
    .SM_Start = SmStart,
    .SM_End = SmEnd,
    .supportedStates = UpdateZigbeeModuleSM_SupportedStates,
    .numberOfSupportedStates = sizeof(UpdateZigbeeModuleSM_SupportedStates)/sizeof(supportedStateMachineStates_t),
    .initialState = ZBSM_INITIALSTATE,
    .stateMachineTimeout.smTimeoutLength = HANDSHAKE_REQUEST_MAX_TIMEOUT,
    .stateMachineTimeout.smTimeout_cb = smTimeout,
    .parentStateMachine = NULL,
    .activeSubStateMachine = NULL,
    .sm_name = "Update ZB Module ",
};


static uint8_t inUpdateZbModule = 0;
static uint32_t zigBeeImage_FlashBaseAddress;
static uint32_t zigBeeImage_FlashOffset;
static uint8_t srecBuffer[SREC_BUFFER_SIZE];
struct BinaryRep
{
    uint8_t recordLength;
    uint8_t buffer[BIN_BUFFER_SIZE];
} binRep;

static uint8_t lastRecordReached;              //indicate a S8 record
static uint8_t hardResetCnt;
static uint8_t frameCounterGetAttempt;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
uint8_t asciiLookup(char asciiChar)
{
    const char lookup [16] = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};

    for (uint8_t i = 0; i < sizeof(lookup); i++)
    {
        if (lookup[i] == tolower(asciiChar))
        {
            return i;
        }
    }
    return 0;
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
uint8_t asciiToHex(char * asciiStr)
{
    uint8_t strLength;
    uint8_t hexValue = 0;

    strLength = strlen(asciiStr);

    for (uint8_t i = 0; i < strLength; i++)
    {
        uint8_t multiplier = (strLength-1-i)*16;

        hexValue += (multiplier > 0)?(asciiLookup(asciiStr[i])*multiplier):asciiLookup(asciiStr[i]);
    }

    return hexValue;
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
uint32_t convertToBinaryRecord (uint8_t* srecPtr)
{
    char recordType[3];
    char recordLength_ASCII[3];
    uint8_t recordLength_BIN;
    int32_t numSrecBytes = 0;

    memset (recordType, '\0', 3);
    memset (recordLength_ASCII, '\0', 3);

    strncpy (recordType,(char const *)(srecPtr+SREC_TYPE_POS), 2);
    strncpy (recordLength_ASCII,(char const *)(srecPtr+SREC_LENGTH_POS), 2);

    if ('S' == recordType[0])
    {
        switch (recordType[1])
        {
        case '7':
        case '8':
        case '9':
            lastRecordReached = 1;
            break;
        default:
            break;
        }

        recordLength_BIN = asciiToHex(recordLength_ASCII);

        memcpy (binRep.buffer, srecPtr, 2);   //copy record type ("Sx") integral
        binRep.buffer[2] = recordLength_BIN;     //then the record length in binary representation

        for (uint8_t i = 0; i < recordLength_BIN; i++)
        {
            char asciiVal [3];

            memset (asciiVal, '\0', 3);

            strncpy (asciiVal, (char const *)(srecPtr+(i*2)+SREC_ADDRESS_POS),2);
            binRep.buffer[i+BIN_ADDRESS_POS] = asciiToHex(asciiVal);
        }

        binRep.recordLength = recordLength_BIN;   //each byte represented as 2 ASCII character

        numSrecBytes = SREC_HEADER_LENGTH + recordLength_BIN*2;
    }

    return numSrecBytes;
}





/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void SmStart(void)
{
    InitZigBeeInterface();

    loaderStates_t bootLoaderState = GetBootLoaderState();

    switch (bootLoaderState)
    {
    case LOADER_STATE_1:
    case LOADER_STATE_2:
    case LOADER_STATE_7:
    case LOADER_STATE_8:
        zigBeeImage_FlashBaseAddress = EXTFLASH_ZIGBEE_IMAGE_A_START;
        break;

    case LOADER_STATE_3:
    case LOADER_STATE_4:
    case LOADER_STATE_5:
    case LOADER_STATE_6:
        zigBeeImage_FlashBaseAddress = EXTFLASH_ZIGBEE_IMAGE_B_START;
        break;

    default:
        LoaderStateCorrupted_ResetToFactory();
        break;
    }
    zigBeeImage_FlashOffset = 0;
    memset(srecBuffer,0,SREC_BUFFER_SIZE);
    lastRecordReached = 0;

//    inUpdateZbModule = 1;
    hardResetCnt = 0;
    frameCounterGetAttempt = 0;

    DBTHRD_SetData(DBTYPE_FORCE_ZIGBEE_UPDATE,(void*)0, INDEX_DONT_CARE, DBCHANGE_LOCAL);
    CONSOLE_EVENT_MESSAGE("ZigBee Update Started");
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void SmEnd(uint8_t forceEnd)
{
    inUpdateZbModule = 0;

    if (0 != forceEnd)
    {
        ZGB_PostEvents(ZBEVENT_UPDATEZB_UPDATE_FAILED);
    }
    else
    {
        ZGB_PostEvents(ZBEVENT_UPDATEZB_UPDATE_COMPLETED);
    }
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void srecAckTimeout(TimerHandle_t xTimer)
{
    ZGB_PostEvents(ZBEVENT_UPDATEZB_RECORD_TIMEOUT);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void finalizationTimeout(TimerHandle_t xTimer)
{
    ZGB_PostEvents(ZBEVENT_FINALIZE_TIMEOUT);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void waitBootloader(TimerHandle_t xTimer)
{
    ZGB_PostEvents(ZBEVENT_WAITBOOTLOADER_TIMEOUT);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void frameCounterTimeout(TimerHandle_t xTimer)
{
    ZGB_PostEvents(ZBEVENT_FRAMECOUNTER_TIMEOUT);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void handShakeRepeatTimeout(TimerHandle_t xTimer)
{
    ZGB_PostEvents(ZBEVENT_UPDATEZB_HANDSHAKE_TIMEOUT);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void smTimeout(TimerHandle_t xTimer)
{
    EndStateMachine(&UpdateZigbeeModuleStateMachine, SM_FORCE_END);
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(zigbeeModule_hardReset)
{
    inUpdateZbModule = 1;
    ZigBeeModule_HardReset();

    return SM_RESTARTSTATETIMEOUT;


//    nextOp->nextState = ZBSM_INIT_STACKVERSION;
//    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_REQUESTSTACKVERSION);
//
//    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author Jean-Fran�ois Many
* @date   2017/11/20
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(requestFrameCounter)
{
    if (frameCounterGetAttempt < MAX_FRAMECOUNTER_GET_ATTEMPT)
    {
        frameCounterGetAttempt++;
        ZbRequestFrameCounter();
        return SM_RESTARTSTATETIMEOUT;
    }
    else
    {
        nextOp->nextState = ZBSM_UPDATEZB_RESET_MODULE;
        nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_MODULEHARDRESET);

        return SM_SETNEXTSTATE;
    }
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author Jean-Fran�ois Many
* @date   2017/12/08
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(FrameCounterReceived)
{
    nextOp->nextState = ZBSM_UPDATEZB_RESET_MODULE;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_MODULEHARDRESET);

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(uartConfig)
{

    InitZigBeeInterface();
    if (0 != UART_Init())           //error during config.  Reset zigbee module
    {
        ZGB_PostEvents(ZBEVENT_MODULEHARDRESET);
        if (MAX_HARDRESET_RESET > hardResetCnt++)
        {
            return SM_RESTARTSTATETIMEOUT;
        }
        else
        {
            return SM_TERMINATESTATEMACHINE;
        }
    }
    else
    {
        nextOp->nextState = ZBSM_UPDATEZB_ENTER_BOOTLOADER;
        nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_UPDATEZB_HANDSHAKE_REQUEST);

        return SM_SETNEXTSTATE;
    }
}


/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(handShakeRequest)
{
    ZbBootLoaderHandShakeRequest();

    return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(handShakeConfirm)
{
    (void)event;


    nextOp->nextState = ZBSM_UPDATEZB_READ_RECORD_FROM_FLASH;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_UPDATEZB_READ_NEXT_RECORD_FROM_FLASH);

    StopStateMachineTimeout(&UpdateZigbeeModuleStateMachine);
    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/

#define DECRYPTED_BUFFER_SIZE 2*SREC_BUFFER_SIZE
#define NEW_BLOCK_IDX SREC_BUFFER_SIZE
static uint8_t decryptedSrecBuffer[DECRYPTED_BUFFER_SIZE];
static uint8_t nextSrecIdx = NEW_BLOCK_IDX;

ZIGBEE_EVENTCORE_FUNC(readZbImageRecord)
{
    (void)event;

    uint32_t bytesParsed = 0;
    uint32_t currentIdx = nextSrecIdx;
    uint16_t bytesLeft = DECRYPTED_BUFFER_SIZE - currentIdx;

    HAL_ReadFlashData (zigBeeImage_FlashBaseAddress+zigBeeImage_FlashOffset,
                        SREC_BUFFER_SIZE, srecBuffer);

    DecryptBlock(srecBuffer, decryptedSrecBuffer + NEW_BLOCK_IDX, SREC_BUFFER_SIZE);

    bytesParsed = convertToBinaryRecord(decryptedSrecBuffer+currentIdx);

    if( bytesParsed  != 0)
    {
        currentIdx += bytesParsed;
        bytesLeft = DECRYPTED_BUFFER_SIZE - currentIdx;

        if (IsSrecordComplete(decryptedSrecBuffer+currentIdx, bytesLeft) != SREC_OK)
        {
            // Since the current SRecord is not complete, copy the partial Srec
            // contents to the first half of decryptedSrecBuffer and parse from
            // there the next time this function is called.
            nextSrecIdx = NEW_BLOCK_IDX - bytesLeft;

            memcpy( decryptedSrecBuffer + nextSrecIdx,
                    decryptedSrecBuffer + currentIdx,
                    bytesLeft );

            zigBeeImage_FlashOffset += SREC_BUFFER_SIZE;
        }
        else
        {
            // Since the next srecord is complete in the current buffer, we don't
            // need to copy it to the buffer or read the next block.
            nextSrecIdx = currentIdx;
        }

        if(lastRecordReached == 1)
        {
            // Reset nextSrecIdx to its default value
            nextSrecIdx = NEW_BLOCK_IDX;
        }

        nextOp->nextState = ZBSM_UPDATEZB_WRITE_RECORD_TO_MODULE;
        nextOp->nextEvent = ZBSM_SetEvent (ZBEVENT_UPDATEZB_WRITE_RECORD_TO_MODULE);

        return SM_SETNEXTSTATE;
    }
    else
    {
        return SM_TERMINATESTATEMACHINE;
    }

}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(writeRecordToModule)
{
    ZbBootLoaderSendRecord (binRep.buffer, binRep.recordLength+3);

    nextOp->nextState = ZBSM_UPDATEZB_WAIT_ACK;

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(writerecordAcknowledge)
{

    if (0 == lastRecordReached)
    {
        nextOp->nextState = ZBSM_UPDATEZB_READ_RECORD_FROM_FLASH;
        nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_UPDATEZB_READ_NEXT_RECORD_FROM_FLASH);

        return SM_SETNEXTSTATE;
    }
    else
    {
        nextOp->nextState = ZBSM_WAIT_FINALIZE;
        ZBTHRD_KeepSameNetwork (1);

        return SM_SETNEXTSTATE;
    }
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(abortUpdate)
{
    (void)nextOp;
    (void)event;

    return SM_TERMINATESTATEMACHINE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(finalizeUpdate)
{
    (void)nextOp;
    (void)event;

    return SM_ENDSTATEMACHINE;
}


/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
uint8_t isInUpdateZbModule (void)
{
    return inUpdateZbModule;
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
