/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2017, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    ZigBee_ClustesrEmulation.c
* @date    2017/03/02
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <string.h>
#include <time.h>

#include ".\Application\inc\ZigBee_ClustersEmulation.h"
#include ".\Application\inc\ZigBeeStateMachines.h"
#include ".\HAL\inc\HAL_ZigBeeInterface.h"

#include ".\DB\inc\THREAD_DB.h"
#include ".\ZigBee\inc\THREAD_ZigBee.h"

#include ".\Console\inc\THREAD_Console.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
typedef enum
{
  ZCL_NO_LOCKOUT = 0x00,
  ZCL_LEVEL_1_LOCKOUT = 0x01,
  ZCL_LEVEL_2_LOCKOUT = 0x02,
  ZCL_LEVEL_3_LOCKOUT = 0x03,
  ZCL_LEVEL_4_LOCKOUT = 0x04,
  ZCL_LEVEL_5_LOCKOUT = 0x05
} ZCL_ThUiConfKeypadLockOut_t;

typedef enum
{
    TSTAT_WAITING   =   0,
    TSTAT_ALIVE,
    TSTAT_DISCONNECTED,
} zbStatConnectStatus_t;

typedef enum
{
    ZB_ALERT_WINDOW = 1,
    ZB_ALERT_OVERHEAT,
    ZB_ALERT_GFCI,
    ZB_ALERT_END_OF_LIFE,
    ZB_ALERT_FLOOR_TEMPERATURE,
    ZB_ALERT_RELAY_WEAR_OUT,
    ZB_ALERT_NO_FLOOR_SENSOR,
} zbAlertMapping_t;

#define OFFSET_1970_TO_2000     946684800   //Number of seconds between Jan 1 1970 and Jan 1 2000
/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
static uint8_t statIdToRemove = 0;
uint8_t ack_Module_Reset = 0;
zbStatConnectStatus_t TstatConnectionStatus[THERMOSTAT_INSTANCES];

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
extern TimerHandle_t ZigBeePingTimeout;

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static void Incoming_ThermostatClusterAttributes (uint8_t statIndex, uint16_t dataId, void * data);
static void Incoming_ThermostatUIConfClusterAttributes (uint8_t statIndex, uint16_t dataId, void * data);
static void Incoming_HumidityMeasurementClusterAttributes (uint8_t statIndex, uint16_t dataId, void * data);
static void Incoming_BasicClusterAttributes (uint8_t statIndex, uint16_t dataId, void * data);

static void ThermostatIsAlive(uint8_t tstat);


/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief Timer that expires when we must send a Ping to the ZigBee module
* @inputs xTimer: timer instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/03/10
*******************************************************************************/
void OnZigBeePingTrigger( TimerHandle_t xTimer )
{
    char strBuf[35];

    sprintf (strBuf, "ping %d\r\n", ack_Module_Reset);
    ack_Module_Reset = 0;
    SendZigBeeCmd(strBuf,0,ZB_INMSG_DONT_CARE);

    //If a ZigBee Uart error is detected, perform a UART_Init again
    if (isZigbeeUartInError() != 0)
    {
        UART_Init();
    }
}

/*******************************************************************************
* @brief Timer that expires if we did not receive the "pong" from the ZigBee module
* @inputs xTimer: timer instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/03/10
*******************************************************************************/
void OnZigBeePingTimeout( TimerHandle_t xTimer )
{
    vTracePrint(zbEventChannel, "Ping timeout");
    ZBTHRD_ReinitUart();
}

/*******************************************************************************
* @brief Timer that expires when we must check if a thermostat is disconnected
* @inputs xTimer: timer instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/02/07
*******************************************************************************/
void OnZigBeeTstatTimeout( TimerHandle_t xTimer )
{
    uint8_t tstat;
    uint8_t model[33];
    uint8_t alert;

    for (tstat = 1; tstat < THERMOSTAT_INSTANCES; tstat++)
    {
        DBTHRD_GetData(DBTYPE_TSTAT_MODEL, &model, tstat);
        if (model[0] != NULL)
        {
            //If thermostat did not give any sign of life, signal alert
            if (TstatConnectionStatus[tstat] == TSTAT_WAITING)
            {
                TstatConnectionStatus[tstat] = TSTAT_DISCONNECTED;
                alert = DB_SET_ALERT | DBALERT_NO_ZIGBEE;
                DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, tstat, DBCHANGE_LOCAL);
            }
            //If the thermostat is alive, good, continue to monitor
            else if (TstatConnectionStatus[tstat] == TSTAT_ALIVE)
            {
                TstatConnectionStatus[tstat] = TSTAT_WAITING;
            }
            else
            {
                //If thermostat is already disconnected, do nothing
            }
        }
    }
}

/*******************************************************************************
* @brief  Declare a thermostat alive (ZigBee working)
* @inputs tstat: Thermostat Index
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/02/07
*******************************************************************************/
static void ThermostatIsAlive(uint8_t tstat)
{
    uint8_t alert;

    if (tstat < THERMOSTAT_INSTANCES)
    {
        if (TstatConnectionStatus[tstat] == TSTAT_DISCONNECTED)
        {
            alert = DB_CLEAR_ALERT | DBALERT_NO_ZIGBEE;
            DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, tstat, DBCHANGE_LOCAL);
        }

        TstatConnectionStatus[tstat] = TSTAT_ALIVE;
    }
}



/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/03/02
*******************************************************************************/
#pragma optimize = none
static void Incoming_ThermostatClusterAttributes (uint8_t statIndex, uint16_t dataId, void * data)
{
    uint8_t floorMode;
    DBTHRD_GetData(DBTYPE_FLOOR_MODE, &floorMode, statIndex);

    switch (dataId)
    {
    /********************************************/
    /*          Local Temperature               */
    /********************************************/
    case ZBATTRIBUTE_LOCALTEMPERATURE:
        if (floorMode == CONTROLMODE_FLOOR)
        {
            DBTHRD_SetData(DBTYPE_FLOOR_TEMPERATURE,
                    data,
                    statIndex,
                    DBCHANGE_ZIGBEE);
        }
        else
        {
            DBTHRD_SetData(DBTYPE_AMBIENT_TEMPERATURE,
                    data,
                    statIndex,
                    DBCHANGE_ZIGBEE);
        }
        ThermostatIsAlive(statIndex);
        break;

    /********************************************/
    /*              Heat Setpoint               */
    /********************************************/
    case ZBATTRIBUTE_OCCHEATSETPOINT:
        if (floorMode == CONTROLMODE_FLOOR)
        {
            DBTHRD_SetData(DBTYPE_FLOOR_SETPOINT,
                data,
                statIndex,
                DBCHANGE_ZIGBEE);
        }
        else
        {
            DBTHRD_SetData(DBTYPE_AMBIENT_SETPOINT,
                    data,
                    statIndex,
                    DBCHANGE_ZIGBEE);
        }
        break;

    /********************************************/
    /*              PI Heat Demand              */
    /********************************************/
    case ZBATTRIBUTE_PIHEATDEMAND:
        DBTHRD_SetData(DBTYPE_HEAT_DEMAND,
                data,
                statIndex,
                DBCHANGE_ZIGBEE);
        break;

    /********************************************/
    /*     Power Consumption (Manuf. specif.)   */
    /********************************************/
    case ZBATTRIBUTE_POWERCONSUMPTION:
        DBTHRD_SetData(DBTYPE_LOCAL_DAILY_CONSUMPTION,
                data,
                statIndex,
                DBCHANGE_ZIGBEE);
        break;

    /********************************************/
    /*           Alerts (Manuf. specif.)        */
    /********************************************/
    case ZBATTRIBUTE_ALERT:
        {
            uint8_t * alert;
            uint8_t newAlert;
            dbType_Alerts_t currentAlert;
            uint8_t i;
            uint8_t alertInstance;
            uint8_t alert_id;

            alert = (uint8_t*)data;
            DBTHRD_GetData(DBTYPE_ACTIVE_ALERTS, (void*)&currentAlert, statIndex);
            for (i = 0; i < DBALERT_MAX_ALERT; i++)
            {
                switch (i)
                {
                    case ZB_ALERT_WINDOW:
                        alert_id = DBALERT_OPEN_WINDOW;
                        break;

                    case ZB_ALERT_OVERHEAT:
                        alert_id = DBALERT_OVERHEAT;
                        break;

                    case ZB_ALERT_GFCI:
                        alert_id = DBALERT_GFCI;
                        break;

                    case ZB_ALERT_END_OF_LIFE:
                        alert_id = DBALERT_END_OF_LIFE;
                        break;

                    case ZB_ALERT_FLOOR_TEMPERATURE:
                        alert_id = DBALERT_FLOOR_TEMP_LIMIT_REACHED;
                        break;

                    case ZB_ALERT_RELAY_WEAR_OUT:
                        alert_id = DBALERT_RELAYS_WEARING_OUT;
                        break;

                    case ZB_ALERT_NO_FLOOR_SENSOR:
                        alert_id = DBALERT_NO_FLOOR_SENSOR;
                        break;

                    default:
                        alert_id = DBALERT_NO_ALERT;
                        break;
                }
                newAlert = 0;
                if (*alert & (1<<i))
                {
                    newAlert = DB_SET_ALERT | alert_id;
                }
                else
                {
                    for (alertInstance = 0; alertInstance < ALERT_INSTANCES; alertInstance++)
                    {
                        if (currentAlert.ActiveAlerts[alertInstance] == alert_id)
                        {
                            newAlert = DB_CLEAR_ALERT | alert_id;
                        }
                    }
                }
                if (newAlert != 0)
                {
                    DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,
                        (void*)&newAlert,
                        statIndex,
                        DBCHANGE_ZIGBEE);
                }
            }
            break;
        }

    case ZBATTRIBUTE_FLOOR_MODE:
        DBTHRD_SetData(DBTYPE_FLOOR_MODE,
                data,
                statIndex,
                DBCHANGE_ZIGBEE);
        break;

    case ZBATTRIBUTE_RELAY_CYCLE_COUNT:
        DBTHRD_SetData(DBTYPE_RELAY_CYCLE_COUNT,
                data,
                statIndex,
                DBCHANGE_ZIGBEE);
        break;

    /********************************************/
    /********************************************/
    default:
        break;
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/03/02
*******************************************************************************/
static void Incoming_ThermostatUIConfClusterAttributes (uint8_t statIndex, uint16_t dataId, void * data)
{
    switch (dataId)
    {
    /********************************************/
    /*              Keypad lockout              */
    /********************************************/
    case ZBATTRIBUTE_KEYPADLOCKOUT:
        {
            Lock_t lock;
            ZCL_ThUiConfKeypadLockOut_t * systemLock;

            systemLock = (ZCL_ThUiConfKeypadLockOut_t*)data;
            if (*systemLock == ZCL_NO_LOCKOUT)
            {
                lock = LOCK_OFF;
            }
            else
            {
                lock = LOCK_ON;
            }
            DBTHRD_SetData(DBTYPE_LOCK_STATE,
                            (void*)&lock,
                            statIndex,
                            DBCHANGE_ZIGBEE);
        }
        break;

    /********************************************/
    /********************************************/
    default:
        break;
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/03/02
*******************************************************************************/
static void Incoming_HumidityMeasurementClusterAttributes (uint8_t statIndex, uint16_t dataId, void * data)
{
    switch (dataId)
    {
    /********************************************/
    /*          Humidity Measured value         */
    /********************************************/
    case ZBATTRIBUTE_LOCALHUMIDITY:
        {
            uint16_t * humidity_16;
            RelativeHumidity_t humidity;

            humidity_16 = (uint16_t*)data;
            humidity = (RelativeHumidity_t)(*humidity_16 / 100);
            DBTHRD_SetData(DBTYPE_AMBIENT_HUMIDITY,
                            (void*)&humidity,
                            statIndex,
                            DBCHANGE_ZIGBEE);
        }
        break;

    /********************************************/
    /********************************************/
    default:
        break;
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/03/02
*******************************************************************************/
static void Incoming_BasicClusterAttributes (uint8_t statIndex, uint16_t dataId, void * data)
{
    switch (dataId)
    {
    /*********************+**********************/
    /*              Model identifier            */
    /********************************************/
    case ZBATTRIBUTE_MODELID:
        DBTHRD_SetData(DBTYPE_TSTAT_MODEL,
                    data,
                    statIndex,
                    DBCHANGE_ZIGBEE);
        break;

    /*********************+**********************/
    /*            Location description          */
    /********************************************/
    case ZBATTRIBUTE_LOCATIONDESC:
        {
            char tmpBuffer[100];
            uint8_t str[NAME_SIZE_AND_NULL];

            if (IsStatInReconfiguration(statIndex) == 1)
            {
                sprintf((char*)tmpBuffer, "%s",(char const*)data);
            }
            else
            {
                sprintf((char*)tmpBuffer, "%s%02d",(char const*)data, statIndex);
            }
            if (strlen((const char*)tmpBuffer) > sizeof(str))
            {
                tmpBuffer[sizeof(str) - 1] = NULL;
            }
            sprintf((char*)str, tmpBuffer);
            DBTHRD_SetData(DBTYPE_TSTAT_NAME,
                    (void*)&str[0],
                    statIndex,
                    DBCHANGE_ZIGBEE);
        }
        break;

    /*********************+**********************/
    /*             Application version          */
    /********************************************/
    case ZBATTRIBUTE_APPLICATIONVERSION:
        DBTHRD_SetData(DBTYPE_TSTAT_VERSION,
                    data,
                    statIndex,
                    DBCHANGE_ZIGBEE);
        break;

    /*********************+**********************/
    /*               Stack version              */
    /********************************************/
    case ZBATTRIBUTE_STACKVERSION:
        DBTHRD_SetData(DBTYPE_ZIGBEE_VERSION,
                data,
                statIndex,
                DBCHANGE_ZIGBEE);
        break;

    /*********************+**********************/
    /*               Date code                  */
    /********************************************/
    case ZBATTRIBUTE_DATECODE:
        DBTHRD_SetData(DBTYPE_SERIAL_NUMBER,
                data,
                statIndex,
                DBCHANGE_ZIGBEE);
        break;

    /*********************+**********************/
    /********************************************/
    default:
        break;
    }
}



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/03/02
*******************************************************************************/
void ZigBeeIncomingAttributeMngmt (uint16_t shortAddress, uint16_t clusterId, uint16_t dataId, void * data)
{
    uint8_t statId;
    dbErr_t dbStatus;

    dbStatus = DBTHRD_GetData(DBTYPE_INDEX_FROM_ZBSHORTADDR, (void *)&statId, shortAddress);

    if (DBERR_EXISTING_STAT == dbStatus)
    {
        switch (clusterId)
        {
        /********************************************/
        /*              Thermostat Cluster          */
        /********************************************/
        case ZBCLUSTER_THERMOSTAT:
            Incoming_ThermostatClusterAttributes(statId, dataId, data);
            break;
        /********************************************/
        /*      Thermostat UI Conf Cluster          */
        /********************************************/
        case ZBCLUSTER_THERMOSTAT_UI_CONF:
            Incoming_ThermostatUIConfClusterAttributes(statId, dataId, data);
            break;
        /********************************************/
        /*        Humidity Measurement Cluster      */
        /********************************************/
        case ZBCLUSTER_HUMIDITYMEAS:
            Incoming_HumidityMeasurementClusterAttributes(statId, dataId, data);
            break;
        /********************************************/
        /*              Basic Cluster               */
        /********************************************/
        case ZBCLUSTER_BASIC:
            Incoming_BasicClusterAttributes(statId, dataId, data);
            break;

        /********************************************/
        /********************************************/
        default:
            break;
        }
    }
    else
    {
        CONSOLE_RUNTIME_MESSAGE("ShortAddress is unknown, make a rediscovery attempt");
        ZBTHRD_RediscoverZBStat(shortAddress);
    }
}



/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/03/09
*******************************************************************************/
void UpdateTstatGroupId (uint8_t tStatIdx)
{
    uint8_t groupId;
    zigBeeDescriptor_t zigBeeStat;

    if ((THIS_THERMOSTAT != tStatIdx) && (tStatIdx < THERMOSTAT_INSTANCES))
    {
        if (DBERR_OK == DBTHRD_GetData(DBTYPE_ZIGBEE_DESCRIPTOR, (void*)&zigBeeStat, tStatIdx))
        {
            if (DBERR_OK == DBTHRD_GetData(DBTYPE_TSTAT_PARENT_GROUPID, (void*)&groupId, tStatIdx))
            {
                char tmpBuffer[100];

//  {"removeAllGroups", "sdd", processRemoveAllGroupsCmd,"->Send Remove All Group command: removeAllGroup[addrMode][addr][ep]\r\n"},
//                sprintf(tmpBuffer,"removeAllGroups 0 %d %d\r\n",
//                            zigBeeStat.shortAddress,
//                            zigBeeStat.thermostatEndPoint);
//                SendZigBeeCmd(tmpBuffer,0,ZB_INMSG_DONT_CARE);
////  {"addGroup", "sddd", processAddGroupCmd, "->Send Add Group command: addGroup [addrMode][addr][ep][gid]\r\n"},
//                sprintf(tmpBuffer,"addGroup 0 %d %d %d\r\n",
//                            zigBeeStat.shortAddress,
//                            zigBeeStat.thermostatEndPoint,
//                            groupId+1);
//                SendZigBeeCmd(tmpBuffer,5,ZB_INMSG_ADD_GROUP_RESP_IND);
                sprintf(tmpBuffer,"updateGroup 0 %d %d %d\r\n",
                            zigBeeStat.shortAddress,
                            zigBeeStat.thermostatEndPoint,
                            groupId+1);
                SendZigBeeCmd(tmpBuffer,3,ZB_INMSG_ADD_GROUP_RESP_IND);


            }
        }
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/12/05
*******************************************************************************/
void SendOccupiedHeatGroupSetpointAttribute (uint8_t groupIdx)
{
    uint16_t setpoint;

    if (groupIdx < GROUP_INSTANCES)
    {
        if (DBERR_OK == DBTHRD_GetData(DBTYPE_GROUP_SETPOINT, (void*)&setpoint, groupIdx))
        {
            char tmpBuffer[100];


            //  {"writeAttribute", "sddddddd",
            //  [addrMode][addr][ep][clusterId][attrId][type][attrValue][attrSize]\r\n"},
            sprintf(tmpBuffer,"writeAttributeNoResp -g %d 25 0x0201 0x0012 0x29 %d 2\r\n",
                        groupIdx+1,
                        setpoint);
            SendZigBeeCmd(tmpBuffer,0,ZB_INMSG_DONT_CARE);
        }
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/12/05
*******************************************************************************/
void SendOccupiedHeatSetpointAttribute (uint8_t thermsotatIndex)
{
    uint16_t setpoint;
    uint8_t floorMode;
    zigBeeDescriptor_t zigBeeStat;

    if ((thermsotatIndex != THIS_THERMOSTAT) && (thermsotatIndex < THERMOSTAT_INSTANCES))
    {
        if (DBERR_OK == DBTHRD_GetData(DBTYPE_ZIGBEE_DESCRIPTOR, (void*)&zigBeeStat, thermsotatIndex))
        {
            char tmpBuffer[100];

            DBTHRD_GetData(DBTYPE_FLOOR_MODE, (void*)&floorMode, thermsotatIndex);
            if (floorMode == CONTROLMODE_FLOOR)
            {
                DBTHRD_GetData(DBTYPE_FLOOR_SETPOINT, (void*)&setpoint, thermsotatIndex);
            }
            else
            {
                DBTHRD_GetData(DBTYPE_AMBIENT_SETPOINT, (void*)&setpoint, thermsotatIndex);
            }

            //  {"writeAttribute", "sddddddd",
            //  [addrMode][addr][ep][clusterId][attrId][type][attrValue][attrSize]\r\n"},
            sprintf(tmpBuffer,"writeAttribute 0 %d %d 0x0201 0x0012 0x29 %d 2\r\n",
                        zigBeeStat.shortAddress,
                        zigBeeStat.thermostatEndPoint,
                        setpoint);

            /* TODO : Format a response message for write attribute  */
            SendZigBeeCmd(tmpBuffer,3,ZB_INMSG_WRITE_ATTR_RESP);
        }
    }
}

/*******************************************************************************
* @brief  Send the Temperature Display Mode attribute to all devices
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/18
*******************************************************************************/
void SendTemperatureDisplayModeAttribute(void)
{
    uint8_t format;
    char tmpBuffer[100];


    DBTHRD_GetData(DBTYPE_TEMPERATURE_FORMAT, (void*)&format, INDEX_DONT_CARE);

    //  {"writeAttribute", "sddddddd",
    //  [addrMode][addr][ep][clusterId][attrId][type][attrValue][attrSize]\r\n"},
    sprintf(tmpBuffer,"writeAttributeNoResp 0 0xffff 0xff 0x0204 0x0000 0x30 %d 1\r\n",
                format);
    SendZigBeeCmd(tmpBuffer,0,ZB_INMSG_DONT_CARE);

}

/*******************************************************************************
* @brief  Send the Outdoor Temperature attribute to all devices
* @inputs update: specify if we only want to refresh the internal value or broadcast it
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/18
*******************************************************************************/
void SendOutdoorTemperatureAttribute(uint8_t update)
{
    int16_t temperature;
    char tmpBuffer[100];

    DBTHRD_GetData(DBTYPE_OUTDOOR_TEMPERATURE, (void*)&temperature, INDEX_DONT_CARE);

    //  {"writeLocalAttribute", "dddddd",
    //  [ep][clusterId][direction][attrId][type][value]\r\n"},
    sprintf(tmpBuffer,"writeLocalAttribute 20 0x0402 0 0 0x29 %d\r\n",
                temperature);

    /* TODO : Format a response message for this write attribute  */
    SendZigBeeCmd(tmpBuffer,0,ZB_INMSG_DONT_CARE);

    if (update == BROADCAST_UPDATE)
    {
        zigBeeDescriptor_t zigBeeStat;

        for (uint8_t i = 1; i < THERMOSTAT_INSTANCES; i++)
        {
            if (DBERR_OK == DBTHRD_GetData(DBTYPE_ZIGBEE_DESCRIPTOR, (void*)&zigBeeStat, i))
            {
                //  {"reportAttribute", "sdddddddd",
                //  [addrMode][addr][ep][clusterId][attrId][type][attrValue][attrSize][direction]\r\n"},
                sprintf(tmpBuffer,"reportAttribute 0 %d %d 0x0402 0x0000 0x29 %d 2 1\r\n",
                        zigBeeStat.shortAddress,
                        zigBeeStat.thermostatEndPoint,
                        temperature);

                SendZigBeeCmd(tmpBuffer,3,ZB_INMSG_WRITE_ATTR_RESP);
            }
        }
    }
}

/*******************************************************************************
* @brief  Send the Keypad Lockout attribute to the specified thermostat
* @inputs thermsotatIndex: specified thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/18
*******************************************************************************/
void SendKeypadLockoutAttribute(uint8_t thermsotatIndex)
{
    Lock_t lock;
    ZCL_ThUiConfKeypadLockOut_t systemLock;
    zigBeeDescriptor_t zigBeeStat;

    if ((thermsotatIndex != THIS_THERMOSTAT) && (thermsotatIndex < THERMOSTAT_INSTANCES))
    {
        if (DBERR_OK == DBTHRD_GetData(DBTYPE_ZIGBEE_DESCRIPTOR, (void*)&zigBeeStat, thermsotatIndex))
        {
            char tmpBuffer[100];


            DBTHRD_GetData(DBTYPE_LOCK_STATE, (void*)&lock, thermsotatIndex);
            if (lock == LOCK_ON)
            {
                systemLock = ZCL_LEVEL_1_LOCKOUT;
            }
            else
            {
                systemLock = ZCL_NO_LOCKOUT;
            }
            //  {"writeAttribute", "sddddddd",
            //  [addrMode][addr][ep][clusterId][attrId][type][attrValue][attrSize]\r\n"},
            sprintf(tmpBuffer,"writeAttribute 0 %d %d 0x0204 0x0001 0x30 %d 1\r\n",
                        zigBeeStat.shortAddress,
                        zigBeeStat.thermostatEndPoint,
                        systemLock);

            SendZigBeeCmd(tmpBuffer,3,ZB_INMSG_WRITE_ATTR_RESP);
        }
    }
}

/*******************************************************************************
* @brief  Send the Keypad Lockout attribute to the specified thermostat
* @inputs thermsotatIndex: specified thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/18
*******************************************************************************/
void SendSystemKeypadLockoutAttribute(void)
{
    Lock_t lock;
    ZCL_ThUiConfKeypadLockOut_t systemLock;
    zigBeeDescriptor_t zigBeeStat;

    char tmpBuffer[100];


    DBTHRD_GetData(DBTYPE_SYSTEMLOCK, (void*)&lock, INDEX_DONT_CARE);
    if (lock == LOCK_ON)
    {
        systemLock = ZCL_LEVEL_1_LOCKOUT;
    }
    else
    {
        systemLock = ZCL_NO_LOCKOUT;
    }

    for (uint8_t i = 1; i < THERMOSTAT_INSTANCES; i++)
    {
        if (DBERR_OK == DBTHRD_GetData(DBTYPE_ZIGBEE_DESCRIPTOR, (void*)&zigBeeStat, i))
        {
            //  {"writeAttribute", "sddddddd",
            //  [addrMode][addr][ep][clusterId][attrId][type][attrValue][attrSize]\r\n"},
            sprintf(tmpBuffer,"writeAttribute 0 %d %d 0x0204 0x0001 0x30 %d 1\r\n",
                        zigBeeStat.shortAddress,
                        zigBeeStat.thermostatEndPoint,
                        systemLock);

            SendZigBeeCmd(tmpBuffer,3,ZB_INMSG_WRITE_ATTR_RESP);
        }
    }
}
/*******************************************************************************
* @brief  Send the Location Description attribute to the specified thermostat
* @inputs thermsotatIndex: specified thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/18
*******************************************************************************/
void SendLocationDescriptionAttribute(uint8_t thermsotatIndex)
{
    uint8_t name[NAME_SIZE_AND_NULL];

    char tmpBuffer[100];
    zigBeeDescriptor_t zigBeeStat;

    if ((thermsotatIndex != THIS_THERMOSTAT) && (thermsotatIndex < THERMOSTAT_INSTANCES))
    {
        if (DBERR_OK == DBTHRD_GetData(DBTYPE_ZIGBEE_DESCRIPTOR, (void*)&zigBeeStat, thermsotatIndex))
        {
            DBTHRD_GetData(DBTYPE_TSTAT_NAME_UTF8, (void*)&name, thermsotatIndex);
            if (strlen((const char*)name) > 15)
            {
                name[15] = 0;
            }
            //  {"writeAttributeString", "sdddddsd",
            //  [addrMode][addr][ep][clusterId][attrId][type][attrString][attrSize]\r\n"},
            sprintf(tmpBuffer,"writeAttributeString 0 %d %d 0x0000 0x0010 0x42 \"%s\" %d\r\n",
                        zigBeeStat.shortAddress,
                        zigBeeStat.thermostatEndPoint,
                        name,
                        strlen((char const*)name));
            SendZigBeeCmd(tmpBuffer,0,ZB_INMSG_DONT_CARE);
        }
    }
}

/*******************************************************************************
* @brief  Send the Time Format attribute to all devices
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/18
*******************************************************************************/
void SendTimeFormatAttribute(void)
{
    uint8_t format;
    char tmpBuffer[100];

    DBTHRD_GetData(DBTYPE_TIME_FORMAT, (void*)&format, INDEX_DONT_CARE);

    //  {"writeLocalAttribute", "dddddd",
    //  [ep][clusterId][direction][attrId][type][value]\r\n"},
    sprintf(tmpBuffer,"writeLocalAttribute 20 0x000a 0 0x4001 0x30 %d\r\n",
                format);

    /* TODO : Format a response message for this write attribute  */
    SendZigBeeCmd(tmpBuffer,0,ZB_INMSG_DONT_CARE);

    //  {"reportAttribute", "sdddddddd",
    //  [addrMode][addr][ep][clusterId][attrId][type][attrValue][attrSize][direction]\r\n"},
    sprintf(tmpBuffer,"reportAttribute 0 0xffff 0xff 0x000a 0x4001 0x30 %d 1 1\r\n",
                format);
    SendZigBeeCmd(tmpBuffer,0,ZB_INMSG_DONT_CARE);
}

/*******************************************************************************
* @brief  Send the Local Time attribute to all devices
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/18
*******************************************************************************/
void SendTimeAttribute(void)
{
    char tmpBuffer[100];
    dbType_Time_t dbTime;
    dbType_Date_t dbDate;
    struct tm currentTime;
    time_t currentFrom1970Time;
    time_t currentFrom2000Time;


    DBTHRD_GetData(DBTYPE_LOCALTIME, (void*)&dbTime, INDEX_DONT_CARE);
    DBTHRD_GetData(DBTYPE_DATE, (void*)&dbDate, INDEX_DONT_CARE);

    currentTime.tm_sec = dbTime.Seconds;
    currentTime.tm_min = dbTime.Minutes;
    currentTime.tm_hour = dbTime.Hours;
    currentTime.tm_mday = dbDate.Day;
    currentTime.tm_mon = dbDate.Month - 1;
    currentTime.tm_year = dbDate.Year - 1900;
    currentTime.tm_wday = dbDate.DayOfWeek;
    currentTime.tm_yday = dbDate.DayOfYear;
    currentTime.tm_isdst = 0;

    currentFrom1970Time = mktime(&currentTime);

    currentFrom2000Time = currentFrom1970Time - OFFSET_1970_TO_2000;

    //  {"writeLocalAttribute", "dddddd",
    //  [ep][clusterId][direction][attrId][type][value]\r\n"},
    sprintf(tmpBuffer,"writeLocalAttribute 20 0x000a 0 0x0007 0xe2 %d\r\n",
                currentFrom2000Time);

    /* TODO : format a response message for this write attribute  */
    SendZigBeeCmd(tmpBuffer,0,ZB_INMSG_DONT_CARE);

    //  {"reportAttribute", "sdddddddd",
    //  [addrMode][addr][ep][clusterId][attrId][type][attrValue][attrSize][direction]\r\n"},
    sprintf(tmpBuffer,"reportAttribute 0 0xffff 0xff 0x000a 0x0007 0xe2 %d 4 1\r\n",
                currentFrom2000Time);
    SendZigBeeCmd(tmpBuffer,0,ZB_INMSG_DONT_CARE);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/03/06
*******************************************************************************/
uint8_t tstatNetworkLeaveReq (uint8_t tstatId)
{
    zigBeeDescriptor_t zigBeeStat;

    if ((tstatId != THIS_THERMOSTAT) && (tstatId < THERMOSTAT_INSTANCES))
    {
        if (DBERR_OK == DBTHRD_GetData(DBTYPE_ZIGBEE_DESCRIPTOR, (void*)&zigBeeStat, tstatId))
        {
            char tmpBuffer[100];

            statIdToRemove = tstatId;

            sprintf(tmpBuffer,"sendMgmtLeaveReq %d %d %d 0 0\r\n",
                    zigBeeStat.shortAddress,
                    (uint32_t)((zigBeeStat.extAddress&0xffffffff00000000)>>32),
                    (uint32_t)(zigBeeStat.extAddress&0x00000000ffffffff));

            SendZigBeeCmd(tmpBuffer,5,ZB_INMSG_LEAVE_RESP);
            return 0;
        }
        return 1;
    }
    return -1;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval uint8_t stat id to remove
* @author Jean-Fran�ois Many
* @date   2017/05/18
*******************************************************************************/
uint8_t GetStatIdToRemove(void)
{
    return statIdToRemove;
}

/*******************************************************************************
* @brief  Send a request to receive the extended address associated with
*         the sent short address
* @inputs shortAddress: short address for which we seek the extended address
* @retval None
* @author Jean-Fran�ois Many
* @date   2018/01/30
*******************************************************************************/
void SendIeeeAddrReq(uint16_t shortAddress)
{
    char tmpBuffer[100];

    //ieeeAddrReq [dstAddr] [nwkAddOfInt] [reqType]
    sprintf(tmpBuffer,"ieeeAddrReq %d %d %d\r\n",
                            shortAddress,
                            shortAddress,
                            0); //(SINGLE_RESPONSE_REQUESTTYPE = 0)

    SendZigBeeCmd(tmpBuffer,5,ZB_INMSG_IEEE_ADDR_RESP);
}


/** Copyright(C) 2017 Stelpro Design, All Rights Reserved**/
