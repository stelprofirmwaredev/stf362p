/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    FactoryTestStateMachine.c
* @date    2017/06/29
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include ".\Application\inc\ZigBeeStateMachines.h"
#include ".\HAL\BitCloud\inc\HAL_OutgoingBitCloudMsg.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define SETCHANNELMASK_TIMEOUT      (3*1000)    // in ms

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static void start (void);
static void channelMaskTimeout (TimerHandle_t xTimer);

ZIGBEE_EVENTCORE_FUNC(destroyNwk);
ZIGBEE_EVENTCORE_FUNC(nwkDestroyed);
ZIGBEE_EVENTCORE_FUNC(requestStackVersion);
ZIGBEE_EVENTCORE_FUNC(stackVersionReceived);
ZIGBEE_EVENTCORE_FUNC(setPrimaryChannelMask);
ZIGBEE_EVENTCORE_FUNC(setPrimaryChannelMaskTimeout);
ZIGBEE_EVENTCORE_FUNC(setSecondaryChannelMaskTimeout);
ZIGBEE_EVENTCORE_FUNC(setSecondaryChannelMask);
ZIGBEE_EVENTCORE_FUNC(startAddBenchTestDevice);
ZIGBEE_EVENTCORE_FUNC(deviceAnnce);

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
supportedStateMachineStates_t FactoryTestSM_SupportedStates [] =
{
    /*****************************************************************/
    /*  factory test state Machine : Initialization state            */
    /*****************************************************************/
    {
        .stateId = ZBSM_TESTMODE_DESTROYCURRENTNETWORK,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_DESTROYCURRENTNETWORK, .eventCore = destroyNwk},
            {.eventId = ZBEVENT_NETWORKDESTROYED, .eventCore = nwkDestroyed},
        },
        .numberOfSupportedEvents = 2,
        .stateTimeout.smTimeoutLength = SM_NO_TIMEOUT,
        .stateTimeout.smTimeout_cb = NULL,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  factory test state Machine : stack version state            */
    /*****************************************************************/
    {
        .stateId = ZBSM_INIT_STACKVERSION,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_REQUESTSTACKVERSION, .eventCore = requestStackVersion},
            {.eventId = ZBEVENT_STACKVERSIONRECEIVED, .eventCore = stackVersionReceived},
            {.eventId = ZBEVENT_STACKVERSIONTIMEOUT, .eventCore = stackVersionReceived},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = SM_NO_TIMEOUT,
        .stateTimeout.smTimeout_cb = NULL,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  factory test state Machine : primary channel mask state      */
    /*****************************************************************/
    {
        .stateId = ZBSM_TESTMODE_SETPRIMARYCHANNELMASK,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_SETPRIMARYCHANNELMASK, .eventCore = setPrimaryChannelMask},
            {.eventId = ZBEVENT_CHANNELMASK_STATE_TIMEOUT, .eventCore = setPrimaryChannelMaskTimeout},
            {.eventId = ZBEVENT_SETPRIMARYCHANNELMASK_IND, .eventCore = setPrimaryChannelMaskTimeout},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = SETCHANNELMASK_TIMEOUT,
        .stateTimeout.smTimeout_cb = channelMaskTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  factory test state Machine : secondary channel mask state    */
    /*****************************************************************/
    {
        .stateId = ZBSM_TESTMODE_SETSECONDARYCHANNELMASK,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_SETSECONDARYCHANNELMASK, .eventCore = setSecondaryChannelMask},
            {.eventId = ZBEVENT_CHANNELMASK_STATE_TIMEOUT, .eventCore = setSecondaryChannelMaskTimeout},
            {.eventId = ZBEVENT_SETSECONDARYCHANNELMASK_IND, .eventCore = setSecondaryChannelMaskTimeout},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = SETCHANNELMASK_TIMEOUT,
        .stateTimeout.smTimeout_cb = channelMaskTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  factory test state Machine : create network state            */
    /*****************************************************************/
    {
        .stateId = ZBSM_TESTMODE_JOINNETWORK,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTCOORDINATORMODE, .eventCore = startAddBenchTestDevice},
        },
        .numberOfSupportedEvents = 1,
        .stateTimeout.smTimeoutLength = SM_NO_TIMEOUT,
        .stateTimeout.smTimeout_cb = NULL,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /*****************************************************************/
    /*  factory test state Machine : run in addbenchtest mode   */
    /*****************************************************************/
    {
        .stateId = ZBSM_TESTMODE_ADDBENCHTESTDEVICE,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_DEVICEANNCERECEIVED, .eventCore = deviceAnnce},
        },
        .numberOfSupportedEvents = 1,
        .stateTimeout.smTimeoutLength = SM_NO_TIMEOUT,
        .stateTimeout.smTimeout_cb = NULL,
        .stateTimeout.smTimeoutHandler = NULL,
    },

};

stateMachineDefinition_t FactoryTestStateMachine =
{
    .SM_Start = start,
    .SM_End = NULL,
    .supportedStates = FactoryTestSM_SupportedStates,
    .numberOfSupportedStates = sizeof(FactoryTestSM_SupportedStates)/sizeof(supportedStateMachineStates_t),
    .initialState = ZBSM_TESTMODE_DESTROYCURRENTNETWORK,
    .stateMachineTimeout.smTimeoutLength = SM_NO_TIMEOUT,
    .stateMachineTimeout.smTimeout_cb = NULL,
    .parentStateMachine = NULL,
    .activeSubStateMachine = NULL,
    .sm_name = "Factory Test",
};

static uint8_t testModeChannel;
extern stateMachineDefinition_t initializationStateMachine;
extern stateMachineDefinition_t addStatStateMachine;
extern stateMachineDefinition_t DestroyZbNwkStateMachine;

extern uint8_t testStatus;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/29
*******************************************************************************/
static void start (void)
{
    testModeChannel = 0;
    testStatus = 0xFF;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(requestStackVersion)
{
    ZbReadStackVersion();
    return SM_SUCCESS;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(stackVersionReceived)
{
    nextOp->nextState = ZBSM_TESTMODE_SETPRIMARYCHANNELMASK;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_SETPRIMARYCHANNELMASK);

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/29
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(setPrimaryChannelMask)
{
    ZbSetPrimaryChannelMask (testModeChannel);

    return SM_SUCCESS;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/29
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(setPrimaryChannelMaskTimeout)
{
    nextOp->nextState = ZBSM_TESTMODE_SETSECONDARYCHANNELMASK;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_SETSECONDARYCHANNELMASK) ;

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/29
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(setSecondaryChannelMask)
{
    ZbSetSecondaryChannelMask (0);

    return SM_SUCCESS;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/29
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(setSecondaryChannelMaskTimeout)
{
    StartStateMachine (&initializationStateMachine, ZBSM_SetEvent(ZBEVENT_INIT_IN_FACTORY_MODE));
    nextOp->nextState = ZBSM_TESTMODE_JOINNETWORK;

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/29
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(startAddBenchTestDevice)
{
    ZbNetworkSteering();
    nextOp->nextState = ZBSM_TESTMODE_ADDBENCHTESTDEVICE;

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/03/06
*******************************************************************************/
static void channelMaskTimeout (TimerHandle_t xTimer)
{
    ZGB_PostEvents(ZBEVENT_CHANNELMASK_STATE_TIMEOUT);
}


/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(deviceAnnce)
{
    StartStateMachine (&addStatStateMachine, ZBSM_SetEvent(ZBEVENT_FACTORYTESTDEVICE_JOINED));
    (void)event;
    (void)nextOp;


    return SM_SUCCESS;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(destroyNwk)
{
    StartStateMachine (&DestroyZbNwkStateMachine, ZBSM_SetEvent(ZBEVENT_REMOVESTAT));

    return SM_SUCCESS;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(nwkDestroyed)
{
    nextOp->nextState = ZBSM_INIT_STACKVERSION;
    nextOp->nextEvent = ZBSM_SetEvent(ZBEVENT_REQUESTSTACKVERSION);

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/06/29
*******************************************************************************/
void SetTestModeChannel(uint8_t channel)
{
    testModeChannel = channel;
}




/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
