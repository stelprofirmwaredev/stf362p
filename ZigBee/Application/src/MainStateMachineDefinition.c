/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    MainStateMachineDefinition.c
* @date    2017/06/09
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include ".\Application\inc\ZigBeeStateMachines.h"
#include ".\HAL\BitCloud\inc\HAL_OutgoingBitCloudMsg.h"
#include ".\HAL\inc\HAL_ZigBeeInterface.h"

#include ".\DB\classes\inc\class_Thermostats.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define PERMITJOIN_TIMEOUT      175000          //175000 ms = 175 sec = 2min 55 sec.
#define MAIN_REMOVE_STAT_TIMEOUT    1000            //1 sec





/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static void MainSmStart (void);
static void MainSmEnd (uint8_t forceEnd);
static void OnPermitJoinTimeout (TimerHandle_t xTimer);
//static void initialStateTimeout (TimerHandle_t xTimer);
static void removeTstatTimeout (TimerHandle_t xTimer);


ZIGBEE_EVENTCORE_FUNC(mainInitialization);
ZIGBEE_EVENTCORE_FUNC(startCoordinator);
ZIGBEE_EVENTCORE_FUNC(addTstat);
ZIGBEE_EVENTCORE_FUNC(joinPermitTimeout);
ZIGBEE_EVENTCORE_FUNC(config_new_device);
ZIGBEE_EVENTCORE_FUNC(addingTstatHasEnded);
ZIGBEE_EVENTCORE_FUNC(abortAddStatExecution);
ZIGBEE_EVENTCORE_FUNC(stopAddingTstat);
ZIGBEE_EVENTCORE_FUNC(reconfigureTstat);
ZIGBEE_EVENTCORE_FUNC(tstatReconfigHasEnded);
ZIGBEE_EVENTCORE_FUNC(sendLeaveRequest);
ZIGBEE_EVENTCORE_FUNC(LeaveRsp);

uint8_t isZigBeeModuleUnusable(void);
/*******************************************************************************
*    Private variables definitions
*******************************************************************************/

supportedStateMachineStates_t mainSM_SupportedStates [] =
{
    /****************************************************/
    /*  Main state Machine : Initialization state       */
    /****************************************************/
    {
        .stateId = ZBSM_INITIALSTATE,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTINITIALIZATION, .eventCore = mainInitialization},
            {.eventId = ZBEVENT_NETWORKDESTROYED, .eventCore = mainInitialization},
        },
        .numberOfSupportedEvents = 2,
        .stateTimeout.smTimeoutLength = SM_NO_TIMEOUT,
        .stateTimeout.smTimeout_cb = NULL/*initialStateTimeout*/,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /****************************************************/
    /*  Main state Machine : Joining network            */
    /****************************************************/
    {
        .stateId = ZBSM_MAIN_JOINING_NETWORK,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_STARTCOORDINATORMODE, .eventCore = startCoordinator},
        },
        .numberOfSupportedEvents = 1,
        .stateTimeout.smTimeoutLength = SM_NO_TIMEOUT,
        .stateTimeout.smTimeout_cb = NULL,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /****************************************************/
    /*  Main state Machine : running in coordinator mode*/
    /****************************************************/
    {
        .stateId = ZBSM_MAIN_RUNNING_COORDINATOR,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_ADDTSTAT, .eventCore = addTstat},
            {.eventId = ZBEVENT_RECONFIGURE, .eventCore = reconfigureTstat},
            {.eventId = ZBEVENT_REMOVESTAT, .eventCore = sendLeaveRequest},
        },
        .numberOfSupportedEvents = 3,
        .stateTimeout.smTimeoutLength = SM_NO_TIMEOUT,
        .stateTimeout.smTimeout_cb = NULL,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /****************************************************/
    /*  Main state Machine : Adding Tstat               */
    /****************************************************/
    {
        .stateId = ZBSM_MAIN_ADDINGTSTAT,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_RESUME, .eventCore = NULL},
            {.eventId = ZBEVENT_DEVICEANNCERECEIVED, .eventCore = config_new_device},
            {.eventId = ZBEVENT_JOINPERMITTIMEOUT, .eventCore = joinPermitTimeout},
            {.eventId = ZBEVENT_ADDINGTSTATHASENDED, .eventCore = addingTstatHasEnded},
            {.eventId = ZBEVENT_STOPADDTSTAT, .eventCore = stopAddingTstat},
            {.eventId = ZBEVENT_ADDSTAT_STATE_TIMEOUT, .eventCore = abortAddStatExecution},
        },
        .numberOfSupportedEvents = 6,
        .stateTimeout.smTimeoutLength = PERMITJOIN_TIMEOUT,
        .stateTimeout.smTimeout_cb = OnPermitJoinTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /****************************************************/
    /*  Main state Machine : reconfigure Tstat           */
    /****************************************************/
    {
        .stateId = ZBSM_MAIN_RECONFIGURETSTAT,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_ADDINGTSTATHASENDED, .eventCore = tstatReconfigHasEnded},
            {.eventId = ZBEVENT_ADDSTAT_STATE_TIMEOUT, .eventCore = abortAddStatExecution},
        },
        .numberOfSupportedEvents = 2,
        .stateTimeout.smTimeoutLength = SM_NO_TIMEOUT,
        .stateTimeout.smTimeout_cb = NULL,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /****************************************************/
    /*  Main state Machine : remove Tstat           */
    /****************************************************/
    {
        .stateId = ZBSM_MAIN_REMOVETSTAT,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZBEVENT_REMOVESTAT_RESP, .eventCore = LeaveRsp},
            {.eventId = ZBEVENT_REMOVESTAT_TIMEOUT, .eventCore = LeaveRsp},
        },
        .numberOfSupportedEvents = 2,
        .stateTimeout.smTimeoutLength = MAIN_REMOVE_STAT_TIMEOUT,
        .stateTimeout.smTimeout_cb = removeTstatTimeout,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /****************************************************/
    /*  Main state Machine : test mode state       */
    /****************************************************/
    {
        .stateId = ZBSM_MAIN_TESTMODE,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZB_NOEVENT, .eventCore = NULL},

        },
        .numberOfSupportedEvents = 1,
        .stateTimeout.smTimeoutLength = SM_NO_TIMEOUT,
        .stateTimeout.smTimeout_cb = NULL,
        .stateTimeout.smTimeoutHandler = NULL,
    },

    /****************************************************/
    /*  Main state Machine : dead state       */
    /****************************************************/
    {
        .stateId = ZBSM_MAIN_MODULE_IS_DEAD,
        .supportedEvents = (supportedStateEvents_t [])
        {
            {.eventId = ZB_NOEVENT, .eventCore = NULL},

        },
        .numberOfSupportedEvents = 1,
        .stateTimeout.smTimeoutLength = SM_NO_TIMEOUT,
        .stateTimeout.smTimeout_cb = NULL,
        .stateTimeout.smTimeoutHandler = NULL,
    },
};

stateMachineDefinition_t MainStateMachine =
{
    .SM_Start = MainSmStart,
    .SM_End = MainSmEnd,
    .supportedStates = mainSM_SupportedStates,
    .numberOfSupportedStates = sizeof(mainSM_SupportedStates)/sizeof(supportedStateMachineStates_t),
    .initialState = ZBSM_INITIALSTATE,
    .stateMachineTimeout.smTimeoutLength = -1,
    .stateMachineTimeout.smTimeoutHandler = NULL,
    .stateMachineTimeout.smTimeout_cb = NULL,
    .parentStateMachine = NULL,
    .activeSubStateMachine = NULL,
    .sm_name = "Main ",
};

extern stateMachineDefinition_t initializationStateMachine;
extern stateMachineDefinition_t addStatStateMachine;
extern stateMachineDefinition_t FactoryTestStateMachine;
extern zbDeviceToAdd_t currentZigBeeDevice;
static uint8_t autoRestart_AddProcess = 0;
extern uint8_t testStatus;
extern TimerHandle_t ZigBeePingTrigger;
extern TimerHandle_t ZigBeePingTimeout;
static uint8_t ReconfigurationTable[THERMOSTAT_INSTANCES];

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void MainSmStart (void)
{
    for (uint8_t i = 0 ; i < THERMOSTAT_INSTANCES; i++)
    {
        ReconfigurationTable[i] = 0;
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void MainSmEnd (uint8_t forceEnd)
{
    if (0 != forceEnd)
    {
        DBTHRD_SetData(DBTYPE_ZIGBEE_ASSOCIATION_PERMIT,&(uint8_t){0}, INDEX_DONT_CARE, DBCHANGE_LOCAL);
    }
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(abortAddStatExecution)
{
    EndStateMachine (&addStatStateMachine, SM_FORCE_END);
    if (0 == autoRestart_AddProcess)
    {
        nextOp->nextState = ZBSM_MAIN_RUNNING_COORDINATOR;
        return SM_SETNEXTSTATE;
    }
    else
    {
        /* getting ready for next device announce */
        currentZigBeeDevice.zbDescriptor.extAddress = -1;
        currentZigBeeDevice.zbDescriptor.shortAddress = -1;
        currentZigBeeDevice.zbDescriptor.thermostatEndPoint = -1;
        currentZigBeeDevice.clusterToBind = 0;
        currentZigBeeDevice.deviceIsTstat = 0;

        return SM_SUCCESS;
    }
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(stopAddingTstat)
{
    autoRestart_AddProcess = 0;
    if (NULL == MainStateMachine.activeSubStateMachine)
    {
        ZGB_PostEvents(ZBEVENT_ADDINGTSTATHASENDED);
    }
    return SM_SUCCESS;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(mainInitialization)
{
    if (0 == isZigBeeModuleUnusable())
    {
        StartStateMachine (&initializationStateMachine, ZBSM_SetEvent(ZBEVENT_INITNETWORK));
        nextOp->nextState = ZBSM_MAIN_JOINING_NETWORK;
    }
    else
    {
        TurnOffZigBeeModule();
        DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,&(uint8_t){DB_SET_ALERT | DBALERT_ZIGBEE_ERROR}, THIS_THERMOSTAT, DBCHANGE_LOCAL);
        nextOp->nextState = ZBSM_MAIN_MODULE_IS_DEAD;
    }
    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(startCoordinator)
{
    nextOp->nextState = ZBSM_MAIN_RUNNING_COORDINATOR;

    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(addTstat)
{
    currentZigBeeDevice.zbDescriptor.extAddress = -1;
    currentZigBeeDevice.zbDescriptor.shortAddress = -1;
    currentZigBeeDevice.zbDescriptor.thermostatEndPoint = -1;
    currentZigBeeDevice.clusterToBind = 0;
    currentZigBeeDevice.deviceIsTstat = 0;

    autoRestart_AddProcess = 1;
    SetPermitJoin(180);
    nextOp->nextState = ZBSM_MAIN_ADDINGTSTAT;


    return SM_SETNEXTSTATE;
}


/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(joinPermitTimeout)
{
   SetPermitJoin(180);

   return SM_RESTARTSTATETIMEOUT;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(config_new_device)
{
    StartStateMachine (&addStatStateMachine, ZBSM_NoEvent());
    (void)event;
    (void)nextOp;


    return SM_SUCCESS;
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(reconfigureTstat)
{
    zigBeeDescriptor_t zigBeeStat;
    uint8_t statIndex = event.statId;

    autoRestart_AddProcess = 0;
    if ((statIndex != THIS_THERMOSTAT) && (statIndex < THERMOSTAT_INSTANCES))
    {
        if (DBERR_OK == DBTHRD_GetData(DBTYPE_ZIGBEE_DESCRIPTOR, (void*)&zigBeeStat, statIndex))
        {
            currentZigBeeDevice.zbDescriptor.shortAddress = zigBeeStat.shortAddress;
            currentZigBeeDevice.zbDescriptor.extAddress = zigBeeStat.extAddress;
            currentZigBeeDevice.zbDescriptor.thermostatEndPoint = -1;
            currentZigBeeDevice.clusterToBind = 0;
            currentZigBeeDevice.deviceIsTstat = 0;

            ReconfigurationTable[statIndex] = 1;

            UpdateTstatGroupId(statIndex);
            StartStateMachine (&addStatStateMachine, ZBSM_SetEventWIdx(ZBEVENT_RECONFIGURETSTAT,statIndex));

            nextOp->nextState = ZBSM_MAIN_RECONFIGURETSTAT;

            return SM_SETNEXTSTATE;
        }
    }
    return SM_ERRCANNOTEXECUTE;
}


/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(tstatReconfigHasEnded)
{
    if (event.statId < THERMOSTAT_INSTANCES)
    {
        ReconfigurationTable[event.statId] = 0;
    }
    return addingTstatHasEnded(event, nextOp);
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(addingTstatHasEnded)
{
    if (0 == autoRestart_AddProcess)
    {
        nextOp->nextState = ZBSM_MAIN_RUNNING_COORDINATOR;
        SetPermitJoin(0);
        /************************************************************/
        /*  At this point in the development (23-03-2017), it is    */
        /*  not sure if it is mandatory to support the find and bind*/
        /*  for the BDB module. So, at the end of a add tstat seq.  */
        /*  a find and bind is issued to comply with the BDB spec.  */
        /************************************************************/
        StartFindAndBind();

        return SM_SETNEXTSTATE;
    }
    else
    {
        /* getting ready for next device announce */
        currentZigBeeDevice.zbDescriptor.extAddress = -1;
        currentZigBeeDevice.zbDescriptor.shortAddress = -1;
        currentZigBeeDevice.zbDescriptor.thermostatEndPoint = -1;
        currentZigBeeDevice.clusterToBind = 0;
        currentZigBeeDevice.deviceIsTstat = 0;

        return SM_SUCCESS;
    }
}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(sendLeaveRequest)
{
    if (0 == tstatNetworkLeaveReq (event.statId))
    {
        nextOp->nextState = ZBSM_MAIN_REMOVETSTAT;
        return SM_SETNEXTSTATE;
    }
    else
    {
        classTH_DeleteThermostat(event.statId, DELETE_ONE_TERMOSTAT);
        return SM_SUCCESS;      //actually not a success, but it make the SM stay in the same state
    }

}

/*******************************************************************************
* @brief
* @inputs
*   - zigBeeSMEvents_t event : definition of the incoming event
*   - smNextOperation_t *nextOp : pointer to configure the next operation, either
*                   the next event (for restart state machine) or next state (for
*                   SM_SETNEXTSTATE)
* @retval
*   stateMachineErrorCode_t
* @author J-F. Simard
* @date   2017/06/12
*******************************************************************************/
ZIGBEE_EVENTCORE_FUNC(LeaveRsp)
{
    classTH_DeleteThermostat(event.statId, DELETE_ONE_TERMOSTAT);

    nextOp->nextState = ZBSM_MAIN_RUNNING_COORDINATOR;
    return SM_SETNEXTSTATE;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-François Many
* @date   2017/03/06
*******************************************************************************/
static void OnPermitJoinTimeout (TimerHandle_t xTimer)
{
    ZGB_PostEvents(ZBEVENT_JOINPERMITTIMEOUT);
}

///*******************************************************************************
//* @brief
//* @inputs None
//* @retval None
//* @author Jean-François Many
//* @date   2017/03/06
//*******************************************************************************/
//static void initialStateTimeout (TimerHandle_t xTimer)
//{
//    ZGB_PostEvents(ZBEVENT_STARTINITIALIZATION);
//}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-François Many
* @date   2017/03/06
*******************************************************************************/
static void removeTstatTimeout (TimerHandle_t xTimer)
{
    uint8_t statId;

    statId = GetStatIdToRemove();
    ZGB_PostEventsWithStatId(ZBEVENT_REMOVESTAT_TIMEOUT,statId);
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  Check if the specified thermostat is currently under reconfiguration
* @inputs statIndex: thermostat index to verify the reconfiguration state
* @retval uint8_t reconfiguration status: 1: in reconfiguration, 0: not in reconfiguration
* @author Jean-Fran?ois Many
* @date   2017/05/24
*******************************************************************************/
uint8_t IsStatInReconfiguration(uint8_t statIndex)
{
    return (ReconfigurationTable[statIndex]);
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
