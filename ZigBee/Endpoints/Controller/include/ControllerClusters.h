/**************************************************************************//**
  \file ControllerClusters.h

  \brief
    Combined Interface clusters interface.

  \author
    Atmel Corporation: http://www.atmel.com \n
    Support email: avr@atmel.com

  Copyright (c) 2008-2013, Atmel Corporation. All rights reserved.
  Licensed under Atmel's Limited License Agreement (BitCloudTM).

  \internal
    History:
******************************************************************************/
#ifndef _CTLCLUSTER_H
#define _CTLCLUSTER_H

/******************************************************************************
                    Includes section
******************************************************************************/
#include <zcl.h>

/******************************************************************************
                    Definitions section
******************************************************************************/
#define CTL_SERVER_CLUSTERS_COUNT     4
#define CTL_SERVER_CLUSTER_INIT_COUNT CTL_SERVER_CLUSTERS_COUNT

#define CTL_CLIENT_CLUSTER_INIT_COUNT 0
#define CTL_CLIENT_CLUSTERS_COUNT     6

/******************************************************************************
                    Externals
******************************************************************************/
extern ZCL_Cluster_t ControllerServerClusters[CTL_SERVER_CLUSTERS_COUNT];
extern ZCL_Cluster_t ControllerClientClusters[CTL_CLIENT_CLUSTERS_COUNT];
extern void (*ControllerServerClusterInitFunctions[CTL_SERVER_CLUSTER_INIT_COUNT])();

extern ClusterId_t   ControllerServerClusterIds[CTL_SERVER_CLUSTERS_COUNT];
extern ClusterId_t   ControllerClientClusterIds[CTL_CLIENT_CLUSTERS_COUNT];

#endif // _CTLCLUSTER_H
