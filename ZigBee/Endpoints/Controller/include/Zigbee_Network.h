/*******************************************************************************
* @file    Zigbee_Network.h
* @date    2016/11/24
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef Zigbee_Network_H
#define Zigbee_Network_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>

#include "zdoCommon.h"
#include "zdo.h"
#include "zcl.h"

/*******************************************************************************
* Public constants definitions
*******************************************************************************/



/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void ZB_ManageNewThermostat (DeviceAnnce_t * device);
void HAL_LeaveZigBeeNetwork(uint8_t statIndex);
void HAL_ConfirmLeaveNetwork(ZDO_ZdpResp_t *zdpResp);
void ManageReadAttributeResp(ZCL_Notify_t *ntfy);


#endif /* Zigbee_Network_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
