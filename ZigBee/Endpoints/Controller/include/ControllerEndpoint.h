/*******************************************************************************
* @file    _ControllerEndpoint.h
* @date    2016/11/25
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef _ControllerEndpoint_H
#define _ControllerEndpoint_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>

#include "zclDevice.h"
#include "zcl.h"

/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define CTL_SRC_ENDPOINT_ID     0x14
#define CTL_ENDPOINT_SERVER_CLUSTERS_COUNT   3
#define CTL_ENDPOINT_CLIENT_CLUSTERS_COUNT   6


/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void InitControllerEndpoint (void);
void ControllerSecurityInit(void);
AppBindReq_t **getDeviceBindRequest(void);
void ZCL_ReadAttributeResp(ZCL_Notify_t *ntfy);

#endif /* _ControllerEndpoint_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
