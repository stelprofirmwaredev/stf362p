/**************************************************************************//**
  \file ControllerClusters.c

  \brief
    Combined Interface clusters implementation.

  \author
    Atmel Corporation: http://www.atmel.com \n
    Support email: avr@atmel.com

  Copyright (c) 2008-2013, Atmel Corporation. All rights reserved.
  Licensed under Atmel's Limited License Agreement (BitCloudTM).

  \internal
    History:
******************************************************************************/

/******************************************************************************
                    Includes section
******************************************************************************/
#include ".\Endpoints\Controller\include\ControllerClusters.h"

//#include <haClusters.h>
//#include <ciClusters.h>
#include <basicCluster.h>
#include <identifyCluster.h>
#include <ciBasicCluster.h>
#include <ciIdentifyCluster.h>
//#include <ciOnOffCluster.h>
//#include <ciLevelControlCluster.h>
#include <ciGroupsCluster.h>
//#include <ciScenesCluster.h>
//#include <ciOccupancySensingCluster.h>
#include <ciTemperatureMeasurementCluster.h>
#include <ciHumidityMeasurementCluster.h>
//#include <ciIlluminanceMeasurementCluster.h>
#include <ciThermostatCluster.h>
#include <ciThermostatUiConfCluster.h>
//#include <ciDiagnosticsCluster.h>
//#include <ciFanControlCluster.h>
//#include <ciPowerConfigurationCluster.h>
#include <ciTimeCluster.h>
//#include <ciAlarmsCluster.h>
//#include <ciIasZoneCluster.h>
//#include <ciIasACECluster.h>


/******************************************************************************
                    Global variables
******************************************************************************/
ZCL_Cluster_t ControllerServerClusters[CTL_SERVER_CLUSTERS_COUNT] =
{
    ZCL_DEFINE_BASIC_CLUSTER_SERVER(&ciBasicClusterServerAttributes, &ciBasicClusterServerCommands),
    DEFINE_IDENTIFY_CLUSTER(ZCL_SERVER_CLUSTER_TYPE, &ciIdentifyClusterServerAttributes, &ciIdentifyCommands),
    DEFINE_TIME_CLUSTER(ZCL_SERVER_CLUSTER_TYPE, &ciTimeClusterServerAttributes, NULL),
    DEFINE_TEMPERATURE_MEASUREMENT_CLUSTER(ZCL_SERVER_CLUSTER_TYPE, &ciTemperatureMeasurementClusterServerAttributes),
};

void (*ControllerServerClusterInitFunctions[CTL_SERVER_CLUSTER_INIT_COUNT])() =
{
  basicClusterInit,
  identifyClusterInit,
  timeClusterInit,
  temperatureMeasurementClusterInit,
};

ZCL_Cluster_t ControllerClientClusters[CTL_CLIENT_CLUSTERS_COUNT] =
{
  ZCL_DEFINE_BASIC_CLUSTER_CLIENT(),
  DEFINE_IDENTIFY_CLUSTER(ZCL_CLIENT_CLUSTER_TYPE, NULL, &ciIdentifyCommands),
  DEFINE_GROUPS_CLUSTER(ZCL_CLIENT_CLUSTER_TYPE, NULL, &ciGroupsCommands),
  DEFINE_HUMIDITY_MEASUREMENT_CLUSTER(ZCL_CLIENT_CLUSTER_TYPE, NULL),
  DEFINE_THERMOSTAT_CLUSTER(ZCL_CLIENT_CLUSTER_TYPE, NULL, NULL),
  DEFINE_THERMOSTAT_UI_CONF_CLUSTER(ZCL_CLIENT_CLUSTER_TYPE, NULL, NULL),
};

ClusterId_t ControllerServerClusterIds[CTL_SERVER_CLUSTERS_COUNT] =
{
  BASIC_CLUSTER_ID,
  IDENTIFY_CLUSTER_ID,
  TIME_CLUSTER_ID,
  TEMPERATURE_MEASUREMENT_CLUSTER_ID,
};

ClusterId_t ControllerClientClusterIds[CTL_CLIENT_CLUSTERS_COUNT] =
{
  BASIC_CLUSTER_ID,
  IDENTIFY_CLUSTER_ID,
  GROUPS_CLUSTER_ID,
  HUMIDITY_MEASUREMENT_CLUSTER_ID,
  THERMOSTAT_CLUSTER_ID,
  THERMOSTAT_UI_CONF_CLUSTER_ID,
};

// eof ControllerClusters.c
