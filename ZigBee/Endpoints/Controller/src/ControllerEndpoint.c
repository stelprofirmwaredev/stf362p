/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    ControllerEndpoint.c
* @date    2016/11/25
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>

#include ".\DB\inc\THREAD_DB.h"
#include ".\Endpoints\Controller\include\ControllerEndpoint.h"
#include ".\Endpoints\Controller\include\ControllerClusters.h"
#include ".\Endpoints\Controller\include\Zigbee_Network.h"

#include "zcl.h"
#include "zclSecurityManager.h"
#include "commandManager.h"
#include "StringObjects.h"
#include <time.h>

#include <ciThermostatCluster.h>
#include <ciBasicCluster.h>
#include <basicCluster.h>
#include <ciIdentifyCluster.h>
#include <identifyCluster.h>
#include <ciHumidityMeasurementCluster.h>
#include <ciTemperatureMeasurementCluster.h>
#include <ciThermostatUIConfCluster.h>
#include <ciTimeCluster.h>


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define MESSAGE_HEADER_LENGTH   3 //id(2 bytes) + type (1 byte)
#define OFFSET_1970_TO_2000     946684800   //Number of seconds between Jan 1 1970 and Jan 1 2000
#define ZIGBEE_TSTAT_TIMEOUT    (2*60*1000)    //2 minutes * 60 seconds/minute * 1000 ms/second
#define TSTAT_WAITING           0
#define TSTAT_ALIVE             1
#define TSTAT_DISCONNECTED      2

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
static ZCL_DeviceEndpoint_t ControllerEndpoint =
{
  .simpleDescriptor =
  {
    .endpoint            = CTL_SRC_ENDPOINT_ID,
    .AppProfileId        = PROFILE_ID_HOME_AUTOMATION,
    .AppDeviceId         = HA_COMBINED_INTERFACE_ID,
    .AppInClustersCount  = ARRAY_SIZE(ControllerServerClusterIds),
    .AppInClustersList   = ControllerServerClusterIds,
    .AppOutClustersCount = ARRAY_SIZE(ControllerClientClusterIds),
    .AppOutClustersList  = ControllerClientClusterIds,
  },
  .serverCluster = ControllerServerClusters,
  .clientCluster = ControllerClientClusters,
};

static ClusterId_t ControllerClientClusterToBindIds[CTL_ENDPOINT_CLIENT_CLUSTERS_COUNT] =
{
    THERMOSTAT_CLUSTER_ID,
    BASIC_CLUSTER_ID,
    IDENTIFY_CLUSTER_ID,
    GROUPS_CLUSTER_ID,
    THERMOSTAT_UI_CONF_CLUSTER_ID,
    HUMIDITY_MEASUREMENT_CLUSTER_ID,
};

//static ClusterId_t ControllerServerClusterToBindIds[CTL_ENDPOINT_SERVER_CLUSTERS_COUNT] =
//{
//  BASIC_CLUSTER_ID,
//  IDENTIFY_CLUSTER_ID,
//  TIME_CLUSTER_ID,
//  TEMPERATURE_MEASUREMENT_CLUSTER_ID,
//  IAS_ACE_CLUSTER_ID,
//};

static AppBindReq_t ControllerBindReq =
{
  .nwkAddrOfInterest = RX_ON_WHEN_IDLE_ADDR,
  .remoteServers     = ControllerClientClusterToBindIds,
  .remoteServersCnt  = ARRAY_SIZE(ControllerClientClusterToBindIds),
  .remoteClients     = NULL,//ControllerServerClusterToBindIds,
  .remoteClientsCnt  = NULL,//ARRAY_SIZE(ControllerServerClusterToBindIds),
  .profile           = PROFILE_ID_HOME_AUTOMATION,
  .srcEndpoint       = CTL_SRC_ENDPOINT_ID,
  .callback          = NULL,
};
static AppBindReq_t* deviceBindReqs[APP_ENDPOINTS_AMOUNT];
static ZCL_LinkKeyDesc_t lightKeyDesc = {CCPU_TO_LE64(APS_UNIVERSAL_EXTENDED_ADDRESS), HA_LINK_KEY};
static uint8_t TstatConnectionStatus[THERMOSTAT_INSTANCES];

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
TimerHandle_t ZigBeeTstatTimeout;


/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static void ThermostatReportInd(ZCL_Addressing_t *addressing, uint8_t reportLength, uint8_t *reportPayload);
static void ThermostatAttrEventInd(ZCL_Addressing_t *addressing, ZCL_AttributeId_t attributeId, ZCL_AttributeEvent_t event);
static void ThermostatUIConfigReportInd(ZCL_Addressing_t *addressing, uint8_t reportLength, uint8_t *reportPayload);
static void ThermostatUIConfigAttrEventInd(ZCL_Addressing_t *addressing, ZCL_AttributeId_t attributeId, ZCL_AttributeEvent_t event);
static void HumidityMeasurementReportInd(ZCL_Addressing_t *addressing, uint8_t reportLength, uint8_t *reportPayload);
static void HumidityMeasurementAttrEventInd(ZCL_Addressing_t *addressing, ZCL_AttributeId_t attributeId, ZCL_AttributeEvent_t event);
static void zigBeeStatNotificationResp (ZCL_Notify_t *ntfy);
static void SendOccupiedHeatSetpointAttribute (uint8_t thermsotatIndex);
static void SendTemperatureDisplayModeAttribute(void);
static void SendOutdoorTemperatureAttribute(void);
static void SendKeypadLockoutAttribute(uint8_t thermsotatIndex);
static void SendLocationDescriptionAttribute(uint8_t thermsotatIndex);
static void SendTimeFormatAttribute(void);
static void SendTimeAttribute(void);
static void ThermostatIsAlive(uint8_t tstat);
static void OnZigBeeTstatTimeout( TimerHandle_t xTimer );

DB_NOTIFICATION(NotifyZBStat_AmbientSetpoint);
DB_NOTIFICATION(NotifyZBStat_GroupSetpoint);
DB_NOTIFICATION(NotifyZBStat_UnitFormat);
DB_NOTIFICATION(NotifyZBStat_OutdoorTemperature);
DB_NOTIFICATION(NotifyZBStat_LockState);
DB_NOTIFICATION(NotifyZBStat_TstatName);
DB_NOTIFICATION(NotifyZBStat_TimeFormat);
DB_NOTIFICATION(NotifyZBStat_LocalTime);


/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief Timer that expires when we must check if a thermostat is disconnected
* @inputs xTimer: timer instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/02/07
*******************************************************************************/
static void OnZigBeeTstatTimeout( TimerHandle_t xTimer )
{
    uint8_t tstat;
    uint8_t model[32];
    uint8_t alert;

    for (tstat = 1; tstat < THERMOSTAT_INSTANCES; tstat++)
    {
        DBTHRD_GetData (DBTYPE_TSTAT_MODEL, &model, tstat);
        if (model[0] != NULL)
        {
            if (TstatConnectionStatus[tstat] == TSTAT_WAITING)
            {
                TstatConnectionStatus[tstat] = TSTAT_DISCONNECTED;
                alert = DB_SET_ALERT | DBALERT_NO_ZIGBEE;
                DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, tstat, DBCHANGE_LOCAL);
            }
            else
            {
                TstatConnectionStatus[tstat] = TSTAT_WAITING;
            }
        }
    }
}

/*******************************************************************************
* @brief  Declare a thermostat alive (ZigBee working)
* @inputs tstat: Thermostat Index
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/02/07
*******************************************************************************/
static void ThermostatIsAlive(uint8_t tstat)
{
    uint8_t alert;

    if (TstatConnectionStatus[tstat] == TSTAT_DISCONNECTED)
    {
        alert = DB_CLEAR_ALERT | DBALERT_NO_ZIGBEE;
        DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, tstat, DBCHANGE_LOCAL);
    }

    TstatConnectionStatus[tstat] = TSTAT_ALIVE;
}

/**************************************************************************//**
\brief Attribute Event indication handler(to indicate when attr values have
        read or written)

\param[in] addressing - pointer to addressing information;
\param[in] reportLength - data payload length;
\param[in] reportPayload - data pointer
******************************************************************************/
static void ThermostatAttrEventInd(ZCL_Addressing_t *addressing, ZCL_AttributeId_t attributeId, ZCL_AttributeEvent_t event)
{
    asm("nop");
}

/**************************************************************************//**
\brief Report attribute indication handler

\param[in] addressing - pointer to addressing information;
\param[in] reportLength - data payload length;
\param[in] reportPayload - data pointer
******************************************************************************/
static void ThermostatReportInd(ZCL_Addressing_t *addressing, uint8_t reportLength, uint8_t *reportPayload)
{
    ZCL_Report_t *rep = (ZCL_Report_t *)reportPayload;
    uint8_t statIndex;
    int16_t remainingLength = reportLength;
    uint8_t messageLength;
    uint8_t alert;
    uint8_t newAlert;
    dbType_Alerts_t currentAlert;
    uint8_t i;
    uint8_t alertInstance;

    while (remainingLength > 0)
    {
        if (DBERR_NONEXISTING_STAT != DBTHRD_GetData (DBTYPE_INDEX_FROM_ZBSHORTADDR, (void*)&statIndex, addressing->addr.shortAddress))
        {
            switch (rep->id)
            {
            case ZCL_THERMOSTAT_CLUSTER_LOCAL_TEMPERATURE_SERVER_ATTRIBUTE_ID:
                {
                DBTHRD_SetData(DBTYPE_AMBIENT_TEMPERATURE,
                                (void*)&rep->value[0],
                                statIndex,
                                DBCHANGE_ZIGBEE);
                ThermostatIsAlive(statIndex);
                }
                break;

            case ZCL_THERMOSTAT_CLUSTER_OCCUPIED_HEATING_SETPOINT_SERVER_ATTRIBUTE_ID:
                {
                DBTHRD_SetData(DBTYPE_AMBIENT_SETPOINT,
                                (void*)&rep->value[0],
                                statIndex,
                                DBCHANGE_ZIGBEE);
                }
                break;

            case ZCL_THERMOSTAT_CLUSTER_PI_HEATING_DEMAND_SERVER_ATTRIBUTE_ID:
                {
                DBTHRD_SetData(DBTYPE_HEAT_DEMAND,
                                (void*)&rep->value[0],
                                statIndex,
                                DBCHANGE_ZIGBEE);
                }
                break;

            case ZCL_THERMOSTAT_CLUSTER_POWER_CONSUMPTION_MANUF_SPECIFIC_ATTRIBUTE_ID:
                {
                    DBTHRD_SetData(DBTYPE_LOCAL_DAILY_CONSUMPTION,
                                (void*)&rep->value[0],
                                statIndex,
                                DBCHANGE_ZIGBEE);
                }
                break;

            case ZCL_THERMOSTAT_CLUSTER_ALERT_MANUF_SPECIFIC_ATTRIBUTE_ID:
                {
                    alert = rep->value[0];
                    DBTHRD_GetData(DBTYPE_ACTIVE_ALERTS, (void*)&currentAlert, statIndex);
                    for (i = 0; i < ALERT_INSTANCES; i++)
                    {
                        newAlert = 0;
                        if (alert & (1<<i))
                        {
                            newAlert = DB_SET_ALERT | (i + 1);
                        }
                        else
                        {
                            for (alertInstance = 0; alertInstance < ALERT_INSTANCES; alertInstance++)
                            {
                                if (currentAlert.ActiveAlerts[alertInstance] == (i + 1))
                                {
                                    newAlert = DB_CLEAR_ALERT | (i + 1);
                                }
                            }
                        }
                        if (newAlert != 0)
                        {
                            DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,
                                (void*)&newAlert,
                                statIndex,
                                DBCHANGE_ZIGBEE);
                        }
                    }
                }
                break;

            default:
                break;
            }
        }
        messageLength = (ZCL_GetAttributeLength(rep->type, rep->value) + MESSAGE_HEADER_LENGTH);
        rep = (ZCL_Report_t *)(reportPayload + messageLength);
        remainingLength -= messageLength;
    }
}

/**************************************************************************//**
\brief Report attribute indication handler

\param[in] addressing - pointer to addressing information;
\param[in] reportLength - data payload length;
\param[in] reportPayload - data pointer
******************************************************************************/
static void ThermostatUIConfigReportInd(ZCL_Addressing_t *addressing, uint8_t reportLength, uint8_t *reportPayload)
{
    ZCL_Report_t *rep = (ZCL_Report_t *)reportPayload;
    uint8_t statIndex;
    int16_t remainingLength = reportLength;
    uint8_t messageLength;
    Lock_t lock;
    ZCL_ThUiConfKeypadLockOut_t systemLock;

    while (remainingLength > 0)
    {
        if (DBERR_NONEXISTING_STAT != DBTHRD_GetData (DBTYPE_INDEX_FROM_ZBSHORTADDR, (void*)&statIndex, addressing->addr.shortAddress))
        {
            switch (rep->id)
            {
            case ZCL_THERMOSTAT_UI_CONF_CLUSTER_KEYPAD_LOCKOUT_SERVER_ATTRIBUTE_ID:
                {
                systemLock = (ZCL_ThUiConfKeypadLockOut_t)rep->value[0];
                if (systemLock == ZCL_NO_LOCKOUT)
                {
                    lock = LOCK_OFF;
                }
                else
                {
                    lock = LOCK_ON;
                }
                DBTHRD_SetData(DBTYPE_LOCK_STATE,
                                (void*)&lock,
                                statIndex,
                                DBCHANGE_ZIGBEE);
                }
                break;

            default:
                break;
            }
        }
        messageLength = (ZCL_GetAttributeLength(rep->type, rep->value) + MESSAGE_HEADER_LENGTH);
        rep = (ZCL_Report_t *)(reportPayload + messageLength);
        remainingLength -= messageLength;
    }
}

/**************************************************************************//**
\brief Attribute Event indication handler(to indicate when attr values have
        read or written)

\param[in] addressing - pointer to addressing information;
\param[in] reportLength - data payload length;
\param[in] reportPayload - data pointer
******************************************************************************/
static void ThermostatUIConfigAttrEventInd(ZCL_Addressing_t *addressing, ZCL_AttributeId_t attributeId, ZCL_AttributeEvent_t event)
{
}

/**************************************************************************//**
\brief Report attribute indication handler

\param[in] addressing - pointer to addressing information;
\param[in] reportLength - data payload length;
\param[in] reportPayload - data pointer
******************************************************************************/
static void HumidityMeasurementReportInd(ZCL_Addressing_t *addressing, uint8_t reportLength, uint8_t *reportPayload)
{
    ZCL_Report_t *rep = (ZCL_Report_t *)reportPayload;
    uint8_t statIndex;
    int16_t remainingLength = reportLength;
    uint8_t messageLength;
    uint16_t humidity;

    while (remainingLength > 0)
    {
        if (DBERR_NONEXISTING_STAT != DBTHRD_GetData (DBTYPE_INDEX_FROM_ZBSHORTADDR, (void*)&statIndex, addressing->addr.shortAddress))
        {
            switch (rep->id)
            {
            case ZCL_HUMIDITY_MEASUREMENT_CLUSTER_SERVER_MEASURED_VALUE_ATTRIBUTE_ID:
                {
                humidity = (rep->value[0] | (rep->value[1] << 8)) /100;
                DBTHRD_SetData(DBTYPE_AMBIENT_HUMIDITY,
                                (void*)&humidity,
                                statIndex,
                                DBCHANGE_ZIGBEE);
                }
                break;
            default:
                break;
            }
        }
        messageLength = (ZCL_GetAttributeLength(rep->type, rep->value) + MESSAGE_HEADER_LENGTH);
        rep = (ZCL_Report_t *)(reportPayload + messageLength);
        remainingLength -= messageLength;
    }
}


/**************************************************************************//**
\brief Attribute Event indication handler(to indicate when attr values have
        read or written)

\param[in] addressing - pointer to addressing information;
\param[in] reportLength - data payload length;
\param[in] reportPayload - data pointer
******************************************************************************/
static void HumidityMeasurementAttrEventInd(ZCL_Addressing_t *addressing, ZCL_AttributeId_t attributeId, ZCL_AttributeEvent_t event)
{
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/12/05
*******************************************************************************/
void zigBeeStatNotificationResp (ZCL_Notify_t *ntfy)
{
    (void)ntfy;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/12/05
*******************************************************************************/
static void SendOccupiedHeatSetpointAttribute (uint8_t thermsotatIndex)
{
    ZCL_Request_t *req;
    ZCL_WriteAttributeReq_t * writeAttrReq;
    uint16_t * setpoint;
    zigBeeDescriptor_t zigBeeStat;

    if ((thermsotatIndex != THIS_THERMOSTAT) && (thermsotatIndex < THERMOSTAT_INSTANCES))
    {
        if (DBERR_OK == DBTHRD_GetData(DBTYPE_ZIGBEE_DESCRIPTOR, (void*)&zigBeeStat, thermsotatIndex))
        {
            if (!(req = getFreeCommand()))
                return;


            writeAttrReq = (ZCL_WriteAttributeReq_t*)req->requestPayload;
            setpoint = (uint16_t *)(req->requestPayload+(sizeof(ZCL_WriteAttributeReq_t )- sizeof(uint8_t)));

            writeAttrReq->id = ZCL_THERMOSTAT_CLUSTER_OCCUPIED_HEATING_SETPOINT_SERVER_ATTRIBUTE_ID;
            writeAttrReq->type = ZCL_S16BIT_DATA_TYPE_ID;

            DBTHRD_GetData(DBTYPE_AMBIENT_SETPOINT, (void*)setpoint, thermsotatIndex);

            req->id              = ZCL_WRITE_ATTRIBUTES_COMMAND_ID;
            req->requestLength   = sizeof(uint16_t) + sizeof(ZCL_WriteAttributeReq_t) - sizeof(uint8_t);
            req->endpointId      = APP_SRC_ENDPOINT_ID;
            req->defaultResponse = ZCL_FRAME_CONTROL_DISABLE_DEFAULT_RESPONSE;

            req->dstAddressing.addrMode             = APS_EXT_ADDRESS;
            req->dstAddressing.addr.extAddress      = zigBeeStat.extAddress;
            req->dstAddressing.profileId            = PROFILE_ID_HOME_AUTOMATION;
            req->dstAddressing.endpointId           = zigBeeStat.thermostatEndPoint;
            req->dstAddressing.clusterId            = THERMOSTAT_CLUSTER_ID;
            req->dstAddressing.clusterSide          = ZCL_CLUSTER_SIDE_SERVER;
            req->dstAddressing.manufacturerSpecCode = 0;
            req->dstAddressing.sequenceNumber       = ZCL_GetNextSeqNumber();

            req->ZCL_Notify = zigBeeStatNotificationResp;

            commandManagerSendAttribute(req);
        }
    }
}

/*******************************************************************************
* @brief  Send the Temperature Display Mode attribute to all devices
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/18
*******************************************************************************/
static void SendTemperatureDisplayModeAttribute(void)
{
    ZCL_Request_t *req;
    ZCL_WriteAttributeReq_t * writeAttrReq;
    uint8_t* format;

    if (!(req = getFreeCommand()))
        return;


    writeAttrReq = (ZCL_WriteAttributeReq_t*)req->requestPayload;
    format = (uint8_t *)(req->requestPayload+(sizeof(ZCL_WriteAttributeReq_t )- sizeof(uint8_t)));

    writeAttrReq->id = ZCL_THERMOSTAT_UI_CONF_CLUSTER_TEMPERATURE_DISPLAY_MODE_SERVER_ATTRIBUTE_ID;
    writeAttrReq->type = ZCL_8BIT_ENUM_DATA_TYPE_ID;

    DBTHRD_GetData(DBTYPE_TEMPERATURE_FORMAT, (void*)format, INDEX_DONT_CARE);

    req->id              = ZCL_WRITE_ATTRIBUTES_COMMAND_ID;
    req->requestLength   = sizeof(uint8_t) + sizeof(ZCL_WriteAttributeReq_t) - sizeof(uint8_t);
    req->endpointId      = APP_SRC_ENDPOINT_ID;
    req->defaultResponse = ZCL_FRAME_CONTROL_DISABLE_DEFAULT_RESPONSE;

    req->dstAddressing.addrMode             = APS_SHORT_ADDRESS;
    req->dstAddressing.addr.shortAddress    = BROADCAST_ADDR_ALL;
    req->dstAddressing.profileId            = PROFILE_ID_HOME_AUTOMATION;
    req->dstAddressing.endpointId           = APS_BROADCAST_ENDPOINT;
    req->dstAddressing.clusterId            = THERMOSTAT_UI_CONF_CLUSTER_ID;
    req->dstAddressing.clusterSide          = ZCL_CLUSTER_SIDE_SERVER;
    req->dstAddressing.manufacturerSpecCode = 0;
    req->dstAddressing.sequenceNumber       = ZCL_GetNextSeqNumber();

    req->ZCL_Notify = zigBeeStatNotificationResp;

    commandManagerSendAttribute(req);
}

/*******************************************************************************
* @brief  Send the Outdoor Temperature attribute to all devices
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/18
*******************************************************************************/
static void SendOutdoorTemperatureAttribute(void)
{
    ZCL_Request_t *req;
    ZCL_WriteAttributeReq_t * writeAttrReq;
    int16_t* temperature;

    if (!(req = getFreeCommand()))
        return;


    writeAttrReq = (ZCL_WriteAttributeReq_t*)req->requestPayload;
    temperature = (int16_t *)(req->requestPayload+(sizeof(ZCL_WriteAttributeReq_t )- sizeof(uint8_t)));

    writeAttrReq->id = ZCL_TEMPERATURE_MEASUREMENT_CLUSTER_SERVER_MEASURED_VALUE_ATTRIBUTE_ID;
    writeAttrReq->type = ZCL_S16BIT_DATA_TYPE_ID;

    DBTHRD_GetData(DBTYPE_OUTDOOR_TEMPERATURE, (void*)temperature, INDEX_DONT_CARE);

    ciTemperatureMeasurementClusterServerAttributes.measuredValue.value = *temperature;

    req->id              = ZCL_REPORT_ATTRIBUTES_COMMAND_ID;
    req->requestLength   = sizeof(int16_t) + sizeof(ZCL_WriteAttributeReq_t) - sizeof(uint8_t);
    req->endpointId      = APP_SRC_ENDPOINT_ID;
    req->defaultResponse = ZCL_FRAME_CONTROL_DISABLE_DEFAULT_RESPONSE;

    req->dstAddressing.addrMode             = APS_SHORT_ADDRESS;
    req->dstAddressing.addr.shortAddress    = BROADCAST_ADDR_ALL;
    req->dstAddressing.profileId            = PROFILE_ID_HOME_AUTOMATION;
    req->dstAddressing.endpointId           = APS_BROADCAST_ENDPOINT;
    req->dstAddressing.clusterId            = TEMPERATURE_MEASUREMENT_CLUSTER_ID;
    req->dstAddressing.clusterSide          = ZCL_CLUSTER_SIDE_CLIENT;
    req->dstAddressing.manufacturerSpecCode = 0;
    req->dstAddressing.sequenceNumber       = ZCL_GetNextSeqNumber();

    req->ZCL_Notify = zigBeeStatNotificationResp;

    commandManagerSendAttribute(req);
}

/*******************************************************************************
* @brief  Send the Keypad Lockout attribute to the specified thermostat
* @inputs thermsotatIndex: specified thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/18
*******************************************************************************/
static void SendKeypadLockoutAttribute(uint8_t thermsotatIndex)
{
    ZCL_Request_t *req;
    ZCL_WriteAttributeReq_t * writeAttrReq;
    Lock_t lock;
    ZCL_ThUiConfKeypadLockOut_t* systemLock;
    zigBeeDescriptor_t zigBeeStat;

    if ((thermsotatIndex != THIS_THERMOSTAT) && (thermsotatIndex < THERMOSTAT_INSTANCES))
    {
        if (DBERR_OK == DBTHRD_GetData(DBTYPE_ZIGBEE_DESCRIPTOR, (void*)&zigBeeStat, thermsotatIndex))
        {
            if (!(req = getFreeCommand()))
                return;


            writeAttrReq = (ZCL_WriteAttributeReq_t*)req->requestPayload;
            systemLock = (ZCL_ThUiConfKeypadLockOut_t *)(req->requestPayload+(sizeof(ZCL_WriteAttributeReq_t )- sizeof(uint8_t)));

            writeAttrReq->id = ZCL_THERMOSTAT_UI_CONF_CLUSTER_KEYPAD_LOCKOUT_SERVER_ATTRIBUTE_ID;
            writeAttrReq->type = ZCL_8BIT_ENUM_DATA_TYPE_ID;

            DBTHRD_GetData(DBTYPE_LOCK_STATE, (void*)&lock, thermsotatIndex);
            if (lock == LOCK_ON)
            {
                *systemLock = ZCL_LEVEL_1_LOCKOUT;
            }
            else
            {
                *systemLock = ZCL_NO_LOCKOUT;
            }

            req->id              = ZCL_WRITE_ATTRIBUTES_COMMAND_ID;
            req->requestLength   = sizeof(uint8_t) + sizeof(ZCL_WriteAttributeReq_t) - sizeof(uint8_t);
            req->endpointId      = APP_SRC_ENDPOINT_ID;
            req->defaultResponse = ZCL_FRAME_CONTROL_DISABLE_DEFAULT_RESPONSE;

            req->dstAddressing.addrMode             = APS_EXT_ADDRESS;
            req->dstAddressing.addr.extAddress      = zigBeeStat.extAddress;
            req->dstAddressing.profileId            = PROFILE_ID_HOME_AUTOMATION;
            req->dstAddressing.endpointId           = zigBeeStat.thermostatEndPoint;
            req->dstAddressing.clusterId            = THERMOSTAT_UI_CONF_CLUSTER_ID;
            req->dstAddressing.clusterSide          = ZCL_CLUSTER_SIDE_SERVER;
            req->dstAddressing.manufacturerSpecCode = 0;
            req->dstAddressing.sequenceNumber       = ZCL_GetNextSeqNumber();

            req->ZCL_Notify = zigBeeStatNotificationResp;

            commandManagerSendAttribute(req);
        }
    }
}

/*******************************************************************************
* @brief  Send the Location Description attribute to the specified thermostat
* @inputs thermsotatIndex: specified thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/18
*******************************************************************************/
static void SendLocationDescriptionAttribute(uint8_t thermsotatIndex)
{
    //TODO
}

/*******************************************************************************
* @brief  Send the Time Format attribute to all devices
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/18
*******************************************************************************/
static void SendTimeFormatAttribute(void)
{
    ZCL_Request_t *req;
    ZCL_WriteAttributeReq_t * writeAttrReq;
    uint8_t* format;

    if (!(req = getFreeCommand()))
        return;


    writeAttrReq = (ZCL_WriteAttributeReq_t*)req->requestPayload;
    format = (uint8_t *)(req->requestPayload+(sizeof(ZCL_WriteAttributeReq_t )- sizeof(uint8_t)));

    writeAttrReq->id = ZCL_TIME_CLUSTER_TIME_FORMAT_MANUF_SPECIFIC_ATTRIBUTE_ID;
    writeAttrReq->type = ZCL_8BIT_ENUM_DATA_TYPE_ID;

    DBTHRD_GetData(DBTYPE_TIME_FORMAT, (void*)format, INDEX_DONT_CARE);

    req->id              = ZCL_REPORT_ATTRIBUTES_COMMAND_ID;//ZCL_WRITE_ATTRIBUTES_COMMAND_ID;
    req->requestLength   = sizeof(uint8_t) + sizeof(ZCL_WriteAttributeReq_t) - sizeof(uint8_t);
    req->endpointId      = APP_SRC_ENDPOINT_ID;
    req->defaultResponse = ZCL_FRAME_CONTROL_DISABLE_DEFAULT_RESPONSE;

    req->dstAddressing.addrMode             = APS_SHORT_ADDRESS;
    req->dstAddressing.addr.shortAddress    = BROADCAST_ADDR_ALL;
    req->dstAddressing.profileId            = PROFILE_ID_HOME_AUTOMATION;
    req->dstAddressing.endpointId           = APS_BROADCAST_ENDPOINT;
    req->dstAddressing.clusterId            = TIME_CLUSTER_ID;
    req->dstAddressing.clusterSide          = ZCL_CLUSTER_SIDE_CLIENT;
    req->dstAddressing.manufacturerSpecCode = CS_MANUFACTURER_CODE;
    req->dstAddressing.sequenceNumber       = ZCL_GetNextSeqNumber();

    req->ZCL_Notify = zigBeeStatNotificationResp;

    commandManagerSendAttribute(req);
}

/*******************************************************************************
* @brief  Send the Time attribute to all devices
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/18
*******************************************************************************/
static void SendTimeAttribute(void)
{
    ZCL_Request_t *req;
    ZCL_WriteAttributeReq_t * writeAttrReq;
    dbType_Time_t dbTime;
    dbType_Date_t dbDate;
    struct tm currentTime;
    time_t currentFrom1970Time;
    time_t* currentFrom2000Time;
    uint8_t applyDst;

    if (!(req = getFreeCommand()))
        return;


    writeAttrReq = (ZCL_WriteAttributeReq_t*)req->requestPayload;
    currentFrom2000Time = (time_t *)(req->requestPayload+(sizeof(ZCL_WriteAttributeReq_t )- sizeof(uint8_t)));

    writeAttrReq->id = ZCL_TIME_CLUSTER_SERVER_LOCAL_TIME_ATTRIBUTE_ID;
    writeAttrReq->type = ZCL_UTC_TIME_DATA_TYPE_ID;

    DBTHRD_GetData(DBTYPE_LOCALTIME, (void*)&dbTime, INDEX_DONT_CARE);
    DBTHRD_GetData(DBTYPE_DATE, (void*)&dbDate, INDEX_DONT_CARE);

    currentTime.tm_sec = dbTime.Seconds;
    currentTime.tm_min = dbTime.Minutes;
    currentTime.tm_hour = dbTime.Hours;
    currentTime.tm_mday = dbDate.Day;
    currentTime.tm_mon = dbDate.Month - 1;
    currentTime.tm_year = dbDate.Year - 1900;
    currentTime.tm_wday = dbDate.DayOfWeek;
    currentTime.tm_yday = dbDate.DayOfYear;
    currentTime.tm_isdst = 0;

    currentFrom1970Time = mktime(&currentTime);

    applyDst = 0;
    if (dbTime.IsDstValid)
    {
        if (dbTime.IsDstActive)
        {
            if (currentFrom1970Time < dbTime.DstChangeTime)
            {
                applyDst = 1;
            }
        }
        else
        {
            if (currentFrom1970Time > dbTime.DstChangeTime)
            {
                applyDst = 1;
            }
        }
    }

    currentTime.tm_isdst = applyDst;
    currentFrom1970Time = mktime(&currentTime);

    //currentFrom1970Time += ((dbTime.TimeZone*60) - (applyDst*60*60));

    *currentFrom2000Time = currentFrom1970Time - OFFSET_1970_TO_2000;

    req->id              = ZCL_REPORT_ATTRIBUTES_COMMAND_ID;//ZCL_WRITE_ATTRIBUTES_COMMAND_ID;
    req->requestLength   = sizeof(uint32_t) + sizeof(ZCL_WriteAttributeReq_t) - sizeof(uint8_t);
    req->endpointId      = APP_SRC_ENDPOINT_ID;
    req->defaultResponse = ZCL_FRAME_CONTROL_DISABLE_DEFAULT_RESPONSE;

    req->dstAddressing.addrMode             = APS_SHORT_ADDRESS;
    req->dstAddressing.addr.shortAddress    = BROADCAST_ADDR_ALL;
    req->dstAddressing.profileId            = PROFILE_ID_HOME_AUTOMATION;
    req->dstAddressing.endpointId           = APS_BROADCAST_ENDPOINT;
    req->dstAddressing.clusterId            = TIME_CLUSTER_ID;
    req->dstAddressing.clusterSide          = ZCL_CLUSTER_SIDE_CLIENT;
    req->dstAddressing.manufacturerSpecCode = 0;
    req->dstAddressing.sequenceNumber       = ZCL_GetNextSeqNumber();

    req->ZCL_Notify = zigBeeStatNotificationResp;

    commandManagerSendAttribute(req);
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/12/05
*******************************************************************************/
DB_NOTIFICATION(NotifyZBStat_GroupSetpoint)
{
    dbType_Group_t group;

    if (DBERR_OK == DBTHRD_GetData(DBTYPE_GROUP_INFORMATIONS, (void*)&group, instance))
    {
        for (uint8_t i = 0; i < THERMOSTAT_INSTANCES; i++)
        {
            if ((group.GroupMembers.MembersList & (1<<i)) != 0)
            {
                SendOccupiedHeatSetpointAttribute(i);
            }
        }

    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/12/05
*******************************************************************************/
DB_NOTIFICATION(NotifyZBStat_AmbientSetpoint)
{
    SendOccupiedHeatSetpointAttribute(instance);
}

/*******************************************************************************
* @brief  Send Unit Format on ZigBee network when there is a change
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/18
*******************************************************************************/
DB_NOTIFICATION(NotifyZBStat_UnitFormat)
{
    SendTemperatureDisplayModeAttribute();
}

/*******************************************************************************
* @brief  Send Outdoor Temperature on ZigBee network when there is a change
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/18
*******************************************************************************/
DB_NOTIFICATION(NotifyZBStat_OutdoorTemperature)
{
    SendOutdoorTemperatureAttribute();
}

/*******************************************************************************
* @brief  Send Lock State on ZigBee network when there is a change
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/18
*******************************************************************************/
DB_NOTIFICATION(NotifyZBStat_LockState)
{
    SendKeypadLockoutAttribute(instance);
}

/*******************************************************************************
* @brief  Send Thermostat Name on ZigBee network when there is a change
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/18
*******************************************************************************/
DB_NOTIFICATION(NotifyZBStat_TstatName)
{
    SendLocationDescriptionAttribute(instance);
}

/*******************************************************************************
* @brief  Send Time Format on ZigBee network when there is a change
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/18
*******************************************************************************/
DB_NOTIFICATION(NotifyZBStat_TimeFormat)
{
    SendTimeFormatAttribute();
}

/*******************************************************************************
* @brief  Send Local Time on ZigBee network when there is a change
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/18
*******************************************************************************/
DB_NOTIFICATION(NotifyZBStat_LocalTime)
{
    SendTimeAttribute();
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/**************************************************************************//**
\brief Indication of read attribute response

\param[in] resp - pointer to response
******************************************************************************/
void ZCL_ReadAttributeResp(ZCL_Notify_t *ntfy)
{
    ZCL_NextElement_t element;
    ZCL_ReadAttributeResp_t *readAttributeResp;
    uint8_t statIndex;
    ZCL_Status_t status;
    uint8_t str[32];
    Lock_t lock;
    ZCL_ThUiConfKeypadLockOut_t systemLock;
    uint16_t humidity;
    uint8_t alert;
    uint8_t newAlert;
    dbType_Alerts_t currentAlert;
    uint8_t i;
    uint8_t alertInstance;

    if (ZCL_SUCCESS_STATUS == ntfy->status)
    {
        element.id            = ZCL_READ_ATTRIBUTES_RESPONSE_COMMAND_ID;
        element.payloadLength = ntfy->responseLength;
        element.payload       = ntfy->responsePayload;
        element.content       = NULL;

        do
        {
            status = ZCL_GetNextElement(&element);

            if ((status != ZCL_INVALID_PARAMETER_STATUS) && (status != ZCL_INVALID_VALUE_STATUS))
            {
                readAttributeResp = (ZCL_ReadAttributeResp_t *) element.content;

                if (DBERR_NONEXISTING_STAT != DBTHRD_GetData (DBTYPE_INDEX_FROM_ZBSHORTADDR, (void*)&statIndex, ntfy->addressing->addr.shortAddress))
                {
                    switch (ntfy->addressing->clusterId)
                    {
                        case THERMOSTAT_CLUSTER_ID:
                            switch (readAttributeResp->id)
                            {
                                case ZCL_THERMOSTAT_CLUSTER_LOCAL_TEMPERATURE_SERVER_ATTRIBUTE_ID:
                                {
                                    DBTHRD_SetData(DBTYPE_AMBIENT_TEMPERATURE,
                                                (void*)&readAttributeResp->value[0],
                                                statIndex,
                                                DBCHANGE_ZIGBEE);
                                    ThermostatIsAlive(statIndex);
                                }
                                break;

                                case ZCL_THERMOSTAT_CLUSTER_OCCUPIED_HEATING_SETPOINT_SERVER_ATTRIBUTE_ID:
                                {
                                    DBTHRD_SetData(DBTYPE_AMBIENT_SETPOINT,
                                            (void*)&readAttributeResp->value[0],
                                            statIndex,
                                            DBCHANGE_ZIGBEE);
                                }
                                break;

                                case ZCL_THERMOSTAT_CLUSTER_PI_HEATING_DEMAND_SERVER_ATTRIBUTE_ID:
                                {
                                    DBTHRD_SetData(DBTYPE_HEAT_DEMAND,
                                            (void*)&readAttributeResp->value[0],
                                            statIndex,
                                            DBCHANGE_ZIGBEE);
                                }
                                break;

                                case ZCL_THERMOSTAT_CLUSTER_ALERT_MANUF_SPECIFIC_ATTRIBUTE_ID:
                                {
                                    alert = readAttributeResp->value[0];
                                    DBTHRD_GetData(DBTYPE_ACTIVE_ALERTS, (void*)&currentAlert, statIndex);
                                    for (i = 0; i < ALERT_INSTANCES; i++)
                                    {
                                        newAlert = 0;
                                        if (alert & (1<<i))
                                        {
                                            newAlert = DB_SET_ALERT | (i + 1);
                                        }
                                        else
                                        {
                                            for (alertInstance = 0; alertInstance < ALERT_INSTANCES; alertInstance++)
                                            {
                                                if (currentAlert.ActiveAlerts[alertInstance] == (i + 1))
                                                {
                                                    newAlert = DB_CLEAR_ALERT | (i + 1);
                                                }
                                            }
                                        }
                                        if (newAlert != 0)
                                        {
                                            DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,
                                                (void*)&newAlert,
                                                statIndex,
                                                DBCHANGE_ZIGBEE);
                                        }
                                    }
                                }
                                break;

                                default:
                                {
                                }
                                break;
                            }
                            break;

                        case BASIC_CLUSTER_ID:
                            switch (readAttributeResp->id)
                            {
                                case ZCL_BASIC_CLUSTER_SERVER_MODEL_IDENTIFIER_ATTRIBUTE_ID:
                                {
                                    DBTHRD_SetData(DBTYPE_TSTAT_MODEL,
                                            (void*)&readAttributeResp->value[1],
                                            statIndex,
                                            DBCHANGE_ZIGBEE);
                                }
                                break;

                                case ZCL_BASIC_CLUSTER_SERVER_LOCATION_DESCRIPTION_ATTRIBUTE_ID:
                                {
                                    sprintf((char*)str, "%s_%d",(char const*)&readAttributeResp->value[1], statIndex);
                                    DBTHRD_SetData(DBTYPE_TSTAT_NAME,
                                            (void*)&str[0],
                                            statIndex,
                                            DBCHANGE_ZIGBEE);
                                }
                                break;

                                case ZCL_BASIC_CLUSTER_SERVER_APPLICATION_VERSION_ATTRIBUTE_ID:
                                {
                                    DBTHRD_SetData(DBTYPE_TSTAT_VERSION,
                                            (void*)&readAttributeResp->value[0],
                                            statIndex,
                                            DBCHANGE_ZIGBEE);
                                }
                                break;

                                case ZCL_BASIC_CLUSTER_SERVER_STACK_VERSION_ATTRIBUTE_ID:
                                {
                                    DBTHRD_SetData(DBTYPE_ZIGBEE_VERSION,
                                            (void*)&readAttributeResp->value[0],
                                            statIndex,
                                            DBCHANGE_ZIGBEE);
                                }
                                break;

                                default:
                                {
                                }
                                break;
                            }
                            break;

                        case THERMOSTAT_UI_CONF_CLUSTER_ID:
                            switch (readAttributeResp->id)
                            {
                                case ZCL_THERMOSTAT_UI_CONF_CLUSTER_KEYPAD_LOCKOUT_SERVER_ATTRIBUTE_ID:
                                {
                                    systemLock = (ZCL_ThUiConfKeypadLockOut_t)readAttributeResp->value[0];
                                    if (systemLock == ZCL_NO_LOCKOUT)
                                    {
                                        lock = LOCK_OFF;
                                    }
                                    else
                                    {
                                        lock = LOCK_ON;
                                    }
                                    DBTHRD_SetData(DBTYPE_LOCK_STATE,
                                                    (void*)&lock,
                                                    statIndex,
                                                    DBCHANGE_ZIGBEE);
                                }
                                break;

                                default:
                                {
                                }
                                break;
                            }
                            break;

                        case HUMIDITY_MEASUREMENT_CLUSTER_ID:
                            switch (readAttributeResp->id)
                            {
                                case ZCL_HUMIDITY_MEASUREMENT_CLUSTER_SERVER_MEASURED_VALUE_ATTRIBUTE_ID:
                                {
                                    humidity = (readAttributeResp->value[0] | (readAttributeResp->value[1] << 8)) /100;
                                    DBTHRD_SetData(DBTYPE_AMBIENT_HUMIDITY,
                                                    (void*)&humidity,
                                                    statIndex,
                                                    DBCHANGE_ZIGBEE);
                                }
                                break;

                                default:
                                {
                                }
                                break;
                            }
                            break;

                        default:
                        {
                        }
                        break;
                    }
                }
            }
            else
            {
                break;
            }
        }
        while (ZCL_END_PAYLOAD_REACHED_STATUS != status);

    }
    else
    {
    }
    ManageReadAttributeResp(ntfy);
}

/**************************************************************************//**
\brief InitControllerEndpoint
\param[in] resp - pointer to response
******************************************************************************/
void InitControllerEndpoint (void)
{
    ZCL_Cluster_t *cluster;

    ZCL_RegisterEndpoint(&ControllerEndpoint);
    deviceBindReqs[0] = &ControllerBindReq;

    /********  Init Thermostat Cluster *******/
    cluster = ZCL_GetCluster(CTL_SRC_ENDPOINT_ID, THERMOSTAT_CLUSTER_ID, ZCL_CLUSTER_SIDE_CLIENT);
    if (cluster)
    {
        cluster->ZCL_ReportInd = ThermostatReportInd;
        cluster->ZCL_AttributeEventInd = ThermostatAttrEventInd;
    }

    /********  Init Thermostat UI Config Cluster *******/
    cluster = ZCL_GetCluster(CTL_SRC_ENDPOINT_ID, THERMOSTAT_UI_CONF_CLUSTER_ID, ZCL_CLUSTER_SIDE_CLIENT);
    if (cluster)
    {
        cluster->ZCL_ReportInd = ThermostatUIConfigReportInd;
        cluster->ZCL_AttributeEventInd = ThermostatUIConfigAttrEventInd;
    }

    /********  Init Humidity Measurement Cluster *******/
    cluster = ZCL_GetCluster(CTL_SRC_ENDPOINT_ID, HUMIDITY_MEASUREMENT_CLUSTER_ID, ZCL_CLUSTER_SIDE_CLIENT);
    if (cluster)
    {
        cluster->ZCL_ReportInd = HumidityMeasurementReportInd;
        cluster->ZCL_AttributeEventInd = HumidityMeasurementAttrEventInd;
    }


    DBTHRD_SubscribeToNotification( DBTYPE_AMBIENT_SETPOINT,
                                   NotifyZBStat_AmbientSetpoint,
                                   (dbChangeSource_t)(DBCHANGE_LOCAL|DBCHANGE_CLOUD),
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification( DBTYPE_GROUP_SETPOINT,
                                   NotifyZBStat_GroupSetpoint,
                                   (dbChangeSource_t)(DBCHANGE_ALLCHANGES&~DBCHANGE_ZIGBEE),
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification( DBTYPE_TEMPERATURE_FORMAT,
                                   NotifyZBStat_UnitFormat,
                                   (dbChangeSource_t)(DBCHANGE_ALLCHANGES&~DBCHANGE_ZIGBEE),
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification( DBTYPE_OUTDOOR_TEMPERATURE,
                                   NotifyZBStat_OutdoorTemperature,
                                   (dbChangeSource_t)(DBCHANGE_ALLCHANGES&~DBCHANGE_ZIGBEE),
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification( DBTYPE_LOCK_STATE,
                                   NotifyZBStat_LockState,
                                   (dbChangeSource_t)(DBCHANGE_ALLCHANGES&~DBCHANGE_ZIGBEE),
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification( DBTYPE_TSTAT_NAME,
                                   NotifyZBStat_TstatName,
                                   (dbChangeSource_t)(DBCHANGE_ALLCHANGES&~DBCHANGE_ZIGBEE),
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification( DBTYPE_TIME_FORMAT,
                                   NotifyZBStat_TimeFormat,
                                   (dbChangeSource_t)(DBCHANGE_ALLCHANGES&~DBCHANGE_ZIGBEE),
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification( DBTYPE_MINUTE_CHANGE,
                                   NotifyZBStat_LocalTime,
                                   (dbChangeSource_t)(DBCHANGE_ALLCHANGES&~DBCHANGE_ZIGBEE),
                                   INDEX_DONT_CARE);

    ZigBeeTstatTimeout = xTimerCreate ("ZigBee Tstat Timeout",
                                       pdMS_TO_TICKS( ZIGBEE_TSTAT_TIMEOUT ),
                                       pdTRUE,     //auto-reload
                                       (void*)0,
                                       OnZigBeeTstatTimeout);
    xTimerStart(ZigBeeTstatTimeout, 0);
    basicClusterInit();
    identifyClusterInit();
    temperatureMeasurementClusterInit();
    humidityMeasurementClusterInit();
    thermostatClusterInit();
    timeClusterInit();
}

/**************************************************************************//**
\brief Gets bind request

\return pointer to a bind request used by HA device
******************************************************************************/
AppBindReq_t **getDeviceBindRequest(void)
{
  return deviceBindReqs;
}

/**************************************************************************//**
\brief Performs security initialization actions
******************************************************************************/
void ControllerSecurityInit(void)
{
  ZCL_Set_t zclSet;
  ExtAddr_t trustCenterAddress;
  ExtAddr_t macAddr;
  APS_TcMode_t tcMode;
  DeviceType_t deviceType;
  // Setup security parameters
  bool securityOn;

  CS_ReadParameter(CS_SECURITY_ON_ID, &securityOn);
  if (securityOn)
  {
    uint8_t preconfiguredStatus;
    CS_ReadParameter(CS_APS_TRUST_CENTER_ADDRESS_ID, &trustCenterAddress);
    CS_ReadParameter(CS_UID_ID, &macAddr);
    if (IS_EQ_EXT_ADDR(macAddr, trustCenterAddress))
      tcMode = APS_CENTRALIZED_TRUST_CENTER;
    else
      tcMode = APS_NOT_TRUST_CENTER;

    CS_ReadParameter(CS_ZDO_SECURITY_STATUS_ID, &preconfiguredStatus);
    CS_ReadParameter(CS_DEVICE_TYPE_ID, &deviceType);

    if ((PRECONFIGURED_NETWORK_KEY == preconfiguredStatus)
      || (APS_CENTRALIZED_TRUST_CENTER == tcMode)
    )
    {
      uint8_t nwkDefaultKey[SECURITY_KEY_SIZE];

      CS_ReadParameter(CS_NETWORK_KEY_ID, &nwkDefaultKey);
      NWK_SetKey(nwkDefaultKey, NWK_STARTUP_ACTIVE_KEY_SEQUENCE_NUMBER);
      NWK_ActivateKey(NWK_STARTUP_ACTIVE_KEY_SEQUENCE_NUMBER);
    }
  }

  ZCL_ResetSecurity();

  //Setting the Link Key Descriptor
  zclSet.attr.id = ZCL_LINK_KEY_DESC_ID;
  zclSet.attr.value.linkKeyDesc = &lightKeyDesc;
  ZCL_Set(&zclSet);
}

/**************************************************************************//**
\brief ZDO Binding indication function

\param[out] bindInd - ZDO bind indication parameters structure pointer
******************************************************************************/
void ZDO_BindIndication(ZDO_BindInd_t *bindInd)
{
  (void)bindInd;
}

/**************************************************************************//**
\brief ZDO Unbinding indication function

\param[out] unbindInd - ZDO unbind indication parameters structure pointer
******************************************************************************/
void ZDO_UnbindIndication(ZDO_UnbindInd_t *unbindInd)
{
  (void)unbindInd;
}
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
