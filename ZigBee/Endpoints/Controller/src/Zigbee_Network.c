/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    Zigbee_Network.c
* @date    2016/11/24
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>

#include ".\Endpoints\Controller\include\Zigbee_Network.h"
#include ".\Endpoints\Controller\include\ControllerEndpoint.h"

#include "zclDevice.h"
#include "zdo.h"
#include "zcl.h"
#include "haClusters.h"
#include "clusters.h"
#include "zclThermostatCluster.h"
#include "zclThermostatUIConfCluster.h"
#include "zclBasicCluster.h"
#include "zclHumidityMeasurementCluster.h"
#include "commandManager.h"

#include ".\DB\inc\THREAD_DB.h"
#include ".\DB\classes\inc\class_Thermostats.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define MAX_ATTRIBUTE_SETUP 8

typedef struct
{
    ClusterId_t ClusterId;
    uint8_t BindingStatus;
    ZCL_AttributeId_t AttributeToConfigureForReporting[MAX_ATTRIBUTE_SETUP];
    ZCL_AttributeId_t AttributeToRead[MAX_ATTRIBUTE_SETUP];
    uint8_t NbAttributeToConfigure;
    uint8_t NbAttributeToRead;
} ClusterList_t;

typedef struct
{
    uint8_t MatchNumber;
    Endpoint_t Endpoint;
    APS_AddrMode_t AddrMode;
    ExtAddr_t ExtAddress;
    APS_Address_t Address;
    ClusterList_t ClusterList[CTL_ENDPOINT_CLIENT_CLUSTERS_COUNT];
    uint8_t NbBindCluster;
} SetupTable_t;

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
static DescModeManagerMem_t descModeMem;
static SetupTable_t SetupTable;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static void zdpMatchDescResponse(ZDO_ZdpResp_t *resp);
static void zdpMatchDescReq(ShortAddr_t addr,uint8_t ep);
static void zdpSimpleDescReq(ShortAddr_t addr,uint8_t ep);
static void zdpSimpleDescResponse(ZDO_ZdpResp_t *resp);
static void SendBindRequests(void);
static void ManageBindResp(ZDO_ZdpResp_t *resp);
static void SendConfigureReportingRequests(void);
static void ManageConfigureReportResp(ZCL_Notify_t *ntfy);
static void SendReadAttributeRequests(void);
static void FlushThermostat(ZDO_ZdpResp_t *zdpResp);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
\brief Sends the simple Descriptor request

\param[in] addr - nwk Address of Interest
\param[in] ep   - Endpoint for which the simple desctiptor is requested
*****************************************************************************/
static void zdpSimpleDescReq(ShortAddr_t addr,uint8_t ep)
{
  ZDO_ZdpReq_t *zdpReq = &descModeMem.zdpReq;
  ZDO_SimpleDescReq_t *simpleDescReq = &zdpReq->req.reqPayload.simpleDescReq;

  zdpReq->ZDO_ZdpResp              = zdpSimpleDescResponse;
  zdpReq->reqCluster               = SIMPLE_DESCRIPTOR_CLID;
  zdpReq->dstAddrMode              = APS_SHORT_ADDRESS;
  zdpReq->dstAddress.shortAddress  = addr;
  simpleDescReq->nwkAddrOfInterest = addr;
  simpleDescReq->endpoint          = ep;
  ZDO_ZdpReq(zdpReq);
}

/**************************************************************************//**
\brief simple Descriptor response callback

\param[in] resp - response payload
******************************************************************************/
static void zdpSimpleDescResponse(ZDO_ZdpResp_t *resp)
{
    ZDO_SimpleDescResp_t * simpleDescrResp = (ZDO_SimpleDescResp_t*)&resp->respPayload.simpleDescResp;

    AppBindReq_t **appBindRequest   = getDeviceBindRequest();

    SetupTable.MatchNumber = 0;

    for (uint8_t inClusters = 0; inClusters < simpleDescrResp->simpleDescriptor.AppInClustersCount; inClusters++)
    {
        for (uint8_t remoteClusters = 0; remoteClusters < appBindRequest[0]->remoteServersCnt; remoteClusters++)
        {
            if (simpleDescrResp->simpleDescriptor.AppInClustersList[inClusters] == appBindRequest[0]->remoteServers[remoteClusters])
            {
                SetupTable.ClusterList[SetupTable.MatchNumber++].ClusterId = simpleDescrResp->simpleDescriptor.AppInClustersList[inClusters];
                break;
            }
        }
    }

    SetupTable.NbBindCluster = 0;
    SetupTable.Endpoint = simpleDescrResp->simpleDescriptor.endpoint;
    SetupTable.AddrMode = resp->srcAddrMode;
    SetupTable.ExtAddress = resp->srcAddress.extAddress;

    SendBindRequests();
}

/**************************************************************************//**
\brief Send all Bind requests, but one at a time

\param[in] none
******************************************************************************/
static void SendBindRequests(void)
{
    ZDO_ZdpReq_t *zdpReq = &descModeMem.zdpReq;
    ZDO_BindReq_t *bindReq = &zdpReq->req.reqPayload.bindReq;

    AppBindReq_t **appBindRequest   = getDeviceBindRequest();
    ExtAddr_t ownExtAddr;
    zigBeeDescriptor_t DB_DeviceDescriptor;

    bindReq->clusterId = SetupTable.ClusterList[SetupTable.NbBindCluster].ClusterId;

    zdpReq->ZDO_ZdpResp = ManageBindResp;
    zdpReq->reqCluster = BIND_CLID;
    zdpReq->dstAddrMode = SetupTable.AddrMode;
    COPY_EXT_ADDR(zdpReq->dstAddress.extAddress, SetupTable.ExtAddress);

    CS_ReadParameter(CS_UID_ID, &ownExtAddr);

    bindReq->srcAddr = SetupTable.ExtAddress;
    bindReq->srcEndpoint = SetupTable.Endpoint;
    bindReq->dstAddrMode = APS_EXT_ADDRESS;
    COPY_EXT_ADDR(bindReq->dstExtAddr, ownExtAddr);
    bindReq->dstEndpoint = appBindRequest[0]->srcEndpoint;

    ZDO_ZdpReq(zdpReq);

    if (SetupTable.NbBindCluster == 0)
    {
        DB_DeviceDescriptor.shortAddress = zdpReq->dstAddress.shortAddress;
        DB_DeviceDescriptor.extAddress = zdpReq->dstAddress.extAddress;
        DB_DeviceDescriptor.thermostatEndPoint = SetupTable.Endpoint;
        DBTHRD_SetData(DBTYPE_ZIGBEE_DESCRIPTOR,
                       (void*)&DB_DeviceDescriptor,
                       zdpReq->dstAddress.shortAddress,
                       DBCHANGE_ZIGBEE);
    }
}

/**************************************************************************//**
\brief Manages all Bind responses

\param[in] ZDO_ZdpResp_t *resp
******************************************************************************/
static void ManageBindResp(ZDO_ZdpResp_t *resp)
{
    if (resp->respPayload.bindResp.status == ZDO_SUCCESS_STATUS)
    {
        switch(SetupTable.ClusterList[SetupTable.NbBindCluster].ClusterId)
        {
            case THERMOSTAT_CLUSTER_ID:
                SetupTable.ClusterList[SetupTable.NbBindCluster].BindingStatus = 1;
                SetupTable.ClusterList[SetupTable.NbBindCluster].NbAttributeToConfigure = 5;
                SetupTable.ClusterList[SetupTable.NbBindCluster].AttributeToConfigureForReporting[0] = ZCL_THERMOSTAT_CLUSTER_LOCAL_TEMPERATURE_SERVER_ATTRIBUTE_ID;
                SetupTable.ClusterList[SetupTable.NbBindCluster].AttributeToConfigureForReporting[1] = ZCL_THERMOSTAT_CLUSTER_OCCUPIED_HEATING_SETPOINT_SERVER_ATTRIBUTE_ID;
                SetupTable.ClusterList[SetupTable.NbBindCluster].AttributeToConfigureForReporting[2] = ZCL_THERMOSTAT_CLUSTER_PI_HEATING_DEMAND_SERVER_ATTRIBUTE_ID;
                SetupTable.ClusterList[SetupTable.NbBindCluster].AttributeToConfigureForReporting[3] = ZCL_THERMOSTAT_CLUSTER_POWER_CONSUMPTION_MANUF_SPECIFIC_ATTRIBUTE_ID;
                SetupTable.ClusterList[SetupTable.NbBindCluster].AttributeToConfigureForReporting[4] = ZCL_THERMOSTAT_CLUSTER_ALERT_MANUF_SPECIFIC_ATTRIBUTE_ID;
                SetupTable.ClusterList[SetupTable.NbBindCluster].NbAttributeToRead = 4;
                SetupTable.ClusterList[SetupTable.NbBindCluster].AttributeToRead[0] = ZCL_THERMOSTAT_CLUSTER_LOCAL_TEMPERATURE_SERVER_ATTRIBUTE_ID;
                SetupTable.ClusterList[SetupTable.NbBindCluster].AttributeToRead[1] = ZCL_THERMOSTAT_CLUSTER_OCCUPIED_HEATING_SETPOINT_SERVER_ATTRIBUTE_ID;
                SetupTable.ClusterList[SetupTable.NbBindCluster].AttributeToRead[2] = ZCL_THERMOSTAT_CLUSTER_PI_HEATING_DEMAND_SERVER_ATTRIBUTE_ID;
                SetupTable.ClusterList[SetupTable.NbBindCluster].AttributeToRead[3] = ZCL_THERMOSTAT_CLUSTER_ALERT_MANUF_SPECIFIC_ATTRIBUTE_ID;
                break;

            case BASIC_CLUSTER_ID:
                SetupTable.ClusterList[SetupTable.NbBindCluster].BindingStatus = 1;
                SetupTable.ClusterList[SetupTable.NbBindCluster].NbAttributeToConfigure = 0;
                SetupTable.ClusterList[SetupTable.NbBindCluster].NbAttributeToRead = 4;
                SetupTable.ClusterList[SetupTable.NbBindCluster].AttributeToRead[0] = ZCL_BASIC_CLUSTER_SERVER_MODEL_IDENTIFIER_ATTRIBUTE_ID;
                SetupTable.ClusterList[SetupTable.NbBindCluster].AttributeToRead[1] = ZCL_BASIC_CLUSTER_SERVER_LOCATION_DESCRIPTION_ATTRIBUTE_ID;
                SetupTable.ClusterList[SetupTable.NbBindCluster].AttributeToRead[2] = ZCL_BASIC_CLUSTER_SERVER_APPLICATION_VERSION_ATTRIBUTE_ID;
                SetupTable.ClusterList[SetupTable.NbBindCluster].AttributeToRead[3] = ZCL_BASIC_CLUSTER_SERVER_STACK_VERSION_ATTRIBUTE_ID;
                break;

            case IDENTIFY_CLUSTER_ID:
                SetupTable.ClusterList[SetupTable.NbBindCluster].BindingStatus = 1;
                SetupTable.ClusterList[SetupTable.NbBindCluster].NbAttributeToConfigure = 0;
                SetupTable.ClusterList[SetupTable.NbBindCluster].NbAttributeToRead = 0;
                break;

            case GROUPS_CLUSTER_ID:
                SetupTable.ClusterList[SetupTable.NbBindCluster].BindingStatus = 1;
                SetupTable.ClusterList[SetupTable.NbBindCluster].NbAttributeToConfigure = 0;
                SetupTable.ClusterList[SetupTable.NbBindCluster].NbAttributeToRead = 0;
                break;

            case THERMOSTAT_UI_CONF_CLUSTER_ID:
                SetupTable.ClusterList[SetupTable.NbBindCluster].BindingStatus = 1;
                SetupTable.ClusterList[SetupTable.NbBindCluster].NbAttributeToConfigure = 2;
                SetupTable.ClusterList[SetupTable.NbBindCluster].AttributeToConfigureForReporting[0] = ZCL_THERMOSTAT_UI_CONF_CLUSTER_TEMPERATURE_DISPLAY_MODE_SERVER_ATTRIBUTE_ID;
                SetupTable.ClusterList[SetupTable.NbBindCluster].AttributeToConfigureForReporting[1] = ZCL_THERMOSTAT_UI_CONF_CLUSTER_KEYPAD_LOCKOUT_SERVER_ATTRIBUTE_ID;
                SetupTable.ClusterList[SetupTable.NbBindCluster].NbAttributeToRead = 1;
                SetupTable.ClusterList[SetupTable.NbBindCluster].AttributeToRead[0] = ZCL_THERMOSTAT_UI_CONF_CLUSTER_KEYPAD_LOCKOUT_SERVER_ATTRIBUTE_ID;
                break;

            case HUMIDITY_MEASUREMENT_CLUSTER_ID:
                SetupTable.ClusterList[SetupTable.NbBindCluster].BindingStatus = 1;
                SetupTable.ClusterList[SetupTable.NbBindCluster].NbAttributeToConfigure = 1;
                SetupTable.ClusterList[SetupTable.NbBindCluster].AttributeToConfigureForReporting[0] = ZCL_HUMIDITY_MEASUREMENT_CLUSTER_SERVER_MEASURED_VALUE_ATTRIBUTE_ID;
                SetupTable.ClusterList[SetupTable.NbBindCluster].NbAttributeToRead = 1;
                SetupTable.ClusterList[SetupTable.NbBindCluster].AttributeToRead[0] = ZCL_HUMIDITY_MEASUREMENT_CLUSTER_SERVER_MEASURED_VALUE_ATTRIBUTE_ID;
                break;

            default:
                break;
        }

        SetupTable.NbBindCluster++;
        SetupTable.Address = resp->srcAddress;
        SetupTable.AddrMode = resp->srcAddrMode;

        if (SetupTable.NbBindCluster < SetupTable.MatchNumber)
        {
            SendBindRequests();
        }
        else
        {
            SendConfigureReportingRequests();
        }
    }
}

/**************************************************************************//**
\brief Sends all Configuration Reporting requests, but one at a time

\param[in] None
******************************************************************************/
static void SendConfigureReportingRequests(void)
{
    ZCL_Request_t *req;
    ZCL_NextElement_t element;
    ZCL_ConfigureReportingReq_t configureReportingReq;
    uint8_t cluster;
    uint8_t attributeFound;
    uint16_t manufSpecCode = 0;

    attributeFound = 0;
    for (cluster = 0; cluster < SetupTable.MatchNumber; cluster++)
    {
        if (SetupTable.ClusterList[cluster].BindingStatus == 1)
        {
            if (SetupTable.ClusterList[cluster].NbAttributeToConfigure != 0)
            {
                switch (SetupTable.ClusterList[cluster].ClusterId)
                {
                    case THERMOSTAT_CLUSTER_ID:
                        switch (SetupTable.ClusterList[cluster].AttributeToConfigureForReporting[SetupTable.ClusterList[cluster].NbAttributeToConfigure - 1])
                        {
                            case ZCL_THERMOSTAT_CLUSTER_LOCAL_TEMPERATURE_SERVER_ATTRIBUTE_ID:
                                attributeFound = 1;
                                configureReportingReq.direction            = ZCL_FRAME_CONTROL_DIRECTION_CLIENT_TO_SERVER;
                                configureReportingReq.attributeId          = ZCL_THERMOSTAT_CLUSTER_LOCAL_TEMPERATURE_SERVER_ATTRIBUTE_ID;
                                configureReportingReq.attributeType        = ZCL_S16BIT_DATA_TYPE_ID;
                                configureReportingReq.minReportingInterval = 10;
                                configureReportingReq.maxReportingInterval = 60;
                                configureReportingReq.reportableChange[0]  = 50;
                                configureReportingReq.reportableChange[1]  = 0;
                                break;

                            case ZCL_THERMOSTAT_CLUSTER_OCCUPIED_HEATING_SETPOINT_SERVER_ATTRIBUTE_ID:
                                attributeFound = 1;
                                configureReportingReq.direction            = ZCL_FRAME_CONTROL_DIRECTION_CLIENT_TO_SERVER;
                                configureReportingReq.attributeId          = ZCL_THERMOSTAT_CLUSTER_OCCUPIED_HEATING_SETPOINT_SERVER_ATTRIBUTE_ID;
                                configureReportingReq.attributeType        = ZCL_S16BIT_DATA_TYPE_ID;
                                configureReportingReq.minReportingInterval = 1;
                                configureReportingReq.maxReportingInterval = 0;
                                configureReportingReq.reportableChange[0]  = 50;
                                configureReportingReq.reportableChange[1]  = 0;
                                break;

                            case ZCL_THERMOSTAT_CLUSTER_PI_HEATING_DEMAND_SERVER_ATTRIBUTE_ID:
                                attributeFound = 1;
                                configureReportingReq.direction            = ZCL_FRAME_CONTROL_DIRECTION_CLIENT_TO_SERVER;
                                configureReportingReq.attributeId          = ZCL_THERMOSTAT_CLUSTER_PI_HEATING_DEMAND_SERVER_ATTRIBUTE_ID;
                                configureReportingReq.attributeType        = ZCL_U8BIT_DATA_TYPE_ID;
                                configureReportingReq.minReportingInterval = 15;
                                configureReportingReq.maxReportingInterval = 300;
                                configureReportingReq.reportableChange[0]  = 5;
                                break;

                            case ZCL_THERMOSTAT_CLUSTER_POWER_CONSUMPTION_MANUF_SPECIFIC_ATTRIBUTE_ID:
                                attributeFound = 1;
                                manufSpecCode = CS_MANUFACTURER_CODE;
                                configureReportingReq.direction            = ZCL_FRAME_CONTROL_DIRECTION_CLIENT_TO_SERVER;
                                configureReportingReq.attributeId          = ZCL_THERMOSTAT_CLUSTER_POWER_CONSUMPTION_MANUF_SPECIFIC_ATTRIBUTE_ID;
                                configureReportingReq.attributeType        = ZCL_U16BIT_DATA_TYPE_ID;
                                configureReportingReq.minReportingInterval = 3600;  //1 hour
                                configureReportingReq.maxReportingInterval = 43200; //12 hours
                                configureReportingReq.reportableChange[0]  = 1;
                                break;

                            case ZCL_THERMOSTAT_CLUSTER_ALERT_MANUF_SPECIFIC_ATTRIBUTE_ID:
                                attributeFound = 1;
                                manufSpecCode = CS_MANUFACTURER_CODE;
                                configureReportingReq.direction            = ZCL_FRAME_CONTROL_DIRECTION_CLIENT_TO_SERVER;
                                configureReportingReq.attributeId          = ZCL_THERMOSTAT_CLUSTER_ALERT_MANUF_SPECIFIC_ATTRIBUTE_ID;
                                configureReportingReq.attributeType        = ZCL_U8BIT_DATA_TYPE_ID;
                                configureReportingReq.minReportingInterval = 1;
                                configureReportingReq.maxReportingInterval = 0;
                                configureReportingReq.reportableChange[0]  = 1;
                                break;

                            default:
                                break;
                        }
                        break;

                    case THERMOSTAT_UI_CONF_CLUSTER_ID:
                        switch (SetupTable.ClusterList[cluster].AttributeToConfigureForReporting[SetupTable.ClusterList[cluster].NbAttributeToConfigure - 1])
                        {
                            case ZCL_THERMOSTAT_UI_CONF_CLUSTER_TEMPERATURE_DISPLAY_MODE_SERVER_ATTRIBUTE_ID:
                                attributeFound = 1;
                                configureReportingReq.direction            = ZCL_FRAME_CONTROL_DIRECTION_CLIENT_TO_SERVER;
                                configureReportingReq.attributeId          = ZCL_THERMOSTAT_UI_CONF_CLUSTER_TEMPERATURE_DISPLAY_MODE_SERVER_ATTRIBUTE_ID;
                                configureReportingReq.attributeType        = ZCL_8BIT_ENUM_DATA_TYPE_ID;
                                configureReportingReq.minReportingInterval = 1;
                                configureReportingReq.maxReportingInterval = 0;
                                configureReportingReq.reportableChange[0]  = 1;
                                break;

                            case ZCL_THERMOSTAT_UI_CONF_CLUSTER_KEYPAD_LOCKOUT_SERVER_ATTRIBUTE_ID:
                                attributeFound = 1;
                                configureReportingReq.direction            = ZCL_FRAME_CONTROL_DIRECTION_CLIENT_TO_SERVER;
                                configureReportingReq.attributeId          = ZCL_THERMOSTAT_UI_CONF_CLUSTER_KEYPAD_LOCKOUT_SERVER_ATTRIBUTE_ID;
                                configureReportingReq.attributeType        = ZCL_8BIT_ENUM_DATA_TYPE_ID;
                                configureReportingReq.minReportingInterval = 1;
                                configureReportingReq.maxReportingInterval = 0;
                                configureReportingReq.reportableChange[0]  = 1;
                                break;

                            default:
                                break;
                        }
                        break;

                    case HUMIDITY_MEASUREMENT_CLUSTER_ID:
                        switch (SetupTable.ClusterList[cluster].AttributeToConfigureForReporting[SetupTable.ClusterList[cluster].NbAttributeToConfigure - 1])
                        {
                            case ZCL_HUMIDITY_MEASUREMENT_CLUSTER_SERVER_MEASURED_VALUE_ATTRIBUTE_ID:
                                attributeFound = 1;
                                configureReportingReq.direction            = ZCL_FRAME_CONTROL_DIRECTION_CLIENT_TO_SERVER;
                                configureReportingReq.attributeId          = ZCL_HUMIDITY_MEASUREMENT_CLUSTER_SERVER_MEASURED_VALUE_ATTRIBUTE_ID;
                                configureReportingReq.attributeType        = ZCL_U16BIT_DATA_TYPE_ID;
                                configureReportingReq.minReportingInterval = 10;
                                configureReportingReq.maxReportingInterval = 60;
                                break;

                            default:
                                break;
                        }
                        break;

                    default:
                        break;
                }
                if (attributeFound != 0)
                {
                    break;
                }
            }
        }
    }
    //Check if there another attribute needs to be configured for reporting
    if (attributeFound != 0)
    {
        if (!(req = getFreeCommand()))
        return;

        element.payloadLength = 0;
        element.payload = req->requestPayload;
        element.id = ZCL_CONFIGURE_REPORTING_COMMAND_ID;
        element.content = &configureReportingReq;

        ZCL_PutNextElement(&element);

        req->id              = ZCL_CONFIGURE_REPORTING_COMMAND_ID;
        req->requestLength   = element.payloadLength;
        req->endpointId      = CTL_SRC_ENDPOINT_ID;
        req->defaultResponse = ZCL_FRAME_CONTROL_DISABLE_DEFAULT_RESPONSE;

        req->dstAddressing.addrMode             = SetupTable.AddrMode;
        req->dstAddressing.addr                 = SetupTable.Address;
        req->dstAddressing.profileId            = PROFILE_ID_HOME_AUTOMATION;
        req->dstAddressing.endpointId           = SetupTable.Endpoint;
        req->dstAddressing.clusterId            = SetupTable.ClusterList[cluster].ClusterId;
        req->dstAddressing.clusterSide          = ZCL_CLUSTER_SIDE_SERVER;
        req->dstAddressing.manufacturerSpecCode = manufSpecCode;
        req->dstAddressing.sequenceNumber       = ZCL_GetNextSeqNumber();

        req->ZCL_Notify = ManageConfigureReportResp;

        commandManagerSendAttribute(req);
    }
    else
    {
        //Configuration reporting finished, now lets read attributes
        SendReadAttributeRequests();
    }
}

/**************************************************************************//**
\brief Manages all Configuration Reporting responses

\param[in] ZCL_Notify_t *ntfy
******************************************************************************/
static void ManageConfigureReportResp(ZCL_Notify_t *ntfy)
{
    uint8_t cluster;
    uint8_t configurationCompleted = 1;

    //Update the number of attribute to configure
    for (cluster = 0; cluster < SetupTable.MatchNumber; cluster++)
    {
        if (SetupTable.ClusterList[cluster].BindingStatus == 1)
        {
            if (SetupTable.ClusterList[cluster].NbAttributeToConfigure != 0)
            {
                SetupTable.ClusterList[cluster].NbAttributeToConfigure--;
                break;
            }
        }
    }

    for (cluster = 0; cluster < SetupTable.MatchNumber; cluster++)
    {
        if (SetupTable.ClusterList[cluster].BindingStatus == 1)
        {
            if (SetupTable.ClusterList[cluster].NbAttributeToConfigure != 0)
            {
                configurationCompleted = 0;
            }
        }
    }

    if (configurationCompleted == 0)
    {
        //Send next configure reporting request
        SendConfigureReportingRequests();
    }
    else
    {
        SetupTable.AddrMode = ntfy->addressing->addrMode;
        SetupTable.Address = ntfy->addressing->addr;
        SendReadAttributeRequests();
    }
}

/**************************************************************************//**
\brief Sends all Read Attribute requests, but one at a time

\param[in] None
******************************************************************************/
static void SendReadAttributeRequests(void)
{
    ZCL_Request_t *req;
    ZCL_NextElement_t element;
    ZCL_ReadAttributeReq_t readAttrReqElement;
    uint8_t cluster;
    uint8_t attributeFound;
    uint16_t manufSpecCode = 0;

    attributeFound = 0;
    for (cluster = 0; cluster < SetupTable.MatchNumber; cluster++)
    {
        if (SetupTable.ClusterList[cluster].BindingStatus == 1)
        {
            if (SetupTable.ClusterList[cluster].NbAttributeToRead != 0)
            {
                switch (SetupTable.ClusterList[cluster].ClusterId)
                {
                    case THERMOSTAT_CLUSTER_ID:
                        switch (SetupTable.ClusterList[cluster].AttributeToRead[SetupTable.ClusterList[cluster].NbAttributeToRead - 1])
                        {
                            case ZCL_THERMOSTAT_CLUSTER_LOCAL_TEMPERATURE_SERVER_ATTRIBUTE_ID:
                                attributeFound = 1;
                                readAttrReqElement.id = ZCL_THERMOSTAT_CLUSTER_LOCAL_TEMPERATURE_SERVER_ATTRIBUTE_ID;
                                break;

                            case ZCL_THERMOSTAT_CLUSTER_OCCUPIED_HEATING_SETPOINT_SERVER_ATTRIBUTE_ID:
                                attributeFound = 1;
                                readAttrReqElement.id = ZCL_THERMOSTAT_CLUSTER_OCCUPIED_HEATING_SETPOINT_SERVER_ATTRIBUTE_ID;
                                break;

                            case ZCL_THERMOSTAT_CLUSTER_PI_HEATING_DEMAND_SERVER_ATTRIBUTE_ID:
                                attributeFound = 1;
                                readAttrReqElement.id = ZCL_THERMOSTAT_CLUSTER_PI_HEATING_DEMAND_SERVER_ATTRIBUTE_ID;
                                break;

                            case ZCL_THERMOSTAT_CLUSTER_ALERT_MANUF_SPECIFIC_ATTRIBUTE_ID:
                                attributeFound = 1;
                                manufSpecCode = CS_MANUFACTURER_CODE;
                                readAttrReqElement.id = ZCL_THERMOSTAT_CLUSTER_ALERT_MANUF_SPECIFIC_ATTRIBUTE_ID;
                                break;

                            default:
                                break;
                        }
                        break;

                    case BASIC_CLUSTER_ID:
                        switch (SetupTable.ClusterList[cluster].AttributeToRead[SetupTable.ClusterList[cluster].NbAttributeToRead - 1])
                        {
                            case ZCL_BASIC_CLUSTER_SERVER_MODEL_IDENTIFIER_ATTRIBUTE_ID:
                                attributeFound = 1;
                                readAttrReqElement.id = ZCL_BASIC_CLUSTER_SERVER_MODEL_IDENTIFIER_ATTRIBUTE_ID;
                                break;

                            case ZCL_BASIC_CLUSTER_SERVER_LOCATION_DESCRIPTION_ATTRIBUTE_ID:
                                attributeFound = 1;
                                readAttrReqElement.id = ZCL_BASIC_CLUSTER_SERVER_LOCATION_DESCRIPTION_ATTRIBUTE_ID;
                                break;

                            case ZCL_BASIC_CLUSTER_SERVER_APPLICATION_VERSION_ATTRIBUTE_ID:
                                attributeFound = 1;
                                readAttrReqElement.id = ZCL_BASIC_CLUSTER_SERVER_APPLICATION_VERSION_ATTRIBUTE_ID;
                                break;

                            case ZCL_BASIC_CLUSTER_SERVER_STACK_VERSION_ATTRIBUTE_ID:
                                attributeFound = 1;
                                readAttrReqElement.id = ZCL_BASIC_CLUSTER_SERVER_STACK_VERSION_ATTRIBUTE_ID;
                                break;

                            default:
                                break;
                        }
                        break;

                    case THERMOSTAT_UI_CONF_CLUSTER_ID:
                        switch (SetupTable.ClusterList[cluster].AttributeToRead[SetupTable.ClusterList[cluster].NbAttributeToRead - 1])
                        {
                            case ZCL_THERMOSTAT_UI_CONF_CLUSTER_KEYPAD_LOCKOUT_SERVER_ATTRIBUTE_ID:
                                attributeFound = 1;
                                readAttrReqElement.id = ZCL_THERMOSTAT_UI_CONF_CLUSTER_KEYPAD_LOCKOUT_SERVER_ATTRIBUTE_ID;
                                break;

                            default:
                                break;
                        }
                        break;

                    case HUMIDITY_MEASUREMENT_CLUSTER_ID:
                        switch (SetupTable.ClusterList[cluster].AttributeToRead[SetupTable.ClusterList[cluster].NbAttributeToRead - 1])
                        {
                            case ZCL_HUMIDITY_MEASUREMENT_CLUSTER_SERVER_MEASURED_VALUE_ATTRIBUTE_ID:
                                attributeFound = 1;
                                readAttrReqElement.id = ZCL_HUMIDITY_MEASUREMENT_CLUSTER_SERVER_MEASURED_VALUE_ATTRIBUTE_ID;
                                break;

                            default:
                                break;
                        }
                        break;

                    default:
                        break;
                }
                if (attributeFound != 0)
                {
                    break;
                }
            }
        }
    }

    //Check if there another attribute needs to be read
    if (attributeFound != 0)
    {
        if (!(req = getFreeCommand()))
        return;

        element.payloadLength = 0;
        element.payload = req->requestPayload;
        element.id = ZCL_READ_ATTRIBUTES_COMMAND_ID;
        element.content = &readAttrReqElement;

        ZCL_PutNextElement(&element);

        fillCommandRequest(req, ZCL_READ_ATTRIBUTES_COMMAND_ID, element.payloadLength);

        req->dstAddressing.addrMode             = SetupTable.AddrMode;
        req->dstAddressing.addr                 = SetupTable.Address;
        req->dstAddressing.profileId            = PROFILE_ID_HOME_AUTOMATION;
        req->dstAddressing.endpointId           = SetupTable.Endpoint;
        req->dstAddressing.clusterId            = SetupTable.ClusterList[cluster].ClusterId;
        req->dstAddressing.clusterSide          = ZCL_CLUSTER_SIDE_SERVER;
        req->dstAddressing.manufacturerSpecCode = manufSpecCode;
        req->dstAddressing.sequenceNumber       = ZCL_GetNextSeqNumber();

        req->ZCL_Notify = ZCL_ReadAttributeResp;

        commandManagerSendAttribute(req);
    }
}

/**************************************************************************//**
\brief Manages all Read Attribute responses

\param[in] None
******************************************************************************/
void ManageReadAttributeResp(ZCL_Notify_t *ntfy)
{
    uint8_t cluster;
    uint8_t readCompleted = 1;

    //Update the number of attribute to read
    for (cluster = 0; cluster < SetupTable.MatchNumber; cluster++)
    {
        if (SetupTable.ClusterList[cluster].BindingStatus == 1)
        {
            if (SetupTable.ClusterList[cluster].NbAttributeToRead != 0)
            {
                SetupTable.ClusterList[cluster].NbAttributeToRead--;
                break;
            }
        }
    }

    for (cluster = 0; cluster < SetupTable.MatchNumber; cluster++)
    {
        if (SetupTable.ClusterList[cluster].BindingStatus == 1)
        {
            if (SetupTable.ClusterList[cluster].NbAttributeToRead != 0)
            {
                readCompleted = 0;
            }
        }
    }

    if (readCompleted == 0)
    {
        //Send next read attribute request
        SendReadAttributeRequests();
    }
    else
    {
        //End of new device init
    }
}

/**************************************************************************//**
\brief Sends the Match Descriptor request

\param[in] addr - nwk Address of Interest
\param[in] ep   - Endpoint which requests Match desctiptor
******************************************************************************/
static void zdpMatchDescReq(ShortAddr_t addr,uint8_t ep)
{
    ZDO_ZdpReq_t *zdpReq = &descModeMem.zdpReq;
    ZDO_MatchDescReq_t *matchDescReq = &zdpReq->req.reqPayload.matchDescReq;
    uint8_t flag=0;
    AppBindReq_t **appBindRequest   = getDeviceBindRequest();
    zdpReq->ZDO_ZdpResp             = zdpMatchDescResponse;
    zdpReq->reqCluster              = MATCH_DESCRIPTOR_CLID;
    zdpReq->dstAddrMode             = APS_SHORT_ADDRESS;
    zdpReq->dstAddress.shortAddress = addr;
    matchDescReq->nwkAddrOfInterest = addr;
    matchDescReq->profileId         = PROFILE_ID_HOME_AUTOMATION;

    for (uint8_t epCount = 0; epCount < APP_ENDPOINTS_AMOUNT; epCount++)
    {
        if( appBindRequest[epCount]->srcEndpoint == ep)
        {
            matchDescReq->numInClusters = appBindRequest[epCount]->remoteServersCnt;
            for (uint8_t i = 0; i < appBindRequest[epCount]->remoteServersCnt; i++)
                matchDescReq->inClusterList[i] = appBindRequest[epCount]->remoteServers[i];

            matchDescReq->numOutClusters = appBindRequest[epCount]->remoteClientsCnt;
            for (uint8_t i = 0; i < appBindRequest[epCount]->remoteClientsCnt; i++)
                matchDescReq->outClusterList[i] = appBindRequest[epCount]->remoteClients[i];
            flag++;
            break;
        }
    }

    if(flag!=0)
    {
        ZDO_ZdpReq(zdpReq);
    }
}

/**************************************************************************//**
\brief Match Descriptor response callback

\param[in] resp - response payload
******************************************************************************/
static void zdpMatchDescResponse(ZDO_ZdpResp_t *resp)
{
    ZDO_MatchDescResp_t * matchDescr = (ZDO_MatchDescResp_t*)&resp->respPayload.matchDescResp;
    zdpSimpleDescReq(matchDescr->nwkAddrOfInterest,matchDescr->matchList[0]);
}


/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/

static void FlushThermostat(ZDO_ZdpResp_t *zdpResp)
{
    uint8_t statIndex;

    if (0 != zdpResp->srcAddress.extAddress)
    {
        if (DBERR_OK == DBTHRD_GetData (DBTYPE_INDEX_FROM_ZBEXTADDR, (void *)&statIndex, zdpResp->srcAddress.extAddress))
        {
            classTH_DeleteThermostat(statIndex);
        }
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void HAL_LeaveZigBeeNetwork(uint8_t statIndex)
{
    static ZDO_ZdpReq_t leaveReq;
    zigBeeDescriptor_t zbStatDescriptor;
    dbErr_t err;

    ZDO_MgmtLeaveReq_t *zdpLeaveReq = &leaveReq.req.reqPayload.mgmtLeaveReq;

    err = DBTHRD_GetData (DBTYPE_ZIGBEE_DESCRIPTOR, (void *)&zbStatDescriptor, statIndex);
    if (err == DBERR_OK)
    {
        //Fill ZDO_MgmtLeaveReq_t struct elements
        zdpLeaveReq->deviceAddr = zbStatDescriptor.extAddress; //should be 0 for self leave. For other nodes to leave the network corresponding address should be given
        zdpLeaveReq->rejoin = 0; //Rejoin will happen after leave it the field is set to 1 and will not if it is set to 0
        zdpLeaveReq->removeChildren = 0; //If set, children of requested node also will leave the network.

        //Fill ZDO_ZdpReq_t struct elements
        leaveReq.ZDO_ZdpResp = FlushThermostat; //Callback function for the request
        leaveReq.reqCluster = MGMT_LEAVE_CLID;
        leaveReq.dstAddrMode = APS_SHORT_ADDRESS;
        leaveReq.dstAddress.shortAddress = zbStatDescriptor.shortAddress;
        leaveReq.dstAddress.extAddress = zbStatDescriptor.extAddress;

        //Send ZDP request over air.
        ZDO_ZdpReq(&leaveReq);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void HAL_ConfirmLeaveNetwork(ZDO_ZdpResp_t *zdpResp)
{
    dbType_ZigBeeNetworkInfo_t nwInfo;

    nwInfo.ActiveChannel = 0;
    nwInfo.InANetwork = 0;
    nwInfo.PanID = 0xFFFF;
    nwInfo.ShortAdd = 0;
    DBTHRD_SetData (DBTYPE_ZIGBEE_NETWORK_INFO, &nwInfo, INDEX_DONT_CARE, DBCHANGE_ZIGBEE);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/12/14
*******************************************************************************/
void ZB_ManageNewThermostat (DeviceAnnce_t * device)
{
    uint8_t cluster;
    uint8_t attribute;

    for (cluster = 0; cluster < CTL_ENDPOINT_CLIENT_CLUSTERS_COUNT; cluster++)
    {
        SetupTable.ClusterList[cluster].ClusterId = 0xFFFF;
        SetupTable.ClusterList[cluster].BindingStatus = 0;
        SetupTable.ClusterList[cluster].NbAttributeToConfigure = 0;
        SetupTable.ClusterList[cluster].NbAttributeToRead = 0;
        for (attribute = 0; attribute < MAX_ATTRIBUTE_SETUP; attribute++)
        {
            SetupTable.ClusterList[cluster].AttributeToConfigureForReporting[attribute] = 0xFFFF;
            SetupTable.ClusterList[cluster].AttributeToRead[attribute] = 0xFFFF;
        }
    }
    zdpMatchDescReq(device->shortAddr, APP_SRC_ENDPOINT_ID);
}

/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
