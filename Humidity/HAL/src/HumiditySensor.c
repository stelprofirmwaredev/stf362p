/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/******************************************************************************
* @file    HAL_HumiditySensor.c
* @date    2016/09/08
* @authors Jean-Fran�ois Many
* @brief   Humidity Sensor module
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include "typedef.h"
#include "HAL_Interrupt.h"
#include "stm32f4xx_hal.h"
#include ".\HAL\inc\HumiditySensor.h"
#include ".\DB\inc\THREAD_DB.h"
#include "cmsis_os.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define I2C_CLOCK_SPEED         100000
#define OWN_ADDRESS             0x01
#define SENSOR_ADDRESS          0x5F<<1 //Left shift 1 bit to make room for R/W bit (7-bit addressing)
#define SENSOR_WRITE_ADDRESS    0xBE<<1 //Left shift 1 bit to make room for R/W bit (7-bit addressing)
#define SENSOR_READ_ADDRESS     0xBF<<1 //Left shift 1 bit to make room for R/W bit (7-bit addressing)
#define HUMIDITY_READ_TIMEOUT   25     //25ms        //(HAL_MAX_DELAY-1)
#define MAX_RETRY               20

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
static I2C_HandleTypeDef Humidity_I2C;
static RelativeHumidity_t Humidity;


/*******************************************************************************
*    Public variables definitions
*******************************************************************************/


/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static void I2C_Init(void);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/09
*******************************************************************************/
static void I2C_Init(void)
{
  Humidity_I2C.Instance = I2C3;
  Humidity_I2C.Init.ClockSpeed = I2C_CLOCK_SPEED;
  Humidity_I2C.Init.DutyCycle = I2C_DUTYCYCLE_2;
  Humidity_I2C.Init.OwnAddress1 = OWN_ADDRESS;
  Humidity_I2C.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  Humidity_I2C.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  Humidity_I2C.Init.OwnAddress2 = 0;
  Humidity_I2C.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  Humidity_I2C.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&Humidity_I2C) != HAL_OK)
  {
//    Error_Handler();
  }
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  Initalize the humidity sensor
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/09/08
*******************************************************************************/
void HumiditySensor_Init(void)
{
#ifdef DEVKIT_PLATFORM
    //On Devkit platform, Humidity sensor is always powered
#else
    GPIO_InitTypeDef GPIO_InitStruct;

    InitHumidityEnableGPIO(GPIO_InitStruct);
#endif /* DEVKIT_PLATFORM */
    TurnOnHumiditySensor();
    I2C_Init();
}

/*******************************************************************************
* @brief  Humidity Sensor monitoring
* @inputs None
* @retval status: 0 = success, 1 = failure
* @author Jean-Fran�ois Many
* @date   2016/09/08
*******************************************************************************/
uint8_t HAL_HumiditySensorMonitoring(void)
{
    S16BIT humidity_sense;
    static U8BIT firstTime = TRUE;
    U8BIT buffer[2];
    static U8BIT calibrateRH[2];
    static U8BIT calibrateADC[4];
    static S16BIT calibrateADC16[2];
    static HAL_StatusTypeDef statusHum = HAL_OK;
    U8BIT retry = 0;
    float m;
    float b;

    if (firstTime == TRUE)
    {
        buffer[0] = 0x3F;
        buffer[1] = 0x06;
        statusHum |= HAL_I2C_Mem_Read(&Humidity_I2C, SENSOR_ADDRESS, H0_RH_X2, 1, &calibrateRH[0], 1, HUMIDITY_READ_TIMEOUT);
        statusHum |= HAL_I2C_Mem_Read(&Humidity_I2C, SENSOR_ADDRESS, H1_RH_X2, 1, &calibrateRH[1], 1, HUMIDITY_READ_TIMEOUT);
        statusHum |= HAL_I2C_Mem_Read(&Humidity_I2C, SENSOR_ADDRESS, H0_T0_OUT_H, 1, &calibrateADC[0], 1, HUMIDITY_READ_TIMEOUT);
        statusHum |= HAL_I2C_Mem_Read(&Humidity_I2C, SENSOR_ADDRESS, H0_T0_OUT_L, 1, &calibrateADC[1], 1, HUMIDITY_READ_TIMEOUT);
        statusHum |= HAL_I2C_Mem_Read(&Humidity_I2C, SENSOR_ADDRESS, H1_T0_OUT_H, 1, &calibrateADC[2], 1, HUMIDITY_READ_TIMEOUT);
        statusHum |= HAL_I2C_Mem_Read(&Humidity_I2C, SENSOR_ADDRESS, H1_T0_OUT_L, 1, &calibrateADC[3], 1, HUMIDITY_READ_TIMEOUT);
        if (statusHum == HAL_OK)
        {
            humidity_sense = (calibrateADC[0] << 8) | calibrateADC[1];
            if (humidity_sense < 0)
            {
                calibrateADC16[0] = -(~humidity_sense + 1);
            }
            else
            {
                calibrateADC16[0] = humidity_sense;
            }
            humidity_sense = (calibrateADC[2] << 8) | calibrateADC[3];
            if (humidity_sense < 0)
            {
                calibrateADC16[1] = -(~humidity_sense + 1);
            }
            else
            {
                calibrateADC16[1] = humidity_sense;
            }
            calibrateRH[0] = calibrateRH[0] / 2;
            calibrateRH[1] = calibrateRH[1] / 2;
            firstTime = FALSE;
        }
    }

    buffer[0] = CTRL_REG1;
    buffer[1] = 0x84;
    statusHum |= HAL_I2C_Mem_Write(&Humidity_I2C, SENSOR_ADDRESS, buffer[0], 1, &buffer[1], 1, HUMIDITY_READ_TIMEOUT);

    buffer[0] = CTRL_REG2;
    buffer[1] = 0x01;
    statusHum |= HAL_I2C_Mem_Write(&Humidity_I2C, SENSOR_ADDRESS, buffer[0], 1, &buffer[1], 1, HUMIDITY_READ_TIMEOUT);

    buffer[0] = 0;
    while ((buffer[0] & 0x02) == 0)
    {
        statusHum |= HAL_I2C_Mem_Read(&Humidity_I2C, SENSOR_ADDRESS, STATUS_REG, 1, &buffer[0], 1, HUMIDITY_READ_TIMEOUT);
        if (statusHum != HAL_OK)
        {
            break;
        }
        else
        {
            retry++;
            if (retry > MAX_RETRY)
            {
                break;
            }
        }
    }
    statusHum |= HAL_I2C_Mem_Read(&Humidity_I2C, SENSOR_ADDRESS, HUMIDITY_OUT_H, 1, &buffer[0], 1, HUMIDITY_READ_TIMEOUT);
    statusHum |= HAL_I2C_Mem_Read(&Humidity_I2C, SENSOR_ADDRESS, HUMIDITY_OUT_L, 1, &buffer[1], 1, HUMIDITY_READ_TIMEOUT);
    if (statusHum == HAL_OK)
    {
        humidity_sense = (buffer[0] << 8) | buffer[1];
        if (humidity_sense < 0)
        {
            humidity_sense = -(~humidity_sense + 1);
        }
        //Humidity = mx+b
        //Find m
        m = (float)((float)(calibrateRH[1] - calibrateRH[0]) / (float)(calibrateADC16[1] - calibrateADC16[0]));
        //Find b
        b = calibrateRH[0] - (float)(m * (float)calibrateADC16[0]);
        Humidity = (RelativeHumidity_t)(m * (float)humidity_sense + b);
        if (Humidity <= 100)
        {
            DBTHRD_SetData(DBTYPE_AMBIENT_HUMIDITY,(void*)&Humidity, 0, DBCHANGE_LOCAL);
            return 0;
        }
        else
        {
            return 1;
        }
    }
    else
    {
        TurnOffHumiditySensor();
        HumiditySensor_Init();
        return 1;
    }
}

/** Copyright(C) 2014 Stelpro Design, All Rights Reserved**/
