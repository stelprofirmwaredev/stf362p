/*******************************************************************************
* @file    HAL_HumiditySensor.h
* @date    2016/09/08
* @authors Jean-Fran�ois Many
* @brief
*******************************************************************************/

#ifndef HAL_HumiditySensor_H
#define HAL_HumiditySensor_H


/*******************************************************************************
* Includes
*******************************************************************************/


/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define WHO_AM_I        0x0F
#define AV_CONF         0x10
#define CTRL_REG1       0x20
#define CTRL_REG2       0x21
#define CTRL_REG3       0x22
#define STATUS_REG      0x27
#define HUMIDITY_OUT_L  0x28
#define HUMIDITY_OUT_H  0x29
#define TEMP_OUT_L      0x2A
#define TEMP_OUT_H      0x2B
#define H0_RH_X2        0x30
#define H1_RH_X2        0x31
#define H0_T0_OUT_L     0x36
#define H0_T0_OUT_H     0x37
#define H1_T0_OUT_L     0x3A
#define H1_T0_OUT_H     0x3B

#ifdef DEVKIT_PLATFORM
    #define TurnOnHumiditySensor()
    #define TurnOffHumiditySensor()
#else
    #define TurnOnHumiditySensor()                  (HAL_GPIO_WritePin(HUM_ENABLE_GPIO_Port, HUM_ENABLE_Pin, GPIO_PIN_RESET))
    #define TurnOffHumiditySensor()                 (HAL_GPIO_WritePin(HUM_ENABLE_GPIO_Port, HUM_ENABLE_Pin, GPIO_PIN_SET));osDelay(10);
    #define InitHumidityEnableGPIO(GPIO_InitStruct) GPIO_InitStruct.Pin = HUM_ENABLE_Pin; \
                                                    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP; \
                                                    GPIO_InitStruct.Pull = GPIO_NOPULL; \
                                                    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW; \
                                                    HAL_GPIO_Init(HUM_ENABLE_GPIO_Port, &GPIO_InitStruct);
#endif /* DEVKIT_PLATFORM */

/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void HumiditySensor_Init(void);
uint8_t HAL_HumiditySensorMonitoring(void);


#endif /* HAL_HumiditySensor_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
