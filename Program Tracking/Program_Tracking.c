/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2018, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/
/*******************************************************************************
* @file    Program_Tracking.c
* @date    2018/12/20
* @authors Jean-Fran�ois Many
* @brief   Program Tracking Module
*******************************************************************************/
/*******************************************************************************
*    Includes
*******************************************************************************/
#include "typedef.h"
#include "convert.h"
#include "common_types.h"
#include "string.h"
#include <stdlib.h>
#include ".\DB\inc\THREAD_DBDataTypes.h"
#include ".\DB\inc\THREAD_DB.h"
#include "APP_SetpointManager.h"
#include "HomeMenu.h"
#include "Program_Tracking.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define PROGRAM_NOT_FOUND   20000

#define PGM_WAKE                1
#define PGM_LEAVE               2
#define PGM_RETURN              3
#define PGM_SLEEP               4

#define FULL_SCHEDULE_SIZE      28
#define SUNDAY                  7

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
static Program_t ActivePgm;
static uint8_t LastProgramInSchedule;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static uint8_t ConvertPgm28ToPgm4(uint8_t pgm28);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static uint8_t ConvertPgm28ToPgm4(uint8_t pgm28)
{
    uint8_t pgm4;

    //Set program icon
    pgm4 = (pgm28 % 4) + 1;

    return pgm4;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
uint16_t GetMinutesOfWeek(void)
{
    uint16_t mow;
    dbType_Time_t localTime;
    dbType_Date_t localDate;

    //Get Date
    DBTHRD_GetData(DBTYPE_DATE, (void*)&localDate, THIS_THERMOSTAT);
    //Get Time
    DBTHRD_GetData(DBTYPE_LOCALTIME, (void*)&localTime, INDEX_DONT_CARE);
    //Calculate minute offset based on week day
    mow = 0;
    if (localTime.IsTimeSet)
    {
        mow = (localDate.DayOfWeek - 1) * 1440;

        //Calculate actual minutes of the week
        mow += localTime.Hours * 60 + localTime.Minutes;
    }

    return mow;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void FetchFullProgramSchedule(FullSchedule_t* schedule)
{
    uint8_t i;
    uint16_t day_offset;
    PgmSchedule_t wakeSchedule;
    PgmSchedule_t leaveSchedule;
    PgmSchedule_t returnSchedule;
    PgmSchedule_t sleepSchedule;
    uint16_t lastActiveProgramTime;

    //Build the week program schedule
    for (i = 0; i < 7; i ++)
    {
        day_offset = 1440 * i;
        DBTHRD_GetData(DBTYPE_WAKE_SCHEDULE,(void*)&wakeSchedule, THIS_THERMOSTAT);
        DBTHRD_GetData(DBTYPE_LEAVE_SCHEDULE,(void*)&leaveSchedule, THIS_THERMOSTAT);
        DBTHRD_GetData(DBTYPE_RETURN_SCHEDULE,(void*)&returnSchedule, THIS_THERMOSTAT);
        DBTHRD_GetData(DBTYPE_SLEEP_SCHEDULE,(void*)&sleepSchedule, THIS_THERMOSTAT);
        schedule->PgmStartTime[i*4] = wakeSchedule.Day[i].ScheduleTime + day_offset;
        schedule->PgmEnabled[i*4] = wakeSchedule.Day[i].ScheduleEnabled;
        schedule->PgmStartTime[i*4+1] = leaveSchedule.Day[i].ScheduleTime + day_offset;
        schedule->PgmEnabled[i*4+1] = leaveSchedule.Day[i].ScheduleEnabled;
        schedule->PgmStartTime[i*4+2] = returnSchedule.Day[i].ScheduleTime + day_offset;
        schedule->PgmEnabled[i*4+2] = returnSchedule.Day[i].ScheduleEnabled;
        schedule->PgmStartTime[i*4+3] = sleepSchedule.Day[i].ScheduleTime + day_offset;
        schedule->PgmEnabled[i*4+3] = sleepSchedule.Day[i].ScheduleEnabled;
    }

    //Determine the last active program of the schedule in case the actual time is before the first week program time
    lastActiveProgramTime = 0;
    for (i = 0; i < FULL_SCHEDULE_SIZE; i++)
    {
        if (schedule->PgmEnabled[i])
        {
            if (schedule->PgmStartTime[i] > lastActiveProgramTime)
            {
                lastActiveProgramTime = schedule->PgmStartTime[i];
                LastProgramInSchedule = i;
            }
        }
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
uint8_t GetLastProgramInSchedule(void)
{
    return LastProgramInSchedule;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void CheckProgramTracking(void)
{
    uint16_t minutes_of_week;
    FullSchedule_t full_program_schedule;
    ScheduleModeEnum_t scheduleMode;
    Setpoint100_t pgmSetpoint;
    uint8_t floorMode;

    FetchFullProgramSchedule(&full_program_schedule);

    minutes_of_week = GetMinutesOfWeek();

    for (uint8_t pgm = 0; pgm < FULL_SCHEDULE_SIZE; pgm++)
    {
        if ((minutes_of_week == full_program_schedule.PgmStartTime[pgm]) && (full_program_schedule.PgmEnabled[pgm]))
        {
            //Activation time is reached
            //Cancel derogation
            scheduleMode = MODE_PROG;
            DBTHRD_SetData(DBTYPE_SCHEDULE_MODE, (void*)&scheduleMode, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            //Set active program
            ActivePgm = (Program_t)ConvertPgm28ToPgm4(pgm);
            DBTHRD_SetData(DBTYPE_ACTIVE_PGM, (void*)&ActivePgm, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            //Set the program setpoint to either the Floor or Ambient setpoint
            pgmSetpoint = DeterminePgmSetpoint(ActivePgm);
            DBTHRD_GetData(DBTYPE_FLOOR_MODE,(void*)&floorMode, THIS_THERMOSTAT);
            if (floorMode == CONTROLMODE_FLOOR)
            {
                DBTHRD_SetData(DBTYPE_FLOOR_SETPOINT,(void*)&pgmSetpoint, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            }
            else
            {
                DBTHRD_SetData(DBTYPE_AMBIENT_SETPOINT,(void*)&pgmSetpoint, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            }
        }
    }
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void FindActiveProgram(void)
{
    uint16_t minutes_of_week;
    uint16_t pgm_found_time = PROGRAM_NOT_FOUND;
    uint8_t found_pgm = 0;
    uint8_t pgm;
    FullSchedule_t full_program_schedule;
    Setpoint100_t pgmSetpoint;
    uint8_t floorMode;
    ScheduleModeEnum_t scheduleMode;

    FetchFullProgramSchedule(&full_program_schedule);

    minutes_of_week = GetMinutesOfWeek();

    //Loop on the full schedule
    for (pgm = 0; pgm < FULL_SCHEDULE_SIZE; pgm++)
    {
        if ((minutes_of_week >= full_program_schedule.PgmStartTime[pgm]) && (full_program_schedule.PgmEnabled[pgm]))
        {
            if ((full_program_schedule.PgmStartTime[pgm] >= pgm_found_time) || (pgm_found_time == PROGRAM_NOT_FOUND))
            {
                pgm_found_time = full_program_schedule.PgmStartTime[pgm];
                found_pgm = pgm;
            }
        }
    }

    //If no program was found (because we are before the first week program), use the last program of the week
    if (pgm_found_time == PROGRAM_NOT_FOUND)
    {
        found_pgm = GetLastProgramInSchedule();
    }

    ActivePgm = (Program_t)ConvertPgm28ToPgm4(found_pgm);
    DBTHRD_SetData(DBTYPE_ACTIVE_PGM, (void*)&ActivePgm, INDEX_DONT_CARE, DBCHANGE_LOCAL);
    DBTHRD_GetData(DBTYPE_SCHEDULE_MODE,(void*)&scheduleMode, THIS_THERMOSTAT);
    if (scheduleMode == MODE_PROG)
    {
        //Set the program setpoint to either the Floor or Ambient setpoint
        pgmSetpoint = DeterminePgmSetpoint(ActivePgm);
        DBTHRD_GetData(DBTYPE_FLOOR_MODE,(void*)&floorMode, THIS_THERMOSTAT);
        if (floorMode == CONTROLMODE_FLOOR)
        {
            DBTHRD_SetData(DBTYPE_FLOOR_SETPOINT,(void*)&pgmSetpoint, THIS_THERMOSTAT, DBCHANGE_LOCAL);
        }
        else
        {
            DBTHRD_SetData(DBTYPE_AMBIENT_SETPOINT,(void*)&pgmSetpoint, THIS_THERMOSTAT, DBCHANGE_LOCAL);
        }
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
uint32_t DeterminePgmSetpoint(uint8_t pgm)
{
    uint32_t setpoint;

    switch (pgm)
    {
        case PGM_WAKE:
            DBTHRD_GetData(DBTYPE_WAKE_SETPOINT,(void*)&setpoint, THIS_THERMOSTAT);
            break;

        case PGM_LEAVE:
            DBTHRD_GetData(DBTYPE_LEAVE_SETPOINT,(void*)&setpoint, THIS_THERMOSTAT);
            break;

        case PGM_RETURN:
            DBTHRD_GetData(DBTYPE_RETURN_SETPOINT,(void*)&setpoint, THIS_THERMOSTAT);
            break;

        case PGM_SLEEP:
            DBTHRD_GetData(DBTYPE_SLEEP_SETPOINT,(void*)&setpoint, THIS_THERMOSTAT);
            break;

        default:
            break;
    }

    return setpoint;
}

/** Copyright(C) 2018 Stelpro Design, All Rights Reserved**/
