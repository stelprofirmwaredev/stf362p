/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    BSP_Ayla_WM_N_MB_30.c
* @date    2016/08/24
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <string.h>
#include ".\BSP\Ayla WM-N-BM-30\inc\BSP_Ayla_WM_N_MB_30.h"
#include ".\HAL\inc\HAL_CloudInterface.h"

#include ".\HAL\inc\HAL_WatchDog.h"
#include ".\HAL\inc\AylaDataOps.h"

#include "stm32f4xx_hal.h"
#ifdef __ICCARM__
    #include "cmsis_os.h"
#endif

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
/*******************************************************************************
*    Private variables definitions
*******************************************************************************/

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
extern SemaphoreHandle_t xSem_DmaTx;
extern SemaphoreHandle_t xSem_DmaRx;

static DMA_HandleTypeDef hdma_uart1_rx;
static DMA_HandleTypeDef hdma_uart1_tx;
USART_HandleTypeDef hTestBenchUart;
#ifdef FREERTOS_SUPPORT
    #include "trcRecorder.h"
    extern char* wifiEventChannel;
#endif

static uint8_t test_bench_rx_complete;
static uint8_t test_bench_tx_complete;

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static uint8_t SendStartMasterOut (uint16_t size, uint8_t * padding);
static void SendLength (uint16_t size);
static void SendPayLoad(uint8_t * payload, uint16_t size);
static void SendPadding(uint8_t numberOfPads);
static uint8_t SendStartMasterIn (Ayla_Command_t command);
static uint16_t ReceivePacketLength (void);
static uint8_t ReceivePayLoad(uint8_t * receiveBuffer, uint16_t size);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/**
  * @brief  Rx Transfer completed callbacks.
  * @param  husart: pointer to a USART_HandleTypeDef structure that contains
  *                the configuration information for the specified USART module.
  * @retval None
  */
void HAL_USART_RxCpltCallback(USART_HandleTypeDef *husart)
{
    test_bench_rx_complete = 1;
}

/**
  * @brief  Tx Transfer completed callbacks.
  * @param  husart: pointer to a USART_HandleTypeDef structure that contains
  *                the configuration information for the specified USART module.
  * @retval None
  */
void HAL_USART_TxCpltCallback(USART_HandleTypeDef *husart)
{
    test_bench_tx_complete = 1;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
uint8_t send_1_byte_and_receive_1_byte(uint8_t * byte_out, uint8_t *byte_in)
{
    uint8_t status = 0;
    uint8_t timeout = 0;
    
    test_bench_rx_complete = 0;
    HAL_USART_TransmitReceive_DMA(&hTestBenchUart,byte_out,byte_in,1);
    
    while ((test_bench_rx_complete == 0) && (timeout < 100))
    {
        osDelay(1);
        timeout++;
    }
    
    if (timeout)
    {
        status = 1;
    }
    
    return status;
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/08/31
*******************************************************************************/
static uint8_t SendStartMasterOut (uint16_t size, uint8_t * padding)
{
    uint8_t lengthIn8Multiple;
    uint8_t err_status;
    uint8_t rcv_status;
    *padding = 0;

    /****************************************************************/
    /* the Master Out Command includes the length of the transfer,   */
    /*  + the length of the CRC, rounded up to the next multiple of */
    /*  8 ("+7" / "8")                                              */
    /****************************************************************/
    lengthIn8Multiple = ((size+7)/8);
    *padding = (lengthIn8Multiple * 8) - size;

    err_status = BSP_SendSingleByteCommand((Ayla_Command_t)(AYLA_COMMAND_MASTER_OUT | lengthIn8Multiple), &rcv_status);

    if (err_status != 0)
    {
        return 2;
    }
    else if (rcv_status & AYLA_STATUS_BUSY)
    {
        return 1;
    }
    else
    {
        return(0);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/02
*******************************************************************************/
static uint8_t SendStartMasterIn (Ayla_Command_t command)
{
    uint8_t rcv_status;
    uint8_t err_status;

    err_status = BSP_SendSingleByteCommand(command, &rcv_status);
    if (err_status != 0)
    {
        return 2;
    }
    else if (AYLA_COMMAND_MASTER_IN != rcv_status)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/02
*******************************************************************************/
static uint16_t ReceivePacketLength (void)
{
    uint8_t bufferLength[2];

    PULL_WIFI_CS_LINE();

    READ_FROM_WIFI ( bufferLength, 2 );

    RELEASE_WIFI_CS_LINE();

    return (((uint16_t)bufferLength[0]<<8) | bufferLength[1]);
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/02
*******************************************************************************/
static uint8_t ReceivePayLoad(uint8_t * receiveBuffer, uint16_t size)
{
    uint8_t result = 0;

    HAL_WiFi_SPI_Init(SPI_WITH_CRC, SPI_RX_MODE);             // using the dma, the CRC is automatically sent

    PULL_WIFI_CS_LINE();

    WIFI_RECEIVE_WITH_DMA ( receiveBuffer, size-1 );

#ifdef __ICCARM__
    xSemaphoreTake(xSem_DmaRx,portMAX_DELAY);               //wait until the DMA has ended the RX
#endif
    RELEASE_WIFI_CS_LINE();

    HAL_WiFi_SPI_Init(SPI_WITHOUT_CRC, SPI_TXRX_MODE);             // using the dma, the CRC is automatically sent


    /* TODO : Manage RX CRC error  */

    return (result);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/01
*******************************************************************************/
static void SendLength (uint16_t size)
{
    uint8_t lengthBuffer[2];

    lengthBuffer[0] = (size&0xff00)>>8;
    lengthBuffer[1] = (size&0x00ff);

    PULL_WIFI_CS_LINE();

    WRITE_TO_WIFI (lengthBuffer, 2);

    RELEASE_WIFI_CS_LINE();

}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/01
*******************************************************************************/
static void SendPayLoad(uint8_t * payload, uint16_t size)
{

    HAL_WiFi_SPI_Init(SPI_WITH_CRC,SPI_TXRX_MODE);             // using the dma, the CRC is automatically sent

    PULL_WIFI_CS_LINE();

    WIFI_TRANSMIT_WITH_DMA ( payload, size );

#ifdef __ICCARM__
    xSemaphoreTake(xSem_DmaTx,portMAX_DELAY);               //wait until the DMA has ended the TX
#endif

    RELEASE_WIFI_CS_LINE();

    HAL_WiFi_SPI_Init(SPI_WITHOUT_CRC, SPI_TXRX_MODE);             // using the dma, the CRC is automatically sent
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/01
*******************************************************************************/
static void SendPadding(uint8_t numberOfPads)
{
    uint8_t paddingBuffer[7];

    memset(paddingBuffer, 0, 7);

    HAL_WiFi_SPI_Init(SPI_WITHOUT_CRC, SPI_TXRX_MODE);             // using the dma, the CRC is automatically sent

    PULL_WIFI_CS_LINE();
    if (numberOfPads > 1)
    {
        WIFI_TRANSMIT_WITH_DMA ( paddingBuffer, numberOfPads );

#ifdef __ICCARM__
        xSemaphoreTake(xSem_DmaTx,portMAX_DELAY);               //wait until the DMA has ended the TX
#endif
    }
    else if (numberOfPads == 1)
    {
        WRITE_TO_WIFI (paddingBuffer, 1);
    }
    else
    {
        ;       //nada
    }

    RELEASE_WIFI_CS_LINE();
}



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/08/31
*******************************************************************************/
#pragma optimize=none
uint8_t BSP_SendPacketToWifi (uint8_t *payload, uint16_t size)
{
    uint8_t numberOfPads;
    ayla_Packet_t * dataPacket;
    uint8_t status;

    dataPacket = (ayla_Packet_t *)payload;


/****************************************************************************/
/*      Document "Ayla Module and MCU Interface Specification v8".pdf       */
/*                                                                          */
/*    Section 2.4. : Packet transfers                                       */
/*              2.4.1 : Packet from MCU to Module                           */
/****************************************************************************/


    /************************************/
    /*              Step 2-3            */
    /************************************/
    status = SendStartMasterOut(size, &numberOfPads);
    if (0 == status)
    {
#ifdef FREERTOS_SUPPORT
        vTracePrintF(wifiEventChannel, "Sending Protocol : 0x%x , Opcode : 0x%x",dataPacket->ProtocolID,dataPacket->OpCode);
#endif

        /************************************/
        /*              Step 4              */
        /************************************/
        SendLength(size);

        /************************************/
        /*              Step 5              */
        /************************************/
    #ifdef FREERTOS_SUPPORT
        vTracePrintF(wifiEventChannel, "Sending %d bytes of payload",size);
    #endif
        SendPayLoad(payload,size);

        /************************************/
        /*              Step 6              */
        /************************************/
    #ifdef FREERTOS_SUPPORT
        vTracePrintF(wifiEventChannel, "Sending %d bytes of padding",numberOfPads);
    #endif
        SendPadding(numberOfPads);


    #ifdef FREERTOS_SUPPORT
        vTracePrint(wifiEventChannel, "Transmission completed");
    #endif

        return 0;
    }
    else
    {
        return status;
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/08/31
*******************************************************************************/
#pragma optimize=none
uint8_t BSP_FetchPacketFromWifi (uint8_t *receiveBuffer, uint16_t * packetLength)
{
    static Ayla_Command_t masterInCommand = AYLA_COMMAND_MASTER_IN;
    uint8_t status;

/****************************************************************************/
/*      Document "Ayla Module and MCU Interface Specification v8".pdf       */
/*                                                                          */
/*    Section 2.4. : Packet transfers                                       */
/*              2.4.2 : Packet from Module to MCU                           */
/****************************************************************************/
    /************************************/
    /*              Step 2-3            */
    /************************************/
    //Proceed only if Master In was received, otherwise cancel the packet reception
    status = SendStartMasterIn(masterInCommand);
    if (0 == status)
    {
#ifdef FREERTOS_SUPPORT
        vTracePrint(wifiEventChannel, "Receiving a packet");
#endif
        masterInCommand = AYLA_COMMAND_MASTER_IN;
        *packetLength = ReceivePacketLength();

        if (0 != ReceivePayLoad(receiveBuffer,*packetLength))
        {
            masterInCommand = AYLA_COMMAND_START_RETRY_MASTER_IN;
        }
    }
    return status;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/08/31
*******************************************************************************/
uint8_t BSP_SendSingleByteCommand (Ayla_Command_t commandBuffer, uint8_t* pReceiveBuffer)
{
    uint8_t errorCode = 0;
    HAL_StatusTypeDef status;
    *pReceiveBuffer = RADIO_TIMEOUT;


    status = (HAL_StatusTypeDef)send_1_byte_and_receive_1_byte((uint8_t *)&commandBuffer, pReceiveBuffer);

    if (status != HAL_OK)
    {
        errorCode = 1;
    }

    return errorCode;
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/06
*******************************************************************************/
int8_t BSP_PollIncomingMessageIntr (void)
{
    return (0);
}


/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void Init_TestBench_Communication_Peripheral(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    __HAL_RCC_USART1_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_DMA2_CLK_ENABLE();

    hTestBenchUart.Instance = USART1;
    hTestBenchUart.Init.BaudRate = 115200;
    hTestBenchUart.Init.WordLength = UART_WORDLENGTH_8B;
    hTestBenchUart.Init.StopBits = UART_STOPBITS_1;
    hTestBenchUart.Init.Parity = UART_PARITY_NONE;
    hTestBenchUart.Init.Mode = UART_MODE_TX_RX;

    hdma_uart1_rx.Instance = DMA2_Stream5;
    hdma_uart1_rx.Init.Channel = DMA_CHANNEL_4;
    hdma_uart1_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
    hdma_uart1_rx.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_uart1_rx.Init.MemInc = DMA_MINC_ENABLE;
    hdma_uart1_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma_uart1_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    hdma_uart1_rx.Init.Mode = DMA_NORMAL;
    hdma_uart1_rx.Init.Priority = DMA_PRIORITY_HIGH;
    hdma_uart1_rx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;

    hdma_uart1_tx.Instance = DMA2_Stream7;
    hdma_uart1_tx.Init.Channel = DMA_CHANNEL_4;
    hdma_uart1_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
    hdma_uart1_tx.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_uart1_tx.Init.MemInc = DMA_MINC_ENABLE;
    hdma_uart1_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma_uart1_tx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    hdma_uart1_tx.Init.Mode = DMA_NORMAL;
    hdma_uart1_tx.Init.Priority = DMA_PRIORITY_HIGH;
    hdma_uart1_tx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;


    HAL_DMA_Init(&hdma_uart1_rx);
    __HAL_LINKDMA(&hTestBenchUart,hdmarx,hdma_uart1_rx);

    HAL_DMA_Init(&hdma_uart1_tx);
    __HAL_LINKDMA(&hTestBenchUart,hdmatx,hdma_uart1_tx);

    HAL_USART_Init(&hTestBenchUart);
//
//    /* UART1_IRQn interrupt configuration */
//    HAL_NVIC_SetPriority(USART1_IRQn, 6, 0);
//    HAL_NVIC_EnableIRQ(USART1_IRQn);
//
//    HAL_NVIC_SetPriority(DMA2_Stream7_IRQn, 6, 0);
//    HAL_NVIC_EnableIRQ(DMA2_Stream7_IRQn);


    /**USART1 GPIO Configuration
    PA9     ------> USART1_TX
    PA10     ------> USART1_RX
    */
    GPIO_InitStruct.Pin = GPIO_PIN_9|GPIO_PIN_10;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
