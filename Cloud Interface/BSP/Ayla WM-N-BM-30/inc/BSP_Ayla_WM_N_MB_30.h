/*******************************************************************************
* @file    BSP_Ayla_WM_N_MB_30.h
* @date    2016/08/24
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef BSP_Ayla_WM_N_MB_30_H
#define BSP_Ayla_WM_N_MB_30_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include ".\HAL\inc\AylaDefinitions.h"


#ifdef __ICCARM__
    #include "stm32f4xx_hal.h"
    #include "cmsis_os.h"
#endif          //#ifdef __ICCARM__


/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define WIFI_WAIT_NOTBUSY                           5
#define WIFI_WAIT_MASTEROUT                         1
#define WIFI_WAIT_MASTERIN                          1
#define WIFI_WAIT_MODULEREADY                       100



#ifdef __ICCARM__

    #define PULL_WIFI_RESET_LINE()                HAL_GPIO_WritePin(          \
                                                    WIFI_RST_GPIO_Port,     \
                                                    WIFI_RST_Pin,           \
                                                    GPIO_PIN_RESET);
    #define RELEASE_WIFI_RESET_LINE()             HAL_GPIO_WritePin(          \
                                                    WIFI_RST_GPIO_Port,     \
                                                    WIFI_RST_Pin,           \
                                                    GPIO_PIN_SET);

    #define PULL_WIFI_CS_LINE()                 HAL_GPIO_WritePin(                   \
                                                    WIFI_SLAVE_SELECT_GPIO_Port,  \
                                                    WIFI_SLAVE_SELECT_Pin,        \
                                                    GPIO_PIN_RESET);
    #define RELEASE_WIFI_CS_LINE()              HAL_GPIO_WritePin(                   \
                                                    WIFI_SLAVE_SELECT_GPIO_Port,  \
                                                    WIFI_SLAVE_SELECT_Pin,        \
                                                    GPIO_PIN_SET);

    #define IS_WIFIRADIO_READY()               (HAL_GPIO_ReadPin (          \
                                                     WIFI_READY_N_GPIO_Port,    \
                                                     WIFI_READY_N_Pin) == GPIO_PIN_RESET  \
                                                         ? 1 : 0)
    #define IS_WIFI_MESSAGE_PENDING()           (HAL_GPIO_ReadPin (          \
                                                     WIFI_WIFI_INTR_N_GPIO_Port,    \
                                                     WIFI_WIFI_INTR_N_Pin) == GPIO_PIN_RESET  \
                                                         ? 1 : 0)


extern SPI_HandleTypeDef hWiFiSpi;

    #define READ_FROM_WIFI(data, size)                  HAL_SPI_Receive (&hWiFiSpi,        \
                                                            data,               \
                                                            size,               \
                                                            HAL_MAX_DELAY-1)
    #define WRITE_TO_WIFI(data, size)                   HAL_SPI_Transmit (&hWiFiSpi,        \
                                                            data,               \
                                                            size,               \
                                                            HAL_MAX_DELAY-1)
    #define WIFI_TRANSMIT_AND_RECEIVE(txbuf,rxbuf,size) HAL_SPI_TransmitReceive(   \
                                                            &hWiFiSpi,             \
                                                           txbuf,               \
                                                           rxbuf,               \
                                                           size,                \
                                                           1000)

    #define WIFI_TRANSMIT_WITH_DMA(txbuf,size)          HAL_SPI_Transmit_DMA (     \
                                                           &hWiFiSpi,             \
                                                           txbuf,               \
                                                           size-1)

    #define WIFI_RECEIVE_WITH_DMA(rxbuf,size)          HAL_SPI_Receive_DMA (     \
                                                           &hWiFiSpi,             \
                                                           rxbuf,               \
                                                           size)

#else       //ifdef __ICCARM__
    #define PULL_WIFI_RESET_LINE()
    #define RELEASE_WIFI_RESET_LINE()
    #define PULL_WIFI_CS_LINE()
    #define RELEASE_WIFI_CS_LINE()
    #define IS_WIFI_READY()                     1
    #define IS_WIFI_MESSAGE_PENDING()           1
    #define READ_FROM_WIFI(data, size)
    #define WRITE_TO_WIFI(data, size)
    #define WIFI_TRANSMIT_AND_RECEIVE(txbuf,rxbuf,size)
    #define WIFI_TRANSMIT_WITH_DMA(txbuf,size)
    #define WIFI_RECEIVE_WITH_DMA(rxbuf,size)
#endif
/*******************************************************************************
* Public structures definitions
*******************************************************************************/
typedef enum
{
    AYLA_OK = 0,
    AYLA_PING_FAILED,
} Ayla_Status_t;

typedef enum
{
    AYLA_COMMAND_POLL                   = 0x00,
    AYLA_COMMAND_MASTER_OUT             = 0x80,
    AYLA_COMMAND_MASTER_IN              = 0xF1,
    AYLA_COMMAND_START_RETRY_MASTER_IN  = 0xF2,
} Ayla_Command_t;

#define AYLA_STATUS_INVALID         (1<<7)
#define AYLA_STATUS_ADSBUSY         (1<<3)
#define AYLA_STATUS_ERROR           (1<<2)
#define AYLA_STATUS_ATTENTION       (1<<1)
#define AYLA_STATUS_BUSY            (1<<0)

#define RADIO_TIMEOUT               (0x5A)

#define AYLA_CRC_LENGTH             1               //expressed in bytes

/*******************************************************************************
* Public variables declarations
*******************************************************************************/


/*******************************************************************************
* Public functions declarations
*******************************************************************************/
Ayla_Status_t BSP_PingWiFiModule (void);
uint8_t BSP_SendSingleByteCommand (Ayla_Command_t commandBuffer, uint8_t* receiveBuffer);
uint8_t BSP_SendPacketToWifi (uint8_t *payload, uint16_t size);
uint8_t BSP_FetchPacketFromWifi (uint8_t *receiveBuffer, uint16_t * packetLength);
void BSP_WaitWiFiReady (void);
int8_t BSP_PollIncomingMessageIntr (void);

#endif /* BSP_Ayla_WM_N_MB_30_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
