/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    WiFi_Notifications.c
* @date    2016/10/20
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>

#include ".\Application\inc\WiFi_Notifications.h"

#include ".\DB\inc\THREAD_DB.h"
#include ".\DB\inc\THREAD_DBDataTypes.h"
#include "tools.h"
#include ".\WiFi\inc\THREAD_WiFi.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
//#define AMBIENT_TEMPERATURE_REPORT_PERIOD   900000  //15 minutes expressed in ms
//#define AMBIENT_HUMIDITY_REPORT_PERIOD      3600000 //60 minutes expressed in ms
//#define HEAT_DEMAND_REPORT_PERIOD           3600000 //60 minutes expressed in ms
//#define LIVE_HEAT_DEMAND_REPORT_PERIOD      15000   //15 seconds expressed in ms

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
//TimerHandle_t AmbientTemperatureReportTimer = NULL;
//TimerHandle_t AmbientHumidityReportTimer = NULL;
//TimerHandle_t HeatDemandReportTimer = NULL;
//TimerHandle_t LiveHeatDemandReportTimer = NULL;
//static susbscriptionHandle_t hLiveHeatDemandChangeNotification = DBTHRD_NOTIFHANDLE_NOTSET;
//static TEMPERATURE_C_t LastAmbientTemperature[THERMOSTAT_INSTANCES] = {0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000};
//static RelativeHumidity_t LastAmbientHumidity[THERMOSTAT_INSTANCES] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
//static uint8_t LastHeatDemand[THERMOSTAT_INSTANCES] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
extern DB_NOTIFICATION(OnCloudConnectionUpdate);
DB_NOTIFICATION(WiFi_NotifyMode);
DB_NOTIFICATION(WiFi_NotifyAmbientTemperature);
DB_NOTIFICATION(WiFi_NotifyFloorTemperature);
DB_NOTIFICATION(WiFi_NotifyLockState);
DB_NOTIFICATION(WiFi_NotifyLockDelta);
DB_NOTIFICATION(WiFi_NotifyHeatDemand);
//DB_NOTIFICATION(WiFi_NotifyPeriodicHeatDemand);
DB_NOTIFICATION(WiFi_NotifyDefaultSetpoint);
DB_NOTIFICATION(WiFi_NotifyFloorSetpoint);
DB_NOTIFICATION(WiFi_NotifyAmbientSetpoint);
DB_NOTIFICATION(WiFi_NotifyTimeFormat);
DB_NOTIFICATION(WiFi_NotifyIndoorHumidity);
DB_NOTIFICATION(WiFi_NotifyActiveAlerts);
DB_NOTIFICATION(WiFi_NotifyTemperatureUnitFormat);
DB_NOTIFICATION(WiFi_NotifyLanguage);
DB_NOTIFICATION(WiFi_NotifyGeofencingEnable);
DB_NOTIFICATION(WiFi_NotifyZigBeeAssociationPermit);
DB_NOTIFICATION(WiFi_NotifyThermostatName);
DB_NOTIFICATION(WiFi_NotifyThermostatModel);
DB_NOTIFICATION(WiFi_NotifyGroupName);
DB_NOTIFICATION(WiFi_NotifyGroupMembers);
DB_NOTIFICATION(WiFi_NotifyGroupSetpoint);
DB_NOTIFICATION(WiFi_NotifyGroupAwaySetpoint);
DB_NOTIFICATION(WiFi_NotifyGroupVacationSetpoint);
DB_NOTIFICATION(WiFi_NotifyGroupStandbySetpoint);
DB_NOTIFICATION(WiFi_NotifyActivityStartRequested);
DB_NOTIFICATION(WiFi_NotifyThermostatAdded);
DB_NOTIFICATION(WiFi_NotifyThermostatRemoved);
DB_NOTIFICATION(WiFi_NotifyDiagnostic);
DB_NOTIFICATION(WiFi_NotifyTstatDailyConsumption);
DB_NOTIFICATION(WiFi_NotifyFloorMode);
DB_NOTIFICATION(WiFi_NotifyRelayCycleCount);
//DB_NOTIFICATION(WiFi_CancelLiveHeatDemandReport);

//static void InitializeReportTimers(void);
//static void FireAmbientTemperatureReport(TimerHandle_t xTimer);
//static void FireAmbientHumidityReport(TimerHandle_t xTimer);
//static void FireHeatDemandReport(TimerHandle_t xTimer);
//static void LiveHeatDemandReportPeriodOver(TimerHandle_t xTimer);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/

//static void InitializeReportTimers(void)
//{
//    AmbientTemperatureReportTimer = xTimerCreate ("Ambient Temperature Report Timer",
//                                           pdMS_TO_TICKS(AMBIENT_TEMPERATURE_REPORT_PERIOD),
//                                           pdTRUE,                  //auto reload
//                                           (void *) 0,
//                                           FireAmbientTemperatureReport);
//
//    AmbientHumidityReportTimer = xTimerCreate ("Ambient Humidity Report Timer",
//                                           pdMS_TO_TICKS(AMBIENT_HUMIDITY_REPORT_PERIOD),
//                                           pdTRUE,                  //auto reload
//                                           (void *) 0,
//                                           FireAmbientHumidityReport);
//
//    HeatDemandReportTimer = xTimerCreate ("Heat Demand Report Timer",
//                                           pdMS_TO_TICKS(HEAT_DEMAND_REPORT_PERIOD),
//                                           pdTRUE,                  //auto reload
//                                           (void *) 0,
//                                           FireHeatDemandReport);
//
//    LiveHeatDemandReportTimer = xTimerCreate ("Live Heat Demand Report Timer",
//                                              pdMS_TO_TICKS(LIVE_HEAT_DEMAND_REPORT_PERIOD),
//                                              pdFALSE,              //one shot
//                                              (void *) 0,
//                                              LiveHeatDemandReportPeriodOver);
//
//    if (NULL != AmbientTemperatureReportTimer)
//    {
//        xTimerStart(AmbientTemperatureReportTimer, 0);
//    }
//    if (NULL != AmbientHumidityReportTimer)
//    {
//        xTimerStart(AmbientHumidityReportTimer, 0);
//    }
//    if (NULL != HeatDemandReportTimer)
//    {
//        xTimerStart(HeatDemandReportTimer, 0);
//    }
//    if (NULL != LiveHeatDemandReportTimer)
//    {
//        xTimerStart(LiveHeatDemandReportTimer, 0);
//    }
//}

//static void FireAmbientTemperatureReport(TimerHandle_t xTimer)
//{
//    DBTHRD_SetData(DBTYPE_AMBIENT_TEMPERATURE_REPORT,(void*)0, INDEX_DONT_CARE, DBCHANGE_LOCAL);
//}
//
//static void FireAmbientHumidityReport(TimerHandle_t xTimer)
//{
//    DBTHRD_SetData(DBTYPE_AMBIENT_HUMIDITY_REPORT,(void*)0, INDEX_DONT_CARE, DBCHANGE_LOCAL);
//}
//
//static void FireHeatDemandReport(TimerHandle_t xTimer)
//{
//    DBTHRD_SetData(DBTYPE_HEAT_DEMAND_REPORT,(void*)0, INDEX_DONT_CARE, DBCHANGE_LOCAL);
//}
//
//static void LiveHeatDemandReportPeriodOver(TimerHandle_t xTimer)
//{
//    DBTHRD_SetData(DBTYPE_LIVE_HEAT_DEMAND_REPORT_OVER,(void*)0, INDEX_DONT_CARE, DBCHANGE_LOCAL);
//}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author J-F. Simard
* @date   2016/10/20
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyTemperatureUnitFormat)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_TEMPERATUREFORMAT, instance, source);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2018/09/12
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyLanguage)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_LANGUAGE, instance, source);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2018/09/13
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyDiagnostic)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_DIAGNOSTIC, instance, source);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2019/03/07
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyTstatDailyConsumption)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_TSTAT_DAILY_CONSUMPTION, instance, source);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2018/09/13
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyZigBeeChannel)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_ZIGBEECHANNEL, instance, source);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author J-F. Simard
* @date   2016/10/20
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyActiveAlerts)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_ACTIVEALERTS, instance, source);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author J-F. Simard
* @date   2016/10/20
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyIndoorHumidity)
{
//    uint8_t tstat;
//    RelativeHumidity_t humidity;
//    uint8_t hasChanged = 0;
//
//    for (tstat = 0; tstat < THERMOSTAT_INSTANCES; tstat++)
//    {
//        DBTHRD_GetData(DBTYPE_AMBIENT_HUMIDITY, &humidity, tstat);
//        humidity = TLS_Round((int16_t)humidity,5);
//        if (humidity != LastAmbientHumidity[tstat])
//        {
//            hasChanged = 1;
//            LastAmbientHumidity[tstat] = humidity;
//        }
//    }
//    if (hasChanged == 1)
//    {
//        WIFITHRD_SendPropToWiFi(WIFITYPE_AMBIENTHUMIDITY, instance, source);
//    }
    if (status == DBERR_CHANGEABOVETHRESHOLD)
    {
        RelativeHumidity_t humidity;

        DBTHRD_GetData(DBTYPE_AMBIENT_HUMIDITY, &humidity, instance);
        WIFITHRD_SendPropToWiFi(WIFITYPE_AMBIENTHUMIDITY, instance, source);
    }
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author J-F. Simard
* @date   2016/10/20
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyTimeFormat)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_TIMEFORMAT, instance, source);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author J-F. Simard
* @date   2016/10/20
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyDefaultSetpoint)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_DEFAULTSETPOINT, INDEX_DONT_CARE, source);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author J-F. Simard
* @date   2016/10/20
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyHeatDemand)
{
//    uint8_t tstat;
//    uint8_t heatDemand;
//    uint8_t hasChanged = 0;
//    uint8_t displayedHeatBars;
//    uint8_t newHeatBars;
//
//    for (tstat = 0; tstat < THERMOSTAT_INSTANCES; tstat++)
//    {
//        DBTHRD_GetData(DBTYPE_HEAT_DEMAND, &heatDemand, tstat);
//
//        //Check current heat bars display
//        displayedHeatBars = LastHeatDemand[tstat] / 20;
//        if (LastHeatDemand[tstat] % 20)
//        {
//            displayedHeatBars++;
//        }
//
//        //Check new heat bars display
//        newHeatBars = heatDemand / 20;
//        if (heatDemand % 20)
//        {
//            newHeatBars++;
//        }
//
//        if (displayedHeatBars != newHeatBars)
//        {
//            hasChanged = 1;
//            LastHeatDemand[tstat] = heatDemand;
//        }
//    }
//    if (hasChanged == 1)
//    {
//        WIFITHRD_SendPropToWiFi(WIFITYPE_HEATDEMAND, instance, source);
//    }
    if (status == DBERR_CHANGEABOVETHRESHOLD)
    {
        uint8_t heatDemand;

        DBTHRD_GetData(DBTYPE_HEAT_DEMAND, &heatDemand, instance);
        WIFITHRD_SendPropToWiFi(WIFITYPE_HEATDEMAND, instance, source);
    }
}

///*******************************************************************************
//* @brief
//* @inputs
//*       dbErr_t status;
//*       uint8_t instance
//* @retval None
//* @author J-F. Simard
//* @date   2016/10/20
//*******************************************************************************/
//DB_NOTIFICATION(WiFi_NotifyPeriodicHeatDemand)
//{
//    uint8_t tstat;
//    uint8_t heatDemand;
//    uint8_t hasChanged = 0;
//    uint8_t displayedHeatBars;
//    uint8_t newHeatBars;
//
//    for (tstat = 0; tstat < THERMOSTAT_INSTANCES; tstat++)
//    {
//        DBTHRD_GetData(DBTYPE_HEAT_DEMAND, &heatDemand, tstat);
//
//        //Check current heat bars display
//        displayedHeatBars = LastHeatDemand[tstat] / 20;
//        if (LastHeatDemand[tstat] % 20)
//        {
//            displayedHeatBars++;
//        }
//
//        //Check new heat bars display
//        newHeatBars = heatDemand / 20;
//        if (heatDemand % 20)
//        {
//            newHeatBars++;
//        }
//
//        if (displayedHeatBars != newHeatBars)
//        {
//            hasChanged = 1;
//            LastHeatDemand[tstat] = heatDemand;
//        }
//    }
//    if (hasChanged == 1)
//    {
//        WIFITHRD_SendPropToWiFi(WIFITYPE_HEATDEMAND, instance, source);
//    }
//}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author J-F. Simard
* @date   2016/10/20
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyLockDelta)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_LOCKDELTA, instance, source);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author J-F. Simard
* @date   2016/10/20
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyLockState)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_LOCKSTATE, instance, source);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author J-F. Simard
* @date   2016/10/20
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyAmbientTemperature)
{
//    uint8_t tstat;
//    TEMPERATURE_C_t temperature;
//    uint8_t hasChanged = 0;
//
//    for (tstat = 0; tstat < THERMOSTAT_INSTANCES; tstat++)
//    {
//        DBTHRD_GetData(DBTYPE_AMBIENT_TEMPERATURE, &temperature, tstat);
//        temperature = TLS_Round((int16_t)temperature,50);
//        if (temperature != LastAmbientTemperature[tstat])
//        {
//            hasChanged = 1;
//            LastAmbientTemperature[tstat] = temperature;
//        }
//    }
//    if (hasChanged == 1)
//    {
//        WIFITHRD_SendPropToWiFi(WIFITYPE_AMBIENTTEMPERATURE, instance, source);
//    }
    if (status == DBERR_CHANGEABOVETHRESHOLD)
    {
        TEMPERATURE_C_t temperature;

        DBTHRD_GetData(DBTYPE_AMBIENT_TEMPERATURE, &temperature, instance);
        WIFITHRD_SendPropToWiFi(WIFITYPE_AMBIENTTEMPERATURE, instance, source);
    }
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author J-F. Simard
* @date   2016/10/20
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyFloorTemperature)
{
    if (status == DBERR_CHANGEABOVETHRESHOLD)
    {
        TEMPERATURE_C_t temperature;

        DBTHRD_GetData(DBTYPE_FLOOR_TEMPERATURE, &temperature, instance);
        WIFITHRD_SendPropToWiFi(WIFITYPE_FLOORTEMPERATURE, instance, source);
    }
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/06/15
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyMode)
{
    if (source == DBCHANGE_MODECHANGE)
    {
        WIFITHRD_SendPropToWiFi(WIFITYPE_HOMESTATE, INDEX_DONT_CARE, source);
    }
    WIFITHRD_SendPropToWiFi(WIFITYPE_HOMESTATECHANGEDBYGEOFENCING, INDEX_DONT_CARE, source);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/06/15
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyGroupSetpoint)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_GROUPSETPOINT, instance, source);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2019/01/15
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyFloorSetpoint)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_FLOORSETPOINT, instance, source);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/06/15
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyAmbientSetpoint)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_AMBIENTSETPOINT, instance, source);

//    //Potential future change in heat demand, allow heat demand live report for 15 seconds
//    if (DBTHRD_NOTIFHANDLE_NOTSET == hLiveHeatDemandChangeNotification)
//    {
//        hLiveHeatDemandChangeNotification = DBTHRD_SubscribeToNotification( DBTYPE_HEAT_DEMAND,
//                                                       WiFi_NotifyHeatDemand,
//                                                       DBCHANGE_ALLCHANGES,
//                                                       INDEX_DONT_CARE);
//    }
//    if (NULL != LiveHeatDemandReportTimer)
//    {
//        xTimerReset(LiveHeatDemandReportTimer,0);
//    }
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/10/24
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyGeofencingEnable)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_GEOFENCINGENABLE, INDEX_DONT_CARE, source);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/10/24
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyZigBeeAssociationPermit)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_ZIGBEEASSOCIATIONPERMIT, INDEX_DONT_CARE, source);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/10/25
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyThermostatName)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_THERMOSTATNAME, instance, source);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/11/03
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyThermostatModel)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_THERMOSTATMODEL, instance, source);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/10/25
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyGroupName)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_GROUPNAME, instance, source);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/10/25
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyGroupMembers)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_GROUPMEMBERS, instance, source);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/10/25
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyGroupAwaySetpoint)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_GROUPAWAYSETPOINT, instance, source);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/10/25
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyGroupVacationSetpoint)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_GROUPVACATIONSETPOINT, instance, source);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/10/25
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyGroupStandbySetpoint)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_GROUPSTANDBYSETPOINT, instance, source);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/11/17
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyActivityStartRequested)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_ACTIVITYSTARTREQUESTED, instance, source);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/11/17
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyThermostatAdded)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_AMBIENTTEMPERATURE, instance, source);
    WIFITHRD_SendPropToWiFi(WIFITYPE_AMBIENTHUMIDITY, instance, source);
    WIFITHRD_SendPropToWiFi(WIFITYPE_FLOORTEMPERATURE, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    WIFITHRD_SendPropToWiFi(WIFITYPE_FLOORMODE, THIS_THERMOSTAT, DBCHANGE_LOCAL);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/11/17
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyThermostatRemoved)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_AMBIENTTEMPERATURE, instance, source);
    WIFITHRD_SendPropToWiFi(WIFITYPE_AMBIENTHUMIDITY, instance, source);
    WIFITHRD_SendPropToWiFi(WIFITYPE_AMBIENTSETPOINT, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    WIFITHRD_SendPropToWiFi(WIFITYPE_FLOORTEMPERATURE, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    WIFITHRD_SendPropToWiFi(WIFITYPE_FLOORSETPOINT, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    WIFITHRD_SendPropToWiFi(WIFITYPE_FLOORMODE, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    WIFITHRD_SendPropToWiFi(WIFITYPE_LOCKSTATE, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    //DBTYPE_LOCK_SETPOINT_LIMIT ????
    WIFITHRD_SendPropToWiFi(WIFITYPE_ACTIVEALERTS, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    WIFITHRD_SendPropToWiFi(WIFITYPE_GROUPMEMBERS, 0, DBCHANGE_LOCAL);
    WIFITHRD_SendPropToWiFi(WIFITYPE_THERMOSTATNAME, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    WIFITHRD_SendPropToWiFi(WIFITYPE_THERMOSTATMODEL, instance, source);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/07/17
*******************************************************************************/
//DB_NOTIFICATION(WiFi_CancelLiveHeatDemandReport)
//{
//    if (DBTHRD_NOTIFHANDLE_NOTSET != hLiveHeatDemandChangeNotification)
//    {
//        DBTHRD_UnsubscribeToNotification(hLiveHeatDemandChangeNotification);
//        hLiveHeatDemandChangeNotification = DBTHRD_NOTIFHANDLE_NOTSET;
//    }
//}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2019/03/18
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyFloorMode)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_FLOORMODE, instance, source);
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2019/03/19
*******************************************************************************/
DB_NOTIFICATION(WiFi_NotifyRelayCycleCount)
{
    WIFITHRD_SendPropToWiFi(WIFITYPE_RELAY_CYCLE_COUNT, instance, source);
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/10/20
*******************************************************************************/
void RegisterWiFiNotifications(void)
{
    DBTHRD_SubscribeToNotification(DBTYPE_CLOUDCONNECTIVITY,
                                                       OnCloudConnectionUpdate,
                                                       DBCHANGE_ALLCHANGES,
                                                       INDEX_DONT_CARE);

//    DBTHRD_SubscribeToNotification( DBTYPE_AMBIENT_TEMPERATURE_REPORT,
//                                   WiFi_NotifyAmbientTemperature,
//                                   DBCHANGE_LOCAL,
//                                   INDEX_DONT_CARE);
//
//    DBTHRD_SubscribeToNotification( DBTYPE_AMBIENT_HUMIDITY_REPORT,
//                                   WiFi_NotifyIndoorHumidity,
//                                   DBCHANGE_LOCAL,
//                                   INDEX_DONT_CARE);
//
//    DBTHRD_SubscribeToNotification( DBTYPE_HEAT_DEMAND_REPORT,
//                                   WiFi_NotifyPeriodicHeatDemand,
//                                   (dbChangeSource_t)(DBCHANGE_LOCAL|DBCHANGE_ZIGBEE),
//                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_AMBIENT_TEMPERATURE,
                                   WiFi_NotifyAmbientTemperature,
                                   (dbChangeSource_t)(DBCHANGE_LOCAL|DBCHANGE_ZIGBEE),
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_FLOOR_TEMPERATURE,
                                   WiFi_NotifyFloorTemperature,
                                   (dbChangeSource_t)(DBCHANGE_LOCAL|DBCHANGE_ZIGBEE),
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_AMBIENT_HUMIDITY,
                                   WiFi_NotifyIndoorHumidity,
                                   (dbChangeSource_t)(DBCHANGE_LOCAL|DBCHANGE_ZIGBEE),
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_HEAT_DEMAND,
                                   WiFi_NotifyHeatDemand,
                                   (dbChangeSource_t)(DBCHANGE_LOCAL|DBCHANGE_ZIGBEE),
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_LOCK_STATE,
                                   WiFi_NotifyLockState,
                                   (dbChangeSource_t)(DBCHANGE_LOCAL|DBCHANGE_ZIGBEE),
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_LOCK_SETPOINT_LIMIT,
                                   WiFi_NotifyLockDelta,
                                   DBCHANGE_LOCAL,
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_DEFAULT_SETPOINT,
                                   WiFi_NotifyDefaultSetpoint,
                                   DBCHANGE_LOCAL,
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_TIME_FORMAT,
                                   WiFi_NotifyTimeFormat,
                                   DBCHANGE_LOCAL,
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_ACTIVE_ALERTS,
                                   WiFi_NotifyActiveAlerts,
                                   (dbChangeSource_t)(DBCHANGE_LOCAL|DBCHANGE_ZIGBEE),
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_TEMPERATURE_FORMAT,
                                   WiFi_NotifyTemperatureUnitFormat,
                                   DBCHANGE_LOCAL,
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_LANGUAGE_SETTING,
                                   WiFi_NotifyLanguage,
                                   DBCHANGE_LOCAL,
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_DIAGNOSTIC,
                                   WiFi_NotifyDiagnostic,
                                   DBCHANGE_LOCAL,
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_ZIGBEE_NETWORK_INFO,
                                   WiFi_NotifyZigBeeChannel,
                                   DBCHANGE_NETWORK_IS_SET,
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_GEOFENCING_ENABLE,
                                   WiFi_NotifyGeofencingEnable,
                                   DBCHANGE_LOCAL,
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_ZIGBEE_ASSOCIATION_PERMIT,
                                   WiFi_NotifyZigBeeAssociationPermit,
                                   DBCHANGE_LOCAL,
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_TSTAT_NAME,
                                   WiFi_NotifyThermostatName,
                                   (dbChangeSource_t)(DBCHANGE_LOCAL|DBCHANGE_ZIGBEE),
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_TSTAT_MODEL,
                                   WiFi_NotifyThermostatModel,
                                   (dbChangeSource_t)(DBCHANGE_LOCAL|DBCHANGE_ZIGBEE),
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_GROUP_NAME,
                                   WiFi_NotifyGroupName,
                                   DBCHANGE_LOCAL,
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_GROUP_MEMBERS,
                                   WiFi_NotifyGroupMembers,
                                   (dbChangeSource_t)(DBCHANGE_LOCAL|DBCHANGE_NEWSTATADDED),
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_FLOOR_SETPOINT_CHANGE,
                                   WiFi_NotifyFloorSetpoint,
                                   (dbChangeSource_t)(DBCHANGE_LOCAL | DBCHANGE_ZIGBEE | DBCHANGE_OPENWINDOW),
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_FLOOR_SETPOINT,
                                   WiFi_NotifyFloorSetpoint,
                                   (dbChangeSource_t)(DBCHANGE_LOCAL | DBCHANGE_ZIGBEE | DBCHANGE_OPENWINDOW),
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_AMBIENT_SETPOINT_CHANGE,
                                   WiFi_NotifyAmbientSetpoint,
                                   (dbChangeSource_t)(DBCHANGE_LOCAL | DBCHANGE_ZIGBEE | DBCHANGE_OPENWINDOW),
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_AMBIENT_SETPOINT,
                                   WiFi_NotifyAmbientSetpoint,
                                   (dbChangeSource_t)(DBCHANGE_LOCAL | DBCHANGE_ZIGBEE | DBCHANGE_OPENWINDOW),
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_GROUP_SETPOINT_CHANGE,
                                   WiFi_NotifyGroupSetpoint,
                                   (dbChangeSource_t)(DBCHANGE_LOCAL | DBCHANGE_GROUPCHANGE),
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_RESIDENCE_STATE_CHANGE,
                                   WiFi_NotifyMode,
                                   (dbChangeSource_t)(DBCHANGE_MODECHANGE | DBCHANGE_CLOUD),
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_GROUP_AWAY_SETPOINT,
                                   WiFi_NotifyGroupAwaySetpoint,
                                   DBCHANGE_LOCAL,
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_GROUP_VACATION_SETPOINT,
                                   WiFi_NotifyGroupVacationSetpoint,
                                   DBCHANGE_LOCAL,
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_GROUP_STANDBY_SETPOINT,
                                   WiFi_NotifyGroupStandbySetpoint,
                                   DBCHANGE_LOCAL,
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification( DBTYPE_ACTIVITY_START,
                                   WiFi_NotifyActivityStartRequested,
                                   (dbChangeSource_t)(DBCHANGE_LOCAL | DBCHANGE_ACTIVITY_LOCAL | DBCHANGE_ACTIVITY_MOBILE | DBCHANGE_ACTIVITY_SCHEDULED),
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification(DBTYPE_THERMOSTAT_FULLY_CONFIGURED,
                                   WiFi_NotifyThermostatAdded,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification(DBTYPE_THERMOSTAT_REMOVED,
                                   WiFi_NotifyThermostatRemoved,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification(DBTYPE_LOCAL_DAILY_CONSUMPTION,
                                   WiFi_NotifyTstatDailyConsumption,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);

//    DBTHRD_SubscribeToNotification(DBTYPE_LIVE_HEAT_DEMAND_REPORT_OVER,
//                                   WiFi_CancelLiveHeatDemandReport,
//                                   DBCHANGE_ALLCHANGES,
//                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification(DBTYPE_FLOOR_MODE,
                                   WiFi_NotifyFloorMode,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification(DBTYPE_RELAY_CYCLE_COUNT,
                                   WiFi_NotifyRelayCycleCount,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);

//    InitializeReportTimers();
}
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
