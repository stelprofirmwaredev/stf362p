/*******************************************************************************
* @file    AylaRegistration.h
* @date    2016/10/06
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef _AylaRegistration_H
#define _AylaRegistration_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>

#include ".\HAL\inc\AylaDefinitions.h"
#include ".\HAL\inc\HAL_WiFiTypes.h"


/*******************************************************************************
* Public constants definitions
*******************************************************************************/



/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void GetRegistrationToken (void (*regTokencB)(RegisterStatus_t, uint8_t*));
void ControlOps_ParseConfToken_Client (ayla_Packet_t * receivedPacket, uint16_t payloadLength);
void GetRegistrationStatus(void (*regStatuscB)(RegisterStatus_t, uint8_t*));
void GenerateNewRegistrationToken (void);

#endif /* _AylaRegistration_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
