/*******************************************************************************
* @file    _AylaProperties.h
* @date    2016/10/06
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef _AylaProperties_H
#define _AylaProperties_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>
#include <stddef.h>
#include ".\DB\inc\THREAD_DB.h"
#include ".\HAL\inc\AylaMetaData.h"
#include ".\HAL\inc\AylaDefinitions.h"
#include ".\HAL\inc\Ayla_API.h"

/*******************************************************************************
* Public constants definitions
*******************************************************************************/
typedef uint8_t (*sendFunc_t)(void* pData, dbChangeSource_t source);
typedef void (*receiveFunc_t)(aylaTLV_t *);
typedef void (*respFunc_t)(void* pData, uint16_t requestId);


typedef struct
{
    uint8_t                 propertyName[33];
    sendFunc_t              sendFunction;
    receiveFunc_t           receiveFunction;
    respFunc_t              respFunction;
    const metaEvent_t       *metaEvent;
    const metaObject_t      *metaObject;
}prop_t;

typedef struct
{
    uint8_t                 weatherString[30];
    uint8_t                 weatherDayId;
    uint8_t                 weatherNightId;
}weather_t;

typedef enum
{
    CLEAR_ID = 0,
    CLOUDY_ID,
    DRIFTINGSNOW_ID,
    FAIRDAY_ID,
    FAIRNIGHT_ID,
    FOGGY_ID,
    FREEZINGRAIN_ID,
    HAIL_ID,
    PARTLYCLOUDYDAY_ID,
    PARTLYCLOUDYNIGHT_ID,
    RAIN_ID,
    RAINSNOWSHOWERS_ID,
    SNOW_ID,
    SUNNY_ID,
    THUNDERANDHAIL_ID,
    THUNDERSTORMSDAY_ID,
    THUNDERSTORMSNIGHT_ID,
    TROPICALSTORM_ID,
    WINDY_ID,

    MAX_WEATHER_ID
}weatherId_t;

/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/



#endif /* _AylaProperties_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
