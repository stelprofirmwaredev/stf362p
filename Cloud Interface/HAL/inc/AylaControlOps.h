/*******************************************************************************
* @file    AylaControlOps.h
* @date    2016/09/07
* @authors J-F. Simard
* @brief 
*******************************************************************************/

#ifndef _AylaControlOps_H
#define _AylaControlOps_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>
#include ".\HAL\inc\AylaDefinitions.h"

/*******************************************************************************
* Public constants definitions
*******************************************************************************/

/*******************************************************************************
* Public structures definitions
*******************************************************************************/


/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void Ayla_ControlOpsInit(void);
void Ayla_HandleControlOperation (ayla_Packet_t * receivedPacket, uint16_t packetLength);


#endif /* _AylaControlOps_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
