/*******************************************************************************
* @file    AylaDefinitions.h
* @date    2016/09/06
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef _AylaDefinitions_H
#define _AylaDefinitions_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>


/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define AYLA_HEADER_FOOTER_OFFSET                   5  //Protocol (1) + opcode (1) + reqId (2) + crc (1)
#define AYLA_TLV_BASE_LENGTH                        2  //Type (1) + Length (1)
#define AYLA_MAX_WIFI_PROFILE                       10

typedef enum
{
    AYLAPROTOCOL_CONTROLOPS = 0,
    AYLAPROTOCOL_DATAOPS,
    AYLAPROTOCOL_PINGTEST,
} ayla_ProtocolID_t;

typedef enum
{
    DATAOPCODE_SENDTLVVERSION  = 0,
    DATAOPCODE_PROPVALUEREQUEST,
    DATAOPCODE_RXTLV,
    DATAOPCODE_NAK,
    DATAOPCODE_REQUESTPROP,
    DATAOPCODE_REQUESTPROP_RESP,
    DATAOPCODE_REQUESTNEXTPROP,
    DATAOPCODE_SENDTLVS,
    DATAOPCODE_FILEDATAPOINT_REQ,
    DATAOPCODE_FILEDATAPOINT_RESP,
    DATAOPCODE_FILEDATAPOINT_CREATE,
    DATAOPCODE_FILEDATAPOINT_FETCHED,
    DATAOPCODE_FILEDATAPOINT_STOP,
    DATAOPCODE_FILEDATAPOINT_SEND,
    DATAOPCODE_CONNECTSTATUS,
    DATAOPCODE_ECHOFAIL,
    DATAOPCODE_ENABLESERVICELISTENER,
    DATAOPCODE_ERROR,
    DATAOPCODE_CONFIRM,
    DATAOPCODE_PROPNOTIFICATION,
    DATAOPCODE_EVENTNOTIFICATION,
    CONTROLOPCODE_RESPONSE,
    CONTROLOPCODE_GET_CONFIGITEMS,
    CONTROLOPCODE_SET_CONFIGITEMS,
    CONTROLOPCODE_SAVECONFIG,
    CONTROLOPCODE_NAK,
    CONTROLOPCODE_LOAD_STARTUPCONFIG,
    CONTROLOPCODE_LOAD_FACTORYCONFIG,
    CONTROLOPCODE_OTA_STATUS,
    CONTROLOPCODE_OTA_COMMAND,
    CONTROLOPCODE_LOGOPS,
    CONTROLOPCODE_MCUOTA_REPORT_START,
    CONTROLOPCODE_MCUOTA_LOAD,
    CONTROLOPCODE_MCUOTA_STATUS,
    CONTROLOPCODE_MCUOTA_BOOT,
    CONTROLOPCODE_CONFUPDATE,
    CONTROLOPCODE_WIFI_JOIN,
    CONTROLOPCODE_WIFI_DELETE,

    MAX_OPCODEDEFINITION,
} ayla_OpCode_t;

typedef struct
{
    ayla_ProtocolID_t protocolId;
    uint8_t opCode;
} AylaHeader_t;

typedef enum
{
    NAKERROR_TIMEOUT                        = 0x01,
    NAKERROR_LENGTHERROR                    = 0x02,
    NAKERROR_UNKNOWNTYPE                    = 0x03,
    NAKERROR_UNKNOWNVARIABLE                = 0x04,
    NAKERROR_INVALIDTLV                     = 0x05,
    NAKERROR_INVALIDOPCODE                  = 0x06,
    NAKERROR_INVALIDDATAPOINTOFFSET         = 0x07,
    NAKERROR_INVALIDREQUEST                 = 0x08,
    NAKERROR_INVALIDNAME                    = 0x0a,
    NAKERROR_CONNECTIONERROR                = 0x0b,
    NAKERROR_ADSBUSY                        = 0x0c,
    NAKERROR_INTERNAL                       = 0x0d,
    NAKERROR_CHECKSUMERROR                  = 0x0e,
    NAKERROR_ALREADYDONE                    = 0x0f,
    NAKERROR_BOOTERROR                      = 0x10,
    NAKERROR_OVERFLOWERROR                  = 0x11,
    NAKERROR_BADVALUE                       = 0x12,
    NAKERROR_PROPERTYLENGTH                 = 0x13,
    NAKERROR_UNEXPECTEDOPERATION            = 0x14,
    NAKERROR_DATAPOINTMETADATAERROR         = 0x15,
    NAKERROR_ACKIDERROR                     = 0x16,
} ayla_NAK_Error_t;

typedef enum
{
    TLVTYPE_NAME                            = 0x01,
    TLVTYPE_INTEGER                         = 0x02,
    TLVTYPE_UINTEGER                        = 0x03,
    TLVTYPE_BINARY                          = 0x04,
    TLVTYPE_TEXT                            = 0x05,
    TLVTYPE_CONFIGTOKENS                    = 0x06,
    TLVTYPE_ERROR                           = 0x07,
    TLVTYPE_FORMAT                          = 0x08,
    TLVTYPE_FLOATINGPOINT                   = 0x0B,
    TLVTYPE_BOOL                            = 0x0F,
    TLVTYPE_CONTINUATIONTOKEN               = 0x10,
    TLVTYPE_OFFSET                          = 0x11,
    TLVTYPE_LENGTH                          = 0x12,
    TLVTYPE_LOCATION                        = 0x13,
    TLVTYPE_EOF                             = 0x14,
    TLVTYPE_BCD                             = 0x15,
    TLVTYPE_CENTS                           = 0x16,
    TLVTYPE_NODES                           = 0x17,
    TLVTYPE_ECHO                            = 0x18,
    TLVTYPE_FEATUREMASK                     = 0x19,
    TLVTYPE_FACTORYRESET                    = 0x1A,
    TLVTYPE_DELETECONF                      = 0x1B,
    TLVTYPE_REGISTRATION                    = 0x1C,
    TLVTYPE_EVENTMASK                       = 0x1D,
    TLVTYPE_ACKID                           = 0x1E,
    TLVTYPE_SCHEDULE                        = 0x20,
    TLVTYPE_DATAPOINTMETADATA               = 0x34,
    TLVTYPE_WIFISTATUS                      = 0x35,
} ayla_TLV_def_t;

typedef enum
{
    CONFTOKEN_ENABLE                        = 0x01,
    CONFTOKEN_READY                         = 0x02,
    CONFTOKEN_SYSTEM                        = 0x03,
    CONFTOKEN_WIFI                          = 0x04,
    CONFTOKEN_CLIENT                        = 0x06,
    CONFTOKEN_STATUS                        = 0x08,
    CONFTOKEN_START                         = 0x09,
    CONFTOKEN_N                             = 0x0C,
    CONFTOKEN_TIME                          = 0x0D,
    CONFTOKEN_VERSION                       = 0x10,
    CONFTOKEN_TIMEZONE                      = 0x18,
    CONFTOKEN_DEVID                         = 0x1B,
    CONFTOKEN_SETUPMODE                     = 0x1C,
    CONFTOKEN_PROFILE                       = 0x25,
    CONFTOKEN_SSID                          = 0x26,
    CONFTOKEN_SECURITY                      = 0x27,
    CONFTOKEN_SCAN                          = 0x2E,
    CONFTOKEN_RSSI                          = 0x35,
    CONFTOKEN_REG                           = 0x4f,
    CONFTOKEN_DSTACTIVE                     = 0x74,
    CONFTOKEN_DSTCHANGE                     = 0x75,
    CONFTOKEN_DSTVALID                      = 0x76,
//    CONFTOKEN_                              = 0x04,
//    CONFTOKEN_                              = 0x04,
//    CONFTOKEN_                              = 0x04,
} ayla_conf_tokens_t;

typedef enum
{
    OTASTATUSERR_OK             = 0,
    OTASTATUSERR_LEN_ERR        = 0x02,
    OTASTATUSERR_INTERNAL       = 0x0d,
    OTASTATUSERR_CHECKSUM       = 0x0e,
    OTASTATUSERR_ALREADY        = 0x0f,
    OTASTATUSERR_BOOT           = 0x10,
} ayla_otaStatusErr_t;

typedef enum
{
    WIFIJOIN_SUCCESS            = 0x00,
    WIFIJOIN_RES_UNAVAILABLE    = 0x01,
    WIFIJOIN_CONNECT_TIMEOUT    = 0x02,
    WIFIJOIN_INVALID_KEY        = 0x03,
    WIFIJOIN_NETWK_NOT_FOUND    = 0x04,
    WIFIJOIN_NOT_AUTHENTICATED  = 0x05,
    WIFIJOIN_WRONG_KEY          = 0x06,
    WIFIJOIN_NO_IP_ADDRESS      = 0x07,
    WIFIJOIN_NO_IP_ROUTE        = 0x08,
    WIFIJOIN_NO_DNS_SERVER      = 0x09,
    WIFIJOIN_AP_DISCONNECTED    = 0x0a,
    WIFIJOIN_LOSS_OF_SIGNAL     = 0x0b,
    WIFIJOIN_DNS_LOOKUP_FAILED  = 0x0c,
    WIFIJOIN_ADS_CONN_REDIRECT  = 0x0d,
    WIFIJOIN_ADS_CONN_TIMEOUT   = 0x0e,
    WIFIJOIN_NO_PROFILE_AVAIL   = 0x0f,
    WIFIJOIN_SEC_METH_NOT_SUPP  = 0x10,
    WIFIJOIN_NETW_TYPE_UNSUPP   = 0x11,
    WIFIJOIN_WIFI_PROTOCOL_ERR  = 0x12,
    WIFIJOIN_ADS_AUTH_ERROR     = 0x13,
    WIFIJOIN_OPERATION_IN_PROG  = 0x14,
    WIFIJOIN_SETUP_UNSUPP       = 0x15,
} ayla_wifiStatus_t;

typedef struct
{
    ayla_ProtocolID_t ProtocolID;
    uint8_t OpCode;
    union
    {
        uint16_t RequestID;
        struct
        {
            uint8_t RequestID_LSB;
            uint8_t RequestID_MSB;
        };
    };
    uint8_t Payload[380];
} ayla_Packet_t;

typedef struct
{
    ayla_TLV_def_t Type;
    uint8_t Length;
    uint8_t Value[255];
} ayla_incomingTLV_t;

/*******************************************************************************
* Public structures definitions
*******************************************************************************/


/*******************************************************************************
* Public variables declarations
*******************************************************************************/
extern const AylaHeader_t OpCodeTable [];


/*******************************************************************************
* Public functions declarations
*******************************************************************************/



#endif /* _AylaDefinitions_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
