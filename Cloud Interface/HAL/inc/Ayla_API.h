/*******************************************************************************
* @file    Ayla_API.h
* @date    2017/04/26
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef Ayla_API_H
#define Ayla_API_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include ".\HAL\inc\AylaDefinitions.h"


/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define NULL_TERMINATION_SIZE           1		//format is "\0"
#define DELIMITER_SIZE                  1       //format is any 1 byte character (usually ';' or ',')
#define SETPOINT_STRING_SIZE            5       //format is "dd.d;"
#define TEMPERATURE_STRING_SIZE			5       //format is "dd.d;"
#define ALERT_MAX_STRING_SIZE			10		//format is "0;" or "1,2,3,4,5;"
#define HUMIDITY_STRING_SIZE			4		//format is "ddd;"
#define LOCK_STATE_STRING_SIZE			2		//format is "d;"
#define LOCK_DELTA_STRING_SIZE			2		//format is "d;"
#define HEAT_DEMAND_STRING_SIZE			4		//format is "ddd;"
#define SW_VERSION_STRING_SIZE			30		//format is "ff.ffssssssssssssss..." (00.15RC10xxxxxxxxx...)
#define OEM_VERSION_STRING_SIZE			5		//format is "ff.ff"
#define THERMOSTAT_NAME_STRING_SIZE		32      //support for maximum 32 bytes in the string (count in bytes,not in char)
#define THERMOSTAT_MODEL_STRING_SIZE    32      //support for maximum 32 bytes in the string (count in bytes,not in char)
#define GROUP_NAME_STRING_SIZE		    32      //support for maximum 32 bytes in the string (count in bytes,not in char)
#define GROUP_MEMBER_STRING_SIZE		3		//format is "dd,"
#define ACTIVITY_START_STRING_SIZE		2		//format is "d;"  ("0;" or "1;")
#define ACTIVITY_NAME_STRING_SIZE		32      //support for maximum 32 bytes in the string (count in bytes,not in char)
#define ACTIVITY_READYAT_STRING_SIZE	2		//format is "d;"  ("0;" or "1;")
#define ACTIVITY_STARTTIME_STRING_SIZE	5		//format is "dddd;"
#define ACTIVITY_WEEKDAYS_STRING_SIZE	2		//format is "d;" or "d,"
#define DIAGNOSTIC_STRING_SIZE		    100     //support for maximum 100 bytes in the string (count in bytes,not in char)
#define FLOOR_MODE_STRING_SIZE          2       //format is "d;"
#define RELAY_CYCLE_COUNT_STRING_SIZE   11      //format is "dddddddddd;"

#define OUTDOOR_TEMPERATURE_STRING_SIZE	6       //format is "-dd.d," or "-dd.d;"
#define YEAR_STRING_SIZE				5		//format is "dddd,"
#define MONTH_STRING_SIZE				3		//format is "dd,"
#define DAY_STRING_SIZE					3		//format is "dd,"
#define POWER_CONSUMPT_STRING_SIZE		6		//format is "ddddd,"
#define DAILY_SYSTEM_CONSUMPT_STRING_SIZE								\
									(OUTDOOR_TEMPERATURE_STRING_SIZE + 	\
                                     OUTDOOR_TEMPERATURE_STRING_SIZE + 	\
									 YEAR_STRING_SIZE + 				\
									 MONTH_STRING_SIZE + 				\
									 DAY_STRING_SIZE +   				\
									 POWER_CONSUMPT_STRING_SIZE)
#define MONTHLY_SYSTEM_CONSUMPT_STRING_SIZE								\
									(OUTDOOR_TEMPERATURE_STRING_SIZE + 	\
									 POWER_CONSUMPT_STRING_SIZE)


#define MANDATORY_WIFI_MSG(datapoint,code_section)  do{code_section; }while(datapoint==NULL);

typedef struct _aylaTLV_t
{
    ayla_TLV_def_t type;
    uint16_t length;            //actual tlv.length is 8 bits for protocol send
                                // but for management purposes, it needs to
                                // be at 16 bits.  TEXT Tlvs can have up to 1024 bytes
    uint8_t *value;
    uint8_t isLongString;       // flag used for long string management
    uint16_t longStringOffset;
    uint16_t longStringTotalLength;
    struct _aylaTLV_t *next;
} aylaTLV_t;

typedef struct
{
    AylaHeader_t header;
    union
    {
        uint16_t RequestID;
        struct
        {
            uint8_t RequestID_LSB;
            uint8_t RequestID_MSB;
        };
    };
    uint8_t headerFooterSize;

    struct _aylaTLV_t *firstTLV;
    struct _aylaTLV_t *lastTLV;
} aylaDataPoint_t;


/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
aylaDataPoint_t* Ayla_CreateDataPoint (ayla_OpCode_t opCode, uint16_t requestId);
void Ayla_AddTLV_to_dataPoint(aylaDataPoint_t ** dataPoint, aylaTLV_t * tlv);

aylaTLV_t * Ayla_CreateCentsTLV (uint32_t value);
aylaTLV_t * Ayla_CreateIntegerTLV (uint32_t value);
aylaTLV_t * Ayla_CreateTextTLV (char * text);
aylaTLV_t * Ayla_CreateConfigTokenTLV (int numberOfToken, ...);
aylaTLV_t * Ayla_CreateFeatureMaskTLV (uint8_t mask);
aylaTLV_t * Ayla_CreateNameTLV (uint8_t const * name);
aylaTLV_t * Ayla_CreateErrorTLV (uint8_t error);
aylaTLV_t * Ayla_CreateBinaryTLV (uint8_t * table, uint8_t tableLength);
aylaTLV_t * Ayla_CreateBooleanTLV (uint8_t value);
ayla_OpCode_t Get_OpCode(AylaHeader_t opCodeDescription);
uint32_t DeserializeToInteger32Bits (uint8_t * bufferIn, uint8_t size);
void destroy_datapoint(aylaDataPoint_t ** dataPoint);

#endif /* Ayla_API_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
