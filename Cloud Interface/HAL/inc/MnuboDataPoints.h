/*******************************************************************************
* @file    MnuboDataPoints.h
* @date    2017/01/25
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef _MnuboDataPoints_H
#define _MnuboDataPoints_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include ".\DB\inc\THREAD_DBDataTypes.h"


/*******************************************************************************
* Public constants definitions
*******************************************************************************/
typedef enum
{
    MNUBO_OBJ_TSTAT_BASE        = 0x0000,
    MNUBO_OBJ_GROUP_BASE        = 0x0100,
    MNUBO_OBJ_ACTIVITY_BASE     = 0x0200,
    MNUBO_OBJ_RESIDENCE         = 0x1000,

    MNUBO_OBJ_MASK              = 0xFF00,
    MNUBO_ID_MASK               = 0x00FF,
} mnuboObjId_t;

typedef enum
{
    MNUBO_EVENT_UPDATE_GROUP_SETPOINT = 0,
    MNUBO_EVENT_UPDATE_RESIDENCE,
    MNUBO_EVENT_UPDATE_TSTAT_SETPOINT,
    MNUBO_EVENT_UPDATE_CONSUMPTION,
    MNUBO_EVENT_UPDATE_SET_ALERT,
    MNUBO_EVENT_UPDATE_CLEAR_ALERT,
    MNUBO_EVENT_START_HEAT_SESSION,
    MNUBO_EVENT_STOP_HEAT_SESSION,

    MNUBO_EVENT_CNT,
} mnuboEventId_t;

typedef enum
{
    MNUBO_DATA_AMBIENT_SETPOINT = 0,
    MNUBO_DATA_AMBIENT_TEMPERATURE,
    MNUBO_DATA_GROUP_SETPOINT,
    MNUBO_DATA_TSTAT_MODEL,
    MNUBO_DATA_OUTDOOR_TEMPERATURE,
    MNUBO_DATA_OUTDOOR_HUMIDITY,
    MNUBO_DATA_RESIDENCE_STATE,
    MNUBO_DATA_TSTAT_DAILY_CONSUMPTION,
    MNUBO_DATA_AMBIENT_HUMIDITY,
    MNUBO_DATA_TRIGGER_LOCAL,
    MNUBO_DATA_TRIGGER_CLOUD,
    MNUBO_DATA_TRIGGER_ZIGBEE,
    MNUBO_DATA_TRIGGER_GEOFENCING,
    MNUBO_DATA_TRIGGER_ACTIVITY_LOCAL,
    MNUBO_DATA_TRIGGER_ACTIVITY_MOBILE,
    MNUBO_DATA_TRIGGER_ACTIVITY_SCHEDULED,
    MNUBO_DATA_TRIGGER_OPEN_WINDOW,
    MNUBO_DATA_TRIGGER_MODE_CHANGE,
    MNUBO_DATA_TRIGGER_GROUP_CHANGE,
    MNUBO_DATA_ALERT,
    MNUBO_DATA_ALERT_LIST,
    MNUBO_DATA_LATITUDE,
    MNUBO_DATA_LONGITUDE,
    MNUBO_DATA_SERIAL_NUMBER,
    MNUBO_DATA_TSTAT_NAME,
    MNUBO_DATA_GROUP_NAME,
    MNUBO_DATA_ACTIVITY_NAME,
    MNUBO_DATA_ACTIVITY_TIME,
    MNUBO_DATA_ACTIVITY_READY_AT,
    MNUBO_DATA_RESIDENCE_DAILY_CONSUMPTION,
    MNUBO_DATA_RESIDENCE_MONTHLY_CONSUMPTION,
} mnuboDataId_t;

typedef enum
{
	MNUBO_ELEMENT_TYPE_OBJECT = 0,
	MNUBO_ELEMENT_TYPE_EVENT,
} mnuboElementType_t;

/*******************************************************************************
* Public structures definitions
*******************************************************************************/
typedef struct _mnuboData_t
{
    mnuboElementType_t type;
    mnuboDataId_t dataId;
    uint8_t objectIdx;
    struct _mnuboData_t * nextData;
} mnuboData_t;

typedef struct _mnuboObject_t
{
    mnuboObjId_t deviceId;
    mnuboData_t * objectData;
    struct _mnuboObject_t * nextObject;
    void * parent_datapoint;
} mnuboObject_t;

typedef struct _mnuboEvent_t
{
    mnuboEventId_t eventId;
    mnuboObjId_t deviceId;
    mnuboData_t * eventData;
    struct _mnuboEvent_t *nextEvent;
    void * parent_datapoint;
} mnuboEvent_t;

typedef struct _mnuboDataPoint_t
{
    mnuboEvent_t *event;
    mnuboObject_t *object;
    uint8_t eventCnt;
    uint8_t objectCnt;
    struct _mnuboDataPoint_t * nextDataPoint;
    uint8_t invalid_datapoint;
//    struct _mnuboDataPoint_t * previousDataPoint;
} mnuboDataPoint_t;

typedef mnuboDataPoint_t * hMnuboDataPoint;
typedef mnuboEvent_t * hMnuboEvent;
typedef mnuboObject_t * hMnuboObject;
typedef mnuboData_t * hMnuboData;

/*******************************************************************************
* Public variables declarations
*******************************************************************************/


/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void InitAnalytics (void);
hMnuboDataPoint Mnubo_CreateNewDataPoint (void);
void Mnubo_DeleteDataPoint (hMnuboDataPoint * dataPoint);
hMnuboObject Mnubo_AddObjectToDataPoint (hMnuboDataPoint dataPoint,
                                            mnuboObjId_t deviceId);
hMnuboEvent Mnubo_AddEventToDataPoint (hMnuboDataPoint dataPoint,
                                          mnuboEventId_t eventId,
                                          mnuboObjId_t deviceId);
void Mnubo_AddDataToObject (hMnuboObject object, mnuboDataId_t dataId);
void Mnubo_AddDataToEvent (hMnuboEvent event, mnuboDataId_t dataId);
uint8_t Mnubo_SendDatapoint (hMnuboDataPoint dataPoint);

void ANALYTICS_SendDataPoint(hMnuboDataPoint dataPoint);
#endif /* _MnuboDataPoints_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
