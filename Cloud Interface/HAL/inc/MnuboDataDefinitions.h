/*******************************************************************************
* @file    MnuboDataDefinitions.h
* @date    2017/02/03
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef _MnuboDataDefinitions_H
#define _MnuboDataDefinitions_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include ".\DB\inc\THREAD_DBDataTypes.h"


/*******************************************************************************
* Public constants definitions
*******************************************************************************/



/*******************************************************************************
* Public structures definitions
*******************************************************************************/
typedef enum
{
    MNUBO_DATATYPE_STRING = 0,
    MNUBO_DATATYPE_UINTX100,
    MNUBO_DATATYPE_INTX100,
    MNUBO_DATATYPE_UINT,
    MNUBO_DATATYPE_INT,
    MNUBO_DATATYPE_TRIGGER_LOCAL,
    MNUBO_DATATYPE_TRIGGER_CLOUD,
    MNUBO_DATATYPE_TRIGGER_ZIGBEE,
    MNUBO_DATATYPE_TRIGGER_GEOFENCING,
    MNUBO_DATATYPE_TRIGGER_ACTIVITY_LOCAL,
    MNUBO_DATATYPE_TRIGGER_ACTIVITY_MOBILE,
    MNUBO_DATATYPE_TRIGGER_ACTIVITY_SCHEDULED,
    MNUBO_DATATYPE_TRIGGER_OPEN_WINDOW,
    MNUBO_DATATYPE_TRIGGER_MODE_CHANGE,
    MNUBO_DATATYPE_TRIGGER_GROUP_CHANGE,
    MNUBO_DATATYPE_ALERT,
    MNUBO_DATATYPE_ALERT_LIST,
    MNUBO_DATATYPE_LATITUDE,
    MNUBO_DATATYPE_LONGITUDE,
    MNUBO_DATATYPE_RESIDENCE_DAILY_CONSUMPTION,
} MnuboDataType_t;

typedef struct
{
    char * event_dataName;
    char * object_dataName;
    MnuboDataType_t dataType;
    dbDataTypes_t dbReferenceType;
} MnuboDataDefinition_t;


/*******************************************************************************
* Public variables declarations
*******************************************************************************/
MnuboDataDefinition_t mnuboDataDefinition [] =
{
    [MNUBO_DATA_AMBIENT_SETPOINT] =
    {
        .event_dataName = "tst_setpoint",   //Must not exceed 30 bytes
        .object_dataName = "room_setpoint", //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_UINTX100,
        .dbReferenceType = DBTYPE_AMBIENT_SETPOINT,
    },

    [MNUBO_DATA_AMBIENT_TEMPERATURE] =
    {
        .event_dataName = NULL,             //Must not exceed 30 bytes
        .object_dataName = "room_temp",     //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_UINTX100,
        .dbReferenceType = DBTYPE_AMBIENT_TEMPERATURE,
    },

    [MNUBO_DATA_GROUP_SETPOINT] =
    {
        .event_dataName = "grp_setpoint",       //Must not exceed 30 bytes
        .object_dataName = "group_setpoint",    //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_UINTX100,
        .dbReferenceType = DBTYPE_GROUP_SETPOINT,
    },

    [MNUBO_DATA_GROUP_NAME] =
    {
        .event_dataName = NULL,                //Must not exceed 30 bytes
        .object_dataName = "group_name",       //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_STRING,
        .dbReferenceType = DBTYPE_GROUP_NAME,
    },

    [MNUBO_DATA_TSTAT_MODEL] =
    {
        .event_dataName = NULL,                     //Must not exceed 30 bytes
        .object_dataName = "tstat_model_number",    //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_STRING,
        .dbReferenceType = DBTYPE_TSTAT_MODEL,
    },

    [MNUBO_DATA_TSTAT_NAME] =
    {
        .event_dataName = NULL,                     //Must not exceed 30 bytes
        .object_dataName = "tstat_name",            //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_STRING,
        .dbReferenceType = DBTYPE_TSTAT_NAME,
    },

    [MNUBO_DATA_SERIAL_NUMBER] =
    {
        .event_dataName = NULL,                     //Must not exceed 30 bytes
        .object_dataName = "tstat_serial_number",   //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_STRING,
        .dbReferenceType = DBTYPE_SERIAL_NUMBER,
    },

    [MNUBO_DATA_OUTDOOR_TEMPERATURE] =
    {
        .event_dataName = NULL,             //Must not exceed 30 bytes
        .object_dataName = "outdoor_temp",  //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_INTX100,
        .dbReferenceType = DBTYPE_OUTDOOR_TEMPERATURE,
    },

    [MNUBO_DATA_OUTDOOR_HUMIDITY] =
    {
        .event_dataName = NULL,             //Must not exceed 30 bytes
        .object_dataName = "outdoor_hum",   //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_UINT,
        .dbReferenceType = DBTYPE_OUTDOOR_HUMIDITY,
    },

    [MNUBO_DATA_RESIDENCE_STATE] =
    {
        .event_dataName = "residence_state",    //Must not exceed 30 bytes
        .object_dataName = "residence_state",   //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_UINT,
        .dbReferenceType = DBTYPE_HOME_STATE,
    },

    [MNUBO_DATA_TSTAT_DAILY_CONSUMPTION] =
    {
        .event_dataName = "tstat_daily_consumption",    //Must not exceed 30 bytes
        .object_dataName = "tstat_daily_consumption",   //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_UINT,
        .dbReferenceType = DBTYPE_LOCAL_DAILY_CONSUMPTION,
    },

    [MNUBO_DATA_RESIDENCE_DAILY_CONSUMPTION] =
    {
        .event_dataName = "daily_consumption",    //Must not exceed 30 bytes
        .object_dataName = "daily_consumption",   //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_RESIDENCE_DAILY_CONSUMPTION,
        .dbReferenceType = DBTYPE_DAILY_CONSUMPTION,
    },

    [MNUBO_DATA_RESIDENCE_MONTHLY_CONSUMPTION] =
    {
        .event_dataName = "monthly_consumption",    //Must not exceed 30 bytes
        .object_dataName = "monthly_consumption",   //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_UINT,
        .dbReferenceType = DBTYPE_MONTHLY_CONSUMPTION,
    },

    [MNUBO_DATA_AMBIENT_HUMIDITY] =
    {
        .event_dataName = NULL,             //Must not exceed 30 bytes
        .object_dataName = "room_humidity", //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_UINT,
        .dbReferenceType = DBTYPE_AMBIENT_HUMIDITY,
    },

    [MNUBO_DATA_TRIGGER_LOCAL] =
    {
        .event_dataName = "trigger",    //Must not exceed 30 bytes
        .object_dataName = NULL,        //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_TRIGGER_LOCAL,
        .dbReferenceType = NULL,
    },

    [MNUBO_DATA_TRIGGER_CLOUD] =
    {
        .event_dataName = "trigger",    //Must not exceed 30 bytes
        .object_dataName = NULL,        //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_TRIGGER_CLOUD,
        .dbReferenceType = NULL,
    },

    [MNUBO_DATA_TRIGGER_ZIGBEE] =
    {
        .event_dataName = "trigger",    //Must not exceed 30 bytes
        .object_dataName = NULL,        //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_TRIGGER_ZIGBEE,
        .dbReferenceType = NULL,
    },

    [MNUBO_DATA_TRIGGER_GEOFENCING] =
    {
        .event_dataName = "trigger",    //Must not exceed 30 bytes
        .object_dataName = NULL,        //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_TRIGGER_GEOFENCING,
        .dbReferenceType = NULL,
    },

    [MNUBO_DATA_TRIGGER_ACTIVITY_LOCAL] =
    {
        .event_dataName = "trigger",    //Must not exceed 30 bytes
        .object_dataName = NULL,        //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_TRIGGER_ACTIVITY_LOCAL,
        .dbReferenceType = NULL,
    },

    [MNUBO_DATA_TRIGGER_ACTIVITY_MOBILE] =
    {
        .event_dataName = "trigger",    //Must not exceed 30 bytes
        .object_dataName = NULL,        //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_TRIGGER_ACTIVITY_MOBILE,
        .dbReferenceType = NULL,
    },

    [MNUBO_DATA_TRIGGER_ACTIVITY_SCHEDULED] =
    {
        .event_dataName = "trigger",    //Must not exceed 30 bytes
        .object_dataName = NULL,        //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_TRIGGER_ACTIVITY_SCHEDULED,
        .dbReferenceType = NULL,
    },

    [MNUBO_DATA_TRIGGER_OPEN_WINDOW] =
    {
        .event_dataName = "trigger",    //Must not exceed 30 bytes
        .object_dataName = NULL,        //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_TRIGGER_OPEN_WINDOW,
        .dbReferenceType = NULL,
    },

    [MNUBO_DATA_TRIGGER_MODE_CHANGE] =
    {
        .event_dataName = "trigger",    //Must not exceed 30 bytes
        .object_dataName = NULL,        //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_TRIGGER_MODE_CHANGE,
        .dbReferenceType = NULL,
    },

    [MNUBO_DATA_TRIGGER_GROUP_CHANGE] =
    {
        .event_dataName = "trigger",    //Must not exceed 30 bytes
        .object_dataName = NULL,        //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_TRIGGER_GROUP_CHANGE,
        .dbReferenceType = NULL,
    },

    [MNUBO_DATA_ALERT] =
    {
        .event_dataName = "alert",      //Must not exceed 30 bytes
        .object_dataName = "alert",     //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_ALERT,
        .dbReferenceType = DBTYPE_ACTIVE_ALERTS,
    },

    [MNUBO_DATA_ALERT_LIST] =
    {
        .event_dataName = "alert_list",  //Must not exceed 30 bytes
        .object_dataName = "alert_list", //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_ALERT_LIST,
        .dbReferenceType = DBTYPE_ACTIVE_ALERTS,
    },

    [MNUBO_DATA_LATITUDE] =
    {
        .event_dataName = NULL,    //Must not exceed 30 bytes
        .object_dataName = "x_registration_latitude",        //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_LATITUDE,
        .dbReferenceType = DBTYPE_GEOFENCE_CENTER,
    },

    [MNUBO_DATA_LONGITUDE] =
    {
        .event_dataName = NULL,    //Must not exceed 30 bytes
        .object_dataName = "x_registration_longitude",        //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_LONGITUDE,
        .dbReferenceType = DBTYPE_GEOFENCE_CENTER,
    },

    [MNUBO_DATA_ACTIVITY_NAME] =
    {
        .event_dataName = NULL,                //Must not exceed 30 bytes
        .object_dataName = "activity_name",    //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_STRING,
        .dbReferenceType = DBTYPE_ACTIVITY_NAME,
    },

    [MNUBO_DATA_ACTIVITY_TIME] =
    {
        .event_dataName = NULL,                //Must not exceed 30 bytes
        .object_dataName = "activity_start_time",    //Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_UINT,
        .dbReferenceType = DBTYPE_ACTIVITY_START_TIME,
    },

    [MNUBO_DATA_ACTIVITY_READY_AT] =
    {
        .event_dataName = NULL,                //Must not exceed 30 bytes
        .object_dataName = "activity_ready_at",//Must not exceed 30 bytes
        .dataType = MNUBO_DATATYPE_UINT,
        .dbReferenceType = DBTYPE_ACTIVITY_READY_AT,
    },
};


/*******************************************************************************
* Public functions declarations
*******************************************************************************/



#endif /* _MnuboDataDefinitions_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
