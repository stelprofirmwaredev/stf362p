/*******************************************************************************
* @file    AylaNetwork.h
* @date    2016/09/20
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef _AylaNetwork_H
#define _AylaNetwork_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>

#include ".\HAL\inc\AylaDefinitions.h"
#include ".\HAL\inc\HAL_WiFiTypes.h"

/*******************************************************************************
* Public constants definitions
*******************************************************************************/
typedef enum
{
    WIFISCANEVENT_STARTNEWSCAN = 0,
    WIFISCANEVENT_SCANCOMPLETE,
    WIFISCANEVENT_SNAPSHOTCAPTURED,
    WIFISCANEVENT_SSIDRECEIVED,
    WIFISCANEVENT_SECURITYRECEIVED,
    WIFISCANEVENT_STOPSCAN,
} wifiScanSMEvents_t;

typedef enum
{
    WIFIJOINEVENT_JOINNETWORK = 0,
    WIFIJOINEVENT_NAKRESPONSE,
    WIFIJOINEVENT_CONNECTED,
    WIFIJOINEVENT_CONNECTIONTIMEOUT,
    WIFIJOINEVENT_BADPASSWORD,
    WIFIJOINEVENT_NOTFOUND,
    WIFIJOINEVENT_CONNECTIONLOSS,
    WIFIJOINEVENT_ERROR,
    WIFIJOINEVENT_NOSERVER,

} wifiJoinSMEvents_t;

#define WIFI_SCAN_POLL_READY_PERIOD       50            //expressed in ms
#define WIFI_JOIN_TIMEOUT                 25000         //expressed in ms

/*******************************************************************************
* Public structures definitions
*******************************************************************************/


typedef enum
{
    WIFISCANSM_INIT             = 0,
    WIFISCANSM_WAITSCANCOMPLETE,
    WIFISCANSM_CAPTURESCANRESULT,

    WIFISCANSM_TOTALSTEPS
} wifiScanSMStates_t;

typedef enum
{
    WIFIJOINSM_READYTOJOIN             = 0,
    WIFIJOINSM_JOINING,

    WIFIJOINSM_TOTALSTEPS
} wifiJoinSMStates_t;



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void WifiNetworksScan_StateMachine (wifiScanSMEvents_t event, void * pData);
void WifiNetworksJoin_StateMachine (wifiJoinSMEvents_t event, void * pData);
void ControlOps_ParseConfToken_WiFiScan (ayla_Packet_t * receivedPacket, uint16_t payloadLength);
void ControlOps_ParseConfToken_WiFiProfile (ayla_Packet_t * receivedPacket, uint16_t payloadLength);
void ControlOps_ParseConfToken_WiFiStatus (ayla_Packet_t * receivedPacket, uint16_t payloadLength);
void AylaJoinNetwork (uint8_t network, uint8_t* pKey);
void StartNewWiFiScan (void (*callback)(void *));
void StopWiFiScan(void);
#endif /* _AylaNetwork_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
