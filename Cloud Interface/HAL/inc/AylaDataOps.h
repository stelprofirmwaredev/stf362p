/*******************************************************************************
* @file    AylaDataOps.h
* @date    2016/09/06
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef _AylaDataOps_H
#define _AylaDataOps_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>
#include ".\HAL\inc\AylaDefinitions.h"
#include ".\HAL\inc\HAL_WiFiTypes.h"

/*******************************************************************************
* Public constants definitions
*******************************************************************************/


/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void Ayla_HandleDataOperation (ayla_Packet_t * receivedPacket, uint16_t packetLength);
void GetAllProperties(void);
void GetSpecificProperty(WIFITYPE_t wifiType);
void SetDisconnectReason(uint8_t reason);
uint8_t GetDisconnectReason(void);

#endif /* _AylaDataOps_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
