/*******************************************************************************
* @file    HAL_CloudInterface.h
* @date    2016/08/24
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef HAL_CloudInterface_H
#define HAL_CloudInterface_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>
#include ".\WiFi\inc\THREAD_WiFi.h"
#include ".\BSP\Ayla WM-N-BM-30\inc\BSP_Ayla_WM_N_MB_30.h"
#include ".\HAL\inc\AylaControlOps.h"
#include ".\HAL\inc\AylaDataOps.h"
#include ".\HAL\inc\AylaNetwork.h"
#include ".\HAL\inc\HAL_WiFiTypes.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\HAL\inc\AylaDefinitions.h"
#include ".\HAL\inc\Ayla_API.h"

#ifdef __ICCARM__
    #include "cmsis_os.h"
#endif

/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define SPI_WITHOUT_CRC             0
#define SPI_WITH_CRC                1
#define SPI_TXRX_MODE               0
#define SPI_RX_MODE                 1

/*******************************************************************************
Keep the delay after the RELEASE_WIFI_RESET_LINE long enough,
otherwise when the WiFi module disables its core after 10 minutes of inactivity,
we are unable to restart it because the WiFi Radio ready condition is never true
and we keep resetting the wifi module forever, therefore creating a deadlock
*******************************************************************************/
#define HAL_RESET_WIFI()                PULL_WIFI_RESET_LINE();\
                                        osDelay(100);\
                                        RELEASE_WIFI_RESET_LINE();\
                                        osDelay(1500);\

#define HAL_IS_WIFIRADIO_READY()          IS_WIFIRADIO_READY()

#define HAL_POLL_RADIO_FOR_NEW_MESSAGE()  BSP_PollIncomingMessageIntr()

#define HAL_READ_MESSAGE_FROM_RADIO(buffer,rxlength)                           \
                                          BSP_FetchPacketFromWifi(             \
                                                            buffer,            \
                                                            rxlength)

#define HAL_SCAN_WIFI_NETWORK(cb)           StartNewWiFiScan(cb)
#define HAL_STOP_WIFI_SCAN                  StopWiFiScan


#define TYPE_INTEGER            0x02
#define TYPE_UNSIGNED_INTEGER   0x03
#define TYPE_BINARY             0x04
#define TYPE_BOOLEAN            0x0F

#define LENGTH_8BITS    1;
#define LENGTH_16BITS   2;
#define LENGTH_32BITS   4;
#define LENGTH_64BITS   8;


/*******************************************************************************
* Public structures definitions
*******************************************************************************/
typedef enum
{
    WIFISTARTEVENT_BEGININIT       = 0,
    WIFISTARTEVENT_LISTENERSERVICE_ENABLED,
    WIFISTARTEVENT_VERSIONRECEIVED,
    WIFISTARTEVENT_SSID_RECEIVED,
    WIFISTARTEVENT_PFILE_ENABLEBIT_RECEIVED,
    WIFISTARTEVENT_PROFILELOADED,
    WIFISTARTEVENT_UTCTIME_RECEIVED,
    WIFISTARTEVENT_TIMEZONE_RECEIVED,
    WIFISTARTEVENT_DST_ACTIVE_RECEIVED,
    WIFISTARTEVENT_DST_CHANGE_RECEIVED,
    WIFISTARTEVENT_DST_VALID_RECEIVED,
    WIFISTARTEVENT_REG_STATUS_RECEIVED,
    WIFISTARTEVENT_DSN_RECEIVED,
    WIFISTARTEVENT_CLOUD_IN_SYNC,
    WIFISTARTEVENT_MISSING_ELEMENTS,

} wifiRadioInitEvents_t;


typedef struct
{
    uint16_t packetSize;
    ayla_Packet_t packet;
} wifiMsg_t;

typedef struct
{
    uint8_t sendRequest;
    uint8_t index;
    dbChangeSource_t source;
}propertiesToSend_t;

/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void HAL_WiFi_SPI_Init(uint8_t crcEnable, uint8_t receiveOnlyMode);
void InitCloudInterface(void);
void HAL_ReadWiFiData (uint8_t * dataBuffer, uint8_t length);
void HAL_SendProperty(WIFITYPE_t propType, uint8_t* index, dbChangeSource_t* source);
void HAL_ParseReceivedPacket(uint8_t * buffer, uint16_t packetLength);
void HAL_JoinWiFiNetworks (wifi_joinNetworks_t * networkInfo);
void HAL_GetCurrentNetwork (void (*currentNetworkcB)(void));
void HAL_GenerateNewRegistrationToken (void);
void HAL_ResetCloudInterface (void);
uint8_t HAL_QWiFiMsg(aylaDataPoint_t *newDataPoint);
int8_t HAL_isWiFiTxComplete(void);
void HAL_FreeWiFiQ(void);
int8_t HAL_SendCommand(void);
uint8_t HAL_isWifiMsgToTx(void);
void HAL_SetOTAPendingAlert(void);
void ApplyOTANow(void);
uint16_t HAL_GetOTAResetTimer(void);

#endif /* HAL_CloudInterface_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
