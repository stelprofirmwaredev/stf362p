/*******************************************************************************
* @file    AylaMetaData.h
* @date    2016/12/09
* @authors J-F. Simard
* @brief 
*******************************************************************************/

#ifndef _AylaMetaData_H
#define _AylaMetaData_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>
#include ".\HAL\inc\HAL_WiFiTypes.h"
#include ".\DB\inc\THREAD_DB.h"

/*******************************************************************************
* Public constants definitions
*******************************************************************************/
typedef struct _metaValueDescriptor_t
{
    char * valueName;
    dbDataTypes_t valueDataType;
    const struct _metaValueDescriptor_t *nextValue;
} metaValueDescriptor_t;

typedef struct
{
    char * eventType;
    const metaValueDescriptor_t * metaValue;
} metaEvent_t;

typedef struct
{
    char * eventType;
    const metaValueDescriptor_t * metaValue;
} metaObject_t;

/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/
extern const metaEvent_t UpdateSetpointEvent;

/*******************************************************************************
* Public functions declarations
*******************************************************************************/
uint16_t  AylaAddMetaEvent (uint8_t * metaBuffer, uint8_t metaNumber, WIFITYPE_t property);


#endif /* _AylaMetaData_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
