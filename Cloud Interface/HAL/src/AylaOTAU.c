/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    AylaOTAU.c
* @date    2016/09/20
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include ".\HAL\inc\AylaOTAU.h"
#include ".\HAL\inc\AylaDefinitions.h"
#include ".\HAL\inc\HAL_CloudInterface.h"
#include ".\Console\inc\THREAD_Console.h"

#include ".\BSP\Ayla WM-N-BM-30\inc\BSP_Ayla_WM_N_MB_30.h"

#include ".\DB\inc\THREAD_DB.h"


#include "trcRecorder.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
extern char* otaChunksChannel;
extern TimerHandle_t RadioOTATimeout;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/20
*******************************************************************************/
#pragma optimize=none
void FetchMCU_OTAU_Chunk(ayla_Packet_t * receivedPacket, uint16_t packetLength)
{
    ayla_incomingTLV_t * offsetTLV;
    ayla_incomingTLV_t * dataChunkTLV;
    dbMcuOtauChunk_t otauChunk;

    if (packetLength > 0)
    {
        offsetTLV = (ayla_incomingTLV_t *)&receivedPacket->Payload[0];
        otauChunk.offset = offsetTLV->Value[3];
        otauChunk.offset |= (uint32_t)offsetTLV->Value[2]<<8;
        otauChunk.offset |= (uint32_t)offsetTLV->Value[1]<<16;
        otauChunk.offset |= (uint32_t)offsetTLV->Value[0]<<24;


        dataChunkTLV = (ayla_incomingTLV_t *)&offsetTLV->Value[4];
        otauChunk.size = dataChunkTLV->Length;

        for (uint8_t i = 0; i < otauChunk.size; i++)
        {
            otauChunk.data[i] = dataChunkTLV->Value[i];
        }

        vTracePrint(otaChunksChannel, "Chunk Received");

        DBTHRD_StoreOtauChunk(&otauChunk);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/14
*******************************************************************************/
#pragma optimize=none
void MCU_OTAU_Request(ayla_Packet_t * receivedPacket, uint16_t packetLength)
{
    ayla_incomingTLV_t * firmwareVersionTLV;
    ayla_incomingTLV_t * imageSizeTLV;
    uint32_t imageSize;
    char firmwareVersion[100];

    if (packetLength > 0)
    {
        firmwareVersionTLV = (ayla_incomingTLV_t *)receivedPacket->Payload;
        {
            uint8_t i;
            for (i = 0; i < firmwareVersionTLV->Length; i++)
            {
                firmwareVersion[i] = firmwareVersionTLV->Value[i];
            }
            firmwareVersion[i] = 0;
        }
        imageSizeTLV =
            (ayla_incomingTLV_t *)&firmwareVersionTLV->Value[firmwareVersionTLV->Length];


        /************************************************************/
        /*              Network Integer format is MSB first         */
        /************************************************************/
        imageSize = imageSizeTLV->Value[3];
        imageSize |= (uint32_t)imageSizeTLV->Value[2]<<8;
        imageSize |= (uint32_t)imageSizeTLV->Value[1]<<16;
        imageSize |= (uint32_t)imageSizeTLV->Value[0]<<24;

        if (imageSize <= OTAUIMAGE_MAX_SIZE)
        {
            CONSOLE_EVENT_MESSAGE ("OTA Request for firmware version ");
            CONSOLE_MESSAGE_APPEND_STRING ((char*)firmwareVersion);
            if (DBTHRD_StartMcuOtauSession(imageSize) != 0)
            {
                aylaDataPoint_t *newDataPoint;
                
                newDataPoint = Ayla_CreateDataPoint (CONTROLOPCODE_MCUOTA_STATUS, 0x00d0);
                
                Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateErrorTLV (OTASTATUSERR_ALREADY));
                
                HAL_QWiFiMsg (newDataPoint);
                
            }
            else
            {
                HAL_QWiFiMsg (Ayla_CreateDataPoint (CONTROLOPCODE_MCUOTA_REPORT_START, 0x00c0));
            }
        }
        else
        {           
            aylaDataPoint_t *newDataPoint;
            
            newDataPoint = Ayla_CreateDataPoint (CONTROLOPCODE_MCUOTA_STATUS, 0x00d0);
            
            Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateErrorTLV (OTASTATUSERR_LEN_ERR));
            
            HAL_QWiFiMsg (newDataPoint);
            
        }
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/14
*******************************************************************************/
void ConfirmRadioOTAU_Ready (void)
{    
    HAL_QWiFiMsg (Ayla_CreateDataPoint (CONTROLOPCODE_OTA_COMMAND, 0x00b0));

    xTimerStart(RadioOTATimeout, 0);
}

/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
