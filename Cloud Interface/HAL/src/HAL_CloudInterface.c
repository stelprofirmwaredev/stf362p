/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    HAL_CloudInterface.c
* @date    2016/08/24
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <string.h>
#include <time.h>
#include ".\HAL\inc\HAL_CloudInterface.h"

#include "stm32f4xx_hal.h"

#include ".\BSP\Ayla WM-N-BM-30\inc\BSP_Ayla_WM_N_MB_30.h"
#include ".\HAL\inc\AylaDataOps.h"
#include ".\HAL\inc\AylaControlOps.h"
#include ".\HAL\inc\AylaNetwork.h"
#include ".\HAL\inc\AylaRegistration.h"
#include ".\HAL\inc\AylaDefinitions.h"
#include ".\HAL\inc\AylaProperties.h"

#include ".\DB\inc\THREAD_DBDataTypes.h"
#include ".\DB\inc\THREAD_DB.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
typedef enum
{
    WIFISTARTSM_INIT                = 0,
    WIFISTARTSM_GETWIFIMODULEVERSION,
    WIFISTARTSM_GETWIFIPROFILES,
    WIFISTARTSM_GETDSN,
    WIFISTARTSM_GET_UTC_TIME,
    WIFISTARTSM_GET_TIMEZONE,
    WIFISTARTSM_GET_DST_ACTIVE,
    WIFISTARTSM_GET_DST_CHANGE,
    WIFISTARTSM_GET_DST_VALID,
    WIFISTARTSM_GET_REG_STATUS,
    WIFISTARTSM_SYNC_CLOUD_DATA,

    WIFISTARTSM_SYSTEMRUNNING,

    WIFISTARTSM_NUMBEROFSTATES
} wifiRadioInitSM_t;

typedef struct _wifiMsgQ_t
{
    wifiMsg_t wifiMsg;
    struct _wifiMsgQ_t *next;
}wifiMsgQ_t;

#define OTA_ONE_MINUTE_DELAY    60000   //1 minute (in ms)
#define MAX_OTA_RESET_DELAY     240     //4 hours (in minutes)

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
#define WIFI_TX_MSG_MAX     3
SemaphoreHandle_t wifi_tx_msg_cnt;
//SemaphoreHandle_t wifi_q_available;
SemaphoreHandle_t xSem_DmaTx;
SemaphoreHandle_t xSem_DmaRx;

wifiRadioInitSM_t wifiInitCurrentState;
wifiMsgQ_t *wifiMsgQHead;
wifiMsgQ_t *wifiMsgQTail;

static uint16_t OTA_Reset_Minutes = 0;
static wifiMsgQ_t static_wifi_buffer;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
SPI_HandleTypeDef hWiFiSpi;
DMA_HandleTypeDef hWifiSpi_TxDma;
DMA_HandleTypeDef hWifiSpi_RxDma;
extern const prop_t propTable[];
TimerHandle_t OTA_Reset_Timer = NULL;
extern char* otaChunksChannel;
#ifdef __ICCARM__
    extern osThreadId wifiTaskHandle;
    extern osThreadId sendPropertyTaskHandle;
#endif

#ifdef FREERTOS_SUPPORT
    #include "trcRecorder.h"
    extern char* wifiEventChannel;
#endif

extern wifi_state_machine_t WiFiSerialState;
extern char ExceptionDiag[];
extern dbType_Time_t SystemTime;
extern propertiesToSend_t propertiesToSend[];

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
extern void EnableListenerService (void);
extern void GetWiFiModuleVersion(void);
extern void LoadWiFiProfiles (uint8_t profileID);
extern void GetActiveProfile (void (*currentNetworkcB)(void));
extern void GetSignalStrength (void);
extern void GetTimeZone (void);
extern void GetUtcTime (void);
extern void GetDSTActive (void);
extern void GetDSTChange (void);
extern void GetDSTValid (void);
extern void GetSecurity (uint8_t scanResultNumber);
extern void GetSSID (uint8_t scanResultNumber);
extern void PollWiFiScanReady (TimerHandle_t xTimer);
extern void SnapShotScanResult(void);
extern void UpdateCurrentNetwork(void);
extern void SendFactoryReset(void);
extern void GetDSN (void);
static void GPIO_Init(void);
DB_NOTIFICATION(OnCloudConnectionUpdate);
static void OnOTAResetTimeout( TimerHandle_t xTimer );
extern uint8_t GetResetCause(void);
extern void ClearResetCause(void);
static void SendResetReason(void);
DB_NOTIFICATION(PeriodicDiagnosticCheck);
extern void GetAllInfoFromCloud(void);


/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/17
*******************************************************************************/
static void GPIO_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOE_CLK_ENABLE();


    /**SPI4 GPIO Configuration
    PE2     ------> SPI4_SCK
    PE5     ------> SPI4_MISO
    PE6     ------> SPI4_MOSI
    */
    GPIO_InitStruct.Pin = WIFI__SPI__CLK_Pin|WIFI_MOSI_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_MEDIUM;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI4;
    HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = WIFI_MISO_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_MEDIUM;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI4;
    HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);


    HAL_GPIO_WritePin(WIFI_SLAVE_SELECT_GPIO_Port, WIFI_SLAVE_SELECT_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(WIFI_RST_GPIO_Port, WIFI_RST_Pin, GPIO_PIN_RESET);

    /*Configure GPIO pin : WIFI_SLAVE_SELECT_Pin */
    GPIO_InitStruct.Pin = WIFI_SLAVE_SELECT_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(WIFI_SLAVE_SELECT_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = WIFI_RST_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;//GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(WIFI_RST_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = WIFI_WIFI_INTR_N_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(WIFI_WIFI_INTR_N_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = WIFI_READY_N_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(WIFI_READY_N_GPIO_Port, &GPIO_InitStruct);

}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/01
*******************************************************************************/
static void DMA_Init(void)
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA2_Stream0_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);
  /* DMA2_Stream1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream1_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream1_IRQn);

}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/12/13
*******************************************************************************/
DB_NOTIFICATION(OnCloudConnectionUpdate)
{
    WIFITHRD_GetCurrentNetwork (UpdateCurrentNetwork);
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/14
*******************************************************************************/
#pragma optimize=none
void WiFiRadioInitStateMachine (wifiRadioInitEvents_t event, void * pData)
{
    ayla_incomingTLV_t * profileSsidTLV;
    ayla_incomingTLV_t * dataTLV;
    uint8_t profileId;
    dbType_WiFiProfile_t receivedProfile;
    uint8_t i;
    uint16_t applyDst;

    time_t currentFrom1970Time;
    time_t dstChangeTimeFrom1970Time;
    struct tm * currentTime;

    dbType_Time_t dbTime;
    dbType_Date_t dbDate;
    dbType_CloudConnectivity_t dbCloudConnectivity;
    uint16_t timeZone;
    char radioVersion[16];
    char* pch;
//    uint8_t debugLevel;

    switch (wifiInitCurrentState)
    {
    /****************************************/
    /****************************************/
    case WIFISTARTSM_INIT:
        switch (event)
        {
        /****************************************/
        /****************************************/
        case WIFISTARTEVENT_BEGININIT:
            EnableListenerService();
            GetWiFiModuleVersion();

            wifiInitCurrentState = WIFISTARTSM_GETWIFIMODULEVERSION;
            break;

        /****************************************/
        /****************************************/
        case WIFISTARTEVENT_VERSIONRECEIVED:
            dataTLV = (ayla_incomingTLV_t * )pData;
            pch = strtok((char*)dataTLV->Value, " ");
            pch = strtok(NULL, " ");
            strcpy((char*)radioVersion, pch);
            DBTHRD_SetData(DBTYPE_WIFI_MODULE_VERSION, (void*)&radioVersion, INDEX_DONT_CARE, DBCHANGE_CLOUD);
            LoadWiFiProfiles(0);
            break;



        /****************************************/
        /*           unhandled events           */
        /****************************************/
        case WIFISTARTEVENT_PFILE_ENABLEBIT_RECEIVED:
        case WIFISTARTEVENT_SSID_RECEIVED:
        case WIFISTARTEVENT_PROFILELOADED:
        default:
            break;
        }
        break;

    case WIFISTARTSM_GETWIFIMODULEVERSION:
        switch (event)
        {
        /****************************************/
        /****************************************/
        case WIFISTARTEVENT_VERSIONRECEIVED:
            dataTLV = (ayla_incomingTLV_t * )pData;
            pch = strtok((char*)dataTLV->Value, " ");
            pch = strtok(NULL, " ");
            strcpy((char*)radioVersion, pch);
            DBTHRD_SetData(DBTYPE_WIFI_MODULE_VERSION, (void*)&radioVersion, INDEX_DONT_CARE, DBCHANGE_CLOUD);
            LoadWiFiProfiles(0);

            wifiInitCurrentState = WIFISTARTSM_GETWIFIPROFILES;
            break;

        /****************************************/
        /*           unhandled events           */
        /****************************************/
        default:
            break;
        }
        break;

    /****************************************/
    /****************************************/
    case WIFISTARTSM_GETWIFIPROFILES:
        switch (event)
        {
        /****************************************/
        /****************************************/
        case WIFISTARTEVENT_SSID_RECEIVED:
            profileSsidTLV = (ayla_incomingTLV_t *)pData;
            profileId = profileSsidTLV->Value[2];             //[2] contains profile ID

            dataTLV = (ayla_incomingTLV_t *)&profileSsidTLV->Value[4];  //start of next TLV

            receivedProfile.profileId = profileId;
            for (i = 0; i < dataTLV->Length; i++)
            {
                receivedProfile.SsID[i] = dataTLV->Value[i];
            }
            receivedProfile.SsID[i] = 0;                    //add NULL at the end of the string

            DBTHRD_SetData(DBTYPE_SSID,(void*)&receivedProfile, INDEX_DONT_CARE, DBCHANGE_LOCAL);

            profileId++;
            if (profileId < AYLA_MAX_WIFI_PROFILE)
            {
                LoadWiFiProfiles(profileId);
            }
            else
            {
                GetActiveProfile(NULL);
                GetDSN();
                wifiInitCurrentState = WIFISTARTSM_GETDSN;
            }
            break;


        /****************************************/
        /* If receive a Begin Init during here, */
        /* force to restart init                */
        /****************************************/
        case WIFISTARTEVENT_BEGININIT:
            LoadWiFiProfiles(0);
            break;

        /****************************************/
        /*           unhandled events           */
        /****************************************/
        default:
            break;
        }
        break;


    case WIFISTARTSM_GETDSN:
        switch (event)
        {
        /****************************************/
        /****************************************/
        case WIFISTARTEVENT_DSN_RECEIVED:
            dataTLV = (ayla_incomingTLV_t * )pData;
            char dsn[30];

            if (dataTLV->Length < sizeof(dsn))
            {
                strncpy (dsn, (char const*)dataTLV->Value, dataTLV->Length);
            }
            else
            {
                strncpy (dsn, (char const*)dataTLV->Value, sizeof(dsn));
            }

            DBTHRD_SetData(DBTYPE_DSN, (void*)dsn, INDEX_DONT_CARE, DBCHANGE_CLOUD);

            wifiInitCurrentState = WIFISTARTSM_SYNC_CLOUD_DATA;
            SynchronizeCloud();
            break;

        default:
            break;
        }
        break;

    /****************************************/
    /****************************************/
    case WIFISTARTSM_SYNC_CLOUD_DATA:
        switch (event)
        {
        /****************************************/
        /****************************************/
        case WIFISTARTEVENT_CLOUD_IN_SYNC:
            GetTimeZone();
            wifiInitCurrentState = WIFISTARTSM_GET_TIMEZONE;
            break;

        case WIFISTARTEVENT_MISSING_ELEMENTS:
            GetAllInfoFromCloud();
            break;

        default:
            break;
        }
        break;

    /****************************************/
    /****************************************/
    case WIFISTARTSM_GET_UTC_TIME:
        switch (event)
        {
        /****************************************/
        /****************************************/
        case WIFISTARTEVENT_UTCTIME_RECEIVED:
            dataTLV = (ayla_incomingTLV_t * )pData;

            currentFrom1970Time = (time_t)DeserializeToInteger32Bits(dataTLV->Value, dataTLV->Length);

            DBTHRD_GetData(DBTYPE_LOCALTIME, (void*)&dbTime, NULL);

            applyDst = 0;
            if (dbTime.IsDstValid)
            {
                if (dbTime.IsDstActive)
                {
                    if (currentFrom1970Time < dbTime.DstChangeTime)
                    {
                        applyDst = 1;
                    }
                }
                else
                {
                    if (currentFrom1970Time > dbTime.DstChangeTime)
                    {
                        applyDst = 1;
                    }
                }
            }
            else
            {
                ;
            }

            currentFrom1970Time -= ((dbTime.TimeZone*60) - (applyDst*60*60));

            currentTime = gmtime(&currentFrom1970Time);


            /* Must initialize the calendar date before the time because    */
            /*  the setUTCTIME will recalculate the day of year from the    */
            /*  time zone.                                                  */
            dbDate.Year = currentTime->tm_year + 1900;
            dbDate.Month = currentTime->tm_mon + 1;
            dbDate.Day = currentTime->tm_mday;
            dbDate.DayOfWeek = currentTime->tm_wday;
            dbDate.DayOfYear = currentTime->tm_yday;
            DBTHRD_SetData(DBTYPE_DATE, (void*)&dbDate, INDEX_DONT_CARE, DBCHANGE_CLOUD);



            dbTime.Hours = currentTime->tm_hour;
            dbTime.Minutes = currentTime->tm_min;
            dbTime.Seconds = currentTime->tm_sec;

            DBTHRD_SetData(DBTYPE_LOCALTIME, (void*)&dbTime, INDEX_DONT_CARE, DBCHANGE_CLOUD);

            GetRegistrationStatus(NULL);
            wifiInitCurrentState = WIFISTARTSM_GET_REG_STATUS;
            break;

        /****************************************/
        /*           unhandled events           */
        /****************************************/
        case WIFISTARTEVENT_BEGININIT:
        case WIFISTARTEVENT_LISTENERSERVICE_ENABLED:
        case WIFISTARTEVENT_SSID_RECEIVED:
        case WIFISTARTEVENT_PFILE_ENABLEBIT_RECEIVED:
        case WIFISTARTEVENT_PROFILELOADED:
        case WIFISTARTEVENT_TIMEZONE_RECEIVED:
        case WIFISTARTEVENT_DST_ACTIVE_RECEIVED:
        case WIFISTARTEVENT_DST_CHANGE_RECEIVED:
        case WIFISTARTEVENT_DST_VALID_RECEIVED:
        case WIFISTARTEVENT_REG_STATUS_RECEIVED:
        default:
            break;
        }
        break;

    /****************************************/
    /****************************************/
    case WIFISTARTSM_GET_TIMEZONE:
        switch (event)
        {
        /****************************************/
        /*           unhandled events           */
        /****************************************/
        case WIFISTARTEVENT_TIMEZONE_RECEIVED:
            dataTLV = (ayla_incomingTLV_t * )pData;

            timeZone = (uint16_t)DeserializeToInteger32Bits(dataTLV->Value, dataTLV->Length);

            dbTime.TimeZone = timeZone;

            DBTHRD_SetData(DBTYPE_TIMEZONE, (void*)&dbTime, INDEX_DONT_CARE, DBCHANGE_CLOUD);

            wifiInitCurrentState = WIFISTARTSM_GET_DST_ACTIVE;
            GetDSTActive();
            break;

        /****************************************/
        /*           unhandled events           */
        /****************************************/
        case WIFISTARTEVENT_BEGININIT:
        case WIFISTARTEVENT_LISTENERSERVICE_ENABLED:
        case WIFISTARTEVENT_SSID_RECEIVED:
        case WIFISTARTEVENT_PFILE_ENABLEBIT_RECEIVED:
        case WIFISTARTEVENT_PROFILELOADED:
        case WIFISTARTEVENT_UTCTIME_RECEIVED:
        case WIFISTARTEVENT_DST_ACTIVE_RECEIVED:
        case WIFISTARTEVENT_DST_CHANGE_RECEIVED:
        case WIFISTARTEVENT_DST_VALID_RECEIVED:
        case WIFISTARTEVENT_REG_STATUS_RECEIVED:
        default:
            break;
        }
        break;

    /****************************************/
    /****************************************/
    case WIFISTARTSM_GET_DST_ACTIVE:
        switch (event)
        {
        /****************************************/
        /****************************************/
        case WIFISTARTEVENT_DST_ACTIVE_RECEIVED:
            dataTLV = (ayla_incomingTLV_t * )pData;

            dbTime.IsDstActive = dataTLV->Value[0];
            DBTHRD_SetData(DBTYPE_DST_ACTIVE, (void*)&dbTime, INDEX_DONT_CARE, DBCHANGE_CLOUD);

            GetDSTChange();
            wifiInitCurrentState = WIFISTARTSM_GET_DST_CHANGE;
            break;

        /****************************************/
        /*           unhandled events           */
        /****************************************/
        case WIFISTARTEVENT_BEGININIT:
        case WIFISTARTEVENT_LISTENERSERVICE_ENABLED:
        case WIFISTARTEVENT_SSID_RECEIVED:
        case WIFISTARTEVENT_PFILE_ENABLEBIT_RECEIVED:
        case WIFISTARTEVENT_PROFILELOADED:
        case WIFISTARTEVENT_UTCTIME_RECEIVED:
        case WIFISTARTEVENT_TIMEZONE_RECEIVED:
        case WIFISTARTEVENT_DST_CHANGE_RECEIVED:
        case WIFISTARTEVENT_DST_VALID_RECEIVED:
        case WIFISTARTEVENT_REG_STATUS_RECEIVED:
        default:
            break;
        }

        break;

    /****************************************/
    /****************************************/
    case WIFISTARTSM_GET_DST_CHANGE:
        switch (event)
        {
        case WIFISTARTEVENT_DST_CHANGE_RECEIVED:
            dataTLV = (ayla_incomingTLV_t * )pData;

            dstChangeTimeFrom1970Time = (time_t)DeserializeToInteger32Bits(dataTLV->Value, dataTLV->Length);
            dbTime.DstChangeTime = dstChangeTimeFrom1970Time;
            DBTHRD_SetData(DBTYPE_DST_CHANGE, (void*)&dbTime, INDEX_DONT_CARE, DBCHANGE_CLOUD);

            GetDSTValid();
            wifiInitCurrentState = WIFISTARTSM_GET_DST_VALID;
            break;

        /****************************************/
        /*           unhandled events           */
        /****************************************/
        case WIFISTARTEVENT_BEGININIT:
        case WIFISTARTEVENT_LISTENERSERVICE_ENABLED:
        case WIFISTARTEVENT_SSID_RECEIVED:
        case WIFISTARTEVENT_PFILE_ENABLEBIT_RECEIVED:
        case WIFISTARTEVENT_PROFILELOADED:
        case WIFISTARTEVENT_UTCTIME_RECEIVED:
        case WIFISTARTEVENT_TIMEZONE_RECEIVED:
        case WIFISTARTEVENT_DST_ACTIVE_RECEIVED:
        case WIFISTARTEVENT_DST_VALID_RECEIVED:
        case WIFISTARTEVENT_REG_STATUS_RECEIVED:
        default:
            break;
        }

        break;

    /****************************************/
    /****************************************/
    case WIFISTARTSM_GET_DST_VALID:
        switch (event)
        {
        case WIFISTARTEVENT_DST_VALID_RECEIVED:
            dataTLV = (ayla_incomingTLV_t * )pData;


            dbTime.IsDstValid = dataTLV->Value[0];
            DBTHRD_SetData(DBTYPE_DST_VALID, (void*)&dbTime, INDEX_DONT_CARE, DBCHANGE_CLOUD);

            GetUtcTime();
            wifiInitCurrentState = WIFISTARTSM_GET_UTC_TIME;
            break;

        /****************************************/
        /*           unhandled events           */
        /****************************************/
        case WIFISTARTEVENT_BEGININIT:
        case WIFISTARTEVENT_LISTENERSERVICE_ENABLED:
        case WIFISTARTEVENT_SSID_RECEIVED:
        case WIFISTARTEVENT_PFILE_ENABLEBIT_RECEIVED:
        case WIFISTARTEVENT_PROFILELOADED:
        case WIFISTARTEVENT_UTCTIME_RECEIVED:
        case WIFISTARTEVENT_TIMEZONE_RECEIVED:
        case WIFISTARTEVENT_DST_ACTIVE_RECEIVED:
        case WIFISTARTEVENT_DST_CHANGE_RECEIVED:
        case WIFISTARTEVENT_REG_STATUS_RECEIVED:
        default:
            break;
        }

        break;

    /****************************************/
    /****************************************/
    case WIFISTARTSM_GET_REG_STATUS:
        switch (event)
        {
        /****************************************/
        /****************************************/
        case WIFISTARTEVENT_REG_STATUS_RECEIVED:
            dataTLV = (ayla_incomingTLV_t * )pData;

            DBTHRD_GetData(DBTYPE_CLOUDCONNECTIVITY, (void *)&dbCloudConnectivity, INDEX_DONT_CARE);
            dbCloudConnectivity.isRegistered = dataTLV->Value[0];
            dbCloudConnectivity.isConnectedToCloud = 1;

            DBTHRD_SetData(DBTYPE_CLOUDCONNECTIVITY, (void*)&dbCloudConnectivity, INDEX_DONT_CARE, DBCHANGE_REGISTER);

            wifiInitCurrentState = WIFISTARTSM_SYSTEMRUNNING;
            SendResetReason();
            break;


        /****************************************/
        /*           unhandled events           */
        /****************************************/
        case WIFISTARTEVENT_BEGININIT:
        case WIFISTARTEVENT_LISTENERSERVICE_ENABLED:
        case WIFISTARTEVENT_SSID_RECEIVED:
        case WIFISTARTEVENT_PFILE_ENABLEBIT_RECEIVED:
        case WIFISTARTEVENT_PROFILELOADED:
        case WIFISTARTEVENT_UTCTIME_RECEIVED:
        case WIFISTARTEVENT_TIMEZONE_RECEIVED:
        case WIFISTARTEVENT_DST_ACTIVE_RECEIVED:
        case WIFISTARTEVENT_DST_CHANGE_RECEIVED:
        case WIFISTARTEVENT_DST_VALID_RECEIVED:
        default:
            break;
        }

        break;

    /****************************************/
    /****************************************/
    case WIFISTARTSM_SYSTEMRUNNING:
        switch (event)
        {
            case WIFISTARTEVENT_TIMEZONE_RECEIVED:
                dataTLV = (ayla_incomingTLV_t * )pData;

                timeZone = (uint16_t)DeserializeToInteger32Bits(dataTLV->Value, dataTLV->Length);
                dbTime.TimeZone = timeZone;

                DBTHRD_SetData(DBTYPE_TIMEZONE, (void*)&dbTime, INDEX_DONT_CARE, DBCHANGE_CLOUD);

                GetDSTActive();
                break;
            case WIFISTARTEVENT_DST_ACTIVE_RECEIVED:
                dataTLV = (ayla_incomingTLV_t * )pData;

                dbTime.IsDstActive = dataTLV->Value[0];
                DBTHRD_SetData(DBTYPE_DST_ACTIVE, (void*)&dbTime, INDEX_DONT_CARE, DBCHANGE_CLOUD);

                GetDSTChange();
                break;
            case WIFISTARTEVENT_DST_CHANGE_RECEIVED:
                dataTLV = (ayla_incomingTLV_t * )pData;

                dstChangeTimeFrom1970Time = (time_t)DeserializeToInteger32Bits(dataTLV->Value, dataTLV->Length);

                dbTime.DstChangeTime = dstChangeTimeFrom1970Time;
                DBTHRD_SetData(DBTYPE_DST_CHANGE, (void*)&dbTime, INDEX_DONT_CARE, DBCHANGE_CLOUD);

                GetDSTValid();
                break;
            case WIFISTARTEVENT_DST_VALID_RECEIVED:
                dataTLV = (ayla_incomingTLV_t * )pData;

                dbTime.IsDstValid = dataTLV->Value[0];
                DBTHRD_SetData(DBTYPE_DST_VALID, (void*)&dbTime, INDEX_DONT_CARE, DBCHANGE_CLOUD);

                GetUtcTime();
                break;
            case WIFISTARTEVENT_UTCTIME_RECEIVED:
                dataTLV = (ayla_incomingTLV_t * )pData;

                currentFrom1970Time = (time_t)DeserializeToInteger32Bits(dataTLV->Value, dataTLV->Length);

                DBTHRD_GetData(DBTYPE_LOCALTIME, (void*)&dbTime, NULL);

                applyDst = 0;
                if (dbTime.IsDstValid)
                {
                    if (dbTime.IsDstActive)
                    {
                        if (currentFrom1970Time < dbTime.DstChangeTime)
                        {
                            applyDst = 1;
                        }
                    }
                    else
                    {
                        if (currentFrom1970Time > dbTime.DstChangeTime)
                        {
                            applyDst = 1;
                        }
                    }
                }
                else
                {
                    ;
                }

                currentFrom1970Time -= ((dbTime.TimeZone*60) - (applyDst*60*60));

                currentTime = gmtime(&currentFrom1970Time);


                /* Must initialize the calendar date before the time because    */
                /*  the setUTCTIME will recalculate the day of year from the    */
                /*  time zone.                                                  */
                dbDate.Year = currentTime->tm_year + 1900;
                dbDate.Month = currentTime->tm_mon + 1;
                dbDate.Day = currentTime->tm_mday;
                dbDate.DayOfWeek = currentTime->tm_wday;
                dbDate.DayOfYear = currentTime->tm_yday;
                DBTHRD_SetData(DBTYPE_DATE, (void*)&dbDate, INDEX_DONT_CARE, DBCHANGE_CLOUD);

                dbTime.Hours = currentTime->tm_hour;
                dbTime.Minutes = currentTime->tm_min;
                dbTime.Seconds = currentTime->tm_sec;

                DBTHRD_SetData(DBTYPE_LOCALTIME, (void*)&dbTime, INDEX_DONT_CARE, DBCHANGE_CLOUD);

//                DBTHRD_GetData(DBTYPE_DEBUG_LEVEL, (void*)&debugLevel, NULL);
//                if (debugLevel > 0)
                {
                    char diag[100];

                    sprintf(diag, "TimeUpdate: tz=%d, da=%d, dct=%d, dv=%d, ad=%d, cF1970T=%d, h=%d, m=%d, sh=%d, sm=%d\n",
                    dbTime.TimeZone,
                    dbTime.IsDstActive,
                    dbTime.DstChangeTime,
                    dbTime.IsDstValid,
                    applyDst,
                    currentFrom1970Time,
                    dbTime.Hours,
                    dbTime.Minutes,
                    SystemTime.Hours,
                    SystemTime.Minutes);

                    DBTHRD_SetData(DBTYPE_DIAGNOSTIC,(void*)&diag, INDEX_DONT_CARE, DBCHANGE_LOCAL);
                }

                break;
            default:
                break;
        }
        break;
    /****************************************/
    /****************************************/
    default :
        switch (event)
        {
        /****************************************/
        /*           unhandled events           */
        /****************************************/
        case WIFISTARTEVENT_PFILE_ENABLEBIT_RECEIVED:
        case WIFISTARTEVENT_SSID_RECEIVED:
        case WIFISTARTEVENT_BEGININIT:
        case WIFISTARTEVENT_PROFILELOADED:
        default:
            break;
        }
        break;

    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/06
*******************************************************************************/
#pragma optimize = none
void HAL_ParseReceivedPacket(uint8_t * buffer, uint16_t packetLength)
{
    ayla_Packet_t * receivedPacket = (ayla_Packet_t*)buffer;

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Packet Received; Protocol : 0x%x , Opcode : 0x%x, L:%d",receivedPacket->ProtocolID,receivedPacket->OpCode, packetLength);
#endif

    packetLength -= AYLA_HEADER_FOOTER_OFFSET;

    osDelay(1);
    if (buffer !=  NULL)
    {
        switch (receivedPacket->ProtocolID)
        {
        case AYLAPROTOCOL_CONTROLOPS:
            Ayla_HandleControlOperation((ayla_Packet_t *)receivedPacket, packetLength);
            break;

        case AYLAPROTOCOL_DATAOPS:
            Ayla_HandleDataOperation ((ayla_Packet_t *)receivedPacket, packetLength);
            break;

        default :
            break;
        }
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/08/24
*******************************************************************************/
void HAL_WiFi_SPI_Init(uint8_t crcEnable, uint8_t receiveOnlyMode)
{
    __HAL_RCC_SPI4_CLK_ENABLE();

    hWiFiSpi.Instance = SPI4;
    hWiFiSpi.Init.Mode = SPI_MODE_MASTER;

    if (receiveOnlyMode != 0)
    {
        hWiFiSpi.Init.Direction = SPI_DIRECTION_2LINES_RXONLY;
    }
    else
    {
        hWiFiSpi.Init.Direction = SPI_DIRECTION_2LINES;
    }

    hWiFiSpi.Init.DataSize = SPI_DATASIZE_8BIT;
    hWiFiSpi.Init.CLKPolarity = SPI_POLARITY_LOW;
    hWiFiSpi.Init.CLKPhase = SPI_PHASE_2EDGE;
    hWiFiSpi.Init.NSS = SPI_NSS_SOFT;
    hWiFiSpi.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;       //with peripheral clock=90MHz, SCLK = 5.625MHz
    hWiFiSpi.Init.FirstBit = SPI_FIRSTBIT_MSB;
    hWiFiSpi.Init.TIMode = SPI_TIMODE_DISABLE;

    if (crcEnable != 0)
    {
        hWiFiSpi.Init.CRCCalculation = SPI_CRCCALCULATION_ENABLE;
        hWiFiSpi.Init.CRCPolynomial = 7;
    }
    else
    {
        hWiFiSpi.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
        hWiFiSpi.Init.CRCPolynomial = 7;
    }
    HAL_SPI_Init(&hWiFiSpi);

    hWifiSpi_TxDma.Instance = DMA2_Stream1;
    hWifiSpi_TxDma.Init.Channel = DMA_CHANNEL_4;
    hWifiSpi_TxDma.Init.Direction = DMA_MEMORY_TO_PERIPH;
    hWifiSpi_TxDma.Init.PeriphInc = DMA_PINC_DISABLE;
    hWifiSpi_TxDma.Init.MemInc = DMA_MINC_ENABLE;
    hWifiSpi_TxDma.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hWifiSpi_TxDma.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    hWifiSpi_TxDma.Init.Mode = DMA_NORMAL;
    hWifiSpi_TxDma.Init.Priority = DMA_PRIORITY_VERY_HIGH;
    hWifiSpi_TxDma.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
    HAL_DMA_Init(&hWifiSpi_TxDma);

    __HAL_LINKDMA(&hWiFiSpi,hdmatx,hWifiSpi_TxDma);

    hWifiSpi_RxDma.Instance = DMA2_Stream0;
    hWifiSpi_RxDma.Init.Channel = DMA_CHANNEL_4;
    hWifiSpi_RxDma.Init.Direction = DMA_PERIPH_TO_MEMORY;
    hWifiSpi_RxDma.Init.PeriphInc = DMA_PINC_DISABLE;
    hWifiSpi_RxDma.Init.MemInc = DMA_MINC_ENABLE;
    hWifiSpi_RxDma.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hWifiSpi_RxDma.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    hWifiSpi_RxDma.Init.Mode = DMA_NORMAL;
    hWifiSpi_RxDma.Init.Priority = DMA_PRIORITY_VERY_HIGH;
    hWifiSpi_RxDma.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
    HAL_DMA_Init(&hWifiSpi_RxDma);

    __HAL_LINKDMA(&hWiFiSpi,hdmarx,hWifiSpi_RxDma);
}



/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/15
*******************************************************************************/
void HAL_JoinWiFiNetworks (wifi_joinNetworks_t * networkInfo)
{
    WifiNetworksJoin_StateMachine(WIFIJOINEVENT_JOINNETWORK, (void*)networkInfo);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/19
*******************************************************************************/
void HAL_GetCurrentNetwork (void (*currentNetworkcB)(void))
{
    GetActiveProfile(currentNetworkcB);
}

/*******************************************************************************
* @brief Get WiFi RSSI
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/02/13
*******************************************************************************/
void HAL_GetSignalStrength (void)
{
    GetSignalStrength();
}

/*******************************************************************************
* @brief Get WiFi version
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/12/05
*******************************************************************************/
void HAL_GetWiFiModuleVersion (void)
{
    GetWiFiModuleVersion();
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/28
*******************************************************************************/
void HAL_GetRegistrationToken (void (*regTokencB)(RegisterStatus_t, uint8_t*))
{
    GetRegistrationToken(regTokencB);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/28
*******************************************************************************/
void HAL_GenerateNewRegistrationToken (void)
{
    GenerateNewRegistrationToken();
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/18
*******************************************************************************/
void HAL_GetRegistrationStatus (void (*regStatuscB)(RegisterStatus_t, uint8_t*))
{
    GetRegistrationStatus(regStatuscB);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/27
*******************************************************************************/
void HAL_GetAllProperties(void)
{
    GetAllProperties();
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/24
*******************************************************************************/
void HAL_GetSpecificProperty(WIFITYPE_t wifiType)
{
    if (wifiType < MAX_PROPERTY_TYPE)
    {
        GetSpecificProperty(wifiType);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/25
*******************************************************************************/
void HAL_UpdateTime(void)
{
    GetTimeZone();
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/12/12
*******************************************************************************/
void HAL_GetSecurity(uint8_t scanResultNumber)
{
    GetSecurity(scanResultNumber);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/12/12
*******************************************************************************/
void HAL_GetSSID(uint8_t scanResultNumber)
{
    GetSSID(scanResultNumber);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/12/12
*******************************************************************************/
void HAL_PollWiFiScanReady(void)
{
    PollWiFiScanReady(NULL);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/12/12
*******************************************************************************/
void HAL_SnapShotScanResult(void)
{
    SnapShotScanResult();
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/08/24
*******************************************************************************/
void HAL_ReadWiFiData (uint8_t * dataBuffer, uint8_t length)
{

}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/08/24
*******************************************************************************/
void HAL_ResetCloudInterface (void)
{
    Ayla_ControlOpsInit();
    HAL_ResetWiFiInitStateMachine();
    wifiMsgQHead = NULL;
    wifiMsgQTail = NULL;

    HAL_RESET_WIFI();
}

/*******************************************************************************
* @brief  Reset the WiFi module to factory default
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/02/14
*******************************************************************************/
void HAL_FactoryReset(void)
{
    SendFactoryReset();
    Ayla_ControlOpsInit();
    HAL_ResetWiFiInitStateMachine();
}

/*******************************************************************************
* @brief  Reset the WiFi initialization state machine
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/02/20
*******************************************************************************/
void HAL_ResetWiFiInitStateMachine(void)
{
    wifiInitCurrentState = WIFISTARTSM_INIT;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/08/24
*******************************************************************************/
void InitCloudInterface(void)
{
#ifdef WIFI_TEST_BENCH
    extern void Init_TestBench_Communication_Peripheral();
    
    Init_TestBench_Communication_Peripheral();
#else
    GPIO_Init();
    HAL_WiFi_SPI_Init(SPI_WITHOUT_CRC, SPI_TXRX_MODE);
    DMA_Init();
#endif

    HAL_ResetCloudInterface();
    HAL_SetWiFiState(WIFI_IDLE);

    wifi_tx_msg_cnt = xSemaphoreCreateCounting(WIFI_TX_MSG_MAX,WIFI_TX_MSG_MAX);
    configASSERT(wifi_tx_msg_cnt);

//    wifi_q_available = xSemaphoreCreateCounting(1,1);
//    configASSERT(wifi_q_available);

    xSem_DmaRx = xSemaphoreCreateBinary();
    configASSERT(xSem_DmaRx);

    xSem_DmaTx = xSemaphoreCreateBinary();
    configASSERT(xSem_DmaTx);

}

/*******************************************************************************
* @brief  Send a property in the cloud
* @inputs propType: Property WiFi type (enum)
* @inputs index: Index of the property instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/10/03
*******************************************************************************/
void HAL_SendProperty(WIFITYPE_t propType, uint8_t* index, dbChangeSource_t* source)
{
    if (propType < MAX_PROPERTY_TYPE)
    {
        if (propTable[propType].sendFunction != NULL)
        {
            propertiesToSend[propType].sendRequest = 1;
            propertiesToSend[propType].index = *index;
            propertiesToSend[propType].source = *source;
            //propTable[propType].sendFunction((void*)index, *source);
        }
    }
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/08/24
*******************************************************************************/
int8_t HAL_isWiFiTxComplete(void)
{
    uint8_t rcv_status;
    uint8_t err_status;

    err_status = BSP_SendSingleByteCommand(AYLA_COMMAND_POLL, &rcv_status);

    if (err_status != 0)
    {
        return 2;
    }
    else if (rcv_status & AYLA_STATUS_ERROR)
    {
        return -1;
    }
    else if (rcv_status & (AYLA_STATUS_INVALID|AYLA_STATUS_BUSY))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}


/*******************************************************************************
* @brief  allocate data memory from the RTOS heap.  Altough it serve an important
*       system task, the function is not blocking forever.  It is up to the calling
*       services, case by case, to address the criticality of the task.
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void * allocate_wifi_msg_memory (void)
{
    void * mem_ptr = NULL;
    uint8_t retry = 3;

    while ((NULL == mem_ptr) && (retry-- > 0))
    {
        mem_ptr = pvPortMalloc(sizeof(wifiMsgQ_t));

        if (NULL == mem_ptr)
        {
            osDelay(10);
        }
    }
    return mem_ptr;
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/16
*******************************************************************************/
#pragma optimize=none
uint8_t HAL_QWiFiMsg(aylaDataPoint_t *newDataPoint)
{
    wifiMsgQ_t *newWifiMsg;
    uint8_t status = 1;

//    if (xSemaphoreTake(wifi_q_available,pdMS_TO_TICKS(5000)) == pdTRUE)
//    {
        while (NULL != newDataPoint)      //loop on datapoint for ong string items
        {
            if (pdTRUE == xSemaphoreTake(wifi_tx_msg_cnt,pdMS_TO_TICKS(5000) ))  //each message takes one resource
            {

                if (NULL == wifiMsgQHead)
                {
                    /* if wifiMsgQHead is empty, it is safe to use the static buffer */
                    newWifiMsg = &static_wifi_buffer;
                }
                else
                {
                    newWifiMsg = allocate_wifi_msg_memory();
                }

                if (NULL != newWifiMsg)
                {
                    uint16_t size = (uint16_t)newDataPoint->headerFooterSize;
                    aylaTLV_t * tlvs;
                    uint16_t payloadIdx = 0;

                    newWifiMsg->next = NULL;

                    newWifiMsg->wifiMsg.packet.ProtocolID = newDataPoint->header.protocolId;
                    newWifiMsg->wifiMsg.packet.OpCode = newDataPoint->header.opCode;
                    newWifiMsg->wifiMsg.packet.RequestID_LSB = newDataPoint->RequestID_LSB;
                    newWifiMsg->wifiMsg.packet.RequestID_MSB = newDataPoint->RequestID_MSB;


                /****************************************************/
                /*  parse all TLVs contained in the dataPoint       */
                /****************************************************/
                    tlvs = newDataPoint->firstTLV;
                    while (NULL != tlvs)
                    {
                        uint16_t offset = 0;

                    /************************************************************/
                    /*  if TLV is TEXT (UTF-8) and length is > 255, the TLV     */
                    /*  value needs to be fragmented in multiple messages.      */
                    /*  The first message will have the length TLV and the first*/
                    /*  255 bytes of the TEXT TLV.                              */
                    /*  All subsequent message will have the OFFSET TLV with    */
                    /*  a chunk of maximum 255 bytes from the TEXT TLV.         */
                    /*  The last message (which has less than 255 bytes of the  */
                    /*  TEXT TLV will end with the EOF TLV.                     */
                    /************************************************************/
                        if ((TLVTYPE_TEXT == tlvs->type) && (tlvs->length >= 255))
                        {
                            if (0 == tlvs->isLongString)
                            {
                                tlvs->isLongString = 1;
                                newWifiMsg->wifiMsg.packet.Payload[payloadIdx++] = TLVTYPE_LENGTH;
                                newWifiMsg->wifiMsg.packet.Payload[payloadIdx++] = 2;
                                newWifiMsg->wifiMsg.packet.Payload[payloadIdx++] = ((uint8_t *)&tlvs->length)[1];
                                newWifiMsg->wifiMsg.packet.Payload[payloadIdx++] = ((uint8_t *)&tlvs->length)[0];

                                tlvs->longStringTotalLength = tlvs->length;
                                tlvs->length = 255;
                                tlvs->longStringOffset = tlvs->length;
                                size+=4;
                            }
                            else
                            {
                                newWifiMsg->wifiMsg.packet.Payload[payloadIdx++] = TLVTYPE_OFFSET;
                                newWifiMsg->wifiMsg.packet.Payload[payloadIdx++] = 2;
                                newWifiMsg->wifiMsg.packet.Payload[payloadIdx++] = ((uint8_t*)&tlvs->longStringOffset)[1];
                                newWifiMsg->wifiMsg.packet.Payload[payloadIdx++] = ((uint8_t*)&tlvs->longStringOffset)[0];

                                tlvs->length = tlvs->longStringTotalLength - tlvs->longStringOffset;
                                if (tlvs->length > 255)
                                {
                                    tlvs->length = 255;
                                }
                                offset = tlvs->longStringOffset;
                                tlvs->longStringOffset += tlvs->length;
                                size+=4;
                            }
                        }

                        newWifiMsg->wifiMsg.packet.Payload[payloadIdx++] = tlvs->type;
                        newWifiMsg->wifiMsg.packet.Payload[payloadIdx++] = (uint8_t)tlvs->length;

                        memcpy (&newWifiMsg->wifiMsg.packet.Payload[payloadIdx], &tlvs->value[offset],tlvs->length);

                        payloadIdx += tlvs->length;
                        size += AYLA_TLV_BASE_LENGTH + tlvs->length;

                        if (0 != tlvs->isLongString)
                        {
                            if (tlvs->length < 255)
                            {
                            /****************************************************/
                            /*  if the parsing of the long string has ended,    */
                            /*  it is allowed to continue parsing other TLV in  */
                            /*  the message (if ever it happens)                */
                            /****************************************************/
                                newWifiMsg->wifiMsg.packet.Payload[payloadIdx++] = TLVTYPE_EOF;
                                newWifiMsg->wifiMsg.packet.Payload[payloadIdx++] = 0;
                                size += 2;                  //add TLV_EOF and TLV length to the total size
                                tlvs->isLongString = 0;
                            }
                            else
                            {
                            /****************************************************/
                            /*  if the parsing of the long string is still      */
                            /*  ongoing, the TLV parsing loop must be exited.   */
                            /****************************************************/
                                break;
                            }
                        }
                        tlvs = tlvs->next;
                    }

                    newWifiMsg->wifiMsg.packetSize = size;

                    if (NULL == wifiMsgQHead)
                    {
                        wifiMsgQHead = newWifiMsg;
                        wifiMsgQTail = wifiMsgQHead;
                    }
                    else
                    {
                        wifiMsgQTail->next = newWifiMsg;
                        wifiMsgQTail = wifiMsgQTail->next;
                    }

                 /***************************************************/
                 /* if the last TLV parsed is a long string, it must*/
                 /* loop back to the same datapoint to parse the    */
                 /* whole message.                                  */
                 /***************************************************/
                    if ((NULL == tlvs) ||
                        (NULL != tlvs && (0 == tlvs->isLongString)))
                    {
                        destroy_datapoint(&newDataPoint);
                        status = 0;
                    }
                }
                else
                {
                    vTracePrint(wifiEventChannel, "Heap full : msg not queued");
                    destroy_datapoint(&newDataPoint);
                    xSemaphoreGive(wifi_tx_msg_cnt);
                    break;
                }
            }
            else
            {
                vTracePrint(wifiEventChannel, "something went wrong; deleting msg");
                destroy_datapoint(&newDataPoint);
            }
        }  //while (NULL != newDataPoint)

//        xSemaphoreGive(wifi_q_available);
//    }
//    else
//    {
//        vTracePrint(wifiEventChannel, "semaphore not taken, destroy datapoint");
//        destroy_datapoint(&newDataPoint);
//    }

    return status;
}

/*******************************************************************************
* @brief Send the queued messages if ADS is not busy
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/01/12
*******************************************************************************/
int8_t HAL_SendCommand(void)
{
    ayla_Packet_t * packetToSend;
    uint8_t status = 0;

    uint8_t rcv_status;
    uint8_t err_status;

    err_status = BSP_SendSingleByteCommand(AYLA_COMMAND_POLL, &rcv_status);

    packetToSend = &wifiMsgQHead->wifiMsg.packet;

    if (err_status != 0)
    {
        status = 2;
    }
    else
    {
        if (0 == (rcv_status & (AYLA_STATUS_INVALID|AYLA_STATUS_BUSY)))
        {
            if (AYLAPROTOCOL_CONTROLOPS == packetToSend->ProtocolID)
            {
                status = BSP_SendPacketToWifi((uint8_t *)packetToSend, wifiMsgQHead->wifiMsg.packetSize);
            }
            else if (AYLAPROTOCOL_DATAOPS == packetToSend->ProtocolID)
            {
                {
                    if (0 == (rcv_status & AYLA_STATUS_ADSBUSY))
                    {
                        status = BSP_SendPacketToWifi((uint8_t *)packetToSend, wifiMsgQHead->wifiMsg.packetSize);
                    }
                    else
                    {
                        status = -1;
                    }
                }
            }
            else
            {
            }
        }
        else
        {
            status = -1;
        }
    }
    return status;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/17
*******************************************************************************/
void HAL_FreeWiFiQ(void)
{
    wifiMsgQ_t * msgToFree;

    msgToFree = wifiMsgQHead;

    wifiMsgQHead = wifiMsgQHead->next;

    if (msgToFree != &static_wifi_buffer)
    {
        vPortFree(msgToFree);
    }
    xSemaphoreGive(wifi_tx_msg_cnt);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/17
*******************************************************************************/
uint8_t HAL_isWifiMsgToTx(void)
{

    if (NULL != wifiMsgQHead)
    {
        return 1;
    }
    return 0;
}

/*******************************************************************************
* @brief Change the WiFi State
* @inputs state: new state for WiFi State Machine
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/01/12
*******************************************************************************/
void HAL_SetWiFiState(wifi_state_machine_t state)
{
    WiFiSerialState = state;
}

/*******************************************************************************
* @brief Check if a message is pending from the WiFi Module
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/01/12
*******************************************************************************/
uint8_t HAL_IsMessagePending(void)
{
    uint8_t rcv_status;
    uint8_t err_status;

    err_status = BSP_SendSingleByteCommand(AYLA_COMMAND_POLL, &rcv_status);

    if (err_status != 0)
    {
        return 2;
    }
    else if ((rcv_status &
        (AYLA_STATUS_INVALID|AYLA_STATUS_BUSY|AYLA_STATUS_ATTENTION)) ==
        AYLA_STATUS_ATTENTION)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

/*******************************************************************************
*
*                           INTERRUPTS CALLBACK
*
*******************************************************************************/


/*******************************************************************************
  * @brief Tx Transfer completed callback.
  * @param  hspi: pointer to a SPI_HandleTypeDef structure that contains
  *               the configuration information for SPI module.
  * @retval None
*******************************************************************************/
void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef *hspi)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    if (hspi == &hWiFiSpi)
    {

    /* Unblock the handling task so the task can perform any processing necessitated
        by the interrupt.  xHandlingTask is the task's handle, which was obtained
        when the task was created.  The handling task's notification value
        is bitwise ORed with the interrupt status - ensuring bits that are already
        set are not overwritten. */
        xSemaphoreGiveFromISR(xSem_DmaTx,&xHigherPriorityTaskWoken);
    /* Force a context switch if xHigherPriorityTaskWoken is now set to pdTRUE.
        The macro used to do this is dependent on the port and may be called
        portEND_SWITCHING_ISR. */
         portYIELD_FROM_ISR( xHigherPriorityTaskWoken );    //Force a context switch
    }

}

/*******************************************************************************
  * @brief Rx Transfer completed callback.
  * @param  hspi: pointer to a SPI_HandleTypeDef structure that contains
  *               the configuration information for SPI module.
  * @retval None
*******************************************************************************/
void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef *hspi)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    if (hspi == &hWiFiSpi)
    {

    /* Unblock the handling task so the task can perform any processing necessitated
        by the interrupt.  xHandlingTask is the task's handle, which was obtained
        when the task was created.  The handling task's notification value
        is bitwise ORed with the interrupt status - ensuring bits that are already
        set are not overwritten. */
        xSemaphoreGiveFromISR(xSem_DmaRx,&xHigherPriorityTaskWoken);

    /* Force a context switch if xHigherPriorityTaskWoken is now set to pdTRUE.
        The macro used to do this is dependent on the port and may be called
        portEND_SWITCHING_ISR. */
         portYIELD_FROM_ISR( xHigherPriorityTaskWoken );    //Force a context switch
    }

}

/*******************************************************************************
  * @brief SPI error callback.
  * @param  hspi: pointer to a SPI_HandleTypeDef structure that contains
  *               the configuration information for SPI module.
  * @retval None
*******************************************************************************/
void HAL_SPI_ErrorCallback(SPI_HandleTypeDef *hspi)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    if (hspi == &hWiFiSpi)
    {

    /* Unblock the handling task so the task can perform any processing necessitated
        by the interrupt.  xHandlingTask is the task's handle, which was obtained
        when the task was created.  The handling task's notification value
        is bitwise ORed with the interrupt status - ensuring bits that are already
        set are not overwritten. */
        xSemaphoreGiveFromISR(xSem_DmaRx,&xHigherPriorityTaskWoken);

    /* Force a context switch if xHigherPriorityTaskWoken is now set to pdTRUE.
        The macro used to do this is dependent on the port and may be called
        portEND_SWITCHING_ISR. */
         portYIELD_FROM_ISR( xHigherPriorityTaskWoken );    //Force a context switch
    }

}

/*******************************************************************************
* @brief  Set the OTA Pending alert and start the OTA reset timer
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/07/12
*******************************************************************************/
void HAL_SetOTAPendingAlert(void)
{
    uint8_t alert;

    alert = DB_SET_ALERT | DBALERT_OTA_PENDING;
    DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);

    OTA_Reset_Minutes = MAX_OTA_RESET_DELAY;
    if (NULL == OTA_Reset_Timer)
    {
        OTA_Reset_Timer = xTimerCreate ("OTA Reset Timer",
                                        pdMS_TO_TICKS(OTA_ONE_MINUTE_DELAY),
                                        pdFALSE,     //one-shot
                                        (void*)0,
                                        OnOTAResetTimeout);
    }
    if (NULL != OTA_Reset_Timer)
    {
        xTimerStart(OTA_Reset_Timer,0);
    }
}

/*******************************************************************************
* @brief Callback when OTA Reset Timer expires
* @inputs xTimer: timer instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/07/12
*******************************************************************************/
static void OnOTAResetTimeout( TimerHandle_t xTimer )
{
    if (OTA_Reset_Minutes > 0)
    {
        OTA_Reset_Minutes--;
    }
    if (OTA_Reset_Minutes == 0)
    {
        vTracePrint(otaChunksChannel, "OTA SystemReset");
        NVIC_SystemReset();
    }
    else
    {
        xTimerStart(OTA_Reset_Timer,0);
    }
}

/*******************************************************************************
* @brief Returns the remaining OTA Reset Timer delay
* @inputs xTimer: timer instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/07/12
*******************************************************************************/
uint16_t HAL_GetOTAResetTimer(void)
{
    return (uint16_t)(OTA_Reset_Minutes);
}

/*******************************************************************************
* @brief Cancel the OTA reset timer and reset now
* @inputs xTimer: timer instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/07/12
*******************************************************************************/
void ApplyOTANow(void)
{
    OTA_Reset_Minutes = 0;
    OnOTAResetTimeout(OTA_Reset_Timer);
}

/*******************************************************************************
* @brief Send the Reset Reason in the diagnostic property
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2018/09/13
*******************************************************************************/
static void SendResetReason(void)
{
    uint8_t reset_reason = GetResetCause();
    uint8_t disconnect_reason = GetDisconnectReason();
    ClearResetCause();
    SetDisconnectReason(0);
    if (reset_reason != 0)
    {
        char reset_reason_str[4] = {0,0,0,0};
        sprintf(reset_reason_str, "0x%x", reset_reason);
        char diag[64] = "ResetReason:";
        strncat(diag, (char const*)&reset_reason_str, 4);
        DBTHRD_SetData(DBTYPE_DIAGNOSTIC,(void*)&diag, INDEX_DONT_CARE, DBCHANGE_LOCAL);
        DBTHRD_SubscribeToNotification (DBTYPE_MINUTE_CHANGE,
                                   PeriodicDiagnosticCheck,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
    }
    else
    {
        char disconnect_reason_str[4] = {0,0,0,0};
        sprintf(disconnect_reason_str, "0x%x", disconnect_reason);
        char diag[64] = "DisconnectReason:";
        strncat(diag, (char const*)&disconnect_reason_str, 4);
        DBTHRD_SetData(DBTYPE_DIAGNOSTIC,(void*)&diag, INDEX_DONT_CARE, DBCHANGE_LOCAL);
    }
}

/*******************************************************************************
* @brief Periodically check if there is a diagnostic report to send
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2018/09/13
*******************************************************************************/
DB_NOTIFICATION(PeriodicDiagnosticCheck)
{
    uint8_t* chksumPointer;
    uint8_t chksum;
    uint8_t i;
    uint8_t chksum_in_string;

    if (strlen(ExceptionDiag) != 0)
    {
        chksumPointer = (uint8_t *)ExceptionDiag;

        chksum = 0xF0;
        for (i = 0;i < ((strlen(ExceptionDiag) - 5)); i++)
        {
            if (i >= 100)
            {
                break;
            }
            chksum += chksumPointer[i];
        }
        if (chksumPointer[i+3] <= '9')
        {
            chksum_in_string = (chksumPointer[i+3] - 0x30) << 4;
        }
        else
        {
            chksum_in_string = (chksumPointer[i+3] - 0x57) << 4;
        }
        if (chksumPointer[i+4] <= '9')
        {
            chksum_in_string |= chksumPointer[i+4] - 0x30;
        }
        else
        {
            chksum_in_string |= chksumPointer[i+4] - 0x57;
        }
        if (chksum == chksum_in_string)
        {
            DBTHRD_SetData(DBTYPE_DIAGNOSTIC,(void*)ExceptionDiag, INDEX_DONT_CARE, DBCHANGE_LOCAL);
        }

        for (i = 0; i < 100; i++)
        {
            ExceptionDiag[i] = 0;
        }
    }
}

/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
