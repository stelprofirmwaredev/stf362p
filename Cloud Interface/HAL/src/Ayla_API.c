/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    Ayla_API.c
* @date    2017/04/26
* @authors J-F. Simard
* @brief   
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include ".\HAL\inc\Ayla_API.h"
#include ".\HAL\inc\AylaDefinitions.h"

#include <stdint.h>
#include <string.h>
#include <stdarg.h>

#include "cmsis_os.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
const AylaHeader_t OpCodeTable [MAX_OPCODEDEFINITION] = 
{
    [DATAOPCODE_SENDTLVVERSION]               = 
    {
        .opCode = 0x01,
        .protocolId = AYLAPROTOCOL_DATAOPS,
    },
    [DATAOPCODE_PROPVALUEREQUEST]             = 
    {
        .opCode = 0x02,
        .protocolId = AYLAPROTOCOL_DATAOPS,
    },
    [DATAOPCODE_RXTLV]                        = 
    {
        .opCode = 0x03,
        .protocolId = AYLAPROTOCOL_DATAOPS,
    },
    [DATAOPCODE_NAK]                          = 
    {
        .opCode = 0x05,
        .protocolId = AYLAPROTOCOL_DATAOPS,
    },
    [DATAOPCODE_REQUESTPROP]                  = 
    {
        .opCode = 0x06,
        .protocolId = AYLAPROTOCOL_DATAOPS,
    },
    [DATAOPCODE_REQUESTPROP_RESP]             = 
    {
        .opCode = 0x07,
        .protocolId = AYLAPROTOCOL_DATAOPS,
    },
    [DATAOPCODE_REQUESTNEXTPROP]              = 
    {
        .opCode = 0x08,
        .protocolId = AYLAPROTOCOL_DATAOPS,
    },
    [DATAOPCODE_SENDTLVS]                     = 
    {
        .opCode = 0x09,
        .protocolId = AYLAPROTOCOL_DATAOPS,
    },
    [DATAOPCODE_FILEDATAPOINT_REQ]            = 
    {
        .opCode = 0x0a,
        .protocolId = AYLAPROTOCOL_DATAOPS,
    },
    [DATAOPCODE_FILEDATAPOINT_RESP]           =
    {
        .opCode = 0x0b,
        .protocolId = AYLAPROTOCOL_DATAOPS,
    },
    [DATAOPCODE_FILEDATAPOINT_CREATE]         = 
    {
        .opCode = 0x0c,
        .protocolId = AYLAPROTOCOL_DATAOPS,
    },
    [DATAOPCODE_FILEDATAPOINT_FETCHED]        = 
    {
        .opCode = 0x0d,
        .protocolId = AYLAPROTOCOL_DATAOPS,
    },
    [DATAOPCODE_FILEDATAPOINT_STOP]           = 
    {
        .opCode = 0x0e,
        .protocolId = AYLAPROTOCOL_DATAOPS,
    },
    [DATAOPCODE_FILEDATAPOINT_SEND]           = 
    {
        .opCode = 0x0f,
        .protocolId = AYLAPROTOCOL_DATAOPS,
    },
    [DATAOPCODE_CONNECTSTATUS]                = 
    {
        .opCode = 0x11,
        .protocolId = AYLAPROTOCOL_DATAOPS,
    },
    [DATAOPCODE_ECHOFAIL]                     = 
    {
        .opCode = 0x12,
        .protocolId = AYLAPROTOCOL_DATAOPS,
    },
    [DATAOPCODE_ENABLESERVICELISTENER]        = 
    {
        .opCode = 0x13,
        .protocolId = AYLAPROTOCOL_DATAOPS,
    },
    [DATAOPCODE_ERROR]                        = 
    {
        .opCode = 0x14,
        .protocolId = AYLAPROTOCOL_DATAOPS,
    },
    [DATAOPCODE_CONFIRM]                      = 
    {
        .opCode = 0x15,
        .protocolId = AYLAPROTOCOL_DATAOPS,
    },
    [DATAOPCODE_PROPNOTIFICATION]             = 
    {
        .opCode = 0x16,
        .protocolId = AYLAPROTOCOL_DATAOPS,
    },
    [DATAOPCODE_EVENTNOTIFICATION]            = 
    {
        .opCode = 0x17,
        .protocolId = AYLAPROTOCOL_DATAOPS,
    },
    [CONTROLOPCODE_RESPONSE]                  = 
    {
        .opCode = 0x01,
        .protocolId = AYLAPROTOCOL_CONTROLOPS,
    },
    [CONTROLOPCODE_GET_CONFIGITEMS]           = 
    {
        .opCode = 0x02,
        .protocolId = AYLAPROTOCOL_CONTROLOPS,
    },
    [CONTROLOPCODE_SET_CONFIGITEMS]           = 
    {
        .opCode = 0x03,
        .protocolId = AYLAPROTOCOL_CONTROLOPS,
    },
    [CONTROLOPCODE_SAVECONFIG]                = 
    {
        .opCode = 0x04,
        .protocolId = AYLAPROTOCOL_CONTROLOPS,
    },
    [CONTROLOPCODE_NAK]                       = 
    {
        .opCode = 0x06,
        .protocolId = AYLAPROTOCOL_CONTROLOPS,
    },
    [CONTROLOPCODE_LOAD_STARTUPCONFIG]        = 
    {
        .opCode = 0x07,
        .protocolId = AYLAPROTOCOL_CONTROLOPS,
    },
    [CONTROLOPCODE_LOAD_FACTORYCONFIG]        = 
    {
        .opCode = 0x08,
        .protocolId = AYLAPROTOCOL_CONTROLOPS,
    },
    [CONTROLOPCODE_OTA_STATUS]                = 
    {
        .opCode = 0x09,
        .protocolId = AYLAPROTOCOL_CONTROLOPS,
    },
    [CONTROLOPCODE_OTA_COMMAND]               = 
    {
        .opCode = 0x0A,
        .protocolId = AYLAPROTOCOL_CONTROLOPS,
    },
    [CONTROLOPCODE_LOGOPS]                    = 
    {
        .opCode = 0x0C,
        .protocolId = AYLAPROTOCOL_CONTROLOPS,
    },
    [CONTROLOPCODE_MCUOTA_REPORT_START]       = 
    {
        .opCode = 0x0D,
        .protocolId = AYLAPROTOCOL_CONTROLOPS,
    },
    [CONTROLOPCODE_MCUOTA_LOAD]               = 
    {
        .opCode = 0x0E,
        .protocolId = AYLAPROTOCOL_CONTROLOPS,
    },
    [CONTROLOPCODE_MCUOTA_STATUS]             = 
    {
        .opCode = 0x0F,
        .protocolId = AYLAPROTOCOL_CONTROLOPS,
    },
    [CONTROLOPCODE_MCUOTA_BOOT]               = 
    {
        .opCode = 0x10,
        .protocolId = AYLAPROTOCOL_CONTROLOPS,
    },
    [CONTROLOPCODE_CONFUPDATE]                =
    {
        .opCode = 0x11,
        .protocolId = AYLAPROTOCOL_CONTROLOPS,
    },
    [CONTROLOPCODE_WIFI_JOIN]                 = 
    {
        .opCode = 0x12,
        .protocolId = AYLAPROTOCOL_CONTROLOPS,
    },
    [CONTROLOPCODE_WIFI_DELETE]               = 
    {
        .opCode = 0x13,
        .protocolId = AYLAPROTOCOL_CONTROLOPS,
    },
};



/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/05/04
*******************************************************************************/
#pragma optimize=none
uint32_t DeserializeToInteger32Bits (uint8_t * bufferIn, uint8_t size)
{
	uint32_t deserializeValue = 0;
	
    if (size <= 4)
    {
        for (uint8_t i = 0; i < size; i++)
        {
            deserializeValue |= bufferIn[i] << (8 * (size - (i+1)));
        }
    }
	return deserializeValue;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/05/04
*******************************************************************************/
ayla_OpCode_t Get_OpCode(AylaHeader_t opCodeDescription)
{
    for (uint8_t i = 0; i < MAX_OPCODEDEFINITION; i++)
    {
        if (OpCodeTable[i].protocolId == opCodeDescription.protocolId)
        {
            if (OpCodeTable[i].opCode == opCodeDescription.opCode)
            {
                return ((ayla_OpCode_t) i);
            }
        }
    }
    return (ayla_OpCode_t)-1;
}

/*******************************************************************************
* @brief  allocate data memory from the RTOS heap.  Altough it serve an important 
*       system task, the function is not blocking forever.  It is up to the calling
*       services, case by case, to address the criticality of the task.
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
static void * allocate_data_memory (size_t size)
{
    void * mem_ptr = NULL;
    uint8_t retry = 3;
    
    while ((NULL == mem_ptr) && (retry-- > 0))
    {
        mem_ptr = pvPortMalloc(size);
        
        if (NULL == mem_ptr)
        {
            osDelay(10);
        }
    }
    return mem_ptr;
}


/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/04/26
*******************************************************************************/
aylaTLV_t * Ayla_CreateCentsTLV (uint32_t value)
{
    aylaTLV_t *tlv;
    
    tlv = allocate_data_memory(sizeof(aylaTLV_t));
    
    if (NULL != tlv)
    {
        tlv->type = TLVTYPE_CENTS;
        tlv->length = 4;
		tlv->isLongString = 0;
        tlv->next = NULL;
        {
            uint8_t *valueList;
            
            valueList = allocate_data_memory(tlv->length);
            
            if (NULL != valueList)
            {
				uint8_t *valueTbl;
				
				valueTbl = (uint8_t*)&value;
				for (uint8_t i = 0; i < 4; i++)
				{
					valueList[i] = valueTbl[(4-1)-i];
				}
                tlv->value = valueList;
            }
            else
            {
                vPortFree(tlv);
                tlv = NULL;
            }        
        }
        /* From here, do not manipulate pointer "tlv" has it could have been 
                destroyed in the preceding operations             */
    }
    
    return tlv;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/04/26
*******************************************************************************/
aylaTLV_t * Ayla_CreateIntegerTLV (uint32_t value)
{
    aylaTLV_t *tlv;
    
    tlv = allocate_data_memory(sizeof(aylaTLV_t));
    
    if (NULL != tlv)
    {
        tlv->type = TLVTYPE_INTEGER;        
		if (value < 256)
		{
			tlv->length = 1;
		} 
		else if (value < 65536)
		{
			tlv->length = 2;
		}
		else
		{
			tlv->length = 4;
		}
		tlv->isLongString = 0;
        tlv->next = NULL;
        {
            uint8_t *valueList;
            
            valueList = allocate_data_memory(tlv->length);
            
            if (NULL != valueList)
            {
                memcpy (valueList, (const void*)&value, tlv->length);
                tlv->value = valueList;
            }
            else
            {
                vPortFree(tlv);
                tlv = NULL;
            }        
        }
        /* From here, do not manipulate pointer "tlv" has it could have been 
                destroyed in the preceding operations             */
    }
    
    return tlv;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/04/26
*******************************************************************************/
aylaTLV_t * Ayla_CreateBooleanTLV (uint8_t value)
{
    aylaTLV_t *tlv;
    
    tlv = allocate_data_memory(sizeof(aylaTLV_t));
    
    if (NULL != tlv)
    {
        tlv->type = TLVTYPE_BOOL;        
		tlv->length = 1;
		tlv->isLongString = 0;
		tlv->next = NULL;
		
        {
            uint8_t *valueList;
            
            valueList = allocate_data_memory(tlv->length);
            
            if (NULL != valueList)
            {
                *valueList = value;
                tlv->value = valueList;
            }
            else
            {
                vPortFree(tlv);
                tlv = NULL;
            }        
        }
        /* From here, do not manipulate pointer "tlv" has it could have been 
                destroyed in the preceding operations             */
    }
    
    return tlv;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/04/26
*******************************************************************************/
aylaTLV_t * Ayla_CreateNameTLV (uint8_t const * name)
{
    aylaTLV_t *tlv;
    
    tlv = allocate_data_memory(sizeof(aylaTLV_t));
    
    if (NULL != tlv)
    {
        tlv->type = TLVTYPE_NAME;
        tlv->length = strlen((char const *) name);
		tlv->isLongString = 0;
        tlv->next = NULL;
        {
            uint8_t *valueList;
            
            valueList = allocate_data_memory(tlv->length);
            
            if (NULL != valueList)
            {
				strncpy((char *)valueList, (char const *)name,tlv->length);
                tlv->value = valueList;
            }
            else
            {
                vPortFree(tlv);
                tlv = NULL;
            }        
        }
        /* From here, do not manipulate pointer "tlv" has it could have been 
                destroyed in the preceding operations             */
    }
    
    return tlv;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/04/26
*******************************************************************************/
aylaTLV_t * Ayla_CreateTextTLV (char * text)
{
    aylaTLV_t *tlv;
    
    tlv = allocate_data_memory(sizeof(aylaTLV_t));
    
    if (NULL != tlv)
    {
        tlv->type = TLVTYPE_TEXT;
        tlv->length = strlen(text);
		tlv->isLongString = 0;
        tlv->next = NULL;
        {
            uint8_t *valueList;
            
            valueList = allocate_data_memory(tlv->length);
            
            if (NULL != valueList)
            {
				strncpy((char*)valueList, text, tlv->length);
                tlv->value = valueList;
            }
            else
            {
                vPortFree(tlv);
                tlv = NULL;
            }        
        }
        /* From here, do not manipulate pointer "tlv" has it could have been 
                destroyed in the preceding operations             */
    }
    
    return tlv;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/04/26
*******************************************************************************/
aylaTLV_t * Ayla_CreateBinaryTLV (uint8_t * table, uint8_t tableLength)
{
    aylaTLV_t *tlv;
    
    tlv = allocate_data_memory(sizeof(aylaTLV_t));
    
    if (NULL != tlv)
    {
        tlv->type = TLVTYPE_BINARY;
        tlv->length = tableLength;
		tlv->isLongString = 0;
        tlv->next = NULL;
        {
            uint8_t *valueList;
            
            valueList = allocate_data_memory(tlv->length);
            
            if (NULL != valueList)
            {
				memcpy(valueList, (char const*)table, tlv->length);
                tlv->value = valueList;
            }
            else
            {
                vPortFree(tlv);
                tlv = NULL;
            }        
        }
        /* From here, do not manipulate pointer "tlv" has it could have been 
                destroyed in the preceding operations             */
    }
    
    return tlv;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/04/26
*******************************************************************************/
aylaTLV_t * Ayla_CreateConfigTokenTLV (int numberOfToken, ...)
{
    aylaTLV_t * tlv;
    
    tlv = allocate_data_memory(sizeof(aylaTLV_t));
    
    if (NULL != tlv)
    {
        tlv->type = TLVTYPE_CONFIGTOKENS;
        tlv->length = numberOfToken;
		tlv->isLongString = 0;
        tlv->next = NULL;
        {
            uint8_t *valueList;
            
            valueList = allocate_data_memory(numberOfToken);
            
            if (NULL != valueList)
            {
                va_list valist;
                va_start(valist, numberOfToken);
            
                for (uint8_t i = 0; i < numberOfToken; i++)
                {
                    valueList[i] = va_arg(valist, int);
                }
                
                va_end(valist);
                
                tlv->value = valueList;
            }
            else
            {
                vPortFree(tlv);
                tlv = NULL;
            }        
        }
        /* From here, do not manipulate pointer "tlv" has it could have been 
                destroyed in the preceding operations             */
    }
    
    return tlv;
}


/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/04/27
*******************************************************************************/
aylaTLV_t * Ayla_CreateFeatureMaskTLV (uint8_t mask)
{
    aylaTLV_t *tlv;
    
    tlv = allocate_data_memory(sizeof(aylaTLV_t));
    
    if (NULL != tlv)
    {
        tlv->type = TLVTYPE_FEATUREMASK;
        tlv->length = 1;
		tlv->isLongString = 0;
        tlv->next = NULL;
        {
            uint8_t *valueList;
            
            valueList = allocate_data_memory(1);
            
            if (NULL != valueList)
            {
				valueList[0] = mask;
                tlv->value = valueList;
            }
            else
            {
                vPortFree(tlv);
                tlv = NULL;
            }        
        }
        /* From here, do not manipulate pointer "tlv" has it could have been 
                destroyed in the preceding operations             */
    }
    
    return tlv;
}


/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/04/27
*******************************************************************************/
aylaTLV_t * Ayla_CreateErrorTLV (uint8_t error)
{
    aylaTLV_t *tlv;
    
    tlv = allocate_data_memory(sizeof(aylaTLV_t));
    
    if (NULL != tlv)
    {
        tlv->type = TLVTYPE_ERROR;
        tlv->length = 1;
		tlv->isLongString = 0;
        tlv->next = NULL;
        {
            uint8_t *valueList;
            
            valueList = allocate_data_memory(1);
            
            if (NULL != valueList)
            {
				valueList[0] = error;
                tlv->value = valueList;
            }
            else
            {
                vPortFree(tlv);
                tlv = NULL;
            }        
        }
        /* From here, do not manipulate pointer "tlv" has it could have been 
                destroyed in the preceding operations             */
    }
    
    return tlv;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void destroy_datapoint(aylaDataPoint_t ** dataPoint)
{
    if (NULL != *dataPoint)
    {
        aylaDataPoint_t * data = *dataPoint;
        aylaTLV_t * tlv = data->firstTLV;
        
        while (tlv != NULL)
        {
            aylaTLV_t * next_tlv = tlv->next;
            if (NULL != tlv->value)
            {
                vPortFree(tlv->value);
            }
            vPortFree(tlv);
            tlv = next_tlv;
        }
        vPortFree(*dataPoint);
        *dataPoint = NULL;
    }
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/04/26
*******************************************************************************/
void Ayla_AddTLV_to_dataPoint(aylaDataPoint_t ** _dataPoint, aylaTLV_t * tlv)
{
    if (NULL != *_dataPoint)
    {
        aylaDataPoint_t * dataPoint = *_dataPoint;
        if (NULL != tlv)
        {
            if (NULL == dataPoint->firstTLV)
            {
                dataPoint->firstTLV = tlv;
                dataPoint->lastTLV = dataPoint->firstTLV;
            }
            else
            {
                dataPoint->lastTLV->next = tlv;
                dataPoint->lastTLV = dataPoint->lastTLV->next;
            }
        }
        else
        {
            destroy_datapoint(_dataPoint);
        }
    }    
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/04/26
*******************************************************************************/
aylaDataPoint_t* Ayla_CreateDataPoint (ayla_OpCode_t opCode, uint16_t requestId)
{
    aylaDataPoint_t * datapoint;
    
    datapoint = allocate_data_memory(sizeof(aylaDataPoint_t));
    
    if (NULL != datapoint)
    {
        datapoint->header.opCode = OpCodeTable[opCode].opCode;
        datapoint->header.protocolId = OpCodeTable[opCode].protocolId;
        datapoint->RequestID = requestId;
		datapoint->headerFooterSize = AYLA_HEADER_FOOTER_OFFSET;
		
        datapoint->firstTLV = NULL;
        datapoint->lastTLV = NULL;
    }
    
    return datapoint;
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
