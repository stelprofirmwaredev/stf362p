/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    AylaSendProperties.c
* @date    2016/10/06
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <string.h>
#include <math.h>

#include ".\HAL\inc\HAL_CloudInterface.h"
#include ".\HAL\inc\AylaDefinitions.h"
#include ".\HAL\inc\AylaProperties.h"

#include ".\BSP\Ayla WM-N-BM-30\inc\BSP_Ayla_WM_N_MB_30.h"

#include ".\DB\inc\THREAD_DBDataTypes.h"
#include ".\DB\inc\THREAD_DB.h"

#include "tools.h"
#include "StringObjects.h"
#include "SwVersion.h"

#include "APP_SetpointManager.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
extern const prop_t propTable[];
#ifdef FREERTOS_SUPPORT
    #include "trcRecorder.h"
    extern char* wifiEventChannel;
#endif


/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static void addStringSetpointToBuffer (char *textBuffer, Setpoint100_t setpoint, char * separator, char *defaultVal);

/*******************************************************************************
*    functions definitions
*******************************************************************************/

/*******************************************************************************
* @brief  this function adds at the end of a string buffer
*			(concatenation using strcat()) a setpoint in the format of "dd.d;"
*
* @inputs :
*			char* : pointer to a string buffer
*			Setpoint100_t : setpoint to print to the string buffer
* @retval None
* @author
* @date
*******************************************************************************/
static void addStringSetpointToBuffer (char *textBuffer, Setpoint100_t setpoint, char * separator, char *defaultVal)
{
	char setpointStr[SETPOINT_STRING_SIZE+NULL_TERMINATION_SIZE];

	if ((setpoint >= MINIMUM_SETPOINT) && (setpoint <= MAXIMUM_FLOOR_SETPOINT))           //must not print a value more than 99.9
	{
		sprintf (setpointStr, "%.1f%s", (float)setpoint/100, separator);
	}
	else
	{
		sprintf (setpointStr, "%s%s",defaultVal, separator);
	}
	strcat(textBuffer, setpointStr);
}



/*******************************************************************************
* @brief  Send the Default Setpoint to the Cloud
* @inputs pData: void pointer to the Default Setpoint instance (don't care)
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/06
*******************************************************************************/
uint8_t SendDefaultSetpoint(void* pData, dbChangeSource_t source)
{
    int16_t number;

    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x000c);


#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"DefaultSetpoint\" ");
#endif

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_DEFAULTSETPOINT].propertyName));

    DBTHRD_GetData(DBTYPE_DEFAULT_SETPOINT, &number, INDEX_DONT_CARE);
    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateCentsTLV(number));

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Home State to the Cloud
* @inputs pData: void pointer to the Home State instance (don't care)
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/05
*******************************************************************************/
uint8_t SendHomeState(void* pData, dbChangeSource_t source)
{
    ModeEnum_t state;

    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x000d);


#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"ResidenceState\" ");
#endif

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_HOMESTATE].propertyName));


    DBTHRD_GetData(DBTYPE_HOME_STATE, &state, INDEX_DONT_CARE);
    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateIntegerTLV (state));

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Ambient Setpoint to the Cloud
* @inputs pData: void pointer to the Ambient Setpoint instance (index)
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/03
*******************************************************************************/
uint8_t SendAmbientSetpoint(void* pData, dbChangeSource_t source)
{
    Setpoint100_t setpoint;
    uint8_t i;
    char textBuffer[(SETPOINT_STRING_SIZE*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x000e);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_AMBIENTSETPOINT].propertyName));

    strcpy(textBuffer,"");
    for (i = 0; i < THERMOSTAT_INSTANCES; i++)
    {
        DBTHRD_GetData(DBTYPE_AMBIENT_SETPOINT, &setpoint, i);
        addStringSetpointToBuffer (textBuffer, setpoint, ";", "0");
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Property \"TstatAmbientSetpoint\" for TH%d",*((uint8_t*)pData));
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void * allocate_text_buffer (size_t size)
{
    void * mem_ptr = NULL;

    while (NULL == mem_ptr)
    {
        mem_ptr = pvPortMalloc(size);

        if (NULL == mem_ptr)
        {
            osDelay(1);
        }
    }
    return mem_ptr;
}

/*******************************************************************************
* @brief  Send the Ambient Temperature to the Cloud
* @inputs pData: void pointer to the Ambient Temperature instance (index)
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/03
*******************************************************************************/
uint8_t SendAmbientTemperature(void* pData, dbChangeSource_t source)
{
    uint8_t i;
    char *textBuffer;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x000f);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_AMBIENTTEMPERATURE].propertyName));

    /* textBuffer is using malloc to avoid requesting ~100 bytes off the stack */
    textBuffer = allocate_text_buffer((TEMPERATURE_STRING_SIZE*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE);

    if (NULL != textBuffer)
    {
        strcpy(textBuffer,"");
        for (i = 0; i < THERMOSTAT_INSTANCES; i++)
        {
            TEMPERATURE_C_t room_temp;
            char roomTempStr [TEMPERATURE_STRING_SIZE+NULL_TERMINATION_SIZE];

            DBTHRD_GetData(DBTYPE_AMBIENT_TEMPERATURE, &room_temp, i);

            if ((room_temp != DB_SENSOR_INVALID) && (room_temp != DB_SENSOR_HIGH) && (room_temp != DB_SENSOR_LOW))
            {
                room_temp = TLS_Round(room_temp, 50);
                sprintf(roomTempStr, "%.1f;", (float)room_temp/100);
            }
            else
            {
                sprintf(roomTempStr, ";");
            }
            strcat(textBuffer, roomTempStr);
        }

        Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

        vPortFree(textBuffer);

#ifdef FREERTOS_SUPPORT
        vTracePrintF(wifiEventChannel, "Sending Property \"TstatAmbientTemperature\" for TH%d",*((uint8_t*)pData));
#endif
        return (HAL_QWiFiMsg (newDataPoint));
    }
    return -1;
}

/*******************************************************************************
* @brief  Send the Active Alerts to the Cloud
* @inputs pData: void pointer to the thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/11
*******************************************************************************/
uint8_t SendActiveAlerts(void* pData, dbChangeSource_t source)
{
    uint8_t i, j;
    uint8_t alert_text[3];
    dbType_Alerts_t alert;
    char textBuffer [(ALERT_MAX_STRING_SIZE*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0010);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_ACTIVEALERTS].propertyName));



    strcpy(textBuffer, "");
    for (i = 0; i < THERMOSTAT_INSTANCES; i++)
    {
        DBTHRD_GetData(DBTYPE_ACTIVE_ALERTS, &alert, i);
        for (j = 0; j < ALERT_INSTANCES; j++)
        {
            if (alert.ActiveAlerts[j] <= 9)
            {
                alert_text[0] = alert.ActiveAlerts[j] + 0x30;
                alert_text[1] = 0;
            }
            else
            {
                alert_text[0] = (alert.ActiveAlerts[j] / 10) + 0x30;
                alert_text[1] = (alert.ActiveAlerts[j] % 10) + 0x30;
                alert_text[2] = 0;
            }
            //Check if there is an active alert
            if (alert_text[0] != '0')
            {
                strncat(textBuffer, (char const*)alert_text, strlen((char const*)alert_text));
                if (j != (ALERT_INSTANCES - 1))
                {
                    if (alert.ActiveAlerts[j+1] != NULL)
                    {
                        strcat(textBuffer, ",");
                    }
                }
            }
            else
            {
                //Check if this thermostat instance has no alert at all
                if (j == 0)
                {
                    //If this thermostat instance has no active alert, set the alert value to 0
                    strncat(textBuffer, (char const*)&alert_text[0], 1);
                    break;
                }
            }
        }
        strcat(textBuffer, ";");
    }


    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Property \"TstatActiveAlerts\" for TH%d",*((uint8_t*)pData));
#endif
    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Ambient Humidity to the Cloud
* @inputs pData: void pointer to the Ambient Humidity instance (index)
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/12
*******************************************************************************/
uint8_t SendAmbientHumidity(void* pData, dbChangeSource_t source)
{
    char textBuffer[(HUMIDITY_STRING_SIZE*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE];
    uint8_t i;

    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0011);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_AMBIENTHUMIDITY].propertyName));

    strcpy(textBuffer, "");
    for (i = 0; i < THERMOSTAT_INSTANCES; i++)
    {
        RelativeHumidity_t roomHumidity;
        char roomHumidityStr [HUMIDITY_STRING_SIZE+NULL_TERMINATION_SIZE];

        DBTHRD_GetData(DBTYPE_AMBIENT_HUMIDITY, &roomHumidity, i);
        if (roomHumidity != DB_HUMIDITY_INVALID)
        {
            roomHumidity = TLS_Round((int16_t)roomHumidity,5);
            sprintf (roomHumidityStr, "%d;", roomHumidity);
        }
        else
        {
            sprintf (roomHumidityStr, "0;");
        }

        strcat(textBuffer,roomHumidityStr);
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Property \"TstatAmbientHumidity\" for TH%d",*((uint8_t*)pData));
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Temperature Format to the Cloud
* @inputs pData: void pointer to the Temperature Format data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
uint8_t SendTemperatureFormat(void* pData, dbChangeSource_t source)
{
    uint8_t tempFormat;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0012);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_TEMPERATUREFORMAT].propertyName));

    DBTHRD_GetData(DBTYPE_TEMPERATURE_FORMAT, &tempFormat, INDEX_DONT_CARE);
    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateIntegerTLV(tempFormat));

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"TemperatureFormat\" ");
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Time Format to the Cloud
* @inputs pData: void pointer to the Time Format data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
uint8_t SendTimeFormat(void* pData, dbChangeSource_t source)
{
    uint8_t timeFormat;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0013);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_TIMEFORMAT].propertyName));

    DBTHRD_GetData(DBTYPE_TIME_FORMAT, &timeFormat, INDEX_DONT_CARE);
    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateIntegerTLV(timeFormat));

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"TimeFormat\" ");
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Lock State to the Cloud
* @inputs pData: void pointer to the thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
uint8_t SendLockState(void* pData, dbChangeSource_t source)
{
    char lockState;
    char textBuffer[(LOCK_STATE_STRING_SIZE*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE];
    uint8_t i;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0014);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_LOCKSTATE].propertyName));

    strcpy(textBuffer, "");
    for (i = 0; i < THERMOSTAT_INSTANCES; i++)
    {
        char lockStateStr[LOCK_STATE_STRING_SIZE+NULL_TERMINATION_SIZE];

        DBTHRD_GetData(DBTYPE_LOCK_STATE, &lockState, i);

        if (lockState < 10)             //can display only one digit
        {
            sprintf (lockStateStr, "%d;", lockState);
        }
        else
        {
            sprintf (lockStateStr, "0;");
        }
        strcat(textBuffer, lockStateStr);
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"LockState\" ");
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Lock Delta to the Cloud
* @inputs pData: void pointer to the thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
uint8_t SendLockDelta(void* pData, dbChangeSource_t source)
{
    uint8_t lockDelta;
    char textBuffer[(LOCK_DELTA_STRING_SIZE*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE];
    uint8_t i;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0015);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_LOCKDELTA].propertyName));

    strcpy(textBuffer, "");
    for (i = 0; i < THERMOSTAT_INSTANCES; i++)
    {
        char lockDeltaStr[3];

        DBTHRD_GetData(DBTYPE_LOCK_SETPOINT_LIMIT, &lockDelta, i);

        if (lockDelta < 10)             //can display only 1 digit
        {
            sprintf(lockDeltaStr, "%d;", lockDelta);
        }
        else
        {
            sprintf(lockDeltaStr, "0;");
        }
        strcat(textBuffer, lockDeltaStr);
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"TstatLockDelta\" ");
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Home State Changed By Geofencing indicator to the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
uint8_t SendHomeStateChangedByGeofencing(void* pData, dbChangeSource_t source)
{
    uint8_t geofencingChange;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0016);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_HOMESTATECHANGEDBYGEOFENCING].propertyName));

    /********************************************************/
    /*              PropertyValue                           */
    /********************************************************/
    DBTHRD_GetData(DBTYPE_GEOFENCING_CHANGE, &geofencingChange, INDEX_DONT_CARE);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                                Ayla_CreateIntegerTLV(geofencingChange));

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"StateChangedByGeofencing\" ");
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Heat Demand to the Cloud
* @inputs pData: void pointer to the thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
uint8_t SendHeatDemand(void* pData, dbChangeSource_t source)
{
    uint8_t heatDemand;
    char textBuffer[(HEAT_DEMAND_STRING_SIZE*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE];
    uint8_t i;

    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0017);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_HEATDEMAND].propertyName));

    strcpy(textBuffer, "");
    for (i = 0; i < THERMOSTAT_INSTANCES; i++)
    {
        char heatDemandStr[HEAT_DEMAND_STRING_SIZE+NULL_TERMINATION_SIZE];

        DBTHRD_GetData(DBTYPE_HEAT_DEMAND, &heatDemand, i);
        sprintf(heatDemandStr, "%d;", heatDemand);
        strcat(textBuffer, heatDemandStr);
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Property \"TstatHeatDemand\" for TH%d",*((uint8_t*)pData));
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Controller Version to the Cloud
* @inputs pData: void pointer to index (don't care)
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/20
*******************************************************************************/
uint8_t SendControllerVersion(void* pData, dbChangeSource_t source)
{
    char textBuffer[SW_VERSION_STRING_SIZE+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0018);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_CONTROLLERVERSION].propertyName));


    sprintf (textBuffer, "%05.2f%s",(float)SW_VER/100, BUILD_INFO);

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"ControlerSwVersion\" ");
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the oem Host Version to the Cloud
* @inputs pData: void pointer to index (don't care)
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/20
*******************************************************************************/
uint8_t SendOemHostVersion(void* pData, dbChangeSource_t source)
{
    char textBuffer[OEM_VERSION_STRING_SIZE+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0019);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_OEMHOSTVERSION].propertyName));


    sprintf (textBuffer, "%.1f",(float)OEM_HOST_VERSION/10);

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"oem_host_version\" ");
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the ZigBee Association Permit flag to the Cloud
* @inputs pData: void pointer to index (don't care)
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/20
*******************************************************************************/
uint8_t SendZigBeeAssociationPermit(void* pData, dbChangeSource_t source)
{
    uint8_t zap;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x001a);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_ZIGBEEASSOCIATIONPERMIT].propertyName));

    DBTHRD_GetData(DBTYPE_ZIGBEE_ASSOCIATION_PERMIT, &zap, INDEX_DONT_CARE);
    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateIntegerTLV(zap));

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"ZigBeeAssociationPermit\" ");
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Thermostat Name to the Cloud
* @inputs pData: void pointer to the thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/24
*******************************************************************************/
uint8_t SendThermostatName(void* pData, dbChangeSource_t source)
{
    char *textBuffer;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x001b);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_THERMOSTATNAME].propertyName));


    /* textBuffer is using malloc to avoid requesting ~700 off the stack */
    textBuffer = allocate_text_buffer (((THERMOSTAT_NAME_STRING_SIZE+DELIMITER_SIZE)*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE);
    if (NULL != textBuffer)
    {
        strcpy(textBuffer, "");

        for (uint8_t i = 0; i < THERMOSTAT_INSTANCES; i++)
        {
			char longname[THERMOSTAT_NAME_STRING_SIZE];

            DBTHRD_GetData(DBTYPE_TSTAT_NAME_UTF8, &longname, i);
            strcat(textBuffer, longname);
            strcat(textBuffer, ";");
        }

        Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

        vPortFree (textBuffer);

#ifdef FREERTOS_SUPPORT
        vTracePrintF(wifiEventChannel, "Sending Property \"TstatName\" for TH%d",*((uint8_t*)pData));
#endif

        return (HAL_QWiFiMsg (newDataPoint));
    }
    return -1;
}

/*******************************************************************************
* @brief  Send the Geofence Enable bit to the Cloud
* @inputs pData: don't care
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/24
*******************************************************************************/
uint8_t SendGeofenceEnable(void* pData, dbChangeSource_t source)
{
    uint8_t state;

    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x001c);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_GEOFENCINGENABLE].propertyName));

    DBTHRD_GetData(DBTYPE_GEOFENCING_ENABLE, &state, INDEX_DONT_CARE);
    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateIntegerTLV(state));

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"GeofencingOnOff\" ");
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Geofence Mobile Id to the Cloud
* @inputs pData: pointer to user index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/24
*******************************************************************************/
uint8_t SendGeofencingMobileId(void* pData, dbChangeSource_t source)
{
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x001d);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_GEOFENCINGMOBILEID].propertyName));


    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(";"));

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Property \"MobileID\" for Mobile %d",*((uint8_t*)pData));
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Group Names to the Cloud
* @inputs pData: pointer to user index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/26
*******************************************************************************/
uint8_t SendGroupName(void* pData, dbChangeSource_t source)
{
    char *textBuffer;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x001e);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_GROUPNAME].propertyName));


    /* textBuffer is using malloc to avoid requesting ~300 off the stack */
    textBuffer = allocate_text_buffer (((GROUP_NAME_STRING_SIZE+DELIMITER_SIZE)*GROUP_INSTANCES)+NULL_TERMINATION_SIZE);
    if (NULL != textBuffer)
    {
        strcpy(textBuffer, "");
        for (uint8_t i = 0; i < GROUP_INSTANCES; i++)
        {
			char longname[GROUP_NAME_STRING_SIZE];

            DBTHRD_GetData(DBTYPE_GROUP_NAME_UTF8, &longname, i);
            strcat(textBuffer, longname);
            strcat(textBuffer, ";");
        }

        Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

        vPortFree (textBuffer);

#ifdef FREERTOS_SUPPORT
        vTracePrintF(wifiEventChannel, "Sending Property \"GroupName\" for Group %d",*((uint8_t*)pData));
#endif

        return (HAL_QWiFiMsg (newDataPoint));
    }
    return -1;
}

/*******************************************************************************
* @brief  Send the Group Members to the Cloud
* @inputs pData: pointer to user index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/26
*******************************************************************************/
uint8_t SendGroupMembers(void* pData, dbChangeSource_t source)
{
    uint8_t i, j;
    dbType_GroupMembers_t groupMembers;

    /* GROUP_INSTANCES is needed in the calculation to add ";" character */
    char textBuffer[(GROUP_MEMBER_STRING_SIZE*THERMOSTAT_INSTANCES)+GROUP_INSTANCES+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x001f);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_GROUPMEMBERS].propertyName));

    strcpy(textBuffer, "");
    for (i = 0; i < GROUP_INSTANCES; i++)
    {
        DBTHRD_GetData(DBTYPE_GROUP_MEMBERS, &groupMembers, i);
        //Check if this group instance has members
        if (groupMembers.MembersList != 0)
        {
            for (j = 0; j < THERMOSTAT_INSTANCES; j++)
            {
                if (0 != (groupMembers.MembersList & (1<<j)))
                {
                    char memberString[GROUP_MEMBER_STRING_SIZE+NULL_TERMINATION_SIZE];

                    groupMembers.MembersList &= ~(1<<j);
                    if (groupMembers.MembersList != 0)
                    {
                        sprintf (memberString, "%d,", j+1);
                    }
                    else
                    {
                        sprintf (memberString, "%d", j+1);
                    }
                    strcat(textBuffer, memberString);
                }
            }
        }
        else
        {
            //This group instance has no member, do not set any value
        }
        strcat(textBuffer, ";");
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Property \"GroupMembers\" for Group %d",*((uint8_t*)pData));
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Group Setpoint to the Cloud
* @inputs pData: pointer to user index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/26
*******************************************************************************/
uint8_t SendGroupSetpoint(void* pData, dbChangeSource_t source)
{
    char textBuffer[(SETPOINT_STRING_SIZE*GROUP_INSTANCES)+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0020);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_GROUPSETPOINT].propertyName));


    strcpy(textBuffer, "");
    for (uint8_t i = 0; i < GROUP_INSTANCES; i++)
    {
		Setpoint100_t setpoint;

        DBTHRD_GetData(DBTYPE_GROUP_SETPOINT, &setpoint, i);
        addStringSetpointToBuffer (textBuffer, setpoint, ";", "0");
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Property \"GroupSetpoint\" for Group %d",*((uint8_t*)pData));
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Group Away Setpoint to the Cloud
* @inputs pData: pointer to user index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/26
*******************************************************************************/
uint8_t SendGroupAwaySetpoint(void* pData, dbChangeSource_t source)
{
    char textBuffer[(SETPOINT_STRING_SIZE*GROUP_INSTANCES)+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0021);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_GROUPAWAYSETPOINT].propertyName));


    strcpy(textBuffer, "");
    for (uint8_t i = 0; i < GROUP_INSTANCES; i++)
    {
		Setpoint100_t setpoint;

        DBTHRD_GetData(DBTYPE_GROUP_AWAY_SETPOINT, &setpoint, i);
        addStringSetpointToBuffer (textBuffer, setpoint, ";", "0");
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Property \"GroupAwaySetpoint\" for Group %d",*((uint8_t*)pData));
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Group Vacation Setpoint to the Cloud
* @inputs pData: pointer to user index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/26
*******************************************************************************/
uint8_t SendGroupVacationSetpoint(void* pData, dbChangeSource_t source)
{
    char textBuffer[(SETPOINT_STRING_SIZE*GROUP_INSTANCES)+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0022);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_GROUPVACATIONSETPOINT].propertyName));


    strcpy(textBuffer, "");
    for (uint8_t i = 0; i < GROUP_INSTANCES; i++)
    {
		Setpoint100_t setpoint;

        DBTHRD_GetData(DBTYPE_GROUP_VACATION_SETPOINT, &setpoint, i);
        addStringSetpointToBuffer (textBuffer, setpoint, ";", "0");
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Prop. \"GroupVacationSetpoint\" for Group %d",*((uint8_t*)pData));
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Group Standby Setpoint to the Cloud
* @inputs pData: pointer to user index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/26
*******************************************************************************/
uint8_t SendGroupStandbySetpoint(void* pData, dbChangeSource_t source)
{
    char textBuffer[(SETPOINT_STRING_SIZE*GROUP_INSTANCES)+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0023);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_GROUPSTANDBYSETPOINT].propertyName));


    strcpy(textBuffer, "");
    for (uint8_t i = 0; i < GROUP_INSTANCES; i++)
    {
		Setpoint100_t setpoint;

        DBTHRD_GetData(DBTYPE_GROUP_STANDBY_SETPOINT, &setpoint, i);
        addStringSetpointToBuffer (textBuffer, setpoint, ";", "0");
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Prop. \"GroupStandbySetpoint\" for Group %d",*((uint8_t*)pData));
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Thermostat Model to the Cloud
* @inputs pData: void pointer to the thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/03
*******************************************************************************/
uint8_t SendThermostatModel(void* pData, dbChangeSource_t source)
{
    char *textBuffer;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0024);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_THERMOSTATMODEL].propertyName));


    /* textBuffer is using malloc to avoid requesting ~700 off the stack */
    textBuffer = allocate_text_buffer (((THERMOSTAT_MODEL_STRING_SIZE+DELIMITER_SIZE)*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE);
    if (NULL != textBuffer)
    {
        strcpy(textBuffer, "");

        for (uint8_t i = 0; i < THERMOSTAT_INSTANCES; i++)
        {
            /* longname buffer must be long enough to contains THERMOSTAT_MODEL_STRING_SIZE */
            /*  of UTF-8 characters                                                         */
            char longname[(THERMOSTAT_MODEL_STRING_SIZE*2)+NULL_TERMINATION_SIZE];

            DBTHRD_GetData(DBTYPE_TSTAT_MODEL_UTF8, &longname, i);

            /* must not print more than THERMOSTAT_MODEL_STRING_SIZE characters, otherwise  */
            /*  it will not fit in an Ayla Data point                                       */
            /*  Ayla datapoint max = 1024 bytes (512 if all UTF-8 special characters)       */
            strncat(textBuffer, longname, THERMOSTAT_MODEL_STRING_SIZE);
            strcat(textBuffer, ";");
        }

        Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

        vPortFree (textBuffer);

#ifdef FREERTOS_SUPPORT
        vTracePrintF(wifiEventChannel, "Sending Property \"TstatModelNumber\" for TH%d",*((uint8_t*)pData));
#endif

        return (HAL_QWiFiMsg (newDataPoint));
    }
    return -1;
}

/*******************************************************************************
* @brief  Send the Activity Start Request to the cloud
* @inputs pData: void pointer to the activity instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/17
*******************************************************************************/
uint8_t SendActivityStartRequested(void* pData, dbChangeSource_t source)
{
    char textBuffer[(ACTIVITY_START_STRING_SIZE*ACTIVITY_INSTANCES)+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0025);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_ACTIVITYSTARTREQUESTED].propertyName));


    //Always return cleared activity request back to the cloud to avoid having more than more request at a time
    strcpy(textBuffer, "0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;");

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"ActivityStartRequested\" ");
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Activity Names to the Cloud
* @inputs pData: pointer to activity index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/12/05
*******************************************************************************/
uint8_t SendActivityName(void* pData, dbChangeSource_t source)
{
    char *textBuffer;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0028);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_ACTIVITYNAME].propertyName));


    /* textBuffer is using malloc to avoid requesting ~700 off the stack */
    textBuffer = allocate_text_buffer (((ACTIVITY_NAME_STRING_SIZE+DELIMITER_SIZE)*ACTIVITY_INSTANCES)+NULL_TERMINATION_SIZE);
    if (NULL != textBuffer)
    {
        strcpy(textBuffer, "");
        for (uint8_t i = 0; i < ACTIVITY_INSTANCES; i++)
        {
			char longname[ACTIVITY_NAME_STRING_SIZE];

            DBTHRD_GetData(DBTYPE_ACTIVITY_NAME_UTF8, &longname, i);
            strcat(textBuffer, longname);
            strcat(textBuffer, ";");
        }

        Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

        vPortFree (textBuffer);

#ifdef FREERTOS_SUPPORT
        vTracePrintF(wifiEventChannel, "Sending Property \"ActivityName\" for Activity %d",*((uint8_t*)pData));
#endif

        return (HAL_QWiFiMsg (newDataPoint));
    }
    return -1;
}

/*******************************************************************************
* @brief  Send the Activity Ready At setting to the Cloud
* @inputs pData: pointer to activity index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/12/05
*******************************************************************************/
uint8_t SendActivityReadyAt(void* pData, dbChangeSource_t source)
{
    char textBuffer[(ACTIVITY_READYAT_STRING_SIZE*ACTIVITY_INSTANCES)+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0029);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_ACTIVITYREADYAT].propertyName));


    strcpy(textBuffer, "");

    for (uint8_t i = 0; i < ACTIVITY_INSTANCES; i++)
    {
        uint8_t readyState;
        char readyStateStr[ACTIVITY_READYAT_STRING_SIZE+NULL_TERMINATION_SIZE];

        DBTHRD_GetData(DBTYPE_ACTIVITY_READY_AT, &readyState, i);
        if (readyState < 9)           //must not print more than 1 digit
        {
            sprintf (readyStateStr, "%d;", readyState);
        }
        else
        {
            sprintf (readyStateStr, "0;");
        }
        strcat(textBuffer, readyStateStr);
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"ActivityReadyAt\" ");
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Activity Setpoint per Group to the Cloud
* @inputs pData: pointer to activity index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/12/05
*******************************************************************************/
uint8_t SendActivitySetpointPerGroup(void* pData, dbChangeSource_t source)
{
    char *textBuffer;
    aylaDataPoint_t * newDataPoint;


    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x002a);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_ACTIVITYSETPOINTPERGROUP].propertyName));


    /* textBuffer is using malloc to avoid requesting ~800 off the stack */
    textBuffer = allocate_text_buffer((SETPOINT_STRING_SIZE*GROUP_INSTANCES*ACTIVITY_INSTANCES)+NULL_TERMINATION_SIZE);

    if (NULL != textBuffer)
    {
        strcpy(textBuffer, "");
        for (uint8_t i = 0; i < ACTIVITY_INSTANCES; i++)
        {
            char setpointStr[SETPOINT_STRING_SIZE*GROUP_INSTANCES] = "";
            Setpoint100_t setpoints[GROUP_INSTANCES];

            DBTHRD_GetData(DBTYPE_ACTIVITY_SETPOINT_PER_GROUP, &setpoints, i);

            for (uint8_t j = 0; j < GROUP_INSTANCES-1; j++)
            {
                addStringSetpointToBuffer (setpointStr, setpoints[j], ",", NULL);
            }

            addStringSetpointToBuffer (setpointStr, setpoints[GROUP_INSTANCES-1], ";", NULL);
            strcat(textBuffer, setpointStr);
        }

        Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

        vPortFree (textBuffer);

#ifdef FREERTOS_SUPPORT
        vTracePrintF(wifiEventChannel, "Sending Property \"ActivitySetpointPerGroup\" for Activity %d",*((uint8_t*)pData));
#endif

        return (HAL_QWiFiMsg (newDataPoint));
    }
    return -1;
}

/*******************************************************************************
* @brief  Send the Activity Start Time to the Cloud
* @inputs pData: pointer to activity index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/12/05
*******************************************************************************/
uint8_t SendActivityStartTime(void* pData, dbChangeSource_t source)
{
    char textBuffer[(ACTIVITY_STARTTIME_STRING_SIZE*ACTIVITY_INSTANCES)+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x002b);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_ACTIVITYSTARTTIME].propertyName));


    strcpy(textBuffer, "");

    for (uint8_t i = 0; i < ACTIVITY_INSTANCES; i++)
    {
        char activityStartTimeStr[ACTIVITY_STARTTIME_STRING_SIZE+NULL_TERMINATION_SIZE];
        uint16_t activityStartTime;

        DBTHRD_GetData(DBTYPE_ACTIVITY_START_TIME, &activityStartTime, i);
        if ((activityStartTime != (uint16_t)ON_DEMAND) && (activityStartTime < 1440))
        {
            sprintf (activityStartTimeStr, "%d;", activityStartTime);
        }
        else
        {
            sprintf (activityStartTimeStr, "-1;");
        }
        strcat(textBuffer, activityStartTimeStr);
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Property \"ActivityStartTime\" for Activity %d",*((uint8_t*)pData));
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Activity Week Days to the Cloud
* @inputs pData: pointer to activity index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/12/05
*******************************************************************************/
uint8_t SendActiviyWeekDays(void* pData, dbChangeSource_t source)
{
    char *textBuffer;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x002c);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_ACTIVITYWEEKDAYS].propertyName));


    /* textBuffer is using malloc to avoid requesting ~300 off the stack */
    textBuffer = allocate_text_buffer ((ACTIVITY_WEEKDAYS_STRING_SIZE*7*ACTIVITY_INSTANCES)+NULL_TERMINATION_SIZE);

    if (NULL != textBuffer)
    {
        strcpy(textBuffer, "");

        for (uint8_t i = 0; i < ACTIVITY_INSTANCES; i++)
        {
            uint8_t activityWeekDays;

            DBTHRD_GetData(DBTYPE_ACTIVITY_WEEK_DAYS, &activityWeekDays, i);
            for (uint8_t j = 0; j < 7; j++)
            {
                //Check if the week day is active
                if (0 != (activityWeekDays & (1<<j)))
                {
                    strcat(textBuffer, "1");
                }
                else
                {
                    strcat(textBuffer, "0");
                }
                if (j < 6)
                {
                    strcat(textBuffer, ",");
                }
            }
            strcat(textBuffer, ";");
        }

        Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

        vPortFree(textBuffer);

    #ifdef FREERTOS_SUPPORT
        vTracePrintF(wifiEventChannel, "Sending Property \"ActivityWeekDays\" for Activity %d",*((uint8_t*)pData));
    #endif

        return (HAL_QWiFiMsg (newDataPoint));
    }
    return -1;
}

/*******************************************************************************
* @brief  Send the Daily Consumption to the cloud
* @inputs pData: void pointer to the day of the week
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/25
*******************************************************************************/
uint8_t SendDailyConsumption(void* pData, dbChangeSource_t source)
{
    char *textBuffer;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0026);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_DAILY_CONSUMPTION].propertyName));


    /* textBuffer is using malloc to avoid requesting ~150 bytes off the stack */
    textBuffer = allocate_text_buffer ((DAILY_SYSTEM_CONSUMPT_STRING_SIZE*7)+NULL_TERMINATION_SIZE);
    if (NULL != textBuffer)
    {
        dbType_DailyConsumption_t consumption;

        strcpy(textBuffer, "");


        for (uint8_t i = YESTERDAY; i < 8; i++)
        {
            DBTHRD_GetData(DBTYPE_DAILY_CONSUMPTION, &consumption, i);
            if ((consumption.MinimumTemperature != DB_SENSOR_INVALID) &&
                (consumption.MaximumTemperature != DB_SENSOR_INVALID) &&
                ((consumption.Date.Year > 2000) && (consumption.Date.Year < 2100)) &&
                (consumption.Date.Month <= MNTH_DEC) &&
                (consumption.Date.Day <= 31))
            {
                char dailyConsumptStr [DAILY_SYSTEM_CONSUMPT_STRING_SIZE];
                consumption.MinimumTemperature = TLS_Round(consumption.MinimumTemperature, 50);
                consumption.MaximumTemperature = TLS_Round(consumption.MaximumTemperature, 50);
                sprintf (dailyConsumptStr, "%d,%02d,%02d,%d,%.1f,%.1f;",
                            consumption.Date.Year,
                            consumption.Date.Month,
                            consumption.Date.Day,
                            consumption.Consumption,
                            (float)consumption.MinimumTemperature/100,
                            (float)consumption.MaximumTemperature/100);
                strcat(textBuffer, dailyConsumptStr);
            }
            else
            {
                strcat(textBuffer, ";");
            }
        }

        Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

        vPortFree (textBuffer);

#ifdef FREERTOS_SUPPORT
        vTracePrintF(wifiEventChannel, "Sending Property \"HomeDailyConsumption\" ");
#endif

        return (HAL_QWiFiMsg (newDataPoint));
    }
    return -1;
}

/*******************************************************************************
* @brief  Send the Monthly Consumption to the cloud
* @inputs pData: void pointer to the month of the year
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/28
*******************************************************************************/
uint8_t SendMonthlyConsumption(void* pData, dbChangeSource_t source)
{
    char *textBuffer;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0027);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_MONTHLY_CONSUMPTION].propertyName));


    /* textBuffer is using malloc to avoid requesting ~150 bytes off the stack */
    textBuffer = allocate_text_buffer ((MONTHLY_SYSTEM_CONSUMPT_STRING_SIZE*12)+NULL_TERMINATION_SIZE);
    if (NULL != textBuffer)
    {
        strcpy(textBuffer, "");

        for (uint8_t i = 0; i < 12; i++)
        {
            dbType_MonthlyConsumption_t consumption;
            DBTHRD_GetData(DBTYPE_MONTHLY_CONSUMPTION, &consumption, i);
            if (consumption.AverageTemperature != DB_SENSOR_INVALID)
            {
                char monthlyConsumptStr [MONTHLY_SYSTEM_CONSUMPT_STRING_SIZE];

                consumption.AverageTemperature = TLS_Round(consumption.AverageTemperature, 50);
                sprintf (monthlyConsumptStr, "%d,%.1f;",
                                consumption.Consumption,
                                (float)consumption.AverageTemperature/100);
                strcat(textBuffer, monthlyConsumptStr);
            }
            else
            {
                strcat(textBuffer, ";");
            }
        }

        Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

        vPortFree (textBuffer);

#ifdef FREERTOS_SUPPORT
        vTracePrintF(wifiEventChannel, "Sending Property \"HomeMonthlyConsumption\" ");
#endif

        return (HAL_QWiFiMsg (newDataPoint));
    }
    return -1;
}

/*******************************************************************************
* @brief  Send the Geofence Mobile State to the Cloud
* @inputs pData: pointer to user index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/12/20
*******************************************************************************/
uint8_t SendGeofencingMobileState(void* pData, dbChangeSource_t source)
{
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x002d);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_GEOFENCINGMOBILESTATE].propertyName));


    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(";"));

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Property \"MobileState\" for Mobile %d",*((uint8_t*)pData));
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Maestro Controller Language to the Cloud
* @inputs pData: pointer to the language
* @retval None
* @author Jean-Fran�ois Many
* @date 2018/09/12
*******************************************************************************/
uint8_t SendLanguage(void* pData, dbChangeSource_t source)
{
    uint8_t language_setting;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x002e);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_LANGUAGE].propertyName));

    DBTHRD_GetData(DBTYPE_LANGUAGE_SETTING, &language_setting, INDEX_DONT_CARE);
    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateIntegerTLV(language_setting));

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"Language\" ");
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Diagnostic report to the Cloud
* @inputs pData: pointer to the diagnostic buffer
* @retval None
* @author Jean-Fran�ois Many
* @date 2018/09/13
*******************************************************************************/
uint8_t SendDiagnostic(void* pData, dbChangeSource_t source)
{
    char *textBuffer;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x002f);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_DIAGNOSTIC].propertyName));


    /* textBuffer is using malloc to avoid requesting ~700 off the stack */
    textBuffer = allocate_text_buffer ((DIAGNOSTIC_STRING_SIZE+DELIMITER_SIZE)+NULL_TERMINATION_SIZE);
    if (NULL != textBuffer)
    {
        strcpy(textBuffer, "");

        DBTHRD_GetData(DBTYPE_DIAGNOSTIC, textBuffer, INDEX_DONT_CARE);

        Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

        vPortFree (textBuffer);

#ifdef FREERTOS_SUPPORT
        vTracePrintF(wifiEventChannel, "Sending Property \"Diagnostic\" ");
#endif

        return (HAL_QWiFiMsg (newDataPoint));
    }
    return -1;
}

/*******************************************************************************
* @brief  Send the ZigBee channel to the Cloud
* @inputs pData: pointer to the zigbee channel buffer
* @retval None
* @author Jean-Fran�ois Many
* @date 2018/10/05
*******************************************************************************/
uint8_t SendZigBeeChannel(void* pData, dbChangeSource_t source)
{
    dbType_ZigBeeNetworkInfo_t nwInfo;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0030);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_ZIGBEECHANNEL].propertyName));

    DBTHRD_GetData(DBTYPE_ZIGBEE_NETWORK_INFO,(void*)&nwInfo, INDEX_DONT_CARE);
    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateIntegerTLV(nwInfo.ActiveChannel));

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"ZigBeeChannel\" ");
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Debug Level report to the Cloud
* @inputs pData: pointer to the debug level buffer
* @retval None
* @author Jean-Fran�ois Many
* @date 2018/12/14
*******************************************************************************/
uint8_t SendDebugLevel(void* pData, dbChangeSource_t source)
{
    uint8_t debug_level;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0031);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_DEBUGLEVEL].propertyName));

    DBTHRD_GetData(DBTYPE_DEBUG_LEVEL,(void*)&debug_level, INDEX_DONT_CARE);
    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateIntegerTLV(debug_level));

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"DebugLevel\" ");
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Thermostat Daily Consumption to the Cloud
* @inputs pData: void pointer to the thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2019/03/07
*******************************************************************************/
uint8_t SendTstatDailyConsumption(void* pData, dbChangeSource_t source)
{
    uint8_t consumption;
    char textBuffer[(POWER_CONSUMPT_STRING_SIZE*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE];
    uint8_t i;

    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0032);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_TSTAT_DAILY_CONSUMPTION].propertyName));

    strcpy(textBuffer, "");
    for (i = 0; i < THERMOSTAT_INSTANCES; i++)
    {
        char consumptionStr[POWER_CONSUMPT_STRING_SIZE+NULL_TERMINATION_SIZE];

        DBTHRD_GetData(DBTYPE_LOCAL_DAILY_CONSUMPTION, &consumption, i);
        sprintf(consumptionStr, "%d;", consumption);
        strcat(textBuffer, consumptionStr);
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Property \"TstatDailyConsumption\" for TH%d",*((uint8_t*)pData));
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Floor Mode to the Cloud
* @inputs pData: void pointer to the thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2019/03/18
*******************************************************************************/
uint8_t SendFloorMode(void* pData, dbChangeSource_t source)
{
    char floorMode;
    char textBuffer[(FLOOR_MODE_STRING_SIZE*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE];
    uint8_t i;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0033);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_FLOORMODE].propertyName));

    strcpy(textBuffer, "");
    for (i = 0; i < THERMOSTAT_INSTANCES; i++)
    {
        char floorModeStr[FLOOR_MODE_STRING_SIZE+NULL_TERMINATION_SIZE];

        DBTHRD_GetData(DBTYPE_FLOOR_MODE, &floorMode, i);

        if (floorMode < 10)             //can display only one digit
        {
            sprintf (floorModeStr, "%d;", floorMode);
        }
        else
        {
            sprintf (floorModeStr, "0;");
        }
        strcat(textBuffer, floorModeStr);
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"FloorMode\" ");
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Relay Cycle Count to the Cloud
* @inputs pData: void pointer to the thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2019/03/19
*******************************************************************************/
uint8_t SendRelayCycleCount(void* pData, dbChangeSource_t source)
{
    char relayCycleCount;
    char textBuffer[(RELAY_CYCLE_COUNT_STRING_SIZE*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE];
    uint8_t i;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0034);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_RELAY_CYCLE_COUNT].propertyName));

    strcpy(textBuffer, "");
    for (i = 0; i < THERMOSTAT_INSTANCES; i++)
    {
        char relayCycleCountStr[RELAY_CYCLE_COUNT_STRING_SIZE+NULL_TERMINATION_SIZE];

        DBTHRD_GetData(DBTYPE_RELAY_CYCLE_COUNT, &relayCycleCount, i);

        sprintf (relayCycleCountStr, "%d;", relayCycleCount);
        strcat(textBuffer, relayCycleCountStr);
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"RelayCycleCount\" ");
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/*******************************************************************************
* @brief  Send the Floor Temperature to the Cloud
* @inputs pData: void pointer to the Floor Setpoint instance (index)
* @retval None
* @author Jean-Fran�ois Many
* @date 2019/03/20
*******************************************************************************/
uint8_t SendFloorTemperature(void* pData, dbChangeSource_t source)
{
    uint8_t i;
    char *textBuffer;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0035);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_FLOORTEMPERATURE].propertyName));

    /* textBuffer is using malloc to avoid requesting ~100 bytes off the stack */
    textBuffer = allocate_text_buffer((TEMPERATURE_STRING_SIZE*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE);

    if (NULL != textBuffer)
    {
        strcpy(textBuffer,"");
        for (i = 0; i < THERMOSTAT_INSTANCES; i++)
        {
            TEMPERATURE_C_t floor_temp;
            char floorTempStr [TEMPERATURE_STRING_SIZE+NULL_TERMINATION_SIZE];

            DBTHRD_GetData(DBTYPE_FLOOR_TEMPERATURE, &floor_temp, i);

            if ((floor_temp != DB_SENSOR_INVALID) && (floor_temp != DB_SENSOR_HIGH) && (floor_temp != DB_SENSOR_LOW))
            {
                floor_temp = TLS_Round(floor_temp, 50);
                sprintf(floorTempStr, "%.1f;", (float)floor_temp/100);
            }
            else
            {
                sprintf(floorTempStr, ";");
            }
            strcat(textBuffer, floorTempStr);
        }

        Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

        vPortFree(textBuffer);

#ifdef FREERTOS_SUPPORT
        vTracePrintF(wifiEventChannel, "Sending Property \"TstatFloorTemperature\" for TH%d",*((uint8_t*)pData));
#endif
        return (HAL_QWiFiMsg (newDataPoint));
    }
    return -1;
}

/*******************************************************************************
* @brief  Send the Floor Setpoint to the Cloud
* @inputs pData: void pointer to the Floor Setpoint instance (index)
* @retval None
* @author Jean-Fran�ois Many
* @date 2019/03/20
*******************************************************************************/
uint8_t SendFloorSetpoint(void* pData, dbChangeSource_t source)
{
    Setpoint100_t setpoint;
    uint8_t i;
    char textBuffer[(SETPOINT_STRING_SIZE*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x0036);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_FLOORSETPOINT].propertyName));

    strcpy(textBuffer,"");
    for (i = 0; i < THERMOSTAT_INSTANCES; i++)
    {
        DBTHRD_GetData(DBTYPE_FLOOR_SETPOINT, &setpoint, i);
        addStringSetpointToBuffer (textBuffer, setpoint, ";", "0");
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Property \"TstatFloorSetpoint\" for TH%d",*((uint8_t*)pData));
#endif

    return (HAL_QWiFiMsg (newDataPoint));
}

/******************************************************************************/
/* Send property response functions                                           */
/******************************************************************************/
/*******************************************************************************
* @brief  Send the Default Setpoint to the Cloud
* @inputs pData: void pointer to the Default Setpoint instance (don't care)
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/06
*******************************************************************************/
void SendDefaultSetpointResp(void* pData, uint16_t requestId)
{
    int16_t number;

    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);


#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"DefaultSetpoint\" ");
#endif

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_DEFAULTSETPOINT].propertyName));

    DBTHRD_GetData(DBTYPE_DEFAULT_SETPOINT, &number, INDEX_DONT_CARE);
    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateCentsTLV(number));

    HAL_QWiFiMsg (newDataPoint);
}

/*******************************************************************************
* @brief  Send the Home State to the Cloud
* @inputs pData: void pointer to the Home State instance (don't care)
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/05
*******************************************************************************/
void SendHomeStateResp(void* pData, uint16_t requestId)
{
    ModeEnum_t state;

    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);


#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"ResidenceState\" ");
#endif

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_HOMESTATE].propertyName));


    DBTHRD_GetData(DBTYPE_HOME_STATE, &state, INDEX_DONT_CARE);
    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateIntegerTLV (state));

    HAL_QWiFiMsg (newDataPoint);
}

/*******************************************************************************
* @brief  Send the Ambient Setpoint to the Cloud
* @inputs pData: void pointer to the Ambient Setpoint instance (index)
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/03
*******************************************************************************/
void SendAmbientSetpointResp(void* pData, uint16_t requestId)
{
    Setpoint100_t setpoint;
    uint8_t i;
    char textBuffer[(SETPOINT_STRING_SIZE*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_AMBIENTSETPOINT].propertyName));

    strcpy(textBuffer,"");
    for (i = 0; i < THERMOSTAT_INSTANCES; i++)
    {
        DBTHRD_GetData(DBTYPE_AMBIENT_SETPOINT, &setpoint, i);
        addStringSetpointToBuffer (textBuffer, setpoint, ";", "0");
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Property \"TstatAmbientSetpoint\" for TH%d",*((uint8_t*)pData));
#endif
}


/*******************************************************************************
* @brief  Send the Ambient Temperature to the Cloud
* @inputs pData: void pointer to the Ambient Temperature instance (index)
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/03
*******************************************************************************/
void SendAmbientTemperatureResp(void* pData, uint16_t requestId)
{
    uint8_t i;
    char *textBuffer;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_AMBIENTTEMPERATURE].propertyName));

    /* textBuffer is using malloc to avoid requesting ~100 bytes off the stack */
    textBuffer = allocate_text_buffer ((TEMPERATURE_STRING_SIZE*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE);

    if (NULL != textBuffer)
    {
        strcpy(textBuffer,"");
        for (i = 0; i < THERMOSTAT_INSTANCES; i++)
        {
            TEMPERATURE_C_t room_temp;
            char roomTempStr [TEMPERATURE_STRING_SIZE+NULL_TERMINATION_SIZE];

            DBTHRD_GetData(DBTYPE_AMBIENT_TEMPERATURE, &room_temp, i);

            if ((room_temp != DB_SENSOR_INVALID) && (room_temp != DB_SENSOR_HIGH) && (room_temp != DB_SENSOR_LOW))
            {
                room_temp = TLS_Round(room_temp, 50);
                sprintf(roomTempStr, "%.1f;", (float)room_temp/100);
            }
            else
            {
                sprintf(roomTempStr, ";");
            }
            strcat(textBuffer, roomTempStr);
        }

        Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

        HAL_QWiFiMsg (newDataPoint);

        vPortFree(textBuffer);

#ifdef FREERTOS_SUPPORT
        vTracePrintF(wifiEventChannel, "Sending Property \"TstatAmbientTemperature\" for TH%d",*((uint8_t*)pData));
#endif
    }
}

/*******************************************************************************
* @brief  Send the Active Alerts to the Cloud
* @inputs pData: void pointer to the thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/11
*******************************************************************************/
void SendActiveAlertsResp(void* pData, uint16_t requestId)
{
    uint8_t i, j;
    uint8_t alert_text[3];
    dbType_Alerts_t alert;
    char textBuffer [(ALERT_MAX_STRING_SIZE*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_ACTIVEALERTS].propertyName));



    strcpy(textBuffer, "");
    for (i = 0; i < THERMOSTAT_INSTANCES; i++)
    {
        DBTHRD_GetData(DBTYPE_ACTIVE_ALERTS, &alert, i);
        for (j = 0; j < ALERT_INSTANCES; j++)
        {
            if (alert.ActiveAlerts[j] <= 9)
            {
                alert_text[0] = alert.ActiveAlerts[j] + 0x30;
                alert_text[1] = 0;
            }
            else
            {
                alert_text[0] = (alert.ActiveAlerts[j] / 10) + 0x30;
                alert_text[1] = (alert.ActiveAlerts[j] % 10) + 0x30;
                alert_text[2] = 0;
            }
            //Check if there is an active alert
            if (alert_text[0] != '0')
            {
                strncat(textBuffer, (char const*)alert_text, strlen((char const*)alert_text));
                if (j != (ALERT_INSTANCES - 1))
                {
                    if (alert.ActiveAlerts[j+1] != NULL)
                    {
                        strcat(textBuffer, ",");
                    }
                }
            }
            else
            {
                //Check if this thermostat instance has no alert at all
                if (j == 0)
                {
                    //If this thermostat instance has no active alert, set the alert value to 0
                    strncat(textBuffer, (char const*)&alert_text[0], 1);
                    break;
                }
            }
        }
        strcat(textBuffer, ";");
    }


    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Property \"TstatActiveAlerts\" for TH%d",*((uint8_t*)pData));
#endif
}

/*******************************************************************************
* @brief  Send the Ambient Humidity to the Cloud
* @inputs pData: void pointer to the Ambient Humidity instance (index)
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/12
*******************************************************************************/
void SendAmbientHumidityResp(void* pData, uint16_t requestId)
{
    char textBuffer[(HUMIDITY_STRING_SIZE*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE];
    uint8_t i;

    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_AMBIENTHUMIDITY].propertyName));

    strcpy(textBuffer, "");
    for (i = 0; i < THERMOSTAT_INSTANCES; i++)
    {
        RelativeHumidity_t roomHumidity;
        char roomHumidityStr [HUMIDITY_STRING_SIZE+NULL_TERMINATION_SIZE];

        DBTHRD_GetData(DBTYPE_AMBIENT_HUMIDITY, &roomHumidity, i);
        if (roomHumidity != DB_HUMIDITY_INVALID)
        {
            roomHumidity = TLS_Round((int16_t)roomHumidity,5);
            sprintf (roomHumidityStr, "%d;", roomHumidity);
        }
        else
        {
            sprintf (roomHumidityStr, "0;");
        }

        strcat(textBuffer,roomHumidityStr);
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Property \"TstatAmbientHumidity\" for TH%d",*((uint8_t*)pData));
#endif
}

/*******************************************************************************
* @brief  Send the Temperature Format to the Cloud
* @inputs pData: void pointer to the Temperature Format data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
void SendTemperatureFormatResp(void* pData, uint16_t requestId)
{
    uint8_t tempFormat;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_TEMPERATUREFORMAT].propertyName));

    DBTHRD_GetData(DBTYPE_TEMPERATURE_FORMAT, &tempFormat, INDEX_DONT_CARE);
    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateIntegerTLV(tempFormat));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"TemperatureFormat\" ");
#endif

}

/*******************************************************************************
* @brief  Send the Time Format to the Cloud
* @inputs pData: void pointer to the Time Format data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
void SendTimeFormatResp(void* pData, uint16_t requestId)
{
    uint8_t timeFormat;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_TIMEFORMAT].propertyName));

    DBTHRD_GetData(DBTYPE_TIME_FORMAT, &timeFormat, INDEX_DONT_CARE);
    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateIntegerTLV(timeFormat));

    HAL_QWiFiMsg (newDataPoint);


#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"TimeFormat\" ");
#endif
}

/*******************************************************************************
* @brief  Send the Lock State to the Cloud
* @inputs pData: void pointer to the thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
void SendLockStateResp(void* pData, uint16_t requestId)
{
    char lockState;
    char textBuffer[(LOCK_STATE_STRING_SIZE*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE];
    uint8_t i;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_LOCKSTATE].propertyName));

    strcpy(textBuffer, "");
    for (i = 0; i < THERMOSTAT_INSTANCES; i++)
    {
        char lockStateStr[LOCK_STATE_STRING_SIZE+NULL_TERMINATION_SIZE];

        DBTHRD_GetData(DBTYPE_LOCK_STATE, &lockState, i);

        if (lockState < 10)             //can display only one digit
        {
            sprintf (lockStateStr, "%d;", lockState);
        }
        else
        {
            sprintf (lockStateStr, "0;");
        }
        strcat(textBuffer, lockStateStr);
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"LockState\" ");
#endif
}

/*******************************************************************************
* @brief  Send the Lock Delta to the Cloud
* @inputs pData: void pointer to the thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
void SendLockDeltaResp(void* pData, uint16_t requestId)
{
    uint8_t lockDelta;
    char textBuffer[(LOCK_DELTA_STRING_SIZE*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE];
    uint8_t i;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_LOCKDELTA].propertyName));

    strcpy(textBuffer, "");
    for (i = 0; i < THERMOSTAT_INSTANCES; i++)
    {
        char lockDeltaStr[3];

        DBTHRD_GetData(DBTYPE_LOCK_SETPOINT_LIMIT, &lockDelta, i);

        if (lockDelta < 10)             //can display only 1 digit
        {
            sprintf(lockDeltaStr, "%d;", lockDelta);
        }
        else
        {
            sprintf(lockDeltaStr, "0;");
        }
        strcat(textBuffer, lockDeltaStr);
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"TstatLockDelta\" ");
#endif
}

/*******************************************************************************
* @brief  Send the Home State Changed By Geofencing indicator to the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
void SendHomeStateChangedByGeofencingResp(void* pData, uint16_t requestId)
{
    uint8_t geofencingChange;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_HOMESTATECHANGEDBYGEOFENCING].propertyName));

    /********************************************************/
    /*              PropertyValue                           */
    /********************************************************/
    DBTHRD_GetData(DBTYPE_GEOFENCING_CHANGE, &geofencingChange, INDEX_DONT_CARE);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                                Ayla_CreateIntegerTLV(geofencingChange));
    //FormatAylaPacket
    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"StateChangedByGeofencing\" ");
#endif
}

/*******************************************************************************
* @brief  Send the Heat Demand to the Cloud
* @inputs pData: void pointer to the thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
extern void SendHeatDemandResp(void* pData, uint16_t requestId)
{
    uint8_t heatDemand;
    char textBuffer[(HEAT_DEMAND_STRING_SIZE*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE];
    uint8_t i;

    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_HEATDEMAND].propertyName));

    strcpy(textBuffer, "");
    for (i = 0; i < THERMOSTAT_INSTANCES; i++)
    {
        char heatDemandStr[HEAT_DEMAND_STRING_SIZE+NULL_TERMINATION_SIZE];

        DBTHRD_GetData(DBTYPE_HEAT_DEMAND, &heatDemand, i);
        sprintf(heatDemandStr, "%d;", heatDemand);
        strcat(textBuffer, heatDemandStr);
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Property \"TstatHeatDemand\" for TH%d",*((uint8_t*)pData));
#endif
}

/*******************************************************************************
* @brief  Send the Controller Version to the Cloud
* @inputs pData: void pointer to index (don't care)
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/20
*******************************************************************************/
void SendControllerVersionResp(void* pData, uint16_t requestId)
{
    char textBuffer[SW_VERSION_STRING_SIZE+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_CONTROLLERVERSION].propertyName));


    sprintf (textBuffer, "%05.2f%s",(float)SW_VER/100, BUILD_INFO);

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"ControlerSwVersion\" ");
#endif
}

/*******************************************************************************
* @brief  Send the oem Host Version to the Cloud
* @inputs pData: void pointer to index (don't care)
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/20
*******************************************************************************/
void SendOemHostVersionResp(void* pData, uint16_t requestId)
{
    char textBuffer[OEM_VERSION_STRING_SIZE+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_OEMHOSTVERSION].propertyName));


    sprintf (textBuffer, "%.1f",(float)OEM_HOST_VERSION/10);

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"oem_host_version\" ");
#endif
}

/*******************************************************************************
* @brief  Send the ZigBee Association Permit flag to the Cloud
* @inputs pData: void pointer to index (don't care)
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/20
*******************************************************************************/
void SendZigBeeAssociationPermitResp(void* pData, uint16_t requestId)
{
    uint8_t zap;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_ZIGBEEASSOCIATIONPERMIT].propertyName));

    DBTHRD_GetData(DBTYPE_ZIGBEE_ASSOCIATION_PERMIT, &zap, INDEX_DONT_CARE);
    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateIntegerTLV(zap));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"ZigBeeAssociationPermit\" ");
#endif
}

/*******************************************************************************
* @brief  Send the Thermostat Name to the Cloud
* @inputs pData: void pointer to the thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/24
*******************************************************************************/
void SendThermostatNameResp(void* pData, uint16_t requestId)
{
    char *textBuffer;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_THERMOSTATNAME].propertyName));


    /* textBuffer is using malloc to avoid requesting ~700 off the stack */
    textBuffer = allocate_text_buffer (((THERMOSTAT_NAME_STRING_SIZE+DELIMITER_SIZE)*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE);
    if (NULL != textBuffer)
    {
        strcpy(textBuffer, "");

        for (uint8_t i = 0; i < THERMOSTAT_INSTANCES; i++)
        {
			char longname[THERMOSTAT_NAME_STRING_SIZE];

            DBTHRD_GetData(DBTYPE_TSTAT_NAME_UTF8, &longname, i);
            strcat(textBuffer, longname);
            strcat(textBuffer, ";");
        }

        Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

        vPortFree (textBuffer);
        HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
        vTracePrintF(wifiEventChannel, "Sending Property \"TstatName\" for TH%d",*((uint8_t*)pData));
#endif
    }
}

/*******************************************************************************
* @brief  Send the Geofence Enable bit to the Cloud
* @inputs pData: don't care
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/24
*******************************************************************************/
void SendGeofenceEnableResp(void* pData, uint16_t requestId)
{
    uint8_t state;

    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_GEOFENCINGENABLE].propertyName));

    DBTHRD_GetData(DBTYPE_GEOFENCING_ENABLE, &state, INDEX_DONT_CARE);
    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateIntegerTLV(state));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"GeofencingOnOff\" ");
#endif

}

/*******************************************************************************
* @brief  Send the Geofence Mobile Id to the Cloud
* @inputs pData: pointer to user index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/24
*******************************************************************************/
void SendGeofencingMobileIdResp(void* pData, uint16_t requestId)
{
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_GEOFENCINGMOBILEID].propertyName));


    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(";"));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Property \"MobileID\" for Mobile %d",*((uint8_t*)pData));
#endif
}

/*******************************************************************************
* @brief  Send the Group Names to the Cloud
* @inputs pData: pointer to user index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/26
*******************************************************************************/
void SendGroupNameResp(void* pData, uint16_t requestId)
{
    char *textBuffer;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_GROUPNAME].propertyName));


    /* textBuffer is using malloc to avoid requesting ~300 off the stack */
    textBuffer = allocate_text_buffer (((GROUP_NAME_STRING_SIZE+DELIMITER_SIZE)*GROUP_INSTANCES)+NULL_TERMINATION_SIZE);
    if (NULL != textBuffer)
    {
        strcpy(textBuffer, "");
        for (uint8_t i = 0; i < GROUP_INSTANCES; i++)
        {
			char longname[GROUP_NAME_STRING_SIZE];

            DBTHRD_GetData(DBTYPE_GROUP_NAME_UTF8, &longname, i);
            strcat(textBuffer, longname);
            strcat(textBuffer, ";");
        }

        Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

        vPortFree (textBuffer);
        HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
        vTracePrintF(wifiEventChannel, "Sending Property \"GroupName\" for Group %d",*((uint8_t*)pData));
#endif
    }
}

/*******************************************************************************
* @brief  Send the Group Members to the Cloud
* @inputs pData: pointer to user index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/26
*******************************************************************************/
void SendGroupMembersResp(void* pData, uint16_t requestId)
{
    uint8_t i, j;
    dbType_GroupMembers_t groupMembers;

    /* GROUP_INSTANCES is needed in the calculation to add ";" character */
    char textBuffer[(GROUP_MEMBER_STRING_SIZE*THERMOSTAT_INSTANCES)+GROUP_INSTANCES+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_GROUPMEMBERS].propertyName));

    strcpy(textBuffer, "");
    for (i = 0; i < GROUP_INSTANCES; i++)
    {
        DBTHRD_GetData(DBTYPE_GROUP_MEMBERS, &groupMembers, i);
        //Check if this group instance has members
        if (groupMembers.MembersList != 0)
        {
            for (j = 0; j < THERMOSTAT_INSTANCES; j++)
            {
                if (0 != (groupMembers.MembersList & (1<<j)))
                {
                    char memberString[GROUP_MEMBER_STRING_SIZE+NULL_TERMINATION_SIZE];

                    groupMembers.MembersList &= ~(1<<j);
                    if (groupMembers.MembersList != 0)
                    {
                        sprintf (memberString, "%d,", j+1);
                    }
                    else
                    {
                        sprintf (memberString, "%d", j+1);
                    }
                    strcat(textBuffer, memberString);
                }
            }
        }
        else
        {
            //This group instance has no member, do not set any value
        }
        strcat(textBuffer, ";");
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

    HAL_QWiFiMsg (newDataPoint);


#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Property \"GroupMembers\" for Group %d",*((uint8_t*)pData));
#endif
}

/*******************************************************************************
* @brief  Send the Group Setpoint to the Cloud
* @inputs pData: pointer to user index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/26
*******************************************************************************/
void SendGroupSetpointResp(void* pData, uint16_t requestId)
{
    char textBuffer[(SETPOINT_STRING_SIZE*GROUP_INSTANCES)+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_GROUPSETPOINT].propertyName));


    strcpy(textBuffer, "");
    for (uint8_t i = 0; i < GROUP_INSTANCES; i++)
    {
		Setpoint100_t setpoint;

        DBTHRD_GetData(DBTYPE_GROUP_SETPOINT, &setpoint, i);
        addStringSetpointToBuffer (textBuffer, setpoint, ";", "0");
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Property \"GroupSetpoint\" for Group %d",*((uint8_t*)pData));
#endif
}

/*******************************************************************************
* @brief  Send the Group Away Setpoint to the Cloud
* @inputs pData: pointer to user index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/26
*******************************************************************************/
void SendGroupAwaySetpointResp(void* pData, uint16_t requestId)
{
    char textBuffer[(SETPOINT_STRING_SIZE*GROUP_INSTANCES)+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_GROUPAWAYSETPOINT].propertyName));


    strcpy(textBuffer, "");
    for (uint8_t i = 0; i < GROUP_INSTANCES; i++)
    {
		Setpoint100_t setpoint;

        DBTHRD_GetData(DBTYPE_GROUP_AWAY_SETPOINT, &setpoint, i);
        addStringSetpointToBuffer (textBuffer, setpoint, ";", "0");
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Property \"GroupAwaySetpoint\" for Group %d",*((uint8_t*)pData));
#endif
}

/*******************************************************************************
* @brief  Send the Group Vacation Setpoint to the Cloud
* @inputs pData: pointer to user index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/26
*******************************************************************************/
void SendGroupVacationSetpointResp(void* pData, uint16_t requestId)
{
    char textBuffer[(SETPOINT_STRING_SIZE*GROUP_INSTANCES)+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_GROUPVACATIONSETPOINT].propertyName));


    strcpy(textBuffer, "");
    for (uint8_t i = 0; i < GROUP_INSTANCES; i++)
    {
		Setpoint100_t setpoint;

        DBTHRD_GetData(DBTYPE_GROUP_VACATION_SETPOINT, &setpoint, i);
        addStringSetpointToBuffer (textBuffer, setpoint, ";", "0");
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Prop. \"GroupVacationSetpoint\" for Group %d",*((uint8_t*)pData));
#endif
}

/*******************************************************************************
* @brief  Send the Group Standby Setpoint to the Cloud
* @inputs pData: pointer to user index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/26
*******************************************************************************/
void SendGroupStandbySetpointResp(void* pData, uint16_t requestId)
{
    char textBuffer[(SETPOINT_STRING_SIZE*GROUP_INSTANCES)+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_GROUPSTANDBYSETPOINT].propertyName));


    strcpy(textBuffer, "");
    for (uint8_t i = 0; i < GROUP_INSTANCES; i++)
    {
		Setpoint100_t setpoint;

        DBTHRD_GetData(DBTYPE_GROUP_STANDBY_SETPOINT, &setpoint, i);
        addStringSetpointToBuffer (textBuffer, setpoint, ";", "0");
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Prop. \"GroupStandbySetpoint\" for Group %d",*((uint8_t*)pData));
#endif
}

/*******************************************************************************
* @brief  Send the Thermostat Model to the Cloud
* @inputs pData: void pointer to the thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/03
*******************************************************************************/
void SendThermostatModelResp(void* pData, uint16_t requestId)
{
    char *textBuffer;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_THERMOSTATMODEL].propertyName));


    /* textBuffer is using malloc to avoid requesting ~700 off the stack */
    textBuffer = allocate_text_buffer (((THERMOSTAT_MODEL_STRING_SIZE+DELIMITER_SIZE)*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE);
    if (NULL != textBuffer)
    {
        strcpy(textBuffer, "");

        for (uint8_t i = 0; i < THERMOSTAT_INSTANCES; i++)
        {
            /* longname buffer must be long enough to contains THERMOSTAT_MODEL_STRING_SIZE */
            /*  of UTF-8 characters                                                         */
            char longname[(THERMOSTAT_MODEL_STRING_SIZE*2)+NULL_TERMINATION_SIZE];

            DBTHRD_GetData(DBTYPE_TSTAT_MODEL_UTF8, &longname, i);

            /* must not print more than THERMOSTAT_MODEL_STRING_SIZE characters, otherwise  */
            /*  it will not fit in an Ayla Data point                                       */
            /*  Ayla datapoint max = 1024 bytes (512 if all UTF-8 special characters)       */
            strncat(textBuffer, longname, THERMOSTAT_MODEL_STRING_SIZE);
            strcat(textBuffer, ";");
        }

        Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

        vPortFree (textBuffer);
        HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
        vTracePrintF(wifiEventChannel, "Sending Property \"TstatModelNumber\" for TH%d",*((uint8_t*)pData));
#endif
    }
}

/*******************************************************************************
* @brief  Send the Activity Start Request to the cloud
* @inputs pData: void pointer to the activity instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/17
*******************************************************************************/
void SendActivityStartRequestedResp(void* pData, uint16_t requestId)
{
    char textBuffer[(ACTIVITY_START_STRING_SIZE*ACTIVITY_INSTANCES)+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_ACTIVITYSTARTREQUESTED].propertyName));

    //Always return cleared activity request back to the cloud to avoid having more than more request at a time
    strcpy(textBuffer, "0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;");

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"ActivityStartRequested\" ");
#endif
}

/*******************************************************************************
* @brief  Send the Activity Names to the Cloud
* @inputs pData: pointer to activity index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/12/05
*******************************************************************************/
void SendActivityNameResp(void* pData, uint16_t requestId)
{
    char *textBuffer;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_ACTIVITYNAME].propertyName));


    /* textBuffer is using malloc to avoid requesting ~700 off the stack */
    textBuffer = allocate_text_buffer (((ACTIVITY_NAME_STRING_SIZE+DELIMITER_SIZE)*ACTIVITY_INSTANCES)+NULL_TERMINATION_SIZE);
    if (NULL != textBuffer)
    {
        strcpy(textBuffer, "");
        for (uint8_t i = 0; i < ACTIVITY_INSTANCES; i++)
        {
			char longname[ACTIVITY_NAME_STRING_SIZE];

            DBTHRD_GetData(DBTYPE_ACTIVITY_NAME_UTF8, &longname, i);
            strcat(textBuffer, longname);
            strcat(textBuffer, ";");
        }

        Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

        vPortFree (textBuffer);
        HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
        vTracePrintF(wifiEventChannel, "Sending Property \"ActivityName\" for Activity %d",*((uint8_t*)pData));
#endif
    }
}

/*******************************************************************************
* @brief  Send the Activity Ready At setting to the Cloud
* @inputs pData: pointer to activity index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/12/05
*******************************************************************************/
void SendActivityReadyAtResp(void* pData, uint16_t requestId)
{
    char textBuffer[(ACTIVITY_READYAT_STRING_SIZE*ACTIVITY_INSTANCES)+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_ACTIVITYREADYAT].propertyName));


    strcpy(textBuffer, "");

    for (uint8_t i = 0; i < ACTIVITY_INSTANCES; i++)
    {
        uint8_t readyState;
        char readyStateStr[ACTIVITY_READYAT_STRING_SIZE+NULL_TERMINATION_SIZE];

        DBTHRD_GetData(DBTYPE_ACTIVITY_READY_AT, &readyState, i);
        if (readyState < 9)           //must not print more than 1 digit
        {
            sprintf (readyStateStr, "%d;", readyState);
        }
        else
        {
            sprintf (readyStateStr, "0;");
        }
        strcat(textBuffer, readyStateStr);
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"ActivityReadyAt\" ");
#endif
}

/*******************************************************************************
* @brief  Send the Activity Setpoint per Group to the Cloud
* @inputs pData: pointer to activity index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/12/05
*******************************************************************************/
void SendActivitySetpointPerGroupResp(void* pData, uint16_t requestId)
{
    char *textBuffer;
    aylaDataPoint_t * newDataPoint;


    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_ACTIVITYSETPOINTPERGROUP].propertyName));


    /* textBuffer is using malloc to avoid requesting ~800 off the stack */
    textBuffer = allocate_text_buffer((SETPOINT_STRING_SIZE*GROUP_INSTANCES*ACTIVITY_INSTANCES)+NULL_TERMINATION_SIZE);

    if (NULL != textBuffer)
    {
        strcpy(textBuffer, "");
        for (uint8_t i = 0; i < ACTIVITY_INSTANCES; i++)
        {
            char setpointStr[SETPOINT_STRING_SIZE*GROUP_INSTANCES] = "";
            Setpoint100_t setpoints[GROUP_INSTANCES];

            DBTHRD_GetData(DBTYPE_ACTIVITY_SETPOINT_PER_GROUP, &setpoints, i);

            for (uint8_t j = 0; j < GROUP_INSTANCES-1; j++)
            {
                addStringSetpointToBuffer (setpointStr, setpoints[j], ",", NULL);
            }

            addStringSetpointToBuffer (setpointStr, setpoints[GROUP_INSTANCES-1], ";", NULL);
            strcat(textBuffer, setpointStr);
        }

        Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

        HAL_QWiFiMsg (newDataPoint);
        vPortFree (textBuffer);

    #ifdef FREERTOS_SUPPORT
        vTracePrintF(wifiEventChannel, "Sending Property \"ActivitySetpointPerGroup\" for Activity %d",*((uint8_t*)pData));
    #endif
    }
}

/*******************************************************************************
* @brief  Send the Activity Start Time to the Cloud
* @inputs pData: pointer to activity index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/12/05
*******************************************************************************/
void SendActivityStartTimeResp(void* pData, uint16_t requestId)
{
    char textBuffer[(ACTIVITY_STARTTIME_STRING_SIZE*ACTIVITY_INSTANCES)+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_ACTIVITYSTARTTIME].propertyName));


    strcpy(textBuffer, "");

    for (uint8_t i = 0; i < ACTIVITY_INSTANCES; i++)
    {
        char activityStartTimeStr[ACTIVITY_STARTTIME_STRING_SIZE+NULL_TERMINATION_SIZE];
        uint16_t activityStartTime;

        DBTHRD_GetData(DBTYPE_ACTIVITY_START_TIME, &activityStartTime, i);
        if ((activityStartTime != (uint16_t)ON_DEMAND) && (activityStartTime < 1440))
        {
            sprintf (activityStartTimeStr, "%d;", activityStartTime);
        }
        else
        {
            sprintf (activityStartTimeStr, "-1;");
        }
        strcat(textBuffer, activityStartTimeStr);
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Property \"ActivityStartTime\" for Activity %d",*((uint8_t*)pData));
#endif
}

/*******************************************************************************
* @brief  Send the Activity Week Days to the Cloud
* @inputs pData: pointer to activity index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/12/05
*******************************************************************************/
void SendActiviyWeekDaysResp(void* pData, uint16_t requestId)
{
    char *textBuffer;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_ACTIVITYWEEKDAYS].propertyName));


    /* textBuffer is using malloc to avoid requesting ~300 off the stack */
    textBuffer = allocate_text_buffer ((ACTIVITY_WEEKDAYS_STRING_SIZE*7*ACTIVITY_INSTANCES)+NULL_TERMINATION_SIZE);

    if (NULL != textBuffer)
    {
        strcpy(textBuffer, "");

        for (uint8_t i = 0; i < ACTIVITY_INSTANCES; i++)
        {
            uint8_t activityWeekDays;

            DBTHRD_GetData(DBTYPE_ACTIVITY_WEEK_DAYS, &activityWeekDays, i);
            for (uint8_t j = 0; j < 7; j++)
            {
                //Check if the week day is active
                if (0 != (activityWeekDays & (1<<j)))
                {
                    strcat(textBuffer, "1");
                }
                else
                {
                    strcat(textBuffer, "0");
                }
                if (j < 6)
                {
                    strcat(textBuffer, ",");
                }
            }
            strcat(textBuffer, ";");
        }

        Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

        vPortFree(textBuffer);

        HAL_QWiFiMsg (newDataPoint);

    #ifdef FREERTOS_SUPPORT
        vTracePrintF(wifiEventChannel, "Sending Property \"ActivityWeekDays\" for Activity %d",*((uint8_t*)pData));
    #endif
    }
}

/*******************************************************************************
* @brief  Send the Daily Consumption to the cloud
* @inputs pData: void pointer to the day of the week
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/25
*******************************************************************************/
void SendDailyConsumptionResp(void* pData, uint16_t requestId)
{
    char *textBuffer;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_DAILY_CONSUMPTION].propertyName));


    /* textBuffer is using malloc to avoid requesting ~150 bytes off the stack */
    textBuffer = allocate_text_buffer ((DAILY_SYSTEM_CONSUMPT_STRING_SIZE*7)+NULL_TERMINATION_SIZE);
    if (NULL != textBuffer)
    {
        dbType_DailyConsumption_t consumption;

        strcpy(textBuffer, "");


        for (uint8_t i = YESTERDAY; i < 8; i++)
        {
            DBTHRD_GetData(DBTYPE_DAILY_CONSUMPTION, &consumption, i);
            if ((consumption.MinimumTemperature != DB_SENSOR_INVALID) &&
                (consumption.MaximumTemperature != DB_SENSOR_INVALID) &&
                ((consumption.Date.Year > 2000) && (consumption.Date.Year < 2100)) &&
                (consumption.Date.Month <= MNTH_DEC) &&
                (consumption.Date.Day <= 31))
            {
                char dailyConsumptStr [DAILY_SYSTEM_CONSUMPT_STRING_SIZE];
                consumption.MinimumTemperature = TLS_Round(consumption.MinimumTemperature, 50);
                consumption.MaximumTemperature = TLS_Round(consumption.MaximumTemperature, 50);
                sprintf (dailyConsumptStr, "%d,%02d,%02d,%d,%.1f,%.1f;",
                            consumption.Date.Year,
                            consumption.Date.Month,
                            consumption.Date.Day,
                            consumption.Consumption,
                            (float)consumption.MinimumTemperature/100,
                            (float)consumption.MaximumTemperature/100);
                strcat(textBuffer, dailyConsumptStr);
            }
            else
            {
                strcat(textBuffer, ";");
            }
        }

        Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

        vPortFree (textBuffer);
        HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
        vTracePrintF(wifiEventChannel, "Sending Property \"HomeDailyConsumption\" ");
#endif
    }
}

/*******************************************************************************
* @brief  Send the Monthly Consumption to the cloud
* @inputs pData: void pointer to the month of the year
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/28
*******************************************************************************/
void SendMonthlyConsumptionResp(void* pData, uint16_t requestId)
{
    char *textBuffer;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_MONTHLY_CONSUMPTION].propertyName));


    /* textBuffer is using malloc to avoid requesting ~150 bytes off the stack */
    textBuffer = allocate_text_buffer ((MONTHLY_SYSTEM_CONSUMPT_STRING_SIZE*12)+NULL_TERMINATION_SIZE);
    if (NULL != textBuffer)
    {
        strcpy(textBuffer, "");

        for (uint8_t i = 0; i < 12; i++)
        {
            dbType_MonthlyConsumption_t consumption;
            DBTHRD_GetData(DBTYPE_MONTHLY_CONSUMPTION, &consumption, i);
            if (consumption.AverageTemperature != DB_SENSOR_INVALID)
            {
                char monthlyConsumptStr [MONTHLY_SYSTEM_CONSUMPT_STRING_SIZE];

                consumption.AverageTemperature = TLS_Round(consumption.AverageTemperature, 50);
                sprintf (monthlyConsumptStr, "%d,%.1f;",
                                consumption.Consumption,
                                (float)consumption.AverageTemperature/100);
                strcat(textBuffer, monthlyConsumptStr);
            }
            else
            {
                strcat(textBuffer, ";");
            }
        }

        Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

        vPortFree (textBuffer);

        HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
        vTracePrintF(wifiEventChannel, "Sending Property \"HomeMonthlyConsumption\" ");
#endif
    }
}

/*******************************************************************************
* @brief  Send the Geofence Mobile State to the Cloud
* @inputs pData: pointer to user index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/12/20
*******************************************************************************/
void SendGeofencingMobileStateResp(void* pData, uint16_t requestId)
{
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_GEOFENCINGMOBILESTATE].propertyName));


    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(";"));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Property \"MobileState\" for Mobile %d",*((uint8_t*)pData));
#endif
}

/*******************************************************************************
* @brief  Send the Maestro Controller Language to the Cloud
* @inputs pData: pointer to the language
* @retval None
* @author Jean-Fran�ois Many
* @date 2018/09/12
*******************************************************************************/
void SendLanguageResp(void* pData, uint16_t requestId)
{
    uint8_t language_setting;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_LANGUAGE].propertyName));

    DBTHRD_GetData(DBTYPE_LANGUAGE_SETTING, &language_setting, INDEX_DONT_CARE);
    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateIntegerTLV(language_setting));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"Language\" ");
#endif
}

/*******************************************************************************
* @brief  Send the Diagnostic report to the Cloud
* @inputs pData: pointer to the diagnostic buffer
* @retval None
* @author Jean-Fran�ois Many
* @date 2018/09/13
*******************************************************************************/
void SendDiagnosticResp(void* pData, uint16_t requestId)
{
    char *textBuffer;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_DIAGNOSTIC].propertyName));


    /* textBuffer is using malloc to avoid requesting ~700 off the stack */
    textBuffer = allocate_text_buffer ((DIAGNOSTIC_STRING_SIZE+DELIMITER_SIZE)+NULL_TERMINATION_SIZE);
    if (NULL != textBuffer)
    {
        strcpy(textBuffer, "");

        DBTHRD_GetData(DBTYPE_DIAGNOSTIC, textBuffer, INDEX_DONT_CARE);

        Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

        vPortFree (textBuffer);
        HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
        vTracePrintF(wifiEventChannel, "Sending Property \"Diagnostic\" ");
#endif
    }
}

/*******************************************************************************
* @brief  Send the Debug Level report to the Cloud
* @inputs pData: pointer to the debug level buffer
* @retval None
* @author Jean-Fran�ois Many
* @date 2018/12/14
*******************************************************************************/
void SendDebugLevelResp(void* pData, uint16_t requestId)
{
    uint8_t debug_level;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_DEBUGLEVEL].propertyName));

    DBTHRD_GetData(DBTYPE_DEBUG_LEVEL,(void*)&debug_level, INDEX_DONT_CARE);
    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateIntegerTLV(debug_level));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"DebugLevel\" ");
#endif
}

/*******************************************************************************
* @brief  Send the ZigBee Channel to the Cloud
* @inputs pData: pointer to the zigbee channel buffer
* @retval None
* @author Jean-Fran�ois Many
* @date 2018/10/05
*******************************************************************************/
void SendZigBeeChannelResp(void* pData, uint16_t requestId)
{
    dbType_ZigBeeNetworkInfo_t nwInfo;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_ZIGBEECHANNEL].propertyName));

    DBTHRD_GetData(DBTYPE_ZIGBEE_NETWORK_INFO,(void*)&nwInfo, INDEX_DONT_CARE);
    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateIntegerTLV(nwInfo.ActiveChannel));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"ZigBeeChannel\" ");
#endif
}

/*******************************************************************************
* @brief  Send the Thermostat Daily Consumption to the Cloud
* @inputs pData: void pointer to the thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2019/03/07
*******************************************************************************/
void SendTstatDailyConsumptionResp(void* pData, uint16_t requestId)
{
    uint8_t consumption;
    char textBuffer[(POWER_CONSUMPT_STRING_SIZE*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE];
    uint8_t i;

    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_TSTAT_DAILY_CONSUMPTION].propertyName));

    strcpy(textBuffer, "");
    for (i = 0; i < THERMOSTAT_INSTANCES; i++)
    {
        char consumptionStr[POWER_CONSUMPT_STRING_SIZE+NULL_TERMINATION_SIZE];

        DBTHRD_GetData(DBTYPE_LOCAL_DAILY_CONSUMPTION, &consumption, i);
        sprintf(consumptionStr, "%d;", consumption);
        strcat(textBuffer, consumptionStr);
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Property \"TstatDailyConsumption\" for TH%d",*((uint8_t*)pData));
#endif
}

/*******************************************************************************
* @brief  Send the Floor Mode to the Cloud
* @inputs pData: void pointer to the thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2019/03/18
*******************************************************************************/
void SendFloorModeResp(void* pData, uint16_t requestId)
{
    char floorMode;
    char textBuffer[(FLOOR_MODE_STRING_SIZE*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE];
    uint8_t i;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_FLOORMODE].propertyName));

    strcpy(textBuffer, "");
    for (i = 0; i < THERMOSTAT_INSTANCES; i++)
    {
        char floorModeStr[FLOOR_MODE_STRING_SIZE+NULL_TERMINATION_SIZE];

        DBTHRD_GetData(DBTYPE_LOCK_STATE, &floorMode, i);

        if (floorMode < 10)             //can display only one digit
        {
            sprintf (floorModeStr, "%d;", floorMode);
        }
        else
        {
            sprintf (floorModeStr, "0;");
        }
        strcat(textBuffer, floorModeStr);
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"FloorMode\" ");
#endif
}

/*******************************************************************************
* @brief  Send the Relay Cycle Count to the Cloud
* @inputs pData: void pointer to the thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2019/03/19
*******************************************************************************/
void SendRelayCycleCountResp(void* pData, uint16_t requestId)
{
    char relayCycleCount;
    char textBuffer[(RELAY_CYCLE_COUNT_STRING_SIZE*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE];
    uint8_t i;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_RELAY_CYCLE_COUNT].propertyName));

    strcpy(textBuffer, "");
    for (i = 0; i < THERMOSTAT_INSTANCES; i++)
    {
        char relayCycleCountStr[RELAY_CYCLE_COUNT_STRING_SIZE+NULL_TERMINATION_SIZE];

        DBTHRD_GetData(DBTYPE_RELAY_CYCLE_COUNT, &relayCycleCount, i);

        sprintf (relayCycleCountStr, "%d;", relayCycleCount);
        strcat(textBuffer, relayCycleCountStr);
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Sending Property \"RelayCycleCount\" ");
#endif
}

/*******************************************************************************
* @brief  Send the Floor Temperature to the Cloud
* @inputs pData: void pointer to the thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2019/03/20
*******************************************************************************/
void SendFloorTemperatureResp(void* pData, uint16_t requestId)
{
    uint8_t i;
    char *textBuffer;
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_FLOORTEMPERATURE].propertyName));

    /* textBuffer is using malloc to avoid requesting ~100 bytes off the stack */
    textBuffer = allocate_text_buffer ((TEMPERATURE_STRING_SIZE*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE);

    if (NULL != textBuffer)
    {
        strcpy(textBuffer,"");
        for (i = 0; i < THERMOSTAT_INSTANCES; i++)
        {
            TEMPERATURE_C_t floor_temp;
            char floorTempStr [TEMPERATURE_STRING_SIZE+NULL_TERMINATION_SIZE];

            DBTHRD_GetData(DBTYPE_FLOOR_TEMPERATURE, &floor_temp, i);

            if ((floor_temp != DB_SENSOR_INVALID) && (floor_temp != DB_SENSOR_HIGH) && (floor_temp != DB_SENSOR_LOW))
            {
                floor_temp = TLS_Round(floor_temp, 50);
                sprintf(floorTempStr, "%.1f;", (float)floor_temp/100);
            }
            else
            {
                sprintf(floorTempStr, ";");
            }
            strcat(textBuffer, floorTempStr);
        }

        Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

        HAL_QWiFiMsg (newDataPoint);

        vPortFree(textBuffer);

#ifdef FREERTOS_SUPPORT
        vTracePrintF(wifiEventChannel, "Sending Property \"TstatFloorTemperature\" for TH%d",*((uint8_t*)pData));
#endif
    }
}

/*******************************************************************************
* @brief  Send the Floor Setpoint to the Cloud
* @inputs pData: void pointer to the thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2019/03/20
*******************************************************************************/
void SendFloorSetpointResp(void* pData, uint16_t requestId)
{
    Setpoint100_t setpoint;
    uint8_t i;
    char textBuffer[(SETPOINT_STRING_SIZE*THERMOSTAT_INSTANCES)+NULL_TERMINATION_SIZE];
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_REQUESTPROP_RESP, requestId);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateNameTLV (propTable[WIFITYPE_FLOORSETPOINT].propertyName));

    strcpy(textBuffer,"");
    for (i = 0; i < THERMOSTAT_INSTANCES; i++)
    {
        DBTHRD_GetData(DBTYPE_FLOOR_SETPOINT, &setpoint, i);
        addStringSetpointToBuffer (textBuffer, setpoint, ";", "0");
    }

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(textBuffer));

    HAL_QWiFiMsg (newDataPoint);

#ifdef FREERTOS_SUPPORT
    vTracePrintF(wifiEventChannel, "Sending Property \"TstatFloorSetpoint\" for TH%d",*((uint8_t*)pData));
#endif
}

/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
