/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    MnuboToString.c
* @date    2017/01/31
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include "cmsis_os.h"

#include ".\HAL\inc\MnuboDataPoints.h"
#include ".\HAL\inc\MnuboDataDefinitions.h"

#include ".\HAL\inc\AylaDefinitions.h"
#include ".\HAL\inc\HAL_CloudInterface.h"

#include ".\DB\inc\THREAD_DB.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
typedef struct _mnuboStringElement_t
{
    uint16_t strSize;
    char * str;
    struct _mnuboStringElement_t *next;
} mnuboStringElement_t;

const char *mnuboEventStringTable [MNUBO_EVENT_CNT] =
{
    [MNUBO_EVENT_UPDATE_GROUP_SETPOINT] = "update_grp_setpoint",
    [MNUBO_EVENT_UPDATE_RESIDENCE] = "update_residence",
    [MNUBO_EVENT_UPDATE_TSTAT_SETPOINT] = "update_tst_setpoint",
    [MNUBO_EVENT_UPDATE_CONSUMPTION] = "update_consumption",
    [MNUBO_EVENT_UPDATE_SET_ALERT] = "update_set_alert",
    [MNUBO_EVENT_UPDATE_CLEAR_ALERT] = "update_clear_alert",
    [MNUBO_EVENT_START_HEAT_SESSION] = "start_heat_session",
    [MNUBO_EVENT_STOP_HEAT_SESSION] = "stop_heat_session",
};

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
#ifdef FREERTOS_SUPPORT
    #include "trcRecorder.h"
    extern char* mnuboEventChannel;
#endif



/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
uint8_t printMnuboDataToBuffer (mnuboData_t * mnuboData, mnuboStringElement_t * datalist);
uint8_t printMnuboElementHeaderToBuffer (mnuboElementType_t type, mnuboStringElement_t * header, void * ptrElement);
uint8_t printMnuboElementToBuffer (mnuboElementType_t type, void * ptrElementDescription, mnuboStringElement_t * newElement);
void deleteStringElement (mnuboStringElement_t * element);
void deleteStringElement_linkedList(mnuboStringElement_t ** first_element);
mnuboStringElement_t * printMnuboDataPointToBuffer (mnuboStringElement_t * event, mnuboStringElement_t * object);
void getMnuboData (char * propName, char * propValue, mnuboData_t * mnuboData, uint8_t * listDataType);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/02/03
*******************************************************************************/
void getMnuboData (char * propName, char * propValue, mnuboData_t * mnuboData, uint8_t * listDataType)
{
    /********************************************************/
    /*      By default, assume the data type is not a list  */
	/********************************************************/
    *listDataType = 0;

	/****************************************************************/
    /*      Copy data name base on element type : object or event   */
	/****************************************************************/
    if (MNUBO_ELEMENT_TYPE_OBJECT == mnuboData->type)
    {
        strcpy (propName, mnuboDataDefinition[mnuboData->dataId].object_dataName);
    }
    else if (MNUBO_ELEMENT_TYPE_EVENT == mnuboData->type)
    {
        strcpy (propName, mnuboDataDefinition[mnuboData->dataId].event_dataName);
    }
    else
    {
        strcpy (propName, "");
    }

	/********************************************************************/
    /*     Print data value based on its base type : UINTX100, STRING   */
    /*      INT, etc.                                                   */
	/********************************************************************/
    switch (mnuboDataDefinition[mnuboData->dataId].dataType)
    {

	/********************************************/
    /*  Type STRING :                           */
	/********************************************/
    case MNUBO_DATATYPE_STRING:
        {

            DBTHRD_GetData(mnuboDataDefinition[mnuboData->dataId].dbReferenceType,
                            (void*)propValue,
                            mnuboData->objectIdx);
        }
        break;

	/********************************************/
    /*  Type UINT X 100 :                       */
    /*  e.g. Setpoint X 100                     */
	/********************************************/
    case MNUBO_DATATYPE_UINTX100:
        {
            uint16_t dataX100 = 0;

            DBTHRD_GetData(mnuboDataDefinition[mnuboData->dataId].dbReferenceType,
                            (void*)&dataX100,
                            mnuboData->objectIdx);
            sprintf (propValue, "%.2f", (float)dataX100/100);
        }
        break;

	/********************************************/
    /*  Type INT X 100 :                        */
    /*  e.g. Outdoor Temperature X 100          */
	/********************************************/
    case MNUBO_DATATYPE_INTX100:
        {
            int16_t dataX100 = 0;

            DBTHRD_GetData(mnuboDataDefinition[mnuboData->dataId].dbReferenceType,
                            (void*)&dataX100,
                            mnuboData->objectIdx);
            sprintf (propValue, "%.2f", (float)dataX100/100);
        }
        break;

	/********************************************/
    /*  Type UINT :                             */
    /*  e.g. Outdoor Humidity                   */
	/********************************************/
    case MNUBO_DATATYPE_UINT:
        {
            uint16_t data = 0;

            DBTHRD_GetData(mnuboDataDefinition[mnuboData->dataId].dbReferenceType,
                        (void*)&data,
                        mnuboData->objectIdx);
            sprintf (propValue, "%d", data);
        }
        break;

	/********************************************/
    /*  Type INT :                              */
	/********************************************/
    case MNUBO_DATATYPE_INT:
        {
            int16_t data = 0;

            DBTHRD_GetData(mnuboDataDefinition[mnuboData->dataId].dbReferenceType,
                        (void*)&data,
                        mnuboData->objectIdx);
            sprintf (propValue, "%d", data);
        }
        break;

    /********************************************/
    /*  Type TRIGGER                            */
	/********************************************/
    case MNUBO_DATATYPE_TRIGGER_LOCAL:
        strcpy(propValue, "Local");
        break;

    case MNUBO_DATATYPE_TRIGGER_CLOUD:
        strcpy(propValue, "Cloud");
        break;

    case MNUBO_DATATYPE_TRIGGER_ZIGBEE:
        strcpy(propValue, "ZigBee");
        break;

    case MNUBO_DATATYPE_TRIGGER_GEOFENCING:
        strcpy(propValue, "Geofencing");
        break;

    case MNUBO_DATATYPE_TRIGGER_ACTIVITY_LOCAL:
        strcpy(propValue, "Activity Local");
        break;

    case MNUBO_DATATYPE_TRIGGER_ACTIVITY_MOBILE:
        strcpy(propValue, "Activity Mobile");
        break;

    case MNUBO_DATATYPE_TRIGGER_ACTIVITY_SCHEDULED:
        strcpy(propValue, "Activity Scheduled");
        break;

    case MNUBO_DATATYPE_TRIGGER_OPEN_WINDOW:
        strcpy(propValue, "Open Window");
        break;

    case MNUBO_DATATYPE_TRIGGER_MODE_CHANGE:
        strcpy(propValue, "Mode Change");
        break;

    case MNUBO_DATATYPE_TRIGGER_GROUP_CHANGE:
        strcpy(propValue, "Group Change");
        break;

    /********************************************/
    /*  Type ALERT                              */
	/********************************************/
    case MNUBO_DATATYPE_ALERT:
    {
        dbType_Alerts_t currentAlert;

        DBTHRD_GetData(mnuboDataDefinition[mnuboData->dataId].dbReferenceType,
            (void*)&currentAlert,
            mnuboData->objectIdx);
        sprintf(propValue, "%d", currentAlert.LastAlertId);
    }
    break;

    /********************************************/
    /*  Type ALERT_LIST                         */
	/********************************************/
    case MNUBO_DATATYPE_ALERT_LIST:
    {
        dbType_Alerts_t currentAlert;
        uint8_t i;

        //This data type is a list
        *listDataType = 1;

        DBTHRD_GetData(mnuboDataDefinition[mnuboData->dataId].dbReferenceType,
            (void*)&currentAlert,
            mnuboData->objectIdx);
        strcpy(propValue, "");
        for (i = 0; i < ALERT_INSTANCES; i++)
        {
            currentAlert.ActiveAlerts[i] = currentAlert.ActiveAlerts[i] + 0x30;
            //Check if there is an active alert
            if (currentAlert.ActiveAlerts[i] != '0')
            {
                strncat(propValue, (char const*)&currentAlert.ActiveAlerts[i], 1);
                if (i != (ALERT_INSTANCES - 1))
                {
                    if (currentAlert.ActiveAlerts[i+1] != NULL)
                    {
                        strcat(propValue, ",");
                    }
                }
            }
            else
            {
                //Check if this thermostat instance has no alert at all
                if (i == 0)
                {
                    //If this thermostat instance has no active alert, set the alert value to 0
                    strncat(propValue, (char const*)&currentAlert.ActiveAlerts[i], 1);
                    break;
                }
            }
        }
    }
    break;

    /********************************************/
    /*  Type LATITUDE                           */
	/********************************************/
    case MNUBO_DATATYPE_LATITUDE:
    {
        dbGeofenceCenter_t geofenceCenter;
        float latitude;

        DBTHRD_GetData(mnuboDataDefinition[mnuboData->dataId].dbReferenceType,
                        (void*)&geofenceCenter,
                        mnuboData->objectIdx);
        latitude = atof((const char*)geofenceCenter.latitude);
        sprintf (propValue, "%.6f",latitude);
    }
    break;

    /********************************************/
    /*  Type LONGITUDE                          */
	/********************************************/
    case MNUBO_DATATYPE_LONGITUDE:
    {
        dbGeofenceCenter_t geofenceCenter;
        float longitude;

        DBTHRD_GetData(mnuboDataDefinition[mnuboData->dataId].dbReferenceType,
                        (void*)&geofenceCenter,
                        mnuboData->objectIdx);
        longitude = atof((const char*)geofenceCenter.longitude);
        sprintf (propValue, "%.6f",longitude);
    }
    break;

    /****************************************************/
    /*  Type MNUBO_DATATYPE_RESIDENCE_DAILY_CONSUMPTION */
	/****************************************************/
    case MNUBO_DATATYPE_RESIDENCE_DAILY_CONSUMPTION:
    {
        dbType_DailyConsumption_t res_daily_consumption;

        DBTHRD_GetData(mnuboDataDefinition[mnuboData->dataId].dbReferenceType,
            (void*)&res_daily_consumption,
            1);
        sprintf(propValue, "%d", res_daily_consumption.Consumption);
    }
    break;

    default :
        strcpy (propValue, "");
        break;
    }
}



/*******************************************************************************
* @brief  Send mnubo data for analytics
* @inputs pData: pointer to user index
* @retval None
* @author Jean-François Simard
* @date 2017/02/02
*******************************************************************************/
void SendDataPointToCloud (mnuboStringElement_t * dataPoint)
{
    dbType_CloudConnectivity_t cloudConnectivity;

    DBTHRD_GetData(DBTYPE_CLOUDCONNECTIVITY, (void*)&cloudConnectivity, INDEX_DONT_CARE);

    if ((0 != cloudConnectivity.isConnectedToCloud) && (0 != cloudConnectivity.isRegistered))
    {
        aylaDataPoint_t * newDataPoint;

        newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_SENDTLVS, 0x2d2d);

        Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateNameTLV ("mnubo"));
        Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateTextTLV(dataPoint->str));

        HAL_QWiFiMsg (newDataPoint);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void * allocate_data_memory (size_t size)
{
    void * mem_ptr = NULL;
//    uint8_t retry = 3;

//    while ((NULL == mem_ptr) && (retry-- > 0))
    {
        mem_ptr = pvPortMalloc(size);

//        if (NULL == mem_ptr)
//        {
//            osDelay(10);
//        }
    }
    return mem_ptr;
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/02/01
*******************************************************************************/
mnuboStringElement_t * printMnuboDataPointToBuffer (mnuboStringElement_t * event, mnuboStringElement_t * object)
{
#define MAX_BUFFER_USAGE        900        //maximum allowed bytes allowed in string buffer; this gives room for padding and extra ','
#define STRING_BUFFER_SIZE      1024

    int16_t totalSize;
    uint8_t noMoreRoom = 0;
    uint8_t incomplete_data_point = 0;
    mnuboStringElement_t * dataPoint = NULL;
    mnuboStringElement_t * current_event = event;
    mnuboStringElement_t * current_object = object;

    /****************************************************************/
    /*  First Step :                                                */
    /*      evaluate the total size of the data points, including   */
    /*      all the brackets ([]), braces ({}) and commas (,)       */
    totalSize = strlen("{}");

    if (NULL != current_event)
    {
        mnuboStringElement_t * existingEvent = current_event;

        totalSize += strlen("\"events\":[]");
        while (NULL != existingEvent)
        {
            totalSize += existingEvent->strSize;
            existingEvent = existingEvent->next;
        }
    }

    if (NULL != current_object)
    {
        mnuboStringElement_t * existingObject = current_object;

        totalSize += strlen("\"objects\":[]");
        while (NULL != existingObject)
        {
            totalSize += existingObject->strSize;
            existingObject = existingObject->next;
        }
    }

    if ((NULL != current_object) && (NULL != current_event))
    {
        totalSize += strlen(",");           //comma between the events and objects
    }
    /****************************************************************/

    /****************************************************************/
    /*  Second Step :                                               */
    /*      fragment and concatenate the objects and the event into */
    /*      one or many strings (up to 1024 per datapoint)          */

    while (totalSize > 0)
    {
        mnuboStringElement_t * newDataPoint = allocate_data_memory (sizeof(mnuboStringElement_t));
#ifdef FREERTOS_SUPPORT
            vTracePrint(mnuboEventChannel, "new cloud data point");
#endif
        if (NULL != newDataPoint)
        {
            char * dataPointStr = allocate_data_memory(STRING_BUFFER_SIZE);

            newDataPoint->next = dataPoint;
            newDataPoint->str = NULL;
            newDataPoint->strSize = 0;

            dataPoint = newDataPoint;

            if (NULL != dataPointStr)
            {
                memset(dataPointStr,0,STRING_BUFFER_SIZE);
                sprintf (dataPointStr, "{");

                if (NULL != current_event)
                {
                    uint8_t firstEvent;

                    strcat (dataPointStr, "\"events\":[");

                    noMoreRoom = 0;
                    firstEvent = 1;
                    while (NULL != current_event)
                    {
                        if ((strlen(dataPointStr) + current_event->strSize) < MAX_BUFFER_USAGE)
                        {
                            if (firstEvent == 0)
                            {
                                strcat(dataPointStr, ",");
                            }
                            strcat (dataPointStr, (const char*)current_event->str);

                            current_event = current_event->next;

                            if (NULL != current_event)
                            {
                                firstEvent = 0;
                            }
                        }
                        else
                        {
                            noMoreRoom = 1;
                            break;
                        }
                    }

                    strcat (dataPointStr, "]");
                }

                if ((strlen(dataPointStr) < MAX_BUFFER_USAGE) && (noMoreRoom == 0))
                {
                    if (NULL != current_object)
                    {
                        uint8_t firstObject;

                        if (NULL != strstr(dataPointStr, "events"))
                        {
                            strcat(dataPointStr,",");
                        }
                        strcat (dataPointStr, "\"objects\":[");

                        firstObject = 1;
                        while (NULL != current_object)
                        {
                            if ((strlen(dataPointStr) + current_object->strSize) < MAX_BUFFER_USAGE)
                            {
                                if (firstObject == 0)
                                {
                                    strcat(dataPointStr, ",");
                                }
                                strcat (dataPointStr, (const char*)current_object->str);

                                current_object = current_object->next;

                                if (NULL != current_object)
                                {
                                    firstObject = 0;
                                }
                            }
                            else
                            {
                                noMoreRoom = 1;
                                break;
                            }
                        }

                        strcat (dataPointStr, "]");
                    }
                }
                else
                {
                    noMoreRoom = 1;
                }
                strcat (dataPointStr, "}");

                newDataPoint->strSize = strlen(dataPointStr);
                newDataPoint->str = dataPointStr;

                // newDataPoint->next = dataPoint;
                // dataPoint = newDataPoint;

                totalSize -= newDataPoint->strSize;
                noMoreRoom = 0;
            }
            else
            {
                incomplete_data_point = 1;
                break;
            }
        }
        else
        {
            incomplete_data_point = 1;
            break;
        }
    }
    /****************************************************************/

    /* if cloud datapoint is incomplete, free already
       allocated memory and retry later                             */
    if (0 != incomplete_data_point)
    {
        deleteStringElement_linkedList(&dataPoint);
    }

    return (dataPoint);
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/27
*******************************************************************************/
void getDeviceId (char * deviceIdStr, mnuboObjId_t deviceId)
{
    char dsn [30];

    DBTHRD_GetData(DBTYPE_DSN, (void*)dsn, INDEX_DONT_CARE);

    switch (deviceId&MNUBO_OBJ_MASK)
    {
    case MNUBO_OBJ_TSTAT_BASE:
        sprintf (deviceIdStr, "%s-TS%02d",dsn,(deviceId&MNUBO_ID_MASK)+1);
        break;

    case MNUBO_OBJ_GROUP_BASE:
        sprintf (deviceIdStr, "%s-GR%02d",dsn,(deviceId&MNUBO_ID_MASK)+1);
        break;

    case MNUBO_OBJ_ACTIVITY_BASE:
        sprintf (deviceIdStr, "%s-AC%02d",dsn,(deviceId&MNUBO_ID_MASK)+1);
        break;

    case MNUBO_OBJ_RESIDENCE:
        sprintf (deviceIdStr, "%s",dsn);
        break;

    default:
        sprintf (deviceIdStr, "");
        break;
    }
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/31
*******************************************************************************/
void deleteStringElement (mnuboStringElement_t * element)
{
    if (NULL != element)
    {
        if (NULL != element->str)
        {
            vPortFree (element->str);
        }
        vPortFree (element);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void deleteStringElement_linkedList(mnuboStringElement_t ** _first_element)
{
    mnuboStringElement_t * first_element = *_first_element;
    while (NULL != first_element)
    {
        mnuboStringElement_t * next_element;
        next_element = first_element->next;
        deleteStringElement (first_element);
        first_element = next_element;
    }
    *_first_element = NULL;
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/31
*******************************************************************************/
uint8_t printMnuboElementToBuffer (mnuboElementType_t type, void * ptrElementDescription, mnuboStringElement_t * newElement)
{
	mnuboData_t * elementData = NULL;
    uint8_t error_status = 0;

	switch (type)
	{
	case MNUBO_ELEMENT_TYPE_OBJECT:
        {
            mnuboObject_t * object = (mnuboObject_t *)ptrElementDescription;
            elementData = object->objectData;
        }
		break;

	case MNUBO_ELEMENT_TYPE_EVENT:
        {
            mnuboEvent_t * event = (mnuboEvent_t *)ptrElementDescription;
            elementData = event->eventData;
        }
		break;

	default:
		break;
	}

	if (NULL != newElement)
	{
		mnuboStringElement_t elementHeader =
		{
			.next = NULL,
			.strSize = 0,
			.str = NULL,
		};
		mnuboStringElement_t dataList =
		{
			.next = NULL,
			.strSize = 0,
			.str = NULL,
		};

		error_status = printMnuboElementHeaderToBuffer (type,
										 &elementHeader,
										 ptrElementDescription);

		error_status = printMnuboDataToBuffer (elementData, &dataList);

        if (0 == error_status)
        {
            newElement->strSize = elementHeader.strSize + dataList.strSize;
            newElement->next = NULL;
            newElement->str = allocate_data_memory(newElement->strSize + 1 + 1);		// +1 : '}' ; +1 : '\0'

            vTracePrint(mnuboEventChannel, "printing an element to string buffer");

            if (NULL != newElement->str)
            {
                if (NULL != elementHeader.str)
                {
                    strcpy(newElement->str, elementHeader.str);
                }

                if (NULL != dataList.str)
                {
                    strcat (newElement->str, dataList.str);
                }

                strcat (newElement->str, "}");
            }
            else
            {
                error_status = 1;
            }
        }

        if (NULL != elementHeader.str)
        {
            vPortFree (elementHeader.str);
        }

        if (NULL != dataList.str)
        {
            vPortFree (dataList.str);
        }
	}
    return error_status;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/31
*******************************************************************************/
uint8_t printMnuboElementHeaderToBuffer (mnuboElementType_t type, mnuboStringElement_t * header, void * ptrElement)
{
#define STRING_BUFFER_LENGTH   100
    uint8_t error_status = 0;
	char * eventString;
	char wifiDSN[21];           //  AC000W000825669  => 15 characters
                                //  -TS00            => 5 characters
                                //  \0              => 1 character
                                //---------------
                                //  21 characters
	mnuboObjId_t deviceId;
	mnuboEventId_t eventId;

    eventString = allocate_data_memory(STRING_BUFFER_LENGTH);

    if (NULL != eventString)
    {
        memset(eventString, 0, STRING_BUFFER_LENGTH);
        switch (type)
        {
        case MNUBO_ELEMENT_TYPE_OBJECT:
            {
                mnuboObject_t * ptrObject = (mnuboObject_t *) ptrElement;
                deviceId = ptrObject->deviceId;
            }
            break;

        case MNUBO_ELEMENT_TYPE_EVENT:
            {
                mnuboEvent_t * ptrEvent = (mnuboEvent_t *) ptrElement;
                deviceId = ptrEvent->deviceId;
                eventId = ptrEvent->eventId;
            }
            break;

        default:
            break;

        }

        getDeviceId (wifiDSN, deviceId);

        /****************************************************/
        /*  print in buffer :                               */
        /*    {                                             */
        /*      "x_event_type":"eventType",                 */
        /*      "x_device_id":"deviceId",                   */
        /****************************************************/

        vTracePrint(mnuboEventChannel, "print header to string buffer");

        if (MNUBO_ELEMENT_TYPE_EVENT == type)
        {
            sprintf (eventString, "{\"x_object\":{\"x_device_id\":\"%s\"},\"x_event_type\":\"%s\"" ,
                     wifiDSN,mnuboEventStringTable[eventId]);
        }
        else if (MNUBO_ELEMENT_TYPE_OBJECT == type)
        {
            sprintf (eventString, "{\"x_device_id\":\"%s\",\"x_object_type\":" , wifiDSN);
            switch (deviceId&MNUBO_OBJ_MASK)
            {
            case MNUBO_OBJ_TSTAT_BASE:
                strcat(eventString, "\"thermostat\"");
                break;

            case MNUBO_OBJ_GROUP_BASE:
                strcat(eventString, "\"group\"");
                break;

            case MNUBO_OBJ_ACTIVITY_BASE:
                strcat(eventString, "\"activity\"");
                break;

            case MNUBO_OBJ_RESIDENCE:
                strcat(eventString, "\"residence\"");
                break;

            default:
                strcat(eventString, "\" \"");
                break;
            }
        }

        header->strSize = strlen(eventString);
        header->str = eventString;
    }
    else
    {
        error_status = 1;
    }

    return error_status;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/31
*******************************************************************************/
uint8_t printMnuboDataToBuffer (mnuboData_t * mnuboData, mnuboStringElement_t * datalist)
{
#define DATA_BUFFER_LENGTH  100
    uint8_t error_status = 0;
	mnuboStringElement_t * firstDataElement = NULL;
	uint16_t strSize = 0;
	uint16_t dataCnt = 0;
    uint8_t listDataType = 0;

	while ((NULL != mnuboData) && (0 == error_status))
	{
        char propName[30];
        char propValue[30];

		mnuboStringElement_t * newElement;

        getMnuboData (propName, propValue, mnuboData, &listDataType);

		newElement = allocate_data_memory (sizeof(mnuboStringElement_t));
		if (NULL != newElement)
		{
            char *tmpStrBuf;

            newElement->next = firstDataElement;
            newElement->str = NULL;
            newElement->strSize = 0;

            vTracePrint(mnuboEventChannel, "create a new mnubo element");

            firstDataElement = newElement;

            tmpStrBuf = allocate_data_memory (DATA_BUFFER_LENGTH);

            if (NULL != tmpStrBuf)
            {
                memset(tmpStrBuf, 0, DATA_BUFFER_LENGTH);

                /****************************************************/
                /*  print in buffer :                               */
                /*      "propName":"propValue",                     */
                /****************************************************/
                if (listDataType == 0)
                {
                    sprintf (tmpStrBuf, "\"%s\":\"%s\"", propName, propValue);
                }
                else
                {
                    sprintf (tmpStrBuf, "\"%s\":[\"%s\"]", propName, propValue);
                }

                newElement->strSize = strlen(tmpStrBuf);
                newElement->str = tmpStrBuf;

                strSize += firstDataElement->strSize;
                dataCnt++;
            }
            else
            {
                error_status = 1;
            }
		}
        else
        {
            error_status = 1;
        }
		mnuboData = mnuboData->nextData;
	}

    if (0 == error_status)
    {
        datalist->str = allocate_data_memory(strSize+1+dataCnt);		//dataCnt to separated each data by a ','
    }

	if (NULL != datalist->str)
	{
		datalist->str[0] = '\0';
		while (NULL != firstDataElement)
		{
            mnuboStringElement_t * next;
			strcat (datalist->str, ",");
			strcat (datalist->str, firstDataElement->str);

            next = firstDataElement->next;

            deleteStringElement(firstDataElement);

			firstDataElement = next;
		}
		datalist->strSize = strlen(datalist->str);
	}
	else
	{
		deleteStringElement_linkedList(&firstDataElement);
        error_status = 1;
	}

    return error_status;
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/27
*******************************************************************************/
uint8_t Mnubo_SendDatapoint (hMnuboDataPoint dataPoint)
{
    mnuboStringElement_t * mnuboEventElement = NULL;
    mnuboStringElement_t * mnuboObjectElement = NULL;
    mnuboStringElement_t * mnuboDataPointStr = NULL;

    if (NULL != dataPoint->event)
    {
		mnuboEvent_t * eventPtr = dataPoint->event;

		while ((NULL != eventPtr) && (0 == dataPoint->invalid_datapoint))
		{
			mnuboStringElement_t * newElement;

			newElement = allocate_data_memory(sizeof(mnuboStringElement_t));

            if (NULL != newElement)
            {
                newElement->next = NULL;
                newElement->str = NULL;
                newElement->strSize = 0;

                vTracePrint(mnuboEventChannel, "create a new event element");

                dataPoint->invalid_datapoint = printMnuboElementToBuffer (MNUBO_ELEMENT_TYPE_EVENT,
                                            eventPtr, newElement);

                newElement->next = mnuboEventElement;
                mnuboEventElement = newElement;

                eventPtr = eventPtr->nextEvent;
            }
            else
            {
                dataPoint->invalid_datapoint = 1;
            }
		}
    }

    if ((NULL != dataPoint->object) && (0 == dataPoint->invalid_datapoint))
    {
		mnuboObject_t * objectPtr = dataPoint->object;

		while ((NULL != objectPtr) && (0 == dataPoint->invalid_datapoint))
		{
			mnuboStringElement_t * newElement;

			newElement = allocate_data_memory(sizeof(mnuboStringElement_t));

            if (NULL != newElement)
            {
                newElement->next = NULL;
                newElement->str = NULL;
                newElement->strSize = 0;

                vTracePrint(mnuboEventChannel, "create a new object element");

                dataPoint->invalid_datapoint = printMnuboElementToBuffer (MNUBO_ELEMENT_TYPE_OBJECT,
                                            objectPtr, newElement);

                newElement->next = mnuboObjectElement;
                mnuboObjectElement = newElement;

                objectPtr = objectPtr->nextObject;
            }
            else
            {
                dataPoint->invalid_datapoint = 1;
            }
		}
    }

    if (0 == dataPoint->invalid_datapoint)
    {
        mnuboDataPointStr = printMnuboDataPointToBuffer (mnuboEventElement, mnuboObjectElement);
    }

    deleteStringElement_linkedList(&mnuboEventElement);
    deleteStringElement_linkedList(&mnuboObjectElement);

    if (NULL != mnuboDataPointStr)
    {
        while (NULL != mnuboDataPointStr)
        {
            mnuboStringElement_t * next_datapoint;

            SendDataPointToCloud (mnuboDataPointStr);

            next_datapoint = mnuboDataPointStr->next;

            deleteStringElement(mnuboDataPointStr);
            mnuboDataPointStr = next_datapoint;
        }
    }

    return dataPoint->invalid_datapoint;
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
