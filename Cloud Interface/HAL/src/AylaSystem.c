/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    AylaSystem.c
* @date    2016/10/11
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>

#include ".\HAL\inc\AylaDefinitions.h"
#include ".\HAL\inc\HAL_CloudInterface.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/



/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
extern void WiFiRadioInitStateMachine (wifiRadioInitEvents_t event, void * pData);


/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
void GetWiFiModuleVersion(void)
{
    aylaDataPoint_t * newDataPoint;
    
    newDataPoint = Ayla_CreateDataPoint (CONTROLOPCODE_GET_CONFIGITEMS, 0x28c0);
    
    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateConfigTokenTLV (2, 
                             CONFTOKEN_SYSTEM, CONFTOKEN_VERSION));
    

    HAL_QWiFiMsg (newDataPoint);       
    
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/10/11
*******************************************************************************/
void GetUtcTime (void)
{
    aylaDataPoint_t * newDataPoint;
    
    newDataPoint = Ayla_CreateDataPoint (CONTROLOPCODE_GET_CONFIGITEMS, 0x04c0);
    
    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateConfigTokenTLV (2, 
                             CONFTOKEN_SYSTEM, CONFTOKEN_TIME));
    

    HAL_QWiFiMsg (newDataPoint);          
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/10/11
*******************************************************************************/
void GetTimeZone (void)
{
    aylaDataPoint_t * newDataPoint;
    
    newDataPoint = Ayla_CreateDataPoint (CONTROLOPCODE_GET_CONFIGITEMS, 0x05c0);
    
    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateConfigTokenTLV (2, 
                             CONFTOKEN_SYSTEM, CONFTOKEN_TIMEZONE));
    

    HAL_QWiFiMsg (newDataPoint);              
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/10/11
*******************************************************************************/
void GetDSTActive (void)
{   
    aylaDataPoint_t * newDataPoint;
    
    newDataPoint = Ayla_CreateDataPoint (CONTROLOPCODE_GET_CONFIGITEMS, 0x06c0);
    
    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateConfigTokenTLV (2, 
                             CONFTOKEN_SYSTEM, CONFTOKEN_DSTACTIVE));
    

    HAL_QWiFiMsg (newDataPoint);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/16
*******************************************************************************/
void GetDSTChange (void)
{
    aylaDataPoint_t * newDataPoint;
    
    newDataPoint = Ayla_CreateDataPoint (CONTROLOPCODE_GET_CONFIGITEMS, 0x07c0);
    
    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateConfigTokenTLV (2, 
                             CONFTOKEN_SYSTEM, CONFTOKEN_DSTCHANGE));
    

    HAL_QWiFiMsg (newDataPoint);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/16
*******************************************************************************/
void GetDSTValid (void)
{
    aylaDataPoint_t * newDataPoint;
    
    newDataPoint = Ayla_CreateDataPoint (CONTROLOPCODE_GET_CONFIGITEMS, 0x08c0);
    
    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateConfigTokenTLV (2, 
                             CONFTOKEN_SYSTEM, CONFTOKEN_DSTVALID));
    

    HAL_QWiFiMsg (newDataPoint);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/02/10
*******************************************************************************/
void GetDSN (void)
{
    aylaDataPoint_t * newDataPoint;
    
    newDataPoint = Ayla_CreateDataPoint (CONTROLOPCODE_GET_CONFIGITEMS, 0x58c0);
    
    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateConfigTokenTLV (2, 
                             CONFTOKEN_SYSTEM, CONFTOKEN_DEVID));
    

    HAL_QWiFiMsg (newDataPoint);
}

/*******************************************************************************
* @brief  Parse a configuration token response : /wifi/profile
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/19
*******************************************************************************/
void ControlOps_ParseConfToken_System (ayla_Packet_t * receivedPacket, uint16_t payloadLength)
{
    ayla_incomingTLV_t * firstTLV;

    if (payloadLength > 0)
    {
        firstTLV = (ayla_incomingTLV_t *)receivedPacket->Payload;

        switch (firstTLV->Value[1])
        {
        /************************************/
        /* sys/time                         */
        /************************************/
        case CONFTOKEN_TIME:
            WiFiRadioInitStateMachine(WIFISTARTEVENT_UTCTIME_RECEIVED, (void*)&firstTLV->Value[2]);
            break;

        /************************************/
        /* sys/timezone                     */
        /************************************/
        case CONFTOKEN_TIMEZONE:
            WiFiRadioInitStateMachine(WIFISTARTEVENT_TIMEZONE_RECEIVED,  (void*)&firstTLV->Value[2]);
            break;

        /************************************/
        /* sys/dst_active                   */
        /************************************/
        case CONFTOKEN_DSTACTIVE:
            WiFiRadioInitStateMachine(WIFISTARTEVENT_DST_ACTIVE_RECEIVED,  (void*)&firstTLV->Value[2]);
            break;

        /************************************/
        /* sys/dst_change                   */
        /************************************/
        case CONFTOKEN_DSTCHANGE:
            WiFiRadioInitStateMachine(WIFISTARTEVENT_DST_CHANGE_RECEIVED,  (void*)&firstTLV->Value[2]);
            break;

        /************************************/
        /* sys/dst_valid                    */
        /************************************/
        case CONFTOKEN_DSTVALID:
            WiFiRadioInitStateMachine(WIFISTARTEVENT_DST_VALID_RECEIVED,  (void*)&firstTLV->Value[2]);
            break;

        /************************************/
        /* sys/dev_id                       */
        /************************************/
        case CONFTOKEN_DEVID:
            WiFiRadioInitStateMachine(WIFISTARTEVENT_DSN_RECEIVED,  (void*)&firstTLV->Value[2]);
            break;

        /************************************/
        /* sys/version                      */
        /************************************/
        case CONFTOKEN_VERSION:
            WiFiRadioInitStateMachine(WIFISTARTEVENT_VERSIONRECEIVED,  (void*)&firstTLV->Value[2]);
            break;

        /************************************/
        /************************************/
        default:
            break;
        }
    }
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/



/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
