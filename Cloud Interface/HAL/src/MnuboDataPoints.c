/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    MnuboDataPoints.c
* @date    2017/01/25
* @authors J-F. Simard
* @brief   
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <string.h>

#include "cmsis_os.h"

#include ".\HAL\inc\MnuboDataPoints.h"

#include ".\sync_barrier\inc\SyncBarrier.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/


/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
static mnuboDataPoint_t * firstMnuboDataPoint = NULL;
#ifdef FREERTOS_SUPPORT
    #include "trcRecorder.h"
    char* mnuboEventChannel;
#endif

osThreadId analyticsTaskHandle;    
/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
void StartAnalyticsTask(void const * argument);
hMnuboObject deleteObject (hMnuboObject object);
hMnuboEvent deleteEvent  (hMnuboEvent event);
hMnuboData deleteData (hMnuboData data);


/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/26
*******************************************************************************/
hMnuboData deleteData (hMnuboData data)
{
    mnuboData_t * nextData = NULL;
    if (NULL != data)
    {
        nextData = data->nextData;
        vPortFree(data);
    }
    
    return nextData;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/26
*******************************************************************************/
hMnuboObject deleteObject (hMnuboObject object)
{
    mnuboObject_t * nextObject = NULL;
    
    if (NULL != object)
    {
        mnuboData_t * objectData = NULL;
        
        objectData = object->objectData;
        while (NULL != objectData)
        {
            objectData = deleteData (objectData);
        }                                           
                                     
        nextObject = object->nextObject;
        vPortFree(object);
    }
    return (nextObject);
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/26
*******************************************************************************/
hMnuboEvent deleteEvent (hMnuboEvent event)
{
    mnuboEvent_t * nextEvent = NULL;
    
    if (NULL != event)
    {
        mnuboData_t * eventData = NULL;
        
        eventData = event->eventData;
        while (NULL != eventData)
        {
            eventData = deleteData (eventData);
        }                                           
                                     
        nextEvent = event->nextEvent;
        vPortFree(event);
    }
    return (nextEvent);
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2017/02/20
*******************************************************************************/
void StartAnalyticsTask(void const * argument)
{
    Sync_Barrier((barriers_name_t)BARRIER_TASK_DEFAULT);
    Sync_TaskReady (BARRIER_TASK_ANALYTICS);
    
    for (;;)
    {
        xTaskNotifyWait (0,0xffff,NULL,pdMS_TO_TICKS(10));
        
        while (NULL != firstMnuboDataPoint)
        {
            mnuboDataPoint_t * nextDataPoint;
            firstMnuboDataPoint->invalid_datapoint = 0;
            if (Mnubo_SendDatapoint(firstMnuboDataPoint))
            {
                vTracePrint(mnuboEventChannel, "error with datapoint; will retry...;");
                break;
            }
            else
            {
                vTracePrint(mnuboEventChannel, "datapoint queued ok; deleting;");
                nextDataPoint = firstMnuboDataPoint->nextDataPoint;            
                Mnubo_DeleteDataPoint ( &firstMnuboDataPoint);            
                firstMnuboDataPoint = nextDataPoint;
            }
        }
    }
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/25
*******************************************************************************/
void InitAnalytics (void)
{
    osThreadDef(analyticsTask, StartAnalyticsTask, osPriorityNormal, 0, 256);
    analyticsTaskHandle = osThreadCreate(osThread(analyticsTask), NULL);
    mnuboEventChannel = vTraceStoreUserEventChannelName("Mnubo Events");
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2017/02/20
*******************************************************************************/
void ANALYTICS_SendDataPoint(hMnuboDataPoint dataPoint)
{
    if (NULL != dataPoint)
    {
        if (0 == dataPoint->invalid_datapoint)
        {
            if (NULL != firstMnuboDataPoint)
            {
                mnuboDataPoint_t * existingDataPoint = firstMnuboDataPoint;
                
                while (NULL != existingDataPoint->nextDataPoint)
                {
                    existingDataPoint = existingDataPoint->nextDataPoint;
                }
                existingDataPoint->nextDataPoint = dataPoint;
            }
            else
            {
                firstMnuboDataPoint = dataPoint;
            }
            
            xTaskNotify( analyticsTaskHandle, 0, eNoAction );
        }
        else
        {
            vTracePrint(mnuboEventChannel, "invalid datapoint; deleting...");
            Mnubo_DeleteDataPoint(&dataPoint);
        }
    }
}

/*******************************************************************************
* @brief  allocate data memory from the RTOS heap.  The function is not blocking
*       forever since it does not serve a critical system function
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
static void * allocate_data_memory (size_t size)
{
    void * mem_ptr = NULL;
    uint8_t retry = 3;
    
    while ((NULL == mem_ptr) && (retry-- > 0))
    {
        mem_ptr = pvPortMalloc(size);
        
        if (NULL == mem_ptr)
        {
            osDelay(10);
        }
    }
    return mem_ptr;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/25
*******************************************************************************/
hMnuboDataPoint Mnubo_CreateNewDataPoint (void)
{
    mnuboDataPoint_t * newDataPoint = NULL;

    
    newDataPoint = allocate_data_memory(sizeof(mnuboDataPoint_t));
    
    if (NULL != newDataPoint)
    {
        vTracePrint(mnuboEventChannel, "New Mnubo datapoint created");
        newDataPoint->nextDataPoint = NULL;
        // newDataPoint->previousDataPoint = NULL;
        newDataPoint->event = NULL;
        newDataPoint->object = NULL;
        newDataPoint->eventCnt = 0;
        newDataPoint->objectCnt = 0;
        newDataPoint->invalid_datapoint = 0;
    }

    return newDataPoint;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/25
*******************************************************************************/
void Mnubo_DeleteDataPoint (hMnuboDataPoint *_dataPoint)
{
    if (NULL != *_dataPoint)
    {
        hMnuboDataPoint dataPoint = *_dataPoint;
        /*  Free memory used for datapoint's objects    */
        while (NULL != dataPoint->object)
        {
            dataPoint->object = deleteObject (dataPoint->object);
        }
        
        /*  Free memory used for datapoint's event  */
        while (NULL != dataPoint->event)
        {
            dataPoint->event = deleteEvent (dataPoint->event); 
        }
        
        vTracePrint(mnuboEventChannel, "Mnubo DataPoint deleted");
        
        vPortFree(dataPoint);
        *_dataPoint = NULL;
    }
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/26
*******************************************************************************/
hMnuboObject Mnubo_AddObjectToDataPoint (hMnuboDataPoint dataPoint, 
                                            mnuboObjId_t deviceId)
{
    mnuboObject_t * newObject = NULL;
    
    
    if ((NULL != dataPoint) && (0 == dataPoint->invalid_datapoint))
    {
        newObject = allocate_data_memory (sizeof(mnuboObject_t));
    
        if (NULL != newObject)
        {
            vTracePrint(mnuboEventChannel, "Adding an Object on Mnubo DataPoint");

            newObject->nextObject = dataPoint->object;
            dataPoint->object = newObject;
            
            newObject->deviceId = deviceId;
            newObject->objectData = NULL;
            newObject->parent_datapoint = dataPoint;
            
            dataPoint->objectCnt++;
        }
        else
        {
            dataPoint->invalid_datapoint = 1;
        }
    }
    return newObject;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/26
*******************************************************************************/
hMnuboEvent Mnubo_AddEventToDataPoint (hMnuboDataPoint dataPoint,
                                          mnuboEventId_t eventId,
                                          mnuboObjId_t deviceId)
{
    mnuboEvent_t * newEvent = NULL;
    
    if ((NULL != dataPoint) && (0 == dataPoint->invalid_datapoint))
    {
        newEvent = allocate_data_memory (sizeof(mnuboEvent_t));
        
        if (NULL != newEvent)
        {
            vTracePrint(mnuboEventChannel, "Adding an Event on Mnubo DataPoint");

            newEvent->nextEvent = dataPoint->event;
            dataPoint->event = newEvent;
            
            newEvent->deviceId = deviceId;
            newEvent->eventId = eventId;
            newEvent->eventData = NULL;
            newEvent->parent_datapoint = dataPoint;
            
            dataPoint->eventCnt++;
        }
        else
        {
            dataPoint->invalid_datapoint = 1;
        }
        
    }
    return newEvent;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/26
*******************************************************************************/
void Mnubo_AddDataToObject (hMnuboObject object, mnuboDataId_t dataId)
{
    mnuboDataPoint_t * parent_datapoint = (mnuboDataPoint_t *)object->parent_datapoint;
    
    if ((NULL != object) && (0 == parent_datapoint->invalid_datapoint))
    {
        mnuboData_t * newData = NULL;
        
        newData = allocate_data_memory(sizeof(mnuboData_t));
        
        if (NULL != newData)
        {
            vTracePrint(mnuboEventChannel, "Adding an Object Data on Mnubo DataPoint");

            newData->dataId = dataId;
            newData->type = MNUBO_ELEMENT_TYPE_OBJECT;
            newData->objectIdx = object->deviceId&MNUBO_ID_MASK;
            newData->nextData = object->objectData;
            object->objectData = newData;            
        }
        else
        {
            parent_datapoint->invalid_datapoint = 1;
        }
    }
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/26
*******************************************************************************/
void Mnubo_AddDataToEvent (hMnuboEvent event, mnuboDataId_t dataId)
{
    mnuboDataPoint_t * parent_datapoint = (mnuboDataPoint_t *)event->parent_datapoint;
    
    if ((NULL != event) && (0 == parent_datapoint->invalid_datapoint))
    {
        mnuboData_t * newData = NULL;
        
        newData = allocate_data_memory(sizeof(mnuboData_t));
        
        if (NULL != newData)
        {
            vTracePrint(mnuboEventChannel, "Adding an Event data on Mnubo DataPoint");

            newData->dataId = dataId;
            newData->type = MNUBO_ELEMENT_TYPE_EVENT;
            newData->objectIdx = event->deviceId&MNUBO_ID_MASK;
            newData->nextData = event->eventData;
            event->eventData = newData;
        }
        else
        {
            parent_datapoint->invalid_datapoint = 1;
        }
    }
}





/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
