/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    AylaDataOps.c
* @date    2016/09/06
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include ".\HAL\inc\AylaDataOps.h"

#include <stdint.h>
#include <string.h>

#include ".\HAL\inc\AylaDefinitions.h"
#include ".\HAL\inc\AylaReceiveProperties.h"
#include ".\HAL\inc\AylaProperties.h"


#include ".\BSP\Ayla WM-N-BM-30\inc\BSP_Ayla_WM_N_MB_30.h"
#include ".\HAL\inc\HAL_CloudInterface.h"
#include ".\DB\inc\THREAD_DBDataTypes.h"
#include ".\DB\inc\THREAD_DB.h"
#include "SwVersion.h"
#include ".\strings\inc\strings_tool.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
extern const prop_t propTable[];
#define PROPERTY_NAME_MAX_LENGTH    32
#define WIFI_STATUS_TIMEOUT         10000   //10 seconds (in ms)

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
#ifdef FREERTOS_SUPPORT
    #include "trcRecorder.h"
    extern char* wifiEventChannel;
#endif
static ayla_wifiStatus_t LastJoinStatus = WIFIJOIN_OPERATION_IN_PROG;
static uint8_t DisconnectReason = 0;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
TimerHandle_t WiFi_Status_Timer = NULL;


/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
extern void WiFiRadioInitStateMachine (wifiRadioInitEvents_t event, void * pData);

//static void SendNAK (ayla_Packet_t * receivedPacket, ayla_NAK_Error_t errorID);
static void SendFeatureMask (ayla_Packet_t * receivedPacket, uint8_t featureMask);
static void SendBackProperty (ayla_Packet_t * receivedPacket);
void EnableListenerService (void);
void SendFactoryReset(void);
static void OnWiFiStatusTimeout( TimerHandle_t xTimer );



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
///*******************************************************************************
//* @brief
//* @inputs None
//* @retval None
//* @author J-F. Simard
//* @date 2016/09/06
//*******************************************************************************/
//#pragma optimize=none
//static void SendNAK (ayla_Packet_t * receivedPacket, ayla_NAK_Error_t errorID)
//{
//    uint16_t packetSize = AYLA_HEADER_FOOTER_OFFSET + AYLA_TLV_BASE_LENGTH;
//
//    ayla_Packet_t packetToSend =
//    {
//        .ProtocolID = AYLAPROTOCOL_DATAOPS,
//        .OpCode = DATAOPCODE_NAK,
//    };
//    ayla_TLV_t * outgoingTLV = (ayla_TLV_t *)packetToSend.Payload;
//
//    packetToSend.RequestID_MSB = receivedPacket->RequestID_MSB;
//    packetToSend.RequestID_LSB = receivedPacket->RequestID_LSB;
//
//    outgoingTLV->Type = TLVTYPE_ERROR;
//    outgoingTLV->Length = 1;
//    outgoingTLV->Value[0] = errorID;
//
//    packetSize += outgoingTLV->Length;
//
//    HAL_QWiFiMsg ((uint8_t *)&packetToSend, packetSize);
//}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/10/11
*******************************************************************************/
static void SendFeatureMask (ayla_Packet_t * receivedPacket, uint8_t featureMask)
{
    aylaDataPoint_t * newDataPoint;

    MANDATORY_WIFI_MSG(newDataPoint,{

    newDataPoint = Ayla_CreateDataPoint(DATAOPCODE_NAK, receivedPacket->RequestID);
    Ayla_AddTLV_to_dataPoint(&newDataPoint, Ayla_CreateFeatureMaskTLV(featureMask));

    })

    HAL_QWiFiMsg (newDataPoint);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void * allocate_tlv_memory (size_t size)
{
    void * mem_ptr = NULL;

    while (NULL == mem_ptr)
    {
        mem_ptr = pvPortMalloc(size);

        if (NULL == mem_ptr)
        {
            osDelay(1);
        }
    }
    return mem_ptr;
}

/*******************************************************************************
* @brief  SendBackProperty function to return a specific property
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/10/03
*******************************************************************************/
static void SendBackProperty (ayla_Packet_t * receivedPacket)
{
    uint8_t propIndex;

    ayla_incomingTLV_t * firstTLV;
    ayla_incomingTLV_t * nextTLV;
    ayla_incomingTLV_t * nameTLV;
    static aylaTLV_t valueTLV =
    {
       .next = NULL,
    };

    firstTLV = (ayla_incomingTLV_t *)receivedPacket->Payload;

	nameTLV = NULL;

    if (firstTLV->Type == TLVTYPE_NAME)
    {
		nameTLV = firstTLV;
        nextTLV = (ayla_incomingTLV_t *)&nameTLV->Value[nameTLV->Length];

		if (TLVTYPE_LENGTH == nextTLV->Type)
		{
			valueTLV.isLongString = 1;
			valueTLV.longStringOffset = 0;
			valueTLV.longStringTotalLength = (uint16_t)DeserializeToInteger32Bits(nextTLV->Value, nextTLV->Length);
			nextTLV = (ayla_incomingTLV_t *)&nextTLV->Value[nextTLV->Length];
			if (NULL != valueTLV.value)
			{
				vPortFree(valueTLV.value);
			}

			/* malloc valueTLV.longStringTotalLength+1  */
			/*  +1 is to allow, in case a text TLV, to  */
			/*  add a '\0' character a the end of the   */
			/*  non NULL terminated string              */
			valueTLV.value = allocate_tlv_memory(valueTLV.longStringTotalLength+1);
		}
		else if (TLVTYPE_OFFSET == nextTLV->Type)
		{
			valueTLV.longStringOffset = (uint16_t)DeserializeToInteger32Bits(nextTLV->Value, nextTLV->Length);
			nextTLV = (ayla_incomingTLV_t *)&nextTLV->Value[nextTLV->Length];
		}
		else
		{
			valueTLV.isLongString = 0;
			valueTLV.longStringOffset = 0;

			if (NULL != valueTLV.value)
			{
				vPortFree(valueTLV.value);
			}

			/* malloc nextTLV->Length+1                 */
			/*  +1 is to allow, in case a text TLV, to  */
			/*  add a '\0' character a the end of the   */
			/*  non NULL terminated string              */
			valueTLV.value = allocate_tlv_memory(nextTLV->Length+1);
		}

		valueTLV.type = nextTLV->Type;
		valueTLV.length = nextTLV->Length;
		if (NULL != valueTLV.value)
		{
			memcpy (&valueTLV.value[valueTLV.longStringOffset], nextTLV->Value, valueTLV.length);
		}

		if (0 != valueTLV.isLongString)
		{
			if ((valueTLV.longStringOffset + valueTLV.length) < valueTLV.longStringTotalLength)
			{
				nameTLV = NULL;
			}
			else
			{
				valueTLV.length = valueTLV.longStringTotalLength;
			}
		}
    }

    if (nameTLV != NULL)
    {
		char propertyName[PROPERTY_NAME_MAX_LENGTH];

		stringNCopyWithNull (propertyName,
							(char*)nameTLV->Value,
							((nameTLV->Length+1)<sizeof(propertyName))?
									(nameTLV->Length+1):
									sizeof(propertyName));


        /************************************************************/
        /*      Search for property definition                      */
        /************************************************************/
        for (propIndex = 0; propIndex < MAX_PROPERTY_TYPE; propIndex++)
        {
            if (0 == strcmp ((const char*)propertyName, (const char *)propTable[propIndex].propertyName))
            {
                if (propTable[propIndex].respFunction != NULL)
                {
                    propTable[propIndex].respFunction((void*)THIS_THERMOSTAT, receivedPacket->RequestID);
                }
                vPortFree(valueTLV.value);
                valueTLV.value = NULL;
                valueTLV.isLongString = 0;
                valueTLV.longStringOffset = 0;
                valueTLV.longStringTotalLength = 0;
                break;
            }
        }
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/06
*******************************************************************************/
void EnableListenerService (void)
{
    HAL_QWiFiMsg (Ayla_CreateDataPoint (DATAOPCODE_ENABLESERVICELISTENER,0x000b));
}

/*******************************************************************************
* @brief Function that process the last WiFi Status event after a delay
* @inputs Timer handle
* @retval None
* @author Jean-Fran�ois Many
* @date 2018/10/22
*******************************************************************************/
static void OnWiFiStatusTimeout( TimerHandle_t xTimer )
{
    dbType_CloudConnectivity_t cloudInfo;
    uint8_t alert;

    switch(LastJoinStatus)
    {
        case WIFIJOIN_SUCCESS:
            DBTHRD_GetData(DBTYPE_CLOUDCONNECTIVITY, (void *)&cloudInfo, INDEX_DONT_CARE);
            if (cloudInfo.isConnectedToCloud != 1)
            {
                //New connection established, clear the alerts and resync the data
                cloudInfo.isConnectedToCloud = 1;
                DBTHRD_SetData(DBTYPE_CLOUDCONNECTIVITY,(void*)&cloudInfo, INDEX_DONT_CARE, DBCHANGE_CLOUD);
                alert = DB_CLEAR_ALERT | DBALERT_NO_WIFI;
                DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
                alert = DB_CLEAR_ALERT | DBALERT_SERVER_PROBLEM;
                DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
                HAL_ResetWiFiInitStateMachine();
                WifiNetworksJoin_StateMachine(WIFIJOINEVENT_CONNECTED, NULL);
                WiFiRadioInitStateMachine(WIFISTARTEVENT_BEGININIT, NULL);
            }
            else
            {
                //Still connected, no need to resync, but there may have been a disconnection,
                //so make sure the listener service is enabled
                EnableListenerService();
            }
            break;

        case WIFIJOIN_CONNECT_TIMEOUT:
        case WIFIJOIN_INVALID_KEY:
        case WIFIJOIN_NOT_AUTHENTICATED:
        case WIFIJOIN_WRONG_KEY:
            WifiNetworksJoin_StateMachine(WIFIJOINEVENT_BADPASSWORD, NULL);
            DBTHRD_GetData(DBTYPE_CLOUDCONNECTIVITY, (void *)&cloudInfo, INDEX_DONT_CARE);
            alert = DB_CLEAR_ALERT | DBALERT_SERVER_PROBLEM;
            DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            alert = DB_SET_ALERT | DBALERT_NO_WIFI;
            DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            cloudInfo.isConnectedToCloud = 0;
            DBTHRD_SetData(DBTYPE_CLOUDCONNECTIVITY,(void*)&cloudInfo, INDEX_DONT_CARE, DBCHANGE_CLOUD);
            SetDisconnectReason((uint8_t)LastJoinStatus);

            break;

        case WIFIJOIN_NETWK_NOT_FOUND:
            WifiNetworksJoin_StateMachine(WIFIJOINEVENT_NOTFOUND, NULL);
            DBTHRD_GetData(DBTYPE_CLOUDCONNECTIVITY, (void *)&cloudInfo, INDEX_DONT_CARE);
            alert = DB_CLEAR_ALERT | DBALERT_SERVER_PROBLEM;
            DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            alert = DB_SET_ALERT | DBALERT_NO_WIFI;
            DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            cloudInfo.isConnectedToCloud = 0;
            DBTHRD_SetData(DBTYPE_CLOUDCONNECTIVITY,(void*)&cloudInfo, INDEX_DONT_CARE, DBCHANGE_CLOUD);
            SetDisconnectReason((uint8_t)LastJoinStatus);
            break;

        case WIFIJOIN_AP_DISCONNECTED:
        case WIFIJOIN_LOSS_OF_SIGNAL:
            DBTHRD_GetData(DBTYPE_CLOUDCONNECTIVITY, (void *)&cloudInfo, INDEX_DONT_CARE);
            cloudInfo.isConnectedToCloud = 0;
            DBTHRD_SetData(DBTYPE_CLOUDCONNECTIVITY,(void*)&cloudInfo, INDEX_DONT_CARE, DBCHANGE_CLOUD);
            alert = DB_CLEAR_ALERT | DBALERT_SERVER_PROBLEM;
            DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            alert = DB_SET_ALERT | DBALERT_NO_WIFI;
            DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            WifiNetworksJoin_StateMachine(WIFIJOINEVENT_CONNECTIONLOSS, NULL);
            SetDisconnectReason((uint8_t)LastJoinStatus);
            break;

        case WIFIJOIN_RES_UNAVAILABLE:
        case WIFIJOIN_NO_PROFILE_AVAIL:
        case WIFIJOIN_SEC_METH_NOT_SUPP:
        case WIFIJOIN_NETW_TYPE_UNSUPP:
        case WIFIJOIN_WIFI_PROTOCOL_ERR:
        case WIFIJOIN_SETUP_UNSUPP:
            WifiNetworksJoin_StateMachine(WIFIJOINEVENT_ERROR, NULL);
            break;

        case WIFIJOIN_ADS_AUTH_ERROR:
        case WIFIJOIN_DNS_LOOKUP_FAILED:
        case WIFIJOIN_ADS_CONN_REDIRECT:
        case WIFIJOIN_ADS_CONN_TIMEOUT:
        case WIFIJOIN_NO_IP_ADDRESS:
        case WIFIJOIN_NO_IP_ROUTE:
        case WIFIJOIN_NO_DNS_SERVER:
            DBTHRD_GetData(DBTYPE_CLOUDCONNECTIVITY, (void *)&cloudInfo, INDEX_DONT_CARE);
            cloudInfo.isConnectedToCloud = 0;
            DBTHRD_SetData(DBTYPE_CLOUDCONNECTIVITY,(void*)&cloudInfo, INDEX_DONT_CARE, DBCHANGE_CLOUD);
            alert = DB_CLEAR_ALERT | DBALERT_NO_WIFI;
            DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            alert = DB_SET_ALERT | DBALERT_SERVER_PROBLEM;
            DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            WifiNetworksJoin_StateMachine(WIFIJOINEVENT_NOSERVER, NULL);
            SetDisconnectReason((uint8_t)LastJoinStatus);
            break;

        case WIFIJOIN_OPERATION_IN_PROG:
        default:
            break;
    }
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/06
*******************************************************************************/
#pragma optimize = none
void Ayla_HandleDataOperation (ayla_Packet_t * receivedPacket, uint16_t packetLength)
{
    ayla_incomingTLV_t * firstTLV;
    dbType_CloudConnectivity_t cloudInfo;
//    uint16_t radioVersion;

    firstTLV = (ayla_incomingTLV_t *)receivedPacket->Payload;

    switch (Get_OpCode((AylaHeader_t){.opCode = receivedPacket->OpCode,.protocolId = receivedPacket->ProtocolID}))
    {
    case DATAOPCODE_SENDTLVVERSION:
        break;
    case DATAOPCODE_PROPVALUEREQUEST:
        break;
    case DATAOPCODE_RXTLV:
        ParseReceivedProperties(receivedPacket, packetLength);
        break;

    case DATAOPCODE_NAK:
        break;
    case DATAOPCODE_REQUESTPROP:
        if (packetLength > 0)
        {
            SendBackProperty(receivedPacket);
        }
        else
        {
            SendFeatureMask (receivedPacket, 0x06);     //feature mask :
                                                        // 0x02 => Host MCU OTA
                                                        // 0x04 => Time Subscription
        }
        break;
    case DATAOPCODE_REQUESTPROP_RESP:
        break;
    case DATAOPCODE_REQUESTNEXTPROP:
        break;
    case DATAOPCODE_SENDTLVS:
        break;
    case DATAOPCODE_FILEDATAPOINT_REQ:
        break;
    case DATAOPCODE_FILEDATAPOINT_RESP:
        break;
    case DATAOPCODE_FILEDATAPOINT_CREATE:
        break;
    case DATAOPCODE_FILEDATAPOINT_FETCHED:
        break;
    case DATAOPCODE_FILEDATAPOINT_STOP:
        break;
    case DATAOPCODE_FILEDATAPOINT_SEND:
        break;
    case DATAOPCODE_CONNECTSTATUS:
        if ((firstTLV->Type == TLVTYPE_NODES))
        {
            if (0 != (firstTLV->Value[0]&0x01))
            {
                LastJoinStatus = WIFIJOIN_SUCCESS;
            }
            else
            {
                DBTHRD_GetData(DBTYPE_CLOUDCONNECTIVITY, (void *)&cloudInfo, INDEX_DONT_CARE);
                if (cloudInfo.isConnectedToCloud == 0)
                {
                    LastJoinStatus = WIFIJOIN_NETWK_NOT_FOUND;
                }
                else
                {
                    LastJoinStatus = WIFIJOIN_ADS_CONN_TIMEOUT;
                }
            }

            //Start timer to let the Wi-Fi join status to stabilize
            if (NULL == WiFi_Status_Timer)
            {
                WiFi_Status_Timer = xTimerCreate ("Wi-Fi Status Timer",
                                                pdMS_TO_TICKS(WIFI_STATUS_TIMEOUT),
                                                pdFALSE,     //one-shot
                                                (void*)0,
                                                OnWiFiStatusTimeout);
            }
            if (NULL != WiFi_Status_Timer)
            {
                xTimerStart(WiFi_Status_Timer,0);
            }
        }

        break;
    case DATAOPCODE_ECHOFAIL:
        break;
    case DATAOPCODE_ENABLESERVICELISTENER:
        break;
    case DATAOPCODE_ERROR:
        break;
    case DATAOPCODE_CONFIRM:
        break;
    case DATAOPCODE_PROPNOTIFICATION:
        break;
    case DATAOPCODE_EVENTNOTIFICATION:
        while (packetLength)
        {
            if (firstTLV->Type == TLVTYPE_REGISTRATION)
            {
                DBTHRD_GetData(DBTYPE_CLOUDCONNECTIVITY, (void *)&cloudInfo, INDEX_DONT_CARE);
                if ((firstTLV->Value[0] & 0x01) == 0)
                {
                    //If we were registered before
                    if (cloudInfo.isRegistered == CLOUD_REGISTERED)
                    {
                        //Clear residence data
                        DBTHRD_CLEAR_RESIDENCE_DATA();
                    }
                    cloudInfo.isConnectedToCloud = 1;
                    cloudInfo.isRegistered = CLOUD_UNREGISTERED;
                    DBTHRD_SetData(DBTYPE_CLOUDCONNECTIVITY,(void*)&cloudInfo, INDEX_DONT_CARE, DBCHANGE_REGISTER);
                }
                else
                {
                    //If we were not registered before
                    if (cloudInfo.isRegistered == CLOUD_UNREGISTERED)
                    {
                        //Init the cloud data
                        DBTHRD_INITIALIZE_CLOUD_DATA();
                    }
                    cloudInfo.isConnectedToCloud = 1;
                    HAL_ResetWiFiInitStateMachine();
                    WifiNetworksJoin_StateMachine(WIFIJOINEVENT_CONNECTED, NULL);
                    WiFiRadioInitStateMachine(WIFISTARTEVENT_BEGININIT, NULL);
                    SetDisconnectReason(0x99);
                }
            }
            if (firstTLV->Type == TLVTYPE_WIFISTATUS)
            {
                uint8_t ssidLength;
                uint8_t joinStatus;

                ssidLength = firstTLV->Value[0];
                joinStatus = firstTLV->Value[ssidLength + 1];
                LastJoinStatus = (ayla_wifiStatus_t)joinStatus;

                //Start timer to let the Wi-Fi join status to stabilize
                if (NULL == WiFi_Status_Timer)
                {
                    WiFi_Status_Timer = xTimerCreate ("Wi-Fi Status Timer",
                                                    pdMS_TO_TICKS(WIFI_STATUS_TIMEOUT),
                                                    pdFALSE,     //one-shot
                                                    (void*)0,
                                                    OnWiFiStatusTimeout);
                }
                if (NULL != WiFi_Status_Timer)
                {
                    xTimerStart(WiFi_Status_Timer,0);
                }
            }
            packetLength -= (firstTLV->Length + 2);
            firstTLV = (ayla_incomingTLV_t *)(receivedPacket->Payload + (firstTLV->Length + 2));
        }
        break;

    default :
        break;
    }
}

/*******************************************************************************
* @brief  Function that retrieves all the "To Device" properties from the cloud
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/18
*******************************************************************************/
void GetAllProperties(void)
{
    HAL_QWiFiMsg (Ayla_CreateDataPoint (DATAOPCODE_PROPVALUEREQUEST, 0x000a));
}

/*******************************************************************************
* @brief  Function that retrieves a specific property from the cloud
* @inputs wifiType: specific property to fetch
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/24
*******************************************************************************/
void GetSpecificProperty(WIFITYPE_t wifiType)
{
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (DATAOPCODE_PROPVALUEREQUEST, ((uint16_t)wifiType<<8) | 0x66);

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateNameTLV (propTable[wifiType].propertyName));


    HAL_QWiFiMsg (newDataPoint);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2019/06/10
*******************************************************************************/
uint8_t GetDisconnectReason(void)
{
    return DisconnectReason;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2019/06/10
*******************************************************************************/
void SetDisconnectReason(uint8_t reason)
{
    DisconnectReason = reason;
}

/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
