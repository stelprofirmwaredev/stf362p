/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    AylaMetaData.c
* @date    2016/12/09
* @authors J-F. Simard
* @brief   
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include ".\HAL\inc\AylaMetaData.h"
#include ".\HAL\inc\AylaDefinitions.h"
#include ".\HAL\inc\HAL_WiFiTypes.h"
#include ".\HAL\inc\AylaProperties.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define METATAG_EVENT       "\"mnuboEvent"
#define METATAG_OBJECT      "mnuboObject"
#define METAVALUESTART      "\":{"


/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
extern const prop_t propTable[];


/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
const metaValueDescriptor_t MetaValue_AmbientTemperature = 
{
    .valueName = "AmbientTemperature",
    .valueDataType = DBTYPE_AMBIENT_TEMPERATURE,
    .nextValue = NULL,
};

const metaValueDescriptor_t MetaValue_AmbientSetpoint = 
{
    .valueName = "AmbientSetpoint",
    .valueDataType = DBTYPE_AMBIENT_SETPOINT,
    .nextValue = &MetaValue_AmbientTemperature,
};


const metaEvent_t UpdateSetpointEvent = 
{
    .eventType = "update_setpoint",
    .metaValue = &MetaValue_AmbientSetpoint,
};




/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
uint16_t  AddMetaValue (uint8_t * metaBuffer, WIFITYPE_t property);


/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
uint16_t  AddMetaValue (uint8_t * metaBuffer, WIFITYPE_t property)
{
    uint16_t metaSize = 0;

    if (propTable[property].metaEvent != NULL)
    {
        /*  Add x_event_type        */
        /*  Add x_device_id (DSN)   */
        /*  Add prop 1              */
        /*  Add prop 2              */
        /*  Add prop n              */
    }
    
    return metaSize;
}




/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/12/09
*******************************************************************************/
uint16_t  AylaAddMetaEvent (uint8_t * metaBuffer, uint8_t metaNumber, WIFITYPE_t property)
{
    uint16_t metaSize = AYLA_TLV_BASE_LENGTH;
    
    ayla_TLV_t * metaTLV;
    
    metaTLV = (ayla_TLV_t *)metaBuffer;
    
    metaTLV->Type = TLVTYPE_DATAPOINTMETADATA;
    sprintf ((char *)metaTLV->Value, "%s%d%s\"",METATAG_EVENT,metaNumber,METAVALUESTART);
    metaTLV->Length = strlen((char const*)metaTLV->Value);
    metaSize = metaTLV->Length;
    
    AddMetaValue ((uint8_t *)&metaTLV->Value[metaTLV->Length], property);
    return metaSize;
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
