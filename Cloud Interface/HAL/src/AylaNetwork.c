/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    AylaNetwork.c
* @date    2016/09/20
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <string.h>
#include ".\HAL\inc\AylaNetwork.h"

#include ".\HAL\inc\HAL_CloudInterface.h"
#include ".\HAL\inc\HAL_WiFiTypes.h"
#include ".\HAL\inc\AylaDefinitions.h"

#include ".\BSP\Ayla WM-N-BM-30\inc\BSP_Ayla_WM_N_MB_30.h"

#include ".\DB\inc\THREAD_DBDataTypes.h"
#include ".\DB\inc\THREAD_DB.h"

#include ".\strings\inc\strings_tool.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define MAX_SCAN_ATTEMPTS   20

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
wifiScanSMStates_t wifiScanCurrentState;
wifiJoinSMStates_t wifiJoinCurrentState;

wifiScanResult_t ScanResults;

TimerHandle_t WiFiScanTimer;
TimerHandle_t WiFiJoinTimer;

void (*endOfScancB)(void*);
void (*cbCurrentNetwork)(void);
void (*cbjoinNetwork)(WiFiJoinStatus_t);

static uint8_t scanAttempt = 0;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static void StartWiFiScan (void);
static void JoinWiFiTimeOut (void*);
void SnapShotScanResult(void);
void PollWiFiScanReady (TimerHandle_t xTimer);
extern void WiFiRadioInitStateMachine (wifiRadioInitEvents_t event, void * pData);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/12
*******************************************************************************/
uint8_t isScanReady (ayla_incomingTLV_t * readyTLV)
{
    uint8_t result = 0;         //scan not ready

    if (readyTLV->Value[0] != 0)
    {
        result = 1;
    }

    return (result);
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/10/14
*******************************************************************************/
void GetSignalStrength (void)
{
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (CONTROLOPCODE_GET_CONFIGITEMS, 0x00bb);

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateConfigTokenTLV (3,
                             CONFTOKEN_WIFI, CONFTOKEN_STATUS, CONFTOKEN_RSSI));


    HAL_QWiFiMsg (newDataPoint);
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/14
*******************************************************************************/
void GetActiveProfile (void (*currentNetworkcB)(void))
{
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (CONTROLOPCODE_GET_CONFIGITEMS, 0x00a8);

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateConfigTokenTLV (3,
                             CONFTOKEN_WIFI, CONFTOKEN_STATUS, CONFTOKEN_PROFILE));


    HAL_QWiFiMsg (newDataPoint);

    cbCurrentNetwork = currentNetworkcB;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/12
*******************************************************************************/
void GetSecurity (uint8_t scanResultNumber)
{
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (CONTROLOPCODE_GET_CONFIGITEMS, 0x00a3);

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateConfigTokenTLV (5,
                             CONFTOKEN_WIFI, CONFTOKEN_SCAN, CONFTOKEN_N,
                             scanResultNumber, CONFTOKEN_SECURITY));


    HAL_QWiFiMsg (newDataPoint);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/12
*******************************************************************************/
void GetSSID (uint8_t scanResultNumber)
{
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (CONTROLOPCODE_GET_CONFIGITEMS, 0x00a4);

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateConfigTokenTLV (5,
                             CONFTOKEN_WIFI, CONFTOKEN_SCAN, CONFTOKEN_N,
                             scanResultNumber, CONFTOKEN_SSID));

    HAL_QWiFiMsg (newDataPoint);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/06
*******************************************************************************/
static void StartWiFiScan (void)
{
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (CONTROLOPCODE_SET_CONFIGITEMS, 0x0000);

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateConfigTokenTLV (3,
                             CONFTOKEN_WIFI, CONFTOKEN_SCAN, CONFTOKEN_START));

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateIntegerTLV (1));


    HAL_QWiFiMsg (newDataPoint);
}

/*******************************************************************************
* @brief  Send Factory Reset TLV (erases the WiFi profiles)
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/02/14
*******************************************************************************/
void SendFactoryReset(void)
{
    HAL_QWiFiMsg (Ayla_CreateDataPoint (CONTROLOPCODE_LOAD_FACTORYCONFIG, 0x0077));
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/10/05
*******************************************************************************/
static void JoinWiFiTimeOut (void *pData)
{
    (void)pData;
    WifiNetworksJoin_StateMachine(WIFIJOINEVENT_CONNECTIONTIMEOUT, NULL);
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/10/05
*******************************************************************************/
void StartNewWiFiScan (void (*callback)(void*))
{
    scanAttempt = 0;
    endOfScancB = callback;
    WifiNetworksScan_StateMachine(WIFISCANEVENT_STARTNEWSCAN,NULL);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/12/12
*******************************************************************************/
void StopWiFiScan(void)
{
    WifiNetworksScan_StateMachine(WIFISCANEVENT_STOPSCAN,NULL);
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/10/05
*******************************************************************************/
void WifiNetworksJoin_StateMachine (wifiJoinSMEvents_t event, void * pData)
{
    wifi_joinNetworks_t * networkInfo;
    dbType_CloudConnectivity_t cloudInfo;

    switch (wifiJoinCurrentState)
    {
    /************************************/
    /************************************/
    case WIFIJOINSM_READYTOJOIN:
        switch (event)
        {
        /************************************/
        /************************************/
        case WIFIJOINEVENT_JOINNETWORK:
            networkInfo = (wifi_joinNetworks_t*)pData;
            cbjoinNetwork = networkInfo->joinNetworkcB;
            AylaJoinNetwork(networkInfo->scanId,networkInfo->key);

            WiFiJoinTimer = xTimerCreate ("WiFi Join Timer",
                                           pdMS_TO_TICKS(WIFI_JOIN_TIMEOUT),
                                           pdFALSE,                  //no auto reload
                                           (void *) 0,
                                           JoinWiFiTimeOut);
            xTimerStart (WiFiJoinTimer, 0);

            wifiJoinCurrentState = WIFIJOINSM_JOINING;
            break;


        /************************************/
        /*   Undefined event actions        */
        /************************************/
        case WIFIJOINEVENT_NAKRESPONSE:
        case WIFIJOINEVENT_CONNECTED:
        case WIFIJOINEVENT_CONNECTIONTIMEOUT:
        case WIFIJOINEVENT_BADPASSWORD:
        case WIFIJOINEVENT_NOTFOUND:
        case WIFIJOINEVENT_CONNECTIONLOSS:
        case WIFIJOINEVENT_ERROR:
        case WIFIJOINEVENT_NOSERVER:
        default:
            break;
        }
        break;

    /************************************/
    /************************************/
    case WIFIJOINSM_JOINING:
        switch (event)
        {
        /************************************/
        /************************************/

        case WIFIJOINEVENT_NOTFOUND:
            if (cbjoinNetwork != NULL)
            {
                cbjoinNetwork(WIFI_JOIN_NO_NETWORK);
            }
            cbjoinNetwork = NULL;

            wifiJoinCurrentState = WIFIJOINSM_READYTOJOIN;
            xTimerDelete(WiFiJoinTimer,0);
            break;


        case WIFIJOINEVENT_ERROR:
        case WIFIJOINEVENT_NAKRESPONSE:
        case WIFIJOINEVENT_CONNECTIONTIMEOUT:
            if (cbjoinNetwork != NULL)
            {
                cbjoinNetwork(WIFI_JOIN_GENERIC_ERROR);
            }
            cbjoinNetwork = NULL;

            wifiJoinCurrentState = WIFIJOINSM_READYTOJOIN;
            xTimerDelete(WiFiJoinTimer,0);
            break;

        /************************************/
        /************************************/
        case WIFIJOINEVENT_CONNECTED:
            if (cbjoinNetwork != NULL)
            {
                cbjoinNetwork(WIFI_JOIN_SUCCESS);
            }
            DBTHRD_GetData(DBTYPE_CLOUDCONNECTIVITY, (void *)&cloudInfo, INDEX_DONT_CARE);
            cloudInfo.isRegistered = CLOUD_UNKNOWN;
            DBTHRD_SetData(DBTYPE_CLOUDCONNECTIVITY,(void*)&cloudInfo, INDEX_DONT_CARE, DBCHANGE_CLOUD);
            cbjoinNetwork = NULL;

            wifiJoinCurrentState = WIFIJOINSM_READYTOJOIN;
            xTimerDelete(WiFiJoinTimer,0);
            break;

        /************************************/
        /************************************/
        case WIFIJOINEVENT_BADPASSWORD:
            if (cbjoinNetwork != NULL)
            {
                cbjoinNetwork(WIFI_JOIN_BAD_PASSWORD);
            }
            cbjoinNetwork = NULL;

            wifiJoinCurrentState = WIFIJOINSM_READYTOJOIN;
            xTimerDelete(WiFiJoinTimer,0);
            break;

        /************************************/
        /************************************/
        case WIFIJOINEVENT_NOSERVER:
            if (cbjoinNetwork != NULL)
            {
                cbjoinNetwork(WIFI_JOIN_NO_SERVER);
            }
            cbjoinNetwork = NULL;

            wifiJoinCurrentState = WIFIJOINSM_READYTOJOIN;
            xTimerDelete(WiFiJoinTimer,0);
            break;

        /************************************/
        /*   Undefined event actions        */
        /************************************/
        case WIFIJOINEVENT_CONNECTIONLOSS:
        case WIFIJOINEVENT_JOINNETWORK:
        default:
            break;
        }
        break;

    default:
        break;
    }

}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/06
*******************************************************************************/
void WifiNetworksScan_StateMachine (wifiScanSMEvents_t event, void * pData)
{
    static uint8_t numberOfWiFiNetwork = 0;
    uint8_t * pUint8;
    ayla_incomingTLV_t * pTLV;

    switch (wifiScanCurrentState)
    {
    /************************************/
    /*          State Scan Init         */
    /************************************/
    case WIFISCANSM_INIT:
        switch (event)
        {
        /************************************/
        /************************************/
        case WIFISCANEVENT_STARTNEWSCAN:
            StartWiFiScan();
            WiFiScanTimer = xTimerCreate ("WiFi Scan Timer",
                                           pdMS_TO_TICKS(WIFI_SCAN_POLL_READY_PERIOD),
                                           pdFALSE,                  //no auto reload
                                           (void *) 0,
                                           WIFITHRD_PollWiFiScanReady);
            xTimerStart (WiFiScanTimer, 0);

            wifiScanCurrentState = WIFISCANSM_WAITSCANCOMPLETE;
            break;


        /****************************************/
        /*              unhandled events        */
        /****************************************/
        case WIFISCANEVENT_SCANCOMPLETE:
        case WIFISCANEVENT_SNAPSHOTCAPTURED:
        case WIFISCANEVENT_SSIDRECEIVED:
        case WIFISCANEVENT_SECURITYRECEIVED:
        case WIFISCANEVENT_STOPSCAN:
        default:
            break;
        }
        break;

    /************************************/
    /*          State Start Scan        */
    /************************************/
    case WIFISCANSM_WAITSCANCOMPLETE:
        switch (event)
        {
        /************************************/
        /************************************/
        case WIFISCANEVENT_SCANCOMPLETE:
            WIFITHRD_SnapShotScanResult();
            wifiScanCurrentState = WIFISCANSM_CAPTURESCANRESULT;
            break;

        case WIFISCANEVENT_STOPSCAN:
            wifiScanCurrentState = WIFISCANSM_INIT;
            break;

        /****************************************/
        /*              unhandled events        */
        /****************************************/
        case WIFISCANEVENT_STARTNEWSCAN:
        case WIFISCANEVENT_SNAPSHOTCAPTURED:
        case WIFISCANEVENT_SSIDRECEIVED:
        case WIFISCANEVENT_SECURITYRECEIVED:
        default:
            break;
        }
        break;

    /************************************/
    /*          State Scan Init         */
    /************************************/
    case WIFISCANSM_CAPTURESCANRESULT:
        switch (event)
        {
        /************************************/
        /************************************/
        case WIFISCANEVENT_SNAPSHOTCAPTURED:
            numberOfWiFiNetwork = 0;
            if (pData != NULL)
            {
                pUint8 = (uint8_t *) pData;
                numberOfWiFiNetwork = *pUint8;
            }

            if (numberOfWiFiNetwork > 0)
            {
                ScanResults.numberOfResults = 0;

                WIFITHRD_GetSSID (numberOfWiFiNetwork-1);
                numberOfWiFiNetwork--;
            }
            break;

        /************************************/
        /************************************/
        case WIFISCANEVENT_SSIDRECEIVED:
            if (pData != NULL)
            {
                uint8_t size = sizeof(ScanResults.scanTable[ScanResults.numberOfResults].SSID);
                pTLV = (ayla_incomingTLV_t *)pData;

                if ((pTLV->Length+1) < size)        //add + 1 to hold '\0' at the end
                {
                    size = pTLV->Length+1;
                }
                stringNCopyWithNull ((char*)ScanResults.scanTable[ScanResults.numberOfResults].SSID,
                                     (char*)pTLV->Value,
                                     size);
            }

            ScanResults.numberOfResults++;
            if (numberOfWiFiNetwork > 0)
            {
                WIFITHRD_GetSSID (numberOfWiFiNetwork-1);
                numberOfWiFiNetwork--;
            }
            else
            {
                if (endOfScancB != NULL)
                {
                    endOfScancB((void *)&ScanResults);
                }

                numberOfWiFiNetwork = ScanResults.numberOfResults;
                ScanResults.numberOfResults = 0;

                WIFITHRD_GetSecurity (numberOfWiFiNetwork-1);
                numberOfWiFiNetwork--;
            }
            break;

        /************************************/
        /************************************/
        case WIFISCANEVENT_SECURITYRECEIVED:
            if (pData != NULL)
            {
                pTLV = (ayla_incomingTLV_t *)pData;
                ScanResults.scanTable[ScanResults.numberOfResults].security =
                    pTLV->Value[0];
            }

            ScanResults.numberOfResults++;
            if (numberOfWiFiNetwork > 0)
            {
                WIFITHRD_GetSecurity (numberOfWiFiNetwork-1);
                numberOfWiFiNetwork--;
            }
            else
            {
                if (endOfScancB != NULL)
                {
                    endOfScancB((void *)&ScanResults);
                }

                wifiScanCurrentState = WIFISCANSM_INIT;
                endOfScancB = NULL;
            }
            break;

        case WIFISCANEVENT_STOPSCAN:
            wifiScanCurrentState = WIFISCANSM_INIT;
            break;

        /****************************************/
        /*              unhandled events        */
        /****************************************/
        case WIFISCANEVENT_STARTNEWSCAN:
        case WIFISCANEVENT_SCANCOMPLETE:
        default:
            break;
        }
        break;
    default:
        break;
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/06
*******************************************************************************/
void PollWiFiScanReady (TimerHandle_t xTimer)
{
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (CONTROLOPCODE_GET_CONFIGITEMS, 0x00a5);

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateConfigTokenTLV (3,
                             CONFTOKEN_WIFI, CONFTOKEN_SCAN, CONFTOKEN_READY));

    HAL_QWiFiMsg (newDataPoint);
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/12
*******************************************************************************/
void SnapShotScanResult(void)
{
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (CONTROLOPCODE_GET_CONFIGITEMS, 0x00a6);

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateConfigTokenTLV (3,
                             CONFTOKEN_WIFI, CONFTOKEN_SCAN, CONFTOKEN_N));

    HAL_QWiFiMsg (newDataPoint);
}


/*******************************************************************************
* @brief  Parse a configuration token response : /wifi/scan
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/19
*******************************************************************************/
void ControlOps_ParseConfToken_WiFiScan (ayla_Packet_t * receivedPacket, uint16_t payloadLength)
{
    ayla_incomingTLV_t * firstTLV;
    ayla_incomingTLV_t * readyTLV;
    ayla_incomingTLV_t * ssidTLV;

    if (payloadLength > 0)
    {
        firstTLV = (ayla_incomingTLV_t *)receivedPacket->Payload;

        switch (firstTLV->Value[2])
        {
        /************************************/
        /* wifi/scan/ready/                 */
        /************************************/
        case CONFTOKEN_READY:
            readyTLV = (ayla_incomingTLV_t *)&firstTLV->Value[3];
            if (isScanReady(readyTLV))
            {
                xTimerDelete (WiFiScanTimer, 0);
                WifiNetworksScan_StateMachine(WIFISCANEVENT_SCANCOMPLETE, NULL);
            }
            else
            {
                scanAttempt++;
                if (scanAttempt >= MAX_SCAN_ATTEMPTS)
                {
                    xTimerDelete (WiFiScanTimer, 0);
                    WifiNetworksScan_StateMachine(WIFISCANEVENT_SCANCOMPLETE, NULL);
                }
                else
                {
                    xTimerStart (WiFiScanTimer, 0);
                }
            }
            break;

        /************************************/
        /* wifi/scan/n                      */
        /************************************/
        case CONFTOKEN_N:
            /************************************/
            /************************************/
            if (firstTLV->Length == 3)
            {
                ssidTLV = (ayla_incomingTLV_t *)&firstTLV->Value[3];
                WifiNetworksScan_StateMachine(WIFISCANEVENT_SNAPSHOTCAPTURED, (void *)&ssidTLV->Value[0]);
            }

            /************************************/
            /* wifi/scan/n/<n>/                 */
            /************************************/
            else if (firstTLV->Length == 5)
            {
                switch (firstTLV->Value[4])
                {
                /************************************/
                /* wifi/scan/n/<n>/ssid             */
                /************************************/
                case CONFTOKEN_SSID:
                    ssidTLV = (ayla_incomingTLV_t *)&firstTLV->Value[5];
                    WifiNetworksScan_StateMachine(WIFISCANEVENT_SSIDRECEIVED,(void *)ssidTLV);
                    break;

                /************************************/
                /* wifi/scan/n/<n>/ssid             */
                /************************************/
                case CONFTOKEN_SECURITY:
                    ssidTLV = (ayla_incomingTLV_t *)&firstTLV->Value[5];
                    WifiNetworksScan_StateMachine(WIFISCANEVENT_SECURITYRECEIVED,(void *)ssidTLV);
                    break;

                default:
                    break;
                }
            }
            break;

        /************************************/
        /************************************/
        default:
            break;
        }
    }
}

/*******************************************************************************
* @brief  Parse a configuration token response : /wifi/profile
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/19
*******************************************************************************/
#pragma optimize=none
void ControlOps_ParseConfToken_WiFiProfile (ayla_Packet_t * receivedPacket, uint16_t payloadLength)
{
    ayla_incomingTLV_t * firstTLV;

    if (payloadLength > 0)
    {
        firstTLV = (ayla_incomingTLV_t *)receivedPacket->Payload;

        switch (firstTLV->Value[3])
        {
        /************************************/
        /* wifi/profile/ssid                */
        /************************************/
        case CONFTOKEN_SSID:
            WiFiRadioInitStateMachine(WIFISTARTEVENT_SSID_RECEIVED,
                                      firstTLV);
            break;


        /************************************/
        /* wifi/profile/enable              */
        /************************************/
        case CONFTOKEN_ENABLE:
            break;

        /************************************/
        /* default :  wifi/profile/         */
        /************************************/
        default:
            break;
        }
    }
}

/*******************************************************************************
* @brief  Parse a configuration token response : /wifi/status
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/19
*******************************************************************************/
#pragma optimize=none
void ControlOps_ParseConfToken_WiFiStatus (ayla_Packet_t * receivedPacket, uint16_t payloadLength)
{
    ayla_incomingTLV_t * firstTLV;
    ayla_incomingTLV_t * activeProfileTLV;
    ayla_incomingTLV_t * rssiTLV;
    dbType_CloudConnectivity_t cloudInfo;

    if (payloadLength > 0)
    {
        firstTLV = (ayla_incomingTLV_t *)receivedPacket->Payload;

        switch (firstTLV->Value[2])
        {
        /************************************/
        /* wifi/status/profile              */
        /************************************/
        case CONFTOKEN_PROFILE:
            activeProfileTLV = (ayla_incomingTLV_t * )&firstTLV->Value[3];
            DBTHRD_SetData(DBTYPE_ACTIVEWIFIPROFILE, &activeProfileTLV->Value[0], INDEX_DONT_CARE, DBCHANGE_CLOUD);
            if (cbCurrentNetwork != NULL)
            {
                cbCurrentNetwork();
                cbCurrentNetwork = NULL;
            }
            break;

        /************************************/
        /* wifi/status/rssi                 */
        /************************************/
        case CONFTOKEN_RSSI:
            rssiTLV = (ayla_incomingTLV_t * )&firstTLV->Value[3];
            DBTHRD_GetData(DBTYPE_CLOUDCONNECTIVITY, (void *)&cloudInfo, INDEX_DONT_CARE);
            cloudInfo.signalStrength = rssiTLV->Value[0];
            DBTHRD_SetData(DBTYPE_CLOUDCONNECTIVITY,(void*)&cloudInfo, INDEX_DONT_CARE, DBCHANGE_CLOUD);
            break;

        default:
            break;
        }
    }
}



/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/06
*******************************************************************************/
#pragma optimize = none
void AylaJoinNetwork (uint8_t network, uint8_t* pKey)
{
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (CONTROLOPCODE_WIFI_JOIN, 0x0080);

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateTextTLV ((char*)&ScanResults.scanTable[network].SSID));

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateIntegerTLV (ScanResults.scanTable[network].security));

    Ayla_AddTLV_to_dataPoint (&newDataPoint,
                              Ayla_CreateBinaryTLV (pKey, strlen((char const*)pKey)));

    HAL_QWiFiMsg (newDataPoint);
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/14
*******************************************************************************/
void LoadWiFiProfiles (uint8_t profileID)
{
    aylaDataPoint_t * newDataPoint;

    newDataPoint = Ayla_CreateDataPoint (CONTROLOPCODE_GET_CONFIGITEMS, 0x00a7);

    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateConfigTokenTLV (4,
                             CONFTOKEN_WIFI, CONFTOKEN_PROFILE,
                             profileID, CONFTOKEN_SSID));


    HAL_QWiFiMsg (newDataPoint);
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
