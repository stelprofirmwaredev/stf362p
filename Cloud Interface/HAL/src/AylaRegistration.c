/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    AylaRegistration.c
* @date    2016/10/06
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include ".\HAL\inc\AylaRegistration.h"
#include ".\HAL\inc\AylaDefinitions.h"
#include ".\HAL\inc\HAL_CloudInterface.h"
#include ".\DB\inc\THREAD_DB.h"

#include ".\BSP\Ayla WM-N-BM-30\inc\BSP_Ayla_WM_N_MB_30.h"

#include ".\strings\inc\strings_tool.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
void (*cbGetRegToken)(RegisterStatus_t, uint8_t*);
void (*cbGetRegStatus)(RegisterStatus_t, uint8_t*);


/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
extern void WiFiRadioInitStateMachine (wifiRadioInitEvents_t event, void * pData);


/*******************************************************************************
*    Private functions definitions
*******************************************************************************/


/*******************************************************************************
*    Public functions definitions
*******************************************************************************/

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/28
*******************************************************************************/
void GetRegistrationToken (void (*regTokencB)(RegisterStatus_t, uint8_t*))
{
    aylaDataPoint_t * newDataPoint;
    
    newDataPoint = Ayla_CreateDataPoint (CONTROLOPCODE_GET_CONFIGITEMS, 0x00aa);
    
    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateConfigTokenTLV (2, 
                             CONFTOKEN_CLIENT, CONFTOKEN_REG));
                              
    
    cbGetRegToken = regTokencB;
    HAL_QWiFiMsg (newDataPoint);    
}

/*******************************************************************************
* @brief  Function that retrieves the registration status
* @inputs callback
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/18
*******************************************************************************/
void GenerateNewRegistrationToken (void)
{
    aylaDataPoint_t * newDataPoint;
    
    newDataPoint = Ayla_CreateDataPoint (CONTROLOPCODE_SET_CONFIGITEMS, 0x00ba);
    
    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateConfigTokenTLV (3, 
                             CONFTOKEN_CLIENT, CONFTOKEN_REG, CONFTOKEN_START));
                              
    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateBooleanTLV(1));
    
    
    HAL_QWiFiMsg (newDataPoint); 
}

/*******************************************************************************
* @brief  Function that retrieves the registration status
* @inputs callback
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/18
*******************************************************************************/
void GetRegistrationStatus(void (*regStatuscB)(RegisterStatus_t, uint8_t*))
{
    aylaDataPoint_t * newDataPoint;
    
    newDataPoint = Ayla_CreateDataPoint (CONTROLOPCODE_GET_CONFIGITEMS, 0x00ab);
    
    Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateConfigTokenTLV (3, 
                             CONFTOKEN_CLIENT, CONFTOKEN_REG, CONFTOKEN_READY));

    HAL_QWiFiMsg (newDataPoint); 
    
    if (regStatuscB != NULL)
    {
        cbGetRegStatus = regStatuscB;
    }    
}


/*******************************************************************************
* @brief  Parse a configuration token response : /wifi/status
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/19
*******************************************************************************/
#pragma optimize=none
void ControlOps_ParseConfToken_Client (ayla_Packet_t * receivedPacket, uint16_t payloadLength)
{
    ayla_incomingTLV_t * firstTLV;
    ayla_incomingTLV_t * regTokenTLV;

    if (payloadLength > 0)
    {
        firstTLV = (ayla_incomingTLV_t *)receivedPacket->Payload;

        switch (firstTLV->Value[1])
        {
        /************************************/
        /* /client/reg                      */
        /************************************/
        case CONFTOKEN_REG:
            if (firstTLV->Length == 2)
            {
                regTokenTLV = (ayla_incomingTLV_t * )&firstTLV->Value[2];

                if (cbGetRegToken != NULL)
                {
                    uint8_t regToken[7];        //registration token is assume to be always 6 characters (+1 for '\0')

                    stringNCopyWithNull((char*)regToken, 
                                        (char*)regTokenTLV->Value, 
                                        ((regTokenTLV->Length+1)<sizeof(regToken))?
                                            regTokenTLV->Length+1:
                                             sizeof(regToken));

                    cbGetRegToken(REG_TOKEN_RECEIVED, regToken);
                    cbGetRegToken = NULL;
                }
            }
            else
            {
                switch (firstTLV->Value[2])
                {
                    /************************************/
                    /* /client/reg/ready                */
                    /************************************/
                    case CONFTOKEN_READY:
                        WiFiRadioInitStateMachine(WIFISTARTEVENT_REG_STATUS_RECEIVED, (void*)&firstTLV->Value[3]);
                        break;

                    default:
                        break;
                }
            }
            break;

        default:
            break;
        }
    }
}



/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
