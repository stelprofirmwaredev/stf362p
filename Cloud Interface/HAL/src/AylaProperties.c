/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    AylaProperties.c
* @date    2016/10/06
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include ".\HAL\inc\AylaProperties.h"
#include ".\HAL\inc\HAL_WiFiTypes.h"

/*******************************************************************************
*    functions prototypes
*******************************************************************************/
extern uint8_t SendAmbientTemperature(void* pData, dbChangeSource_t source);
extern uint8_t SendAmbientSetpoint(void* pData, dbChangeSource_t source);
extern uint8_t SendFloorTemperature(void* pData, dbChangeSource_t source);
extern uint8_t SendFloorSetpoint(void* pData, dbChangeSource_t source);
extern void ReceiveAmbientSetpoint(aylaTLV_t* pData);
extern uint8_t SendHomeState(void* pData, dbChangeSource_t source);
extern void ReceiveHomeState(aylaTLV_t* pData);
extern uint8_t SendDefaultSetpoint(void* pData, dbChangeSource_t source);
extern void ReceiveDefaultSetpoint(aylaTLV_t* pData);
extern void ReceiveOutdoorTemperature(aylaTLV_t* pData);
extern void ReceiveOutdoorHumidity(aylaTLV_t* pData);
extern uint8_t SendActiveAlerts(void* pData, dbChangeSource_t source);
extern uint8_t SendAmbientHumidity(void* pData, dbChangeSource_t source);
extern uint8_t SendTemperatureFormat(void* pData, dbChangeSource_t source);
extern uint8_t SendTimeFormat(void* pData, dbChangeSource_t source);
extern uint8_t SendLockState(void* pData, dbChangeSource_t source);
extern void ReceiveLockState(aylaTLV_t* pData);
extern uint8_t SendLockDelta(void* pData, dbChangeSource_t source);
extern void ReceiveLockDelta(aylaTLV_t* pData);
extern uint8_t SendGeofenceEnable(void* pData, dbChangeSource_t source);
extern void ReceiveGeofenceEnable(aylaTLV_t* pData);
extern uint8_t SendGeofencingMobileId(void* pData, dbChangeSource_t source);
extern uint8_t SendGeofencingMobileState(void* pData, dbChangeSource_t source);
extern void ReceiveGeofencingMobileId(aylaTLV_t* pData);
extern void ReceiveGeofencingMobileState(aylaTLV_t* pData);
extern uint8_t SendHomeStateChangedByGeofencing(void* pData, dbChangeSource_t source);
extern uint8_t SendHeatDemand(void* pData, dbChangeSource_t source);
extern uint8_t SendThermostatName(void* pData, dbChangeSource_t source);
extern void ReceiveThermostatName(aylaTLV_t* pData);
extern uint8_t SendControllerVersion(void* pData, dbChangeSource_t source);
extern uint8_t SendOemHostVersion(void* pData, dbChangeSource_t source);
extern uint8_t SendZigBeeAssociationPermit(void* pData, dbChangeSource_t source);
extern void ReceiveZigBeeAssociationPermit(aylaTLV_t* pData);
extern uint8_t SendGroupName(void* pData, dbChangeSource_t source);
extern void ReceiveGroupName(aylaTLV_t* pData);
extern uint8_t SendGroupMembers(void* pData, dbChangeSource_t source);
extern void ReceiveGroupMembers(aylaTLV_t* pData);
extern uint8_t SendGroupSetpoint(void* pDat, dbChangeSource_t source);
extern void ReceiveGroupSetpoint(aylaTLV_t* pData);
extern uint8_t SendGroupAwaySetpoint(void* pData, dbChangeSource_t source);
extern void ReceiveGroupAwaySetpoint(aylaTLV_t* pData);
extern uint8_t SendGroupVacationSetpoint(void* pData, dbChangeSource_t source);
extern void ReceiveGroupVacationSetpoint(aylaTLV_t* pData);
extern uint8_t SendGroupStandbySetpoint(void* pData, dbChangeSource_t source);
extern void ReceiveGroupStandbySetpoint(aylaTLV_t* pData);
extern void ReceiveActivityName(aylaTLV_t* pData);
extern void ReceiveActivityReadyAt(aylaTLV_t* pData);
extern void ReceiveActivitySetpointPerGroup(aylaTLV_t* pData);
extern void ReceiveActivityStartTime(aylaTLV_t* pData);
extern void ReceiveActivityStartRequested(aylaTLV_t* pData);
extern uint8_t SendActivityStartRequested(void* pData, dbChangeSource_t source);
extern void ReceiveActivityWeekDays(aylaTLV_t* pData);
extern void ReceiveGeoFenceCenter(aylaTLV_t* pData);
extern void ReceiveResidenceName(aylaTLV_t* pData);
extern void ReceiveFloorSetpoint(aylaTLV_t* pData);
extern uint8_t SendThermostatModel(void* pData, dbChangeSource_t source);
extern uint8_t SendDailyConsumption(void* pData, dbChangeSource_t source);
extern uint8_t SendMonthlyConsumption(void* pData, dbChangeSource_t source);
extern uint8_t SendTstatDailyConsumption(void* pData, dbChangeSource_t source);
extern void ReceiveDailyConsumption(aylaTLV_t* pData);
extern void ReceiveMonthlyConsumption(aylaTLV_t* pData);
extern void ReceiveThermostatModel(aylaTLV_t* pData);
extern void ReceiveDebugLevel(aylaTLV_t* pData);
extern void ReceiveWeatherCondition(aylaTLV_t* pData);
extern uint8_t SendActivityName(void* pData, dbChangeSource_t source);
extern uint8_t SendActivityReadyAt(void* pData, dbChangeSource_t source);
extern uint8_t SendActivitySetpointPerGroup(void* pData, dbChangeSource_t source);
extern uint8_t SendActivityStartTime(void* pData, dbChangeSource_t source);
extern uint8_t SendActiviyWeekDays(void* pData, dbChangeSource_t source);
extern uint8_t SendLanguage(void* pData, dbChangeSource_t source);
extern uint8_t SendDiagnostic(void* pData, dbChangeSource_t source);
extern uint8_t SendZigBeeChannel(void* pData, dbChangeSource_t source);
extern uint8_t SendDebugLevel(void* pData, dbChangeSource_t source);
extern uint8_t SendFloorMode(void* pData, dbChangeSource_t source);
extern uint8_t SendRelayCycleCount(void* pData, dbChangeSource_t source);

extern void SendAmbientTemperatureResp(void* pData, uint16_t requestId);
extern void SendAmbientSetpointResp(void* pData, uint16_t requestId);
extern void SendFloorTemperatureResp(void* pData, uint16_t requestId);
extern void SendFloorSetpointResp(void* pData, uint16_t requestId);
extern void SendHomeStateResp(void* pData, uint16_t requestId);
extern void SendDefaultSetpointResp(void* pData, uint16_t requestId);
extern void SendActiveAlertsResp(void* pData, uint16_t requestId);
extern void SendAmbientHumidityResp(void* pData, uint16_t requestId);
extern void SendTemperatureFormatResp(void* pData, uint16_t requestId);
extern void SendTimeFormatResp(void* pData, uint16_t requestId);
extern void SendLockStateResp(void* pData, uint16_t requestId);
extern void SendLockDeltaResp(void* pData, uint16_t requestId);
extern void SendGeofenceEnableResp(void* pData, uint16_t requestId);
extern void SendGeofencingMobileIdResp(void* pData, uint16_t requestId);
extern void SendGeofencingMobileStateResp(void* pData, uint16_t requestId);
extern void SendHomeStateChangedByGeofencingResp(void* pData, uint16_t requestId);
extern void SendHeatDemandResp(void* pData, uint16_t requestId);
extern void SendThermostatNameResp(void* pData, uint16_t requestId);
extern void SendControllerVersionResp(void* pData, uint16_t requestId);
extern void SendOemHostVersionResp(void* pData, uint16_t requestId);
extern void SendZigBeeAssociationPermitResp(void* pData, uint16_t requestId);
extern void SendGroupNameResp(void* pData, uint16_t requestId);
extern void SendGroupMembersResp(void* pData, uint16_t requestId);
extern void SendGroupSetpointResp(void* pDat, uint16_t requestId);
extern void SendGroupAwaySetpointResp(void* pData, uint16_t requestId);
extern void SendGroupVacationSetpointResp(void* pData, uint16_t requestId);
extern void SendGroupStandbySetpointResp(void* pData, uint16_t requestId);
extern void SendActivityStartRequestedResp(void* pData, uint16_t requestId);
extern void SendThermostatModelResp(void* pData, uint16_t requestId);
extern void SendDailyConsumptionResp(void* pData, uint16_t requestId);
extern void SendMonthlyConsumptionResp(void* pData, uint16_t requestId);
extern void SendTstatDailyConsumptionResp(void* pData, uint16_t requestId);
extern void SendActivityNameResp(void* pData, uint16_t requestId);
extern void SendActivityReadyAtResp(void* pData, uint16_t requestId);
extern void SendActivitySetpointPerGroupResp(void* pData, uint16_t requestId);
extern void SendActivityStartTimeResp(void* pData, uint16_t requestId);
extern void SendActiviyWeekDaysResp(void* pData, uint16_t requestId);
extern void SendLanguageResp(void* pData, uint16_t requestId);
extern void SendDiagnosticResp(void* pData, uint16_t requestId);
extern void SendZigBeeChannelResp(void* pData, uint16_t requestId);
extern void SendDebugLevelResp(void* pData, uint16_t requestId);
extern void SendFloorModeResp(void* pData, uint16_t requestId);
extern void SendRelayCycleCountResp(void* pData, uint16_t requestId);

/*******************************************************************************
*    Table of property attributes
*******************************************************************************/
const prop_t propTable[MAX_PROPERTY_TYPE] =
{
    [WIFITYPE_AMBIENTTEMPERATURE]=
    {
        .propertyName = "TstatAmbientTemperature",      //Must not exceed 30 bytes
        .sendFunction = SendAmbientTemperature,
        .receiveFunction = NULL,
        .respFunction = SendAmbientTemperatureResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_AMBIENTSETPOINT]=
    {
        .propertyName = "TstatAmbientSetpoint",         //Must not exceed 30 bytes
        .sendFunction = SendAmbientSetpoint,
        .receiveFunction = ReceiveAmbientSetpoint,
        .respFunction = SendAmbientSetpointResp,
        .metaEvent = NULL,  //&UpdateSetpointEvent,
        .metaObject = NULL,
    },

    [WIFITYPE_HOMESTATE]=
    {
        .propertyName = "ResidenceState",               //Must not exceed 30 bytes
        .sendFunction = SendHomeState,
        .receiveFunction = ReceiveHomeState,
        .respFunction = SendHomeStateResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_DEFAULTSETPOINT]=
    {
        .propertyName = "DefaultSetpoint",              //Must not exceed 30 bytes
        .sendFunction = SendDefaultSetpoint,
        .receiveFunction = ReceiveDefaultSetpoint,
        .respFunction = SendDefaultSetpointResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_OUTDOORTEMPERATURE]=
    {
        .propertyName = "OutdoorTemperature",           //Must not exceed 30 bytes
        .sendFunction = NULL,
        .receiveFunction = ReceiveOutdoorTemperature,
        .respFunction = NULL,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_OUTDOORHUMIDITY]=
    {
        .propertyName = "OutdoorHumidity",              //Must not exceed 30 bytes
        .sendFunction = NULL,
        .receiveFunction = ReceiveOutdoorHumidity,
        .respFunction = NULL,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_ACTIVEALERTS]=
    {
        .propertyName = "TstatActiveAlerts",            //Must not exceed 30 bytes
        .sendFunction = SendActiveAlerts,
        .receiveFunction = NULL,
        .respFunction = SendActiveAlertsResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_AMBIENTHUMIDITY]=
    {
        .propertyName = "TstatAmbientHumidity",         //Must not exceed 30 bytes
        .sendFunction = SendAmbientHumidity,
        .receiveFunction = NULL,
        .respFunction = SendAmbientHumidityResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_TEMPERATUREFORMAT]=
    {
        .propertyName = "TemperatureFormat",            //Must not exceed 30 bytes
        .sendFunction = SendTemperatureFormat,
        .receiveFunction = NULL,
        .respFunction = SendTemperatureFormatResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_TIMEFORMAT]=
    {
        .propertyName = "TimeFormat",                   //Must not exceed 30 bytes
        .sendFunction = SendTimeFormat,
        .receiveFunction = NULL,
        .respFunction = SendTimeFormatResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_LOCKSTATE]=
    {
        .propertyName = "TstatLockState",               //Must not exceed 30 bytes
        .sendFunction = SendLockState,
        .receiveFunction = ReceiveLockState,
        .respFunction = SendLockStateResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_LOCKDELTA]=
    {
        .propertyName = "TstatLockDelta",               //Must not exceed 30 bytes
        .sendFunction = SendLockDelta,
        .receiveFunction = ReceiveLockDelta,
        .respFunction = SendLockDeltaResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_GEOFENCINGENABLE]=
    {
        .propertyName = "GeofencingOnOff",              //Must not exceed 30 bytes
        .sendFunction = SendGeofenceEnable,
        .receiveFunction = ReceiveGeofenceEnable,
        .respFunction = SendGeofenceEnableResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_GEOFENCINGMOBILEID]=
    {
        .propertyName = "MobileID",                     //Must not exceed 30 bytes
        .sendFunction = SendGeofencingMobileId,
        .receiveFunction = ReceiveGeofencingMobileId,
        .respFunction = SendGeofencingMobileIdResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_GEOFENCINGMOBILESTATE]=
    {
        .propertyName = "MobileState",                  //Must not exceed 30 bytes
        .sendFunction = SendGeofencingMobileState,
        .receiveFunction = ReceiveGeofencingMobileState,
        .respFunction = SendGeofencingMobileStateResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_HOMESTATECHANGEDBYGEOFENCING]=
    {
        .propertyName = "StateChangedByGeofencing",     //Must not exceed 30 bytes
        .sendFunction = SendHomeStateChangedByGeofencing,
        .receiveFunction = NULL,
        .respFunction = SendHomeStateChangedByGeofencingResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_HEATDEMAND]=
    {
        .propertyName = "TstatHeatDemand",              //Must not exceed 30 bytes
        .sendFunction = SendHeatDemand,
        .receiveFunction = NULL,
        .respFunction = SendHeatDemandResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_THERMOSTATNAME]=
    {
        .propertyName = "TstatName",                    //Must not exceed 30 bytes
        .sendFunction = SendThermostatName,
        .receiveFunction = ReceiveThermostatName,
        .respFunction = SendThermostatNameResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_CONTROLLERVERSION]=
    {
        .propertyName = "ControlerSwVersion",           //Must not exceed 30 bytes
        .sendFunction = SendControllerVersion,
        .receiveFunction = NULL,
        .respFunction = SendControllerVersionResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_OEMHOSTVERSION]=
    {
        .propertyName = "oem_host_version",             //Must not exceed 30 bytes
        .sendFunction = SendOemHostVersion,
        .receiveFunction = NULL,
        .respFunction = SendOemHostVersionResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_ZIGBEEASSOCIATIONPERMIT]=
    {
        .propertyName = "ZigBeeAssociationPermit",      //Must not exceed 30 bytes
        .sendFunction = SendZigBeeAssociationPermit,
        .receiveFunction = ReceiveZigBeeAssociationPermit,
        .respFunction = SendZigBeeAssociationPermitResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_GROUPNAME]=
    {
        .propertyName = "GroupName",                    //Must not exceed 30 bytes
        .sendFunction = SendGroupName,
        .receiveFunction = ReceiveGroupName,
        .respFunction = SendGroupNameResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_GROUPMEMBERS]=
    {
        .propertyName = "GroupMembers",                 //Must not exceed 30 bytes
        .sendFunction = SendGroupMembers,
        .receiveFunction = ReceiveGroupMembers,
        .respFunction = SendGroupMembersResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_GROUPSETPOINT]=
    {
        .propertyName = "GroupSetpoint",                //Must not exceed 30 bytes
        .sendFunction = SendGroupSetpoint,
        .receiveFunction = ReceiveGroupSetpoint,
        .respFunction = SendGroupSetpointResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_GROUPAWAYSETPOINT]=
    {
        .propertyName = "GroupAwaySetpoint",            //Must not exceed 30 bytes
        .sendFunction = SendGroupAwaySetpoint,
        .receiveFunction = ReceiveGroupAwaySetpoint,
        .respFunction = SendGroupAwaySetpointResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_GROUPVACATIONSETPOINT]=
    {
        .propertyName = "GroupVacationSetpoint",        //Must not exceed 30 bytes
        .sendFunction = SendGroupVacationSetpoint,
        .receiveFunction = ReceiveGroupVacationSetpoint,
        .respFunction = SendGroupVacationSetpointResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_GROUPSTANDBYSETPOINT]=
    {
        .propertyName = "GroupStandbySetpoint",         //Must not exceed 30 bytes
        .sendFunction = SendGroupStandbySetpoint,
        .receiveFunction = ReceiveGroupStandbySetpoint,
        .respFunction = SendGroupStandbySetpointResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_ACTIVITYNAME]=
    {
        .propertyName = "ActivityName",                 //Must not exceed 30 bytes
        .sendFunction = SendActivityName,
        .receiveFunction = ReceiveActivityName,
        .respFunction = SendActivityNameResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_ACTIVITYREADYAT]=
    {
        .propertyName = "ActivityReadyAt",              //Must not exceed 30 bytes
        .sendFunction = SendActivityReadyAt,
        .receiveFunction = ReceiveActivityReadyAt,
        .respFunction = SendActivityReadyAtResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_ACTIVITYSETPOINTPERGROUP]=
    {
        .propertyName = "ActivitySetpointPerGroup",     //Must not exceed 30 bytes
        .sendFunction = SendActivitySetpointPerGroup,
        .receiveFunction = ReceiveActivitySetpointPerGroup,
        .respFunction = SendActivitySetpointPerGroupResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_ACTIVITYSTARTREQUESTED]=
    {
        .propertyName = "ActivityStartRequested",       //Must not exceed 30 bytes
        .sendFunction = SendActivityStartRequested,
        .receiveFunction = ReceiveActivityStartRequested,
        .respFunction = SendActivityStartRequestedResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_ACTIVITYSTARTTIME]=
    {
        .propertyName = "ActivityStartTime",            //Must not exceed 30 bytes
        .sendFunction = SendActivityStartTime,
        .receiveFunction = ReceiveActivityStartTime,
        .respFunction = SendActivityStartTimeResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_ACTIVITYWEEKDAYS]=
    {
        .propertyName = "ActivityWeekDays",             //Must not exceed 30 bytes
        .sendFunction = SendActiviyWeekDays,
        .receiveFunction = ReceiveActivityWeekDays,
        .respFunction = SendActiviyWeekDaysResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_MONTHLY_CONSUMPTION]=
    {
        .propertyName = "HomeMonthlyConsumption",       //Must not exceed 30 bytes
        .sendFunction = SendMonthlyConsumption,
        .receiveFunction = ReceiveMonthlyConsumption,
        .respFunction = SendMonthlyConsumptionResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_DAILY_CONSUMPTION]=
    {
        .propertyName = "HomeDailyConsumption",         //Must not exceed 30 bytes
        .sendFunction = SendDailyConsumption,
        .receiveFunction = ReceiveDailyConsumption,
        .respFunction = SendDailyConsumptionResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_FLOORMODE]=
    {
        .propertyName = "TstatFloorMode",               //Must not exceed 30 bytes
        .sendFunction = SendFloorMode,
        .receiveFunction = NULL,//todo
        .respFunction = SendFloorModeResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_FLOORTEMPERATURE]=
    {
        .propertyName = "TstatFloorTemperature",        //Must not exceed 30 bytes
        .sendFunction = SendFloorTemperature,
        .receiveFunction = NULL,
        .respFunction = SendFloorTemperatureResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_FLOORSETPOINT]=
    {
        .propertyName = "TstatFloorSetpoint",           //Must not exceed 30 bytes
        .sendFunction = SendFloorSetpoint,
        .receiveFunction = ReceiveFloorSetpoint,
        .respFunction = SendFloorSetpointResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_THERMOSTATMODEL]=
    {
        .propertyName = "TstatModelNumber",             //Must not exceed 30 bytes
        .sendFunction = SendThermostatModel,
        .receiveFunction = ReceiveThermostatModel,
        .respFunction = SendThermostatModelResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_RESIDENCENAME]=
    {
        .propertyName = "ResidenceName",                //Must not exceed 30 bytes
        .sendFunction = NULL,
        .receiveFunction = ReceiveResidenceName,
        .respFunction = NULL,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_GEOFENCECENTER]=
    {
        .propertyName = "GeofenceCenter",               //Must not exceed 30 bytes
        .sendFunction = NULL,
        .receiveFunction = ReceiveGeoFenceCenter,
        .respFunction = NULL,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_MNUBO] =
    {
        .propertyName = "mnubo",                        //Must not exceed 30 bytes
        .sendFunction = NULL,
        .receiveFunction = NULL,
        .respFunction = NULL,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_LANGUAGE] =
    {
        .propertyName = "Language",                     //Must not exceed 30 bytes
        .sendFunction = SendLanguage,
        .receiveFunction = NULL,
        .respFunction = SendLanguageResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_DIAGNOSTIC] =
    {
        .propertyName = "Diagnostic",                     //Must not exceed 30 bytes
        .sendFunction = SendDiagnostic,
        .receiveFunction = NULL,
        .respFunction = SendDiagnosticResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_ZIGBEECHANNEL] =
    {
        .propertyName = "ZigBeeChannel",                //Must not exceed 30 bytes
        .sendFunction = SendZigBeeChannel,
        .receiveFunction = NULL,
        .respFunction = SendZigBeeChannelResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },

    [WIFITYPE_DEBUGLEVEL] =
    {
        .propertyName = "DebugLevel",             //Must not exceed 30 bytes
        .sendFunction = SendDebugLevel,
        .receiveFunction = ReceiveDebugLevel,
        .respFunction = SendDebugLevelResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },
    [WIFITYPE_TSTAT_DAILY_CONSUMPTION] =
    {
        .propertyName = "TstatDailyConsumption",             //Must not exceed 30 bytes
        .sendFunction = SendTstatDailyConsumption,
        .receiveFunction = NULL,
        .respFunction = SendTstatDailyConsumptionResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    },
    [WIFITYPE_WEATHER_CONDITION] =
    {
        .propertyName = "WeatherCondition",             //Must not exceed 30 bytes
        .sendFunction = NULL,
        .receiveFunction = ReceiveWeatherCondition,
        .respFunction = NULL,
        .metaEvent = NULL,
        .metaObject = NULL,
    },
    [WIFITYPE_RELAY_CYCLE_COUNT] =
    {
        .propertyName = "TstatRelayCycleCount",               //Must not exceed 30 bytes
        .sendFunction = SendRelayCycleCount,
        .receiveFunction = NULL,
        .respFunction = SendRelayCycleCountResp,
        .metaEvent = NULL,
        .metaObject = NULL,
    }
};

const weather_t weatherTable[MAX_WEATHER_TYPE] =
{
    [WEATHERTYPE_BLOWING_DUST]=
    {
        .weatherString = "Blowing Dust",      //Must not exceed 30 bytes
        .weatherDayId = FOGGY_ID,
        .weatherNightId = FOGGY_ID,
    },

    [WEATHERTYPE_BLOWING_SAND]=
    {
        .weatherString = "Blowing Sand",      //Must not exceed 30 bytes
        .weatherDayId = FOGGY_ID,
        .weatherNightId = FOGGY_ID,
    },

    [WEATHERTYPE_BLOWING_SAND_NEARBY]=
    {
        .weatherString = "Blowing Sand Nearby",      //Must not exceed 30 bytes
        .weatherDayId = FOGGY_ID,
        .weatherNightId = FOGGY_ID,
    },

    [WEATHERTYPE_BLOWING_SNOW]=
    {
        .weatherString = "Blowing Snow",      //Must not exceed 30 bytes
        .weatherDayId = DRIFTINGSNOW_ID,
        .weatherNightId = DRIFTINGSNOW_ID,
    },

    [WEATHERTYPE_BLOWING_SNOW_NEARBY]=
    {
        .weatherString = "Blowing Snow Nearby",      //Must not exceed 30 bytes
        .weatherDayId = DRIFTINGSNOW_ID,
        .weatherNightId = DRIFTINGSNOW_ID,
    },

    [WEATHERTYPE_BLOWING_SPRAY]=
    {
        .weatherString = "Blowing Spray",      //Must not exceed 30 bytes
        .weatherDayId = WINDY_ID,
        .weatherNightId = WINDY_ID,
    },

    [WEATHERTYPE_CLEAR]=
    {
        .weatherString = "Clear",      //Must not exceed 30 bytes
        .weatherDayId = SUNNY_ID,
        .weatherNightId = CLEAR_ID,
    },

    [WEATHERTYPE_CLOUDY]=
    {
        .weatherString = "Cloudy",      //Must not exceed 30 bytes
        .weatherDayId = CLOUDY_ID,
        .weatherNightId = CLOUDY_ID,
    },

    [WEATHERTYPE_DRIZZLE]=
    {
        .weatherString = "Drizzle",      //Must not exceed 30 bytes
        .weatherDayId = RAIN_ID,
        .weatherNightId = RAIN_ID,
    },

    [WEATHERTYPE_DRIZZLE_FOG]=
    {
        .weatherString = "Drizzle and Fog",      //Must not exceed 30 bytes
        .weatherDayId = FOGGY_ID,
        .weatherNightId = FOGGY_ID,
    },

    [WEATHERTYPE_DRIFTING_SNOW]=
    {
        .weatherString = "Drifting Snow",      //Must not exceed 30 bytes
        .weatherDayId = DRIFTINGSNOW_ID,
        .weatherNightId = DRIFTINGSNOW_ID,
    },

    [WEATHERTYPE_DUSTSTORM]=
    {
        .weatherString = "Duststorm",      //Must not exceed 30 bytes
        .weatherDayId = FOGGY_ID,
        .weatherNightId = FOGGY_ID,
    },

    [WEATHERTYPE_DUSTSTORM_IN_THE_VICINITY]=
    {
        .weatherString = "Duststorm in the Vicinity",      //Must not exceed 30 bytes
        .weatherDayId = FOGGY_ID,
        .weatherNightId = FOGGY_ID,
    },

    [WEATHERTYPE_FAIR]=
    {
        .weatherString = "Fair",      //Must not exceed 30 bytes
        .weatherDayId = FAIRDAY_ID,
        .weatherNightId = FAIRNIGHT_ID,
    },

    [WEATHERTYPE_FOG]=
    {
        .weatherString = "Fog",      //Must not exceed 30 bytes
        .weatherDayId = FOGGY_ID,
        .weatherNightId = FOGGY_ID,
    },

    [WEATHERTYPE_FOGGY]=
    {
        .weatherString = "Foggy",      //Must not exceed 30 bytes
        .weatherDayId = FOGGY_ID,
        .weatherNightId = FOGGY_ID,
    },

    [WEATHERTYPE_FREEZING_DRIZZLE]=
    {
        .weatherString = "Freezing Drizzle",      //Must not exceed 30 bytes
        .weatherDayId = FREEZINGRAIN_ID,
        .weatherNightId = FREEZINGRAIN_ID,
    },

    [WEATHERTYPE_FREEZING_RAIN]=
    {
        .weatherString = "Freezing Rain",      //Must not exceed 30 bytes
        .weatherDayId = FREEZINGRAIN_ID,
        .weatherNightId = FREEZINGRAIN_ID,
    },

    [WEATHERTYPE_HAIL]=
    {
        .weatherString = "Hail",      //Must not exceed 30 bytes
        .weatherDayId = HAIL_ID,
        .weatherNightId = HAIL_ID,
    },

    [WEATHERTYPE_HAZE]=
    {
        .weatherString = "Haze",      //Must not exceed 30 bytes
        .weatherDayId = FOGGY_ID,
        .weatherNightId = FOGGY_ID,
    },

    [WEATHERTYPE_HEAVY_DRIZZLE]=
    {
        .weatherString = "Heavy Drizzle",      //Must not exceed 30 bytes
        .weatherDayId = RAIN_ID,
        .weatherNightId = RAIN_ID,
    },

    [WEATHERTYPE_HEAVY_DUSTSTORM]=
    {
        .weatherString = "Heavy Duststorm",      //Must not exceed 30 bytes
        .weatherDayId = FOGGY_ID,
        .weatherNightId = FOGGY_ID,
    },

    [WEATHERTYPE_HEAVY_FREEZING_DRIZZLE]=
    {
        .weatherString = "Heavy Freezing Drizzle",      //Must not exceed 30 bytes
        .weatherDayId = FREEZINGRAIN_ID,
        .weatherNightId = FREEZINGRAIN_ID,
    },

    [WEATHERTYPE_HEAVY_FREEZING_RAIN]=
    {
        .weatherString = "Heavy Freezing Rain",      //Must not exceed 30 bytes
        .weatherDayId = FREEZINGRAIN_ID,
        .weatherNightId = FREEZINGRAIN_ID,
    },

    [WEATHERTYPE_HEAVY_RAIN]=
    {
        .weatherString = "Heavy Rain",      //Must not exceed 30 bytes
        .weatherDayId = RAIN_ID,
        .weatherNightId = RAIN_ID,
    },

    [WEATHERTYPE_HEAVY_RAIN_SHOWER]=
    {
        .weatherString = "Heavy Rain Shower",      //Must not exceed 30 bytes
        .weatherDayId = RAIN_ID,
        .weatherNightId = RAIN_ID,
    },

    [WEATHERTYPE_HEAVY_SANDSTORM]=
    {
        .weatherString = "Heavy Sandstorm",      //Must not exceed 30 bytes
        .weatherDayId = FOGGY_ID,
        .weatherNightId = FOGGY_ID,
    },

    [WEATHERTYPE_HEAVY_SLEET]=
    {
        .weatherString = "Heavy Sleet",      //Must not exceed 30 bytes
        .weatherDayId = HAIL_ID,
        .weatherNightId = HAIL_ID,
    },

    [WEATHERTYPE_HEAVY_SLEET_AND_THUNDER]=
    {
        .weatherString = "Heavy Sleet and Thunder",      //Must not exceed 30 bytes
        .weatherDayId = HAIL_ID,
        .weatherNightId = HAIL_ID,
    },

    [WEATHERTYPE_HEAVY_SNOW]=
    {
        .weatherString = "Heavy Snow",      //Must not exceed 30 bytes
        .weatherDayId = SNOW_ID,
        .weatherNightId = SNOW_ID,
    },

    [WEATHERTYPE_HEAVY_SNOW_GRAINS]=
    {
        .weatherString = "Heavy Snow Grains",      //Must not exceed 30 bytes
        .weatherDayId = SNOW_ID,
        .weatherNightId = SNOW_ID,
    },

    [WEATHERTYPE_HEAVY_SNOW_SHOWER]=
    {
        .weatherString = "Heavy Snow Shower",      //Must not exceed 30 bytes
        .weatherDayId = SNOW_ID,
        .weatherNightId = SNOW_ID,
    },

    [WEATHERTYPE_HEAVY_SNOW_WITH_THUNDER]=
    {
        .weatherString = "Heavy Snow with Thunder",      //Must not exceed 30 bytes
        .weatherDayId = SNOW_ID,
        .weatherNightId = SNOW_ID,
    },

    [WEATHERTYPE_HEAVY_THUNDERSTORMS]=
    {
        .weatherString = "Heavy Thunderstorm",      //Must not exceed 30 bytes
        .weatherDayId = THUNDERANDHAIL_ID,
        .weatherNightId = THUNDERANDHAIL_ID,
    },

    [WEATHERTYPE_ICE_CRYSTALS]=
    {
        .weatherString = "Ice Crystals",      //Must not exceed 30 bytes
        .weatherDayId = DRIFTINGSNOW_ID,
        .weatherNightId = DRIFTINGSNOW_ID,
    },

    [WEATHERTYPE_LIGHT_DRIZZLE]=
    {
        .weatherString = "Light Drizzle",      //Must not exceed 30 bytes
        .weatherDayId = RAIN_ID,
        .weatherNightId = RAIN_ID,
    },

    [WEATHERTYPE_LIGHT_FREEZING_DRIZZLE]=
    {
        .weatherString = "Light Freezing Drizzle",      //Must not exceed 30 bytes
        .weatherDayId = FREEZINGRAIN_ID,
        .weatherNightId = FREEZINGRAIN_ID,
    },

    [WEATHERTYPE_LIGHT_FREEZING_RAIN]=
    {
        .weatherString = "Light Freezing Rain",      //Must not exceed 30 bytes
        .weatherDayId = FREEZINGRAIN_ID,
        .weatherNightId = FREEZINGRAIN_ID,
    },

    [WEATHERTYPE_LIGHT_RAIN]=
    {
        .weatherString = "Light Rain",      //Must not exceed 30 bytes
        .weatherDayId = RAIN_ID,
        .weatherNightId = RAIN_ID,
    },

    [WEATHERTYPE_LIGHT_RAIN_SHOWER]=
    {
        .weatherString = "Light Rain Shower",      //Must not exceed 30 bytes
        .weatherDayId = RAIN_ID,
        .weatherNightId = RAIN_ID,
    },

    [WEATHERTYPE_LIGHT_RAIN_WITH_THUNDER]=
    {
        .weatherString = "Light Rain with Thunder",      //Must not exceed 30 bytes
        .weatherDayId = THUNDERSTORMSDAY_ID,
        .weatherNightId = THUNDERSTORMSNIGHT_ID,
    },

    [WEATHERTYPE_LIGHT_SLEET]=
    {
        .weatherString = "Light Sleet",      //Must not exceed 30 bytes
        .weatherDayId = HAIL_ID,
        .weatherNightId = HAIL_ID,
    },

    [WEATHERTYPE_LIGHT_SLEET_AND_THUNDER]=
    {
        .weatherString = "Light Sleet and Thunder",      //Must not exceed 30 bytes
        .weatherDayId = HAIL_ID,
        .weatherNightId = HAIL_ID,
    },

    [WEATHERTYPE_LIGHT_SNOW]=
    {
        .weatherString = "Light Snow",      //Must not exceed 30 bytes
        .weatherDayId = SNOW_ID,
        .weatherNightId = SNOW_ID,
    },

    [WEATHERTYPE_LIGHT_SNOW_GRAINS]=
    {
        .weatherString = "Light Snow Grains",      //Must not exceed 30 bytes
        .weatherDayId = SNOW_ID,
        .weatherNightId = SNOW_ID,
    },

    [WEATHERTYPE_LIGHT_SNOW_SHOWER]=
    {
        .weatherString = "Light Snow Shower",      //Must not exceed 30 bytes
        .weatherDayId = SNOW_ID,
        .weatherNightId = SNOW_ID,
    },

    [WEATHERTYPE_LIGHT_SNOW_SLEET]=
    {
        .weatherString = "Light Snow and Sleet",      //Must not exceed 30 bytes
        .weatherDayId = HAIL_ID,
        .weatherNightId = HAIL_ID,
    },

    [WEATHERTYPE_LIGHT_SNOW_WITH_THUNDER]=
    {
        .weatherString = "Light Snow with Thunder",      //Must not exceed 30 bytes
        .weatherDayId = SNOW_ID,
        .weatherNightId = SNOW_ID,
    },

    [WEATHERTYPE_LOW_DRIFTING_DUST]=
    {
        .weatherString = "Low Drifting Dust",      //Must not exceed 30 bytes
        .weatherDayId = FOGGY_ID,
        .weatherNightId = FOGGY_ID,
    },

    [WEATHERTYPE_LOW_DRIFTING_SAND]=
    {
        .weatherString = "Log Drifting Sand",      //Must not exceed 30 bytes
        .weatherDayId = FOGGY_ID,
        .weatherNightId = FOGGY_ID,
    },

    [WEATHERTYPE_MIST]=
    {
        .weatherString = "Mist",      //Must not exceed 30 bytes
        .weatherDayId = FOGGY_ID,
        .weatherNightId = FOGGY_ID,
    },

    [WEATHERTYPE_MOSTLY_CLOUDY]=
    {
        .weatherString = "Mostly Cloudy",      //Must not exceed 30 bytes
        .weatherDayId = PARTLYCLOUDYDAY_ID,
        .weatherNightId = PARTLYCLOUDYNIGHT_ID,
    },

    [WEATHERTYPE_PARTIAL_FOG]=
    {
        .weatherString = "Partial Fog",      //Must not exceed 30 bytes
        .weatherDayId = FOGGY_ID,
        .weatherNightId = FOGGY_ID,
    },

    [WEATHERTYPE_PARTLY_CLOUDY]=
    {
        .weatherString = "Partly Cloudy",      //Must not exceed 30 bytes
        .weatherDayId = PARTLYCLOUDYDAY_ID,
        .weatherNightId = PARTLYCLOUDYNIGHT_ID,
    },

    [WEATHERTYPE_PATCHES_OF_FOG]=
    {
        .weatherString = "Patches of Fog",      //Must not exceed 30 bytes
        .weatherDayId = FOGGY_ID,
        .weatherNightId = FOGGY_ID,
    },

    [WEATHERTYPE_RAIN]=
    {
        .weatherString = "Rain",      //Must not exceed 30 bytes
        .weatherDayId = RAIN_ID,
        .weatherNightId = RAIN_ID,
    },

    [WEATHERTYPE_RAIN_SHOWER]=
    {
        .weatherString = "Rain Shower",      //Must not exceed 30 bytes
        .weatherDayId = RAIN_ID,
        .weatherNightId = RAIN_ID,
    },

    [WEATHERTYPE_RAIN_SLEET]=
    {
        .weatherString = "Rain and Sleet",      //Must not exceed 30 bytes
        .weatherDayId = RAINSNOWSHOWERS_ID,
        .weatherNightId = RAINSNOWSHOWERS_ID,
    },

    [WEATHERTYPE_RAIN_SNOW_SHOWERS]=
    {
        .weatherString = "Rain and Snow",      //Must not exceed 30 bytes
        .weatherDayId = RAINSNOWSHOWERS_ID,
        .weatherNightId = RAINSNOWSHOWERS_ID,
    },

    [WEATHERTYPE_SAND]=
    {
        .weatherString = "Sand",      //Must not exceed 30 bytes
        .weatherDayId = FOGGY_ID,
        .weatherNightId = FOGGY_ID,
    },

    [WEATHERTYPE_SANDSTORM]=
    {
        .weatherString = "Sandstorm",      //Must not exceed 30 bytes
        .weatherDayId = FOGGY_ID,
        .weatherNightId = FOGGY_ID,
    },

    [WEATHERTYPE_SANDSTORM_IN_THE_VICINITY]=
    {
        .weatherString = "Sandstorm in the Vicinity",      //Must not exceed 30 bytes
        .weatherDayId = FOGGY_ID,
        .weatherNightId = FOGGY_ID,
    },

    [WEATHERTYPE_SHALLOW_FOG]=
    {
        .weatherString = "Shallow Fog",      //Must not exceed 30 bytes
        .weatherDayId = FOGGY_ID,
        .weatherNightId = FOGGY_ID,
    },

    [WEATHERTYPE_SHOWERS_IN_THE_VICINITY]=
    {
        .weatherString = "Showers in the Vicinity",      //Must not exceed 30 bytes
        .weatherDayId = CLOUDY_ID,
        .weatherNightId = CLOUDY_ID,
    },

    [WEATHERTYPE_SLEET]=
    {
        .weatherString = "Sleet",      //Must not exceed 30 bytes
        .weatherDayId = HAIL_ID,
        .weatherNightId = HAIL_ID,
    },

    [WEATHERTYPE_SLEET_FREEZING_RAIN]=
    {
        .weatherString = "Sleet and Freezing Rain",      //Must not exceed 30 bytes
        .weatherDayId = FREEZINGRAIN_ID,
        .weatherNightId = FREEZINGRAIN_ID,
    },

    [WEATHERTYPE_SLEET_AND_THUNDER]=
    {
        .weatherString = "Sleet and Thunder",      //Must not exceed 30 bytes
        .weatherDayId = HAIL_ID,
        .weatherNightId = HAIL_ID,
    },

    [WEATHERTYPE_SMALL_HAIL]=
    {
        .weatherString = "Small Hail",      //Must not exceed 30 bytes
        .weatherDayId = HAIL_ID,
        .weatherNightId = HAIL_ID,
    },

    [WEATHERTYPE_SMOKE]=
    {
        .weatherString = "Smoke",      //Must not exceed 30 bytes
        .weatherDayId = FOGGY_ID,
        .weatherNightId = FOGGY_ID,
    },

    [WEATHERTYPE_SNOW]=
    {
        .weatherString = "Snow",      //Must not exceed 30 bytes
        .weatherDayId = SNOW_ID,
        .weatherNightId = SNOW_ID,
    },

    [WEATHERTYPE_SNOW_GRAINS]=
    {
        .weatherString = "Snow Grains",      //Must not exceed 30 bytes
        .weatherDayId = SNOW_ID,
        .weatherNightId = SNOW_ID,
    },

    [WEATHERTYPE_SNOW_PELLETS]=
    {
        .weatherString = "Snow Pellets",      //Must not exceed 30 bytes
        .weatherDayId = HAIL_ID,
        .weatherNightId = HAIL_ID,
    },

    [WEATHERTYPE_SNOW_SLEET]=
    {
        .weatherString = "Snow and Sleet",      //Must not exceed 30 bytes
        .weatherDayId = HAIL_ID,
        .weatherNightId = HAIL_ID,
    },

    [WEATHERTYPE_SNOW_SHOWER]=
    {
        .weatherString = "Snow Shower",      //Must not exceed 30 bytes
        .weatherDayId = SNOW_ID,
        .weatherNightId = SNOW_ID,
    },

    [WEATHERTYPE_SNOW_AND_THUNDER]=
    {
        .weatherString = "Snow and Thunder",      //Must not exceed 30 bytes
        .weatherDayId = SNOW_ID,
        .weatherNightId = SNOW_ID,
    },

    [WEATHERTYPE_SQUALLS]=
    {
        .weatherString = "Squalls",      //Must not exceed 30 bytes
        .weatherDayId = WINDY_ID,
        .weatherNightId = WINDY_ID,
    },

    [WEATHERTYPE_SUNNY]=
    {
        .weatherString = "Sunny",      //Must not exceed 30 bytes
        .weatherDayId = SUNNY_ID,
        .weatherNightId = CLEAR_ID,
    },

    [WEATHERTYPE_THUNDER]=
    {
        .weatherString = "Thunder",      //Must not exceed 30 bytes
        .weatherDayId = THUNDERANDHAIL_ID,
        .weatherNightId = THUNDERANDHAIL_ID,
    },

    [WEATHERTYPE_THUNDER_AND_HAIL]=
    {
        .weatherString = "Thunder and Hail",      //Must not exceed 30 bytes
        .weatherDayId = THUNDERANDHAIL_ID,
        .weatherNightId = THUNDERANDHAIL_ID,
    },

    [WEATHERTYPE_THUNDER_AND_SMALL_HAIL]=
    {
        .weatherString = "Thunder and Small Hail",      //Must not exceed 30 bytes
        .weatherDayId = THUNDERANDHAIL_ID,
        .weatherNightId = THUNDERANDHAIL_ID,
    },

    [WEATHERTYPE_THUNDER_AND_SNOW_PELLETS]=
    {
        .weatherString = "Thunder and Snow Pellets",      //Must not exceed 30 bytes
        .weatherDayId = THUNDERANDHAIL_ID,
        .weatherNightId = THUNDERANDHAIL_ID,
    },

    [WEATHERTYPE_THUNDER_IN_THE_VICINITY]=
    {
        .weatherString = "Thunder in the Vicinity",      //Must not exceed 30 bytes
        .weatherDayId = THUNDERANDHAIL_ID,
        .weatherNightId = THUNDERANDHAIL_ID,
    },

    [WEATHERTYPE_THUNDERSTORM]=
    {
        .weatherString = "Thunderstorm",      //Must not exceed 30 bytes
        .weatherDayId = THUNDERSTORMSDAY_ID,
        .weatherNightId = THUNDERSTORMSNIGHT_ID,
    },

    [WEATHERTYPE_TROPICAL_STORM]=
    {
        .weatherString = "Tropical Storm",      //Must not exceed 30 bytes
        .weatherDayId = TROPICALSTORM_ID,
        .weatherNightId = TROPICALSTORM_ID,
    },

    [WEATHERTYPE_WIDESPREAD_DUST]=
    {
        .weatherString = "Widespread Dust",      //Must not exceed 30 bytes
        .weatherDayId = FOGGY_ID,
        .weatherNightId = FOGGY_ID,
    },

    [WEATHERTYPE_WINDY]=
    {
        .weatherString = "Wind",      //Must not exceed 30 bytes
        .weatherDayId = WINDY_ID,
        .weatherNightId = WINDY_ID,
    },

    [WEATHERTYPE_WINTRY_MIX]=
    {
        .weatherString = "Wintry Mix",      //Must not exceed 30 bytes
        .weatherDayId = RAINSNOWSHOWERS_ID,
        .weatherNightId = RAINSNOWSHOWERS_ID,
    },
};


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
