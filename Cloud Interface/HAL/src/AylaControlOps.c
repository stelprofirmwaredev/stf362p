/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    AylaControlOps.c
* @date    2016/09/07
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include ".\HAL\inc\AylaControlOps.h"
#include <stdint.h>
#include "cmsis_os.h"

#include ".\BSP\Ayla WM-N-BM-30\inc\BSP_Ayla_WM_N_MB_30.h"
#include ".\HAL\inc\HAL_CloudInterface.h"
#include ".\HAL\inc\AylaDefinitions.h"
#include ".\HAL\inc\AylaOTAU.h"
#include ".\HAL\inc\AylaNetwork.h"
#include ".\HAL\inc\AylaSystem.h"
#include ".\HAL\inc\AylaRegistration.h"

#include ".\DB\inc\THREAD_DBDataTypes.h"
#include ".\DB\inc\THREAD_DB.h"

#include "App_Display.h"

#include "BootLoader_OTA_Control.h"

#include "trcRecorder.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/


/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
extern wifiScanSMStates_t wifiScanCurrentState;
extern wifiJoinSMStates_t wifiJoinCurrentState;

extern void (*endOfScancB)(void*);
extern void (*cbCurrentNetwork)(void);
extern void (*cbGetRegToken)(RegisterStatus_t, uint8_t*);
extern void (*cbGetRegStatus)(RegisterStatus_t, uint8_t*);
extern void (*cbjoinNetwork)(WiFiJoinStatus_t);

extern char* otaChunksChannel;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/


/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static void ControlOps_ParseConfToken (ayla_Packet_t * receivedPacket, uint16_t payloadLength);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
* @brief  Parse a configuration token response
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/19
*******************************************************************************/
#pragma optimize=none
static void ControlOps_ParseConfToken (ayla_Packet_t * receivedPacket, uint16_t payloadLength)
{
    ayla_incomingTLV_t * firstTLV;

    if (payloadLength > 0)
    {
        firstTLV = (ayla_incomingTLV_t *)receivedPacket->Payload;

        switch (firstTLV->Value[0])
        {
        /************************************/
        /* /wifi/                           */
        /************************************/
        case CONFTOKEN_WIFI:
            switch (firstTLV->Value[1])
            {
            /************************************/
            /* wifi/scan/                       */
            /************************************/
            case CONFTOKEN_SCAN:
                ControlOps_ParseConfToken_WiFiScan(receivedPacket, payloadLength);
                break;

            /************************************/
            /* wifi/profile/                    */
            /************************************/
            case CONFTOKEN_PROFILE:
                ControlOps_ParseConfToken_WiFiProfile(receivedPacket, payloadLength);
                break;

            /************************************/
            /* wifi/status/                    */
            /************************************/
            case CONFTOKEN_STATUS:
                ControlOps_ParseConfToken_WiFiStatus(receivedPacket, payloadLength);
                break;

            /************************************/
            /************************************/
            default:
                break;
            }

            break;

        /************************************/
        /* client/                          */
        /************************************/
        case CONFTOKEN_CLIENT:
            ControlOps_ParseConfToken_Client(receivedPacket, payloadLength);
            break;


        /************************************/
        /* sys/                             */
        /************************************/
        case CONFTOKEN_SYSTEM:
            ControlOps_ParseConfToken_System(receivedPacket, payloadLength);
            break;

        /************************************/
        /************************************/
        default:
            break;
        }
    }
}



/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/06
*******************************************************************************/
void ControlOps_ParseResponses (ayla_Packet_t * receivedPacket, uint16_t payloadLength)
{
    ayla_incomingTLV_t * firstTLV;

    if (payloadLength > 0)
    {
        firstTLV = (ayla_incomingTLV_t *)receivedPacket->Payload;

        switch (firstTLV->Type)
        {
        /************************************/
        /************************************/
        case TLVTYPE_CONFIGTOKENS:
            ControlOps_ParseConfToken(receivedPacket, payloadLength);
            break;

        /************************************/
        /************************************/
        default:
            break;
        }
    }
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/06
*******************************************************************************/
void ControlOps_ParseConfigurationUpdates (ayla_Packet_t * receivedPacket, uint16_t payloadLength)
{
    ayla_incomingTLV_t * firstTLV;


    if (payloadLength > 0)
    {
        firstTLV = (ayla_incomingTLV_t *)receivedPacket->Payload;

        switch (firstTLV->Type)
        {
        /************************************/
        /************************************/
            break;

        /************************************/
        /************************************/
        default:
            break;
        }
    }
}


/*******************************************************************************
*    Public functions definitions
*******************************************************************************/


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/07
*******************************************************************************/
#pragma optimize = none
void Ayla_HandleControlOperation (ayla_Packet_t * receivedPacket, uint16_t packetLength)
{
    ayla_incomingTLV_t * firstTLV;

    firstTLV = (ayla_incomingTLV_t *)receivedPacket->Payload;
    dbMcuOtauDownloadStatus OtauStatus;

    switch (Get_OpCode((AylaHeader_t){.opCode = receivedPacket->OpCode,.protocolId = receivedPacket->ProtocolID}))
    {
        /************************************/
        /************************************/
    case CONTROLOPCODE_RESPONSE:
        ControlOps_ParseResponses(receivedPacket, packetLength);
        break;

        /************************************/
        /************************************/
    case CONTROLOPCODE_GET_CONFIGITEMS:
        break;

        /************************************/
        /************************************/
    case CONTROLOPCODE_SET_CONFIGITEMS:
        break;

        /************************************/
        /************************************/
    case CONTROLOPCODE_SAVECONFIG :
        break;

        /************************************/
        /************************************/
    case CONTROLOPCODE_NAK:
        //WifiNetworksJoin_StateMachine(WIFIJOINEVENT_NAKRESPONSE, NULL);
        break;

        /************************************/
        /************************************/
    case CONTROLOPCODE_LOAD_STARTUPCONFIG:
        break;

        /************************************/
        /************************************/
    case CONTROLOPCODE_LOAD_FACTORYCONFIG:
        break;

        /************************************/
        /************************************/
    case CONTROLOPCODE_OTA_STATUS:
        if (packetLength == 0)
        {
            /* TODO : Notify User Interface for Radio Module OTA ready ???  */
            ConfirmRadioOTAU_Ready();

        }
        break;

        /************************************/
        /************************************/
    case CONTROLOPCODE_OTA_COMMAND:
        break;

        /************************************/
        /************************************/
    case CONTROLOPCODE_LOGOPS:
        break;

        /************************************/
        /************************************/
    case CONTROLOPCODE_MCUOTA_REPORT_START:
        MCU_OTAU_Request(receivedPacket, packetLength);
        break;

        /************************************/
        /************************************/
    case CONTROLOPCODE_MCUOTA_LOAD:
        FetchMCU_OTAU_Chunk(receivedPacket, packetLength);
        break;

        /************************************/
        /************************************/
    case CONTROLOPCODE_MCUOTA_STATUS:
        break;

        /************************************/
        /************************************/
    case CONTROLOPCODE_MCUOTA_BOOT:
        vTracePrint(otaChunksChannel, "CONTROLOPCODE_MCUOTA_BOOT");
        do
        {
            uint16_t resetTime;

            DBTHRD_GetData(DBTYPE_MCUOTAUDOWNLOAD_COMPLETE, (void*)&OtauStatus, INDEX_DONT_CARE);
            vTracePrintF(otaChunksChannel, "OtauStatus %d.", OtauStatus);
            if (1 == OtauStatus)
            {
                vTracePrint(otaChunksChannel, "MarkProgress");

                /* TODO : To improve robustness, maybe the system needs to verify
                if the progress bar is ready to be mark as new image ready, but the
                verification has been done before starting the OTAU Download*/

                resetTime = HAL_GetOTAResetTimer();
                if (resetTime == 0)
                {
                    NewOTAImageReceived();
                    HAL_SetOTAPendingAlert();
                }
            }
            else if ((dbMcuOtauDownloadStatus)-1 == OtauStatus)
            {
                aylaDataPoint_t *newDataPoint;

                newDataPoint = Ayla_CreateDataPoint (CONTROLOPCODE_MCUOTA_STATUS, 0x00d0);

                Ayla_AddTLV_to_dataPoint (&newDataPoint, Ayla_CreateErrorTLV (OTASTATUSERR_CHECKSUM));

                HAL_QWiFiMsg (newDataPoint);

            }
            else
            {
                osDelay(10);
            }
        } while (OtauStatus == 0);

//        vTracePrint(otaChunksChannel, "OTA SystemReset");
//        NVIC_SystemReset();

        break;

        /************************************/
        /************************************/
    case CONTROLOPCODE_CONFUPDATE:
        ControlOps_ParseConfigurationUpdates(receivedPacket, packetLength);
        break;

        /************************************/
        /************************************/
    case CONTROLOPCODE_WIFI_JOIN:
        break;

        /************************************/
        /************************************/
    case CONTROLOPCODE_WIFI_DELETE:
        break;

        /************************************/
        /************************************/
    default :
        break;
    }

    (void)firstTLV;
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/07
*******************************************************************************/
void Ayla_ControlOpsInit(void)
{
    wifiScanCurrentState = WIFISCANSM_INIT;
    wifiJoinCurrentState = WIFIJOINSM_READYTOJOIN;
    cbCurrentNetwork = NULL;
    endOfScancB = NULL;
    cbjoinNetwork = NULL;
    cbGetRegToken = NULL;
    cbGetRegStatus = NULL;
}

/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
