/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    AylaReceiveProperties.c
* @date    2016/10/06
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include ".\HAL\inc\HAL_CloudInterface.h"
#include ".\HAL\inc\AylaDefinitions.h"
#include ".\HAL\inc\AylaProperties.h"
#include ".\HAL\inc\Ayla_API.h"

#include ".\BSP\Ayla WM-N-BM-30\inc\BSP_Ayla_WM_N_MB_30.h"
#include ".\DB\inc\THREAD_DBDataTypes.h"
#include ".\DB\inc\THREAD_DB.h"

#include ".\strings\inc\strings_tool.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
extern const prop_t propTable[];
#define MOBILE_ID_MAX_LENGTH        13
#define NAME_EXTENDED_MAX_LENGTH    33
#define MODEL_EXTENDED_MAX_LENGTH   65
#define WEEKDAYS_MAX_LENGTH         14
#define PROPERTY_NAME_MAX_LENGTH    32
#define SETPOINTPERGROUP_MAX_LENGTH 40
#define CONSUMPTION_MAX_LENGTH      30
#define WEATHERCONDITION_MAX_LENGTH 30

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
#ifdef FREERTOS_SUPPORT
    #include "trcRecorder.h"
    extern char* wifiEventChannel;
#endif


/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/


/*******************************************************************************
*    functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  Receive the Default Setpoint from the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/06
*******************************************************************************/
void ReceiveDefaultSetpoint(aylaTLV_t * defaultSetpointTLV)
{
    Setpoint100_t receivedDefaultSetpoint = 0;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : DefaultSetpoint");
#endif

    receivedDefaultSetpoint = (Setpoint100_t)DeserializeToInteger32Bits (defaultSetpointTLV->value, defaultSetpointTLV->length);

    DBTHRD_SetData(DBTYPE_DEFAULT_SETPOINT,(void*)&receivedDefaultSetpoint, INDEX_DONT_CARE,DBCHANGE_CLOUD);
}

/*******************************************************************************
* @brief  Receive the Ambient Setpoint from the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/05
*******************************************************************************/
void ReceiveAmbientSetpoint(aylaTLV_t* ambientSetpointTLV)
{
    Setpoint100_t receivedAmbientSetpoint;
    uint8_t tstat;
    strTokenList_t tokenList;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : TstatAmbientSetpoint");
#endif

    tstat = 0;

	//Add a NULL character at the end of the data
    ambientSetpointTLV->value[ambientSetpointTLV->length] = 0;

    StringTokenizer (&tokenList, (char*)ambientSetpointTLV->value, ";");

    //Loop on all thermostat instances
    while (tstat < THERMOSTAT_INSTANCES)
    {
        if (tstat < tokenList.nbOfTokens)
        {
            receivedAmbientSetpoint = (Setpoint100_t)(atof(GetStringToken(&tokenList,tstat))*100);
        }
        else
        {
            DBTHRD_GetData(DBTYPE_AMBIENT_SETPOINT, (void*)&receivedAmbientSetpoint, tstat);
            //Save to database with LOCAL source to force update the cloud
            DBTHRD_SetData(DBTYPE_AMBIENT_SETPOINT,(void*)&receivedAmbientSetpoint, tstat, DBCHANGE_LOCAL);
        }
        //Save to database
        DBTHRD_SetData(DBTYPE_AMBIENT_SETPOINT,(void*)&receivedAmbientSetpoint, tstat, DBCHANGE_CLOUD);
        tstat++;
    }
    ClearStringTokenList(&tokenList);
}

/*******************************************************************************
* @brief  Receive the Home State from the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/05
*******************************************************************************/
void ReceiveHomeState(aylaTLV_t* HomeStateTLV)
{
    ModeEnum_t receivedHomeState;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : ResidenceState");
#endif

    receivedHomeState = (ModeEnum_t)DeserializeToInteger32Bits(HomeStateTLV->value, HomeStateTLV->length);

    DBTHRD_SetData(DBTYPE_HOME_STATE,(void*)&receivedHomeState, INDEX_DONT_CARE, DBCHANGE_CLOUD);
}

/*******************************************************************************
* @brief  Receive the Outdoor Temperature from the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/11
*******************************************************************************/
void ReceiveOutdoorTemperature(aylaTLV_t* outdoorTemperatureTLV)
{
    TEMPERATURE_C_t receivedOutdoorTemperature = 0;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : OutdoorTemperature");
#endif

    receivedOutdoorTemperature = (TEMPERATURE_C_t)DeserializeToInteger32Bits(outdoorTemperatureTLV->value, outdoorTemperatureTLV->length);
    DBTHRD_SetData(DBTYPE_OUTDOOR_TEMPERATURE,(void*)&receivedOutdoorTemperature, INDEX_DONT_CARE,DBCHANGE_CLOUD);
}

/*******************************************************************************
* @brief  Receive the Outdoor Humidity from the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/11
*******************************************************************************/
void ReceiveOutdoorHumidity(aylaTLV_t* outdoorHumidityTLV)
{
    uint16_t receivedOutdoorHumidity = 0;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : OutdoorHumidity");
#endif

    receivedOutdoorHumidity = (uint16_t)DeserializeToInteger32Bits(outdoorHumidityTLV->value, outdoorHumidityTLV->length);
    receivedOutdoorHumidity /= 100;
    DBTHRD_SetData(DBTYPE_OUTDOOR_HUMIDITY,(void*)&receivedOutdoorHumidity, INDEX_DONT_CARE, DBCHANGE_CLOUD);
}

/*******************************************************************************
* @brief  Receive the Lock State from the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
void ReceiveLockState(aylaTLV_t* lockStateTLV)
{
    uint8_t receivedLockState;
    uint8_t tStat;
    strTokenList_t tokenList;
#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : TstatLockState");
#endif

    tStat = 0;

	//Add a NULL character at the end of the data
    lockStateTLV->value[lockStateTLV->length] = 0;

    StringTokenizer (&tokenList, (char*)lockStateTLV->value, ";");

    //Loop on all thermostat instances
    while (tStat < THERMOSTAT_INSTANCES)
    {
        if (tStat < tokenList.nbOfTokens)
        {
            receivedLockState = atoi(GetStringToken(&tokenList,tStat));
        }
        else
        {
            DBTHRD_GetData(DBTYPE_LOCK_STATE, (void*)&receivedLockState, tStat);
            //Save to database with LOCAL source to force update the cloud
            DBTHRD_SetData(DBTYPE_LOCK_STATE,(void*)&receivedLockState, tStat, DBCHANGE_LOCAL);
        }
        //Save to database
        DBTHRD_SetData(DBTYPE_LOCK_STATE,(void*)&receivedLockState, tStat, DBCHANGE_CLOUD);
        tStat++;
    }
    ClearStringTokenList(&tokenList);
}

/*******************************************************************************
* @brief  Receive the Lock Delta from the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
void ReceiveLockDelta(aylaTLV_t* lockDeltaTLV)
{
    uint8_t receivedLockDelta;
    uint8_t tStat;
    strTokenList_t tokenList;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : TstatLockDelta");
#endif

    tStat = 0;

	//Add a NULL character at the end of the data
    lockDeltaTLV->value[lockDeltaTLV->length] = 0;

    StringTokenizer (&tokenList, (char*)lockDeltaTLV->value, ";");

    //Loop on all thermostat instances
    while (tStat < THERMOSTAT_INSTANCES)
    {
        if (tStat < tokenList.nbOfTokens)
        {
            receivedLockDelta = atoi(GetStringToken(&tokenList,tStat));
        }
        else
        {
            DBTHRD_GetData(DBTYPE_LOCK_SETPOINT_LIMIT, (void*)&receivedLockDelta, tStat);
            //Save to database with LOCAL source to force update the cloud
            DBTHRD_SetData(DBTYPE_LOCK_SETPOINT_LIMIT,(void*)&receivedLockDelta, tStat, DBCHANGE_LOCAL);
        }
        //Save to database
        DBTHRD_SetData(DBTYPE_LOCK_SETPOINT_LIMIT,(void*)&receivedLockDelta, tStat, DBCHANGE_CLOUD);
        tStat++;
    }
    ClearStringTokenList(&tokenList);
}

/*******************************************************************************
* @brief  Receive the Geofence Enable state from the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
void ReceiveGeofenceEnable(aylaTLV_t* GeofenceEnableTLV)
{
    uint8_t receivedGeofenceEnable;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : GeofencingOnOff");
#endif

    receivedGeofenceEnable = (uint8_t)DeserializeToInteger32Bits (GeofenceEnableTLV->value, GeofenceEnableTLV->length);
    DBTHRD_SetData(DBTYPE_GEOFENCING_ENABLE,(void*)&receivedGeofenceEnable, INDEX_DONT_CARE, DBCHANGE_CLOUD);
}

/*******************************************************************************
* @brief  Receive the Geofence Mobile Id from the Cloud
* @inputs pData: void pointer to the user instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
void ReceiveGeofencingMobileId(aylaTLV_t* GeofenceMobileIdTLV)
{
    char *receivedGeofenceMobileId;
    char id[MOBILE_ID_MAX_LENGTH];
    uint8_t user;
    strTokenList_t tokenList;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : MobileID");
#endif

    user = 0;

	//Add a NULL character at the end of the data
    GeofenceMobileIdTLV->value[GeofenceMobileIdTLV->length] = 0;


    StringTokenizer (&tokenList, (char*)GeofenceMobileIdTLV->value, ";");

    //Loop on all thermostat instances
    while (user < USER_INSTANCES)
    {
        if (user < tokenList.nbOfTokens)
        {
            receivedGeofenceMobileId = GetStringToken(&tokenList,user);
        }
        else
        {
            receivedGeofenceMobileId = id;
            DBTHRD_GetData(DBTYPE_GEOFENCING_MOBILE_ID, (void*)receivedGeofenceMobileId, user);
            //Save to database with LOCAL source to force update the cloud
            DBTHRD_SetData(DBTYPE_GEOFENCING_MOBILE_ID,(void*)receivedGeofenceMobileId, user, DBCHANGE_LOCAL);
        }
        //Save to database
        DBTHRD_SetData(DBTYPE_GEOFENCING_MOBILE_ID,(void*)receivedGeofenceMobileId, user, DBCHANGE_CLOUD);
        user++;
    }
    ClearStringTokenList(&tokenList);
}

/*******************************************************************************
* @brief  Receive the Geofence Mobile State from the Cloud
* @inputs pData: void pointer to the user instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
void ReceiveGeofencingMobileState(aylaTLV_t* GeofenceMobileStateTLV)
{
    uint8_t receivedGeofenceMobileState;
    uint8_t user;
	strTokenList_t tokenList;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : MobileState");
#endif

    user = 0;

	//Add a NULL character at the end of the data
    GeofenceMobileStateTLV->value[GeofenceMobileStateTLV->length] = 0;
    StringTokenizer (&tokenList, (char*)GeofenceMobileStateTLV->value, ";");

    //Loop on all user instances
    while (user < USER_INSTANCES)
    {
        if (user < tokenList.nbOfTokens)
        {
            //Mobile state is defined
            receivedGeofenceMobileState = atoi(GetStringToken(&tokenList,user));
        }
        else
        {
            DBTHRD_GetData(DBTYPE_GEOFENCING_MOBILE_STATE, (void*)&receivedGeofenceMobileState, user);
            //Save to database with LOCAL source to force update the cloud
            DBTHRD_SetData(DBTYPE_GEOFENCING_MOBILE_STATE,(void*)&receivedGeofenceMobileState, user, DBCHANGE_LOCAL);
        }
        //Save to database
        DBTHRD_SetData(DBTYPE_GEOFENCING_MOBILE_STATE,(void*)&receivedGeofenceMobileState, user, DBCHANGE_CLOUD);
        user++;
    }
    ClearStringTokenList(&tokenList);
}

/*******************************************************************************
* @brief  Receive the Thermostat Name from the Cloud
* @inputs pData: void pointer to the thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
void ReceiveThermostatName(aylaTLV_t* TstatNameTLV)
{
    char *receivedTstatName;
    char name[NAME_EXTENDED_MAX_LENGTH];
    uint8_t tstat;
    strTokenList_t tokenList;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : TstatName");
#endif

    tstat = 0;

	//Add a NULL character at the end of the data
    TstatNameTLV->value[TstatNameTLV->length] = 0;
    StringTokenizer (&tokenList, (char*)TstatNameTLV->value, ";");

    //Loop on all thermostat instances
    while (tstat < THERMOSTAT_INSTANCES)
    {
        if (tstat < tokenList.nbOfTokens)
        {
            receivedTstatName = GetStringToken(&tokenList,tstat);
        }
        else
        {
            receivedTstatName = name;
            DBTHRD_GetData(DBTYPE_TSTAT_NAME, (void*)receivedTstatName, tstat);
            //Save to database with LOCAL source to force update the cloud
            DBTHRD_SetData(DBTYPE_TSTAT_NAME,(void*)receivedTstatName, tstat, DBCHANGE_LOCAL);
        }
        //Save to database
        DBTHRD_SetData(DBTYPE_TSTAT_NAME,(void*)receivedTstatName, tstat, DBCHANGE_CLOUD);
        tstat++;
    }
	ClearStringTokenList(&tokenList);
}

/*******************************************************************************
* @brief  Receive the ZigBee Association Permit flag from the Cloud
* @inputs pData: void pointer to index (don't care)
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/20
*******************************************************************************/
void ReceiveZigBeeAssociationPermit(aylaTLV_t* ZigBeeAssociationPermitTLV)
{
    uint8_t receivedZigBeeAssociationPermit;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : ZigBeeAssociationPermit");
#endif

    receivedZigBeeAssociationPermit = (uint8_t)DeserializeToInteger32Bits (ZigBeeAssociationPermitTLV->value, ZigBeeAssociationPermitTLV->length);

    DBTHRD_SetData(DBTYPE_ZIGBEE_ASSOCIATION_PERMIT,(void*)&receivedZigBeeAssociationPermit, INDEX_DONT_CARE, DBCHANGE_CLOUD);
}

/*******************************************************************************
* @brief  Receive the Group Name from the Cloud
* @inputs pData: void pointer to the group instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/25
*******************************************************************************/
void ReceiveGroupName(aylaTLV_t* groupNameTLV)
{
    char *receivedGroupName;
    char name[NAME_EXTENDED_MAX_LENGTH];
    uint8_t group;
    strTokenList_t tokenList;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : GroupName");
#endif

    group = 0;

	//Add a NULL character at the end of the data
    groupNameTLV->value[groupNameTLV->length] = 0;
    StringTokenizer (&tokenList, (char*)groupNameTLV->value, ";");

    //Loop on all group instances
    while (group < GROUP_INSTANCES)
    {
        if (group < tokenList.nbOfTokens)
        {
            receivedGroupName = GetStringToken(&tokenList,group);
        }
        else
        {
            receivedGroupName = name;
            DBTHRD_GetData(DBTYPE_GROUP_NAME, (void*)receivedGroupName, group);
            //Save to database with LOCAL source to force update the cloud
            DBTHRD_SetData(DBTYPE_GROUP_NAME,(void*)receivedGroupName, group, DBCHANGE_LOCAL);
        }
        //Save to database
        DBTHRD_SetData(DBTYPE_GROUP_NAME,(void*)receivedGroupName, group, DBCHANGE_CLOUD);
        group++;
    }
	ClearStringTokenList(&tokenList);
}

/*******************************************************************************
* @brief  Receive the Group Members from the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/25
*******************************************************************************/
void ReceiveGroupMembers(aylaTLV_t* groupMembersTLV)
{
    strTokenList_t grouptokenList;
    uint8_t group;
    dbType_GroupMembers_t groupMembers;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : GroupMembers");
#endif

	//Add a NULL character at the end of the data
    groupMembersTLV->value[groupMembersTLV->length] = 0;
    StringTokenizer (&grouptokenList, (char*)groupMembersTLV->value, ";");

    /* need all groups to be tokenized to avoid having a thermostat in 2 groups*/
    if (grouptokenList.nbOfTokens >= GROUP_INSTANCES)
    {
        for (group = 0; group < GROUP_INSTANCES; group++)
        {
            strTokenList_t tokenList;

            if (group < grouptokenList.nbOfTokens)
            {
                groupMembers.MembersList = 0;
                StringTokenizer (&tokenList, GetStringToken(&grouptokenList,group), ",");

                for (uint8_t i = 0; i < tokenList.nbOfTokens; i++)
                {
                    uint8_t intGroupMember;

                    intGroupMember = atoi(GetStringToken(&tokenList,i));
                    if ((intGroupMember > 0) && (intGroupMember <= THERMOSTAT_INSTANCES))
                    {
                        intGroupMember -= 1;        //compute as 0 for first item
                        groupMembers.MembersList |= (1<<intGroupMember);
                    }
                }
                ClearStringTokenList(&tokenList);
            }
            else
            {
                DBTHRD_GetData(DBTYPE_GROUP_MEMBERS, (void*)&groupMembers, group);
                //Save to database with LOCAL source to force update the cloud
                DBTHRD_SetData(DBTYPE_GROUP_MEMBERS,(void*)&groupMembers, group, DBCHANGE_LOCAL);
            }
            //Save to database
            DBTHRD_SetData(DBTYPE_GROUP_MEMBERS,(void*)&groupMembers, group, DBCHANGE_CLOUD);
        }
    }
    else
    {
        //Partial or empty data
        for (group = 0; group < GROUP_INSTANCES; group++)
        {
            DBTHRD_GetData(DBTYPE_GROUP_MEMBERS, (void*)&groupMembers, group);
            //Save to database with LOCAL source to force update the cloud
            DBTHRD_SetData(DBTYPE_GROUP_MEMBERS,(void*)&groupMembers, group, DBCHANGE_LOCAL);
            //Save to database
            DBTHRD_SetData(DBTYPE_GROUP_MEMBERS,(void*)&groupMembers, group, DBCHANGE_CLOUD);
        }
    }
    ClearStringTokenList(&grouptokenList);
}

/*******************************************************************************
* @brief  Receive the Group Setpoint from the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/25
*******************************************************************************/
void ReceiveGroupSetpoint(aylaTLV_t* groupSetpointTLV)
{
    Setpoint100_t receivedGroupSetpoint;
    strTokenList_t tokenList;

    uint8_t group;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : GroupSetpoint");
#endif

    group = 0;

	//Add a NULL character at the end of the data
    groupSetpointTLV->value[groupSetpointTLV->length] = 0;
    StringTokenizer (&tokenList, (char*)groupSetpointTLV->value, ";");

    //Loop on all group instances
    while (group < GROUP_INSTANCES)
    {
        if (group < tokenList.nbOfTokens)
        {
            receivedGroupSetpoint = (Setpoint100_t)(atof(GetStringToken(&tokenList,group))*100);
        }
        else
        {
            DBTHRD_GetData(DBTYPE_GROUP_SETPOINT, (void*)&receivedGroupSetpoint, group);
            //Save to database with LOCAL source to force update the cloud
            DBTHRD_SetData(DBTYPE_GROUP_SETPOINT,(void*)&receivedGroupSetpoint, group, DBCHANGE_LOCAL);
        }
        //Save to database
        DBTHRD_SetData(DBTYPE_GROUP_SETPOINT,(void*)&receivedGroupSetpoint, group, DBCHANGE_CLOUD);
        group++;
    }
    ClearStringTokenList(&tokenList);
}

/*******************************************************************************
* @brief  Receive the Group Away Setpoint from the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/25
*******************************************************************************/
void ReceiveGroupAwaySetpoint(aylaTLV_t* groupAwaySetpointTLV)
{
    Setpoint100_t receivedGroupAwaySetpoint;
    uint8_t group;
    strTokenList_t tokenList;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : GroupAwaySetpoint");
#endif

    group = 0;

	//Add a NULL character at the end of the data
    groupAwaySetpointTLV->value[groupAwaySetpointTLV->length] = 0;
    StringTokenizer (&tokenList, (char*)groupAwaySetpointTLV->value, ";");

    //Loop on all group instances
    while (group < GROUP_INSTANCES)
    {
        if (group < tokenList.nbOfTokens)
        {
            receivedGroupAwaySetpoint = (Setpoint100_t)(atof(GetStringToken(&tokenList,group))*100);
        }
        else
        {
            DBTHRD_GetData(DBTYPE_GROUP_AWAY_SETPOINT, (void*)&receivedGroupAwaySetpoint, group);
            //Save to database with LOCAL source to force update the cloud
            DBTHRD_SetData(DBTYPE_GROUP_AWAY_SETPOINT,(void*)&receivedGroupAwaySetpoint, group, DBCHANGE_LOCAL);
        }
        //Save to database
        DBTHRD_SetData(DBTYPE_GROUP_AWAY_SETPOINT,(void*)&receivedGroupAwaySetpoint, group, DBCHANGE_CLOUD);
        group++;
    }
    ClearStringTokenList(&tokenList);
}

/*******************************************************************************
* @brief  Receive the Group Vacation Setpoint from the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/25
*******************************************************************************/
void ReceiveGroupVacationSetpoint(aylaTLV_t* groupVacationSetpointTLV)
{
    Setpoint100_t receivedGroupVacationSetpoint;
    uint8_t group;
    strTokenList_t tokenList;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : GroupVacationSetpoint");
#endif

    group = 0;

	//Add a NULL character at the end of the data
    groupVacationSetpointTLV->value[groupVacationSetpointTLV->length] = 0;
    StringTokenizer (&tokenList, (char*)groupVacationSetpointTLV->value, ";");

    //Loop on all group instances
    while (group < GROUP_INSTANCES)
    {
        if (group < tokenList.nbOfTokens)
        {
            receivedGroupVacationSetpoint = (Setpoint100_t)(atof(GetStringToken(&tokenList,group))*100);
        }
        else
        {
            DBTHRD_GetData(DBTYPE_GROUP_VACATION_SETPOINT, (void*)&receivedGroupVacationSetpoint, group);
            //Save to database with LOCAL source to force update the cloud
            DBTHRD_SetData(DBTYPE_GROUP_VACATION_SETPOINT,(void*)&receivedGroupVacationSetpoint, group, DBCHANGE_LOCAL);
        }
        //Save to database
        DBTHRD_SetData(DBTYPE_GROUP_VACATION_SETPOINT,(void*)&receivedGroupVacationSetpoint, group, DBCHANGE_CLOUD);
        group++;
    }
    ClearStringTokenList(&tokenList);
}

/*******************************************************************************
* @brief  Receive the Group Standby Setpoint from the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/25
*******************************************************************************/
void ReceiveGroupStandbySetpoint(aylaTLV_t* groupStandbySetpointTLV)
{
    Setpoint100_t receivedGroupStandbySetpoint;
    uint8_t group;
    strTokenList_t tokenList;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : GroupStandbySetpoint");
#endif

    group = 0;

	//Add a NULL character at the end of the data
    groupStandbySetpointTLV->value[groupStandbySetpointTLV->length] = 0;
    StringTokenizer (&tokenList, (char*)groupStandbySetpointTLV->value, ";");

    //Loop on all group instances
    while (group < GROUP_INSTANCES)
    {
        if (group < tokenList.nbOfTokens)
        {
            receivedGroupStandbySetpoint = (Setpoint100_t)(atof(GetStringToken(&tokenList,group))*100);
        }
        else
        {
            DBTHRD_GetData(DBTYPE_GROUP_STANDBY_SETPOINT, (void*)&receivedGroupStandbySetpoint, group);
            //Save to database with LOCAL source to force update the cloud
            DBTHRD_SetData(DBTYPE_GROUP_STANDBY_SETPOINT,(void*)&receivedGroupStandbySetpoint, group, DBCHANGE_LOCAL);
        }
        //Save to database
        DBTHRD_SetData(DBTYPE_GROUP_STANDBY_SETPOINT,(void*)&receivedGroupStandbySetpoint, group, DBCHANGE_CLOUD);
        group++;
    }
    ClearStringTokenList(&tokenList);
}

/*******************************************************************************
* @brief  Receive the Activity Name from the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/17
*******************************************************************************/
void ReceiveActivityName(aylaTLV_t* activityNameTLV)
{
    char *receivedActivityName;
    char name[NAME_EXTENDED_MAX_LENGTH];
    uint8_t activity;
    strTokenList_t tokenList;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : ActivityName");
#endif

    activity = 0;

	//Add a NULL character at the end of the data
    activityNameTLV->value[activityNameTLV->length] = 0;
    StringTokenizer (&tokenList, (char*)activityNameTLV->value, ";");

    //Loop on all activity instances
    while (activity < ACTIVITY_INSTANCES)
    {
        if (activity < tokenList.nbOfTokens)
        {
            receivedActivityName = GetStringToken(&tokenList,activity);
        }
        else
        {
            receivedActivityName = name;
            DBTHRD_GetData(DBTYPE_ACTIVITY_NAME, (void*)receivedActivityName, activity);
            //Save to database with LOCAL source to force update the cloud
            DBTHRD_SetData(DBTYPE_ACTIVITY_NAME,(void*)receivedActivityName, activity, DBCHANGE_LOCAL);
        }
        //Save to database
        DBTHRD_SetData(DBTYPE_ACTIVITY_NAME,(void*)receivedActivityName, activity, DBCHANGE_CLOUD);
        activity++;
    }
    ClearStringTokenList(&tokenList);
}

/*******************************************************************************
* @brief  Receive the Activity Ready At setting from the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/17
*******************************************************************************/
void ReceiveActivityReadyAt(aylaTLV_t* readyAtTLV)
{
    uint8_t receivedReadyAt;
    uint8_t activity;
    strTokenList_t tokenList;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : ActivityReadyAt");
#endif

    activity = 0;

	//Add a NULL character at the end of the data
    readyAtTLV->value[readyAtTLV->length] = 0;
	StringTokenizer (&tokenList, (char*)readyAtTLV->value, ";");

    //Loop on all activity instances
    while (activity < ACTIVITY_INSTANCES)
    {
        if (activity < tokenList.nbOfTokens)
        {
            receivedReadyAt = atoi(GetStringToken(&tokenList,activity));
        }
        else
        {
            DBTHRD_GetData(DBTYPE_ACTIVITY_READY_AT, (void*)&receivedReadyAt, activity);
            //Save to database with LOCAL source to force update the cloud
            DBTHRD_SetData(DBTYPE_ACTIVITY_READY_AT,(void*)&receivedReadyAt, activity, DBCHANGE_LOCAL);
        }
        //Save to database
        DBTHRD_SetData(DBTYPE_ACTIVITY_READY_AT,(void*)&receivedReadyAt, activity, DBCHANGE_CLOUD);
        activity++;
    }
    ClearStringTokenList(&tokenList);
}

/*******************************************************************************
* @brief  Receive the Activity Setpoint per Group from the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/17
*******************************************************************************/
void ReceiveActivitySetpointPerGroup(aylaTLV_t* activitySetpointPerGroupTLV)
{
	strTokenList_t groupTokenList;
    uint8_t j;

	//Add a NULL character at the end of the data
    activitySetpointPerGroupTLV->value[activitySetpointPerGroupTLV->length] = 0;
    StringTokenizer (&groupTokenList, (char*)activitySetpointPerGroupTLV->value, ";");

	for (uint8_t i = 0; i < ACTIVITY_INSTANCES; i++)
	{
        strTokenList_t tokenList;
		Setpoint100_t groupSetpoint [GROUP_INSTANCES];

        if (i < groupTokenList.nbOfTokens)
        {
            memset(groupSetpoint, 0, GROUP_INSTANCES);
            StringTokenizer (&tokenList, GetStringToken(&groupTokenList,i), ",");

            /* Update data only if all group instances have been tokenized */
            if (tokenList.nbOfTokens == GROUP_INSTANCES)
            {
                for (j = 0; j < GROUP_INSTANCES; j++)
                {
                    groupSetpoint [j] = (Setpoint100_t)(atof(GetStringToken(&tokenList,j))*100);
                }
            }
            else
            {
                DBTHRD_GetData(DBTYPE_ACTIVITY_SETPOINT_PER_GROUP, (void*)&groupSetpoint, i);
                //Save to database with LOCAL source to force update the cloud
                DBTHRD_SetData(DBTYPE_ACTIVITY_SETPOINT_PER_GROUP,(void*)&groupSetpoint, i, DBCHANGE_LOCAL);
            }
            ClearStringTokenList(&tokenList);
        }
        else
        {
            DBTHRD_GetData(DBTYPE_ACTIVITY_SETPOINT_PER_GROUP, (void*)&groupSetpoint, i);
            //Save to database with LOCAL source to force update the cloud
            DBTHRD_SetData(DBTYPE_ACTIVITY_SETPOINT_PER_GROUP,(void*)&groupSetpoint, i, DBCHANGE_LOCAL);
        }
        //Save to database
        DBTHRD_SetData(DBTYPE_ACTIVITY_SETPOINT_PER_GROUP,(void*)&groupSetpoint, i, DBCHANGE_CLOUD);
	}

    ClearStringTokenList(&groupTokenList);
}

/*******************************************************************************
* @brief  Receive the Activity Start Request command from the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/17
*******************************************************************************/
void ReceiveActivityStartRequested(aylaTLV_t* startRequestedTLV)
{
    uint8_t receivedStartRequested;
    uint8_t activity;
    strTokenList_t tokenList;
    uint8_t ActivityStartArray[ACTIVITY_INSTANCES] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : ActivityStartRequested");
#endif

    activity = 0;

	//Add a NULL character at the end of the data
    startRequestedTLV->value[startRequestedTLV->length] = 0;
    StringTokenizer (&tokenList, (char*)startRequestedTLV->value, ";");

    //Loop on all activity instances, but stop once an activity is requested
    //This is to avoid overload
    while (activity < ACTIVITY_INSTANCES)
    {
        if (activity < tokenList.nbOfTokens)
        {
            receivedStartRequested = atoi(GetStringToken(&tokenList,activity));
        }
        else
        {
            //Datapoint is incomplete, fill it with 0
            receivedStartRequested = 0;
        }
        ActivityStartArray[activity] = receivedStartRequested;
        activity++;
    }
    //Save to database
    DBTHRD_SetData(DBTYPE_ACTIVITY_START,(void*)&ActivityStartArray, INDEX_DONT_CARE, DBCHANGE_CLOUD);
    ClearStringTokenList(&tokenList);
}

/*******************************************************************************
* @brief  Receive the Activity Start Time from the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/17
*******************************************************************************/
void ReceiveActivityStartTime(aylaTLV_t* activityStartTimeTLV)
{
    int16_t receivedActivityStartTime;
    uint8_t activity;
	strTokenList_t tokenList;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : ActivityStartTime");
#endif

    activity = 0;

	//Add a NULL character at the end of the data
    activityStartTimeTLV->value[activityStartTimeTLV->length] = 0;
    StringTokenizer (&tokenList, (char*)activityStartTimeTLV->value, ";");

    //Loop on all activity instances
    while (activity < ACTIVITY_INSTANCES)
    {
        if (activity < tokenList.nbOfTokens)
        {
            receivedActivityStartTime = atoi(GetStringToken(&tokenList,activity));
        }
        else
        {
            DBTHRD_GetData(DBTYPE_ACTIVITY_START_TIME, (void*)&receivedActivityStartTime, activity);
            //Save to database with LOCAL source to force update the cloud
            DBTHRD_SetData(DBTYPE_ACTIVITY_START_TIME,(void*)&receivedActivityStartTime, activity, DBCHANGE_LOCAL);
        }
        //Save to database
        DBTHRD_SetData(DBTYPE_ACTIVITY_START_TIME,(void*)&receivedActivityStartTime, activity, DBCHANGE_CLOUD);
        activity++;
    }
    ClearStringTokenList(&tokenList);
}

/*******************************************************************************
* @brief  Receive the Activity Week Days from the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/17
*******************************************************************************/
void ReceiveActivityWeekDays(aylaTLV_t* activityWeekDaysTLV)
{
    strTokenList_t activityTokenList;
    uint8_t activity;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : ActivityWeekDays");
#endif

    activity = 0;

	//Add a NULL character at the end of the data
    activityWeekDaysTLV->value[activityWeekDaysTLV->length] = 0;
    StringTokenizer (&activityTokenList, (char*)activityWeekDaysTLV->value, ";");

    //Loop on all activity instances
    while (activity < ACTIVITY_INSTANCES)
    {
        uint8_t receivedActivityWeekDays;
        strTokenList_t tokenList;

        if (activity < activityTokenList.nbOfTokens)
        {
            receivedActivityWeekDays = 0;
            StringTokenizer (&tokenList, GetStringToken(&activityTokenList,activity), ",");

            if (tokenList.nbOfTokens == 7)
            {
                for (uint8_t days = 0; days < 7; days++)
                {
                    uint8_t dayStatus;

                    dayStatus = atoi(GetStringToken(&tokenList,days));
                    receivedActivityWeekDays |= ((dayStatus!=0?0x01:0) << days);
                }
            }
            else
            {
                DBTHRD_GetData(DBTYPE_ACTIVITY_WEEK_DAYS, (void*)&receivedActivityWeekDays, activity);
                //Save to database with LOCAL source to force update the cloud
                DBTHRD_SetData(DBTYPE_ACTIVITY_WEEK_DAYS,(void*)&receivedActivityWeekDays, activity, DBCHANGE_LOCAL);
            }
            ClearStringTokenList(&tokenList);
        }
        else
        {
            DBTHRD_GetData(DBTYPE_ACTIVITY_WEEK_DAYS, (void*)&receivedActivityWeekDays, activity);
            //Save to database with LOCAL source to force update the cloud
            DBTHRD_SetData(DBTYPE_ACTIVITY_WEEK_DAYS,(void*)&receivedActivityWeekDays, activity, DBCHANGE_LOCAL);
        }
        //Save to database
        DBTHRD_SetData(DBTYPE_ACTIVITY_WEEK_DAYS,(void*)&receivedActivityWeekDays, activity, DBCHANGE_CLOUD);
        activity++;
    }
    ClearStringTokenList(&activityTokenList);
}

/*******************************************************************************
* @brief  Receive the Geofence Center from the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/17
*******************************************************************************/
void ReceiveGeoFenceCenter(aylaTLV_t* geofenceCenterTLV)
{
    dbGeofenceCenter_t receivedGeofenceCenter;
	strTokenList_t tokenList;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : GeofenceCenter");
#endif

	//Add a NULL character at the end of the data
    geofenceCenterTLV->value[geofenceCenterTLV->length] = 0;
    StringTokenizer (&tokenList, (char*)geofenceCenterTLV->value, ",");

    /* there must be only 2 tokens in the datapoint, otherwise the value is
        invalid     */
    if (tokenList.nbOfTokens == 2)
    {
        strcpy((char*)receivedGeofenceCenter.latitude, GetStringToken(&tokenList,0));
        strcpy((char*)receivedGeofenceCenter.longitude, GetStringToken(&tokenList,1));
    }
    else
    {
        DBTHRD_GetData(DBTYPE_GEOFENCE_CENTER, (void*)&receivedGeofenceCenter, INDEX_DONT_CARE);
        //Save to database with LOCAL source to force update the cloud
        DBTHRD_SetData(DBTYPE_GEOFENCE_CENTER,(void*)&receivedGeofenceCenter, INDEX_DONT_CARE, DBCHANGE_LOCAL);
    }
    //Save to database
    DBTHRD_SetData(DBTYPE_GEOFENCE_CENTER,(void*)&receivedGeofenceCenter, INDEX_DONT_CARE, DBCHANGE_CLOUD);
    ClearStringTokenList(&tokenList);
}

/*******************************************************************************
* @brief  Receive the Residence Name from the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/06
*******************************************************************************/
void ReceiveResidenceName(aylaTLV_t* pData)
{

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : ResidenceName");
#endif
}

/*******************************************************************************
* @brief  Receive the Floor Setpoint from the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/06
*******************************************************************************/
void ReceiveFloorSetpoint(aylaTLV_t* floorSetpointTLV)
{
    Setpoint100_t receivedFloorSetpoint;
    uint8_t tstat;
    strTokenList_t tokenList;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : TstatFloorSetpoint");
#endif

    tstat = 0;

	//Add a NULL character at the end of the data
    floorSetpointTLV->value[floorSetpointTLV->length] = 0;

    StringTokenizer (&tokenList, (char*)floorSetpointTLV->value, ";");

    //Loop on all thermostat instances
    while (tstat < THERMOSTAT_INSTANCES)
    {
        if (tstat < tokenList.nbOfTokens)
        {
            receivedFloorSetpoint = (Setpoint100_t)(atof(GetStringToken(&tokenList,tstat))*100);
        }
        else
        {
            DBTHRD_GetData(DBTYPE_FLOOR_SETPOINT, (void*)&receivedFloorSetpoint, tstat);
            //Save to database with LOCAL source to force update the cloud
            DBTHRD_SetData(DBTYPE_FLOOR_SETPOINT,(void*)&receivedFloorSetpoint, tstat, DBCHANGE_LOCAL);
        }
        //Save to database
        DBTHRD_SetData(DBTYPE_FLOOR_SETPOINT,(void*)&receivedFloorSetpoint, tstat, DBCHANGE_CLOUD);
        tstat++;
    }
    ClearStringTokenList(&tokenList);
}

/*******************************************************************************
* @brief  Receive the Home Daily Consumption from the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/28
*******************************************************************************/
void ReceiveDailyConsumption(aylaTLV_t* dailyConsumptionTLV)
{
#define DAILY_CONSUMPTION_YEAR_POS                  0
#define DAILY_CONSUMPTION_MONTH_POS                 1
#define DAILY_CONSUMPTION_DAY_POS                   2
#define DAILY_CONSUMPTION_POWER_POS                 3
#define DAILY_CONSUMPTION_MINOUTDOORTEMP_POS        4
#define DAILY_CONSUMPTION_MAXOUTDOORTEMP_POS        5

    strTokenList_t dailyTokenList;
    strTokenList_t tokenList;
    dbType_DailyConsumption_t consumption;
    uint8_t day;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : HomeDailyConsumption");
#endif

    day = YESTERDAY;

	//Add a NULL character at the end of the data
    dailyConsumptionTLV->value[dailyConsumptionTLV->length] = 0;
    StringTokenizer (&dailyTokenList, (char*)dailyConsumptionTLV->value, ";");

    //Loop on all day
    while (day < DAILYCONSUMPTION_NUMBEROFDAYS)
    {
        if (day < dailyTokenList.nbOfTokens)
        {
            StringTokenizer (&tokenList, GetStringToken(&dailyTokenList,day - YESTERDAY), ",");

            if (tokenList.nbOfTokens == 6)
            {
                consumption.Date.Year = atoi(GetStringToken(&tokenList,DAILY_CONSUMPTION_YEAR_POS));
                consumption.Date.Month = atoi(GetStringToken(&tokenList,DAILY_CONSUMPTION_MONTH_POS));
                consumption.Date.Day = atoi(GetStringToken(&tokenList,DAILY_CONSUMPTION_DAY_POS));
                consumption.Consumption = atoi(GetStringToken(&tokenList,DAILY_CONSUMPTION_POWER_POS));
                consumption.MinimumTemperature = (TEMPERATURE_C_t)(atof(GetStringToken(&tokenList,DAILY_CONSUMPTION_MINOUTDOORTEMP_POS))*100);
                consumption.MaximumTemperature = (TEMPERATURE_C_t)(atof(GetStringToken(&tokenList,DAILY_CONSUMPTION_MAXOUTDOORTEMP_POS))*100);
            }
            else
            {
                consumption.Date.Day = NULL;
                consumption.Date.DayOfWeek = NULL;
                consumption.Date.Month = NULL;
                consumption.Date.Year = NULL;
                consumption.Consumption = NULL;
                consumption.MinimumTemperature = DB_SENSOR_INVALID;
                consumption.MaximumTemperature = DB_SENSOR_INVALID;
            }
            ClearStringTokenList (&tokenList);
        }
        else
        {
            DBTHRD_GetData(DBTYPE_DAILY_CONSUMPTION, (void*)&consumption, day);
            //Save to database with LOCAL source to force update the cloud
            DBTHRD_SetData(DBTYPE_DAILY_CONSUMPTION,(void*)&consumption, day, DBCHANGE_LOCAL);
        }
        //Save to database
        DBTHRD_SetData(DBTYPE_DAILY_CONSUMPTION,(void*)&consumption, day, DBCHANGE_CLOUD);
        day++;
    }
    ClearStringTokenList (&dailyTokenList);
}

/*******************************************************************************
* @brief  Receive the Home Monthly Consumption from the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/28
*******************************************************************************/
void ReceiveMonthlyConsumption(aylaTLV_t* monthlyConsumptionTLV)
{
#define MONTHLY_CONSUMPTION_POWER_POS                 0
#define MONTHLY_CONSUMPTION_AVGOUTDOORTEMP_POS        1

    uint8_t month;
    strTokenList_t monthlyTokenList;
    strTokenList_t tokenList;
    dbType_MonthlyConsumption_t consumption;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : HomeMonthlyConsumption");
#endif

    month = 0;

	//Add a NULL character at the end of the data
    monthlyConsumptionTLV->value[monthlyConsumptionTLV->length] = 0;
    StringTokenizer (&monthlyTokenList, (char*)monthlyConsumptionTLV->value, ";");

    //Loop on all months
    while (month < MONTHLYCONSUMPTION_NUMBEROFMONTHS)
    {
        if (month < monthlyTokenList.nbOfTokens)
        {
            StringTokenizer (&tokenList, GetStringToken(&monthlyTokenList,month), ",");

            if (tokenList.nbOfTokens == 2)
            {
                consumption.Consumption = atoi(GetStringToken(&tokenList,MONTHLY_CONSUMPTION_POWER_POS));
                consumption.AverageTemperature = (TEMPERATURE_C_t)(atof(GetStringToken(&tokenList,MONTHLY_CONSUMPTION_AVGOUTDOORTEMP_POS))*100);
            }
            else
            {
                consumption.Consumption = NULL;
                consumption.AverageTemperature = DB_SENSOR_INVALID;
            }
            ClearStringTokenList (&tokenList);
        }
        else
        {
            DBTHRD_GetData(DBTYPE_MONTHLY_CONSUMPTION, (void*)&consumption, month);
            //Save to database with LOCAL source to force update the cloud
            DBTHRD_SetData(DBTYPE_MONTHLY_CONSUMPTION,(void*)&consumption, month, DBCHANGE_LOCAL);
        }
        DBTHRD_SetData(DBTYPE_MONTHLY_CONSUMPTION,(void*)&consumption, month, DBCHANGE_CLOUD);
        month++;
    }
    ClearStringTokenList (&monthlyTokenList);
}

/*******************************************************************************
* @brief  Receive the Thermostat Model from the Cloud
* @inputs pData: void pointer to the thermostat instance
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/12/05
*******************************************************************************/
void ReceiveThermostatModel(aylaTLV_t* TstatModelTLV)
{
    char *receivedTstatModel;
    char model[MODEL_EXTENDED_MAX_LENGTH];
    uint8_t tstat;
    strTokenList_t tokenList;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : TstatModel");
#endif

    tstat = 0;

	//Add a NULL character at the end of the data
    TstatModelTLV->value[TstatModelTLV->length] = 0;
    StringTokenizer (&tokenList, (char*)TstatModelTLV->value, ";");

    //Loop on all thermostat instances
    while (tstat < THERMOSTAT_INSTANCES)
    {
        if (tstat < tokenList.nbOfTokens)
        {
            receivedTstatModel = GetStringToken(&tokenList,tstat);
        }
        else
        {
            receivedTstatModel = model;
            DBTHRD_GetData(DBTYPE_TSTAT_MODEL, (void*)receivedTstatModel, tstat);
            //Save to database with LOCAL source to force update the cloud
            DBTHRD_SetData(DBTYPE_TSTAT_MODEL,(void*)&receivedTstatModel, tstat, DBCHANGE_LOCAL);
        }
        //Save to database
        DBTHRD_SetData(DBTYPE_TSTAT_MODEL,(void*)&receivedTstatModel, tstat, DBCHANGE_CLOUD);
        tstat++;
    }
    ClearStringTokenList (&tokenList);
}

/*******************************************************************************
* @brief  Receive the Debug Level flag from the Cloud
* @inputs pData: void pointer to index (don't care)
* @retval None
* @author Jean-Fran�ois Many
* @date 2018/12/14
*******************************************************************************/
void ReceiveDebugLevel(aylaTLV_t* DebugLevelTLV)
{
    uint8_t receivedDebugLevel;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : DebugLevel");
#endif

    receivedDebugLevel = (uint8_t)DeserializeToInteger32Bits (DebugLevelTLV->value, DebugLevelTLV->length);

    DBTHRD_SetData(DBTYPE_DEBUG_LEVEL,(void*)&receivedDebugLevel, INDEX_DONT_CARE, DBCHANGE_CLOUD);
}

/*******************************************************************************
* @brief  Receive the Daily Consumpio from the Cloud
* @inputs pData: void pointer to the data
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
void ReceiveWeatherCondition(aylaTLV_t* pData)
{
    char *receivedWeatherCondition;
    strTokenList_t tokenList;

#ifdef FREERTOS_SUPPORT
    vTracePrint(wifiEventChannel, "Property received : WeatherCondition");
#endif

	//Add a NULL character at the end of the data
    pData->value[pData->length] = 0;
    StringTokenizer (&tokenList, (char*)pData->value, "/");

    receivedWeatherCondition = GetStringToken(&tokenList,0);
    //Save to database
    DBTHRD_SetData(DBTYPE_WEATHER_CONDITION,(void*)receivedWeatherCondition, INDEX_DONT_CARE, DBCHANGE_CLOUD);
	ClearStringTokenList(&tokenList);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void * allocate_tlv_memory (size_t size)
{
    void * mem_ptr = NULL;

    while (NULL == mem_ptr)
    {
        mem_ptr = pvPortMalloc(size);

        if (NULL == mem_ptr)
        {
            osDelay(1);
        }
    }
    return mem_ptr;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/10/06
*******************************************************************************/
void ParseReceivedProperties (ayla_Packet_t * receivedPacket, uint16_t packetLength)
{
    uint8_t i;

    ayla_incomingTLV_t * firstTLV;
    ayla_incomingTLV_t * nextTLV;
    ayla_incomingTLV_t * nameTLV;
    static aylaTLV_t valueTLV =
    {
       .next = NULL,
    };

    firstTLV = (ayla_incomingTLV_t *)receivedPacket->Payload;

	nameTLV = NULL;

    if (firstTLV->Type == TLVTYPE_NAME)
    {
		nameTLV = firstTLV;
        nextTLV = (ayla_incomingTLV_t *)&nameTLV->Value[nameTLV->Length];

		if (TLVTYPE_LENGTH == nextTLV->Type)
		{
			valueTLV.isLongString = 1;
			valueTLV.longStringOffset = 0;
			valueTLV.longStringTotalLength = (uint16_t)DeserializeToInteger32Bits(nextTLV->Value, nextTLV->Length);
			nextTLV = (ayla_incomingTLV_t *)&nextTLV->Value[nextTLV->Length];
			if (NULL != valueTLV.value)
			{
				vPortFree(valueTLV.value);
			}

			/* malloc valueTLV.longStringTotalLength+1  */
			/*  +1 is to allow, in case a text TLV, to  */
			/*  add a '\0' character a the end of the   */
			/*  non NULL terminated string              */
			valueTLV.value = allocate_tlv_memory(valueTLV.longStringTotalLength+1);
		}
		else if (TLVTYPE_OFFSET == nextTLV->Type)
		{
			valueTLV.longStringOffset = (uint16_t)DeserializeToInteger32Bits(nextTLV->Value, nextTLV->Length);
			nextTLV = (ayla_incomingTLV_t *)&nextTLV->Value[nextTLV->Length];
		}
		else
		{
			valueTLV.isLongString = 0;
			valueTLV.longStringOffset = 0;

			if (NULL != valueTLV.value)
			{
				vPortFree(valueTLV.value);
			}

			/* malloc nextTLV->Length+1                 */
			/*  +1 is to allow, in case a text TLV, to  */
			/*  add a '\0' character a the end of the   */
			/*  non NULL terminated string              */
			valueTLV.value = allocate_tlv_memory(nextTLV->Length+1);
		}

		valueTLV.type = nextTLV->Type;
		valueTLV.length = nextTLV->Length;
		if (NULL != valueTLV.value)
		{
			memcpy (&valueTLV.value[valueTLV.longStringOffset], nextTLV->Value, valueTLV.length);
		}

		if (0 != valueTLV.isLongString)
		{
			if ((valueTLV.longStringOffset + valueTLV.length) < valueTLV.longStringTotalLength)
			{
				nameTLV = NULL;
			}
			else
			{
				valueTLV.length = valueTLV.longStringTotalLength;
			}
		}
    }

    if (nameTLV != NULL)
    {
		char propertyName[PROPERTY_NAME_MAX_LENGTH];

		stringNCopyWithNull (propertyName,
							(char*)nameTLV->Value,
							((nameTLV->Length+1)<sizeof(propertyName))?
									(nameTLV->Length+1):
									sizeof(propertyName));


        /************************************************************/
        /*      Search for property definition                      */
        /************************************************************/
        for (i = 0; i < MAX_PROPERTY_TYPE; i++)
        {
            if (0 == strcmp ((const char*)propertyName, (const char *)propTable[i].propertyName))
            {
                if (propTable[i].receiveFunction != NULL)
                {
                    char string_buffer[100] = "Handling received property : ";

                    strcat(string_buffer,propertyName);
                    vTracePrint(wifiEventChannel, string_buffer);
                    propTable[i].receiveFunction(&valueTLV);
                }
                vPortFree(valueTLV.value);
                valueTLV.value = NULL;
                valueTLV.isLongString = 0;
                valueTLV.longStringOffset = 0;
                valueTLV.longStringTotalLength = 0;
                break;
            }
        }
    }

    //Check if all "To-Device" properties are updated from the Cloud
#ifdef __ICCARM__
    DBTHRD_END_OF_PROPERTY_UPDATE_CHECK();
#endif
}

/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
