#[ Contrôleur TUX](https://stelpro.atlassian.net/wiki/display/TUX/TUX)#

Le projet logiciel Contrôleur TUX est un projet qui comporte plusieurs blocs fonctionnel :

* BootLoader
* Communication Wifi
* Communication Zigbee
* Affichage TFT
* Touch Screen Capacitif
* Sensors (Humidité, température, proximité, courant (A))

À l'intérieur de ce gros projet, on retrouve de plus petit projet logiciel (.ewp), dont 

* Le projet BootLoader
* Le projet Bin_Write
* Le projet STM32F429ZI_DevKit
* Le projet GraphicLCDTest

## [Projet BootLoader](https://stelpro.atlassian.net/wiki/display/TUX/TUX+OTAU) ##
Le projet bootloader est un projet indépendant, mais essentiel de l'application principale du contrôleur.

Son rôle est de déterminer si une mise à jour logiciel est disponible, d'effectuer et de valider la mise à jour.
Il permet également de revenir sur une mise à jour si celle-ci ne s'est pas exécuter correctement.

## Projet Bin_Write ##
Ce projet permet simplement de charger une image de l'application dans une plage de la mémoire flash

## Projet STM32F429ZI_DevKit ##
Ce projet est un prototype complet du produit Contrôleur TUX, sans la partie puissance (120-240VAC).
La capacité des mémoires installées (microcontrôleur, flash externe, SDRAM) à été volontairement sur-budgeté dans le but de permettre l'évaluation des besoins.

## Projet GraphicLCDTest ##
Ce projet (disponible dans la branche .\feature\Display) sert (à servi) à développé l'expertise du groupe logiciel en ce qui concerne les interfaces graphique/touch ainsi que de prototyper un premier ébauche de l'interface utilisateur pour le contrôleur TUX.
La plateforme de développement utilisé est le 32F429DISCOVERY.  L'affichage graphique se fait sur un écran TFT 240X320 et l'interface "touch" est de type résistif.

# **Portabilité** #
Les premières phase de développement du projet de contrôleur TUX se font sur différentes plate-forme en fonction du bloc fonctionnel.  Il est important de pouvoir réutiliser ces blocs sur les versions finales du matériel.

C'est pourquoi l'arborescence des dossiers est séparé en fonction des différents blocs fonctionnel et du microcontroleur.