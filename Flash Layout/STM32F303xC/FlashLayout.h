/*******************************************************************************
* @file    FLASH_LAYOUT.h
* @date    2016/06/15
* @authors J-F. Simard
* @brief STM32F303xC Flash information.
*       Contains information about the flash layout for OTA along with
*       informations about the Flash memory of the microcontroller
*
*******************************************************************************/

#ifndef FLASH_LAYOUT_H
#define FLASH_LAYOUT_H


/*******************************************************************************
* Includes
*******************************************************************************/



/*******************************************************************************
* Public constants definitions
*******************************************************************************/
typedef enum
{
    FLASH_BASE_BOOTLOADER       = 0x08000000,
    FLASH_BASE_PROGRESS_BAR     = 0x08001800,
    FLASH_BASE_ACTIVE_IMAGE     = 0x08002000,
    FLASH_BASE_INACTIVE_IMAGE   = 0x08020000,
    FLASH_BASE_SCRATCH_AREA     = 0x0803E000,
    FLASH_BASE_LAST             = 0x0803FFFF
} FlashLayout_t;

#define FLASH_SCRATCH_AREA_SIZE     ((FLASH_BASE_LAST+1)-FLASH_BASE_SCRATCH_AREA)
#define FLASH_IMAGE_SIZE            (FLASH_BASE_INACTIVE_IMAGE-FLASH_BASE_ACTIVE_IMAGE)

#define FLASH_SWAPPING_BLOCK_CNT    (FLASH_IMAGE_SIZE/FLASH_SCRATCH_AREA_SIZE)
#define FLASH_PAGE_SIZE             0x800       //STM32F303xC : 2048 bytes
#define FLASH_PAGE_PER_BLOCK        (FLASH_SCRATCH_AREA_SIZE/FLASH_PAGE_SIZE)

#define NATIVE_ADRESS_BUS_SIZE      4               //express in number of bytes

/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/



#endif /* _FLASH_LAYOUT_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
