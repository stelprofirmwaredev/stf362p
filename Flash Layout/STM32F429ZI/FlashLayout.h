/*******************************************************************************
* @file    FLASH_LAYOUT.h
* @date    2016/06/15
* @authors J-F. Simard
* @brief STM32F429ZI Flash information.
*       Contains information about the flash layout for OTA along with
*       informations about the Flash memory of the microcontroller
*
*******************************************************************************/

#ifndef FLASH_LAYOUT_H
#define FLASH_LAYOUT_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include ".\HAL\inc\HAL_FLASH.h"


/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define NATIVE_ADRESS_BUS_SIZE      4               //ARM 32 bits; expressed in number of bytes
#define IMAGE_BANK_OFFSET           0x3B8000


typedef enum
{
    FLASH_BASE_BOOTLOADER               = 0x08000000,
    FLASH_BASE_OTA_CONTROL              = 0x08004000,
    FLASH_BASE_ACTIVE_IMAGE             = 0x08008000,
    FLASH_BASE_CRC                      = 0x0807FFFC,
    FLASH_BASE_LAST                     = 0x0807FFFF,

    EXTFLASH_HOST_IMAGE_A_START         = 0x00000000,                        //external flash memory
    EXTFLASH_HOST_IMAGE_A_END           = 0x0017FFFF,                        //external flash memory
    EXTFLASH_ZIGBEE_IMAGE_A_START       = 0x00180000,
    EXTFLASH_ZIGBEE_IMAGE_A_END         = 0x00236FFF,
    EXTFLASH_IMAGELIB_A_START           = 0x00237000,
    EXTFLASH_IMAGELIB_A_END             = 0x003B6FFB,
    EXTFLASH_CRC_A                      = 0x003B6FFC,
    EXTFLASH_HOST_IMAGE_B_START         = EXTFLASH_HOST_IMAGE_A_START+IMAGE_BANK_OFFSET,
    EXTFLASH_HOST_IMAGE_B_END           = EXTFLASH_HOST_IMAGE_A_END+IMAGE_BANK_OFFSET,                        //external flash memory
    EXTFLASH_ZIGBEE_IMAGE_B_START       = EXTFLASH_ZIGBEE_IMAGE_A_START+IMAGE_BANK_OFFSET,
    EXTFLASH_ZIGBEE_IMAGE_B_END         = EXTFLASH_ZIGBEE_IMAGE_A_END+IMAGE_BANK_OFFSET,
    EXTFLASH_IMAGELIB_B_START           = EXTFLASH_IMAGELIB_A_START+IMAGE_BANK_OFFSET,
    EXTFLASH_IMAGELIB_B_END             = EXTFLASH_IMAGELIB_A_END+IMAGE_BANK_OFFSET,
    EXTFLASH_CRC_B                      = EXTFLASH_CRC_A+IMAGE_BANK_OFFSET,
} FlashLayout_t;

#define ACTIVE_IMAGE_SECTOR_ID          2

typedef enum
{
    FLASH_SECTOR_16K = 0x4000,
    FLASH_SECTOR_64K = 0x10000,
    FLASH_SECTOR_128K = 0x20000
} FlashSector_Size_t;


#define FLASH_FIRMWARE_BLOCK_CNT    10              //STM32F429ZI : total 24 sectors
                                                    //      - 2 for boot and progress
                                                    //      - 12; Use only first 1MB

#define FLASH_SECTOR_OFFSET         2           //active image starts at sector 2
#define FLASH_FIRST_SECTOR          FLASH_SECTOR_OFFSET
#define FLASH_LAST_SECTOR           FLASH_SECTOR_7

#define EXTFLASH_MAX_BYTE_PER_TRANSACTION       256
#define EXTFLASH_BYTE_PER_PAGE                  MX25_SECTOR_SIZE

#define SIZEOFCRC                       sizeof(uint32_t)
#define OTAUIMAGE_MAX_SIZE              (EXTFLASH_CRC_A + SIZEOFCRC)
#define SMT32RECORD_MAX_SIZE            (EXTFLASH_ZIGBEE_IMAGE_A_START - EXTFLASH_HOST_IMAGE_A_START)


#define NVMPAGE_SIZE            0x1000          //4096 => 4KB
#define NVM_START_PAGE          (EXTFLASH_HOST_IMAGE_B_START + IMAGE_BANK_OFFSET)
#define NVM_END                 0x7fffff
#define NVM_PAGES_AMOUNT        ((NVM_END - NVM_START_PAGE)/NVMPAGE_SIZE)

/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/



#endif /* _FLASH_LAYOUT_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
