/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2019, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    HAL_TemperatureAcquisition_FLORE.c
* @date    2019/05/07
* @authors Jean-Fran�ois Many
* @brief   Temperature Acquisition Module
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include "typedef.h"
#include "stm32f4xx_hal.h"
#include "HAL_Interrupt.h"
#include ".\HAL\inc\HAL_TemperatureAcquisition.h"
#include ".\DB\inc\THREAD_DB.h"
#include "common_types.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define TEMPERATURE_READ_TIMEOUT        (HAL_MAX_DELAY-1)
#define ACQ_MAX                         40
#define REFERENCE_CHANNEL               ADC_CHANNEL_VREFINT //Internal reference
#define AMBIENT_CHANNEL                 ADC_CHANNEL_15      //PC5
#define COMP_CHANNEL                    ADC_CHANNEL_14      //PC4
#define HT_CHANNEL                      ADC_CHANNEL_7       //PA7
//#define TABLE_SIZE_NTCG163JF103FT1      (25)                        // Number of interpolation point use for the thermistance NTCG163JF103FT1
#define TABLE_INDEX_MIN40_C             0
#define TABLE_INDEX_0_C                 8
#define TABLE_INDEX_40_C                16
#define TABLE_INDEX_80_C                24
#define THERMISTOR_ALIM_FAULT_ADC_VALUE 3102                        // valeur A/D de 3102 �quivaut � 2.5V
#define TEMP_INTERNAL_SCALING           100                        	//Scaling factor to get resolution */
#define TEMP_INTERNAL_MIN               -40 * TEMP_INTERNAL_SCALING //Minimal sensing temperature for compensation (Celcius) */
#define TEMP_INTERNAL_MAX               80 * TEMP_INTERNAL_SCALING  //Maximal sensing temperature for compensation (Celcius) */
#define DELTA_TABLE_INTERNAL            5 * TEMP_INTERNAL_SCALING   //Temperature variation between each ADC value in thermistance (Celcius) */
#define DISPLAY_HYSTERESIS              m_DegC(0.13)                //Highly scientific value

#define TABLE_SIZE                      25
#define _R1                             10000


/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
static int16_t AmbientTemperature;
static int16_t CompensationTemperature;
static int16_t HighTemperature;
static ADC_HandleTypeDef TemperatureAcquisition_ADC;
static U16BIT reference_count;
static U16BIT ambient_count;
static U16BIT compensation_count;
static U16BIT hightemp_count;

const unsigned int AcqTable[TABLE_SIZE] =
{
    195652, //-40�C
    148171, //-35�C
    113347, //-30�C
    87559,  //-25�C
    68237,  //-20�C
    53650,  //-15�C
    42506,  //-10�C
    33892,  //-5�C
    27219,  //0�C
    22021,  //5�C
    17926,  //10�C
    14674,  //15�C
    12081,  //20�C
    10000,  //25�C
    8315,   //30�C
    6948,   //35�C
    5834,   //40�C
    4917,   //45�C
    4161,   //50�C
    3535,   //55�C
    3014,   //60�C
    2586,   //65�C
    2228,   //70�C
    1925,   //75�C
    1669    //80�C
};
//// Table de correspondance valeur A/D / degr�s C pour la thermistance NTCG163JF103FT1
//// se r�f�rer au fichier Excel Temp�rature Thermistance
//const unsigned int RES_TAB_NTCG163JF103FT1[TABLE_SIZE_NTCG163JF103FT1] =
//{
//    3890,   //-40�C
//    3831,   //-35�C
//    3758,   //-30�C
//    3672,   //-25�C
//    3569,   //-20�C
//    3451,   //-15�C
//    3315,   //-10�C
//    3164,   //-5�C
//    2997,   //0�C
//    2819,   //5�C
//    2631,   //10�C
//    2438,   //15�C
//    2242,   //20�C
//    2048,   //25�C
//    1859,   //30�C
//    1678,   //35�C
//    1509,   //40�C
//    1349,   //45�C
//    1203,   //50�C
//    1071,   //55�C
//    950,    //60�C
//    843,    //65�C
//    747,    //70�C
//    660,    //75�C
//    586     //80�C
//};

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static int32_t convert_adc_value_to_R (int32_t sensor, int32_t reference);
static int32_t convert_R_to_T_with_Table(int32_t R);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief : In this hardware configuration, the thermistor is assemble as
*           voltage divider with a 10kohms series resistor
*
*          At the end of the conversion, the sensor's adc count represent
*           the voltage divider's voltage (Vout), expressed in 12 bits count
*            (max 4096)
*          The reference's adc count represent the voltage of Vrefint,
*           expressed in 12 bits count.
*
*          This function transforms both adc counts (sensor & reference) into
*           a R value using the following equation:
*
*        Rth = (-Vout*R1)/(Vout-Vin)=> from base equation  Vout = Vin*Rth/(Rth+R1)
*
*           Vin is evaluated using the calibration value of Vrefint when
*           measured @ Vdd = 3.0V and the measured reference count
*
*        Vin = (3.0V * VrefCAL)/ reference
*
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static int32_t convert_adc_value_to_R (int32_t sensor, int32_t reference)
{
    uint16_t *cal_vref_int = ((uint16_t*) ((uint32_t)0x1FFF7A2AU));

    float Vin = 3.3 * *cal_vref_int/reference;
    float Vout = sensor * Vin / 4095;
    int32_t _RTh = (int32_t)((-Vout*_R1)/(Vout-Vin));

    return _RTh;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static int32_t convert_R_to_T_with_Table(int32_t R)
{
    int32_t temperature;
    int32_t deltaY;
    int32_t deltaX;
    uint8_t indexLow;
    uint8_t indexHigh;

    if (R > AcqTable[TABLE_INDEX_MIN40_C])
    {
        temperature = HAL_TEMPSENSOR_INVALID;
    }
    else if (R > AcqTable[TABLE_INDEX_0_C])
    {
        temperature = HAL_TEMPSENSOR_LOW;
    }
    else if (R < AcqTable[TABLE_INDEX_80_C])
    {
        temperature = HAL_TEMPSENSOR_HIGH;
    }
    else
    {
        indexLow = TABLE_SIZE - 1;
        while (R > AcqTable[indexLow])
        {
            indexLow = indexLow - 1;
        }

        indexHigh = indexLow + 1;
        deltaX = R - AcqTable[indexHigh];
        deltaY = AcqTable[indexLow] - AcqTable[indexHigh];
        deltaX = deltaX * DELTA_TABLE_INTERNAL;
        deltaX = deltaX/deltaY;
        return ((indexHigh*DELTA_TABLE_INTERNAL) - deltaX) + TEMP_INTERNAL_MIN;
    }

    return temperature;
}


/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  Temperature Sensor initialization function
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/09/12
*******************************************************************************/
void TemperatureSensor_Init(void)
{
    AmbientTemperature = HAL_TEMPSENSOR_INVALID;
    ambient_count = 0;
    compensation_count = 0;
    hightemp_count = 0;

    /**Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
    */
    TemperatureAcquisition_ADC.Instance = ADC1;
    TemperatureAcquisition_ADC.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
    TemperatureAcquisition_ADC.Init.Resolution = ADC_RESOLUTION_12B;
    TemperatureAcquisition_ADC.Init.ScanConvMode = DISABLE;
    TemperatureAcquisition_ADC.Init.ContinuousConvMode = DISABLE;
    TemperatureAcquisition_ADC.Init.DiscontinuousConvMode = DISABLE;
    TemperatureAcquisition_ADC.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
    TemperatureAcquisition_ADC.Init.DataAlign = ADC_DATAALIGN_RIGHT;
    TemperatureAcquisition_ADC.Init.NbrOfConversion = 1;
    TemperatureAcquisition_ADC.Init.DMAContinuousRequests = DISABLE;
    TemperatureAcquisition_ADC.Init.EOCSelection = ADC_EOC_SINGLE_CONV;

    HAL_ADC_MspInit(&TemperatureAcquisition_ADC);

    if (HAL_ADC_Init(&TemperatureAcquisition_ADC) != HAL_OK)
    {
//        Error_Handler();
    }
}

/*******************************************************************************
* @brief  Temperature Acquisition monitoring function
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/09/12
*******************************************************************************/
void HAL_TemperatureAcquisitionMonitoring(void)
{
    ADC_ChannelConfTypeDef sConfig;
    HAL_StatusTypeDef status = HAL_OK;
    static U32BIT buffer_temperature = 0;
    static U32BIT buffer_compensation = 0;
    static U32BIT buffer_hightemp = 0;
    static U32BIT buffer_reference = 0;
    U8BIT samples;

    HAL_GPIO_WritePin(T_ACQ_ENABLE_GPIO_Port, T_ACQ_ENABLE_Pin, GPIO_PIN_SET);
    osDelay(10);
    for (samples = 0; samples < ACQ_MAX; samples++)
    {
        /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
        */
        sConfig.Channel = REFERENCE_CHANNEL;
        sConfig.Rank = 1;
        sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
        if (HAL_ADC_ConfigChannel(&TemperatureAcquisition_ADC, &sConfig) != HAL_OK)
        {
//            Error_Handler();
        }
        HAL_ADC_Start(&TemperatureAcquisition_ADC);
        status = HAL_ADC_PollForConversion(&TemperatureAcquisition_ADC, TEMPERATURE_READ_TIMEOUT);
        if (status == HAL_OK)
        {
            HAL_ADC_GetValue(&TemperatureAcquisition_ADC);
            reference_count = TemperatureAcquisition_ADC.Instance->DR;
        }
        HAL_ADC_Stop(&TemperatureAcquisition_ADC);

        /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
        */
        sConfig.Channel = AMBIENT_CHANNEL;
        sConfig.Rank = 1;
        sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
        if (HAL_ADC_ConfigChannel(&TemperatureAcquisition_ADC, &sConfig) != HAL_OK)
        {
//            Error_Handler();
        }
        HAL_ADC_Start(&TemperatureAcquisition_ADC);
        status = HAL_ADC_PollForConversion(&TemperatureAcquisition_ADC, TEMPERATURE_READ_TIMEOUT);
        if (status == HAL_OK)
        {
            HAL_ADC_GetValue(&TemperatureAcquisition_ADC);
            ambient_count = TemperatureAcquisition_ADC.Instance->DR;
        }
        HAL_ADC_Stop(&TemperatureAcquisition_ADC);

        /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
        */
        sConfig.Channel = COMP_CHANNEL;
        sConfig.Rank = 1;
        sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
        if (HAL_ADC_ConfigChannel(&TemperatureAcquisition_ADC, &sConfig) != HAL_OK)
        {
//            Error_Handler();
        }
        HAL_ADC_Start(&TemperatureAcquisition_ADC);
        status = HAL_ADC_PollForConversion(&TemperatureAcquisition_ADC, TEMPERATURE_READ_TIMEOUT);
        if (status == HAL_OK)
        {
            HAL_ADC_GetValue(&TemperatureAcquisition_ADC);
            compensation_count = TemperatureAcquisition_ADC.Instance->DR;
        }
        HAL_ADC_Stop(&TemperatureAcquisition_ADC);

        /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
        */
        sConfig.Channel = HT_CHANNEL;
        sConfig.Rank = 1;
        sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
        if (HAL_ADC_ConfigChannel(&TemperatureAcquisition_ADC, &sConfig) != HAL_OK)
        {
//            Error_Handler();
        }
        HAL_ADC_Start(&TemperatureAcquisition_ADC);
        status = HAL_ADC_PollForConversion(&TemperatureAcquisition_ADC, TEMPERATURE_READ_TIMEOUT);
        if (status == HAL_OK)
        {
            HAL_ADC_GetValue(&TemperatureAcquisition_ADC);
            hightemp_count = TemperatureAcquisition_ADC.Instance->DR;
        }
        HAL_ADC_Stop(&TemperatureAcquisition_ADC);

        buffer_reference += reference_count;
        buffer_temperature += ambient_count;
        buffer_compensation += compensation_count;
        buffer_hightemp += hightemp_count;
    }
    HAL_GPIO_WritePin(T_ACQ_ENABLE_GPIO_Port, T_ACQ_ENABLE_Pin, GPIO_PIN_RESET);
    reference_count = buffer_reference / ACQ_MAX;
    ambient_count = buffer_temperature / ACQ_MAX;
    compensation_count = buffer_compensation / ACQ_MAX;
    hightemp_count = buffer_hightemp / ACQ_MAX;

    AmbientTemperature = convert_R_to_T_with_Table
        (convert_adc_value_to_R
         (ambient_count,
          reference_count));
    CompensationTemperature = convert_R_to_T_with_Table
        (convert_adc_value_to_R
         (compensation_count,
          reference_count));
    HighTemperature = convert_R_to_T_with_Table
        (convert_adc_value_to_R
         (hightemp_count,
          reference_count));

    DBTHRD_SetData(DBTYPE_COMPENSATION_TEMPERATURE_READING,(void*)&CompensationTemperature, INDEX_DONT_CARE, DBCHANGE_LOCAL);
    DBTHRD_SetData(DBTYPE_HIGH_TEMPERATURE_READING,(void*)&HighTemperature, INDEX_DONT_CARE, DBCHANGE_LOCAL);
    DBTHRD_SetData(DBTYPE_AMBIENT_TEMPERATURE_READING,(void*)&AmbientTemperature, INDEX_DONT_CARE, DBCHANGE_LOCAL);
    buffer_reference = 0;
    buffer_temperature = 0;
    buffer_compensation = 0;
    buffer_hightemp = 0;
}

/*******************************************************************************
* @brief  Retrieve the actual temperature
* @inputs None
* @retval U8BIT: temperature
* @author Jean-Fran�ois Many
* @date   2016/09/12
*******************************************************************************/
U16BIT HAL_TemperatureAcquisition_GetTemperature(void)
{
    return AmbientTemperature;
}

//***********************************************************************************
///	\brief
///
///	\param  None.
///	\return TRUE if any sensor is invalid, FALSE if all sensors are valid.
//***********************************************************************************
U8BIT TEMP_IsSensorValid(S16BIT sensor)
{
    switch (sensor)
    {
        case HAL_TEMPSENSOR_INVALID:
        case HAL_TEMPSENSOR_HIGH:
        case HAL_TEMPSENSOR_LOW:
            return FALSE;

        default:
            return TRUE;
    }
}

/*******************************************************************************
* @brief  Retrieve the ADC channel counts
* @inputs Pointers to the 3 ADC channels count
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/05/26
*******************************************************************************/
void TEMP_GetADCCount(U16BIT* ambientCount, U16BIT* compCount, U16BIT* powerCount)
{
    *ambientCount = ambient_count;
    *compCount = compensation_count;
    *powerCount = hightemp_count;
}


/** Copyright(C) 2019 Stelpro Design, All Rights Reserved**/
