/*******************************************************************************
* @file    HAL_TemperatureAcquisition.h
* @date    2016/09/12
* @authors Jean-Fran�ois Many
* @brief
*******************************************************************************/

#ifndef HAL_TemperatureAcquisition_H
#define HAL_TemperatureAcquisition_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include "typedef.h"


/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define HAL_TEMPSENSOR_INVALID      DB_SENSOR_INVALID
#define HAL_TEMPSENSOR_HIGH         DB_SENSOR_HIGH
#define HAL_TEMPSENSOR_LOW          DB_SENSOR_LOW


/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void TemperatureSensor_Init(void);
void HAL_TemperatureAcquisitionMonitoring(void);
U16BIT HAL_TemperatureAcquisition_GetTemperature(void);
U8BIT TEMP_IsSensorValid(S16BIT sensor);
void TEMP_GetADCCount(U16BIT* ambientCount, U16BIT* compCount, U16BIT* powerCount);


#endif /* HAL_TemperatureAcquisition_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
