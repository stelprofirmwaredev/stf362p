/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2014, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    TFT_ILI9341.c
* @date    2015/11/16
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include "TFT_ILI9341.h"
#include <stdint.h>
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_spi.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define SPI_WRITE_DELAY (HAL_MAX_DELAY-1)


/*******************************************************************************
*    Private variables definitions
*******************************************************************************/



/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
void LCD_WriteCommand(SPI_HandleTypeDef * spiHandle, uint8_t Command);
void LCD_WriteData(SPI_HandleTypeDef * spiHandle, uint8_t Data);

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/**
  * @brief  Inserts a delay time.
  * @param  nCount: specifies the delay time length.
  * @retval None
  */
static void delay(volatile uint32_t nCount)
{
  volatile uint32_t index = 0;
  for(index = nCount; index != 0; index--)
  {
  }
}

/*******************************************************************************
* @brief  Initialization of the TFT Lcd controller ILI9341
*					for use with RGB parrallel interface
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2015/11/16
*******************************************************************************/
void LCD_PowerOn(SPI_HandleTypeDef * spiHandle)
{
	HAL_GPIO_WritePin(CSX_GPIO_Port, CSX_Pin, GPIO_PIN_SET);
	delay(100);

/*	Command ID not documented, and does not provide any visual difference if not used */
		LCD_WriteCommand(spiHandle, 0xCA);
		LCD_WriteData(spiHandle, 0xC3);
		LCD_WriteData(spiHandle, 0x08);
		LCD_WriteData(spiHandle, 0x50);

	LCD_WriteCommand(spiHandle, LCD_POWERB);
	LCD_WriteData(spiHandle, 0x00);
	LCD_WriteData(spiHandle, 0xC1);
	LCD_WriteData(spiHandle, 0x30);

	LCD_WriteCommand(spiHandle, LCD_POWER_SEQ);
	LCD_WriteData(spiHandle, 0x64);
	LCD_WriteData(spiHandle, 0x03);
	LCD_WriteData(spiHandle, 0x12);
	LCD_WriteData(spiHandle, 0x81);

	LCD_WriteCommand(spiHandle, LCD_DTCA);
	LCD_WriteData(spiHandle, 0x85);
	LCD_WriteData(spiHandle, 0x00);
	LCD_WriteData(spiHandle, 0x78);

	LCD_WriteCommand(spiHandle, LCD_POWERA);
	LCD_WriteData(spiHandle, 0x39);
	LCD_WriteData(spiHandle, 0x2C);
	LCD_WriteData(spiHandle, 0x00);
	LCD_WriteData(spiHandle, 0x34);
	LCD_WriteData(spiHandle, 0x02);

	LCD_WriteCommand(spiHandle, LCD_PRC);
	LCD_WriteData(spiHandle, 0x20);

	LCD_WriteCommand(spiHandle, LCD_DTCB);
	LCD_WriteData(spiHandle, 0x00);
	LCD_WriteData(spiHandle, 0x00);

	LCD_WriteCommand(spiHandle, LCD_FRC);
	LCD_WriteData(spiHandle, 0x00);
	LCD_WriteData(spiHandle, 0x1B);

	LCD_WriteCommand(spiHandle, LCD_DFC);
	LCD_WriteData(spiHandle, 0x0A);
	LCD_WriteData(spiHandle, 0xA2);

	LCD_WriteCommand(spiHandle, LCD_POWER1);
	LCD_WriteData(spiHandle, 0x10);

	LCD_WriteCommand(spiHandle, LCD_POWER2);
	LCD_WriteData(spiHandle, 0x10);

	LCD_WriteCommand(spiHandle, LCD_VCOM1);
	LCD_WriteData(spiHandle, 0x45);
	LCD_WriteData(spiHandle, 0x15);

	LCD_WriteCommand(spiHandle, LCD_VCOM2);
	LCD_WriteData(spiHandle, 0x90);


	LCD_WriteCommand(spiHandle, LCD_MAC);
	LCD_WriteData(spiHandle, 0xC0);		//bit 7: MY =>Row address Order
										//bit 6: MX =>Column address Order
										//bit 5: MV =>Row/Column exchange
										//bit 4: ML =>Vertical refresh order
										//bit 3: BGR => BGR-RGB Order
										//bit 2: MH =>Horizontal refresh order


	LCD_WriteCommand(spiHandle, LCD_3GAMMA_EN);
	LCD_WriteData(spiHandle, 0x00);

	LCD_WriteCommand(spiHandle, LCD_RGB_INTERFACE);
	LCD_WriteData(spiHandle, 0xC2);

	LCD_WriteCommand(spiHandle, LCD_DFC);
	LCD_WriteData(spiHandle, 0x0A);
	LCD_WriteData(spiHandle, 0xA7);
	LCD_WriteData(spiHandle, 0x27);
	LCD_WriteData(spiHandle, 0x04);

	/* colomn address set */
	LCD_WriteCommand(spiHandle, LCD_COLUMN_ADDR);
	LCD_WriteData(spiHandle, 0x00);
	LCD_WriteData(spiHandle, 0x00);
	LCD_WriteData(spiHandle, 0x00);
	LCD_WriteData(spiHandle, 0xEF);
	/* Page Address Set */
	LCD_WriteCommand(spiHandle, LCD_PAGE_ADDR);
	LCD_WriteData(spiHandle, 0x00);
	LCD_WriteData(spiHandle, 0x00);
	LCD_WriteData(spiHandle, 0x01);
	LCD_WriteData(spiHandle, 0x3F);

	LCD_WriteCommand(spiHandle, LCD_INTERFACE);
	LCD_WriteData(spiHandle, 0x01);
	LCD_WriteData(spiHandle, 0x00);
	LCD_WriteData(spiHandle, 0x06);

	LCD_WriteCommand(spiHandle, LCD_GRAM);
	delay(200);

	LCD_WriteCommand(spiHandle, LCD_GAMMA);
	LCD_WriteData(spiHandle, 0x01);

	LCD_WriteCommand(spiHandle, LCD_PGAMMA);
	LCD_WriteData(spiHandle, 0x0F);
	LCD_WriteData(spiHandle, 0x29);
	LCD_WriteData(spiHandle, 0x24);
	LCD_WriteData(spiHandle, 0x0C);
	LCD_WriteData(spiHandle, 0x0E);
	LCD_WriteData(spiHandle, 0x09);
	LCD_WriteData(spiHandle, 0x4E);
	LCD_WriteData(spiHandle, 0x78);
	LCD_WriteData(spiHandle, 0x3C);
	LCD_WriteData(spiHandle, 0x09);
	LCD_WriteData(spiHandle, 0x13);
	LCD_WriteData(spiHandle, 0x05);
	LCD_WriteData(spiHandle, 0x17);
	LCD_WriteData(spiHandle, 0x11);
	LCD_WriteData(spiHandle, 0x00);
	LCD_WriteCommand(spiHandle, LCD_NGAMMA);
	LCD_WriteData(spiHandle, 0x00);
	LCD_WriteData(spiHandle, 0x16);
	LCD_WriteData(spiHandle, 0x1B);
	LCD_WriteData(spiHandle, 0x04);
	LCD_WriteData(spiHandle, 0x11);
	LCD_WriteData(spiHandle, 0x07);
	LCD_WriteData(spiHandle, 0x31);
	LCD_WriteData(spiHandle, 0x33);
	LCD_WriteData(spiHandle, 0x42);
	LCD_WriteData(spiHandle, 0x05);
	LCD_WriteData(spiHandle, 0x0C);
	LCD_WriteData(spiHandle, 0x0A);
	LCD_WriteData(spiHandle, 0x28);
	LCD_WriteData(spiHandle, 0x2F);
	LCD_WriteData(spiHandle, 0x0F);

	LCD_WriteCommand(spiHandle, LCD_SLEEP_OUT);
	delay(200);
	LCD_WriteCommand(spiHandle, LCD_DISPLAY_ON);
	/* GRAM start writing */
	LCD_WriteCommand(spiHandle, LCD_GRAM);


}

/*******************************************************************************
* @brief  Write Command to IL9341 register through SPI 4 line interface
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2015/11/16
*******************************************************************************/
void LCD_WriteCommand(SPI_HandleTypeDef * spiHandle, uint8_t Command)
{
	HAL_GPIO_WritePin(WRX_DCX_GPIO_Port, WRX_DCX_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(CSX_GPIO_Port, CSX_Pin, GPIO_PIN_RESET);

	HAL_SPI_Transmit(spiHandle,&Command,1,SPI_WRITE_DELAY);

	HAL_GPIO_WritePin(CSX_GPIO_Port, CSX_Pin, GPIO_PIN_SET);
}

/*******************************************************************************
* @brief  Write Data to ILI9341 registers through SPI 4 lines interface
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2015/11/16
*******************************************************************************/
void LCD_WriteData(SPI_HandleTypeDef * spiHandle, uint8_t Data)
{
	HAL_GPIO_WritePin(WRX_DCX_GPIO_Port, WRX_DCX_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(CSX_GPIO_Port, CSX_Pin, GPIO_PIN_RESET);

	HAL_SPI_Transmit(spiHandle,&Data,1,SPI_WRITE_DELAY);

	HAL_GPIO_WritePin(CSX_GPIO_Port, CSX_Pin, GPIO_PIN_SET);
}




/** Copyright(C) 2015 Stelpro Design, All Rights Reserved**/
