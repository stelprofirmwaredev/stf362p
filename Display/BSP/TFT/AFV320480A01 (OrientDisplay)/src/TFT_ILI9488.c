/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2014, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    TFT_ILI9488.c
* @date    2015/08/08
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include ".\BSP\TFT\AFV320480A01 (OrientDisplay)\inc\TFT_ILI9488.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define SPI_WRITE_DELAY (HAL_MAX_DELAY-1)


/*******************************************************************************
*    Private variables definitions
*******************************************************************************/



/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
void WriteSPICommand( uint8_t Command);
void WriteSPIData( uint8_t Data);

void SetDisplayFunctionCtrl (void);
void SetMemoryDataAccessCtrl (void);
void SetDisplayInversionCtrl (void);
void SetDisplayOutputCtrl (void);
void SetPowerControl(void);
void Enable_Command2 (void);
void Disable_Command2 (void);
void SetGammaCorrection (void);

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2015/08/08
*******************************************************************************/
static void delay_ms(volatile uint32_t nCount)
{
  volatile uint32_t index = 0;
  for(index = nCount; index != 0; index--)
  {
      for(volatile uint32_t i  = 0; i < _1MS_LOOP; i++);
  }
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2015/08/08
*******************************************************************************/
void SetDisplayFunctionCtrl (void)
{
   	WriteSPICommand(0xB6); //Display Function Control  Blanking Proch Control
	WriteSPIData(0x30);//RGB interface for RAM access, RGB transfer in DE mode  
						 //display data path in memory
						 //Gate outputs in non-display area in Normal scan 
						 //Source output in non-display area =V63
	WriteSPIData(0x22);//Source Output Scan from S1 to S960, Gate Output scan from G1 to G480, scan cycle=2
	WriteSPIData(0x3b);//LCD Drive Line=8*(59+1)

}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2015/08/08
*******************************************************************************/
void SetMemoryDataAccessCtrl (void)
{
  	WriteSPICommand(0x36); //Memory Data Access Control MX, MY, RGB mode                                    
	WriteSPIData(0x1c);//X-Mirror, Top-Left to right-Buttom, RGB  
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2015/08/08
*******************************************************************************/
void SetDisplayInversionCtrl (void)
{
    WriteSPICommand(0xB4); //Column inversion 
	WriteSPIData(0x01);//1-dot inversion

}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2015/08/08
*******************************************************************************/
void SetDisplayOutputCtrl (void)
{
    WriteSPICommand(0xe8); //Display Output Ctrl Adjust
	WriteSPIData(0x40);
	WriteSPIData(0x8a);	
	WriteSPIData(0x00);
	WriteSPIData(0x00);
	WriteSPIData(0x29);//Source eqaulizing period time= 22.5 us
	WriteSPIData(0x19);//Timing for "Gate start"=25 (Tclk)
	WriteSPIData(0x25);//Timing for "Gate End"=37 (Tclk), Gate driver EQ function ON
	WriteSPIData(0x33);
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2015/08/08
*******************************************************************************/
void SetPowerControl(void)
{
/************************************************************************/
/*      Power control2 : higher the value, darker is the display        */
/************************************************************************/
	WriteSPICommand(0xc1); //Power control2                          
	WriteSPIData(0x06);//VAP(GVDD)=5.5+( vcom+vcom offset), VAN(GVCL)=-5.5+( vcom+vcom offset)
	 
	WriteSPICommand(0xc2); //Power control 3                                      
	WriteSPIData(0xa7);//Source driving current level=low, Gamma driving current level=High
	 
	WriteSPICommand(0xc5); //VCOM Control
	WriteSPIData(0x17);//VCOM=1.875

//	WriteSPICommand(0xc6); //VCOM Offset Control
//	WriteSPIData(0x00);//From VMF_REG; VMF_REG 0
    
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2015/08/08
*******************************************************************************/
void TFT_SleepOut (void)
{
    WriteSPICommand(0x11);          //Exit sleep mode
	delay_ms(120);
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2015/08/08
*******************************************************************************/
void TFT_SleepIn (void)
{
    WriteSPICommand(0x10);          //Enter sleep mode
	delay_ms(120);
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2015/08/08
*******************************************************************************/
void Enable_Command2 (void)
{
	WriteSPICommand(0xf0); //Command Set control                                 
	WriteSPIData(0xc3);//Enable extension command 2 partI

	WriteSPICommand(0xf0); //Command Set control                                 
	WriteSPIData(0x96);//Enable extension command 2 partII
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2015/08/08
*******************************************************************************/
void Disable_Command2 (void)
{
	WriteSPICommand(0xf0); //Command Set control                                 
	WriteSPIData(0x3c);//Disable extension command 2 partI

	WriteSPICommand(0xf0); //Command Set control                                 
	WriteSPIData(0x69);//Disable extension command 2 partII
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2015/08/08
*******************************************************************************/
void SetGammaCorrection (void)
{
	WriteSPICommand(0xe0); //Gamma"+"                                             
	WriteSPIData(0xf0);
	WriteSPIData(0x04); 
	WriteSPIData(0x0d);
	WriteSPIData(0x14); 
	WriteSPIData(0x15);
	WriteSPIData(0x1f); 
	WriteSPIData(0x3c);
	WriteSPIData(0x55); 
	WriteSPIData(0x4a);
	WriteSPIData(0x3c); 
	WriteSPIData(0x17);
	WriteSPIData(0x16);
	WriteSPIData(0x1b); 
	WriteSPIData(0x1d); 
    
	WriteSPICommand(0xe1); //Gamma"-"                                             
	WriteSPIData(0xf0);
	WriteSPIData(0x04); 
	WriteSPIData(0x0d);
	WriteSPIData(0x0a); 
	WriteSPIData(0x0b);
	WriteSPIData(0x06); 
	WriteSPIData(0x38);
	WriteSPIData(0x43); 
	WriteSPIData(0x4a);
	WriteSPIData(0x0c); 
	WriteSPIData(0x17);
	WriteSPIData(0x16);
	WriteSPIData(0x1a); 
	WriteSPIData(0x1d); 
    
}

/*******************************************************************************
* @brief  Initialization of the TFT Lcd controller ILI9341
*					for use with RGB parrallel interface
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2015/08/08
*******************************************************************************/
void TFT_PanelInit(void)
{
    WRITE_TFT_BACKLIGHT_OUTPUT(GPIO_PIN_SET);
    WRITE_TFT_RST_OUTPUT (GPIO_PIN_RESET);
    delay_ms(100);

    WRITE_TFT_RST_OUTPUT (GPIO_PIN_SET);
    
    WRITE_CSX_OUTPUT(GPIO_PIN_SET);

	delay_ms(120);

	TFT_SleepOut();    

    SetDisplayFunctionCtrl();
//	SetMemoryDataAccessCtrl();

    
//    Enable_Command2();


//	SetDisplayInversionCtrl();

//    SetDisplayOutputCtrl();

//    SetPowerControl();
	    
//    SetGammaCorrection();

    
//    Disable_Command2 ();

    delay_ms(100);

    WRITE_TFT_BACKLIGHT_OUTPUT(GPIO_PIN_RESET);
}

/*******************************************************************************
* @brief  Write Command to IL9341 register through SPI 4 line interface
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2015/08/08
*******************************************************************************/
void WriteSPICommand(uint8_t Command)
{
    WRITE_DCX_OUTPUT(GPIO_PIN_RESET);
	WRITE_CSX_OUTPUT(GPIO_PIN_RESET);

    WRITE_COMMAND(Command);

	WRITE_CSX_OUTPUT(GPIO_PIN_SET);
}

/*******************************************************************************
* @brief  Write Data to ILI9341 registers through SPI 4 lines interface
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2015/08/08
*******************************************************************************/
void WriteSPIData(uint8_t Data)
{
	WRITE_DCX_OUTPUT(GPIO_PIN_SET);
	WRITE_CSX_OUTPUT(GPIO_PIN_RESET);

    WRITE_DATA(Data);
    
	WRITE_CSX_OUTPUT(GPIO_PIN_SET);
}


/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2015/08/08
*******************************************************************************/
void TFT_Display_ON (void)
{
	WriteSPICommand(0x29);//Display on
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2015/08/08
*******************************************************************************/
void TFT_Display_OFF (void)
{
	WriteSPICommand(0x28);              //Display off
}


/** Copyright(C) 2015 Stelpro Design, All Rights Reserved**/
