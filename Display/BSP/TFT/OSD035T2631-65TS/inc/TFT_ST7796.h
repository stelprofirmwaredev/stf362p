/********************************************************************************
* @file    TFT_ST7796.h
* @date    2015/11/16
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef _TFT_ST7796_H
#define _TFT_ST7796_H


/*******************************************************************************
* Includes
*******************************************************************************/
#ifdef __ICCARM__
    #include "stm32f4xx_hal.h"
#endif


/*******************************************************************************
* Public constants definitions
*******************************************************************************/
/**
  * @brief  LCD Registers
  */
//#define LCD_SLEEP_OUT            0x11   /* Sleep out register */
//#define LCD_GAMMA                0x26   /* Gamma register */
//#define LCD_DISPLAY_OFF          0x28   /* Display off register */
//#define LCD_DISPLAY_ON           0x29   /* Display on register */
//#define LCD_COLUMN_ADDR          0x2A   /* Colomn address register */
//#define LCD_PAGE_ADDR            0x2B   /* Page address register */
//#define LCD_GRAM                 0x2C   /* GRAM register */
//#define LCD_MAC                  0x36   /* Memory Access Control register*/
//#define LCD_PIXEL_FORMAT         0x3A   /* Pixel Format register */
//#define LCD_WDB                  0x51   /* Write Brightness Display register */
//#define LCD_WCD                  0x53   /* Write Control Display register*/
//#define LCD_RGB_INTERFACE        0xB0   /* RGB Interface Signal Control */
//#define LCD_FRC                  0xB1   /* Frame Rate Control register */
//#define LCD_BPC                  0xB5   /* Blanking Porch Control register*/
//#define LCD_DFC                  0xB6   /* Display Function Control register*/
//#define LCD_POWER1               0xC0   /* Power Control 1 register */
//#define LCD_POWER2               0xC1   /* Power Control 2 register */
//#define LCD_VCOM1                0xC5   /* VCOM Control 1 register */
//#define LCD_VCOM2                0xC7   /* VCOM Control 2 register */
//#define LCD_POWERA               0xCB   /* Power control A register */
//#define LCD_POWERB               0xCF   /* Power control B register */
//#define LCD_PGAMMA               0xE0   /* Positive Gamma Correction register*/
//#define LCD_NGAMMA               0xE1   /* Negative Gamma Correction register*/
//#define LCD_DTCA                 0xE8   /* Driver timing control A */
//#define LCD_DTCB                 0xEA   /* Driver timing control B */
//#define LCD_POWER_SEQ            0xED   /* Power on sequence register */
//#define LCD_3GAMMA_EN            0xF2   /* 3 Gamma enable register */
//#define LCD_INTERFACE            0xF6   /* Interface control register */
//#define LCD_PRC                  0xF7   /* Pump ratio control register */
//


#define _1MS_LOOP                   20000



#ifdef __ICCARM__
    #define WRITE_TFT_RST_OUTPUT(state)         HAL_GPIO_WritePin(TFT_RESET_GPIO_Port, TFT_RESET_Pin, state)
    #define WRITE_CSX_OUTPUT(state)             HAL_GPIO_WritePin(TFT_SPI_SLAVE_SELECT_GPIO_Port, TFT_SPI_SLAVE_SELECT_Pin, state)
    #define WRITE_DCX_OUTPUT(state)             HAL_GPIO_WritePin(TFT_DCX_GPIO_Port, TFT_DCX_Pin, state)
    #define WRITE_TFT_BACKLIGHT_OUTPUT(state)   HAL_GPIO_WritePin(TFT_BACKLIGHT_GPIO_Port, TFT_BACKLIGHT_Pin, state)
    #define TOGGLE_TFT_BACKLIGHT_OUTPUT()       HAL_GPIO_TogglePin(TFT_BACKLIGHT_GPIO_Port, TFT_BACKLIGHT_Pin)

    extern SPI_HandleTypeDef hTFT_SPI;

    #define WRITE_DATA(data)                    HAL_SPI_TransmitReceive(&hTFT_SPI,&data,&data,1,SPI_WRITE_DELAY);
    #define WRITE_COMMAND(command)              HAL_SPI_TransmitReceive(&hTFT_SPI,&command,&command,1,SPI_WRITE_DELAY);
    #ifdef DEVKIT_PLATFORM
        #define TFT_BACKLIGHT_ON                    WRITE_TFT_BACKLIGHT_OUTPUT(GPIO_PIN_RESET)
        #define TFT_BACKLIGHT_OFF                   WRITE_TFT_BACKLIGHT_OUTPUT(GPIO_PIN_SET)
    #else
        #define TFT_BACKLIGHT_ON                    WRITE_TFT_BACKLIGHT_OUTPUT(GPIO_PIN_SET)
        #define TFT_BACKLIGHT_OFF                   WRITE_TFT_BACKLIGHT_OUTPUT(GPIO_PIN_RESET)
    #endif /* DEVKIT_PLATFORM */
    #define TFT_TOGGLE_BACKLIGHT                TOGGLE_TFT_BACKLIGHT_OUTPUT()
#else
    #define WRITE_TFT_RST_OUTPUT(state)
    #define WRITE_CSX_OUTPUT(state)
    #define WRITE_DCX_OUTPUT(state)
    #define WRITE_TFT_BACKLIGHT_OUTPUT(state)

    extern SPI_HandleTypeDef hTFT_SPI;

    #define WRITE_DATA(data)
    #define WRITE_COMMAND(command)
    #define TFT_BACKLIGHT_ON
    #define TFT_BACKLIGHT_OFF
#endif


/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void LCD_PowerOn(SPI_HandleTypeDef * spiHandle);
void TFT_PanelInit(void);
void TFT_Display_ON (void);
void TFT_Display_OFF (void);
void TFT_SleepOut (void);
void TFT_SleepIn (void);




#endif /* _TFT_ILI9341_H */
/** Copyright(C) 2015 Stelpro Design, All Rights Reserved**/
