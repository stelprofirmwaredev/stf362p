/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    Display.c
* @date    2016/08/08
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include ".\HAL\inc\Display.h"

#include "stm32f4xx_hal.h"
#include ".\BSP\SDRAM\IS42S16100H\inc\IS42S16100H.h"
#include "fonts.h"
#include "images.h"
#include ".\Images\UI_Temp2b.c"
#include ".\Images\UI_Temp2.c"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#ifndef UI_ENABLE
__no_init uint8_t guiselect;
#endif

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
static DMA2D_HandleTypeDef Dma2dHandler;
static uint32_t ActiveLayer = 0;
static LCD_DrawPropTypeDef DrawProp[MAX_LAYER_NUMBER];
#ifdef DAC_BACKLIGHT
static uint16_t BacklightDACTarget;
#endif /* DAC_BACKLIGHT */
static uint8_t BacklightState = 1;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
SPI_HandleTypeDef hTFT_SPI;
LTDC_HandleTypeDef hTFT_LTDC;
LTDC_HandleTypeDef  LtdcHandler;
#ifdef DAC_BACKLIGHT
DAC_HandleTypeDef hdac;
#endif /* DAC_BACKLIGHT */
extern LTDC_HandleTypeDef hTFT_LTDC;

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
#ifdef DAC_BACKLIGHT
static void MX_DAC_Init(void);
#endif /* DAC_BACKLIGHT */
static void LTDC_Init(void);
static void SPI_Init(void);
static void GPIO_Init(void);
static void DrawSpecificFontChar(uint16_t Xpos, uint16_t Ypos, GUI_CONST_STORAGE GUI_CHARINFO *c, GUI_CONST_STORAGE uint16_t *height);
static void FillBuffer(uint32_t LayerIndex, void *pDst, uint32_t xSize, uint32_t ySize, uint32_t OffLine, uint32_t ColorIndex);
static void ConvertLineToARGB8888(void *pSrc, void *pDst, uint32_t xSize, uint32_t ColorMode);
static void BlendLineToARGB8888(void *pSrc, void *pDst, uint32_t xSize, uint32_t ColorMode, void *pBackImage);
static uint8_t ReplaceUnsupportedCharacters(uint8_t character);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
#ifdef DAC_BACKLIGHT
/* DAC init function */
static void MX_DAC_Init(void)
{

  DAC_ChannelConfTypeDef sConfig;
    __DAC_CLK_ENABLE();
    /**DAC Initialization
    */
  hdac.Instance = DAC;
  if (HAL_DAC_Init(&hdac) != HAL_OK)
  {
//    Error_Handler();
  }

    /**DAC channel OUT2 config
    */
  sConfig.DAC_Trigger = DAC_TRIGGER_NONE;
  sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
  if (HAL_DAC_ConfigChannel(&hdac, &sConfig, DAC_CHANNEL_2) != HAL_OK)
  {
//    Error_Handler();
  }

  HAL_DAC_Start(&hdac, DAC_CHANNEL_2);
}
#endif /* DAC_BACKLIGHT */

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/08
*******************************************************************************/
static void LTDC_Init(void)
{
#ifdef UI_ENABLE
#else
  LTDC_LayerCfgTypeDef pLayerCfg;
  LTDC_LayerCfgTypeDef pLayerCfg1;
#endif

  hTFT_LTDC.Instance = LTDC;
  hTFT_LTDC.Init.HSPolarity = LTDC_HSPOLARITY_AL;
  hTFT_LTDC.Init.VSPolarity = LTDC_VSPOLARITY_AL;
  hTFT_LTDC.Init.DEPolarity = LTDC_DEPOLARITY_AL;
  hTFT_LTDC.Init.PCPolarity = LTDC_PCPOLARITY_IIPC;

  hTFT_LTDC.Init.HorizontalSync = 49;
  hTFT_LTDC.Init.AccumulatedHBP = 51;
  hTFT_LTDC.Init.AccumulatedActiveW = 371;
  hTFT_LTDC.Init.TotalWidth = 409;

  hTFT_LTDC.Init.VerticalSync = 3;
  hTFT_LTDC.Init.AccumulatedVBP = 5;
  hTFT_LTDC.Init.AccumulatedActiveH = 485;
  hTFT_LTDC.Init.TotalHeigh = 487;

  //Set the RGB color the same as the background
  hTFT_LTDC.Init.Backcolor.Blue =  LCD_DEFAULT_BACKGROUND_COLOR&0x000000FF;
  hTFT_LTDC.Init.Backcolor.Green = (LCD_DEFAULT_BACKGROUND_COLOR&0x0000FF00)>>8;
  hTFT_LTDC.Init.Backcolor.Red = (LCD_DEFAULT_BACKGROUND_COLOR&0x00FF0000)>>16;
  HAL_LTDC_Init(&hTFT_LTDC);
//  HAL_LTDC_EnableDither(&hTFT_LTDC);

#ifdef UI_ENABLE
#else
  HAL_LTDC_EnableDither(&hTFT_LTDC);
  pLayerCfg.WindowX0 = 0;
  pLayerCfg.WindowX1 = 320;
  pLayerCfg.WindowY0 = 0;
  pLayerCfg.WindowY1 = 480;
  pLayerCfg.PixelFormat = LTDC_PIXEL_FORMAT_RGB888;//LTDC_PIXEL_FORMAT_ARGB8888;
  pLayerCfg.Alpha = 255;
  pLayerCfg.Alpha0 = 255;
  pLayerCfg.BlendingFactor1 = LTDC_BLENDING_FACTOR1_PAxCA;
  pLayerCfg.BlendingFactor2 = LTDC_BLENDING_FACTOR2_PAxCA;
  if (guiselect != 0)
  {
    pLayerCfg.FBStartAdress = (uint32_t)_acUI_Temp2;
    guiselect = 0;
  }
  else
  {
      pLayerCfg.FBStartAdress = (uint32_t)_acUI_Temp2;
      guiselect = 0xff;
  }

  pLayerCfg.ImageWidth = 320;
  pLayerCfg.ImageHeight = 480;
  pLayerCfg.Backcolor.Blue = 0;
  pLayerCfg.Backcolor.Green = 0;
  pLayerCfg.Backcolor.Red = 0;
  HAL_LTDC_ConfigLayer(&hTFT_LTDC, &pLayerCfg, 0)

  pLayerCfg1.WindowX0 = 0;
  pLayerCfg1.WindowX1 = 0;
  pLayerCfg1.WindowY0 = 0;
  pLayerCfg1.WindowY1 = 0;
  pLayerCfg1.PixelFormat = LTDC_PIXEL_FORMAT_ARGB8888;
  pLayerCfg1.Alpha = 0;
  pLayerCfg1.Alpha0 = 0;
  pLayerCfg1.BlendingFactor1 = LTDC_BLENDING_FACTOR1_PAxCA;
  pLayerCfg1.BlendingFactor2 = LTDC_BLENDING_FACTOR2_PAxCA;
  pLayerCfg1.FBStartAdress = 0;//(uint32_t)&backgroundRamImage;//(uint32_t)background.pixel_data;
  pLayerCfg1.ImageWidth = 0;
  pLayerCfg1.ImageHeight = 0;
  pLayerCfg1.Backcolor.Blue = 0;
  pLayerCfg1.Backcolor.Green = 0;
  pLayerCfg1.Backcolor.Red = 0;
  HAL_LTDC_ConfigLayer(&hTFT_LTDC, &pLayerCfg1, 1);
#endif      //UI_ENABLE
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/08
*******************************************************************************/
static void SPI_Init(void)
{

  hTFT_SPI.Instance = SPI2;
  hTFT_SPI.Init.Mode = SPI_MODE_MASTER;
  hTFT_SPI.Init.Direction = SPI_DIRECTION_2LINES;
  hTFT_SPI.Init.DataSize = SPI_DATASIZE_8BIT;
  hTFT_SPI.Init.CLKPolarity = SPI_POLARITY_LOW;
  hTFT_SPI.Init.CLKPhase = SPI_PHASE_1EDGE;
  hTFT_SPI.Init.NSS = SPI_NSS_SOFT;
  hTFT_SPI.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hTFT_SPI.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hTFT_SPI.Init.TIMode = SPI_TIMODE_DISABLE;
  hTFT_SPI.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hTFT_SPI.Init.CRCPolynomial = 10;
  HAL_SPI_Init(&hTFT_SPI);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/09
*******************************************************************************/
static void GPIO_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    /*  Write default output state  */
    HAL_GPIO_WritePin(TFT_SPI_SLAVE_SELECT_GPIO_Port,
                      TFT_SPI_SLAVE_SELECT_Pin,
                      GPIO_PIN_RESET);

    HAL_GPIO_WritePin(TFT_RESET_GPIO_Port,
                      TFT_RESET_Pin,
                      GPIO_PIN_RESET);

    HAL_GPIO_WritePin(TFT_BACKLIGHT_GPIO_Port,
                      TFT_BACKLIGHT_Pin,
                      GPIO_PIN_RESET);

    HAL_GPIO_WritePin(TFT_DCX_GPIO_Port,
                      TFT_DCX_Pin,
                      GPIO_PIN_SET);


    /*Configure GPIO pins */
    GPIO_InitStruct.Pin = TFT_SPI_SLAVE_SELECT_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(TFT_SPI_SLAVE_SELECT_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = TFT_RESET_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(TFT_RESET_GPIO_Port, &GPIO_InitStruct);

#ifdef DAC_BACKLIGHT
    GPIO_InitStruct.Pin = TFT_BACKLIGHT_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(TFT_BACKLIGHT_GPIO_Port, &GPIO_InitStruct);
#else
    GPIO_InitStruct.Pin = TFT_BACKLIGHT_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(TFT_BACKLIGHT_GPIO_Port, &GPIO_InitStruct);
#endif /* DAC_BACKLIGHT */

    GPIO_InitStruct.Pin = TFT_DCX_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(TFT_DCX_GPIO_Port, &GPIO_InitStruct);
}

/**
  * @brief  Draws a character on LCD.
  * @param  Xpos: the Line where to display the character shape
  * @param  Ypos: start column address
  * @param  c: pointer to the character data
  */
static void DrawSpecificFontChar(uint16_t Xpos, uint16_t Ypos, GUI_CONST_STORAGE GUI_CHARINFO *c, GUI_CONST_STORAGE uint16_t *height)
{
    uint32_t i = 0, j = 0, k = 0;
    uint8_t pchar, pchar1, pchar2;
    uint32_t color;

    for(i = 0; i < *height; i++)
    {
        for (j = 0; j < c->nbBytes; j++)
        {
            pchar = c->table[(i * c->nbBytes) + j];

                                        //Since we are using anti-aliasing (4 bpp), each pixel is contained in 1 nibble (4 bits)
            pchar1 = pchar >> 4;        //Extract the first pixel content
            pchar2 = pchar & 0x0F;      //Extract the second pixel content

            if (pchar1)
            {
                color = DrawProp[ActiveLayer].TextColor & 0x00FFFFFF;       //extract the RGB color
                color |= (pchar1 * 0x11) << 24;                             //anti-aliasing done by using alpha channel
                BSP_LCD_DrawPixel((Xpos + (j * 2) + k), Ypos, color);       //*2 because there are two pixels per byte
            }
            else
            {
            }

            if (pchar2)
            {
                color = DrawProp[ActiveLayer].TextColor & 0x00FFFFFF;       //extract the RGB color
                color |= (pchar2 * 0x11) << 24;                             //anti-aliasing done by using alpha channel
                BSP_LCD_DrawPixel((Xpos + (j * 2) + k + 1), Ypos, color);   //*2 because there are two pixels per byte, +1 because we are addressing the second pixel of the byte
            }
            else
            {
            }
        }
        Ypos++;
    }
}

/**
  * @brief  Fills buffer.
  * @param  LayerIndex: layer index
  * @param  pDst: output color
  * @param  xSize: buffer width
  * @param  ySize: buffer height
  * @param  OffLine: offset
  * @param  ColorIndex: color Index
  */
static void FillBuffer(uint32_t LayerIndex, void * pDst, uint32_t xSize, uint32_t ySize, uint32_t OffLine, uint32_t ColorIndex)
{

  /* Register to memory mode with ARGB8888 as color Mode */
  Dma2dHandler.Init.Mode         = DMA2D_R2M;
  Dma2dHandler.Init.ColorMode    = DMA2D_ARGB8888;
  Dma2dHandler.Init.OutputOffset = OffLine;

  Dma2dHandler.Instance = DMA2D;

  /* DMA2D Initialization */
  if(HAL_DMA2D_Init(&Dma2dHandler) == HAL_OK)
  {
    if(HAL_DMA2D_ConfigLayer(&Dma2dHandler, LayerIndex) == HAL_OK)
    {
      if (HAL_DMA2D_Start(&Dma2dHandler, ColorIndex, (uint32_t)pDst, xSize, ySize) == HAL_OK)
      {
        /* Polling For DMA transfer */
        HAL_DMA2D_PollForTransfer(&Dma2dHandler, 10);
      }
    }
  }
}

/**
  * @brief  Converts Line to ARGB8888 pixel format.
  * @param  pSrc: pointer to source buffer
  * @param  pDst: output color
  * @param  xSize: buffer width
  * @param  ColorMode: input color mode
  */
static void ConvertLineToARGB8888(void * pSrc, void * pDst, uint32_t xSize, uint32_t ColorMode)
{
  /* Configure the DMA2D Mode, Color Mode and output offset */
  Dma2dHandler.Init.Mode         = DMA2D_M2M_PFC;
  Dma2dHandler.Init.ColorMode    = DMA2D_ARGB8888;
  Dma2dHandler.Init.OutputOffset = 0;

  /* Foreground Configuration */
  Dma2dHandler.LayerCfg[1].AlphaMode = DMA2D_NO_MODIF_ALPHA;
  Dma2dHandler.LayerCfg[1].InputAlpha = 0;//0xFF;
  Dma2dHandler.LayerCfg[1].InputColorMode = ColorMode;
  Dma2dHandler.LayerCfg[1].InputOffset = 0;

  Dma2dHandler.Instance = DMA2D;

  /* DMA2D Initialization */
  if(HAL_DMA2D_Init(&Dma2dHandler) == HAL_OK)
  {
    if(HAL_DMA2D_ConfigLayer(&Dma2dHandler, 1) == HAL_OK)
    {
      if (HAL_DMA2D_Start(&Dma2dHandler, (uint32_t)pSrc, (uint32_t)pDst, xSize, 1) == HAL_OK)
      {
        /* Polling For DMA transfer */
        HAL_DMA2D_PollForTransfer(&Dma2dHandler, 10);
      }
    }
  }
}

/**
  * @brief  Blend a line in ARGB8888 pixel format.
  * @param  pSrc: pointer to source buffer
  * @param  pDst: pointer to destination buffer
  * @param  xSize: buffer width
  * @param  ColorMode: input color mode
  * @param  pBackImage: pointer to the back image buffer
  */
static void BlendLineToARGB8888(void *pSrc, void *pDst, uint32_t xSize, uint32_t ColorMode, void *pBackImage)
{
    /* Configure the DMA2D Mode, Color Mode and output offset */
  Dma2dHandler.Init.Mode         = DMA2D_M2M_BLEND;
  Dma2dHandler.Init.ColorMode    = DMA2D_ARGB8888;
  Dma2dHandler.Init.OutputOffset = 0;

  /* Foreground Configuration */
  Dma2dHandler.LayerCfg[0].AlphaMode = DMA2D_NO_MODIF_ALPHA;
  Dma2dHandler.LayerCfg[0].InputAlpha = 0;//0xFF;
  Dma2dHandler.LayerCfg[0].InputColorMode = ColorMode;
  Dma2dHandler.LayerCfg[0].InputOffset = 0;

  Dma2dHandler.LayerCfg[1].AlphaMode = DMA2D_NO_MODIF_ALPHA;
  Dma2dHandler.LayerCfg[1].InputAlpha = 0;//0xFF;
  Dma2dHandler.LayerCfg[1].InputColorMode = ColorMode;
  Dma2dHandler.LayerCfg[1].InputOffset = 0;

  Dma2dHandler.Instance = DMA2D;

  /* DMA2D Initialization */
  if(HAL_DMA2D_Init(&Dma2dHandler) == HAL_OK)
  {
    if(HAL_DMA2D_ConfigLayer(&Dma2dHandler, 1) == HAL_OK)
    {
      if (HAL_DMA2D_BlendingStart(&Dma2dHandler, (uint32_t)pSrc, (uint32_t)pBackImage, (uint32_t)pDst, xSize, 1) == HAL_OK)
      {
        /* Polling For DMA transfer */
        HAL_DMA2D_PollForTransfer(&Dma2dHandler, 10);
      }
    }
  }
}

/*******************************************************************************
* @brief  Function that replaces unsupported characters with a valid replacement
* @inputs Character from a string
* @retval Valid character
* @author Jean-Fran�ois Many
* @date   2018/10/26
*******************************************************************************/
static uint8_t ReplaceUnsupportedCharacters(uint8_t character)
{
    switch (character)
    {
        case '�':
            character = '�';
            break;

        case '�':
            character = 'a';
            break;

        case '�':
            character = '"';
            break;

        case '�':
            character = '"';
            break;

        case '�':
            character = '-';
            break;

        case '�':
            character = '\'';
            break;

        case '�':
            character = 'c';
            break;

        default:
            break;
    }
    return character;
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
#ifdef DAC_BACKLIGHT
/*******************************************************************************
* @brief  Set the backlight DAC target
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/07/19
*******************************************************************************/
void SetBacklightDACTarget (uint16_t target)
{
    BacklightDACTarget = target * 4095 / 100;
}

/*******************************************************************************
* @brief  Get the backlight DAC target
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/07/19
*******************************************************************************/
uint16_t GetBacklightDACTarget (void)
{
    return BacklightDACTarget;
}
#endif /* DAC_BACKLIGHT */

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/08
*******************************************************************************/
void DisplayInit(void)
{
    LTDC_Init();            //Init. LTDC peripheral of the STM32
    SPI_Init();             //Init. SPI peripheral of the STM32.
                            // SPI used to communicate with the TFT Controller/Driver
    GPIO_Init();

#ifdef DAC_BACKLIGHT
    MX_DAC_Init();
#endif /* DAC_BACKLIGHT */
    PanelInit();

    //DisplayON();
}

/*******************************************************************************
* @brief
* @inputs
* @retval None
* @author Jean-Fran�ois Simard
* @date   2016/10/24
*******************************************************************************/
void HAL_ClearScreen (void)
{
    BSP_LCD_Clear(LCD_DEFAULT_BACKGROUND_COLOR);
}

/**
  * @brief  Initializes the LCD.
  * @retval LCD state
  */
void BSP_LCD_Init(void)
{
    LtdcHandler = hTFT_LTDC;

    /* Initialize the SDRAM */
    BSP_SDRAM_Init();
}

/**
  * @brief  Gets the LCD X size.
  * @retval The used LCD X size
  */
uint32_t BSP_LCD_GetXSize(void)
{
  return 320;
}

/**
  * @brief  Gets the LCD Y size.
  * @retval The used LCD Y size
  */
uint32_t BSP_LCD_GetYSize(void)
{
  return 480;
}

/**
  * @brief  Initializes the LCD layers.
  * @param  LayerIndex: the layer foreground or background.
  * @param  FB_Address: the layer frame buffer.
  */
void BSP_LCD_LayerDefaultInit(void)
{
  LCD_LayerCfgTypeDef   Layercfg;

  /* Layer Init */
  Layercfg.WindowX0 = 0;
  Layercfg.WindowX1 = BSP_LCD_GetXSize();
  Layercfg.WindowY0 = 0;
  Layercfg.WindowY1 = BSP_LCD_GetYSize();
  Layercfg.PixelFormat = LTDC_PIXEL_FORMAT_ARGB8888;
  Layercfg.FBStartAdress = LCD_FRAME_BUFFER;
  Layercfg.Alpha = 255;
  Layercfg.Alpha0 = 0;
  Layercfg.Backcolor.Blue = 0;
  Layercfg.Backcolor.Green = 0;
  Layercfg.Backcolor.Red = 0;
  Layercfg.BlendingFactor1 = LTDC_BLENDING_FACTOR1_PAxCA;
  Layercfg.BlendingFactor2 = LTDC_BLENDING_FACTOR2_PAxCA;
  Layercfg.ImageWidth = BSP_LCD_GetXSize();
  Layercfg.ImageHeight = BSP_LCD_GetYSize();

  HAL_LTDC_ConfigLayer(&LtdcHandler, &Layercfg, LCD_BACKGROUND_LAYER);

  DrawProp[LCD_BACKGROUND_LAYER].BackColor = LCD_DEFAULT_BACKGROUND_COLOR;
  DrawProp[LCD_BACKGROUND_LAYER].TextColor = LCD_COLOR_WHITE;

  /* Dithering activation */
  HAL_LTDC_EnableDither(&LtdcHandler);
}

/**
  * @brief  Selects the LCD Layer.
  * @param  LayerIndex: the Layer foreground or background.
  */
void BSP_LCD_SelectLayer(uint32_t LayerIndex)
{
  ActiveLayer = LayerIndex;
}

/**
  * @brief  Configures the Transparency.
  * @param  LayerIndex: the Layer foreground or background.
  * @param  Transparency: the Transparency,
  *    This parameter must range from 0x00 to 0xFF.
  */
void BSP_LCD_SetTransparency(uint32_t LayerIndex, uint8_t Transparency)
{
  HAL_LTDC_SetAlpha(&LtdcHandler, Transparency, LayerIndex);
}

/**
  * @brief  Configures the transparency without reloading.
  * @param  LayerIndex: Layer foreground or background.
  * @param  Transparency: Transparency
  *           This parameter must be a number between Min_Data = 0x00 and Max_Data = 0xFF
  * @retval None
  */
void BSP_LCD_SetTransparency_NoReload(uint32_t LayerIndex, uint8_t Transparency)
{
  HAL_LTDC_SetAlpha_NoReload(&LtdcHandler, Transparency, LayerIndex);
}

/**
  * @brief  Sets a LCD layer frame buffer address.
  * @param  LayerIndex: specifies the Layer foreground or background
  * @param  Address: new LCD frame buffer value
  */
void BSP_LCD_SetLayerAddress(uint32_t LayerIndex, uint32_t Address)
{
  HAL_LTDC_SetAddress(&LtdcHandler, Address, LayerIndex);
}

/**
  * @brief  Sets an LCD layer frame buffer address without reloading.
  * @param  LayerIndex: Layer foreground or background
  * @param  Address: New LCD frame buffer value
  * @retval None
  */
void BSP_LCD_SetLayerAddress_NoReload(uint32_t LayerIndex, uint32_t Address)
{
  HAL_LTDC_SetAddress_NoReload(&LtdcHandler, Address, LayerIndex);
}

/**
  * @brief  Sets the Display window.
  * @param  LayerIndex: layer index
  * @param  Xpos: LCD X position
  * @param  Ypos: LCD Y position
  * @param  Width: LCD window width
  * @param  Height: LCD window height
  */
void BSP_LCD_SetLayerWindow(uint16_t LayerIndex, uint16_t Xpos, uint16_t Ypos, uint16_t Width, uint16_t Height)
{
  /* reconfigure the layer size */
  HAL_LTDC_SetWindowSize(&LtdcHandler, Width, Height, LayerIndex);

  /* reconfigure the layer position */
  HAL_LTDC_SetWindowPosition(&LtdcHandler, Xpos, Ypos, LayerIndex);
}

/**
  * @brief  Configures and sets the color Keying.
  * @param  LayerIndex: the Layer foreground or background
  * @param  RGBValue: the Color reference
  */
void BSP_LCD_SetColorKeying(uint32_t LayerIndex, uint32_t RGBValue)
{
  /* Configure and Enable the color Keying for LCD Layer */
  HAL_LTDC_ConfigColorKeying(&LtdcHandler, RGBValue, LayerIndex);
  HAL_LTDC_EnableColorKeying(&LtdcHandler, LayerIndex);
}

/**
  * @brief  Configures and sets the color keying without reloading.
  * @param  LayerIndex: Layer foreground or background
  * @param  RGBValue: Color reference
  * @retval None
  */
void BSP_LCD_SetColorKeying_NoReload(uint32_t LayerIndex, uint32_t RGBValue)
{
  /* Configure and Enable the color Keying for LCD Layer */
  HAL_LTDC_ConfigColorKeying_NoReload(&LtdcHandler, RGBValue, LayerIndex);
  HAL_LTDC_EnableColorKeying_NoReload(&LtdcHandler, LayerIndex);
}

/**
  * @brief  Disables the color Keying.
  * @param  LayerIndex: the Layer foreground or background
  */
void BSP_LCD_ResetColorKeying(uint32_t LayerIndex)
{
  /* Disable the color Keying for LCD Layer */
  HAL_LTDC_DisableColorKeying(&LtdcHandler, LayerIndex);
}

/**
  * @brief  Disables the color keying without reloading.
  * @param  LayerIndex: Layer foreground or background
  * @retval None
  */
void BSP_LCD_ResetColorKeying_NoReload(uint32_t LayerIndex)
{
  /* Disable the color Keying for LCD Layer */
  HAL_LTDC_DisableColorKeying_NoReload(&LtdcHandler, LayerIndex);
}

/**
  * @brief  Sets the Text color.
  * @param  Color: the Text color code ARGB(8-8-8-8)
  */
void BSP_LCD_SetTextColor(uint32_t Color)
{
  DrawProp[ActiveLayer].TextColor = Color;
}

/**
  * @brief  Reads Pixel.
  * @param  Xpos: the X position
  * @param  Ypos: the Y position
  * @retval RGB pixel color
  */
uint32_t BSP_LCD_ReadPixel(uint16_t Xpos, uint16_t Ypos)
{
  uint32_t ret = 0;

  if(LtdcHandler.LayerCfg[ActiveLayer].PixelFormat == LTDC_PIXEL_FORMAT_ARGB8888)
  {
    /* Read data value from SDRAM memory */
    ret = *(__IO uint32_t*) (LtdcHandler.LayerCfg[ActiveLayer].FBStartAdress + (2*(Ypos*BSP_LCD_GetXSize() + Xpos)));
  }
  else if(LtdcHandler.LayerCfg[ActiveLayer].PixelFormat == LTDC_PIXEL_FORMAT_RGB888)
  {
    /* Read data value from SDRAM memory */
    ret = (*(__IO uint32_t*) (LtdcHandler.LayerCfg[ActiveLayer].FBStartAdress + (2*(Ypos*BSP_LCD_GetXSize() + Xpos))) & 0x00FFFFFF);
  }
  else if((LtdcHandler.LayerCfg[ActiveLayer].PixelFormat == LTDC_PIXEL_FORMAT_RGB565) || \
          (LtdcHandler.LayerCfg[ActiveLayer].PixelFormat == LTDC_PIXEL_FORMAT_ARGB4444) || \
          (LtdcHandler.LayerCfg[ActiveLayer].PixelFormat == LTDC_PIXEL_FORMAT_AL88))
  {
    /* Read data value from SDRAM memory */
    ret = *(__IO uint16_t*) (LtdcHandler.LayerCfg[ActiveLayer].FBStartAdress + (2*(Ypos*BSP_LCD_GetXSize() + Xpos)));
  }
  else
  {
    /* Read data value from SDRAM memory */
    ret = *(__IO uint8_t*) (LtdcHandler.LayerCfg[ActiveLayer].FBStartAdress + (2*(Ypos*BSP_LCD_GetXSize() + Xpos)));
  }

  return ret;
}

/**
  * @brief  Clears the hole LCD.
  * @param  Color: the color of the background
  */
void BSP_LCD_Clear(uint32_t Color)
{
  /* Clear the LCD */
  FillBuffer(ActiveLayer, (uint32_t *)(LtdcHandler.LayerCfg[ActiveLayer].FBStartAdress), BSP_LCD_GetXSize(), BSP_LCD_GetYSize(), 0, Color);
}

/**
  * @brief  Clears a section of the selected line.
  * @param  Line: the line to be cleared
  * @param  Xstart: start clear position
  * @param  Xend: end clear position
  */
void BSP_LCD_ClearStringLineSection(uint16_t Xstart, uint16_t Ystart, uint16_t width, uint16_t height)
{
  uint32_t colorbackup = DrawProp[ActiveLayer].TextColor;
  DrawProp[ActiveLayer].TextColor = DrawProp[ActiveLayer].BackColor;

  /* Draw rectangle with background color */
  BSP_LCD_FillRect(Xstart, Ystart, width, height);

  DrawProp[ActiveLayer].TextColor = colorbackup;
  BSP_LCD_SetTextColor(DrawProp[ActiveLayer].TextColor);
}

/**
  * @brief  Displays one character.
  * @param  Xpos: start column address
  * @param  Ypos: the Line where to display the character shape
  * @param  Ascii: character ascii code
  */
void BSP_LCD_DisplaySpecificFontChar(uint16_t Xpos, uint16_t Ypos, uint8_t Ascii, GUI_CONST_STORAGE GUI_FONT_PROP* font, GUI_CONST_STORAGE uint16_t* height)
{
  DrawSpecificFontChar(Xpos, Ypos, &font->address[Ascii - font->first_char], height);
}

/**
  * @brief  Displays a maximum of 60 char on the LCD.
  * @param  X: pointer to x position (in pixel)
  * @param  Y: pointer to y position (in pixel)
  * @param  pText: pointer to string to display on LCD
  * @param  mode: The display mode
  * @param  font: Specific font to use (files generated by FontCvt by SEGGER
  *    This parameter can be one of the following values:
  *                @arg CENTER_MODE
  *                @arg RIGHT_MODE
  *                @arg LEFT_MODE
  */
void BSP_LCD_DisplaySpecificFontStringAt(uint16_t X, uint16_t Y, uint8_t *pText, Text_AlignModeTypdef mode, const GUI_FONT* font)
{
    static uint16_t refcolumn = 1;
    uint16_t i = 0;
    uint32_t size = 0;
    uint32_t xsize = 0;
    uint8_t  *ptr = pText;
    struct GUI_FONT_PROP const* dynamic_address;

    /* Get the text size */
    while (*ptr++) size ++ ;

    /* length of string */
    for (i = 0; i < size; i++)
    {
        dynamic_address = font->pFirst;
        while (pText[i] > dynamic_address->last_char)
        {
            dynamic_address = dynamic_address->pNext;
            if (dynamic_address == NULL)
            {
                return;
            }
        }
        xsize += dynamic_address->address[pText[i] - dynamic_address->first_char].width;
    }

    switch (mode)
    {
        case CENTER_MODE:
            refcolumn = X+ (BSP_LCD_GetXSize() - xsize) / 2;
            break;
        case LEFT_MODE:
            refcolumn = X;
            break;
        case RIGHT_MODE:
            refcolumn = BSP_LCD_GetXSize() - xsize - X;
            break;
        case APPEND_MODE:
            ;   //Continue where we left
            break;
        default:
            refcolumn = X;
            break;
    }

    /* Send the string character by character on LCD */
    while (*pText != 0)
    {
        *pText = ReplaceUnsupportedCharacters(*pText);
        dynamic_address = font->pFirst;
        while (*pText > dynamic_address->last_char)
        {
            dynamic_address = dynamic_address->pNext;
            if (dynamic_address == NULL)
            {
                return;
            }
        }
        /* Display one character on LCD */
        BSP_LCD_DisplaySpecificFontChar(refcolumn, Y, *pText, dynamic_address, &font->height);
        /* Change column position for the next character */
        refcolumn += dynamic_address->address[*pText - dynamic_address->first_char].width;
        /* Point on the next character */
        pText++;
    }
}

/**
  * @brief  Displays an horizontal line.
  * @param  Xpos: the X position
  * @param  Ypos: the Y position
  * @param  Length: line length
  */
void BSP_LCD_DrawHLine(uint16_t Xpos, uint16_t Ypos, uint16_t Length)
{
  uint32_t xaddress = 0;

  /* Get the line address */
  xaddress = (LtdcHandler.LayerCfg[ActiveLayer].FBStartAdress) + 4*(BSP_LCD_GetXSize()*Ypos + Xpos);

  /* Write line */
  FillBuffer(ActiveLayer, (uint32_t *)xaddress, Length, 1, 0, DrawProp[ActiveLayer].TextColor);
}

/**
  * @brief  Displays a vertical line.
  * @param  Xpos: the X position
  * @param  Ypos: the Y position
  * @param  Length: line length
  */
void BSP_LCD_DrawVLine(uint16_t Xpos, uint16_t Ypos, uint16_t Length)
{
  uint32_t xaddress = 0;

  /* Get the line address */
  xaddress = (LtdcHandler.LayerCfg[ActiveLayer].FBStartAdress) + 4*(BSP_LCD_GetXSize()*Ypos + Xpos);

  /* Write line */
  FillBuffer(ActiveLayer, (uint32_t *)xaddress, 1, Length, (BSP_LCD_GetXSize() - 1), DrawProp[ActiveLayer].TextColor);
}

/**
  * @brief  Displays a rectangle.
  * @param  Xpos: the X position
  * @param  Ypos: the Y position
  * @param  Height: display rectangle height
  * @param  Width: display rectangle width
  */
void BSP_LCD_DrawRect(uint16_t Xpos, uint16_t Ypos, uint16_t Width, uint16_t Height)
{
  /* Draw horizontal lines */
  BSP_LCD_DrawHLine(Xpos, Ypos, Width);
  BSP_LCD_DrawHLine(Xpos, (Ypos+ Height), Width);

  /* Draw vertical lines */
  BSP_LCD_DrawVLine(Xpos, Ypos, Height);
  BSP_LCD_DrawVLine((Xpos + Width), Ypos, Height);
}

/**
  * @brief  Displays a bitmap picture loaded in the internal Flash (32 bpp).
  * @param  X: the bmp x position in the LCD
  * @param  Y: the bmp Y position in the LCD
  * @param  pBmp: Bmp picture address in the internal Flash
  */
void BSP_LCD_DrawBitmap(uint32_t X, uint32_t Y, uint8_t *pBmp)
{
  uint32_t index = 0, width = 0, height = 0, bitpixel = 0;
  uint32_t address;
  uint32_t inputcolormode = 0;

  /* Get bitmap data address offset */
  index = 0;

  /* Read bitmap width */
  width = *(uint32_t *) (pBmp);

  /* Read bitmap height */
  height = *(uint32_t *) (pBmp + 4);

  /* Read bit/pixel */
  bitpixel = (*(uint32_t *) (pBmp + 8)) * 8;

  /* Set Address */
  address = LtdcHandler.LayerCfg[ActiveLayer].FBStartAdress + (((BSP_LCD_GetXSize()*Y) + X)*(4));

  /* Get the Layer pixel format */
  if ((bitpixel/8) == 4)
  {
    inputcolormode = CM_ARGB8888;
  }
  else if ((bitpixel/8) == 2)
  {
    inputcolormode = CM_RGB565;
  }
  else
  {
    inputcolormode = CM_RGB888;
  }

  /* bypass the bitmap header */
  pBmp += 12;

  /* Convert picture to ARGB8888 pixel format */
  for(index=0; index < height; index++)
  {
      /* Pixel format conversion */
      ConvertLineToARGB8888((uint32_t *)pBmp, (uint32_t *)address, width, inputcolormode);

      /* Increment the source and destination buffers */
      address+=  ((BSP_LCD_GetXSize() - width + width)*4);
      pBmp += width*(bitpixel/8);
  }
}

/**
  * @brief  Blend two images together
  * @param  X: the bmp x position in the LCD
  * @param  Y: the bmp Y position in the LCD
  * @param  pBmp: top picture address
  * @param  xBack: the bmp x position in the LCD of the back image
  * @param  yBack: the bmp Y position in the LCD of the back image
  * @param  pBackImage: back picture address
  */
void BSP_LCD_BlendImages(uint32_t X, uint32_t Y, uint8_t *pBmp, uint32_t xBack, uint32_t yBack, uint8_t *pBackImage)
{
  uint32_t index = 0;
  uint32_t width = 0, height = 0, bitpixel = 0, widthBack = 0, bitpixelBack = 0;
  uint32_t address;
  uint32_t inputcolormode = 0;

  /* Get bitmap data address offset */
  index = 0;

  /* Read bitmap width */
  width = *(uint32_t *) (pBmp);
  widthBack = *(uint32_t *) (pBackImage);

  /* Read bitmap height */
  height = *(uint32_t *) (pBmp + 4);

  /* Read bit/pixel */
  bitpixel = (*(uint32_t *) (pBmp + 8)) * 8;
  bitpixelBack = (*(uint32_t *) (pBackImage + 8)) * 8;

  /* Set Address */
  address = LtdcHandler.LayerCfg[ActiveLayer].FBStartAdress + (((BSP_LCD_GetXSize()*Y) + X)*(4));

  /* Get the Layer pixel format */
  if ((bitpixel/8) == 4)
  {
    inputcolormode = CM_ARGB8888;
  }
  else if ((bitpixel/8) == 2)
  {
    inputcolormode = CM_RGB565;
  }
  else
  {
    inputcolormode = CM_RGB888;
  }

  /* bypass the bitmap header */
  pBmp += 12;
  pBackImage += 12;

  /* offset the back image with the top image position */
  pBackImage += (X - xBack) * (bitpixel/8) + (Y - yBack) * widthBack*(bitpixelBack/8);

  /* Convert picture to ARGB8888 pixel format */
  for(index=0; index < height; index++)
  {
      /* Pixel format conversion */
      BlendLineToARGB8888((uint32_t *)pBmp, (uint32_t *)address, width, inputcolormode, (uint32_t *)pBackImage);

      /* Increment the source and destination buffers */
      address +=  BSP_LCD_GetXSize()*(bitpixel/8);
      pBmp += width*(bitpixel/8);
      pBackImage += widthBack*(bitpixelBack/8);
  }
}

/**
  * @brief  Blend two images together
  * @param  X: the X position offset of the front image in the back image
  * @param  Y: the Y position offset of the front image in the back image
  * @param  pBmp: top picture address
  * @param  pBackImage: back picture address
  */
void BSP_LCD_BlendImagesInBuffer(uint32_t X, uint32_t Y, uint8_t *pBmp, uint8_t *pBackImage)
{
  uint32_t index = 0;
  uint32_t width = 0, height = 0, bitpixel = 0, widthBack = 0, bitpixelBack = 0;
  uint32_t address;
  uint32_t inputcolormode = 0;

  /* Get bitmap data address offset */
  index = 0;

  /* Read bitmap width */
  width = *(uint32_t *) (pBmp);
  widthBack = *(uint32_t *) (pBackImage);

  /* Read bitmap height */
  height = *(uint32_t *) (pBmp + 4);

  /* Read bit/pixel */
  bitpixel = (*(uint32_t *) (pBmp + 8)) * 8;
  bitpixelBack = (*(uint32_t *) (pBackImage + 8)) * 8;

  /* bypass the bitmap header */
  pBmp += 12;
  pBackImage += 12;
  /* Set Address */
  address = (uint32_t)pBackImage + (((widthBack*Y) + X)*(4));

  /* Get the Layer pixel format */
  if ((bitpixel/8) == 4)
  {
    inputcolormode = CM_ARGB8888;
  }
  else if ((bitpixel/8) == 2)
  {
    inputcolormode = CM_RGB565;
  }
  else
  {
    inputcolormode = CM_RGB888;
  }

  /* offset the back image with the top image position */
  pBackImage += X * (bitpixel/8) + Y * (widthBack)*(bitpixelBack/8);

  /* Convert picture to ARGB8888 pixel format */
  for(index=0; index < height; index++)
  {
      /* Pixel format conversion */
      BlendLineToARGB8888((uint32_t *)pBmp, (uint32_t *)address, width, inputcolormode, (uint32_t *)pBackImage);

      /* Increment the source and destination buffers */
      address +=  widthBack*(bitpixel/8);
      pBmp += width*(bitpixel/8);
      pBackImage += widthBack*(bitpixelBack/8);
  }
}

/**
  * @brief  Hide an image section
  * @param  X: the bmp x position in the LCD
  * @param  Y: the bmp Y position in the LCD
  * @param  pBmp: top picture address
  * @param  xBack: the bmp x position in the LCD of the back image
  * @param  yBack: the bmp Y position in the LCD of the back image
  * @param  pBackImage: back picture address
  */
void BSP_LCD_HideImage(uint32_t X, uint32_t Y, uint8_t *pBmp, uint32_t xBack, uint32_t yBack, uint8_t *pBackImage)
{
  uint32_t index = 0;
  uint32_t width = 0, height = 0, bitpixel = 0, widthBack = 0, bitpixelBack = 0;
  uint32_t address;
  uint32_t inputcolormode = 0;

  /* Get bitmap data address offset */
  index = 0;

  /* Read bitmap width */
  width = *(uint32_t *) (pBmp);
  widthBack = *(uint32_t *) (pBackImage);

  /* Read bitmap height */
  height = *(uint32_t *) (pBmp + 4);

  /* Read bit/pixel */
  bitpixel = (*(uint32_t *) (pBmp + 8)) * 8;
  bitpixelBack = (*(uint32_t *) (pBackImage + 8)) * 8;

  /* Set Address */
  address = LtdcHandler.LayerCfg[ActiveLayer].FBStartAdress + (((BSP_LCD_GetXSize()*Y) + X)*(4));

  /* Get the Layer pixel format */
  if ((bitpixel/8) == 4)
  {
    inputcolormode = CM_ARGB8888;
  }
  else if ((bitpixel/8) == 2)
  {
    inputcolormode = CM_RGB565;
  }
  else
  {
    inputcolormode = CM_RGB888;
  }

  /* bypass the bitmap header */
  pBmp += 12;
  pBackImage += 12;

  /* offset the back image with the top image position */
  pBackImage += (X - xBack) * (bitpixel/8) + (Y - yBack) * widthBack*(bitpixelBack/8);

  /* Convert picture to ARGB8888 pixel format */
  for(index=0; index < height; index++)
  {
      /* Pixel format conversion */
      BlendLineToARGB8888((uint32_t *)pBackImage, (uint32_t *)address, width, inputcolormode, (uint32_t *)pBmp);

      /* Increment the source and destination buffers */
      address +=  BSP_LCD_GetXSize()*(bitpixel/8);
      pBmp += width*(bitpixel/8);
      pBackImage += widthBack*(bitpixelBack/8);
  }
}


/**
  * @brief  Displays a full rectangle.
  * @param  Xpos: the X position
  * @param  Ypos: the Y position
  * @param  Height: rectangle height
  * @param  Width: rectangle width
  */
void BSP_LCD_FillRect(uint16_t Xpos, uint16_t Ypos, uint16_t Width, uint16_t Height)
{
  uint32_t xaddress = 0;

  /* Set the text color */
  BSP_LCD_SetTextColor(DrawProp[ActiveLayer].TextColor);

  /* Get the rectangle start address */
  xaddress = (LtdcHandler.LayerCfg[ActiveLayer].FBStartAdress) + 4*(BSP_LCD_GetXSize()*Ypos + Xpos);

  /* Fill the rectangle */
  FillBuffer(ActiveLayer, (uint32_t *)xaddress, Width, Height, (BSP_LCD_GetXSize() - Width), DrawProp[ActiveLayer].TextColor);
}

/**
  * @brief  Displays a full rectangle.
  * @param  Xpos: the X position
  * @param  Ypos: the Y position
  * @param  Height: rectangle height
  * @param  Width: rectangle width
  */
void BSP_LCD_FillRectWithColor(uint16_t Xpos, uint16_t Ypos, uint16_t Width, uint16_t Height, uint32_t color)
{
  uint32_t xaddress = 0;

  /* Set the text color */
  BSP_LCD_SetTextColor(color);

  /* Get the rectangle start address */
  xaddress = (LtdcHandler.LayerCfg[ActiveLayer].FBStartAdress) + 4*(BSP_LCD_GetXSize()*Ypos + Xpos);

  /* Fill the rectangle */
  FillBuffer(ActiveLayer, (uint32_t *)xaddress, Width, Height, (BSP_LCD_GetXSize() - Width), DrawProp[ActiveLayer].TextColor);
}
/**
  * @brief  Writes Pixel.
  * @param  Xpos: the X position
  * @param  Ypos: the Y position
  * @param  RGB_Code: the pixel color in ARGB mode (8-8-8-8)
  */
void BSP_LCD_DrawPixel(uint16_t Xpos, uint16_t Ypos, uint32_t RGB_Code)
{
  /* Write data value to all SDRAM memory */
  *(__IO uint32_t*) (LtdcHandler.LayerCfg[ActiveLayer].FBStartAdress + (4*(Ypos*BSP_LCD_GetXSize() + Xpos))) = RGB_Code;
}

/*******************************************************************************
    DSP_Backlight_GetBacklightState

    Return the current backlight state
*******************************************************************************/
uint8_t DSP_Backlight_GetBacklightState(void)
{
    return BacklightState;
}

/*******************************************************************************
    DSP_Backlight_SetBacklightState

    Set the backlight state to TRUE(On) or FALSE(Off)
*******************************************************************************/
void DSP_Backlight_SetBacklightState(uint8_t state)
{
    BacklightState = state;
}

/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
