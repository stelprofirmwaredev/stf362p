/*******************************************************************************
* @file    Display.h
* @date    2016/08/08
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef _Display_H
#define _Display_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os.h"

#ifdef ORIENT_DISPLAY
    #include ".\BSP\TFT\AFV320480A01 (OrientDisplay)\inc\TFT_ILI9488.h"
#else
    #include ".\BSP\TFT\OSD035T2631-65TS\inc\TFT_ST7796.h"
#endif

#include "fonts.h"
#include "ili9341.h"


/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define PanelInit()                     TFT_PanelInit()
#ifdef DAC_BACKLIGHT
#define DisplayON()                     TFT_Display_ON();osDelay(100);SetBacklightDACTarget(100);DSP_Backlight_SetBacklightState(TRUE);
#define DisplayOFF()                    TFT_Display_OFF();SetBacklightDACTarget(0);DSP_Backlight_SetBacklightState(FALSE);
#define BacklightOFF()                  SetBacklightDACTarget(0);DSP_Backlight_SetBacklightState(FALSE);
#define BacklightON()                   SetBacklightDACTarget(100);DSP_Backlight_SetBacklightState(TRUE);
#else
#define DisplayON()                     TFT_Display_ON();osDelay(100);TFT_BACKLIGHT_ON;DSP_Backlight_SetBacklightState(TRUE);
#define DisplayOFF()                    TFT_Display_OFF();TFT_BACKLIGHT_OFF;DSP_Backlight_SetBacklightState(FALSE);
#define BacklightOFF()                  TFT_BACKLIGHT_OFF;DSP_Backlight_SetBacklightState(FALSE);
#define BacklightON()                   TFT_BACKLIGHT_ON;DSP_Backlight_SetBacklightState(TRUE);
#endif /* DAC_BACKLIGHT */

#define DisplayLowPower()               TFT_SleepIn();TFT_BACKLIGHT_OFF;DSP_Backlight_SetBacklightState(FALSE);
#define DisplayExitLowPower()           TFT_SleepOut();TFT_BACKLIGHT_ON;DSP_Backlight_SetBacklightState(TRUE);

#define LCD_LayerCfgTypeDef    LTDC_LayerCfgTypeDef

/**
  * @brief  LCD status structure definition
  */
#define MAX_LAYER_NUMBER       2
#define LCD_FRAME_BUFFER       ((uint32_t)0xC0000000)

/**
  * @brief  LCD color
  */
//Inverted colors
#define LCD_COLOR_BLUE          0xFFFF0000
#define LCD_COLOR_GREEN         0xFF00FF00
#define LCD_COLOR_RED           0xFF0000FF
#define LCD_COLOR_CYAN          0xFFFFFF00
#define LCD_COLOR_MAGENTA       0xFFFF00FF
#define LCD_COLOR_YELLOW        0xFF00FFFF
#define LCD_COLOR_LIGHTBLUE     0xFFFF8080
#define LCD_COLOR_LIGHTGREEN    0xFF80FF80
#define LCD_COLOR_LIGHTRED      0xFF8080FF
#define LCD_COLOR_LIGHTCYAN     0xFFFFFF80
#define LCD_COLOR_LIGHTMAGENTA  0xFFFF80FF
#define LCD_COLOR_LIGHTYELLOW   0xFF80FFFF
#define LCD_COLOR_DARKBLUE      0xFF800000
#define LCD_COLOR_DARKGREEN     0xFF008000
#define LCD_COLOR_DARKRED       0xFF000080
#define LCD_COLOR_DARKCYAN      0xFF808000
#define LCD_COLOR_DARKMAGENTA   0xFF800080
#define LCD_COLOR_DARKYELLOW    0xFF008080
#define LCD_COLOR_WHITE         0xFFFFFFFF
#define LCD_COLOR_LIGHTGRAY     0xFFD3D3D3
#define LCD_COLOR_GRAY          0xFF808080
#define LCD_COLOR_DARKGRAY      0xFF404040
#define LCD_COLOR_BLACK         0xFF000000
#define LCD_COLOR_BROWN         0xFF2A2AA5
#define LCD_COLOR_ORANGE        0xFF00A5FF
#define LCD_COLOR_TRANSPARENT   0xFF000000
#define LCD_COLOR_CHARCOAL      0xFF414042
#define LCD_COLOR_SETPOINT_BOX  0xFF58585A

#define LCD_TUX_GRADIENT_1      0xFF00FF65
#define LCD_TUX_GRADIENT_2      0xFF00FF88
#define LCD_TUX_GRADIENT_3      0xFF00FFAC
#define LCD_TUX_GRADIENT_4      0xFF00FFD0
#define LCD_TUX_GRADIENT_5      0xFF00FFF4
#define LCD_TUX_GRADIENT_6      0xFF00E5FF
#define LCD_TUX_GRADIENT_7      0xFF00C1FF
#define LCD_TUX_GRADIENT_8      0xFF009DFF
#define LCD_TUX_GRADIENT_9      0xFF0079FF
#define LCD_TUX_GRADIENT_10     0xFF0055FF
#define LCD_TUX_GRADIENT_11     0xFF0032FF

//The display background color is (and must be) managed here, to insure uniformity
//  among all the display.
//  + to minimize EMI radiation on the RAM data bus, it is important that the
//  alpha channel is set to 0x00 (full transparent).  This strategy limits the transition
//  on the RAM data bus while fetching the displayed image, where the plain background
//  counts as the majority of the bus transition
#define LCD_DEFAULT_BACKGROUND_COLOR    (LCD_COLOR_BLACK&(~LCD_COLOR_TRANSPARENT))
#if LCD_DEFAULT_BACKGROUND_COLOR != (LCD_COLOR_BLACK&(~LCD_COLOR_TRANSPARENT))
#warning "Are you sure you want to modify the background color?? Check comments above!!"
#endif
/**
  * @brief  LCD Layer
  */
#define LCD_BACKGROUND_LAYER     0x0000
#define LCD_FOREGROUND_LAYER     0x0001

/**
  * @brief LCD Pixel format
  */
#define LCD_PIXEL_FORMAT_ARGB8888         LTDC_PIXEL_FORMAT_ARGB8888
#define LCD_PIXEL_FORMAT_RGB888           LTDC_PIXEL_FORMAT_RGB888
#define LCD_PIXEL_FORMAT_RGB565           LTDC_PIXEL_FORMAT_RGB565
#define LCD_PIXEL_FORMAT_ARGB1555         LTDC_PIXEL_FORMAT_ARGB1555
#define LCD_PIXEL_FORMAT_ARGB4444         LTDC_PIXEL_FORMAT_ARGB4444
#define LCD_PIXEL_FORMAT_L8               LTDC_PIXEL_FORMAT_L8
#define LCD_PIXEL_FORMAT_AL44             LTDC_PIXEL_FORMAT_AL44
#define LCD_PIXEL_FORMAT_AL88             LTDC_PIXEL_FORMAT_AL88


/*******************************************************************************
* Public structures definitions
*******************************************************************************/
/**
  * @brief  Line mode structures definition
  */
typedef enum
{
  CENTER_MODE             = 0x01,    /* center mode */
  RIGHT_MODE              = 0x02,    /* right mode  */
  LEFT_MODE               = 0x03,    /* left mode   */
  APPEND_MODE             = 0x04,    /* append mode */
}Text_AlignModeTypdef;

typedef struct
{
  uint32_t  TextColor;
  uint32_t  BackColor;
  sFONT     *pFont;
}LCD_DrawPropTypeDef;


/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void DisplayInit(void);
void HAL_ClearScreen (void);
void BSP_LCD_SelectLayer(uint32_t LayerIndex);
void BSP_LCD_Init(void);
uint32_t BSP_LCD_GetXSize(void);
uint32_t BSP_LCD_GetYSize(void);
void BSP_LCD_LayerDefaultInit(void);
void BSP_LCD_SetTextColor(uint32_t Color);
uint32_t BSP_LCD_ReadPixel(uint16_t Xpos, uint16_t Ypos);
void BSP_LCD_DrawPixel(uint16_t Xpos, uint16_t Ypos, uint32_t pixel);
void BSP_LCD_Clear(uint32_t Color);
void BSP_LCD_DisplaySpecificFontStringAt(uint16_t X, uint16_t Y, uint8_t *pText, Text_AlignModeTypdef mode, const GUI_FONT* font);
void BSP_LCD_DisplaySpecificFontChar(uint16_t Xpos, uint16_t Ypos, uint8_t Ascii, GUI_CONST_STORAGE GUI_FONT_PROP* font, GUI_CONST_STORAGE uint16_t* height);
void BSP_LCD_DrawHLine(uint16_t Xpos, uint16_t Ypos, uint16_t Length);
void BSP_LCD_DrawVLine(uint16_t Xpos, uint16_t Ypos, uint16_t Length);
void BSP_LCD_DrawRect(uint16_t Xpos, uint16_t Ypos, uint16_t Width, uint16_t Height);
void BSP_LCD_DrawBitmap(uint32_t X, uint32_t Y, uint8_t *pBmp);
void BSP_LCD_BlendImages(uint32_t X, uint32_t Y, uint8_t *pBmp, uint32_t xBack, uint32_t yBack, uint8_t *pBackImage);
void BSP_LCD_BlendImagesInBuffer(uint32_t X, uint32_t Y, uint8_t *pBmp, uint8_t *pBackImage);
void BSP_LCD_HideImage(uint32_t X, uint32_t Y, uint8_t *pBmp, uint32_t xBack, uint32_t yBack, uint8_t *pBackImage);
void BSP_LCD_FillRect(uint16_t Xpos, uint16_t Ypos, uint16_t Width, uint16_t Height);
void BSP_LCD_FillRectWithColor(uint16_t Xpos, uint16_t Ypos, uint16_t Width, uint16_t Height, uint32_t color);
void BSP_LCD_ClearStringLineSection(uint16_t Xstart, uint16_t Ystart, uint16_t width, uint16_t height);
#ifdef DAC_BACKLIGHT
void SetBacklightDACTarget (uint16_t target);
uint16_t GetBacklightDACTarget (void);
#endif /* DAC_BACKLIGHT */
uint8_t DSP_Backlight_GetBacklightState(void);
void DSP_Backlight_SetBacklightState(uint8_t state);

#endif /* _Display_H */


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
