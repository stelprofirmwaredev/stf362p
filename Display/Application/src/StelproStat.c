/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    StelproStat.c
* @date    2016/08/10
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include ".\inc\StelproStat.h"
#include "typedef.h"
#include "MenuHandler.h"
#include "KeyboardHandler.h"
#include "APP_SetpointManager.h"
#include "APP_Display.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/


/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  Master Init function
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/08/04
*******************************************************************************/
void StelproStatInit(void)
{
    DPH_InitDisplay();
    HAL_KeysetInit();
    STPM_Init();
    MNH_Init();
    KBH_Init();
}

/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
