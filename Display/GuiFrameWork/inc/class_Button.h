/*******************************************************************************
* @file    class_Button.h
* @date    2017/08/09
* @authors J-F. Simard
* @brief 
*******************************************************************************/

#ifndef class_Button_H
#define class_Button_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>
#include ".\GuiFrameWork\inc\class_DisplayArea.h"



/*******************************************************************************
* Public constants definitions
*******************************************************************************/



/*******************************************************************************
* Public structures definitions
*******************************************************************************/
typedef displayPosition_t buttonPosition_t;
typedef displaySize_t buttonSize_t;


typedef enum
{
    BUTTON_RELEASED = 0,
    BUTTON_PRESSED,
    BUTTON_HOLD,
} buttonState_t;

typedef void (*buttonCallBack_t) (buttonState_t);

typedef struct
{
    cDisplayArea_t * display;
    uint8_t isClickable;
    uint8_t isActivated;
    buttonPosition_t position;
    buttonSize_t size;
    buttonState_t state;
    buttonCallBack_t buttonCallBack;
} cButton_t;

typedef cButton_t * cButtonHandle;
/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void DestroyButton (cButtonHandle * button);
cButtonHandle CreateNewButton (coordinate_t startX, coordinate_t startY, 
                                      coordinate_t width, coordinate_t height,
                                      buttonCallBack_t buttonCallBack);

cDisplayHandle GetButtonDisplayHandle (cButtonHandle button);
void SetButtonPosition (cButtonHandle button, coordinate_t startX, coordinate_t startY);
void SetButtonSize (cButtonHandle button, coordinate_t width, coordinate_t height);
void ParseButton (coordinate_t xTouch, coordinate_t yTouch);
void GUI_ButtonInit(void);

#endif /* class_Button_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
