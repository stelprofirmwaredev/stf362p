/*******************************************************************************
* @file    class_DisplayArea.h
* @date    2017/08/09
* @authors J-F. Simard
* @brief 
*******************************************************************************/

#ifndef class_DisplayArea_H
#define class_DisplayArea_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>
#include "fonts.h"



/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define DISPLAY_WIDTH                        320
#define DISPLAY_HEIGHT                       480


#define DISPLAY_DEFAULT_TOP_MARGINS             0        //in pixels
#define DISPLAY_DEFAULT_BOTTOM_MARGINS          0        //in pixels
#define DISPLAY_DEFAULT_LEFT_MARGINS            0        //in pixels
#define DISPLAY_DEFAULT_RIGHT_MARGINS           0        //in pixels


/*******************************************************************************
* Public structures definitions
*******************************************************************************/
typedef uint16_t coordinate_t ;

typedef struct
{
    coordinate_t top_margin;
    coordinate_t bottom_margin;
    coordinate_t left_margin;
    coordinate_t right_margin;
} displayMargins_t;

typedef struct
{
    coordinate_t x_coord;
    coordinate_t y_coord;
} displayPosition_t;

typedef struct
{
    coordinate_t width;
    coordinate_t height;
} displaySize_t;

typedef enum
{
    VERTICAL_ALIGN_TOP = 0,
    VERTICAL_ALIGN_CENTER,
    VERTICAL_ALIGN_BOTTOM,
} verticalAlignement_t;

typedef enum
{
    HORIZONTAL_ALIGN_LEFT = 0,
    HORIZONTAL_ALIGN_CENTER,
    HORIZONTAL_ALIGN_RIGHT,
    HORIZONTAL_APPEND,
} horizontalAlignement_t;

typedef struct
{
    verticalAlignement_t vertical;
    horizontalAlignement_t horizontal;
} displayAlignment_t;

typedef struct
{
    uint8_t isConfigured;
    uint8_t displayImage;
    uint32_t imageLocation;
    uint32_t imageSize;
    displayMargins_t margins;
    displayAlignment_t align;
} imageDescr_t;

typedef struct
{
    uint8_t isConfigured;
    uint8_t displayText;
    char str[50];
    uint32_t textColor;
    GUI_FONT const * font;
    displayMargins_t margins;
    displayAlignment_t align;
} textDescr_t;

typedef struct _displayArea_t
{
    struct _displayArea_t * parent;
    struct _displayArea_t * child;
    struct _displayArea_t * sibling;
    
    displayPosition_t startPosition;
    displaySize_t size;
    
    textDescr_t text;
    imageDescr_t image;
} cDisplayArea_t;

typedef cDisplayArea_t * cDisplayHandle;

/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
cDisplayHandle CreateNewDisplayArea (coordinate_t startX, coordinate_t startY, 
                                      coordinate_t width, coordinate_t height);
void DestroyDisplayArea (cDisplayHandle * display);

void SetDisplayPosition (cDisplayHandle displayArea, coordinate_t startX, coordinate_t startY);
void SetDisplaySize (cDisplayHandle displayArea, coordinate_t width, coordinate_t height);
void ResetDisplayMargins (cDisplayHandle displayArea);
void ResetDisplayAlignement (cDisplayHandle displayArea);

void ClearImage (cDisplayHandle displayArea);
void SetImage (cDisplayHandle displayArea, uint32_t imageLocation, uint32_t imageSize);
void SetImageMargins (cDisplayHandle displayArea, displayMargins_t margins);

void ClearText (cDisplayHandle displayArea);
void SetText (cDisplayHandle displayArea, char * str, GUI_FONT const* font);
void SetTextMargins (cDisplayHandle displayArea, displayMargins_t margins);
void SetTextColor (cDisplayHandle displayArea, uint32_t textColor);
void SetTextAlignement (cDisplayHandle displayArea, displayAlignment_t align); 

void ClearDisplayArea (cDisplayHandle displayArea);
void DrawArea (cDisplayHandle displayArea);
void ClearDisplayCache (void);

#endif /* class_DisplayArea_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
