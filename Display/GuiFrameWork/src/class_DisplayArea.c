/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    class_DisplayArea.c
* @date    2017/08/09
* @authors J-F. Simard
* @brief   
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <string.h>

#include ".\GuiFrameWork\inc\class_DisplayArea.h"
#include ".\HAL\inc\Display.h"
#include ".\HAL\inc\HAL_Flash.h"

#include "cmsis_os.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define DISPLAYCACHE_MAP_SIZE       10

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
extern __no_init uint8_t LcdTempBuffer;
uint8_t * displayCache = &LcdTempBuffer;
uint32_t displayCacheSize = (320*480*4);

typedef struct
{
    uint32_t imageId;
    uint32_t imageSize;
    uint8_t * imageLocation;
} cacheMapping_t;

cacheMapping_t displayCacheMap[DISPLAYCACHE_MAP_SIZE];

extern uint32_t RAM_OFFSET;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static coordinate_t getAbsoluteX_Reference (cDisplayHandle displayArea);
static coordinate_t getAbsoluteY_Reference (cDisplayHandle displayArea);


/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
static coordinate_t getAbsoluteX_Reference (cDisplayHandle displayArea)
{
    coordinate_t x_ref = displayArea->startPosition.x_coord;
    
    while (NULL != displayArea->parent)
    {
        x_ref += displayArea->parent->startPosition.x_coord;
        displayArea = displayArea->parent;
    }
    
    return x_ref;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
static coordinate_t getAbsoluteY_Reference (cDisplayHandle displayArea)
{
    coordinate_t y_ref = displayArea->startPosition.y_coord;
    
    while (NULL != displayArea->parent)
    {
        y_ref += displayArea->parent->startPosition.y_coord;
        displayArea = displayArea->parent;
    }
    
    return y_ref;
}


/**
  * @brief  Displays a maximum of 60 char on the LCD.
  *    This parameter can be one of the following values:
  *                @arg CENTER_MODE
  *                @arg RIGHT_MODE
  *                @arg LEFT_MODE
  */
void DisplaySpecificFontStringAt(cDisplayHandle displayArea)
{
    static uint16_t refcolumn = 1;
    uint16_t i = 0;
    uint32_t size = 0;
    uint32_t xsize = 0;
    uint8_t  *ptr;
    struct GUI_FONT_PROP const* dynamic_address;
    coordinate_t absoluteX_ref;
    coordinate_t absoluteY_ref;
    coordinate_t absoluteX_end;
//    coordinate_t absoluteY_end;

    coordinate_t fontHeight;


    uint16_t Y = displayArea->text.margins.top_margin;
    
    const GUI_FONT* font = displayArea->text.font;
    
    
    size = strlen(displayArea->text.str);

    /* length of string */
    for (i = 0; i < size; i++)
    {
        dynamic_address = font->pFirst;
        while (displayArea->text.str[i] > dynamic_address->last_char)
        {
            dynamic_address = dynamic_address->pNext;
            if (dynamic_address == NULL)
            {
                return;
            }
        }
        xsize += dynamic_address->address[displayArea->text.str[i] - dynamic_address->first_char].width;
    }
    
    if (xsize > displayArea->size.width)
    {
        xsize = displayArea->size.width;
    }

    switch (displayArea->text.align.horizontal)
    {
        case HORIZONTAL_ALIGN_CENTER:
            refcolumn = (displayArea->size.width - xsize) / 2;
            break;
        case HORIZONTAL_ALIGN_LEFT:
            refcolumn = displayArea->text.margins.left_margin;
            break;
        case HORIZONTAL_ALIGN_RIGHT:
            refcolumn = displayArea->size.width - xsize - displayArea->text.margins.right_margin;
            break;
        case HORIZONTAL_APPEND:
            ;   //Continue where we left
            break;
        default:
            refcolumn = displayArea->text.margins.left_margin;;
            break;
    }

    absoluteX_ref = getAbsoluteX_Reference (displayArea) + refcolumn;
    absoluteX_end = getAbsoluteX_Reference (displayArea) + displayArea->size.width;
    

    fontHeight = font->height;
    if (fontHeight > displayArea->size.height)
    {
        fontHeight = displayArea->size.height;
    }

    switch (displayArea->text.align.vertical)
    {
    case VERTICAL_ALIGN_TOP:
        break;
    case VERTICAL_ALIGN_CENTER:
        Y += (displayArea->size.height - fontHeight)/2;
        break;
    case VERTICAL_ALIGN_BOTTOM:
        Y =  displayArea->text.margins.bottom_margin + fontHeight;
        break;
    default:
        break;
    }
    
    absoluteY_ref = getAbsoluteY_Reference (displayArea) + Y;

    ptr = (uint8_t*)displayArea->text.str;
    /* Send the string character by character on LCD */
    
    BSP_LCD_SetTextColor (displayArea->text.textColor);
    while (*ptr != 0)
    {
        dynamic_address = font->pFirst;
        while (*ptr > dynamic_address->last_char)
        {
            dynamic_address = dynamic_address->pNext;
            if (dynamic_address == NULL)
            {
                return;
            }
        }
        /* Display one character on LCD */
        BSP_LCD_DisplaySpecificFontChar(absoluteX_ref, absoluteY_ref, *ptr, dynamic_address, &fontHeight);
        /* Change column position for the next character */
        absoluteX_ref += dynamic_address->address[*ptr - dynamic_address->first_char].width;
        if (absoluteX_ref > absoluteX_end)
        {
            return;
        }
        /* Point on the next character */
        ptr++;
    }
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
uint8_t checkCacheForImage (imageDescr_t * image)
{
    for (uint8_t i = 0; i < DISPLAYCACHE_MAP_SIZE; i++)
    {
        if (displayCacheMap[i].imageId == image->imageLocation)
        {
            if (displayCacheMap[i].imageSize == image->imageSize)
            {
                return i;
            }
        }
    }
    return -1;
}


/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
uint8_t getCacheNextFreeBlock (imageDescr_t * image)
{
    uint32_t cacheUsedSpace = 0;
    int8_t i;
    
    for (i = DISPLAYCACHE_MAP_SIZE-1; i >= 0; i--)
    {
        if ((uint32_t)-1 != displayCacheMap[i].imageId)
        {
            cacheUsedSpace = (displayCacheMap[i].imageLocation - displayCache) 
                       + displayCacheMap[i].imageSize;
            if ((displayCacheSize - cacheUsedSpace) >= image->imageSize)
            {
                break;
            }
        }
    }
    
    if (i < (DISPLAYCACHE_MAP_SIZE-1))
    {
        i += 1;
    }
    displayCacheMap[i].imageId = image->imageLocation;
    displayCacheMap[i].imageSize = image->imageSize;
    displayCacheMap[i].imageLocation = displayCache + cacheUsedSpace;
    
    return i;
}


/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void drawImage (cDisplayHandle displayArea)
{
    if (NULL != displayArea)
    {
        uint8_t cacheIdx = checkCacheForImage(&displayArea->image);
        if ((uint8_t)-1 == cacheIdx)
        {
            cacheIdx = getCacheNextFreeBlock (&displayArea->image);
            HAL_ReadFlashData (displayArea->image.imageLocation + RAM_OFFSET, 
                               displayArea->image.imageSize, 
                               displayCacheMap[cacheIdx].imageLocation);
        }        
        
        
        BSP_LCD_DrawBitmap (displayArea->startPosition.x_coord + displayArea->image.margins.left_margin,
                            displayArea->startPosition.y_coord + displayArea->image.margins.top_margin,
                            displayCacheMap[cacheIdx].imageLocation);
    }
    
}



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
cDisplayHandle CreateNewDisplayArea (coordinate_t startX, coordinate_t startY, 
                                      coordinate_t width, coordinate_t height)
{
    cDisplayHandle newDisplayArea;
    
    newDisplayArea = pvPortMalloc(sizeof(cDisplayArea_t));
    
    if (NULL != newDisplayArea)
    {
        newDisplayArea->parent = NULL;
        newDisplayArea->sibling = NULL;
        newDisplayArea->child = NULL;
        
        ResetDisplayMargins(newDisplayArea);
        ResetDisplayAlignement(newDisplayArea);
        ClearImage(newDisplayArea);
        ClearText(newDisplayArea);
        
        SetDisplayPosition (newDisplayArea, startX, startY);
        SetDisplaySize (newDisplayArea, width, height);
    }
    return newDisplayArea;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void DestroyDisplayArea (cDisplayHandle * displayArea)
{
    vPortFree(*displayArea);
    *displayArea = NULL;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void ResetDisplayMargins (cDisplayHandle displayArea)
{
    if (NULL != displayArea)
    {
        displayArea->text.margins.bottom_margin = DISPLAY_DEFAULT_BOTTOM_MARGINS;
        displayArea->text.margins.left_margin = DISPLAY_DEFAULT_LEFT_MARGINS;
        displayArea->text.margins.right_margin = DISPLAY_DEFAULT_RIGHT_MARGINS;
        displayArea->text.margins.top_margin = DISPLAY_DEFAULT_TOP_MARGINS;
        
        displayArea->image.margins.bottom_margin = DISPLAY_DEFAULT_BOTTOM_MARGINS;
        displayArea->image.margins.left_margin = DISPLAY_DEFAULT_LEFT_MARGINS;
        displayArea->image.margins.right_margin = DISPLAY_DEFAULT_RIGHT_MARGINS;
        displayArea->image.margins.top_margin = DISPLAY_DEFAULT_TOP_MARGINS;
    }
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void ResetDisplayAlignement (cDisplayHandle displayArea)
{
    if (NULL != displayArea)
    {
        displayArea->text.align.horizontal = HORIZONTAL_ALIGN_LEFT;
        displayArea->text.align.vertical = VERTICAL_ALIGN_TOP;
        displayArea->image.align.horizontal = HORIZONTAL_ALIGN_LEFT;
        displayArea->image.align.vertical = VERTICAL_ALIGN_TOP;
    }
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void ClearImage (cDisplayHandle displayArea)
{
    if (NULL != displayArea)
    {
        displayArea->image.isConfigured = 0;
        displayArea->image.displayImage = 0;
        displayArea->image.imageLocation = -1;
        displayArea->image.imageSize = -1;
    }
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void SetImage (cDisplayHandle displayArea, uint32_t imageLocation, uint32_t imageSize)
{
    if (NULL != displayArea)
    {
        displayArea->image.isConfigured = 1;
        displayArea->image.displayImage = 1;
        displayArea->image.imageLocation = imageLocation;
        displayArea->image.imageSize = imageSize;
    }
}


/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void ClearText (cDisplayHandle displayArea)
{
    if (NULL != displayArea)
    {
        displayArea->text.isConfigured = 0;
        displayArea->text.displayText = 0;
        displayArea->text.font = NULL;
        memcpy(displayArea->text.str, 0, 50);
        displayArea->text.textColor = LCD_COLOR_WHITE;
    }
}


/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void SetText (cDisplayHandle displayArea, char * str, GUI_FONT const * font)
{
    if (NULL != displayArea)
    {
        displayArea->text.isConfigured = 1;
        displayArea->text.displayText = 1;
        displayArea->text.font = font;
        strcpy(displayArea->text.str, str);
        displayArea->text.textColor = LCD_COLOR_WHITE;
    }
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void ClearDisplayArea (cDisplayHandle displayArea)
{
    ResetDisplayMargins(displayArea);
    ResetDisplayAlignement(displayArea);
    ClearImage(displayArea);
    ClearText(displayArea);
}


/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void SetDisplayPosition (cDisplayHandle displayArea, coordinate_t startX, coordinate_t startY)
{
    if (NULL != displayArea)
    {
        coordinate_t parentStartX;
        coordinate_t parentStartY;
        coordinate_t parentEndX;
        coordinate_t parentEndY;
        
        if (NULL != displayArea->parent)
        {
            parentStartX = displayArea->parent->startPosition.x_coord;
            parentStartY = displayArea->parent->startPosition.y_coord;
            parentEndX = displayArea->parent->startPosition.x_coord + 
                                displayArea->parent->size.width;
            parentEndY = displayArea->parent->startPosition.y_coord + 
                                displayArea->parent->size.height;
        }
        else
        {
            parentStartX = 0;
            parentStartY = 0;
            parentEndX = DISPLAY_WIDTH;
            parentEndY = DISPLAY_HEIGHT;
        }
        
        if (startX < parentStartX)
        {
            startX = parentStartX;
        }
        else if (startX > parentEndX)
        {
            startX = parentEndX;
        }
        else
        {
            ;
        }

        if (startY < parentStartY)
        {
            startY = parentStartY;
        }
        else if (startY > parentEndY)
        {
            startY = parentEndY;
        }
        else
        {
            ;
        }
        
        displayArea->startPosition.x_coord = startX;
        displayArea->startPosition.y_coord = startY;
    }
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void SetDisplaySize (cDisplayHandle displayArea, coordinate_t width, coordinate_t height)
{
    if (NULL != displayArea)
    {
        coordinate_t parentWidth;
        coordinate_t parentHeight;
        
        if (NULL != displayArea->parent)
        {
            parentWidth = displayArea->parent->size.width;
            parentHeight = displayArea->parent->size.height;
        }
        else
        {
            parentWidth = DISPLAY_WIDTH;
            parentHeight = DISPLAY_HEIGHT;
        }
        
        if ((displayArea->startPosition.x_coord+width) > parentWidth)
        {
            width = parentWidth - displayArea->startPosition.x_coord;
        }

        if ((displayArea->startPosition.y_coord+height) > parentHeight)
        {
            height = parentHeight;
        }
        
        displayArea->size.width = width;
        displayArea->size.height = height;        
    }
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void DrawArea (cDisplayHandle displayArea)
{
    if (NULL != displayArea)
    {
        BSP_LCD_FillRectWithColor (displayArea->startPosition.x_coord,
                          displayArea->startPosition.y_coord,
                          displayArea->size.width,
                          displayArea->size.height,
                          0);
        if (0 != displayArea->image.isConfigured)
        {
            drawImage(displayArea);
        }
        if (0 != displayArea->text.isConfigured)
        {
            DisplaySpecificFontStringAt(displayArea);
        }
    }
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void SetImageMargins (cDisplayHandle displayArea, displayMargins_t margins)
{
    displayArea->image.margins.bottom_margin = margins.bottom_margin;
    displayArea->image.margins.left_margin = margins.left_margin;
    displayArea->image.margins.right_margin = margins.right_margin;
    displayArea->image.margins.top_margin = margins.top_margin;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void SetTextMargins (cDisplayHandle displayArea, displayMargins_t margins)
{
    displayArea->text.margins.bottom_margin = margins.bottom_margin;
    displayArea->text.margins.left_margin = margins.left_margin;
    displayArea->text.margins.right_margin = margins.right_margin;
    displayArea->text.margins.top_margin = margins.top_margin;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void SetTextColor (cDisplayHandle displayArea, uint32_t textColor)
{
    displayArea->text.textColor = textColor;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void SetTextAlignement (cDisplayHandle displayArea, displayAlignment_t align)
{
    displayArea->text.align = align;
}


/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void ClearDisplayCache (void)
{
    for (uint8_t i = 0; i < DISPLAYCACHE_MAP_SIZE; i++)
    {
        displayCacheMap[i].imageId = -1;
        displayCacheMap[i].imageLocation = NULL;
        displayCacheMap[i].imageSize = 0;
    }
}

/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
