/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    class_Button.c
* @date    2017/08/09
* @authors J-F. Simard
* @brief   
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <string.h>

#include ".\GuiFrameWork\inc\class_Button.h"
#include ".\GuiFrameWork\inc\class_DisplayArea.h"

#include "cmsis_os.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
typedef struct _buttonRegistration_t
{
    struct _buttonRegistration_t * previousButton;
    struct _buttonRegistration_t * nextButton;
    cButtonHandle buttonHandle;
} buttonRegistration_t;

buttonRegistration_t *registeredButton_Head;
buttonRegistration_t *registeredButton_Tail;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
//static coordinate_t getAbsoluteX_Reference (cButtonHandle button);
//static coordinate_t getAbsoluteY_Reference (cButtonHandle button);


/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void activateNewRegisteredButton(void)
{
    buttonRegistration_t * registeredButton;
    
    registeredButton = registeredButton_Head;
    while (NULL != registeredButton)
    {
        registeredButton->buttonHandle->isActivated = 1;
        registeredButton = registeredButton->nextButton;
    }    
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void unregisterButton (cButtonHandle button)
{
    buttonRegistration_t * registeredButton;
    
    registeredButton = registeredButton_Head;
    while (NULL != registeredButton)
    {
        if (registeredButton->buttonHandle == button)
        {
            if (NULL != registeredButton->previousButton)  //is registered button != head
            {
                registeredButton->previousButton->nextButton = registeredButton->nextButton;
            }
            else
            {
                registeredButton_Head = registeredButton->nextButton;      //replace head
                registeredButton_Head->previousButton = NULL;
            }
            vPortFree(registeredButton);
            break;
        }
        else
        {
            registeredButton = registeredButton->nextButton;
        }
    }
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void registerButton (cButtonHandle button)
{
    buttonRegistration_t * registration;
    registration = pvPortMalloc(sizeof(buttonRegistration_t));
    if (NULL != registration)
    {
        registration->buttonHandle = button;
        registration->nextButton = NULL;
        registration->previousButton = NULL;
        
        if (NULL != registeredButton_Head)
        {
            registration->previousButton = registeredButton_Tail;
            registeredButton_Tail->nextButton = registration;
            registeredButton_Tail = registeredButton_Tail->nextButton;
        }
        else
        {
            registeredButton_Head = registration;
            registeredButton_Tail = registeredButton_Head;
        }
    }
}

///*******************************************************************************
//* @brief  
//* @inputs None
//* @retval None
//* @author 
//* @date   
//*******************************************************************************/
//static coordinate_t getAbsoluteX_Reference (cButtonHandle button)
//{
//    coordinate_t x_ref = button->position.x_coord;
//    
//    return x_ref;
//}
//
///*******************************************************************************
//* @brief  
//* @inputs None
//* @retval None
//* @author 
//* @date   
//*******************************************************************************/
//static coordinate_t getAbsoluteY_Reference (cButtonHandle button)
//{
//    coordinate_t y_ref = button->position.y_coord;
//    
//    return y_ref;
//}


/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void DestroyButton (cButtonHandle * button)
{
    if (NULL != button)
    {
        unregisterButton(*button);
        DestroyDisplayArea (&(*button)->display);
        vPortFree(*button);
        *button = NULL;
    }
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
cButtonHandle CreateNewButton (coordinate_t startX, coordinate_t startY, 
                                      coordinate_t width, coordinate_t height,
                                      buttonCallBack_t buttonCallBack)
{
    cButtonHandle newButton;
    
    newButton = pvPortMalloc(sizeof(cButton_t));
    if (NULL != newButton)
    {   
        newButton->display = CreateNewDisplayArea(startX, startY, width, height);
        SetButtonPosition(newButton, startX, startY);
        SetButtonSize(newButton, width, height);
        newButton->state = BUTTON_RELEASED;
        newButton->buttonCallBack = buttonCallBack;
        newButton->isClickable = 1;
        newButton->isActivated = 0;
        registerButton(newButton);
    }   
    return newButton;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
cDisplayHandle GetButtonDisplayHandle (cButtonHandle button)
{
    if (NULL != button)
    {
        return button->display;
    }
    return NULL;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void SetButtonPosition (cButtonHandle button, coordinate_t startX, coordinate_t startY)
{
    if (NULL != button)
    {
        coordinate_t parentStartX;
        coordinate_t parentStartY;
        coordinate_t parentEndX;
        coordinate_t parentEndY;
        
        parentStartX = 0;
        parentStartY = 0;
        parentEndX = DISPLAY_WIDTH;
        parentEndY = DISPLAY_HEIGHT;
        
        if (startX < parentStartX)
        {
            startX = parentStartX;
        }
        else if (startX > parentEndX)
        {
            startX = parentEndX;
        }
        else
        {
            ;
        }

        if (startY < parentStartY)
        {
            startY = parentStartY;
        }
        else if (startY > parentEndY)
        {
            startY = parentEndY;
        }
        else
        {
            ;
        }
        
        button->position.x_coord = startX;
        button->position.y_coord = startY;
    }
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void SetButtonSize (cButtonHandle button, coordinate_t width, coordinate_t height)
{
    if (NULL != button)
    {
        coordinate_t parentWidth;
        coordinate_t parentHeight;
        
        parentWidth = DISPLAY_WIDTH;
        parentHeight = DISPLAY_HEIGHT;
        
        if ((button->position.x_coord+width) > parentWidth)
        {
            width = parentWidth - button->position.x_coord;
        }

        if ((button->position.y_coord+height) > parentHeight)
        {
            height = parentHeight;
        }
        
        button->size.width = width;
        button->size.height = height;        
    }
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
uint8_t isButtonTouched (buttonRegistration_t * button, coordinate_t xTouch, coordinate_t yTouch)
{
    if (xTouch >= button->buttonHandle->position.x_coord)
    {
        if (xTouch <= (button->buttonHandle->position.x_coord+
                       button->buttonHandle->size.width))
        {
            if (yTouch >= button->buttonHandle->position.y_coord)
            {
                if (yTouch <= (button->buttonHandle->position.y_coord+
                               button->buttonHandle->size.height))
                {
                    return 1;
                }
            }
        }
    }
    return 0;
}


/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
uint8_t isButtonAvailable (buttonRegistration_t * button)
{
    if (0 != button->buttonHandle->isActivated)
    {
        if (0 != button->buttonHandle->isClickable)
        {
            return 1;
        }            
    }
    return 0;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void RunButtonAction (buttonRegistration_t * button)
{
    if (NULL != button->buttonHandle->buttonCallBack)
    {
        button->buttonHandle->buttonCallBack(button->buttonHandle->state);
    }
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void ParseButton (coordinate_t xTouch, coordinate_t yTouch)
{
    static uint8_t pressedCnt = 0;
    buttonRegistration_t * registeredButton = registeredButton_Head;
    
    if (((coordinate_t)-1 != xTouch) && ((coordinate_t)-1 != yTouch))
    {
        while (NULL != registeredButton)
        {
            if(0 != isButtonTouched(registeredButton, xTouch,yTouch))
            {
                if (0 != isButtonAvailable(registeredButton))
                {
                    switch (registeredButton->buttonHandle->state)
                    {
                    case BUTTON_RELEASED:
                        registeredButton->buttonHandle->state = BUTTON_PRESSED;
                        RunButtonAction(registeredButton);
                        break;
                    case BUTTON_PRESSED:
                        if (pressedCnt++ > 66)
                        {
                            registeredButton->buttonHandle->state = BUTTON_HOLD;
                            RunButtonAction(registeredButton);
                        }
                        break;
                        
                    case BUTTON_HOLD:
                        RunButtonAction(registeredButton);
                        break;
                        
                    default:
                        break;
                    }
                }
                break;
            }
            registeredButton = registeredButton->nextButton;
        }
    }
    else
    {
        while (NULL != registeredButton)
        {
            registeredButton->buttonHandle->state = BUTTON_RELEASED;
            registeredButton = registeredButton->nextButton;
            pressedCnt = 0;        
        }
        activateNewRegisteredButton();
    }
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void GUI_ButtonInit(void)
{
    registeredButton_Head = NULL;
    registeredButton_Tail = NULL;    
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
