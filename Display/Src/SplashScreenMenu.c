/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       SplashScreenMenu.c

    \brief      Source code file for the SplashScreenMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the SplashScreenMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "APP_Display.h"
#include "MenuHandler.h"
#include "MenuHandlerIf.h"
#include "MenuTable.h"
#include "SplashScreenMenu.h"
#include ".\HAL\inc\Display.h"
#include ".\DB\inc\THREAD_DB.h"
#include "SwVersion.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/


/*******************************************************************************
    External functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
void MNU_SplashScreen_Terminate(void);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
    MNU_SplashScreen_Entry
*******************************************************************************/
void MNU_SplashScreen_Entry (void)
{
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_SplashScreen_Terminate, (dbErr_t*)DBERR_OK, SEQUENCE_TIMEOUT);

    DPH_DisplaySplashScreen();
    DisplayON();
}

/*******************************************************************************
    MNU_SplashScreen_Core
*******************************************************************************/
void MNU_SplashScreen_Core (void)
{
}


/*******************************************************************************
    MNU_SplashScreen_Terminate
*******************************************************************************/
void MNU_SplashScreen_Terminate (void)
{
    uint8_t onboardingDone;

    BacklightON();
    MNH_ClearTimeoutCallback();
    DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);
//    MNH_SetActiveMenu(MENU_LCDTEST);
    if (onboardingDone != 1)
    {
        MNH_SetActiveMenu(MENU_LANGUAGE);
    }
    else
    {
        MNH_SetActiveMenu(MENU_HOME);
    }
}

/*******************************************************************************
    MNU_SplashScreen_Exit
*******************************************************************************/
void MNU_SplashScreen_Exit (void)
{
}

