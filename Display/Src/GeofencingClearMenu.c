/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       GeofencingClearMenu.c

    \brief      Source code file for the GeofencingClearMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the GeofencingClearMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "GeofencingClearMenu.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\WiFi\inc\THREAD_WiFi.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   GeofencingClearEditState = EDIT_IDLE;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/

/*******************************************************************************
    Private functions definitions
*******************************************************************************/

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_GeofencingClear_Entry
*******************************************************************************/
void MNU_GeofencingClear_Entry(void)
{
    GeofencingClearEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show Geofencing clear options
    //
    DPH_DisplayGeofencingClear();

    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_GeofencingClear_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_GeofencingClear_Core

    Allow erasing the mobile devices list
*******************************************************************************/
void MNU_GeofencingClear_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_GeofencingClear_Commit, (void*)KEY_BACK);
        KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)MNU_GeofencingClear_Commit, (void*)KEY_OPTIONS_3);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_GeofencingClear_Exit
*******************************************************************************/
void MNU_GeofencingClear_Exit(void)
{
    GeofencingClearEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_GeofencingClear_Commit
*******************************************************************************/
void MNU_GeofencingClear_Commit(KEYB_PARAM keyId)
{
    uint8_t user;
    char mobileId[13];
    uint8_t mobileState;

    if (GeofencingClearEditState == EDIT_IN_PROGRESS)
	{
        KBH_ClearAllKeyProperties();
        switch(keyId)
        {
            case KEY_BACK:
                MNH_SetActiveMenu(MENU_GEOFENCING);
                break;

            case KEY_OPTIONS_3:
                {
                    for (user = 0; user < USER_INSTANCES; user++)
                    {
                        mobileId[0] = NULL;
                        mobileState = 1;
                        DBTHRD_SetData(DBTYPE_GEOFENCING_MOBILE_ID,(void*)&mobileId, user, DBCHANGE_LOCAL);
                        DBTHRD_SetData(DBTYPE_GEOFENCING_MOBILE_STATE,(void*)&mobileState, user, DBCHANGE_LOCAL);
                    }
                    WIFITHRD_SendPropToWiFi(WIFITYPE_GEOFENCINGMOBILEID, 0, DBCHANGE_LOCAL);
                    WIFITHRD_SendPropToWiFi(WIFITYPE_GEOFENCINGMOBILESTATE, 0, DBCHANGE_LOCAL);
                    MNH_SetActiveMenu(MENU_GEOFENCING);
                }
                break;
            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
		GeofencingClearEditState = EDIT_IDLE;
	}
}
