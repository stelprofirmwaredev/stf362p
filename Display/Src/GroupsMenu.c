/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2016, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       GroupsMenu.c

    \brief      Source code file for the GroupsMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the GroupsMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include <string.h>
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "GroupsMenu.h"
#include ".\DB\inc\THREAD_DB.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/


/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   GroupsEditState = EDIT_IDLE;
static uint8_t          ActiveGroupNumber;
static uint8_t          SelectedGroupNumber;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/
extern int8_t my_string_compare(const char* a, const char* b);

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/

/*******************************************************************************
    Private functions definitions
*******************************************************************************/

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_Groups_Entry
*******************************************************************************/
void MNU_Groups_Entry(void)
{
    GroupsEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);
    ActiveGroupNumber = 0;

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show Groups List
    //
    DPH_DisplayGroups();

	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Groups_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_Groups_Core

    Show all defined groups with their setpoint
*******************************************************************************/
void MNU_Groups_Core(void)
{
    uint8_t group;
    uint8_t name[NAME_SIZE_AND_NULL];

    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure key in the Groups menu
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_Groups_Commit, (void*)KEY_BACK);
        ActiveGroupNumber = 0;
        for (group = 0; group < GROUP_INSTANCES; group++)
        {
            DBTHRD_GetData(DBTYPE_GROUP_NAME, &name, group);
            if (name[0] != 0)
            {
                ActiveGroupNumber++;
            }
        }
        if (ActiveGroupNumber >= 1)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_1, (FUNC)MNU_Groups_Commit, (void*)KEY_OPTIONS_1);
        }
        if (ActiveGroupNumber >= 2)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)MNU_Groups_Commit, (void*)KEY_OPTIONS_2);
        }
        if (ActiveGroupNumber >= 3)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)MNU_Groups_Commit, (void*)KEY_OPTIONS_3);
        }
        if (ActiveGroupNumber >= 4)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_4, (FUNC)MNU_Groups_Commit, (void*)KEY_OPTIONS_4);
        }
        if (ActiveGroupNumber >= 5)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_5, (FUNC)MNU_Groups_Commit, (void*)KEY_OPTIONS_5);
        }
        if (ActiveGroupNumber >= 6)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_6, (FUNC)MNU_Groups_Commit, (void*)KEY_OPTIONS_6);
        }
        if (ActiveGroupNumber >= 7)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_7, (FUNC)MNU_Groups_Commit, (void*)KEY_OPTIONS_7);
        }
        if (ActiveGroupNumber >= 8)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_8, (FUNC)MNU_Groups_Commit, (void*)KEY_OPTIONS_8);
        }

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_Groups_Exit
*******************************************************************************/
void MNU_Groups_Exit(void)
{
    GroupsEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_Groups_Commit
*******************************************************************************/
void MNU_Groups_Commit(KEYB_PARAM keyId)
{
    MnuSortedGroups_t sortedGroups[GROUP_INSTANCES];
	if (GroupsEditState == EDIT_IN_PROGRESS)
	{
        KBH_ClearAllKeyProperties();
        switch(keyId)
        {
            case KEY_BACK:
                MNH_SetActiveMenu(MENU_HOME);
                break;

            case KEY_OPTIONS_1:
            case KEY_OPTIONS_2:
            case KEY_OPTIONS_3:
            case KEY_OPTIONS_4:
            case KEY_OPTIONS_5:
            case KEY_OPTIONS_6:
            case KEY_OPTIONS_7:
            case KEY_OPTIONS_8:
                MNU_Groups_SortGroups(&sortedGroups[0]);
                SelectedGroupNumber = sortedGroups[keyId - KEY_OPTIONS_1].position;
                MNH_SetActiveMenu(MENU_GROUPDETAILS);
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
		GroupsEditState = EDIT_IDLE;
	}
}

/*******************************************************************************
    MNU_Groups_GetSelectedGroup
*******************************************************************************/
uint8_t MNU_Groups_GetSelectedGroup(void)
{
    return SelectedGroupNumber;
}

/*******************************************************************************
    MNU_Groups_SortGroups
    Sort the groups alphabetically
*******************************************************************************/
uint8_t MNU_Groups_SortGroups(MnuSortedGroups_t* pSortedGroups)
{
    uint8_t group;
    uint8_t definedGroupNumber = 0;
    uint8_t groupName[NAME_SIZE_AND_NULL];
    uint8_t posToCheck;
    uint8_t othersToCompare;

    for (group = 0; group < GROUP_INSTANCES; group++)
    {
        DBTHRD_GetData(DBTYPE_GROUP_NAME, &groupName, group);
        if (groupName[0] != 0)
        {
            pSortedGroups[definedGroupNumber].position = group;
            strcpy((char*)pSortedGroups[definedGroupNumber].str, (char const*)groupName);
            definedGroupNumber++;
        }
    }
    if (definedGroupNumber != 0)
    {
        //Sort groups alphabetically
        for (posToCheck = 0; posToCheck < definedGroupNumber; posToCheck++)
        {
            for (othersToCompare = (posToCheck + 1); othersToCompare < definedGroupNumber; othersToCompare++)
            {
                if (my_string_compare((char const*)pSortedGroups[posToCheck].str, (char const*)pSortedGroups[othersToCompare].str) > 0)
                {
                    group = pSortedGroups[posToCheck].position;
                    strcpy((char*)groupName, (char const*)pSortedGroups[posToCheck].str);
                    pSortedGroups[posToCheck].position = pSortedGroups[othersToCompare].position;
                    strcpy((char*)pSortedGroups[posToCheck].str, (char const*)pSortedGroups[othersToCompare].str);
                    pSortedGroups[othersToCompare].position = group;
                    strcpy((char*)pSortedGroups[othersToCompare].str, (char const*)groupName);
                }
            }
        }
    }
    return definedGroupNumber;
}
