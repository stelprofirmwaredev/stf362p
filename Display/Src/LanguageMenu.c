/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       LanguageMenu.c

    \brief      Source code file for the LanguageMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the LanguageMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "LanguageMenu.h"
#include ".\DB\inc\THREAD_DB.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e           LanguageEditState = EDIT_IDLE;
static DisplayLang_t            LanguageEditValue;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void ChangeLanguage(KEYB_PARAM keyId);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    ChangeLanguage

    Change the language to either Fran�ais or English
*******************************************************************************/
static void ChangeLanguage(KEYB_PARAM keyId)
{
    uint8_t onboardingDone;

    switch (keyId)
    {
        case KEY_OPTIONS_2:
            LanguageEditValue = LANG_FRANCAIS;
            break;

        case KEY_OPTIONS_3:
            LanguageEditValue = LANG_ENGLISH;
            break;

        default:
            break;
    }

    DPH_DisplayLanguage(LanguageEditValue);

    DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);
    if (onboardingDone == 1)
    {
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Language_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    }
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_Language_Entry
*******************************************************************************/
void MNU_Language_Entry(void)
{
    uint8_t onboardingDone;

    DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);
    DBTHRD_GetData(DBTYPE_LANGUAGE_SETTING,(void*)&LanguageEditValue, INDEX_DONT_CARE);
    LanguageEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);

    if (onboardingDone == 1)
    {
        //
        //  Show Back arrow
        //
        DPH_DisplayBack();
    }

    //
    //  Show display language
    //
    DPH_DisplayLanguage(LanguageEditValue);

    if (onboardingDone == 1)
    {
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Language_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    }
}

/*******************************************************************************
    MNU_Language_Core

    Allows to change the display language
*******************************************************************************/
void MNU_Language_Core(void)
{
    uint8_t onboardingDone;

    DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);

    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        if (onboardingDone == 1)
        {
            KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_Language_Commit, (void*)KEY_BACK);
        }
        KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)ChangeLanguage, (void*)KEY_OPTIONS_2);
        KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)ChangeLanguage, (void*)KEY_OPTIONS_3);
        KBH_SetKeyPressProperties(KEY_DONE, (FUNC)MNU_Language_Commit, (void*)KEY_DONE);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_Language_Exit
*******************************************************************************/
void MNU_Language_Exit(void)
{
    LanguageEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_Language_Init
*******************************************************************************/
void MNU_Language_Init(void)
{
    MNU_Language_SetLanguage(LANG_FRANCAIS);
}

/*******************************************************************************
    MNU_Language_Commit
*******************************************************************************/
void MNU_Language_Commit(KEYB_PARAM keyId)
{
    static KEYB_PARAM commitKey = NULL;
    uint8_t onboardingDone;

    if ( LanguageEditState == EDIT_IN_PROGRESS )
    {
        commitKey = keyId;
        KBH_ClearAllKeyProperties();
        switch (commitKey)
        {
            case KEY_DONE:
                MNU_Language_SetLanguage(LanguageEditValue);
                //No break on purpose

            case KEY_BACK:
            default:
                MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Language_Commit, (KEYB_PARAM*)commitKey, IMMEDIATELY);
                break;
        }
        LanguageEditState = EDIT_COMMITTING;
    }
    else if ( LanguageEditState == EDIT_COMMITTING )
    {
        LanguageEditState = EDIT_IDLE;
        DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);

        switch (commitKey)
        {
            case KEY_BACK:
            case KEY_DONE:
                if (onboardingDone == 1)
                {
                    MNH_SetActiveMenu(MENU_OPTIONS);
                }
                else
                {
                    MNH_SetActiveMenu(MENU_UNIT_FORMAT);
                }
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
    }
}

/*******************************************************************************
    MNU_Language_SetLanguage

    Set the language to Fran�ais or English
*******************************************************************************/
void MNU_Language_SetLanguage(DisplayLang_t lang)
{
    DBTHRD_SetData(DBTYPE_LANGUAGE_SETTING,(void*)&lang, INDEX_DONT_CARE, DBCHANGE_LOCAL);
}
