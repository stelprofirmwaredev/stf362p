/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2015, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    APP_Display.c
* @date    2016/07/14
* @authors Jean-Fran�ois Many
* @brief   Display Module
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include "stm32f4xx_hal.h"
#include <string.h>
//#include "stm32f429i_discovery_lcd.h"
#include "typedef.h"
#include "tools.h"
#include "APP_Display.h"
#include "StringObjects.h"
#include "images.h"
#include ".\HAL\inc\Display.h"
#include ".\HAL\inc\HAL_Flash.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\HAL\inc\HAL_WiFiTypes.h"
#include "WiFiScanMenu.h"
#include "BacklightMenu.h"
#include "ActivitiesMenu.h"
#include "GroupsMenu.h"
#include "APP_SetpointManager.h"
#include ".\HAL\inc\HAL_LoadManagement.h"
#include ".\HAL\inc\TemperatureCompensation.h"
#include ".\HAL\inc\HAL_CloudInterface.h"
#include "SwVersion.h"
#include ".\HAL\inc\AylaProperties.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define TITLE_LINE1_Y_POS   20
#define TITLE_LINE2_Y_POS   57
#define MAX_INPUT_CHAR_LENGTH_ON_ONE_LINE 18
#define MAX_INSTALL_CODE_LENGTH 36
#define MAX_MAC_ADDRESS_LENGTH  16
#define TIME_9_PM   1260 //21h * 60 minutes
#define TIME_5_AM   300 //5h * 60 minutes
/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
__no_init uint8_t LcdTempBuffer[320*480*4] @0xC0096000;
__no_init uint8_t LcdTempBuffer2[320*480*4] @0xC012C000;

// 32 bits serial number at start of OTP memory (address: 0x1FFF7800)
// Year : bits 25-31
// Month : bits 21-24
// Day : bits 16-20
// Sequential Number : bits 0-15
__no_init U32BIT ProductSerialNumber @ 0x1FFF7800;

uint32_t RAM_OFFSET;
/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
char Network1[10][20];
extern const weather_t weatherTable[];

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static boolean DphTemperatureInRange(S16BIT temperature, U8BIT* pString, U8BIT options);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    DphTemperatureInRange

    Check the temperature against the system temperature valid range.

    \param temperature Temperature * 100 in celsius units

    \param pErrSid Address of variable to fill with error string ID

    \param options Bounding options for MIN_TEMP and MAX_TEMP

    \return TRUE if temperature is in a valid range
*******************************************************************************/
static boolean DphTemperatureInRange(S16BIT temperature, U8BIT* pString, U8BIT options)
{
    boolean ok = TRUE;
    *pString = 0;
    UnitFormat_t format;

    DBTHRD_GetData(DBTYPE_TEMPERATURE_FORMAT,(void*)&format, INDEX_DONT_CARE);

    if (temperature == DB_SENSOR_INVALID)
    {
        STR_StrCpy(pString, (uint8_t*)STR_GetString(STRID_ERR));
        ok =  FALSE;
    }
    else if (temperature == DB_SENSOR_LOW)
    {
        STR_StrCpy(pString, (uint8_t*)STR_GetString(STRID_LOW));
        ok =   FALSE;
    }
    else if (temperature == DB_SENSOR_HIGH)
    {
        STR_StrCpy(pString, (uint8_t*)STR_GetString(STRID_HIGH));
        ok =   FALSE;
    }
    else if ((temperature <= MIN_TEMP) && (options & TEMP_BOUND) != 0)
    {
        STR_StrCpy(pString, (uint8_t*)STR_GetString(STRID_LOW));
        ok =   FALSE;
    }
    else if ((temperature >= MAX_TEMP) && (options & TEMP_BOUND) != 0)
    {
        STR_StrCpy(pString, (uint8_t*)STR_GetString(STRID_HIGH));
        ok =   FALSE;
    }
    else if ((temperature >= MAX_TEMP_F) && (format == DEGREE_F) && (options & TEMP_BOUND) != 0)
    {
        STR_StrCpy(pString, (uint8_t*)STR_GetString(STRID_HIGH));
        ok =   FALSE;
    }
    else
    {
        //Empty
    }

    return ok;
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  Init the display
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/07/20
*******************************************************************************/
void DPH_InitDisplay(void)
{
    BSP_LCD_Init();
#ifdef UI_ENABLE
    BSP_LCD_LayerDefaultInit();
    BSP_LCD_SelectLayer(LCD_BACKGROUND_LAYER);
    BSP_LCD_Clear(LCD_DEFAULT_BACKGROUND_COLOR);
#endif

}

/*******************************************************************************
* @brief  Display the splash screen
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/07/19
*******************************************************************************/
void DPH_DisplaySplashScreen(void)
{
    HAL_ReadFlashData (STELPRO_LOGO_ADD + RAM_OFFSET, STELPRO_LOGO_SIZE, &LcdTempBuffer[0]);
    BSP_LCD_DrawBitmap(104,164,(uint8_t*)&LcdTempBuffer);
}

/*******************************************************************************
* @brief  Display the current mode
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/07/14
*******************************************************************************/
void DPH_DisplayMode(ScheduleModeEnum_t mode)
{
    uint8_t validMode = 1;
    Program_t pgm;
    switch (mode)
    {
        case MODE_PROG:
        case MODE_DEROGATION:
            DBTHRD_GetData(DBTYPE_ACTIVE_PGM,(void*)&pgm, INDEX_DONT_CARE);
            switch (pgm)
            {
                case PGM_WAKE:
                    HAL_ReadFlashData (WAKE_BTN_ADD + RAM_OFFSET, WAKE_BTN_SIZE, &LcdTempBuffer[0]);
                    break;

                case PGM_LEAVE:
                    HAL_ReadFlashData (LEAVE_BTN_ADD + RAM_OFFSET, LEAVE_BTN_SIZE, &LcdTempBuffer[0]);
                    break;

                case PGM_RETURN:
                    HAL_ReadFlashData (RETURN_BTN_ADD + RAM_OFFSET, RETURN_BTN_SIZE, &LcdTempBuffer[0]);
                    break;

                case PGM_SLEEP:
                    HAL_ReadFlashData (SLEEP_BTN_ADD + RAM_OFFSET, SLEEP_BTN_SIZE, &LcdTempBuffer[0]);
                    break;

                default:
                    break;
            }
            break;

        case MODE_MANUAL:
            HAL_ReadFlashData (MANUAL_BTN_ADD + RAM_OFFSET, MANUAL_BTN_SIZE, &LcdTempBuffer[0]);
            break;

        default:
            validMode = 0;
            break;
    }
    if (validMode == 1)
    {
        BSP_LCD_DrawBitmap(128,262,(uint8_t*)&LcdTempBuffer);
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    }
}

/*******************************************************************************
* @brief  Display the current ambient room temperature
* @inputs temperature: ambient room temperature (x100)
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/07/14
*******************************************************************************/
void DPH_DisplayAmbientRoomTemperature(int16_t temperature, uint8_t isFloorTemp)
{
    uint8_t string_ptr[8];
    uint8_t string_ptr2[8];
    UnitFormat_t format;

    BSP_LCD_ClearStringLineSection(85, 140, 185, 106);
    BSP_LCD_SetTextColor(LCD_DEFAULT_BACKGROUND_COLOR);
    BSP_LCD_FillRect(85, 140, 185, 106);
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    if ((isFloorTemp == CONTROLMODE_AMBIENT) || (isFloorTemp == CONTROLMODE_AMBIENTFLOOR))
    {
        BSP_LCD_DisplaySpecificFontStringAt(0, 113, (uint8_t*)STR_GetString(STRID_INDOOR), CENTER_MODE, &GUI_FontCairoSemiBold30);
    }
    else if (isFloorTemp == CONTROLMODE_FLOOR)
    {
        BSP_LCD_DisplaySpecificFontStringAt(0, 113, (uint8_t*)STR_GetString(STRID_FLOOR), CENTER_MODE, &GUI_FontCairoSemiBold30);
        HAL_ReadFlashData (FOOT_ICON_ADD + RAM_OFFSET, FOOT_ICON_SIZE, &LcdTempBuffer[0]);
        BSP_LCD_DrawBitmap(243,146,(uint8_t*)&LcdTempBuffer);
    }
    if (DphTemperatureInRange(temperature, &string_ptr[0], TEMP_BOUND))
    {
        DBTHRD_GetData(DBTYPE_TEMPERATURE_FORMAT,(void*)&format, INDEX_DONT_CARE);
        if (format == DEGREE_C)
        {
            STR_Temp2Str((int16s)temperature, string_ptr, ADD_SPACING | TEMP_DOT | HIDE_DECIMAL);
            BSP_LCD_DisplaySpecificFontStringAt(0, 140, string_ptr, CENTER_MODE, &GUI_FontCairoLight285);
            STR_StrCpy(string_ptr, ".");
            STR_Temp2Str(temperature, string_ptr2, TEMP_DOT | DECIMAL_ONLY);
            STR_StrCat(string_ptr, string_ptr2);
            BSP_LCD_DisplaySpecificFontStringAt(0, 212, string_ptr, APPEND_MODE, &GUI_FontCairoSemiBold85);
        }
        else
        {
            STR_Temp2Str((int16s)temperature, string_ptr, FAHRENHEIT | ADD_SPACING);
            BSP_LCD_DisplaySpecificFontStringAt(0, 140, string_ptr, CENTER_MODE, &GUI_FontCairoLight285);
        }
    }
    else
    {
        BSP_LCD_DisplaySpecificFontStringAt(0, 170, string_ptr, CENTER_MODE, &GUI_FontCairoSemiBold62);
    }
}


/*******************************************************************************
* @brief  Display a checkmark when the setpoint is applied
* @inputs editInProgress: true or false
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/12/08
*******************************************************************************/
#define SETPOINT_APPLIED    2
void DPH_DisplaySetpointApplied(uint8_t editInProgress)
{
    BSP_LCD_ClearStringLineSection(128, 262, manual_btn.width, manual_btn.height);
    if (editInProgress == SETPOINT_APPLIED)
    {
        HAL_ReadFlashData (CHECK_ICON_ADD + RAM_OFFSET, CHECK_ICON_SIZE, &LcdTempBuffer[0]);
        BSP_LCD_DrawBitmap(144,295,(uint8_t*)&LcdTempBuffer);
    }
}

/*******************************************************************************
* @brief  Display the current setpoint
* @inputs setpoint: temperature setpoint (x100)
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/07/14
*******************************************************************************/
void DPH_DisplaySetpoint(uint16_t setpoint, uint8_t firstEntry, uint8_t offMode, uint8_t type, uint8_t isLocalTstat)
{
    uint8_t string_ptr[8];
    uint8_t string_ptr2[8];
    UnitFormat_t format;
    uint16_t maxSetpoint;
    uint16_t minSetpoint;
    FloorType_t floorType;
    static uint8_t wasInOff = 0;
    static uint8_t lastType = 0;

    if (lastType != type)
    {
        BSP_LCD_ClearStringLineSection(0, 440, 320, 30);
    }

    if (firstEntry || offMode || wasInOff || (lastType != type))
    {
        HAL_ReadFlashData (SETPOINT_BOX_ADD + RAM_OFFSET, SETPOINT_BOX_SIZE, &LcdTempBuffer2[0]);

        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        if (type == SET_GROUP)
        {
            BSP_LCD_DisplaySpecificFontStringAt(0, 440, (uint8_t*)STR_GetString(STRID_SETPOINT_GROUP), CENTER_MODE, &GUI_FontCairoSemiBold30);
        }
        else if (type == SET_GROUP_FLOOR)
        {
            BSP_LCD_DisplaySpecificFontStringAt(0, 440, (uint8_t*)STR_GetString(STRID_SETPOINT_GROUP_FLOOR), CENTER_MODE, &GUI_FontCairoSemiBold30);
        }
        else if (type == SET_FLOOR_LIMIT)
        {
            BSP_LCD_DisplaySpecificFontStringAt(0, 440, (uint8_t*)STR_GetString(STRID_SETPOINT_LIMIT), CENTER_MODE, &GUI_FontCairoSemiBold30);
        }
        else if (type == SET_FLOOR)
        {
            BSP_LCD_DisplaySpecificFontStringAt(0, 440, (uint8_t*)STR_GetString(STRID_SETPOINT_FLOOR), CENTER_MODE, &GUI_FontCairoSemiBold30);
        }
        else if (type == SET_TEMPORARY)
        {
            BSP_LCD_SetTextColor(LCD_COLOR_ORANGE);
            BSP_LCD_DisplaySpecificFontStringAt(0, 440, (uint8_t*)STR_GetString(STRID_SETPOINT_TEMPORARY), CENTER_MODE, &GUI_FontCairoSemiBold30);
        }
        else
        {
            BSP_LCD_DisplaySpecificFontStringAt(0, 440, (uint8_t*)STR_GetString(STRID_SETPOINT_AMBIENT), CENTER_MODE, &GUI_FontCairoSemiBold30);
        }
        //Display Setpoint box
        BSP_LCD_DrawBitmap(25,336,(uint8_t*)&LcdTempBuffer2);
    }
    lastType = type;
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    if (offMode == FALSE)
    {
        //Apply the color found
        BSP_LCD_SetTextColor(DPH_GetColor(setpoint));
        BSP_LCD_FillRect(127, 336, 66, 100);

        DBTHRD_GetData(DBTYPE_TEMPERATURE_FORMAT,(void*)&format, INDEX_DONT_CARE);

        BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
        //Display current setpoint
        if (format == DEGREE_C)
        {
            STR_Temp2Str((int16s)setpoint, string_ptr, ADD_SPACING | TEMP_DOT | HIDE_DECIMAL);
            BSP_LCD_DisplaySpecificFontStringAt(128, 370, string_ptr, LEFT_MODE, &GUI_FontCairoSemiBold81);
            STR_StrCpy(string_ptr, ".");
            STR_Temp2Str((int16s)setpoint, string_ptr2, TEMP_DOT | DECIMAL_ONLY);
            STR_StrCat(string_ptr, string_ptr2);
            BSP_LCD_DisplaySpecificFontStringAt(128, 375, string_ptr, APPEND_MODE, &GUI_FontCairoSemiBold46);
        }
        else
        {
            STR_Temp2Str((int16s)setpoint, string_ptr, FAHRENHEIT | ADD_SPACING);
            BSP_LCD_DisplaySpecificFontStringAt(0, 370, string_ptr, CENTER_MODE, &GUI_FontCairoSemiBold81);
        }

        //Display setpoint + 0.5
        BSP_LCD_ClearStringLineSection(195, 371, 55, 30);
        BSP_LCD_SetTextColor(LCD_COLOR_SETPOINT_BOX);
        BSP_LCD_FillRect(195, 366, 55, 40);
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        maxSetpoint = MAXIMUM_SETPOINT;
        if ((type == SET_FLOOR) || (type == SET_FLOOR_LIMIT) || (type == SET_GROUP_FLOOR))
        {
            DBTHRD_GetData(DBTYPE_FLOOR_TYPE,(void*)&floorType, THIS_THERMOSTAT);
            if ((isLocalTstat != 0) && (floorType == FLOORTYPE_ENGINEERED) && (type != SET_GROUP_FLOOR))
            {
                maxSetpoint = MAXIMUM_ENGINEERED_FLOOR_SETPOINT;
            }
            else
            {
                maxSetpoint = MAXIMUM_FLOOR_SETPOINT;
            }
        }
        if (setpoint < maxSetpoint)
        {
            if (format == DEGREE_C)
            {
                STR_Temp2Str((int16s)setpoint+50, string_ptr, ADD_SPACING | TEMP_DOT | HIDE_DECIMAL);
                BSP_LCD_DisplaySpecificFontStringAt(195, 366, string_ptr, LEFT_MODE, &GUI_FontCairoSemiBold62);
                STR_StrCpy(string_ptr, ".");
                STR_Temp2Str((int16s)(setpoint+50), string_ptr2, TEMP_DOT | DECIMAL_ONLY);
                STR_StrCat(string_ptr, string_ptr2);
                BSP_LCD_DisplaySpecificFontStringAt(195, 373, string_ptr, APPEND_MODE, &GUI_FontCairoSemiBold46);
            }
            else
            {
                STR_Temp2Str((int16s)setpoint+50, string_ptr, FAHRENHEIT | ADD_SPACING);
                BSP_LCD_DisplaySpecificFontStringAt(207, 366, string_ptr, LEFT_MODE, &GUI_FontCairoSemiBold62);
            }
        }

        //Display setpoint - 0.5
        BSP_LCD_ClearStringLineSection(71, 371, 55, 30);
        BSP_LCD_SetTextColor(LCD_COLOR_SETPOINT_BOX);
        BSP_LCD_FillRect(71, 366, 55, 40);
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        minSetpoint = MINIMUM_SETPOINT;
        if (type == SET_FLOOR_LIMIT)
        {
            minSetpoint = MINIMUM_FLOOR_LIMIT;
        }
        if (setpoint > minSetpoint)
        {
            if (format == DEGREE_C)
            {
                STR_Temp2Str((int16s)setpoint-50, string_ptr, ADD_SPACING | TEMP_DOT | HIDE_DECIMAL);
                BSP_LCD_DisplaySpecificFontStringAt(71, 366, string_ptr, LEFT_MODE, &GUI_FontCairoSemiBold62);
                STR_StrCpy(string_ptr, ".");
                STR_Temp2Str((int16s)(setpoint-50), string_ptr2, TEMP_DOT | DECIMAL_ONLY);
                STR_StrCat(string_ptr, string_ptr2);
                BSP_LCD_DisplaySpecificFontStringAt(71, 373, string_ptr, APPEND_MODE, &GUI_FontCairoSemiBold46);
            }
            else
            {
                STR_Temp2Str((int16s)setpoint-50, string_ptr, FAHRENHEIT | ADD_SPACING);
                BSP_LCD_DisplaySpecificFontStringAt(83, 366, string_ptr, LEFT_MODE, &GUI_FontCairoSemiBold62);
            }
        }
        wasInOff = 0;
    }
    else
    {
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_DisplaySpecificFontStringAt(0, 370, (uint8_t*)STR_GetString(STRID_SETPOINT_OFF), CENTER_MODE, &GUI_FontCairoSemiBold62);
        wasInOff = 1;
    }
}

/*******************************************************************************
* @brief  Display the floor temperature limit
* @inputs setpoint: temperature limit (x100)
* @retval None
* @author Jean-Fran�ois Many
* @date   2019/04/09
*******************************************************************************/
void DPH_DisplayFloorLimit(uint16_t limit, uint8_t firstEntry)
{
    //Display the menu header
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE1_Y_POS, (uint8_t*)STR_GetString(STRID_FLOOR_LIMIT_TITLE1), CENTER_MODE, &GUI_FontCairoSemiBold62);
    BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE2_Y_POS, (uint8_t*)STR_GetString(STRID_FLOOR_LIMIT_TITLE2), CENTER_MODE, &GUI_FontCairoSemiBold62);

    //Display advise box
    HAL_ReadFlashData (ADVISE_BOX_ADD + RAM_OFFSET, ADVISE_BOX_SIZE, &LcdTempBuffer[0]);
    BSP_LCD_DrawBitmap(24,169,(uint8_t*)&LcdTempBuffer);
    BSP_LCD_DisplaySpecificFontStringAt(0, 132, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_ADVISE), CENTER_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(0, 182, (uint8_t*)STR_GetString(STRID_FLOOR_LIMIT_ADVISE1), CENTER_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(0, 212, (uint8_t*)STR_GetString(STRID_FLOOR_LIMIT_ADVISE2), CENTER_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(0, 242, (uint8_t*)STR_GetString(STRID_FLOOR_LIMIT_ADVISE3), CENTER_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(0, 272, (uint8_t*)STR_GetString(STRID_FLOOR_LIMIT_ADVISE4), CENTER_MODE, &GUI_FontCairoSemiBold46);

    //Display the temperature limit
    DPH_DisplaySetpoint(limit, firstEntry, FALSE, SET_FLOOR_LIMIT, TRUE);
}

/*******************************************************************************
* @brief  Display the settings button
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/12/07
*******************************************************************************/
void DPH_DisplaySettingsButton(void)
{
    HAL_ReadFlashData (SETTINGS_BTN_ADD + RAM_OFFSET, SETTINGS_BTN_SIZE, &LcdTempBuffer[0]);
    BSP_LCD_DrawBitmap(264,25,(uint8_t*)&LcdTempBuffer);
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
}

/*******************************************************************************
* @brief  Display the lock icon
* @inputs lock: lock state
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/07/14
*******************************************************************************/
void DPH_DisplayLock(Lock_t lockState)
{
    if (lockState == LOCK_ON)
    {
        HAL_ReadFlashData (LOCK_ICON_ADD + RAM_OFFSET, LOCK_ICON_SIZE, &LcdTempBuffer[0]);
        BSP_LCD_DrawBitmap(233, 25, (uint8_t*)&LcdTempBuffer);
    }
    else
    {
        BSP_LCD_ClearStringLineSection(233, 25, lock_icon.width, lock_icon.height);
    }
}

/*******************************************************************************
* @brief  Display the heating status
* @inputs heatDemand: the current heat demand
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/07/14
*******************************************************************************/
void DPH_DisplayHeatingStatus(HEAT_DEMAND_PERCENT_t heatDemand, uint8_t offMode)
{
    U8BIT heatState;

    heatState = Get_OutputDemand();

    if (heatState == FALSE)
    {
        heatDemand = 0; //Do not display flames when drive is off in long cycle
    }

    if (offMode != FALSE)
    {
        //Do not display any heat bars when heating is off
        BSP_LCD_ClearStringLineSection(136, 72, heat_off.width, heat_off.height);
        BSP_LCD_ClearStringLineSection(146, 72, heat_off.width, heat_off.height);
        BSP_LCD_ClearStringLineSection(156, 72, heat_off.width, heat_off.height);
        BSP_LCD_ClearStringLineSection(166, 72, heat_off.width, heat_off.height);
        BSP_LCD_ClearStringLineSection(176, 72, heat_off.width, heat_off.height);
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_DisplaySpecificFontStringAt(0, 72, (uint8_t*)STR_GetString(STRID_SETPOINT_OFF), CENTER_MODE, &GUI_FontCairoSemiBold62);
    }
    else
    {
        BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
        BSP_LCD_DisplaySpecificFontStringAt(0, 72, (uint8_t*)STR_GetString(STRID_SETPOINT_OFF), CENTER_MODE, &GUI_FontCairoSemiBold62);
        if ((heatDemand == 0) || (offMode != FALSE))
        {
            //Do not display any heat bars when heating is off
            BSP_LCD_ClearStringLineSection(136, 72, heat_off.width, heat_off.height);
            BSP_LCD_ClearStringLineSection(146, 72, heat_off.width, heat_off.height);
            BSP_LCD_ClearStringLineSection(156, 72, heat_off.width, heat_off.height);
            BSP_LCD_ClearStringLineSection(166, 72, heat_off.width, heat_off.height);
            BSP_LCD_ClearStringLineSection(176, 72, heat_off.width, heat_off.height);
        }
        else if (heatDemand <= 24)
        {
            HAL_ReadFlashData (HEAT_ON_ICON_ADD + RAM_OFFSET, HEAT_ON_ICON_SIZE, &LcdTempBuffer[0]);
            BSP_LCD_DrawBitmap(136, 72, (uint8_t*)&LcdTempBuffer);

            HAL_ReadFlashData (HEAT_OFF_ICON_ADD + RAM_OFFSET, HEAT_OFF_ICON_SIZE, &LcdTempBuffer[0]);
            BSP_LCD_DrawBitmap(146, 72, (uint8_t*)&LcdTempBuffer);
            BSP_LCD_DrawBitmap(156, 72, (uint8_t*)&LcdTempBuffer);
            BSP_LCD_DrawBitmap(166, 72, (uint8_t*)&LcdTempBuffer);
            BSP_LCD_DrawBitmap(176, 72, (uint8_t*)&LcdTempBuffer);
        }
        else if (heatDemand <= 49)
        {
            HAL_ReadFlashData (HEAT_ON_ICON_ADD + RAM_OFFSET, HEAT_ON_ICON_SIZE, &LcdTempBuffer[0]);
            BSP_LCD_DrawBitmap(136, 72, (uint8_t*)&LcdTempBuffer);
            BSP_LCD_DrawBitmap(146, 72, (uint8_t*)&LcdTempBuffer);

            HAL_ReadFlashData (HEAT_OFF_ICON_ADD + RAM_OFFSET, HEAT_OFF_ICON_SIZE, &LcdTempBuffer[0]);
            BSP_LCD_DrawBitmap(156, 72, (uint8_t*)&LcdTempBuffer);
            BSP_LCD_DrawBitmap(166, 72, (uint8_t*)&LcdTempBuffer);
            BSP_LCD_DrawBitmap(176, 72, (uint8_t*)&LcdTempBuffer);
        }
        else if (heatDemand <= 74)
        {
            HAL_ReadFlashData (HEAT_ON_ICON_ADD + RAM_OFFSET, HEAT_ON_ICON_SIZE, &LcdTempBuffer[0]);
            BSP_LCD_DrawBitmap(136, 72, (uint8_t*)&LcdTempBuffer);
            BSP_LCD_DrawBitmap(146, 72, (uint8_t*)&LcdTempBuffer);
            BSP_LCD_DrawBitmap(156, 72, (uint8_t*)&LcdTempBuffer);

            HAL_ReadFlashData (HEAT_OFF_ICON_ADD + RAM_OFFSET, HEAT_OFF_ICON_SIZE, &LcdTempBuffer[0]);
            BSP_LCD_DrawBitmap(166, 72, (uint8_t*)&LcdTempBuffer);
            BSP_LCD_DrawBitmap(176, 72, (uint8_t*)&LcdTempBuffer);
        }
        else if (heatDemand <= 99)
        {
            HAL_ReadFlashData (HEAT_ON_ICON_ADD + RAM_OFFSET, HEAT_ON_ICON_SIZE, &LcdTempBuffer[0]);
            BSP_LCD_DrawBitmap(136, 72, (uint8_t*)&LcdTempBuffer);
            BSP_LCD_DrawBitmap(146, 72, (uint8_t*)&LcdTempBuffer);
            BSP_LCD_DrawBitmap(156, 72, (uint8_t*)&LcdTempBuffer);
            BSP_LCD_DrawBitmap(166, 72, (uint8_t*)&LcdTempBuffer);

            HAL_ReadFlashData (HEAT_OFF_ICON_ADD + RAM_OFFSET, HEAT_OFF_ICON_SIZE, &LcdTempBuffer[0]);
            BSP_LCD_DrawBitmap(176, 72, (uint8_t*)&LcdTempBuffer);
        }
        else
        {
            HAL_ReadFlashData (HEAT_ON_ICON_ADD + RAM_OFFSET, HEAT_ON_ICON_SIZE, &LcdTempBuffer[0]);
            BSP_LCD_DrawBitmap(136, 72, (uint8_t*)&LcdTempBuffer);
            BSP_LCD_DrawBitmap(146, 72, (uint8_t*)&LcdTempBuffer);
            BSP_LCD_DrawBitmap(156, 72, (uint8_t*)&LcdTempBuffer);
            BSP_LCD_DrawBitmap(166, 72, (uint8_t*)&LcdTempBuffer);
            BSP_LCD_DrawBitmap(176, 72, (uint8_t*)&LcdTempBuffer);
        }
    }
}

/*******************************************************************************
* @brief  Display the alert icon
* @inputs activeAlert: active alert or not
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/08/03
*******************************************************************************/
void DPH_DisplayAlertIcon(U8BIT activeAlert)
{
    if (activeAlert)
    {
        HAL_ReadFlashData (ALERT_ICON_ADD + RAM_OFFSET, ALERT_ICON_SIZE, &LcdTempBuffer[0]);
        BSP_LCD_DrawBitmap(25, 25, (uint8_t*)&LcdTempBuffer);
    }
    else
    {
        BSP_LCD_ClearStringLineSection(25, 25, alert_icon.width, alert_icon.height);
    }
}

/*******************************************************************************
* @brief  Display the time
* @inputs minutes: time of day
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/07/15
*******************************************************************************/
void DPH_DisplayTime(RtcTime_t* time)
{
    uint8_t string_ptr[10];
    uint16_t min_of_day = time->Hours * 60 + time -> Minutes;
    TimeFormat_t format;

    DBTHRD_GetData(DBTYPE_TIME_FORMAT,(void*)&format, INDEX_DONT_CARE);
    if (format == TIME_24H)
    {
        STR_Time2Str(min_of_day, string_ptr, STD_TIME | USE_24H_FORMAT| NO_SPACE);  //Format = "10:50" / "22:08"
    }
    else
    {
        STR_Time2Str(min_of_day, string_ptr, STD_TIME | LONG_12H  | NO_SPACE);  //Format = "10:50am" / "10:08pm"
    }

    BSP_LCD_ClearStringLineSection(98, 17, 135, 45);

    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    BSP_LCD_DisplaySpecificFontStringAt(0, 22, string_ptr, CENTER_MODE, &GUI_FontCairoSemiBold62);
}

/*******************************************************************************
* @brief  Display the back arrow
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/07/18
*******************************************************************************/
void DPH_DisplayBack(void)
{
    HAL_ReadFlashData (BACK_ICON_ADD + RAM_OFFSET, BACK_ICON_SIZE, &LcdTempBuffer[0]);
    BSP_LCD_DrawBitmap(24,24,(uint8_t*)&LcdTempBuffer);
}

/*******************************************************************************
* @brief  Display the root settings menu
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/07/18
*******************************************************************************/
void DPH_DisplayRootSettings(void)
{
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE1_Y_POS, (uint8_t*)STR_GetString(STRID_SETTINGS), CENTER_MODE, &GUI_FontCairoSemiBold62);

    HAL_ReadFlashData (GREY_BTN_ADD + RAM_OFFSET, GREY_BTN_SIZE, &LcdTempBuffer[0]);

    //Display the grey buttons
    BSP_LCD_DrawBitmap(25, 119, (uint8_t*)&LcdTempBuffer);
    BSP_LCD_DrawBitmap(25, 169, (uint8_t*)&LcdTempBuffer);
    BSP_LCD_DrawBitmap(25, 219, (uint8_t*)&LcdTempBuffer);
    BSP_LCD_DrawBitmap(25, 269, (uint8_t*)&LcdTempBuffer);
    BSP_LCD_DrawBitmap(25, 319, (uint8_t*)&LcdTempBuffer);

    //Display the Stelpro red button
    HAL_ReadFlashData (STELPRO_BTN_ADD + RAM_OFFSET, STELPRO_BTN_SIZE, &LcdTempBuffer[0]);
    BSP_LCD_DrawBitmap(25, 369, (uint8_t*)&LcdTempBuffer);

    BSP_LCD_DisplaySpecificFontStringAt(38, 123, (uint8_t*)STR_GetString(STRID_SETTINGS_DATE_TIME), LEFT_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(38, 173, (uint8_t*)STR_GetString(STRID_SETTINGS_PROGRAMS), LEFT_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(38, 223, (uint8_t*)STR_GetString(STRID_SETTINGS_OPTIONS), LEFT_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(38, 273, (uint8_t*)STR_GetString(STRID_SETTINGS_ALERTS), LEFT_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(38, 323, (uint8_t*)STR_GetString(STRID_SETTINGS_RESET), LEFT_MODE, &GUI_FontCairoSemiBold46);
}

/*******************************************************************************
* @brief  Display the mode list
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2019/07/31
*******************************************************************************/
void DPH_DisplayModeList(void)
{
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE1_Y_POS, (uint8_t*)STR_GetString(STRID_MODE), CENTER_MODE, &GUI_FontCairoSemiBold62);

    //Display the grey buttons
    HAL_ReadFlashData (GREY_BTN_ADD + RAM_OFFSET, GREY_BTN_SIZE, &LcdTempBuffer2[0]);
    BSP_LCD_DrawBitmap(25, 119, (uint8_t*)&LcdTempBuffer2);
    BSP_LCD_DrawBitmap(25, 169, (uint8_t*)&LcdTempBuffer2);

    //Display mode list sub-headers
    BSP_LCD_DisplaySpecificFontStringAt(38, 240, (uint8_t*)STR_GetString(STRID_MODE_DEROGATION), LEFT_MODE, &GUI_FontCairoSemiBold30);

    //Display the short grey buttons
    HAL_ReadFlashData (GREY_BTN_SHORT_ADD + RAM_OFFSET, GREY_BTN_SHORT_SIZE, &LcdTempBuffer2[0]);
    BSP_LCD_DrawBitmap(25, 269, (uint8_t*)&LcdTempBuffer2);
    BSP_LCD_DrawBitmap(25, 319, (uint8_t*)&LcdTempBuffer2);
    BSP_LCD_DrawBitmap(25, 369, (uint8_t*)&LcdTempBuffer2);
    BSP_LCD_DrawBitmap(25, 419, (uint8_t*)&LcdTempBuffer2);

    //Display the mode icons
    HAL_ReadFlashData (MANUAL_ICON_ADD + RAM_OFFSET, MANUAL_ICON_SIZE, &LcdTempBuffer[0]);
    BSP_LCD_BlendImages(41, 124, (uint8_t*)&LcdTempBuffer, 25, 119, (uint8_t*)&LcdTempBuffer2);
    HAL_ReadFlashData (SCHEDULE_ICON_ADD + RAM_OFFSET, SCHEDULE_ICON_SIZE, &LcdTempBuffer[0]);
    BSP_LCD_BlendImages(38, 174, (uint8_t*)&LcdTempBuffer, 25, 169, (uint8_t*)&LcdTempBuffer2);
    HAL_ReadFlashData (WAKE_ICON_ADD + RAM_OFFSET, WAKE_ICON_SIZE, &LcdTempBuffer[0]);
    BSP_LCD_BlendImages(40, 276, (uint8_t*)&LcdTempBuffer, 25, 269, (uint8_t*)&LcdTempBuffer2);
    HAL_ReadFlashData (LEAVE_ICON_ADD + RAM_OFFSET, LEAVE_ICON_SIZE, &LcdTempBuffer[0]);
    BSP_LCD_BlendImages(38, 326, (uint8_t*)&LcdTempBuffer, 25, 319, (uint8_t*)&LcdTempBuffer2);
    HAL_ReadFlashData (RETURN_ICON_ADD + RAM_OFFSET, RETURN_ICON_SIZE, &LcdTempBuffer[0]);
    BSP_LCD_BlendImages(38, 376, (uint8_t*)&LcdTempBuffer, 25, 369, (uint8_t*)&LcdTempBuffer2);
    HAL_ReadFlashData (SLEEP_ICON_ADD + RAM_OFFSET, SLEEP_ICON_SIZE, &LcdTempBuffer[0]);
    BSP_LCD_BlendImages(40, 426, (uint8_t*)&LcdTempBuffer, 25, 419, (uint8_t*)&LcdTempBuffer2);

    //Display the pen icons
    HAL_ReadFlashData (PEN_BIG_ADD + RAM_OFFSET, PEN_BIG_SIZE, &LcdTempBuffer2[0]);
    BSP_LCD_DrawBitmap(247, 269, (uint8_t*)&LcdTempBuffer2);
    BSP_LCD_DrawBitmap(247, 319, (uint8_t*)&LcdTempBuffer2);
    BSP_LCD_DrawBitmap(247, 369, (uint8_t*)&LcdTempBuffer2);
    BSP_LCD_DrawBitmap(247, 419, (uint8_t*)&LcdTempBuffer2);

    //Display the text
    BSP_LCD_DisplaySpecificFontStringAt(77, 123, (uint8_t*)STR_GetString(STRID_MODE_MANUAL), LEFT_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(80, 173, (uint8_t*)STR_GetString(STRID_MODE_SCHEDULE), LEFT_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(80, 273, (uint8_t*)STR_GetString(STRID_MODE_WAKE), LEFT_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(80, 323, (uint8_t*)STR_GetString(STRID_MODE_LEAVE), LEFT_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(80, 373, (uint8_t*)STR_GetString(STRID_MODE_RETURN), LEFT_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(80, 423, (uint8_t*)STR_GetString(STRID_MODE_SLEEP), LEFT_MODE, &GUI_FontCairoSemiBold46);
}

/*******************************************************************************
* @brief  Display the options
* @inputs Page: current option page to display
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/07/28
*******************************************************************************/
void DPH_DisplayOptions(void)
{
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE1_Y_POS, (uint8_t*)STR_GetString(STRID_OPTIONS), CENTER_MODE, &GUI_FontCairoSemiBold62);

    //Display the grey buttons
    HAL_ReadFlashData (GREY_BTN_ADD + RAM_OFFSET, GREY_BTN_SIZE, &LcdTempBuffer[0]);
    BSP_LCD_DrawBitmap(25, 119, (uint8_t*)&LcdTempBuffer);
    BSP_LCD_DrawBitmap(25, 169, (uint8_t*)&LcdTempBuffer);
    BSP_LCD_DrawBitmap(25, 219, (uint8_t*)&LcdTempBuffer);
    BSP_LCD_DrawBitmap(25, 269, (uint8_t*)&LcdTempBuffer);
    BSP_LCD_DrawBitmap(25, 319, (uint8_t*)&LcdTempBuffer);
    BSP_LCD_DrawBitmap(25, 369, (uint8_t*)&LcdTempBuffer);

    BSP_LCD_DisplaySpecificFontStringAt(38, 123, (uint8_t*)STR_GetString(STRID_OPTIONS_LANGUAGE), LEFT_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(38, 173, (uint8_t*)STR_GetString(STRID_OPTIONS_UNIT_FORMAT), LEFT_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(38, 223, (uint8_t*)STR_GetString(STRID_OPTIONS_TIME_FORMAT), LEFT_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(38, 273, (uint8_t*)STR_GetString(STRID_OPTIONS_CONTROL_MODE), LEFT_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(38, 323, (uint8_t*)STR_GetString(STRID_OPTIONS_LOCK), LEFT_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(38, 373, (uint8_t*)STR_GetString(STRID_OPTIONS_BACKLIGHT), LEFT_MODE, &GUI_FontCairoSemiBold46);
}

/*******************************************************************************
* @brief  Display the control mode list
* @inputs mode: current selected control mode
* @retval None
* @author Jean-Fran�ois Many
* @date   2019/04/08
*******************************************************************************/
void DPH_DisplayControlMode(ControlMode_t mode, uint8_t firstEntry)
{
    if (firstEntry)
    {
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE1_Y_POS, (uint8_t*)STR_GetString(STRID_CONTROL_MODE_1), CENTER_MODE, &GUI_FontCairoSemiBold62);
        BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE2_Y_POS, (uint8_t*)STR_GetString(STRID_CONTROL_MODE_2), CENTER_MODE, &GUI_FontCairoSemiBold62);

        //Display Apply button
        HAL_ReadFlashData (BLUE_BTN_ADD + RAM_OFFSET, BLUE_BTN_SIZE, &LcdTempBuffer[0]);
        BSP_LCD_DrawBitmap(25, 420, (uint8_t*)&LcdTempBuffer);
        BSP_LCD_DisplaySpecificFontStringAt(0, 424, (uint8_t*)STR_GetString(STRID_APPLY), CENTER_MODE, &GUI_FontCairoSemiBold46);

        //Display grey buttons
        HAL_ReadFlashData (GREY_BTN_ADD + RAM_OFFSET, GREY_BTN_SIZE, &LcdTempBuffer2[0]);
        BSP_LCD_DrawBitmap(25, 119, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_DrawBitmap(25, 169, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_DrawBitmap(25, 219, (uint8_t*)&LcdTempBuffer2);

        //Display menu choices
        BSP_LCD_DisplaySpecificFontStringAt(38, 123, (uint8_t*)STR_GetString(STRID_CONTROL_MODE_AMBIENT), LEFT_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(38, 173, (uint8_t*)STR_GetString(STRID_CONTROL_MODE_FLOOR), LEFT_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(38, 223, (uint8_t*)STR_GetString(STRID_CONTROL_MODE_AMBIENTFLOOR), LEFT_MODE, &GUI_FontCairoSemiBold46);
    }

    //Display checkmark
    HAL_ReadFlashData (CHECK_ICON_ADD + RAM_OFFSET, CHECK_ICON_SIZE, &LcdTempBuffer[0]);
    if (mode == CONTROLMODE_AMBIENT)
    {
        BSP_LCD_BlendImages(256, 123, (uint8_t*)&LcdTempBuffer, 25, 119, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 173, (uint8_t*)&LcdTempBuffer, 25, 169, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 223, (uint8_t*)&LcdTempBuffer, 25, 219, (uint8_t*)&LcdTempBuffer2);

        //Hide the Floor Type button
        BSP_LCD_ClearStringLineSection(25, 269, grey_btn.width, grey_btn.height);
        //Hide the Floor Limit button
        BSP_LCD_ClearStringLineSection(25, 319, grey_btn.width, grey_btn.height);

    }
    else if (mode == CONTROLMODE_FLOOR)
    {
        BSP_LCD_BlendImages(256, 173, (uint8_t*)&LcdTempBuffer, 25, 169, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 123, (uint8_t*)&LcdTempBuffer, 25, 119, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 223, (uint8_t*)&LcdTempBuffer, 25, 219, (uint8_t*)&LcdTempBuffer2);

        //Display grey button for Floor Type
        HAL_ReadFlashData (GREY_BTN_ADD + RAM_OFFSET, GREY_BTN_SIZE, &LcdTempBuffer2[0]);
        BSP_LCD_DrawBitmap(25, 269, (uint8_t*)&LcdTempBuffer2);

        //Display Floor Type choice
        BSP_LCD_DisplaySpecificFontStringAt(38, 273, (uint8_t*)STR_GetString(STRID_CONTROL_MODE_FLOOR_TYPE), LEFT_MODE, &GUI_FontCairoSemiBold46);

        //Hide the Floor Limit button
        BSP_LCD_ClearStringLineSection(25, 319, grey_btn.width, grey_btn.height);
    }
    else if (mode == CONTROLMODE_AMBIENTFLOOR)
    {
        BSP_LCD_BlendImages(256, 223, (uint8_t*)&LcdTempBuffer, 25, 219, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 123, (uint8_t*)&LcdTempBuffer, 25, 119, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 173, (uint8_t*)&LcdTempBuffer, 25, 169, (uint8_t*)&LcdTempBuffer2);

        //Display grey button for Floor Type
        HAL_ReadFlashData (GREY_BTN_ADD + RAM_OFFSET, GREY_BTN_SIZE, &LcdTempBuffer2[0]);
        BSP_LCD_DrawBitmap(25, 269, (uint8_t*)&LcdTempBuffer2);

        //Display Floor Type choice
        BSP_LCD_DisplaySpecificFontStringAt(38, 273, (uint8_t*)STR_GetString(STRID_CONTROL_MODE_FLOOR_TYPE), LEFT_MODE, &GUI_FontCairoSemiBold46);

        //Display grey button for Floor Type
        HAL_ReadFlashData (GREY_BTN_ADD + RAM_OFFSET, GREY_BTN_SIZE, &LcdTempBuffer2[0]);
        BSP_LCD_DrawBitmap(25, 319, (uint8_t*)&LcdTempBuffer2);

        //Display Floor Limit choice
        BSP_LCD_DisplaySpecificFontStringAt(38, 323, (uint8_t*)STR_GetString(STRID_CONTROL_MODE_FLOOR_LIMIT), LEFT_MODE, &GUI_FontCairoSemiBold46);
    }
    else
    {
        ;
    }
}

/*******************************************************************************
* @brief  Display the lock state and setpoint delta
* @inputs lock: current lock state
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/07/29
*******************************************************************************/
void DPH_DisplayLockSettings(Lock_t lock, uint8_t firstEntry)
{
    if (firstEntry)
    {
        //Display menu header
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE1_Y_POS, (uint8_t*)STR_GetString(STRID_LOCK_1), CENTER_MODE, &GUI_FontCairoSemiBold62);
        BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE2_Y_POS, (uint8_t*)STR_GetString(STRID_LOCK_2), CENTER_MODE, &GUI_FontCairoSemiBold62);

        //Display Apply button
        HAL_ReadFlashData (BLUE_BTN_ADD + RAM_OFFSET, BLUE_BTN_SIZE, &LcdTempBuffer[0]);
        BSP_LCD_DrawBitmap(25, 420, (uint8_t*)&LcdTempBuffer);
        BSP_LCD_DisplaySpecificFontStringAt(0, 424, (uint8_t*)STR_GetString(STRID_APPLY), CENTER_MODE, &GUI_FontCairoSemiBold46);

        //Display grey buttons
        HAL_ReadFlashData (GREY_BTN_ADD + RAM_OFFSET, GREY_BTN_SIZE, &LcdTempBuffer2[0]);
        BSP_LCD_DrawBitmap(25, 119, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_DrawBitmap(25, 169, (uint8_t*)&LcdTempBuffer2);

        //Display menu choices
        BSP_LCD_DisplaySpecificFontStringAt(38, 123, (uint8_t*)STR_GetString(STRID_LOCK_UNLOCK), LEFT_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(38, 173, (uint8_t*)STR_GetString(STRID_LOCK_LOCK), LEFT_MODE, &GUI_FontCairoSemiBold46);

        //Display advise box
        HAL_ReadFlashData (ADVISE_BOX_ADD + RAM_OFFSET, ADVISE_BOX_SIZE, &LcdTempBuffer[0]);
        BSP_LCD_DrawBitmap(24,255,(uint8_t*)&LcdTempBuffer);
        BSP_LCD_DisplaySpecificFontStringAt(0, 218, (uint8_t*)STR_GetString(STRID_LOCK_ADVISE), CENTER_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(0, 268, (uint8_t*)STR_GetString(STRID_LOCK_ADVISE1), CENTER_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(0, 298, (uint8_t*)STR_GetString(STRID_LOCK_ADVISE2), CENTER_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(0, 328, (uint8_t*)STR_GetString(STRID_LOCK_ADVISE3), CENTER_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(0, 358, (uint8_t*)STR_GetString(STRID_LOCK_ADVISE4), CENTER_MODE, &GUI_FontCairoSemiBold46);

        //Display the settings button in the advice box
        HAL_ReadFlashData (SETTINGS_BTN_ADD + RAM_OFFSET, SETTINGS_BTN_SIZE, &LcdTempBuffer[0]);
        BSP_LCD_DrawBitmap(250,331,(uint8_t*)&LcdTempBuffer);
    }

    //Display checkmark
    HAL_ReadFlashData (CHECK_ICON_ADD + RAM_OFFSET, CHECK_ICON_SIZE, &LcdTempBuffer[0]);
    if (lock == LOCK_OFF)
    {
        BSP_LCD_BlendImages(256, 123, (uint8_t*)&LcdTempBuffer, 25, 119, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 173, (uint8_t*)&LcdTempBuffer, 25, 169, (uint8_t*)&LcdTempBuffer2);
    }
    else if (lock == LOCK_ON)
    {
        BSP_LCD_BlendImages(256, 173, (uint8_t*)&LcdTempBuffer, 25, 169, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 123, (uint8_t*)&LcdTempBuffer, 25, 119, (uint8_t*)&LcdTempBuffer2);
    }
    else
    {
        ;
    }
}

/*******************************************************************************
* @brief  Display the backlight operation mode
* @inputs bl_mode: current backlight operation mode
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/08/02
*******************************************************************************/
void DPH_DisplayBacklight(BacklightTimeout_t bl_mode, uint8_t firstEntry)
{
    if (firstEntry)
    {
        //Display menu header
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_DisplaySpecificFontStringAt(10, TITLE_LINE1_Y_POS, (uint8_t*)STR_GetString(STRID_BACKLIGHT1), CENTER_MODE, &GUI_FontCairoSemiBold62);
        BSP_LCD_DisplaySpecificFontStringAt(10, TITLE_LINE2_Y_POS, (uint8_t*)STR_GetString(STRID_BACKLIGHT2), CENTER_MODE, &GUI_FontCairoSemiBold62);

        //Display Apply button
        HAL_ReadFlashData (BLUE_BTN_ADD + RAM_OFFSET, BLUE_BTN_SIZE, &LcdTempBuffer[0]);
        BSP_LCD_DrawBitmap(25, 420, (uint8_t*)&LcdTempBuffer);
        BSP_LCD_DisplaySpecificFontStringAt(0, 424, (uint8_t*)STR_GetString(STRID_APPLY), CENTER_MODE, &GUI_FontCairoSemiBold46);

        //Display grey buttons
        HAL_ReadFlashData (GREY_BTN_ADD + RAM_OFFSET, GREY_BTN_SIZE, &LcdTempBuffer2[0]);
        BSP_LCD_DrawBitmap(25, 119, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_DrawBitmap(25, 169, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_DrawBitmap(25, 219, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_DrawBitmap(25, 269, (uint8_t*)&LcdTempBuffer2);

        //Display menu choices
        BSP_LCD_DisplaySpecificFontStringAt(38, 123, (uint8_t*)STR_GetString(STRID_BACKLIGHT_60_SEC), LEFT_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(38, 173, (uint8_t*)STR_GetString(STRID_BACKLIGHT_300_SEC), LEFT_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(38, 223, (uint8_t*)STR_GetString(STRID_BACKLIGHT_600_SEC), LEFT_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(38, 273, (uint8_t*)STR_GetString(STRID_BACKLIGHT_1800_SEC), LEFT_MODE, &GUI_FontCairoSemiBold46);
    }

    //Display checkmark
    HAL_ReadFlashData (CHECK_ICON_ADD + RAM_OFFSET, CHECK_ICON_SIZE, &LcdTempBuffer[0]);
    if (bl_mode == BL_60_SEC)
    {
        BSP_LCD_BlendImages(256, 123, (uint8_t*)&LcdTempBuffer, 25, 119, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 173, (uint8_t*)&LcdTempBuffer, 25, 169, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 223, (uint8_t*)&LcdTempBuffer, 25, 219, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 273, (uint8_t*)&LcdTempBuffer, 25, 269, (uint8_t*)&LcdTempBuffer2);
    }
    else if (bl_mode == BL_300_SEC)
    {
        BSP_LCD_BlendImages(256, 173, (uint8_t*)&LcdTempBuffer, 25, 169, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 123, (uint8_t*)&LcdTempBuffer, 25, 119, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 223, (uint8_t*)&LcdTempBuffer, 25, 219, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 273, (uint8_t*)&LcdTempBuffer, 25, 269, (uint8_t*)&LcdTempBuffer2);
    }
    else if (bl_mode == BL_600_SEC)
    {
        BSP_LCD_BlendImages(256, 223, (uint8_t*)&LcdTempBuffer, 25, 219, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 123, (uint8_t*)&LcdTempBuffer, 25, 119, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 173, (uint8_t*)&LcdTempBuffer, 25, 169, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 273, (uint8_t*)&LcdTempBuffer, 25, 269, (uint8_t*)&LcdTempBuffer2);
    }
    else if (bl_mode == BL_1800_SEC)
    {
        BSP_LCD_BlendImages(256, 273, (uint8_t*)&LcdTempBuffer, 25, 269, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 123, (uint8_t*)&LcdTempBuffer, 25, 119, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 173, (uint8_t*)&LcdTempBuffer, 25, 169, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 223, (uint8_t*)&LcdTempBuffer, 25, 219, (uint8_t*)&LcdTempBuffer2);
    }
    else
    {
        ;
    }
}

/*******************************************************************************
* @brief  Display the language
* @inputs lang: current display language
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/08/01
*******************************************************************************/
void DPH_DisplayLanguage(DisplayLang_t lang)
{
    //Display menu header
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    BSP_LCD_ClearStringLineSection(40, TITLE_LINE1_Y_POS, 280, 46);
    if (lang == LANG_FRANCAIS)
    {
        BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE1_Y_POS, (uint8_t*)STR_GetString(STRID_LANGUAGE_TITLE_FR), CENTER_MODE, &GUI_FontCairoSemiBold62);
    }
    else
    {
        BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE1_Y_POS, (uint8_t*)STR_GetString(STRID_LANGUAGE_TITLE_EN), CENTER_MODE, &GUI_FontCairoSemiBold62);
    }

    //Display Apply button
    HAL_ReadFlashData (BLUE_BTN_ADD + RAM_OFFSET, BLUE_BTN_SIZE, &LcdTempBuffer[0]);
    BSP_LCD_DrawBitmap(25, 420, (uint8_t*)&LcdTempBuffer);
    if (lang == LANG_FRANCAIS)
    {
        BSP_LCD_DisplaySpecificFontStringAt(0, 424, (uint8_t*)STR_GetString(STRID_LANGUAGE_CONFIRM_FR), CENTER_MODE, &GUI_FontCairoSemiBold46);
    }
    else
    {
        BSP_LCD_DisplaySpecificFontStringAt(0, 424, (uint8_t*)STR_GetString(STRID_LANGUAGE_CONFIRM_EN), CENTER_MODE, &GUI_FontCairoSemiBold46);
    }

    //Display grey buttons
    HAL_ReadFlashData (GREY_BTN_ADD + RAM_OFFSET, GREY_BTN_SIZE, &LcdTempBuffer2[0]);
    BSP_LCD_DrawBitmap(25, 119, (uint8_t*)&LcdTempBuffer2);
    BSP_LCD_DrawBitmap(25, 169, (uint8_t*)&LcdTempBuffer2);

    //Display menu choices
    BSP_LCD_DisplaySpecificFontStringAt(38, 123, (uint8_t*)STR_GetString(STRID_LANGUAGE_FRANCAIS), LEFT_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(38, 173, (uint8_t*)STR_GetString(STRID_LANGUAGE_ENGLISH), LEFT_MODE, &GUI_FontCairoSemiBold46);

    //Display checkmark
    HAL_ReadFlashData (CHECK_ICON_ADD + RAM_OFFSET, CHECK_ICON_SIZE, &LcdTempBuffer[0]);
    if (lang == LANG_FRANCAIS)
    {
        BSP_LCD_BlendImages(256, 123, (uint8_t*)&LcdTempBuffer, 25, 119, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 173, (uint8_t*)&LcdTempBuffer, 25, 169, (uint8_t*)&LcdTempBuffer2);
    }
    else if (lang == LANG_ENGLISH)
    {
        BSP_LCD_BlendImages(256, 173, (uint8_t*)&LcdTempBuffer, 25, 169, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 123, (uint8_t*)&LcdTempBuffer, 25, 119, (uint8_t*)&LcdTempBuffer2);
    }
    else
    {
        ;
    }
}

/*******************************************************************************
* @brief  Display the unit format
* @inputs format: current unit format
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/08/01
*******************************************************************************/
void DPH_DisplayUnitFormat(UnitFormat_t format, uint8_t firstEntry)
{
    if (firstEntry)
    {
        //Display menu header
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE1_Y_POS, (uint8_t*)STR_GetString(STRID_TEMP_FORMAT_1), CENTER_MODE, &GUI_FontCairoSemiBold62);
        BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE2_Y_POS, (uint8_t*)STR_GetString(STRID_TEMP_FORMAT_2), CENTER_MODE, &GUI_FontCairoSemiBold62);

        //Display Apply button
        HAL_ReadFlashData (BLUE_BTN_ADD + RAM_OFFSET, BLUE_BTN_SIZE, &LcdTempBuffer[0]);
        BSP_LCD_DrawBitmap(25, 420, (uint8_t*)&LcdTempBuffer);
        BSP_LCD_DisplaySpecificFontStringAt(0, 424, (uint8_t*)STR_GetString(STRID_APPLY), CENTER_MODE, &GUI_FontCairoSemiBold46);

        //Display grey buttons
        HAL_ReadFlashData (GREY_BTN_ADD + RAM_OFFSET, GREY_BTN_SIZE, &LcdTempBuffer2[0]);
        BSP_LCD_DrawBitmap(25, 119, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_DrawBitmap(25, 169, (uint8_t*)&LcdTempBuffer2);

        //Display menu choices
        BSP_LCD_DisplaySpecificFontStringAt(38, 123, (uint8_t*)STR_GetString(STRID_TEMP_FORMAT_CELSIUS), LEFT_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(38, 173, (uint8_t*)STR_GetString(STRID_TEMP_FORMAT_FAHRENHEIT), LEFT_MODE, &GUI_FontCairoSemiBold46);
    }

    //Display checkmark
    HAL_ReadFlashData (CHECK_ICON_ADD + RAM_OFFSET, CHECK_ICON_SIZE, &LcdTempBuffer[0]);
    if (format == DEGREE_C)
    {
        BSP_LCD_BlendImages(256, 123, (uint8_t*)&LcdTempBuffer, 25, 119, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 173, (uint8_t*)&LcdTempBuffer, 25, 169, (uint8_t*)&LcdTempBuffer2);
    }
    else if (format == DEGREE_F)
    {
        BSP_LCD_BlendImages(256, 173, (uint8_t*)&LcdTempBuffer, 25, 169, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 123, (uint8_t*)&LcdTempBuffer, 25, 119, (uint8_t*)&LcdTempBuffer2);
    }
    else
    {
        ;
    }
}

/*******************************************************************************
* @brief  Display the time format
* @inputs format: current time format
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/08/01
*******************************************************************************/
void DPH_DisplayTimeFormat(TimeFormat_t format, uint8_t dst, uint8_t firstEntry)
{
    if (firstEntry)
    {
        //Display menu header
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE1_Y_POS, (uint8_t*)STR_GetString(STRID_TIME_FORMAT_1), CENTER_MODE, &GUI_FontCairoSemiBold62);
        BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE2_Y_POS, (uint8_t*)STR_GetString(STRID_TIME_FORMAT_2), CENTER_MODE, &GUI_FontCairoSemiBold62);

        //Display time format sub-headers
        BSP_LCD_DisplaySpecificFontStringAt(38, 103, (uint8_t*)STR_GetString(STRID_TIME_FORMAT), LEFT_MODE, &GUI_FontCairoSemiBold30);
        BSP_LCD_DisplaySpecificFontStringAt(38, 253, (uint8_t*)STR_GetString(STRID_TIME_FORMAT_DST), LEFT_MODE, &GUI_FontCairoSemiBold30);

        //Display Apply button
        HAL_ReadFlashData (BLUE_BTN_ADD + RAM_OFFSET, BLUE_BTN_SIZE, &LcdTempBuffer[0]);
        BSP_LCD_DrawBitmap(25, 420, (uint8_t*)&LcdTempBuffer);
        BSP_LCD_DisplaySpecificFontStringAt(0, 424, (uint8_t*)STR_GetString(STRID_APPLY), CENTER_MODE, &GUI_FontCairoSemiBold46);

        //Display grey buttons
        HAL_ReadFlashData (GREY_BTN_ADD + RAM_OFFSET, GREY_BTN_SIZE, &LcdTempBuffer2[0]);
        BSP_LCD_DrawBitmap(25, 124, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_DrawBitmap(25, 174, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_DrawBitmap(25, 274, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_DrawBitmap(25, 324, (uint8_t*)&LcdTempBuffer2);

        //Display menu choices
        BSP_LCD_DisplaySpecificFontStringAt(38, 128, (uint8_t*)STR_GetString(STRID_TIME_FORMAT_24_HOURS), LEFT_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(38, 178, (uint8_t*)STR_GetString(STRID_TIME_FORMAT_12_HOURS), LEFT_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(38, 278, (uint8_t*)STR_GetString(STRID_TIME_FORMAT_DST_ON), LEFT_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(38, 328, (uint8_t*)STR_GetString(STRID_TIME_FORMAT_DST_OFF), LEFT_MODE, &GUI_FontCairoSemiBold46);
    }

    //Display checkmark
    HAL_ReadFlashData (CHECK_ICON_ADD + RAM_OFFSET, CHECK_ICON_SIZE, &LcdTempBuffer[0]);
    if (format == TIME_24H)
    {
        BSP_LCD_BlendImages(256, 128, (uint8_t*)&LcdTempBuffer, 25, 124, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 178, (uint8_t*)&LcdTempBuffer, 25, 174, (uint8_t*)&LcdTempBuffer2);
    }
    else if (format == TIME_12H)
    {
        BSP_LCD_BlendImages(256, 178, (uint8_t*)&LcdTempBuffer, 25, 174, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 128, (uint8_t*)&LcdTempBuffer, 25, 124, (uint8_t*)&LcdTempBuffer2);
    }
    else
    {
        ;
    }
    if (dst == 1)
    {
        BSP_LCD_BlendImages(256, 278, (uint8_t*)&LcdTempBuffer, 25, 274, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 328, (uint8_t*)&LcdTempBuffer, 25, 324, (uint8_t*)&LcdTempBuffer2);
    }
    else if (dst == 0)
    {
        BSP_LCD_BlendImages(256, 328, (uint8_t*)&LcdTempBuffer, 25, 324, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 278, (uint8_t*)&LcdTempBuffer, 25, 274, (uint8_t*)&LcdTempBuffer2);
    }
    else
    {
        BSP_LCD_HideImage(256, 278, (uint8_t*)&LcdTempBuffer, 25, 274, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 328, (uint8_t*)&LcdTempBuffer, 25, 324, (uint8_t*)&LcdTempBuffer2);
    }
}

/*******************************************************************************
* @brief  Display the floor type
* @inputs format: current floor type
* @retval None
* @author Jean-Fran�ois Many
* @date   2019/07/16
*******************************************************************************/
void DPH_DisplayFloorType(FloorType_t type, uint8_t firstEntry)
{
    if (firstEntry)
    {
        //Display menu header
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE1_Y_POS, (uint8_t*)STR_GetString(STRID_FLOOR_TYPE_1), CENTER_MODE, &GUI_FontCairoSemiBold62);
        BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE2_Y_POS, (uint8_t*)STR_GetString(STRID_FLOOR_TYPE_2), CENTER_MODE, &GUI_FontCairoSemiBold62);

        //Display Apply button
        HAL_ReadFlashData (BLUE_BTN_ADD + RAM_OFFSET, BLUE_BTN_SIZE, &LcdTempBuffer[0]);
        BSP_LCD_DrawBitmap(25, 420, (uint8_t*)&LcdTempBuffer);
        BSP_LCD_DisplaySpecificFontStringAt(0, 424, (uint8_t*)STR_GetString(STRID_APPLY), CENTER_MODE, &GUI_FontCairoSemiBold46);

        //Display grey buttons
        HAL_ReadFlashData (GREY_BTN_ADD + RAM_OFFSET, GREY_BTN_SIZE, &LcdTempBuffer2[0]);
        BSP_LCD_DrawBitmap(25, 119, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_DrawBitmap(25, 169, (uint8_t*)&LcdTempBuffer2);

        //Display menu choices
        BSP_LCD_DisplaySpecificFontStringAt(38, 123, (uint8_t*)STR_GetString(STRID_FLOOR_TYPE_TILE), LEFT_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(38, 173, (uint8_t*)STR_GetString(STRID_FLOOR_TYPE_ENGINEERED), LEFT_MODE, &GUI_FontCairoSemiBold46);

        //Display advise box
        HAL_ReadFlashData (ADVISE_BOX_ADD + RAM_OFFSET, ADVISE_BOX_SIZE, &LcdTempBuffer[0]);
        BSP_LCD_DrawBitmap(24,255,(uint8_t*)&LcdTempBuffer);
        BSP_LCD_DisplaySpecificFontStringAt(0, 218, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_ADVISE), CENTER_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(0, 268, (uint8_t*)STR_GetString(STRID_FLOOR_TYPE_ADVISE1), CENTER_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(0, 298, (uint8_t*)STR_GetString(STRID_FLOOR_TYPE_ADVISE2), CENTER_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(0, 328, (uint8_t*)STR_GetString(STRID_FLOOR_TYPE_ADVISE3), CENTER_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(0, 358, (uint8_t*)STR_GetString(STRID_FLOOR_TYPE_ADVISE4), CENTER_MODE, &GUI_FontCairoSemiBold46);
    }

    //Display checkmark
    HAL_ReadFlashData (CHECK_ICON_ADD + RAM_OFFSET, CHECK_ICON_SIZE, &LcdTempBuffer[0]);
    if (type == FLOORTYPE_TILE)
    {
        BSP_LCD_BlendImages(256, 123, (uint8_t*)&LcdTempBuffer, 25, 119, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 173, (uint8_t*)&LcdTempBuffer, 25, 169, (uint8_t*)&LcdTempBuffer2);
    }
    else if (type == FLOORTYPE_ENGINEERED)
    {
        BSP_LCD_BlendImages(256, 173, (uint8_t*)&LcdTempBuffer, 25, 169, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_HideImage(256, 123, (uint8_t*)&LcdTempBuffer, 25, 119, (uint8_t*)&LcdTempBuffer2);
    }
    else
    {
        ;
    }
}

/*******************************************************************************
* @brief  Display the reset to default menu
* @inputs reset: reset intention
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/08/02
*******************************************************************************/
void DPH_DisplayReset(Reset_t reset)
{
    //Display menu header
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE1_Y_POS, (uint8_t*)STR_GetString(STRID_RESET), CENTER_MODE, &GUI_FontCairoSemiBold62);

    //Display advise box
    HAL_ReadFlashData (ADVISE_BOX_ADD + RAM_OFFSET, ADVISE_BOX_SIZE, &LcdTempBuffer[0]);
    BSP_LCD_DrawBitmap(24,250,(uint8_t*)&LcdTempBuffer);
    BSP_LCD_DisplaySpecificFontStringAt(0, 213, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_ADVISE), CENTER_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(0, 263, (uint8_t*)STR_GetString(STRID_RESET_ADVISE1), CENTER_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(0, 293, (uint8_t*)STR_GetString(STRID_RESET_ADVISE2), CENTER_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(0, 323, (uint8_t*)STR_GetString(STRID_RESET_ADVISE3), CENTER_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(0, 353, (uint8_t*)STR_GetString(STRID_RESET_ADVISE4), CENTER_MODE, &GUI_FontCairoSemiBold46);

    BSP_LCD_ClearStringLineSection(0, 116, 320, 46);
    switch (reset)
    {
        case NO_RESET:
            BSP_LCD_DisplaySpecificFontStringAt(38, 123, (uint8_t*)STR_GetString(STRID_RESET_RESET_TO_DEFAULT), LEFT_MODE, &GUI_FontCairoSemiBold46);
            //Display blue button
            HAL_ReadFlashData (BLUE_BTN_ADD + RAM_OFFSET, BLUE_BTN_SIZE, &LcdTempBuffer[0]);
            BSP_LCD_DrawBitmap(25, 420, (uint8_t*)&LcdTempBuffer);
            BSP_LCD_DisplaySpecificFontStringAt(0, 424, (uint8_t*)STR_GetString(STRID_YES), CENTER_MODE, &GUI_FontCairoSemiBold46);
            break;

        case RESET_WANTED:
            BSP_LCD_DisplaySpecificFontStringAt(38, 123, (uint8_t*)STR_GetString(STRID_RESET_CONFIRMATION_PROMPT), LEFT_MODE, &GUI_FontCairoSemiBold46);
            //Display blue button
            HAL_ReadFlashData (BLUE_BTN_ADD + RAM_OFFSET, BLUE_BTN_SIZE, &LcdTempBuffer[0]);
            BSP_LCD_DrawBitmap(25, 420, (uint8_t*)&LcdTempBuffer);
            BSP_LCD_DisplaySpecificFontStringAt(0, 424, (uint8_t*)STR_GetString(STRID_RESET_CONFIRMATION), CENTER_MODE, &GUI_FontCairoSemiBold46);
            break;

        case RESET_CONFIRMED:
            BSP_LCD_DisplaySpecificFontStringAt(38, 123, (uint8_t*)STR_GetString(STRID_RESET_IN_PROGRESS_1), LEFT_MODE, &GUI_FontCairoSemiBold46);
            BSP_LCD_ClearStringLineSection(25, 169, blue_btn.width, blue_btn.height);
            BSP_LCD_ClearStringLineSection(24, 24, back_icon.width, back_icon.height);
            break;

        default:
            break;
    }
}

/*******************************************************************************
* @brief  Display the information menu
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/08/03
*******************************************************************************/
void DPH_DisplayInfo(void)
{
    uint8_t string_ptr[33];
    uint8_t stringNum_ptr[8];
    uint16_t ctrl_ver;
    static uint8_t checksum_control;

    DBTHRD_GetData(DBTYPE_CONTROLLER_VERSION,(void*)&ctrl_ver, INDEX_DONT_CARE);
    checksum_control = TMP_GetCompensationChecksum();

    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE1_Y_POS, (uint8_t*)STR_GetString(STRID_INFO), CENTER_MODE, &GUI_FontCairoSemiBold62);

    BSP_LCD_SetTextColor(LCD_COLOR_ORANGE);
    STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_INFO_TSTAT_VERSION));
    BSP_LCD_DisplaySpecificFontStringAt(38, 70, string_ptr, LEFT_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    STR_StrItoA(stringNum_ptr, ctrl_ver/1000);
    STR_StrCpy(string_ptr, stringNum_ptr);
    STR_StrItoA(stringNum_ptr, (ctrl_ver%1000)/100);
    STR_StrCat(string_ptr, stringNum_ptr);
    STR_StrCat(string_ptr, ".");
    STR_StrItoA(stringNum_ptr, (ctrl_ver%100)/10);
    STR_StrCat(string_ptr, stringNum_ptr);
    STR_StrItoA(stringNum_ptr, (ctrl_ver%10));
    STR_StrCat(string_ptr, stringNum_ptr);
    STR_StrCat(string_ptr, BUILD_INFO);
    BSP_LCD_DisplaySpecificFontStringAt(38, 100, string_ptr, LEFT_MODE, &GUI_FontCairoSemiBold46);

//ProductSerialNumber = 0x22240001;
    BSP_LCD_SetTextColor(LCD_COLOR_ORANGE);
    STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_INFO_PRODUCED_ON));
    BSP_LCD_DisplaySpecificFontStringAt(38, 310, string_ptr, LEFT_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    if (ProductSerialNumber != 0xFFFFFFFF)
    {
        STR_StrItoA(stringNum_ptr, (ProductSerialNumber >> 25)+2000);
        STR_StrCpy(string_ptr, stringNum_ptr);
        STR_StrCat(string_ptr, "/");
        STR_StrItoA(stringNum_ptr, ((ProductSerialNumber >> 21) & 0x0F));
        STR_StrCat(string_ptr, stringNum_ptr);
        STR_StrCat(string_ptr, "/");
        STR_StrItoA(stringNum_ptr, ((ProductSerialNumber >> 16) & 0x1F));
        STR_StrCat(string_ptr, stringNum_ptr);
        BSP_LCD_DisplaySpecificFontStringAt(38, 340, string_ptr, LEFT_MODE, &GUI_FontCairoSemiBold46);
    }

    BSP_LCD_SetTextColor(LCD_COLOR_ORANGE);
    STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_INFO_CHECKSUM));
    BSP_LCD_DisplaySpecificFontStringAt(38, 390, string_ptr, LEFT_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_INFO_CHECKSUM_SHORT));
    STR_StrItoA(stringNum_ptr, checksum_control);
    STR_StrCat(string_ptr, stringNum_ptr);
    BSP_LCD_DisplaySpecificFontStringAt(38, 420, string_ptr, LEFT_MODE, &GUI_FontCairoSemiBold46);
}

/*******************************************************************************
* @brief  Display the active alerts
* @inputs Page: Current alert page to be displayed
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/08/03
*******************************************************************************/
void DPH_DisplayAlerts(U8BIT Page)
{
    U8BIT activeAlertNumber;
    uint8_t string_ptr[33];
    uint8_t tstat;
    uint8_t index;
    uint8_t i;
    dbType_Alerts_t activeAlerts;
    dbType_AlertDisplay_t alertToDisplay[100];

    #define ALERTZONE_YPOS_OFFSET           50
    #define ALERTZONE_YPOS_START            69
    #define ALERTTEXT_YPOS_OFFSET           50
    #define ALERTTEXT_YPOS_START            73

    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE1_Y_POS, (uint8_t*)STR_GetString(STRID_ALERTS), CENTER_MODE, &GUI_FontCairoSemiBold62);

    activeAlertNumber = 0;
    for (tstat = 0; tstat < THERMOSTAT_INSTANCES; tstat++)
    {
        DBTHRD_GetData(DBTYPE_ACTIVE_ALERTS, &activeAlerts, tstat);
        for (index = 0; index < ALERT_INSTANCES; index++)
        {
            if (activeAlerts.ActiveAlerts[index] != 0)
            {
                alertToDisplay[activeAlertNumber].tstat = tstat;
                alertToDisplay[activeAlertNumber].alertId = activeAlerts.ActiveAlerts[index];
                activeAlertNumber++;
            }
        }
    }

    if (activeAlertNumber == 0)
    {
        BSP_LCD_DisplaySpecificFontStringAt(0, 107, (uint8_t*)STR_GetString(STRID_ALERTS_GOOD_NEWS), CENTER_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(0, 152, (uint8_t*)STR_GetString(STRID_ALERTS_NO_ALERT), CENTER_MODE, &GUI_FontCairoSemiBold46);
    }
    else
    {
        HAL_ReadFlashData (BLUE_SQUARE_BTN_ADD + RAM_OFFSET, BLUE_SQUARE_BTN_SIZE, &LcdTempBuffer[0]);
        activeAlertNumber = activeAlertNumber - Page * 7;
        if (activeAlertNumber > 7)
        {
            activeAlertNumber = 7;
            BSP_LCD_DrawBitmap(252, 419, (uint8_t*)&LcdTempBuffer);
            BSP_LCD_DisplaySpecificFontStringAt(38, 422, (uint8_t*)STR_GetString(STRID_NEXT), RIGHT_MODE, &GUI_FontCairoSemiBold46);
        }
        if (Page != 0)
        {
            BSP_LCD_DrawBitmap(25, 419, (uint8_t*)&LcdTempBuffer);
            BSP_LCD_DisplaySpecificFontStringAt(38, 422, (uint8_t*)STR_GetString(STRID_PREVIOUS), LEFT_MODE, &GUI_FontCairoSemiBold46);
        }

        HAL_ReadFlashData (GREY_BTN_ADD + RAM_OFFSET, GREY_BTN_SIZE, &LcdTempBuffer[0]);
        for (i = 0; i < activeAlertNumber; i++)
        {
            switch (alertToDisplay[Page * 7 + i].alertId)
            {
                case DBALERT_NO_ZIGBEE:
                    STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_ALERTS_NO_ZIGBEE));
                    break;

                case DBALERT_OPEN_WINDOW:
                    STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_ALERTS_OPEN_WINDOW));
                    break;

                case DBALERT_OVERHEAT:
                    STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_ALERTS_OVERHEAT));
                    break;

                case DBALERT_GFCI:
                    STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_ALERTS_GFCI));
                    break;

                case DBALERT_NO_WIFI:
                    STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_ALERTS_NO_WIFI));
                    break;

                case DBALERT_OTA_PENDING:
                    STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_ALERTS_OTA_PENDING));
                    break;

                case DBALERT_ZIGBEE_ERROR:
                    STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_ALERTS_ZIGBEE_ERROR));
                    break;

                case DBALERT_NO_ZIGBEE_NETWORK:
                    STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_ALERTS_NO_ZIGBEE_NETWORK));
                    break;

                case DBALERT_SERVER_PROBLEM:
                    STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_ALERTS_SERVER_PROBLEM));
                    break;

                case DBALERT_END_OF_LIFE:
                    STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_ALERTS_END_OF_LIFE));
                    break;

                case DBALERT_FLOOR_TEMP_LIMIT_REACHED:
                    STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_ALERTS_FLOOR_TEMP_LIMIT_REACHED));
                    break;

                case DBALERT_RELAYS_WEARING_OUT:
                    STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_ALERTS_RELAYS_WEARING_OUT));
                    break;

                case DBALERT_NO_FLOOR_SENSOR_AF:
                    STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_ALERTS_NO_FLOOR_SENSOR));
                    break;

                case DBALERT_NO_FLOOR_SENSOR_F:
                    STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_ALERTS_NO_FLOOR_SENSOR_F));
                    break;

                case DBALERT_TIME_NOT_SET:
                    STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_ALERTS_TIME_NOT_SET));
                    break;

                default:
                    STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_ALERTS_UNDEFINED));
                    break;
            }
            BSP_LCD_DrawBitmap(25, (ALERTZONE_YPOS_START + ((uint16_t)i*ALERTZONE_YPOS_OFFSET)), (uint8_t*)&LcdTempBuffer);
            BSP_LCD_DisplaySpecificFontStringAt(38, ALERTTEXT_YPOS_START+((uint16_t)i*ALERTTEXT_YPOS_OFFSET), string_ptr, LEFT_MODE, &GUI_FontCairoSemiBold46);
        }
    }
}

/*******************************************************************************
* @brief  Display the active alerts
* @inputs Page: Current alert page to be displayed
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/08/03
*******************************************************************************/
void DPH_DisplayAlertsDetails(uint8_t tstatIndex, uint8_t alertId)
{
    uint8_t string_ptr[40];
    uint8_t string_advice1_ptr[40];
    uint8_t string_advice2_ptr[40];
    uint8_t string_advice3_ptr[40];
    uint8_t string_advice4_ptr[40];

    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE1_Y_POS, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS), CENTER_MODE, &GUI_FontCairoSemiBold62);
    switch (alertId)
    {
        case DBALERT_GFCI:
            STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_GFCI_1));
            STR_StrCpy(string_advice1_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_GFCI_2));
            STR_StrCpy(string_advice2_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_GFCI_3));
            STR_StrCpy(string_advice3_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_GFCI_4));
            STR_StrCpy(string_advice4_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_GFCI_5));
            break;

        case DBALERT_END_OF_LIFE:
            STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_EOL_1));
            STR_StrCpy(string_advice1_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_EOL_2));
            STR_StrCpy(string_advice2_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_EOL_3));
            STR_StrCpy(string_advice3_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_EOL_4));
            STR_StrCpy(string_advice4_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_EOL_5));
            break;

        case DBALERT_FLOOR_TEMP_LIMIT_REACHED:
            STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_FLOOR_TEMP_LIMIT_1));
            STR_StrCpy(string_advice1_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_FLOOR_TEMP_LIMIT_2));
            STR_StrCpy(string_advice2_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_FLOOR_TEMP_LIMIT_3));
            STR_StrCpy(string_advice3_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_FLOOR_TEMP_LIMIT_4));
            STR_StrCpy(string_advice4_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_FLOOR_TEMP_LIMIT_5));
            break;

        case DBALERT_RELAYS_WEARING_OUT:
            STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_RELAYS_WEARING_OUT_1));
            STR_StrCpy(string_advice1_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_RELAYS_WEARING_OUT_2));
            STR_StrCpy(string_advice2_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_RELAYS_WEARING_OUT_3));
            STR_StrCpy(string_advice3_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_RELAYS_WEARING_OUT_4));
            STR_StrCpy(string_advice4_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_RELAYS_WEARING_OUT_5));
            break;

        case DBALERT_NO_FLOOR_SENSOR_AF:
            STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_1));
            STR_StrCpy(string_advice1_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_2));
            STR_StrCpy(string_advice2_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_3));
            STR_StrCpy(string_advice3_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_4));
            STR_StrCpy(string_advice4_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_5));
            break;

        case DBALERT_NO_FLOOR_SENSOR_F:
            STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_F_1));
            STR_StrCpy(string_advice1_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_F_2));
            STR_StrCpy(string_advice2_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_F_3));
            STR_StrCpy(string_advice3_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_F_4));
            STR_StrCpy(string_advice4_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_F_5));
            break;

        case DBALERT_TIME_NOT_SET:
            STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_TIME_NOT_SET_1));
            STR_StrCpy(string_advice1_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_TIME_NOT_SET_2));
            STR_StrCpy(string_advice2_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_TIME_NOT_SET_3));
            STR_StrCpy(string_advice3_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_TIME_NOT_SET_4));
            STR_StrCpy(string_advice4_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_TIME_NOT_SET_5));

            //Display Set Date & Time button
            HAL_ReadFlashData (BLUE_BTN_ADD + RAM_OFFSET, BLUE_BTN_SIZE, &LcdTempBuffer[0]);
            BSP_LCD_DrawBitmap(25, 169, (uint8_t*)&LcdTempBuffer);
            BSP_LCD_DisplaySpecificFontStringAt(0, 173, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_TIME_NOT_SET_ACTION), CENTER_MODE, &GUI_FontCairoSemiBold46);
            break;
            break;

        default:
            STR_StrCpy(string_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_UNDEFINED_1));
            STR_StrCpy(string_advice1_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_UNDEFINED_2));
            STR_StrCpy(string_advice2_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_UNDEFINED_3));
            STR_StrCpy(string_advice3_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_UNDEFINED_4));
            STR_StrCpy(string_advice4_ptr, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_UNDEFINED_5));
            break;
    }
    BSP_LCD_SetTextColor(LCD_COLOR_ORANGE);
    BSP_LCD_DisplaySpecificFontStringAt(38, 67, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_ALERT), LEFT_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    BSP_LCD_DisplaySpecificFontStringAt(38, 107, string_ptr, LEFT_MODE, &GUI_FontCairoSemiBold46);

    HAL_ReadFlashData (ADVISE_BOX_ADD + RAM_OFFSET, ADVISE_BOX_SIZE, &LcdTempBuffer[0]);
    BSP_LCD_DrawBitmap(24,304,(uint8_t*)&LcdTempBuffer);
    BSP_LCD_DisplaySpecificFontStringAt(0, 267, (uint8_t*)STR_GetString(STRID_ALERT_DETAILS_ADVISE), CENTER_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(0, 317, string_advice1_ptr, CENTER_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(0, 347, string_advice2_ptr, CENTER_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(0, 377, string_advice3_ptr, CENTER_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DisplaySpecificFontStringAt(0, 407, string_advice4_ptr, CENTER_MODE, &GUI_FontCairoSemiBold46);
}

/*******************************************************************************
* @brief  Display the time and date menu
* @inputs year, month, day, minutes of day, week day
* @retval None
* @author Jean-Fran�ois Many
* @date   2019/07/30
*******************************************************************************/
void DPH_DisplayDateTime(uint8_t firstEntry, uint16_t year, uint8_t month, uint8_t day, uint16_t minutesOfDay, uint8_t weekDay)
{
    uint8_t string_ptr[14];
    uint8_t bufferstring_ptr[10];
    TimeFormat_t format;
    static uint16_t last_year = 0;
    static uint8_t last_month = 0;
    static uint8_t last_weekday = 0;
    static uint8_t last_day = 0;
    static uint16_t last_time = 2000;

    if (firstEntry)
    {
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE1_Y_POS, (uint8_t*)STR_GetString(STRID_DATETIME_1), CENTER_MODE, &GUI_FontCairoSemiBold62);
        BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE2_Y_POS, (uint8_t*)STR_GetString(STRID_DATETIME_2), CENTER_MODE, &GUI_FontCairoSemiBold62);

        //Display Apply button
        HAL_ReadFlashData (BLUE_BTN_ADD + RAM_OFFSET, BLUE_BTN_SIZE, &LcdTempBuffer[0]);
        BSP_LCD_DrawBitmap(25, 420, (uint8_t*)&LcdTempBuffer);
        BSP_LCD_DisplaySpecificFontStringAt(0, 424, (uint8_t*)STR_GetString(STRID_APPLY), CENTER_MODE, &GUI_FontCairoSemiBold46);

        //Display previous buttons
        HAL_ReadFlashData (BACK_ICON_ADD + RAM_OFFSET, BACK_ICON_SIZE, &LcdTempBuffer[0]);
        BSP_LCD_DrawBitmap(45, 122, (uint8_t*)&LcdTempBuffer);
        BSP_LCD_DrawBitmap(45, 172, (uint8_t*)&LcdTempBuffer);
        BSP_LCD_DrawBitmap(45, 222, (uint8_t*)&LcdTempBuffer);
        BSP_LCD_DrawBitmap(45, 272, (uint8_t*)&LcdTempBuffer);

        //Display next buttons
        HAL_ReadFlashData (NEXT_ICON_ADD + RAM_OFFSET, NEXT_ICON_SIZE, &LcdTempBuffer[0]);
        BSP_LCD_DrawBitmap(252, 122, (uint8_t*)&LcdTempBuffer);
        BSP_LCD_DrawBitmap(252, 172, (uint8_t*)&LcdTempBuffer);
        BSP_LCD_DrawBitmap(252, 222, (uint8_t*)&LcdTempBuffer);
        BSP_LCD_DrawBitmap(252, 272, (uint8_t*)&LcdTempBuffer);

    }
    //Display date/time
    if (last_year != year)
    {
        BSP_LCD_ClearStringLineSection(68, 123, 184, 46);
    }
    STR_StrItoA(bufferstring_ptr, year);
    STR_StrCpy(string_ptr, bufferstring_ptr);
    BSP_LCD_DisplaySpecificFontStringAt(0, 123, string_ptr, CENTER_MODE, &GUI_FontCairoSemiBold46);

    if (last_month != month)
    {
        BSP_LCD_ClearStringLineSection(68, 173, 184, 46);
    }
    switch (month)
    {
        case 1:
            STR_StrCpy(bufferstring_ptr, (uint8_t*)STR_GetString(STRID_JANUARY));
            break;

        case 2:
            STR_StrCpy(bufferstring_ptr, (uint8_t*)STR_GetString(STRID_FEBRUARY));
            break;

        case 3:
            STR_StrCpy(bufferstring_ptr, (uint8_t*)STR_GetString(STRID_MARCH));
            break;

        case 4:
            STR_StrCpy(bufferstring_ptr, (uint8_t*)STR_GetString(STRID_APRIL));
            break;

        case 5:
            STR_StrCpy(bufferstring_ptr, (uint8_t*)STR_GetString(STRID_MAY));
            break;

        case 6:
            STR_StrCpy(bufferstring_ptr, (uint8_t*)STR_GetString(STRID_JUNE));
            break;

        case 7:
            STR_StrCpy(bufferstring_ptr, (uint8_t*)STR_GetString(STRID_JULY));
            break;

        case 8:
            STR_StrCpy(bufferstring_ptr, (uint8_t*)STR_GetString(STRID_AUGUST));
            break;

        case 9:
            STR_StrCpy(bufferstring_ptr, (uint8_t*)STR_GetString(STRID_SEPTEMBER));
            break;

        case 10:
            STR_StrCpy(bufferstring_ptr, (uint8_t*)STR_GetString(STRID_OCTOBER));
            break;

        case 11:
            STR_StrCpy(bufferstring_ptr, (uint8_t*)STR_GetString(STRID_NOVEMBER));
            break;

        case 12:
            STR_StrCpy(bufferstring_ptr, (uint8_t*)STR_GetString(STRID_DECEMBER));
            break;

        default:
            break;
    }
    BSP_LCD_DisplaySpecificFontStringAt(0, 173, bufferstring_ptr, CENTER_MODE, &GUI_FontCairoSemiBold46);

    if ((last_weekday != weekDay) || (last_day != day))
    {
        BSP_LCD_ClearStringLineSection(68, 223, 184, 46);
    }
    switch (weekDay)
    {
        case 1:
            STR_StrCpy(bufferstring_ptr, (uint8_t*)STR_GetString(STRID_MONDAY));
            break;

        case 2:
            STR_StrCpy(bufferstring_ptr, (uint8_t*)STR_GetString(STRID_TUESDAY));
            break;

        case 3:
            STR_StrCpy(bufferstring_ptr, (uint8_t*)STR_GetString(STRID_WEDNESDAY));
            break;

        case 4:
            STR_StrCpy(bufferstring_ptr, (uint8_t*)STR_GetString(STRID_THURSDAY));
            break;

        case 5:
            STR_StrCpy(bufferstring_ptr, (uint8_t*)STR_GetString(STRID_FRIDAY));
            break;

        case 6:
            STR_StrCpy(bufferstring_ptr, (uint8_t*)STR_GetString(STRID_SATURDAY));
            break;

        case 7:
            STR_StrCpy(bufferstring_ptr, (uint8_t*)STR_GetString(STRID_SUNDAY));
            break;

        default:
            break;
    }
    STR_StrCpy(string_ptr, bufferstring_ptr);
    STR_StrCat(string_ptr, ",");
    STR_StrItoA(bufferstring_ptr, day);
    STR_StrCat(string_ptr, bufferstring_ptr);
    BSP_LCD_DisplaySpecificFontStringAt(0, 223, string_ptr, CENTER_MODE, &GUI_FontCairoSemiBold46);

    if (last_time != minutesOfDay)
    {
        BSP_LCD_ClearStringLineSection(68, 273, 184, 46);
    }
    DBTHRD_GetData(DBTYPE_TIME_FORMAT,(void*)&format, INDEX_DONT_CARE);
    if (format == TIME_24H)
    {
        STR_Time2Str(minutesOfDay, string_ptr, STD_TIME | USE_24H_FORMAT| NO_SPACE);  //Format = "10:50" / "22:08"
    }
    else
    {
        STR_Time2Str(minutesOfDay, string_ptr, STD_TIME | LONG_12H  | NO_SPACE);  //Format = "10:50am" / "10:08pm"
    }
    BSP_LCD_DisplaySpecificFontStringAt(0, 273, string_ptr, CENTER_MODE, &GUI_FontCairoSemiBold46);

    last_year = year;
    last_month = month;
    last_weekday = weekDay;
    last_day = day;
    last_time = minutesOfDay;
}

/*******************************************************************************
* @brief  Display the selected program setpoint
* @inputs setpoint: program setpoint (x100)
* @inputs pgm: selected program (wake, leave, return, sleep)
* @retval None
* @author Jean-Fran�ois Many
* @date   2019/08/02
*******************************************************************************/
void DPH_DisplayProgramSetpoint(uint8_t firstEntry, uint16_t setpoint, Program_t pgm)
{
    uint8_t floorMode;
    uint8_t setpoint_type;

    if (firstEntry)
    {
        switch (pgm)
        {
            case PGM_WAKE:
                //Display the menu header
                BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
                BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE1_Y_POS, (uint8_t*)STR_GetString(STRID_WAKE_TITLE1), CENTER_MODE, &GUI_FontCairoSemiBold62);
                BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE2_Y_POS, (uint8_t*)STR_GetString(STRID_WAKE_TITLE2), CENTER_MODE, &GUI_FontCairoSemiBold62);
                //Display Program icon
                HAL_ReadFlashData (WAKE_ICON_ADD + RAM_OFFSET, WAKE_ICON_SIZE, &LcdTempBuffer[0]);
                break;

            case PGM_LEAVE:
                //Display the menu header
                BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
                BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE1_Y_POS, (uint8_t*)STR_GetString(STRID_LEAVE_TITLE1), CENTER_MODE, &GUI_FontCairoSemiBold62);
                BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE2_Y_POS, (uint8_t*)STR_GetString(STRID_LEAVE_TITLE2), CENTER_MODE, &GUI_FontCairoSemiBold62);
                //Display Program icon
                HAL_ReadFlashData (LEAVE_ICON_ADD + RAM_OFFSET, LEAVE_ICON_SIZE, &LcdTempBuffer[0]);
                break;

            case PGM_RETURN:
                //Display the menu header
                BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
                BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE1_Y_POS, (uint8_t*)STR_GetString(STRID_RETURN_TITLE1), CENTER_MODE, &GUI_FontCairoSemiBold62);
                BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE2_Y_POS, (uint8_t*)STR_GetString(STRID_RETURN_TITLE2), CENTER_MODE, &GUI_FontCairoSemiBold62);
                //Display Program icon
                HAL_ReadFlashData (RETURN_ICON_ADD + RAM_OFFSET, RETURN_ICON_SIZE, &LcdTempBuffer[0]);
                break;

            case PGM_SLEEP:
                //Display the menu header
                BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
                BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE1_Y_POS, (uint8_t*)STR_GetString(STRID_SLEEP_TITLE1), CENTER_MODE, &GUI_FontCairoSemiBold62);
                BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE2_Y_POS, (uint8_t*)STR_GetString(STRID_SLEEP_TITLE2), CENTER_MODE, &GUI_FontCairoSemiBold62);
                //Display Program icon
                HAL_ReadFlashData (SLEEP_ICON_ADD + RAM_OFFSET, SLEEP_ICON_SIZE, &LcdTempBuffer[0]);
                break;

            default:
                break;
        }
        BSP_LCD_DrawBitmap(147, 100, (uint8_t*)&LcdTempBuffer);

        //Display the Schedule button
        HAL_ReadFlashData (GREY_BTN_ADD + RAM_OFFSET, GREY_BTN_SIZE, &LcdTempBuffer2[0]);
        //Display the pen in the schedule button
        HAL_ReadFlashData (PEN_BIG_ADD + RAM_OFFSET, PEN_BIG_SIZE, &LcdTempBuffer[0]);
        BSP_LCD_BlendImagesInBuffer(228, 0, (uint8_t*)&LcdTempBuffer,(uint8_t*)&LcdTempBuffer2);
        BSP_LCD_DrawBitmap(25, 194, (uint8_t*)&LcdTempBuffer2);
        BSP_LCD_DisplaySpecificFontStringAt(38, 198, (uint8_t*)STR_GetString(STRID_SCHEDULE), LEFT_MODE, &GUI_FontCairoSemiBold46);
    }

    //Display the temperature limit
    DBTHRD_GetData(DBTYPE_FLOOR_MODE,&floorMode, THIS_THERMOSTAT);
    if (floorMode == CONTROLMODE_FLOOR)
    {
        setpoint_type = SET_FLOOR;
    }
    else
    {
        setpoint_type = SET_AMBIENT;
    }
    DPH_DisplaySetpoint(setpoint, firstEntry, FALSE, setpoint_type, TRUE);
}

/*******************************************************************************
* @brief  Display the selected program schedule
* @inputs timeEdit: time currently being edited in minutes
* @inputs pgm: selected program (wake, leave, return, sleep)
* @inputs selectedDays: selected days to be edited simultaneously
* @inputs pgmSchedule: selected program schedule (enable and time)
* @inputs changeId: bit field stating what needs to be updated based on the last change
* @inputs changePending: indicating if the Apply button should be visible or not
* @retval None
* @author Jean-Fran�ois Many
* @date   2019/08/02
*******************************************************************************/
void DPH_DisplayProgramSchedule(uint8_t firstEntry, uint16_t timeEdit, uint8_t pgm, uint8_t* selectedDays, PgmSchedule_t* pgmSchedule, uint8_t changeId, uint8_t changePending)
{
    static uint8_t EditAllowed = 0;
    static uint8_t previousChangePending = 0;
    uint8_t string_ptr[10];
    uint16_t min_of_day;
    TimeFormat_t format;

    DBTHRD_GetData(DBTYPE_TIME_FORMAT,(void*)&format, INDEX_DONT_CARE);
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    if (firstEntry)
    {
        switch (pgm)
        {
            case PGM_WAKE:
                //Display the menu header
                BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE1_Y_POS, (uint8_t*)STR_GetString(STRID_WAKESCHEDULE), CENTER_MODE, &GUI_FontCairoSemiBold62);
                break;

            case PGM_LEAVE:
                //Display the menu header
                BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE1_Y_POS, (uint8_t*)STR_GetString(STRID_LEAVESCHEDULE), CENTER_MODE, &GUI_FontCairoSemiBold62);
                break;

            case PGM_RETURN:
                //Display the menu header
                BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE1_Y_POS, (uint8_t*)STR_GetString(STRID_RETURNSCHEDULE), CENTER_MODE, &GUI_FontCairoSemiBold62);
                break;

            case PGM_SLEEP:
                //Display the menu header
                BSP_LCD_DisplaySpecificFontStringAt(0, TITLE_LINE1_Y_POS, (uint8_t*)STR_GetString(STRID_SLEEPSCHEDULE), CENTER_MODE, &GUI_FontCairoSemiBold62);
                break;

            default:
                break;
        }

        //Display the days
        BSP_LCD_DisplaySpecificFontStringAt(75, 75, (uint8_t*)STR_GetString(STRID_MON), LEFT_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(75, 115, (uint8_t*)STR_GetString(STRID_TUE), LEFT_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(75, 155, (uint8_t*)STR_GetString(STRID_WED), LEFT_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(75, 195, (uint8_t*)STR_GetString(STRID_THU), LEFT_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(75, 235, (uint8_t*)STR_GetString(STRID_FRI), LEFT_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(75, 275, (uint8_t*)STR_GetString(STRID_SAT), LEFT_MODE, &GUI_FontCairoSemiBold46);
        BSP_LCD_DisplaySpecificFontStringAt(75, 315, (uint8_t*)STR_GetString(STRID_SUN), LEFT_MODE, &GUI_FontCairoSemiBold46);

        //Preload the transparent box in order to be able to blend two images out of bound
        HAL_ReadFlashData (TRANSPARENT_BOX_ADD + RAM_OFFSET, TRANSPARENT_BOX_SIZE, &LcdTempBuffer2[0]);
        //Display the grey box for the selected days
        HAL_ReadFlashData (GREY_BTN_REDUCED_ADD + RAM_OFFSET, GREY_BTN_REDUCED_SIZE, &LcdTempBuffer[0]);
        BSP_LCD_BlendImagesInBuffer(0, 0, (uint8_t*)&LcdTempBuffer,(uint8_t*)&LcdTempBuffer2);
        //Display the pen (grey) for the selected days
        HAL_ReadFlashData (PEN_SMALL_ADD + RAM_OFFSET, PEN_SMALL_SIZE, &LcdTempBuffer[0]);
        BSP_LCD_BlendImagesInBuffer(136, 1, (uint8_t*)&LcdTempBuffer,(uint8_t*)&LcdTempBuffer2);
    }

    if (changeId & CHANGE_SELECT)
    {
        EditAllowed = 0;
    }

    for (uint8_t i = 0; i < 7; i++)
    {
        if (pgmSchedule->Day[i].ScheduleEnabled != 0)
        {
            if (changeId & CHANGE_TOGGLE)
            {
                //Display the Blue Toggle buttons
                HAL_ReadFlashData (TOGGLE_BLUE_ADD + RAM_OFFSET, TOGGLE_BLUE_SIZE, &LcdTempBuffer[0]);
                BSP_LCD_DrawBitmap(28, 82 + i * 40, (uint8_t*)&LcdTempBuffer);
            }

            if (changeId & CHANGE_SELECT)
            {
                if (selectedDays[i] != 0)
                {
                    BSP_LCD_DrawBitmap(130, (77 + i * 40), (uint8_t*)&LcdTempBuffer2);
                    EditAllowed = 1;
                }
                else
                {
                    HAL_ReadFlashData (PEN_SMALL_TRANSP_ADD + RAM_OFFSET, PEN_SMALL_TRANSP_SIZE, &LcdTempBuffer[0]);
                    BSP_LCD_ClearStringLineSection(130, (77 + i * 40), 164, 30);
                    BSP_LCD_DrawBitmap(266, (78 + i * 40), (uint8_t*)&LcdTempBuffer);
                }
                //Display the schedule time for the day
                min_of_day = pgmSchedule->Day[i].ScheduleTime;
                if (format == TIME_24H)
                {
                    STR_Time2Str(min_of_day, string_ptr, STD_TIME | USE_24H_FORMAT| NO_SPACE);  //Format = "10:50" / "22:08"
                }
                else
                {
                    STR_Time2Str(min_of_day, string_ptr, STD_TIME | LONG_12H  | NO_SPACE);  //Format = "10:50am" / "10:08pm"
                }
                BSP_LCD_DisplaySpecificFontStringAt(140, 75 + i * 40, string_ptr, LEFT_MODE, &GUI_FontCairoSemiBold46);
            }
        }
        else
        {
            if (changeId & CHANGE_TOGGLE)
            {
                //Display the White Toggle buttons
                HAL_ReadFlashData (TOGGLE_WHITE_ADD + RAM_OFFSET, TOGGLE_WHITE_SIZE, &LcdTempBuffer[0]);
                BSP_LCD_ClearStringLineSection(130, (77 + i * 40), 164, 30);
                BSP_LCD_DrawBitmap(28, 82 + i * 40, (uint8_t*)&LcdTempBuffer);
            }
        }
    }

    if (changeId & CHANGE_EDIT)
    {
        if (EditAllowed != 0)
        {
            //Display the edited time and arrows
            HAL_ReadFlashData (BACK_ICON_ADD + RAM_OFFSET, BACK_ICON_SIZE, &LcdTempBuffer[0]);
            BSP_LCD_DrawBitmap(45, 367, (uint8_t*)&LcdTempBuffer);
            HAL_ReadFlashData (NEXT_ICON_ADD + RAM_OFFSET, NEXT_ICON_SIZE, &LcdTempBuffer[0]);
            BSP_LCD_DrawBitmap(252, 367, (uint8_t*)&LcdTempBuffer);

            min_of_day = timeEdit;
            if (format == TIME_24H)
            {
                STR_Time2Str(min_of_day, string_ptr, STD_TIME | USE_24H_FORMAT| NO_SPACE);  //Format = "10:50" / "22:08"
            }
            else
            {
                STR_Time2Str(min_of_day, string_ptr, STD_TIME | LONG_12H  | NO_SPACE);  //Format = "10:50am" / "10:08pm"
            }

            BSP_LCD_ClearStringLineSection(98, 367, 135, 45);

            BSP_LCD_DisplaySpecificFontStringAt(0, 362, string_ptr, CENTER_MODE, &GUI_FontCairoSemiBold62);
        }
        else
        {
            BSP_LCD_ClearStringLineSection(25, 367, 270, 42);
        }
    }

    if (changePending != previousChangePending)
    {
        if (changePending != 0)
        {
            //Display Apply button
            HAL_ReadFlashData (BLUE_BTN_ADD + RAM_OFFSET, BLUE_BTN_SIZE, &LcdTempBuffer[0]);
            BSP_LCD_DrawBitmap(25, 420, (uint8_t*)&LcdTempBuffer);
            BSP_LCD_DisplaySpecificFontStringAt(0, 424, (uint8_t*)STR_GetString(STRID_APPLY), CENTER_MODE, &GUI_FontCairoSemiBold46);
        }
        else
        {
            BSP_LCD_ClearStringLineSection(25, 420, 270, 42);
        }
    }
    previousChangePending = changePending;
}

/*******************************************************************************
* @brief  Get the color based on the setpoint
* @inputs setpoint: setpoint to display
* @retval uint32_t: color code for the setpoint
* @author Jean-Fran�ois Many
* @date   2016/08/29
*******************************************************************************/
uint32_t DPH_GetColor(uint16_t setpoint)
{
    uint32_t color;

    if (setpoint <= 1900)
    {
        color = LCD_TUX_GRADIENT_1;
    }
    else if (setpoint <= 1950)
    {
        color = LCD_TUX_GRADIENT_2;
    }
    else if (setpoint <= 2000)
    {
        color = LCD_TUX_GRADIENT_3;
    }
    else if (setpoint <= 2050)
    {
        color = LCD_TUX_GRADIENT_4;
    }
    else if (setpoint <= 2100)
    {
        color = LCD_TUX_GRADIENT_5;
    }
    else if (setpoint <= 2150)
    {
        color = LCD_TUX_GRADIENT_6;
    }
    else if (setpoint <= 2200)
    {
        color = LCD_TUX_GRADIENT_7;
    }
    else if (setpoint <= 2250)
    {
        color = LCD_TUX_GRADIENT_8;
    }
    else if (setpoint <= 2300)
    {
        color = LCD_TUX_GRADIENT_9;
    }
    else if (setpoint <= 2350)
    {
        color = LCD_TUX_GRADIENT_10;
    }
    else
    {
        color = LCD_TUX_GRADIENT_11;
    }

    return color;
}

#ifdef LOAD_CYCLING
void DPH_DisplayLoadCyclingMode(uint8_t mode)
{
    uint8_t string_ptr[33];
    uint8_t stringNum_ptr[8];

    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    BSP_LCD_ClearStringLineSection(0, 345, 320, 46);
    STR_StrItoA(stringNum_ptr, mode);
    STR_StrCpy(string_ptr, stringNum_ptr);
    BSP_LCD_DisplaySpecificFontStringAt(0, 345, string_ptr, CENTER_MODE, &GUI_FontCairoSemiBold46);
}
#endif /* LOAD_CYCLING */

void DPH_DisplayLCDTest(uint16_t x, uint16_t y, uint16_t targetx, uint16_t targety)
{
    uint8_t string_ptr[33];
    uint8_t stringNum_ptr[8];

    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    BSP_LCD_ClearStringLineSection(0, 50, 320, 46);
    STR_StrCpy(string_ptr, "X: ");
    STR_StrItoA(stringNum_ptr, x);
    STR_StrCat(string_ptr, stringNum_ptr);
    STR_StrCat(string_ptr, " Y: ");
    STR_StrItoA(stringNum_ptr, y);
    STR_StrCat(string_ptr, stringNum_ptr);
    BSP_LCD_DisplaySpecificFontStringAt(0, 50, string_ptr, CENTER_MODE, &GUI_FontCairoSemiBold46);

    if ((targetx != 0) || (targety != 0))
    {
        BSP_LCD_ClearStringLineSection(0, 10, 320, 46);
        STR_StrCpy(string_ptr, "TargetX: ");
        STR_StrItoA(stringNum_ptr, targetx);
        STR_StrCat(string_ptr, stringNum_ptr);
        STR_StrCat(string_ptr, " TargetY: ");
        STR_StrItoA(stringNum_ptr, targety);
        STR_StrCat(string_ptr, stringNum_ptr);
        BSP_LCD_DisplaySpecificFontStringAt(0, 10, string_ptr, CENTER_MODE, &GUI_FontCairoSemiBold46);
    }

    HAL_ReadFlashData (BLUE_SQUARE_BTN_ADD + RAM_OFFSET, BLUE_SQUARE_BTN_SIZE, &LcdTempBuffer[0]);
    BSP_LCD_DrawBitmap(252, 219, (uint8_t*)&LcdTempBuffer);
    BSP_LCD_DisplaySpecificFontStringAt(38, 222, (uint8_t*)STR_GetString(STRID_NEXT), RIGHT_MODE, &GUI_FontCairoSemiBold46);
    BSP_LCD_DrawBitmap(25, 219, (uint8_t*)&LcdTempBuffer);
    BSP_LCD_DisplaySpecificFontStringAt(38, 222, (uint8_t*)STR_GetString(STRID_PREVIOUS), LEFT_MODE, &GUI_FontCairoSemiBold46);

    BSP_LCD_SetTextColor(LCD_COLOR_ORANGE);
    BSP_LCD_FillRect(x, y, 2, 2);
    //BSP_LCD_DrawPixel(x, y, LCD_COLOR_BLACK);
}

//Production Test Mode Display
void DPH_HandleTestDisplay(uint8_t testValue)
{
    BSP_LCD_DrawRect(5, 5, 64, 64);
    BSP_LCD_DrawRect(250, 5, 64, 64);
    BSP_LCD_DrawRect(128, 208, 64, 64);
    BSP_LCD_DrawRect(5, 410, 64, 64);
    BSP_LCD_DrawRect(250, 410, 64, 64);

    HAL_ReadFlashData (CHECK_ICON_ADD + RAM_OFFSET, CHECK_ICON_SIZE, &LcdTempBuffer[0]);
    if (testValue & 0x01)
    {
        BSP_LCD_DrawBitmap(21,21,(uint8_t*)&LcdTempBuffer);
    }
    if (testValue & 0x02)
    {
        BSP_LCD_DrawBitmap(266,21,(uint8_t*)&LcdTempBuffer);
    }
    if (testValue & 0x04)
    {
        BSP_LCD_DrawBitmap(144,224,(uint8_t*)&LcdTempBuffer);
    }
    if (testValue & 0x08)
    {
        BSP_LCD_DrawBitmap(21,426,(uint8_t*)&LcdTempBuffer);
    }
    if (testValue & 0x10)
    {
        BSP_LCD_DrawBitmap(266,426,(uint8_t*)&LcdTempBuffer);
    }
}

void DPH_HandleBlackDisplay(void)
{
    BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
    BSP_LCD_FillRect(0, 0, 320, 480);
}

void DPH_HandleWhiteDisplay(void)
{
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    BSP_LCD_FillRect(0, 0, 320, 480);
}

/** Copyright(C) 2014 Stelpro Design, All Rights Reserved**/
