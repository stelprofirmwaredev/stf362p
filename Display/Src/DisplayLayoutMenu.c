/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       DisplayLayoutMenu.c

    \brief      Source code file for the DisplayLayoutMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the DisplayLayoutMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "DisplayLayoutMenu.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/


/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e       DisplayLayoutEditState = EDIT_IDLE;
static DisplayLayout_t      DisplayLayoutEditValue;
static DisplayLayout_t	    DisplayLayout;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/
    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void ChangeDisplayLayout(KEYB_PARAM keyId);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    ChangeDisplayLayout

    Change the layout options to be displayed on screen
*******************************************************************************/
static void ChangeDisplayLayout(KEYB_PARAM keyId)
{
    switch (keyId)
    {
        case KEY_OPTIONS_2:
            DisplayLayoutEditValue.time ^= 1;
            break;

        case KEY_OPTIONS_3:
            DisplayLayoutEditValue.weather ^= 1;
            break;

        case KEY_OPTIONS_4:
            DisplayLayoutEditValue.humidity ^= 1;
            break;

        case KEY_OPTIONS_5:
            DisplayLayoutEditValue.icons ^= 1;
            break;

        default:
            break;
    }

    DPH_DisplayLayout(DisplayLayoutEditValue);
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_DisplayLayout_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_DisplayLayout_Entry
*******************************************************************************/
void MNU_DisplayLayout_Entry(void)
{
    DisplayLayoutEditValue = DisplayLayout;
    DisplayLayoutEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);

	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_DisplayLayout_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_DisplayLayout_Core

    Allow selecting elements to be displayed on screen
*******************************************************************************/
void MNU_DisplayLayout_Core(void)
{
    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show display layout options
    //
    DPH_DisplayLayout(DisplayLayoutEditValue);

    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure key in the Advanced settings menu
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_DisplayLayout_Commit, (void*)KEY_BACK);
        KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)ChangeDisplayLayout, (void*)KEY_OPTIONS_2);
        KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)ChangeDisplayLayout, (void*)KEY_OPTIONS_3);
        KBH_SetKeyPressProperties(KEY_OPTIONS_4, (FUNC)ChangeDisplayLayout, (void*)KEY_OPTIONS_4);
        KBH_SetKeyPressProperties(KEY_OPTIONS_5, (FUNC)ChangeDisplayLayout, (void*)KEY_OPTIONS_5);
        KBH_SetKeyPressProperties(KEY_DONE, (FUNC)MNU_DisplayLayout_Commit, (void*)KEY_DONE);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_DisplayLayout_Exit
*******************************************************************************/
void MNU_DisplayLayout_Exit(void)
{
    DisplayLayoutEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_DisplayLayout_Init
*******************************************************************************/
void MNU_DisplayLayout_Init(void)
{
    DisplayLayoutEditValue.time = 1;
    DisplayLayoutEditValue.weather = 1;
    DisplayLayoutEditValue.humidity = 1;
    DisplayLayoutEditValue.icons = 1;
    MNU_DisplayLayout_SetDisplayLayout(DisplayLayoutEditValue);
}

/*******************************************************************************
    MNU_DisplayLayout_Commit
*******************************************************************************/
void MNU_DisplayLayout_Commit(KEYB_PARAM keyId)
{
	static KEYB_PARAM commitKey = NULL;

    if ( DisplayLayoutEditState == EDIT_IN_PROGRESS )
    {
        commitKey = keyId;
        KBH_ClearAllKeyProperties();
        switch (commitKey)
        {
            case KEY_DONE:
                MNU_DisplayLayout_SetDisplayLayout(DisplayLayoutEditValue);
                //No break on purpose

            case KEY_BACK:
            default:
                MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_DisplayLayout_Commit, (KEYB_PARAM*)commitKey, IMMEDIATELY);
                break;
        }
        DPH_DisplayLayout(DisplayLayoutEditValue);
        DisplayLayoutEditState = EDIT_COMMITTING;
    }
    else if ( DisplayLayoutEditState == EDIT_COMMITTING )
    {
        DisplayLayoutEditState = EDIT_IDLE;
        switch (commitKey)
        {
            case KEY_BACK:
            case KEY_DONE:
                MNH_SetActiveMenu(MENU_OPTIONS);
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
    }
}

/*******************************************************************************
    MNU_DisplayLayout_GetDisplayLayout

    Return the current display layout
*******************************************************************************/
DisplayLayout_t MNU_DisplayLayout_GetDisplayLayout(void)
{
    return DisplayLayout;
}

/*******************************************************************************
    MNU_DisplayLayout_SetDisplayLayout
*******************************************************************************/
void MNU_DisplayLayout_SetDisplayLayout(DisplayLayout_t layout)
{
    DisplayLayout = layout;
    //Todo - Notify display layout
//	 MED_NotifyDisplayLayout(DisplayLayout);
}

