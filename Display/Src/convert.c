/********************************************************************************************
*    convert.c
*
*                            Module Conversion Donn�es (AC629)
*
*    Regroupe les fonctions de conversion de temp�rature ainsi que les fonctions
*    d'arrondissement.
*
*    \author     Jean-Fran�ois Many
*
********************************************************************************************/

/********************************************************************************************
* Inclusions
********************************************************************************************/
#include "typedef.h"
#include "tools.h"
#include "convert.h"

#ifdef XTR_CODE
#include <XTRLogicBlock.h>
#include <XTRUserInterface.h>
#endif /* XTR_CODE */


/********************************************************************************************
*    D�finition des variables globales publiques
********************************************************************************************/

/*** Variables globales sans initialisation ************************************************/

/*** Variables globale initialis�es lors d�claration ***************************************/

/*** Variables globale initialis�es � z�ro *************************************************/


/********************************************************************************************
* D�finition des constantes priv�es
********************************************************************************************/
#ifndef NO_PRB
 #define  NO_PRB                (int16s) 0x8000
#endif


/********************************************************************************************
* D�claration des variables globales priv�es
********************************************************************************************/

/*** Variables globale initialis�es � z�ro *************************************************/


/********************************************************************************************
* D�claration des fonctions priv�es
********************************************************************************************/


/********************************************************************************************
* D�finition des fonctions priv�es
********************************************************************************************/


/********************************************************************************************
* D�finition des fonctions publiques
********************************************************************************************/

/********************************************************************************************
*  Conversion Bianire � BCD
*
*    Convertit un nombre binaire 16 bits en BCD.
*
*    Param�tres :
*        data_in : temp�rature � convertir en BCD. (0 @ 9999)
*
*    Retourne :
*        4 chiffres moins significatifs du nombre convertit en BCD (0 @ 0x9999)
*
********************************************************************************************/
int16u CVT_Bin2Bcd16(int16u data_in)
{
    int16u data_bcd = 0;
    int16u div;

    for (div = 10000; div >= 10; div /= 10) {
        for ( ; data_in >= div; data_in -= div) {
            data_bcd++;
        }
        data_bcd <<= 4;
    }
    data_bcd += data_in;                            //    Ajoute unit�s

    return data_bcd;
}


/********************************************************************************************
*  Conversion Temp�rature � BCD
*
*    Convertit une temp�rature en BCD avec gestion des codes d'erreur.
*
*    Param�tres :
*        temp_to_convert : temp�rature � convertir en BCD.
*
*    Retourne :
*        Temp�rature convertie en BCD avec codes de cont�le (n�gatif; unit�s)
*            Codes de cont�les:    bit 15 : Valeur n�gative.
*                                bit 14 : Affichage code (Er Lo Hi --).
*
********************************************************************************************/
int16u CVT_ConvertTempBCD(int16s temp_to_convert)
{
    int16u data_out = 0;

    if (temp_to_convert == NO_PRB) {                    //    Si sonde absente
        data_out = 0x4BBA;                                //        affiche '--'

    } else {                                            //    sinon
                                                        //        temp�rature d'affichage(x10) = temp�rature(x100) / 10
        temp_to_convert = temp_to_convert / 10;
        if (temp_to_convert > 1999) {                    //        si temp�rature � afficher > 199.9
            data_out = 0x4D1A;                            //            affiche 'HI'

        } else if (temp_to_convert < -1999) {            //        sinon si temp�rature � afficher < -199.9
            data_out = 0x4C0A;                            //            affiche 'LO'

        } else {                                        //        sinon (temp�rature valide)
            if (temp_to_convert < 0) {                    //            si temp�rature n�gative
                data_out |= 0x8000;                        //                affiche '-'
                temp_to_convert = ~temp_to_convert;        //            valeur � afficher = valeur absolue de la temp�rature
                temp_to_convert++;
            }
            data_out |= CVT_Bin2Bcd16(temp_to_convert); //            convertit en BCD
        }
    }

    return data_out;
}


/********************************************************************************************
*  Conversion �F
*
*    Convertit une temp�rature de �C -> �F.
*
*    Param�tres :
*        temp_to_convert : temp�rature en �C � convertir.
*        round : Valeur de l'arrondissement � effectuer. (0 = pas d'arrondis)
*
*    Retourne :
*        Temp�rature convertie en �F.
*
********************************************************************************************/
int16s CVT_ConvertCtoF(int16s temp_to_convert, int16u round)
{
                                                    //    Si temp�rature dans plage valide
    if ((temp_to_convert >= -18000) && (temp_to_convert <= 18000)) {
                                                    //        �F = �C * 9 / 5 + 32
        temp_to_convert = (int16s)( ((int32s)temp_to_convert * 9) / 5);
        temp_to_convert += 3200;
                                                    //        arrondis (s'il y a lieu)
        temp_to_convert = TLS_Round(temp_to_convert, round);
    }

    return temp_to_convert;
}


/********************************************************************************************
*  Conversion �C
*
*    Convertit une temp�rature de �F -> �C.
*
*    Param�tres :
*        temp_to_convert : temp�rature en �F � convertir.
*        round : Valeur de l'arrondissement � effectuer. (0 = pas d'arrondis)
*
*    Retourne :
*        Temp�rature convertie en �C.
*
********************************************************************************************/
int16s CVT_ConvertFtoC(int16s temp_to_convert, int16u round)
{                                                    //    Si temp�rature dans plage valide
    if ((temp_to_convert >= -32000) && (temp_to_convert <= 32000)) {
        temp_to_convert -= 3200;                    //        �C = (�F - 32) * 5 / 9
        temp_to_convert = (int16s)( ((int32s)temp_to_convert * 5) / 9);
                                                    //        arrondis (s'il y a lieu)
        temp_to_convert = TLS_Round(temp_to_convert, round);
    }

    return temp_to_convert;
}
