/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       MenuHandler.c

    \brief      Source code file for the MenuHandler module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the MenuHandler core implementation.

    Requirement: Context definitions specific to each project are expected
                in ContextCollection.c.

********************************************************************************
    \section Change log
    
    March 7 2018 :
        Add a recursive mutex "mutex_coreMenu" to control the flow when
        another thread than HMI wants to activate a new display menu.        
*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "OsTask.h"
#include "MenuTable.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"

#include ".\HAL\inc\Display.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/


/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/
#include "cmsis_os.h"
#ifdef FREERTOS_SUPPORT
    #include "trcRecorder.h"
    extern char* hmiEventChannel;
#endif


/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/

static int8u MnhActiveMenu;         //!< Contains the active menu
static int8u MnhPreviousMenu;       //!< Contains the previous menu
static int16u MnhActiveUI;          //!< Active UI

static MnhTimeoutCallback_t MnhTimeoutCallback;
static void* MnhTimeoutCallbackArgument;

static SemaphoreHandle_t mutex_coreMenu;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/

/*******************************************************************************
    Private functions definitions
*******************************************************************************/


/*******************************************************************************
    Public functions definitions
*******************************************************************************/

void MNH_Init(void)
{
    mutex_coreMenu = xSemaphoreCreateRecursiveMutex();
    configASSERT(mutex_coreMenu);
    
    MnhActiveMenu = MENU_INIT;          //need to init variable to a known state
                                        // before running MNH_SetActiveMenu()
    MNH_MenusInit();
    
}

/*******************************************************************************
    MNH_tCallMenu

    Task id: TSK_MNH_MENU

    Execute the current menu function
*******************************************************************************/
OSTASK_DEF(MNH_tCallMenu)
{
    xSemaphoreTakeRecursive(mutex_coreMenu,portMAX_DELAY);
    if((MnhActiveMenu != NULL) && (MnhActiveMenu < GetMenuCount())) {

        if (Menus[MnhActiveMenu].core != NULL)
        {
            vTracePrintF(hmiEventChannel, "Starting core %d", MnhActiveMenu);
            (Menus[MnhActiveMenu].core)();
            vTracePrintF(hmiEventChannel, "Core %d ends", MnhActiveMenu);
//            OS_StartTask(TSK_APP_DPH_UPDSCREEN);
        }
    }
    xSemaphoreGiveRecursive(mutex_coreMenu);

    return (OSTASK_IS_IDLE);
}

/*******************************************************************************
    MNH_GetActiveMenu

    Get the active menu

    \return Index of the current menu
*******************************************************************************/

int8u MNH_GetActiveMenu(void)
{
    return MnhActiveMenu;
}

/*******************************************************************************
    MNH_GetPreviousMenu

    Get the previous menu

    \return Index of the previous menu
*******************************************************************************/

int8u MNH_GetPreviousMenu(void)
{
    return MnhPreviousMenu;
}

/*******************************************************************************
    MNH_SetActiveMenu

    Set the active menu

    \param menu Index of the target menu
*******************************************************************************/
void MNH_SetActiveMenu(calling_conv_t menu)
{
    xSemaphoreTakeRecursive(mutex_coreMenu,portMAX_DELAY);
    
    if (menu != MnhActiveMenu && menu < GetMenuCount()) {

        vTracePrintF(hmiEventChannel, "Exiting menu %d", MnhActiveMenu);
        if (Menus[MnhActiveMenu].exit != NULL) {

            (Menus[MnhActiveMenu].exit)();
        }
        MnhPreviousMenu = MnhActiveMenu;
        MnhActiveMenu = (int8u)menu;
		MNH_ClearTimeoutCallback();

        if (Menus[MnhActiveMenu].entry != NULL)
        {
            if (MnhActiveMenu != MENU_IDLE)
            {
                HAL_ClearScreen();
            }
            (Menus[MnhActiveMenu].entry)();
            vTracePrintF(hmiEventChannel, "Entering menu %d", MnhActiveMenu);
        }
        OS_StartTask(TSK_APP_MNH_MENU);
    }
    xSemaphoreGiveRecursive(mutex_coreMenu);
    
}

/*******************************************************************************
    MNH_GetUI

    Get the active UI

    \return Index of the current UI
*******************************************************************************/

int16u MNH_GetUI(void)
{
    return MnhActiveUI;
}

/*******************************************************************************
    MNH_SetTimeoutCallback
*******************************************************************************/
void MNH_SetTimeoutCallback(MnhTimeoutCallback_t callback,  void* arg, U16BIT delay)
{
	MnhTimeoutCallback = callback;
	MnhTimeoutCallbackArgument = arg;
	OS_DelayTask(TSK_APP_MNH_EVENTS,OS_MSTOTICK(delay));
}

/*******************************************************************************
    MNH_tUiTimedEvents
*******************************************************************************/
OSTASK_DEF(MNH_tUiTimedEvents)
{
    // Important as this routine might be called by confirm key
    OS_StopTask(TSK_APP_MNH_EVENTS);

    //Check if any special callback must be made before calling the active menu.
	if (MnhTimeoutCallback != NULL)
	{
		MnhTimeoutCallback(MnhTimeoutCallbackArgument);
	}

    OS_StartTask(TSK_APP_MNH_MENU);

    return (OSTASK_IS_IDLE);
}

/*******************************************************************************
    MNH_ClearTimeoutCallback
*******************************************************************************/
void MNH_ClearTimeoutCallback(void)
{
	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)NULL,  NULL, NULL);
}
