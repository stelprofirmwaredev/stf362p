/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       ResetMenu.c

    \brief      Source code file for the ResetMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the ResetMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "main.h"
#include "ResetMenu.h"

#include ".\DB\inc\THREAD_DB.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e           ResetEditState = EDIT_IDLE;
static Reset_t                  ResetEditValue;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void ChangeReset(KEYB_PARAM keyId);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    ChangeReset

    Change the reset intention steps
*******************************************************************************/
static void ChangeReset(KEYB_PARAM keyId)
{
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Reset_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);

    if (ResetEditValue == NO_RESET)
    {
        ResetEditValue = RESET_WANTED;
    }
    else if (ResetEditValue == RESET_WANTED)
    {
        ResetEditValue = RESET_CONFIRMED;
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Reset_Commit, (KEYB_PARAM*)KEY_DONE, IMMEDIATELY);
    }

    DPH_DisplayReset(ResetEditValue);
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_Reset_Entry
*******************************************************************************/
void MNU_Reset_Entry(void)
{
    ResetEditValue = NO_RESET;
    ResetEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show reset intentions steps
    //
    DPH_DisplayReset(ResetEditValue);

    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Reset_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_Reset_Core

    Allows to reset the thermostat to the factory default
*******************************************************************************/
void MNU_Reset_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_Reset_Commit, (void*)KEY_BACK);
        KBH_SetKeyPressProperties(KEY_DONE, (FUNC)ChangeReset, (void*)KEY_DONE);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_Reset_Exit
*******************************************************************************/
void MNU_Reset_Exit(void)
{
    ResetEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_Reset_Commit
*******************************************************************************/
void MNU_Reset_Commit(KEYB_PARAM keyId)
{
    static KEYB_PARAM commitKey = NULL;

    if ( ResetEditState == EDIT_IN_PROGRESS )
    {
        commitKey = keyId;
        KBH_ClearAllKeyProperties();
        DPH_DisplayReset(ResetEditValue);
        switch (commitKey)
        {
            case KEY_DONE:
                if (ResetEditValue == RESET_CONFIRMED)
                {
                    ClearDatabase(CLEAR_DB_REASON_RESET_TO_DEFAULT);
                }
                break;

            case KEY_BACK:
            default:
                MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Reset_Commit, (KEYB_PARAM*)commitKey, IMMEDIATELY);
                break;
        }
        ResetEditState = EDIT_COMMITTING;
    }
    else if ( ResetEditState == EDIT_COMMITTING )
    {
        ResetEditState = EDIT_IDLE;
        switch (commitKey)
        {
            case KEY_BACK:
                MNH_SetActiveMenu(MENU_ADV_SETTINGS);
                break;

            case KEY_DONE:
                //Do nothing, wait for the reset to occur
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
    }
}
