/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2017, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       PIRMenu.c

    \brief      Source code file for the PIRMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the PIRMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "PIRMenu.h"
#include ".\DB\inc\THREAD_DB.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e           PIREditState = EDIT_IDLE;
static PIR_t                    PIREditValue;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void ChangePIR(KEYB_PARAM keyId);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    ChangePIR

    Change the PIR setting to either Enabled or Disabled
*******************************************************************************/
static void ChangePIR(KEYB_PARAM keyId)
{
    switch (keyId)
    {
        case KEY_OPTIONS_2:
            PIREditValue = PIR_DISABLED;
            break;

        case KEY_OPTIONS_3:
            PIREditValue = PIR_ENABLED;
            break;

        default:
            break;
    }

    DPH_DisplayPIR(PIREditValue, FALSE);
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_PIR_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_PIR_Entry
*******************************************************************************/
void MNU_PIR_Entry(void)
{
    DBTHRD_GetData(DBTYPE_PIR_SETTING,(void*)&PIREditValue, INDEX_DONT_CARE);
    PIREditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show PIR setting
    //
    DPH_DisplayPIR(PIREditValue, TRUE);

    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_PIR_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_PIR_Core

    Allows to change the PIR setting
*******************************************************************************/
void MNU_PIR_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_PIR_Commit, (void*)KEY_BACK);
        KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)ChangePIR, (void*)KEY_OPTIONS_2);
        KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)ChangePIR, (void*)KEY_OPTIONS_3);
        KBH_SetKeyPressProperties(KEY_DONE, (FUNC)MNU_PIR_Commit, (void*)KEY_DONE);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_PIR_Exit
*******************************************************************************/
void MNU_PIR_Exit(void)
{
    PIREditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_PIR_Init
*******************************************************************************/
void MNU_PIR_Init(void)
{
    MNU_PIR_SetPIR(PIR_ENABLED);
}

/*******************************************************************************
    MNU_PIR_Commit
*******************************************************************************/
void MNU_PIR_Commit(KEYB_PARAM keyId)
{
    static KEYB_PARAM commitKey = NULL;

    if ( PIREditState == EDIT_IN_PROGRESS )
    {
        commitKey = keyId;
        switch (commitKey)
        {
            case KEY_DONE:
                MNU_PIR_SetPIR(PIREditValue);
                //No break on purpose

            case KEY_BACK:
            default:
                MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_PIR_Commit, (KEYB_PARAM*)commitKey, IMMEDIATELY);
                break;
        }
        PIREditState = EDIT_COMMITTING;
		KBH_ClearAllKeyProperties();
    }
    else if ( PIREditState == EDIT_COMMITTING )
    {
        PIREditState = EDIT_IDLE;
        switch (commitKey)
        {
            case KEY_BACK:
            case KEY_DONE:
                MNH_SetActiveMenu(MENU_OPTIONS);
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
    }
}


/*******************************************************************************
    MNU_PIR_SetPIR

    Set the PIR setting to Enabled or Disabled
*******************************************************************************/
void MNU_PIR_SetPIR(PIR_t pirSetting)
{
    DBTHRD_SetData(DBTYPE_PIR_SETTING,(void*)&pirSetting, INDEX_DONT_CARE, DBCHANGE_LOCAL);
}
