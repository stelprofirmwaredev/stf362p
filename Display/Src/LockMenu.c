/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       LockMenu.c

    \brief      Source code file for the LockMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the LockMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "LockMenu.h"
#include ".\DB\inc\THREAD_DB.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/

/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   LockEditState = EDIT_IDLE;
static Lock_t	    LockEditValue;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void ChangeLock(KEYB_PARAM keyId);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    ChangeLock

    Toggle the Lock
*******************************************************************************/
static void ChangeLock(KEYB_PARAM keyId)
{
    switch (keyId)
    {
        case KEY_OPTIONS_2:
            LockEditValue = LOCK_OFF;
            break;

        case KEY_OPTIONS_3:
            LockEditValue = LOCK_ON;
            break;

        default:
            break;
    }

    DPH_DisplayLockSettings(LockEditValue, FALSE);
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Lock_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_Lock_Entry
*******************************************************************************/
void MNU_Lock_Entry(void)
{
    DBTHRD_GetData(DBTYPE_LOCK_STATE,(void*)&LockEditValue, THIS_THERMOSTAT);

    LockEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show Lock options
    //
    DPH_DisplayLockSettings(LockEditValue, TRUE);

    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Lock_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_Lock_Core

    Allow changing the lock state
*******************************************************************************/
void MNU_Lock_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_Lock_Commit, (void*)KEY_BACK);
        KBH_SetKeyPressProperties(KEY_DONE, (FUNC)MNU_Lock_Commit, (void*)KEY_DONE);
        KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)ChangeLock, (void*)KEY_OPTIONS_2);
        KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)ChangeLock, (void*)KEY_OPTIONS_3);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_Lock_Exit
*******************************************************************************/
void MNU_Lock_Exit(void)
{
    LockEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_Lock_Init
*******************************************************************************/
void MNU_Lock_Init(void)
{
	LockEditValue = LOCK_OFF;
    DBTHRD_SetData(DBTYPE_LOCK_STATE,(void*)&LockEditValue, 0, DBCHANGE_LOCAL);
}

/*******************************************************************************
    MNU_Lock_Commit
*******************************************************************************/
void MNU_Lock_Commit(KEYB_PARAM keyId)
{
    static KEYB_PARAM commitKey = NULL;

    if ( LockEditState == EDIT_IN_PROGRESS )
    {
        commitKey = keyId;
        KBH_ClearAllKeyProperties();
        switch (commitKey)
        {
            case KEY_DONE:
                DBTHRD_SetData(DBTYPE_LOCK_STATE,(void*)&LockEditValue, THIS_THERMOSTAT, DBCHANGE_LOCAL);

            case KEY_BACK:
            default:
                MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Lock_Commit, (KEYB_PARAM*)commitKey, IMMEDIATELY);
                break;
        }
        OS_StartTask(TSK_LIMIT_CHECK);
        LockEditState = EDIT_COMMITTING;
    }
    else if ( LockEditState == EDIT_COMMITTING )
    {
        LockEditState = EDIT_IDLE;
        switch (commitKey)
        {
            case KEY_BACK:
            case KEY_DONE:
                MNH_SetActiveMenu(MENU_OPTIONS);
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
    }
}
