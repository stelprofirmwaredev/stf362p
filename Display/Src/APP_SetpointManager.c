/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    APP_SetpointManager.c
* @date    2016/10/25
* @authors Jean-Fran�ois Many
* @brief   Setpoint Manager Module
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include "typedef.h"
#include "convert.h"
#include "common_types.h"
#include "string.h"
#include <stdlib.h>
#include ".\DB\inc\THREAD_DBDataTypes.h"
#include ".\DB\inc\THREAD_DB.h"
#include "APP_SetpointManager.h"
#include "HomeMenu.h"
#include "Program_Tracking.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define HIGHEST_ANTICIPATION_TEMPERATURE 0      //0�C
#define LOWEST_ANTICIPATION_TEMPERATURE -2500   //-25�C
#define FASTEST_ANTICIPATION_SLOPE  4   //Temp rises 4�C/h
#define AVERAGE_ANTICIPATION_SLOPE  3   //Temp rises 3�C/h
#define SLOWEST_ANTICIPATION_SLOPE  2   //Temp rises 2�C/h
#define LINEAR_SLOPE_X100           8   //0.08�C / hour
#define OFFSET_SLOPE_X100           400 //4�C --> [0.08x + 4]
#define MAX_ANTICIPATION_TIME       120 //2h --> 120 minutes
#define ACTIVITY_NOT_FOUND          20000

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/


/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static DB_NOTIFICATION(OnMinuteChange);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/11/21
*******************************************************************************/
static DB_NOTIFICATION(OnMinuteChange)
{
    ScheduleModeEnum_t scheduleMode;

    DBTHRD_GetData(DBTYPE_SCHEDULE_MODE,(void*)&scheduleMode, THIS_THERMOSTAT);
    if ((scheduleMode == MODE_PROG) || (scheduleMode == MODE_DEROGATION))
    {
        CheckProgramTracking();
    }
}


/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  Setpoint Manager Init Function
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/10/25
*******************************************************************************/
void STPM_Init(void)
{
    DBTHRD_SubscribeToNotification (DBTYPE_MINUTE_CHANGE,
                                   OnMinuteChange,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
}

/*******************************************************************************
* @brief  Calculate the minimum setpoint
* @inputs None
* @retval uint16_t: minimum setpoint value
* @author Jean-Fran�ois Many
* @date   2016/10/25
*******************************************************************************/
uint16_t STPM_GetSetpointMin(void)
{
    uint16_t setpointMin;
    UnitFormat_t format;

    DBTHRD_GetData(DBTYPE_TEMPERATURE_FORMAT,(void*)&format, INDEX_DONT_CARE);
    setpointMin = MINIMUM_SETPOINT;
    if(format == DEGREE_F)
    {
        setpointMin = CVT_ConvertCtoF(setpointMin, 100);
        setpointMin = CVT_ConvertFtoC(setpointMin, 0);
    }
    if (setpointMin < MINIMUM_SETPOINT)
    {
        setpointMin = MINIMUM_SETPOINT;
    }
    else
    {
        ;
    }
    return setpointMin;
}

/*******************************************************************************
* @brief  Calculate the maximum setpoint
* @inputs None
* @retval uint16_t: maximum setpoint value
* @author Jean-Fran�ois Many
* @date   2016/10/25
*******************************************************************************/
uint16_t STPM_GetSetpointMax(void)
{
    uint16_t setpointMax;
    UnitFormat_t format;
    uint8_t floorMode;
    FloorType_t floorType;

    DBTHRD_GetData(DBTYPE_TEMPERATURE_FORMAT,(void*)&format, INDEX_DONT_CARE);
    DBTHRD_GetData(DBTYPE_FLOOR_MODE,(void*)&floorMode, THIS_THERMOSTAT);
    DBTHRD_GetData(DBTYPE_FLOOR_TYPE,(void*)&floorType, THIS_THERMOSTAT);
    setpointMax = MAXIMUM_SETPOINT;
    if (floorMode == CONTROLMODE_FLOOR)
    {
        if (floorType == FLOORTYPE_TILE)
        {
            setpointMax = MAXIMUM_FLOOR_SETPOINT;
        }
        else
        {
            setpointMax = MAXIMUM_ENGINEERED_FLOOR_SETPOINT;
        }
    }
    if(format == DEGREE_F)
    {
        setpointMax = CVT_ConvertCtoF(setpointMax, 100);
        setpointMax = CVT_ConvertFtoC(setpointMax, 0);
    }
    if (floorMode == CONTROLMODE_FLOOR)
    {
        if (floorType == FLOORTYPE_TILE)
        {
            if (setpointMax > MAXIMUM_FLOOR_SETPOINT)
            {
                setpointMax = MAXIMUM_FLOOR_SETPOINT;
            }
        }
        else
        {
            if (setpointMax > MAXIMUM_ENGINEERED_FLOOR_SETPOINT)
            {
                setpointMax = MAXIMUM_ENGINEERED_FLOOR_SETPOINT;
            }
        }
    }
    else
    {
        if (setpointMax > MAXIMUM_SETPOINT)
        {
            setpointMax = MAXIMUM_SETPOINT;
        }
    }
    return setpointMax;
}

/*******************************************************************************
* @brief  Limit Check task
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/08/01
*******************************************************************************/
OSTASK_DEF(STPIF_LimitCheck)
{
    uint16_t setpointToCheck;
    uint16_t min;
    uint16_t max;
    FloorType_t floorType;

    max = STPM_GetSetpointMax();
    min = STPM_GetSetpointMin();
    DBTHRD_GetData(DBTYPE_AMBIENT_SETPOINT,(void*)&setpointToCheck, THIS_THERMOSTAT);
    if (setpointToCheck < min)
    {
        DBTHRD_SetData(DBTYPE_AMBIENT_SETPOINT,(void*)&min, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    }
    if (setpointToCheck > max)
    {
        DBTHRD_SetData(DBTYPE_AMBIENT_SETPOINT,(void*)&max, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    }

    DBTHRD_GetData(DBTYPE_FLOOR_SETPOINT,(void*)&setpointToCheck, THIS_THERMOSTAT);
    if (setpointToCheck < min)
    {
        DBTHRD_SetData(DBTYPE_FLOOR_SETPOINT,(void*)&min, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    }
    if (setpointToCheck > max)
    {
        DBTHRD_SetData(DBTYPE_FLOOR_SETPOINT,(void*)&max, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    }

    DBTHRD_GetData(DBTYPE_FLOOR_LIMIT,(void*)&setpointToCheck, THIS_THERMOSTAT);
    DBTHRD_GetData(DBTYPE_FLOOR_TYPE,(void*)&floorType, THIS_THERMOSTAT);
    if (floorType == FLOORTYPE_TILE)
    {
        max = MAXIMUM_FLOOR_SETPOINT;
    }
    else
    {
        max = MAXIMUM_ENGINEERED_FLOOR_SETPOINT;
    }
    min = MINIMUM_FLOOR_LIMIT;
    if (setpointToCheck < min)
    {
        DBTHRD_SetData(DBTYPE_FLOOR_LIMIT,(void*)&min, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    }
    if (setpointToCheck > max)
    {
        DBTHRD_SetData(DBTYPE_FLOOR_LIMIT,(void*)&max, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    }

    return (OSTASK_IS_IDLE);
}

/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
