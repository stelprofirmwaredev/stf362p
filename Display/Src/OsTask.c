/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       OsTask.c

    \brief      Source code file for the OS task scheduler module.

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the OS task scheduler implementation.

*******************************************************************************/

/*-------------------------------------------------------------------------------
    INCLUDE
-------------------------------------------------------------------------------*/
#include "typedef.h"
#include "HAL_Interrupt.h"
//#include "HalClock.h"
//#include "HalWdog.h"
//#include "HalLowPower.h"
//
#include "OsTaskSetup.h"

#include "Os.h"
#include "cmsis_os.h"

#ifdef DAC_BACKLIGHT
#include ".\HAL\inc\Display.h"
#endif /* DAC_BACKLIGHT */

/*-------------------------------------------------------------------------------
    DEFINITIONS
-------------------------------------------------------------------------------*/
//
//! Number of elements of task table required
//
#define OS_TABLE_SIZE         (((OS_NB_TASK-1)/(sizeof(int8u)*8))+1)

/*-------------------------------------------------------------------------------
    DATA
-------------------------------------------------------------------------------*/
//
//! Task ID for no task
//
//static const int8u OS_NO_TASK = 0xff;


//
//! Table used to convert numbers from 0-7 to bit position
//
FLASH_MEM const int8u MapTbl[8] =
{
    0x01,
    0x02,
    0x04,
    0x08,
    0x10,
    0x20,
    0x40,
    0x80,
};


//
//! Table used to flag tasks that have ran
//
static volatile int8u OsTskWdogTbl[OS_TABLE_SIZE];


//
//! Table used to flag ready tasks
// *** Protect access in critical region ***
//
static volatile int8u OsTskRdyTbl[OS_TABLE_SIZE];
static int8u OsTskLPStatus[OS_TABLE_SIZE];

//
//! timer used to manage software wdog
//
static OsTaskDelay_t OsWdogTmr;


//
//! Table used to manage task delays
// *** Protect access in critical region ***
//
static OsTaskDelay_t OsTskDelayTmr[OS_NB_TASK];


//
//! Current tick count
//
static volatile OsTickCount_t OsTickCnt = 0;


//
//! Number of tick to be processed by scheduler task
//
static int8u OsTickCntDiff = 0;
int16u oscnt=0;
//
//! Table used to store execution time statistics (if enabled)
//
#ifdef OS_STATS_ENABLED
    #define OS_STACK_LENGTH             20
    #define OS_STATS_MAINLOOP_LENGTH    50
    #define OS_MAX_TASK_LENGTH          5500            //expressed in microseconds
    #define OS_MAX_MAIN_LOOP            3500           //expressed in microseconds
    __no_init OsTaskStats_t OsTskStatsTbl[OS_NB_TASK];
    __no_init OsTaskId_t OSTaskStack[OS_STACK_LENGTH];
    __no_init int8u OsTaskStackCnt;
    __no_init OsMainLoopTiming_t MainLoopTiming[OS_STATS_MAINLOOP_LENGTH];
    __no_init OsTaskStats_t MainLoopStats;
    __no_init int16u LoopDuration;
#endif // OS_STATS_ENABLED

#ifdef FREERTOS_SUPPORT
    #include "trcRecorder.h"
    extern char* hmiEventChannel;
#endif

#ifdef DAC_BACKLIGHT
extern DAC_HandleTypeDef hdac;
#endif /* DAC_BACKLIGHT */

/*-------------------------------------------------------------------------------
    DECLARATIONS
-------------------------------------------------------------------------------*/
static OsTaskId_t OS_GetRdyTask(void);
INLINE int8u OS_GetLSBitSet(int8u byte);
static void OS_SetTaskWdog(OsTaskId_t taskId);
static void OS_ManageWdogTmr(void);
static FLAG OS_CheckTaskWdog(OsTaskId_t taskId);
static void OS_CriticalError(void);
static void OS_ClearAllTasks(void);
static void OS_ManageTaskDelay(void);
void OS_SetTaskLPMode (OsTask_Status_t status, OsTaskId_t taskId);

/*-------------------------------------------------------------------------------
    IMPLEMENTATION
-------------------------------------------------------------------------------*/

/***************************************************************************//*!
*	\brief	OS init
*
*   Init the OS, reset the taskdelay table, call external function OSCPU_Init().
*   OSCPU_Init() must be externally defined, and should activate a periodic
*   interrupt to occur.
*
*******************************************************************************/
void OS_Init(void)
{
    // Init os software wdog timer
    OsWdogTmr = OS_TASK_WDOG_PERIOD;

    // Reset all tasks
    OS_ClearAllTasks();

    #ifdef OS_STATS_ENABLED
    {
        // Reset task execution time statistics
        OS_ResetStats();
    }
    #endif // OS_STATS_ENABLED

    // Idle task always active
    OS_StartTask(TSK_OS_IDLE);
}


/***************************************************************************//*!
*	\brief	Starts a task.
*
*   \param taskId
*     Task identifier
*
*******************************************************************************/
void OS_StartTask(OsTaskId_t taskId)
{
    int8u mask;
    int8u index;

    // if task is valid
    if(taskId < OS_NB_TASK) {
        mask = MapTbl[taskId & 0x07];
        index = taskId >> 3;

        // Enter critical
        BSP_InterruptDeclareCritical();
        BSP_InterruptEnterCritical();
        {
            OsTskDelayTmr[taskId] = 0;
            // Mark task active
            OsTskRdyTbl[index] |= mask;
        }
        // Leave critical
        BSP_InterruptExitCritical();
    // endif
    }
}


/***************************************************************************//*!
*	\brief	Stops a task.
*
*   \param taskId
*     Task identifier
*
*******************************************************************************/
void OS_StopTask(OsTaskId_t taskId)
{
    int8u mask;
    int8u index;

    // if task is valid
    if(taskId < OS_NB_TASK) {
        mask = MapTbl[taskId & 0x07];
        index = taskId >> 3;

        // Enter critical
        BSP_InterruptDeclareCritical();
        BSP_InterruptEnterCritical();
        {
            // Desactivate task
            OsTskRdyTbl[index] &= ~mask;

            // Deativate task period timer
            OsTskDelayTmr[taskId] = 0;
        }
        // Leave critical
        BSP_InterruptExitCritical();
    // endif
    }
}


/***************************************************************************//*!
*	\brief	Sets a task to start in 'n' ticks.
*
*   \param taskId
*     Task identifier
*   \param delay
*     delay (in tick units)
*
*******************************************************************************/
void OS_DelayTask(OsTaskId_t taskId, OsTaskDelay_t delay)
{
    // if task is valid
    if(taskId < OS_NB_TASK) {

        // Enter critical
        BSP_InterruptDeclareCritical();
        BSP_InterruptEnterCritical();
        {
            OsTskDelayTmr[taskId] = delay;
        }
        // Leave critical
        BSP_InterruptExitCritical();
    // endif
    }
}


/***************************************************************************//*!
*	\brief	Get time current timer value
*
*   Interface for backward compatibility with Redlink.
*
*   \param taskId
*     Task identifier
*   \return current timer
*
*******************************************************************************/
OsTaskDelay_t OS_GetTaskTimer(OsTaskId_t taskId)
{
    OsTaskDelay_t tmr = 0;

    // if task is valid
    if (taskId < OS_NB_TASK) {
        BSP_InterruptDeclareCritical();
        BSP_InterruptEnterCritical();
        {
            tmr = OsTskDelayTmr[taskId];
        }
        // Leave critical
        BSP_InterruptExitCritical();
    }

    return (tmr);
}


/***************************************************************************//*!
*	\brief	Get current tick count value
*
*   \return current OC tick count value
*
*******************************************************************************/
OsTickCount_t OS_GetTickCount(void)
{
    return (OsTickCnt);
}


/***************************************************************************//*!
*	\brief	OS Management
*
*   Execute the highest priority task ready to execute
*
*   \sa     GetRdyTask(), OS_StartTask(), OS_EnableTask()
*
*******************************************************************************/
void OS_DoEvents(void)
{
    OsTaskId_t taskId;

    // Retrieve ready task
    taskId = OS_GetRdyTask();

    if(taskId != OS_NO_TASK) {

        // Stop task to init is known state
        OS_StopTask(taskId);

        // if task entry is valid
        if(OS_TASK_TABLE[taskId].taskEntry != NULL) {

            BSP_InterruptDeclareCritical();

            // Enter critical
            BSP_InterruptEnterCritical();
            {
                // Restore task delay cleared by OS_StopTask
                OsTskDelayTmr[taskId] = OS_TASK_TABLE[taskId].taskAutoReloadDelay;
            }
            // Leave critical
            BSP_InterruptExitCritical();

            // Call task entry point

            #ifdef OS_STATS_ENABLED
            {
                // Statistics
                static int16u duration, timestamp, snapshot;
                BSP_InterruptEnterCritical();
                {
                    // Save entry time
                    timestamp = OS_STATS_TIMER;
                }
                BSP_InterruptExitCritical();


                OSTaskStack[OsTaskStackCnt] = taskId;

                if (MainLoopTiming[MainLoopStats.task_run_cnt].TaskTraceCnt >= MAX_LOOP_TRACE)
                {
                    MainLoopTiming[MainLoopStats.task_run_cnt].LoopTrace[MAX_LOOP_TRACE-1] = taskId;
                }
                else
                {
                    MainLoopTiming[MainLoopStats.task_run_cnt].LoopTrace[MainLoopTiming[MainLoopStats.task_run_cnt].TaskTraceCnt] = taskId;
                }
                MainLoopTiming[MainLoopStats.task_run_cnt].TaskTraceCnt++;
            #endif // OS_STATS_ENABLED
#ifdef FREERTOS_SUPPORT
                vTracePrintF(hmiEventChannel, "Starting OS Task # %d",taskId);
#endif
            OS_SetTaskLPMode(OS_TASK_TABLE[taskId].taskEntry(),taskId);
            #ifdef OS_STATS_ENABLED
                // Enter critical
                BSP_InterruptEnterCritical();

                {
                    // Calculate task duration
                    snapshot = OS_STATS_TIMER;
                    if(snapshot >= timestamp) {
                        duration = snapshot - timestamp;
                    }
                    else {
                        duration = ((0xffff - timestamp) + snapshot);
                    }
                }
                // Leave critical
                BSP_InterruptExitCritical();

                if ((duration > OS_MAX_TASK_LENGTH) && (taskId != TSK_OS_IDLE))
                {
                    __no_operation();
                }
                // Save task max duration
                if(duration < OsTskStatsTbl[taskId].min) {
                    OsTskStatsTbl[taskId].min = duration;
                }

                if(duration > OsTskStatsTbl[taskId].max) {
                    OsTskStatsTbl[taskId].max = duration;
                }

                if(OsTskStatsTbl[taskId].avg == 0) {
                    OsTskStatsTbl[taskId].avg = duration;
                }
                else
                {
                    OsTskStatsTbl[taskId].avg = ((OsTskStatsTbl[taskId].avg + duration) >> 1);
                }

                OsTskStatsTbl[taskId].task_run_cnt++;

                if(taskId != TSK_OS_IDLE)
                {
                    LoopDuration += duration;
                }

                OsTaskStackCnt++;
                if (OsTaskStackCnt >= OS_STACK_LENGTH)
                {
                    OsTaskStackCnt = 0;
                }
            }
            #endif // OS_STATS_ENABLED
            // Reset task wdog timer
            OS_SetTaskWdog(taskId);

        // endif
        }
    }
}

/***************************************************************************//*!
*	\brief
*
*******************************************************************************/
void OS_SetTaskLPMode (OsTask_Status_t status, OsTaskId_t taskId)
{
    int8u mask;
    int8u index;

    // if task is valid
    if(taskId < OS_NB_TASK)
    {
        mask = MapTbl[taskId & 0x07];
        index = taskId >> 3;

        if (status != 0)
        {
            OsTskLPStatus[index] |= mask;
        }
        else
        {
            OsTskLPStatus[index] &= ~mask;
        }
    }
}

/***************************************************************************//*!
*	\brief
*
*******************************************************************************/
int8u OS_GetTaskLPMode (void)
{
    int8u i;

    for (i = 0; i < OS_TABLE_SIZE; i++)
    {
        if (OsTskLPStatus[i] != 0)
        {
            return ((int8u) OSTASK_IS_WAITING_FOR_PERIPHERAL_EVENT);
        }
    }
    return ((int8u) OSTASK_IS_IDLE);
}

/***************************************************************************//*!
*	\brief	OS_TaskIdle
*
*   Scheduled when there is not any other task to be scheduled
*   (i.e. no ready task for the moment).
*
*******************************************************************************/
OSTASK_DEF(OS_TaskIdle)
{
#ifdef OS_STATS_ENABLED
    int8u i;
#endif

    BSP_InterruptDeclareCritical();

    /* because task status can be held on more than 1 byte  */
    /* and that it can be modified during an interrupt,     */
    /* we must avoid parsing only a part of the status flag */
    BSP_InterruptEnterCritical();
    {
        OS_IsTaskReady();
    }
    BSP_InterruptExitCritical();

#ifdef OS_STATS_ENABLED
    MainLoopTiming[MainLoopStats.task_run_cnt++].LoopTime = LoopDuration;
    if (MainLoopStats.task_run_cnt >= OS_STATS_MAINLOOP_LENGTH)
    {
        MainLoopStats.task_run_cnt = 0;
    }

    MainLoopTiming[MainLoopStats.task_run_cnt].LoopTime = 0;
    MainLoopTiming[MainLoopStats.task_run_cnt].TaskTraceCnt = 0;
    for (i=0; i < MAX_LOOP_TRACE;i++)
    {
         MainLoopTiming[MainLoopStats.task_run_cnt].LoopTrace[i] = OS_NO_TASK;
    }

    // Save task max duration
    if(LoopDuration < MainLoopStats.min) {
        MainLoopStats.min = LoopDuration;
    }
    if(LoopDuration > MainLoopStats.max) {
        MainLoopStats.max = LoopDuration;
    }
    if(MainLoopStats.avg == 0) {
        MainLoopStats.avg = LoopDuration;
    }
    else {
        MainLoopStats.avg = ((MainLoopStats.avg + LoopDuration) >> 1);
    }

    if (LoopDuration > OS_MAX_MAIN_LOOP)
    {
        __no_operation();
    }

    LoopDuration = 0;
#endif

    //Idle task always active
    OS_StartTask(TSK_OS_IDLE);

    return (OSTASK_IS_IDLE);
}


/***************************************************************************//*!
*	\brief	OS_TaskScheduler
*
*   Scheduler task responsible to schedule other task of the system.
*
*******************************************************************************/
OSTASK_DEF(OS_TaskScheduler)
{
    int8u numOfTicks;

    // Enter critical
    BSP_InterruptDeclareCritical();
    BSP_InterruptEnterCritical();
    {
        // get and reset tick difference since last execution
        numOfTicks = OsTickCntDiff;
        OsTickCntDiff = 0;
    }
    // Leave critical
    BSP_InterruptExitCritical();

    // If ever tasks are taking too much CPU time, we may end up with situations
    // where the schedule did not have a chance to run on every tick. So here, we
    // process the number of tick we should process.
    while(numOfTicks--)
    {
        OsTickCnt++;

        // Decrement all task timers and start task for which timers are elapsed
        OS_ManageTaskDelay();

        // Decrement wdog timer and check tasks when it elapses
        OS_ManageWdogTmr();
    }
    return (OSTASK_IS_IDLE);
}


/***************************************************************************//*!
*	\brief	Stop all tasks
*
*   Stops all active task and delayed task
*
*******************************************************************************/
static void OS_ClearAllTasks(void)
{
    int8u i, j;

    BSP_InterruptDeclareCritical();
    BSP_InterruptEnterCritical();
    {
        // Remove all ready tasks
        for(j = 0; j < OS_TABLE_SIZE; j++)
        {
            OsTskRdyTbl[j] = (OsTaskId_t) 0;
            OsTskWdogTbl[j] = (OsTaskId_t) 0;
        }

        for(i = 0; i < OS_NB_TASK; i++)
        {
            // Reset all task delays os the task delay table
            OsTskDelayTmr[i] = 0;
        }
    }
    BSP_InterruptExitCritical();
}

/***************************************************************************//*!
*	\brief	Is task ready
*
*   Verify if a task is ready to be served
*
*   \return 		TRUE : at least one task is ready
*                   FALSE : no tasks ready
*
*******************************************************************************/
int8u OS_IsTaskReady(void)
{
    int8u i;
    for (i = 0; i<OS_TABLE_SIZE; i++)
    {
        if (OsTskRdyTbl[i] != 0)
        {
            return (TRUE);
        }
    }
    return (FALSE);
}

/***************************************************************************//*!
*	\brief	Get task ready
*
*   Find the next ready task - INDEX.
*
*   \return 		Next ready task, one of OsTaskId_t
*           		If no task ready returns OS_NO_TASK
*
*******************************************************************************/
static OsTaskId_t OS_GetRdyTask(void)
{
    int8u i, rdymask;

    // Scan task table
    for(i = 0; i < OS_TABLE_SIZE; i++) {
        rdymask = OsTskRdyTbl[i];

        // A task is active
        if(rdymask != 0) {
            //Get highest priority task
            return ((OsTaskId_t)(OS_GetLSBitSet(rdymask) + i * 8));
        }
    }
    return(OS_NO_TASK);
}


/***************************************************************************//*!
*	\brief	Delay manager
*
*   Perform and manage ongoing delays, must be called once every Operating
*   System RTI (TIC)
*
*   \sa OS_TaskDelay(), OS_ClearTaskDelay()
*
*******************************************************************************/
static void OS_ManageTaskDelay(void)
{
    FLAG sched;
    int8u i;

    // for all tasks
    for(i = 0; i < OS_NB_TASK; i++) {

        sched = FALSE;

        // Enter critical
        BSP_InterruptDeclareCritical();
        BSP_InterruptEnterCritical();
        {
            // if delay running
            if(OsTskDelayTmr[i]) {
                // decrement running delay
                OsTskDelayTmr[i]--;
                // check if delay timer just elapsed
                if (OsTskDelayTmr[i] == 0)
                {
                    sched = TRUE;
                }
            }
        }
        // Leave critical
        BSP_InterruptExitCritical();

        if (sched == TRUE) {
            // start task
            OS_StartTask(OS_TASK_TABLE[i].taskId);
        }
    // endfor
    }
}


/***************************************************************************//*!
*	\brief	Get LSB set
*
*   Find least significant bit set in a byte
*
*   \param  byte	8 bit value to search in
*   \return 		Bit position 0 to 7, return 0 if no bit is set
*
*******************************************************************************/
INLINE int8u OS_GetLSBitSet(int8u byte)
{
    int8u i;

    for(i = 0; i < 8; i++) {
        if(byte & 0x01) return(i);
        byte = byte >> 1;
    }

    return(0);
}


/***************************************************************************//*!
*	\brief	OS_ManageWdogTmr
*
*   Decrements tasks wdog timers
*
*******************************************************************************/
static void OS_ManageWdogTmr(void)
{
    int8u i;

    // Decrement software wdog timer
    OsWdogTmr--;
    if (OsWdogTmr == 0)
    {
        // Reset wdog timer
        OsWdogTmr = OS_TASK_WDOG_PERIOD;

        for(i = 0; i < OS_NB_TASK; i++)
        {
            if (OS_TASK_TABLE[i].taskWdogEnable == TRUE)
            {
                if (OS_CheckTaskWdog((OsTaskId_t)i) == FALSE)
                {
                    OS_CriticalError();
                }
            }
        }
    }
}


/***************************************************************************//*!
*	\brief	Sets task wdog flag
*
*   \param taskId
*     Task identifier
*
*******************************************************************************/
static void OS_SetTaskWdog(OsTaskId_t taskId)
{
    int8u mask;
    int8u index;

    // if task is valid
    if(taskId < OS_NB_TASK) {
        mask = MapTbl[taskId & 0x07];
        index = taskId >> 3;
        // Mark task active
        OsTskWdogTbl[index] |= mask;
    // endif
    }
}


/***************************************************************************//*!
*	\brief	Returns if task had ran since last check and clears
*           its associated flag.
*
*   \param taskId
*     Task identifier
*
*******************************************************************************/
static FLAG OS_CheckTaskWdog(OsTaskId_t taskId)
{
    FLAG run = FALSE;
    int8u mask;
    int8u index;

    // if task is valid
    if(taskId < OS_NB_TASK)
    {
        mask = MapTbl[taskId & 0x07];
        index = taskId >> 3;
        if(OsTskWdogTbl[index] & mask)
        {
            run = TRUE;
            OsTskWdogTbl[index] &= ~mask;
        }
    // endif
    }
    return (run);
}


/***************************************************************************//*!
*	\brief	OS_CriticalError
*
*   Causes a wdog reset.
*
*******************************************************************************/
static void OS_CriticalError(void)
{
    // Disable all interrupts
    HAL_InterruptDisableGlobalInterrupt();
    {
        // Wait until hardware wdog timeout occurs (reset)
        while (TRUE);
    }
}


/***************************************************************************//*!
*	\brief	OS_HandleTickInterrupt
*
*   Handles bsp tick interrupt. Schedules scheduler
*
*******************************************************************************/
INTERRUPTROUTINE(OS_HandleTickInterrupt)
{
#ifdef DAC_BACKLIGHT
    static uint16_t blCnt = 0;
    uint16_t DAC_Target;
#endif /* DAC_BACKLIGHT */
    OsTickCntDiff++;
    OS_TaskScheduler();

#ifdef DAC_BACKLIGHT
    DAC_Target = GetBacklightDACTarget();
    if (blCnt < DAC_Target)
    {
        blCnt+=5;
    }
    else if (blCnt > DAC_Target)
    {
        blCnt-=5;
    }
    HAL_DAC_SetValue(&hdac, DAC_CHANNEL_2, DAC_ALIGN_12B_R, blCnt);
#endif /* DAC_BACKLIGHT */

    oscnt++;

    if (OS_IsTaskReady())
    {
        return (ISR_RESUME_CODE_OP);
    }
    else
    {
        return (ISR_RETURN_TO_SLEEP);
    }

}


#ifdef OS_STATS_ENABLED
/***************************************************************************//*!
*	\brief	Reset stats
*
*   Each task stats are reset to zero.
*
*******************************************************************************/
void OS_ResetStats(void)
{
    int8u i,j;
    for(i = 0; i < OS_NB_TASK; i++) {
        OsTskStatsTbl[i].min = -1;
        OsTskStatsTbl[i].avg = 0;
        OsTskStatsTbl[i].max = 0;
        OsTskStatsTbl[i].task_run_cnt = 0;
        OsTskStatsTbl[i].TaskName = (OsTaskId_e)i;
    }

    MainLoopStats.min = -1;
    MainLoopStats.avg = 0;
    MainLoopStats.max = 0;
    MainLoopStats.task_run_cnt = 0;
    MainLoopStats.TaskName = OS_NO_TASK;

    for (i = 0; i< OS_STACK_LENGTH; i++)
    {
        OSTaskStack[i] = MAX_OS_TASK_ID;
    }

    for (i = 0; i< OS_STATS_MAINLOOP_LENGTH; i++)
    {
        MainLoopTiming[i].LoopTime = 0;
        MainLoopTiming[i].TaskTraceCnt = 0;
        for (j=0; j < MAX_LOOP_TRACE;j++)
        {
             MainLoopTiming[i].LoopTrace[j] = OS_NO_TASK;
        }
    }

    OsTaskStackCnt = 0;
    LoopDuration = 0;


}
#endif // OS_STATS_ENABLED


#ifdef OS_STATS_ENABLED
/***************************************************************************//*!
*	\brief	Get stats
*
*   Get a handle to stats table
*
*   \return			Pointer of os stats table
*
*******************************************************************************/
OsTaskStats_t * OS_GetStats(void)
{
    return(OsTskStatsTbl);
}
#endif // OS_STATS_ENABLED


