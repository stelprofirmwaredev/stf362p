/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                      Copyright 2016, Stelpro Inc.                         */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Inc.            */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    WindowMonitoring.c
* @date    2016/02/24
* @authors Jean-Fran�ois Many
* @brief   Window Monitoring module to detect an open window
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include "typedef.h"
#include "OsTask.h"
#include "string.h"
#include "APP_SetpointManager.h"
#include ".\DB\inc\THREAD_DB.h"
#include "StringObjects.h"
#include "WindowMonitoring.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define MAX_WINDOW_CHECK    10
#define THRESHOLD_PCTRISE   5
#define THRESHOLD_TEMPRISE  20


/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
static U8BIT WindowOpen = FALSE;
static S8BIT Pct_delta = 0;
static TEMPERATURE_C_t Temp_delta = 0;
static U8BIT OpenWindowDelay = 0;
static U8BIT CloseWindowDelay = 0;


/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
DB_NOTIFICATION(Window_OnAmbientSetpointUpdate);


/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  Task that checks if a window is open or close
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2015/10/01
*******************************************************************************/
OSTASK_DEF(WM_WindowCheckTask)
{
    TEMPERATURE_C_t setpoint;
    TEMPERATURE_C_t temperature;

	DBTHRD_GetData(DBTYPE_AMBIENT_SETPOINT,(void*)&setpoint, THIS_THERMOSTAT);
	DBTHRD_GetData(DBTYPE_AMBIENT_TEMPERATURE,(void*)&temperature, THIS_THERMOSTAT);

    if (((Pct_delta >= THRESHOLD_PCTRISE) && (Temp_delta <= 0)) //If heat demand increases while temperature not rising
     || ((Pct_delta >= 0) && (Temp_delta <= -THRESHOLD_TEMPRISE) && (temperature <= setpoint)) //Or if temperature is decreasing while heat demand not decreasing and temperature below setpoint
     && (WM_IsWindowOpen() == FALSE)) // And of course, window must be closed
    {
        OpenWindowDelay++;
        if (OpenWindowDelay >= MAX_WINDOW_CHECK)
        {
            WM_SetWindowOpen(TRUE);
            CloseWindowDelay = 0;
        }
    }
    else
    {
        OpenWindowDelay = 0;
    }

    if ((Temp_delta >= 0) && (WM_IsWindowOpen() == TRUE))  // If temperature is not decreasing while window is open
    {
        CloseWindowDelay++;
        if (CloseWindowDelay >= MAX_WINDOW_CHECK)
        {
            WM_SetWindowOpen(FALSE);
            OpenWindowDelay = 0;
        }
    }
    else
    {
        CloseWindowDelay = 0;
    }

    Pct_delta = 0;
    Temp_delta = 0;

    return (OSTASK_IS_IDLE);
}

/*******************************************************************************
* @brief  Function that sets the heat demand and temperature delta between two cycles
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2015/10/01
*******************************************************************************/
void WM_SetDelta(S8BIT pct, TEMPERATURE_C_t temp)
{
    static U8BIT FirstRun = TRUE;
    static U8BIT counter = 0;

    if (FirstRun == TRUE)
    {
        DBTHRD_SubscribeToNotification(DBTYPE_AMBIENT_SETPOINT,
            Window_OnAmbientSetpointUpdate,
            DBCHANGE_ALLCHANGES,
            THIS_THERMOSTAT);
    }
    else
    {
        counter++;
        Pct_delta += pct;
        Temp_delta += temp;
        if (counter >= 4)
        {
            counter = 0;
            OS_StartTask(TSK_APP_WINDOW_CHECK);
        }
    }
    FirstRun = FALSE;
}

void WM_SetWindowOpen(U8BIT window)
{
    static TEMPERATURE_C_t previousSetpoint = NULL;
    TEMPERATURE_C_t openWindowSetpoint;
    WindowOpenDetection_t status;
    uint8_t alert;

    WindowOpen = window;
    DBTHRD_GetData(DBTYPE_OPEN_WINDOW_SETTING,(void*)&status, INDEX_DONT_CARE);

    //Check if Window Open Detection is enabled
    if (status == WOD_ENABLED)
    {
        if (WindowOpen == TRUE)
        {
			DBTHRD_GetData(DBTYPE_AMBIENT_SETPOINT,(void*)&previousSetpoint, THIS_THERMOSTAT);
            openWindowSetpoint = OPEN_WINDOW_SETPOINT;
			DBTHRD_SetData(DBTYPE_AMBIENT_SETPOINT,(void*)&openWindowSetpoint, THIS_THERMOSTAT, DBCHANGE_OPENWINDOW);
            alert = DB_SET_ALERT | DBALERT_OPEN_WINDOW;
            DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
        }
        else
        {
            if (previousSetpoint != NULL)
            {
				DBTHRD_SetData(DBTYPE_AMBIENT_SETPOINT,(void*)&previousSetpoint, THIS_THERMOSTAT, DBCHANGE_OPENWINDOW);
                alert = DB_CLEAR_ALERT | DBALERT_OPEN_WINDOW;
                DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            }
            previousSetpoint = NULL;
        }
    }
    else
    {
        if (previousSetpoint != NULL)
        {
			DBTHRD_SetData(DBTYPE_AMBIENT_SETPOINT,(void*)&previousSetpoint, THIS_THERMOSTAT, DBCHANGE_OPENWINDOW);
        }
        previousSetpoint = NULL;
    }
    OS_StartTask(TSK_APP_MNH_MENU);
}

U8BIT WM_IsWindowOpen(void)
{
    WindowOpenDetection_t status;

    DBTHRD_GetData(DBTYPE_OPEN_WINDOW_SETTING,(void*)&status, INDEX_DONT_CARE);
    if (status == WOD_ENABLED)
    {
        return WindowOpen;
    }
    else
    {
        return FALSE;
    }
}

U8BIT WM_GetOpenWindowDelay(void)
{
    return OpenWindowDelay;
}

U8BIT WM_GetCloseWindowDelay(void)
{
    return CloseWindowDelay;
}

/*******************************************************************************
* @brief Ambient Setpoint Notification, cancel open window
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/02/08
*******************************************************************************/
DB_NOTIFICATION(Window_OnAmbientSetpointUpdate)
{
    WM_SetWindowOpen(FALSE);
    OpenWindowDelay = 0;
}

#ifdef DEVKIT_PLATFORM
OSTASK_DEF(Simulate_Alert)
{
//    static uint8_t btDown = 1;
    static uint8_t btUp = 1;
    static uint8_t btEnter = 1;
    static uint8_t btLeft = 1;
    static uint8_t btRight = 1;
    uint8_t button;
    static uint8_t nowifi = 0;
    static uint8_t openwindow = 0;
    static uint8_t overheat = 0;
    uint8_t alert;
    uint8_t model[33];
    uint8_t nbStat;
    uint8_t i;
    uint8_t number[5];
    uint16_t value16;

    button = HAL_GPIO_ReadPin(BTN_UP_GPIO_Port, BTN_UP_Pin);
    if (button == 0)
    {
        if (btUp != button)
        {
            openwindow ^= 1;
            if (openwindow != 0)
            {
                alert = DB_SET_ALERT | DBALERT_OPEN_WINDOW;
                DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            }
            else
            {
                alert = DB_CLEAR_ALERT | DBALERT_OPEN_WINDOW;
                DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            }
        }
    }
    btUp = button;

    button = HAL_GPIO_ReadPin(BTN_LEFT_GPIO_Port, BTN_LEFT_Pin);
    if (button == 0)
    {
        if (btLeft != button)
        {
            nowifi ^= 1;
            if (nowifi != 0)
            {
                alert = DB_SET_ALERT | DBALERT_NO_WIFI;
                DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            }
            else
            {
                alert = DB_CLEAR_ALERT | DBALERT_NO_WIFI;
                DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            }
        }
    }
    btLeft = button;

    button = HAL_GPIO_ReadPin(BTN_ENTER_GPIO_Port, BTN_ENTER_Pin);
    if (button == 0)
    {
        if (btEnter != button)
        {
            overheat ^= 1;
            if (overheat != 0)
            {
                alert = DB_SET_ALERT | DBALERT_OVERHEAT;
                DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            }
            else
            {
                alert = DB_CLEAR_ALERT | DBALERT_OVERHEAT;
                DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            }
        }
    }
    btEnter = button;

    button = HAL_GPIO_ReadPin(BTN_RIGHT_GPIO_Port, BTN_RIGHT_Pin);
    if (button == 0)
    {
        if (btRight != button)
        {
            nbStat = 0;
            for (i = 0; i < THERMOSTAT_INSTANCES; i++)
            {
                DBTHRD_GetData(DBTYPE_TSTAT_MODEL, &model, i);
                if (model[0] != NULL)
                {
                    nbStat++;
                }
            }
            if (nbStat < THERMOSTAT_INSTANCES)
            {
                strcpy((char*)model, 0);
                if ((nbStat % 5) == 0)
                {
                    strcpy((char*)model, "FLOOR_");
                }
                strcat((char*)model, "Model_");
                STR_StrItoA(number, (nbStat + 1));
                strcat((char*)model, (char const*)&number);
                DBTHRD_SetData(DBTYPE_TSTAT_MODEL,(void*)&model, nbStat, DBCHANGE_ZIGBEE);
                value16 = 1950;
                DBTHRD_SetData(DBTYPE_AMBIENT_TEMPERATURE,(void*)&value16, nbStat, DBCHANGE_LOCAL);
                value16 = 1900;
                DBTHRD_SetData(DBTYPE_AMBIENT_SETPOINT,(void*)&value16, nbStat, DBCHANGE_LOCAL);
                value16 = 50;
                DBTHRD_SetData(DBTYPE_HEAT_DEMAND,(void*)&value16, nbStat, DBCHANGE_LOCAL);
                CheckDataBaseIntegrity();
            }
        }
    }
    btRight = button;

//    button = HAL_GPIO_ReadPin(BTN_DOWN_GPIO_Port, BTN_DOWN_Pin);
//    if (button == 0)
//    {
//        if (btDown != button)
//        {
//
//        }
//    }
//    btDown = button;

    return (OSTASK_IS_IDLE);
}
#endif /* DEVKIT_PLATFORM */


/** Copyright(C) 2014 Stelpro, All Rights Reserved**/
