/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    IdleMenu.c
* @date    2016/10/26
* @authors J-F. Simard
* @brief
*   March 7 2018:
*       - Change the source of the UserAction() notification for
*           DBCHANGE_WAKEUP_FROM_SLEEP to capture only user actions that wake up
*           the device from sleep
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>

#include "MenuTable.h"
#include "BacklightMenu.h"
#include ".\HAL\inc\Display.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\Sensors\inc\THREAD_Sensors.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
static susbscriptionHandle_t hUserActionNotification = DBTHRD_NOTIFHANDLE_NOTSET;


/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static DB_NOTIFICATION( OnUserAction );



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/10/26
*******************************************************************************/
static DB_NOTIFICATION( OnUserAction )
{
    MNH_SetActiveMenu(MENU_HOME);
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/10/26
*******************************************************************************/
void MNU_Idle_Entry(void)
{
    MNU_SetKeyAssigned(FALSE);
    //HAL_ClearScreen();
    //DisplayOFF();
    BacklightOFF();

    hUserActionNotification = DBTHRD_SubscribeToNotification( DBTYPE_USER_ACTION,
                                                       OnUserAction,
                                                       DBCHANGE_WAKEUP_FROM_SLEEP,
                                                       INDEX_DONT_CARE);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/10/26
*******************************************************************************/
void MNU_Idle_Core(void)
{

}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/10/26
*******************************************************************************/
void MNU_Idle_Exit(void)
{
    HAL_ClearScreen();
    //DisplayON();
    DBTHRD_UnsubscribeToNotification(hUserActionNotification);
}

/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
