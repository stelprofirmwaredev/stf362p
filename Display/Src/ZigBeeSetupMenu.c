/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       ZigBeeSetupMenu.c

    \brief      Source code file for the ZigBeeSetupMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the ZigBeeSetupMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "ZigBeeSetupMenu.h"
#include ".\DB\inc\THREAD_DB.h"

#include "images.h"
#include ".\HAL\inc\Display.h"

#include ".\GuiFrameWork\inc\class_DisplayArea.h"
#include ".\GuiFrameWork\inc\class_Button.h"

#include "StringObjects.h"
#include ".\ZigBee\inc\THREAD_ZigBee.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/


/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

#ifdef ZIGBEE_CERTIFICATION_INTERFACE
cButtonHandle hBackButton = NULL;
cButtonHandle hLeaveButton = NULL;
cButtonHandle hLeaveAndKeepButton = NULL;
cButtonHandle hJoinButton = NULL;
cButtonHandle hAddButton = NULL;
cButtonHandle hIdentifyQueryButton = NULL;

cDisplayHandle hHeaderDisplay = NULL;
cDisplayHandle hPanIdDisplay = NULL;
cDisplayHandle hShortAddressDisplay = NULL;
cDisplayHandle hActiveChannelDisplay = NULL;

static susbscriptionHandle_t hNwkInfoUpdateNotif = DBTHRD_NOTIFHANDLE_NOTSET;
#endif //#ifdef ZIGBEE_CERTIFICATION_INTERFACE

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   ZigBeeSetupEditState = EDIT_IDLE;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/
    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
#ifdef ZIGBEE_CERTIFICATION_INTERFACE
static void backAction(buttonState_t state);
static void LeaveNetwork(buttonState_t state);
static void LeaveNetworkAndKeepPanId(buttonState_t state);
static void JoinNetwork(buttonState_t state);
static void AddZigbeeDevice(buttonState_t state);
void DisplayZigBeeOptions(void);
static DB_NOTIFICATION(RefreshDisplay);

#endif //#ifndef ZIGBEE_CERTIFICATION_INTERFACE

/*******************************************************************************
    Private functions definitions
*******************************************************************************/

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_ZigBeeSetup_Entry
*******************************************************************************/
void MNU_ZigBeeSetup_Entry(void)
{
    ZigBeeSetupEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);

#ifdef ZIGBEE_CERTIFICATION_INTERFACE
    DisplayZigBeeOptions();
    hNwkInfoUpdateNotif = DBTHRD_SubscribeToNotification(DBTYPE_ZIGBEE_NETWORK_INFO,
                                                       RefreshDisplay,
                                                       DBCHANGE_ALLCHANGES,
                                                       INDEX_DONT_CARE);

#else
    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show ZigBee options
    //
    DPH_DisplayZigBeeOptions();
#endif
	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_ZigBeeSetup_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_ZigBeeSetup_Core

    Allow to perform the ZigBee Setup
*******************************************************************************/
void MNU_ZigBeeSetup_Core(void)
{
#ifndef ZIGBEE_CERTIFICATION_INTERFACE
    dbType_ZigBeeNetworkInfo_t nwInfo;
    dbType_ZigBeeJoinInfo_t joinInfo;

    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
//
//         Configure keys
//
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_ZigBeeSetup_Commit, (void*)KEY_BACK);

        DBTHRD_GetData(DBTYPE_ZIGBEE_NETWORK_INFO,(void*)&nwInfo, INDEX_DONT_CARE);
        DBTHRD_GetData(DBTYPE_ZIGBEE_JOIN_INFO,(void*)&joinInfo, INDEX_DONT_CARE);
        if (nwInfo.InANetwork != 0)
        {
            //In coordinator mode, we can add more thermostats
//TODO - enable router mode
            if (joinInfo.DeviceType == 0/*DEVICE_TYPE_COORDINATOR*/)
            {
                KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)MNU_ZigBeeSetup_Commit, (void*)KEY_OPTIONS_2);
            }
            else
            {
                //We cannot add more thermostat in router mode
                KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)NULL, (void*)NULL);
            }
//            //Once in a network, we can leave, but not join again
//            KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)NULL, (void*)NULL);
            KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)MNU_ZigBeeSetup_Commit, (void*)KEY_OPTIONS_3);
            KBH_SetKeyPressProperties(KEY_OPTIONS_4, (FUNC)MNU_ZigBeeSetup_Commit, (void*)KEY_OPTIONS_4);
        }
        else
        {
            //When not in a network, we can join a network or form a network
            KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)MNU_ZigBeeSetup_Commit, (void*)KEY_OPTIONS_2);
            KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)MNU_ZigBeeSetup_Commit, (void*)KEY_OPTIONS_3);
            KBH_SetKeyPressProperties(KEY_OPTIONS_4, (FUNC)NULL, (void*)NULL);
        }

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
#endif
}

/*******************************************************************************
    MNU_ZigBeeSetup_Exit
*******************************************************************************/
void MNU_ZigBeeSetup_Exit(void)
{
    ZigBeeSetupEditState = EDIT_IDLE;
#ifdef ZIGBEE_CERTIFICATION_INTERFACE
    DestroyButton(&hBackButton);
    DestroyButton(&hAddButton);
    DestroyButton(&hJoinButton);
    DestroyButton(&hLeaveButton);
    DestroyButton(&hLeaveAndKeepButton);
    DestroyButton(&hIdentifyQueryButton);

    DestroyDisplayArea (&hHeaderDisplay);
    DestroyDisplayArea (&hPanIdDisplay);
    DestroyDisplayArea (&hShortAddressDisplay);
    DestroyDisplayArea (&hActiveChannelDisplay);

    DBTHRD_UnsubscribeToNotification(hNwkInfoUpdateNotif);
#endif
}

/*******************************************************************************
    MNU_ZigBeeSetup_Commit
*******************************************************************************/
void MNU_ZigBeeSetup_Commit(KEYB_PARAM keyId)
{
	if (ZigBeeSetupEditState == EDIT_IN_PROGRESS)
	{
        KBH_ClearAllKeyProperties();
        switch(keyId)
        {
            case KEY_BACK:
                MNH_SetActiveMenu(MENU_ADV_SETTINGS);
                break;

            case KEY_OPTIONS_2:
                ZBTHRD_ClearUseOfInstallCode();
                MNH_SetActiveMenu(MENU_ADD);
                break;

            case KEY_OPTIONS_3:
                MNH_SetActiveMenu(MENU_INSTALL_CODE);
                break;

            case KEY_OPTIONS_4:
                MNH_SetActiveMenu(MENU_LEAVE);
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
		ZigBeeSetupEditState = EDIT_IDLE;
	}
}

#ifdef ZIGBEE_CERTIFICATION_INTERFACE
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void backAction(buttonState_t state)
{
    MNH_SetActiveMenu(MENU_ADV_SETTINGS);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void AddZigbeeDevice(buttonState_t state)
{
    MNH_SetActiveMenu(MENU_ADD);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void JoinNetwork(buttonState_t state)
{
    ZBTHRD_FormZigBeeNetwork();
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void LeaveNetwork(buttonState_t state)
{
    ZBTHRD_KeepSameNetwork (FALSE);
    MNH_SetActiveMenu(MENU_LEAVE);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void LeaveNetworkAndKeepPanId(buttonState_t state)
{
    ZBTHRD_KeepSameNetwork (TRUE);
    MNH_SetActiveMenu(MENU_LEAVE);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void SendIdentifyQuery(buttonState_t state)
{
    if (BUTTON_PRESSED == state)
    {
        ZBTHRD_IdentifyQuery();
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void displayNwkInfo (void)
{
    char strBuf[50];
    dbType_ZigBeeNetworkInfo_t nwInfo;

    DBTHRD_GetData(DBTYPE_ZIGBEE_NETWORK_INFO,(void*)&nwInfo, INDEX_DONT_CARE);

    //Display the ZigBee informations (Pan ID, Node ID, Active Channel)
    sprintf (strBuf, "%s%04x", (uint8_t*)STR_GetString(STRID_ZIGBEE_SETUP_PANID),  nwInfo.PanID);
    SetText (hPanIdDisplay, strBuf, &GUI_FontCairoSemiBold46);

    sprintf (strBuf, "%s%04x", (uint8_t*)STR_GetString(STRID_ZIGBEE_SETUP_NODEID),  nwInfo.ShortAdd);
    SetText (hShortAddressDisplay, strBuf, &GUI_FontCairoSemiBold46);

    sprintf (strBuf, "%s %d", (uint8_t*)STR_GetString(STRID_ZIGBEE_SETUP_CHANNEL),  nwInfo.ActiveChannel);
    SetText (hActiveChannelDisplay, strBuf, &GUI_FontCairoSemiBold46);


    if (nwInfo.InANetwork != 0)
    {
        hJoinButton->isClickable = 0;
        hLeaveButton->isClickable = 1;
        hLeaveAndKeepButton->isClickable = 1;
        hAddButton->isClickable = 1;
        hIdentifyQueryButton->isClickable = 1;

        SetTextColor (GetButtonDisplayHandle (hJoinButton), LCD_COLOR_GRAY);
        SetTextColor (GetButtonDisplayHandle (hLeaveButton), LCD_COLOR_WHITE);
        SetTextColor (GetButtonDisplayHandle (hLeaveAndKeepButton), LCD_COLOR_WHITE);
        SetTextColor (GetButtonDisplayHandle (hAddButton), LCD_COLOR_WHITE);
        SetTextColor (GetButtonDisplayHandle (hIdentifyQueryButton), LCD_COLOR_WHITE);
    }
    else
    {
        hJoinButton->isClickable = 1;
        hLeaveButton->isClickable = 0;
        hLeaveAndKeepButton->isClickable = 0;
        hAddButton->isClickable = 0;
        hIdentifyQueryButton->isClickable = 0;

        SetTextColor (GetButtonDisplayHandle (hJoinButton), LCD_COLOR_WHITE);
        SetTextColor (GetButtonDisplayHandle (hLeaveButton), LCD_COLOR_GRAY);
        SetTextColor (GetButtonDisplayHandle (hLeaveAndKeepButton), LCD_COLOR_GRAY);
        SetTextColor (GetButtonDisplayHandle (hAddButton), LCD_COLOR_GRAY);
        SetTextColor (GetButtonDisplayHandle (hIdentifyQueryButton), LCD_COLOR_GRAY);
    }

    DrawArea(GetButtonDisplayHandle (hAddButton));
    DrawArea(GetButtonDisplayHandle (hJoinButton));
    DrawArea(GetButtonDisplayHandle (hLeaveButton));
    DrawArea(GetButtonDisplayHandle (hLeaveAndKeepButton));
    DrawArea(GetButtonDisplayHandle (hIdentifyQueryButton));

    DrawArea(hPanIdDisplay);
    DrawArea(hShortAddressDisplay);
    DrawArea(hActiveChannelDisplay);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static DB_NOTIFICATION(RefreshDisplay)
{
    displayNwkInfo();
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void DisplayZigBeeOptions(void)
{
    ClearDisplayCache();

    hBackButton = CreateNewButton(0,8, 64, 61, backAction);
    SetImage (GetButtonDisplayHandle (hBackButton), BACK_ICON_ADD, BACK_ICON_SIZE);
    SetImageMargins(GetButtonDisplayHandle (hBackButton),(displayMargins_t){
                                                            .top_margin = 16,
                                                            .left_margin = 24,
                                                            .bottom_margin = 0,
                                                            .right_margin = 0,
                                                            });
    hHeaderDisplay = CreateNewDisplayArea(65, 20 /*TITLE_LINE1_Y_POS*/, 320, 49);
    SetText (hHeaderDisplay, (char *)STR_GetString(STRID_ZIGBEE_SETUP), &GUI_FontCairoSemiBold62);
    SetTextAlignement (hHeaderDisplay, (displayAlignment_t){
                                            .vertical = VERTICAL_ALIGN_TOP,
                                            .horizontal = HORIZONTAL_ALIGN_LEFT});



    hLeaveButton = CreateNewButton(25, 219, 270, 42, LeaveNetwork);
    SetImage (GetButtonDisplayHandle (hLeaveButton), GREY_BTN_ADD, GREY_BTN_SIZE);
    SetText (GetButtonDisplayHandle (hLeaveButton), (char*)STR_GetString(STRID_ZIGBEE_SETUP_LEAVE_NETWORK), &GUI_FontCairoSemiBold46);
    SetTextMargins(GetButtonDisplayHandle (hLeaveButton),(displayMargins_t){
                                                            .top_margin = 4,
                                                            .left_margin = 13,
                                                            .bottom_margin = 0,
                                                            .right_margin = 0,
                                                            });

    hLeaveAndKeepButton = CreateNewButton(25, 269, 270, 42, LeaveNetworkAndKeepPanId);
    SetImage (GetButtonDisplayHandle (hLeaveAndKeepButton), GREY_BTN_ADD, GREY_BTN_SIZE);
    SetText (GetButtonDisplayHandle (hLeaveAndKeepButton), "Leave and keep PAN Id", &GUI_FontCairoSemiBold46);
    SetTextMargins(GetButtonDisplayHandle (hLeaveAndKeepButton),(displayMargins_t){
                                                            .top_margin = 4,
                                                            .left_margin = 13,
                                                            .bottom_margin = 0,
                                                            .right_margin = 0,
                                                            });

    hJoinButton = CreateNewButton(25, 169, 270, 42, JoinNetwork);
    SetImage (GetButtonDisplayHandle (hJoinButton), GREY_BTN_ADD, GREY_BTN_SIZE);
    if (0 == getKeepSameNetwork())
    {
        SetText (GetButtonDisplayHandle (hJoinButton), "Create Network", &GUI_FontCairoSemiBold46);
    }
    else
    {
        SetText (GetButtonDisplayHandle (hJoinButton), "Re-Create Same Nwk", &GUI_FontCairoSemiBold46);
    }
    SetTextMargins(GetButtonDisplayHandle (hJoinButton),(displayMargins_t){
                                                            .top_margin = 4,
                                                            .left_margin = 13,
                                                            .bottom_margin = 0,
                                                            .right_margin = 0,
                                                            });


    hIdentifyQueryButton = CreateNewButton(25, 69, 270, 42, SendIdentifyQuery);
    SetImage (GetButtonDisplayHandle (hIdentifyQueryButton), GREY_BTN_ADD, GREY_BTN_SIZE);
    SetText (GetButtonDisplayHandle (hIdentifyQueryButton), "Identify Query", &GUI_FontCairoSemiBold46);
    SetTextMargins(GetButtonDisplayHandle (hIdentifyQueryButton),(displayMargins_t){
                                                            .top_margin = 4,
                                                            .left_margin = 13,
                                                            .bottom_margin = 0,
                                                            .right_margin = 0,
                                                            });


    hAddButton = CreateNewButton(25, 119, 270, 42, AddZigbeeDevice);
    SetImage (GetButtonDisplayHandle (hAddButton), GREY_BTN_ADD, GREY_BTN_SIZE);
    SetText (GetButtonDisplayHandle (hAddButton), (char*)STR_GetString(STRID_ZIGBEE_SETUP_ADD_NEW_TSTAT), &GUI_FontCairoSemiBold46);
    SetTextMargins(GetButtonDisplayHandle (hAddButton),(displayMargins_t){
                                                            .top_margin = 4,
                                                            .left_margin = 13,
                                                            .bottom_margin = 0,
                                                            .right_margin = 0,
                                                            });



    hPanIdDisplay = CreateNewDisplayArea(38, 323, 320, 50);
    hShortAddressDisplay = CreateNewDisplayArea(38, 373, 320, 50);
    hActiveChannelDisplay = CreateNewDisplayArea(38, 423, 320, 50);


    displayNwkInfo();

    DrawArea(GetButtonDisplayHandle (hBackButton));
    DrawArea(hHeaderDisplay);

}

#endif //#ifdef ZIGBEE_CERTIFICATION_INTERFACE

