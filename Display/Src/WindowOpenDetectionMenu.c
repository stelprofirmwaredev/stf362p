/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       WindowOpenDetectionMenu.c

    \brief      Source code file for the WindowOpenDetectionMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the WindowOpenDetectionMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "WindowOpenDetectionMenu.h"
#include ".\DB\inc\THREAD_DB.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e           WindowOpenDetectionEditState = EDIT_IDLE;
static WindowOpenDetection_t    WindowOpenDetectionEditValue;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void ChangeWindowOpenDetection(KEYB_PARAM keyId);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    ChangeWindowOpenDetection

    Change the Window Open Detection feature between On and Off
*******************************************************************************/
static void ChangeWindowOpenDetection(KEYB_PARAM keyId)
{
    switch (keyId)
    {
        case KEY_OPTIONS_2:
            WindowOpenDetectionEditValue = WOD_DISABLED;
            break;

        case KEY_OPTIONS_3:
            WindowOpenDetectionEditValue = WOD_ENABLED;
            break;

        default:
            break;
    }

    DPH_DisplayWindowOpenDetection(WindowOpenDetectionEditValue, FALSE);
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_WindowOpenDetection_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_WindowOpenDetection_Entry
*******************************************************************************/
void MNU_WindowOpenDetection_Entry(void)
{
    DBTHRD_GetData(DBTYPE_OPEN_WINDOW_SETTING,(void*)&WindowOpenDetectionEditValue, INDEX_DONT_CARE);
    WindowOpenDetectionEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show window open detection feature state
    //
    DPH_DisplayWindowOpenDetection(WindowOpenDetectionEditValue, TRUE);

    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_WindowOpenDetection_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_WindowOpenDetection_Core

    Allow enabling or disabling the detection of an opened window feature
*******************************************************************************/
void MNU_WindowOpenDetection_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_WindowOpenDetection_Commit, (void*)KEY_BACK);
        KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)ChangeWindowOpenDetection, (void*)KEY_OPTIONS_2);
        KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)ChangeWindowOpenDetection, (void*)KEY_OPTIONS_3);
        KBH_SetKeyPressProperties(KEY_DONE, (FUNC)MNU_WindowOpenDetection_Commit, (void*)KEY_DONE);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_WindowOpenDetection_Exit
*******************************************************************************/
void MNU_WindowOpenDetection_Exit(void)
{
    WindowOpenDetectionEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_WindowOpenDetection_Init
*******************************************************************************/
void MNU_WindowOpenDetection_Init(void)
{
    MNU_WindowOpenDetection_SetWindowOpenDetection(WOD_DISABLED);
}

/*******************************************************************************
    MNU_WindowOpenDetection_Commit
*******************************************************************************/
void MNU_WindowOpenDetection_Commit(KEYB_PARAM keyId)
{
    static KEYB_PARAM commitKey = NULL;

    if ( WindowOpenDetectionEditState == EDIT_IN_PROGRESS )
    {
        KBH_ClearAllKeyProperties();
        commitKey = keyId;
        switch (commitKey)
        {
            case KEY_DONE:
                MNU_WindowOpenDetection_SetWindowOpenDetection(WindowOpenDetectionEditValue);
                //No break on purpose

            case KEY_BACK:
            default:
                MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_WindowOpenDetection_Commit, (KEYB_PARAM*)commitKey, IMMEDIATELY);
                break;
        }
        WindowOpenDetectionEditState = EDIT_COMMITTING;
    }
    else if ( WindowOpenDetectionEditState == EDIT_COMMITTING )
    {
        WindowOpenDetectionEditState = EDIT_IDLE;
        switch (commitKey)
        {
            case KEY_BACK:
            case KEY_DONE:
                MNH_SetActiveMenu(MENU_OPTIONS);
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
    }
}

/*******************************************************************************
    MNU_WindowOpenDetection_SetWindowOpenDetection

    Set the open window detection feature On or Off
*******************************************************************************/
void MNU_WindowOpenDetection_SetWindowOpenDetection(WindowOpenDetection_t wod)
{
    DBTHRD_SetData(DBTYPE_OPEN_WINDOW_SETTING,(void*)&wod, INDEX_DONT_CARE, DBCHANGE_LOCAL);
}
