/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2019, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       ControlModeMenu.c

    \brief      Source code file for the ControlModeMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the ControlModeMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "ControlModeMenu.h"
#include ".\DB\inc\THREAD_DB.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   ControlModeEditState = EDIT_IDLE;
static ControlMode_t    ControlModeEditValue;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void ChangeControlMode(KEYB_PARAM keyId);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    ChangeControlMode

    Change the Control Mode between Ambient, Floor and Off
*******************************************************************************/
static void ChangeControlMode(KEYB_PARAM keyId)
{
    uint8_t onboardingDone;

    switch (keyId)
    {
        case KEY_OPTIONS_2:
            ControlModeEditValue = CONTROLMODE_AMBIENT;
            break;

        case KEY_OPTIONS_3:
            ControlModeEditValue = CONTROLMODE_FLOOR;
            break;

        case KEY_OPTIONS_4:
            ControlModeEditValue = CONTROLMODE_AMBIENTFLOOR;
            break;

        default:
            break;
    }

    DPH_DisplayControlMode(ControlModeEditValue, FALSE);

    DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);
    if (onboardingDone == 1)
    {
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_ControlMode_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    }
    MNU_SetKeyAssigned(FALSE);
}


/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_ControlMode_Entry
*******************************************************************************/
void MNU_ControlMode_Entry(void)
{
    uint8_t onboardingDone;

    DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);
    DBTHRD_GetData(DBTYPE_FLOOR_MODE,(void*)&ControlModeEditValue, THIS_THERMOSTAT);
    ControlModeEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show Control modes list
    //
    DPH_DisplayControlMode(ControlModeEditValue, TRUE);

    if (onboardingDone == 1)
    {
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_ControlMode_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    }
}

/*******************************************************************************
    MNU_ControlMode_Core

    Allow changing the control mode between Ambient, Floor and Off
*******************************************************************************/
void MNU_ControlMode_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_ControlMode_Commit, (void*)KEY_BACK);
        KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)ChangeControlMode, (void*)KEY_OPTIONS_2);
        KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)ChangeControlMode, (void*)KEY_OPTIONS_3);
        KBH_SetKeyPressProperties(KEY_OPTIONS_4, (FUNC)ChangeControlMode, (void*)KEY_OPTIONS_4);
        if ((ControlModeEditValue == CONTROLMODE_FLOOR) || (ControlModeEditValue == CONTROLMODE_AMBIENTFLOOR))
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_5, (FUNC)MNU_ControlMode_Commit, (void*)KEY_OPTIONS_5);
        }
        else
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_5, (FUNC)NULL, (void*)NULL);
        }
        if (ControlModeEditValue == CONTROLMODE_AMBIENTFLOOR)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_6, (FUNC)MNU_ControlMode_Commit, (void*)KEY_OPTIONS_6);
        }
        else
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_6, (FUNC)NULL, (void*)NULL);
        }
        KBH_SetKeyPressProperties(KEY_DONE, (FUNC)MNU_ControlMode_Commit, (void*)KEY_DONE);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_ControlMode_Exit
*******************************************************************************/
void MNU_ControlMode_Exit(void)
{
    ControlModeEditState = EDIT_IDLE;
    OS_StartTask(TSK_LIMIT_CHECK);
}

/*******************************************************************************
    MNU_ControlMode_Init
*******************************************************************************/
void MNU_ControlMode_Init(void)
{
    MNU_ControlMode_SetControlMode(CONTROLMODE_AMBIENT);
}

/*******************************************************************************
    MNU_ControlMode_Commit
*******************************************************************************/
void MNU_ControlMode_Commit(KEYB_PARAM keyId)
{
    static KEYB_PARAM commitKey = NULL;
    uint8_t onboardingDone;

    if ( ControlModeEditState == EDIT_IN_PROGRESS )
    {
        commitKey = keyId;
        KBH_ClearAllKeyProperties();
        switch (commitKey)
        {
            case KEY_OPTIONS_5:
            case KEY_OPTIONS_6:
            case KEY_DONE:
                MNU_ControlMode_SetControlMode(ControlModeEditValue);
                //No break on purpose

            case KEY_BACK:
            default:
                MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_ControlMode_Commit, (KEYB_PARAM*)commitKey, IMMEDIATELY);
                break;
        }
        ControlModeEditState = EDIT_COMMITTING;
    }
    else if ( ControlModeEditState == EDIT_COMMITTING )
    {
        ControlModeEditState = EDIT_IDLE;
        DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);

        switch (commitKey)
        {
            case KEY_BACK:
                if (onboardingDone == 1)
                {
                    MNH_SetActiveMenu(MENU_OPTIONS);
                }
                else
                {
                    MNH_SetActiveMenu(MENU_TIME_FORMAT);
                }
                break;

            case KEY_DONE:
                if (onboardingDone == 1)
                {
                    MNH_SetActiveMenu(MENU_OPTIONS);
                }
                else
                {
                    onboardingDone = 1;
                    DBTHRD_SetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE, DBCHANGE_LOCAL);
                    MNH_SetActiveMenu(MENU_HOME);
                }
                break;

            case KEY_OPTIONS_5:
                MNH_SetActiveMenu(MENU_FLOOR_TYPE);
                break;

            case KEY_OPTIONS_6:
                MNH_SetActiveMenu(MENU_FLOOR_LIMIT);
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
    }
}

/*******************************************************************************
    MNU_ControlMode_SetControlMode

    Set the current control mode
*******************************************************************************/
void MNU_ControlMode_SetControlMode(ControlMode_t mode)
{
    DBTHRD_SetData(DBTYPE_FLOOR_MODE,(void*)&mode, THIS_THERMOSTAT, DBCHANGE_LOCAL);
}
