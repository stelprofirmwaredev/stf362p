/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       MenuHandlerIf.c

    \brief      Source code file for the MenuHandler module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the MenuHandler core implementation.

    Requirement: Context definitions specific to each project are expected
                in ContextCollection.c.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"

#include "MenuHandler.h"
#include "MenuTable.h"
#include "LockMenu.h"
#include "BacklightMenu.h"
#include "LanguageMenu.h"
#include "UnitFormatMenu.h"
#include "TimeFormatMenu.h"
#include ".\DB\inc\THREAD_DB.h"
#include "MenuHandlerIf.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/

    //Empty

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/

/*******************************************************************************
    Private functions definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNH_Init

    Module initialization
*******************************************************************************/

void MNH_MenusInit(void)
{
    MNH_SetActiveMenu(MENU_SPLASH_SCREEN);
}