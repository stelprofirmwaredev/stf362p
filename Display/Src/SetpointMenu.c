/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2016, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       SetpointMenu.c

    \brief      Source code file for the SetpointMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the SetpointMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "convert.h"
#include "tools.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "SetpointMenu.h"
#include ".\DB\inc\THREAD_DB.h"
#include "AlertsMenu.h"
#include "APP_SetpointManager.h"
#include "HomeMenu.h"
/*******************************************************************************
    Private constants definitions
*******************************************************************************/


/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_CONFIRM,
    EDIT_COMMITTING
} MnuEditState_e;


/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   SetpointEditState = EDIT_IDLE;
#ifdef PCT_HEAT
    static HEAT_DEMAND_PERCENT_t holdEditValue;
#else
    static TEMPERATURE_C_t holdEditValue;
#endif
static U8BIT temp_display;
static U8BIT heat_display;
static U8BIT setpoint_display;

#ifdef FLOOR
    static uint8_t minReached;
#endif
uint8_t offMode;

#ifdef ON_OFF
extern uint8_t ForceOnState;
#endif /* ON_OFF */

static susbscriptionHandle_t hAmbientTemperatureNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hAmbientSetpointNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hFloorSetpointNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hTemperatureUnitNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hSetpointHeatDemandNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hSetpointOffModeNotification = DBTHRD_NOTIFHANDLE_NOTSET;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/
    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
DB_NOTIFICATION(MNU_SetpointOnAmbientTemperatureUpdate);
DB_NOTIFICATION(MNU_SetpointOnAmbientSetpointUpdate);
DB_NOTIFICATION(MNU_SetpointOnTemperatureUnitUpdate);
DB_NOTIFICATION(MNU_SetpointOnHeatDemandUpdate);
DB_NOTIFICATION(MNU_SetpointOffModeUpdate);
static void MNU_RefreshSetpointDisplayItems(int16u refreshBits);
#ifdef FLOOR
static void MNU_Setpoint_SetOffMode(void);
#endif

/*******************************************************************************
    Private functions definitions
*******************************************************************************/


/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_Setpoint_Entry
*******************************************************************************/
void MNU_Setpoint_Entry(void)
{
    SetpointEditState = EDIT_IDLE;

    MNU_SetKeyAssigned(FALSE);

    MNU_RefreshSetpointDisplayItems(ALL_DISPLAY_BIT);

    holdEditValue = MNU_Setpoint_GetSetpoint();

    DBTHRD_GetData(DBTYPE_OFF_MODE_SETTING,(void*)&offMode, THIS_THERMOSTAT);
#ifdef FLOOR
    if (holdEditValue == STPM_GetSetpointMin())
    {
        minReached = 1;
    }
    else
    {
        minReached = 0;
    }
#endif

	hAmbientTemperatureNotification = DBTHRD_SubscribeToNotification(DBTYPE_AMBIENT_TEMPERATURE,
                                                       MNU_SetpointOnAmbientTemperatureUpdate,
                                                       DBCHANGE_ALLCHANGES,
                                                       THIS_THERMOSTAT);

	hAmbientSetpointNotification = DBTHRD_SubscribeToNotification(DBTYPE_AMBIENT_SETPOINT,
                                                       MNU_SetpointOnAmbientSetpointUpdate,
                                                       DBCHANGE_ALLCHANGES,
                                                       THIS_THERMOSTAT);

    hFloorSetpointNotification = DBTHRD_SubscribeToNotification(DBTYPE_FLOOR_SETPOINT,
                                                       MNU_SetpointOnAmbientSetpointUpdate,
                                                       DBCHANGE_ALLCHANGES,
                                                       THIS_THERMOSTAT);

	hTemperatureUnitNotification =  DBTHRD_SubscribeToNotification( DBTYPE_TEMPERATURE_FORMAT,
                                                       MNU_SetpointOnTemperatureUnitUpdate,
                                                       DBCHANGE_ALLCHANGES,
                                                       INDEX_DONT_CARE);

    hSetpointHeatDemandNotification = DBTHRD_SubscribeToNotification( DBTYPE_HEAT_DEMAND,
                                                       MNU_SetpointOnHeatDemandUpdate,
                                                       DBCHANGE_LOCAL,
                                                       THIS_THERMOSTAT);

    hSetpointOffModeNotification = DBTHRD_SubscribeToNotification( DBTYPE_OFF_MODE_SETTING,
                                                       MNU_SetpointOffModeUpdate,
                                                       DBCHANGE_LOCAL,
                                                       THIS_THERMOSTAT);

	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Setpoint_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_Setpoint_Core

    Allow to view/change the setpoint
*******************************************************************************/
void MNU_Setpoint_Core(void)
{
    TEMPERATURE_C_t temperature;
    HEAT_DEMAND_PERCENT_t heatDemand;
    uint8_t floorMode;

    DBTHRD_GetData(DBTYPE_FLOOR_MODE, &floorMode, THIS_THERMOSTAT);
    if (floorMode == CONTROLMODE_FLOOR)
    {
        DBTHRD_GetData(DBTYPE_FLOOR_TEMPERATURE,(void*)&temperature, THIS_THERMOSTAT);
    }
    else
    {
        DBTHRD_GetData(DBTYPE_AMBIENT_TEMPERATURE,(void*)&temperature, THIS_THERMOSTAT);
    }
	DBTHRD_GetData(DBTYPE_HEAT_DEMAND,(void*)&heatDemand, THIS_THERMOSTAT);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    // Show heat demand
    //
    if (heat_display)
    {
        DPH_DisplayHeatingStatus(heatDemand, FALSE);
        heat_display = FALSE;
    }

    //
    //	Show ambient temperature
    //
    if (temp_display)
    {
        DPH_DisplayAmbientRoomTemperature(temperature, floorMode);
        temp_display = FALSE;
    }

    //
    //	Show setpoint
    //
    DPH_DisplaySetpointApplied(SetpointEditState);
#ifdef PCT_HEAT
    temperature = MNU_Setpoint_GetHeatPctSetpoint();
    DPH_HandleHeatPctSetpointDisplay(temperature);
#else
    if (setpoint_display)
    {
        temperature = MNU_Setpoint_GetSetpoint();
        if (floorMode == CONTROLMODE_FLOOR)
        {
            DPH_DisplaySetpoint(temperature, TRUE, offMode, SET_FLOOR, TRUE);
        }
        else
        {
            DPH_DisplaySetpoint(temperature, TRUE, offMode, SET_AMBIENT, TRUE);
        }
        setpoint_display = FALSE;
    }
#endif


    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_Setpoint_Commit, (void*)KEY_BACK);
#ifdef FLOOR
        if (minReached == 0)
        {
            KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_Setpoint_DecSetpointStartTurbo, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
        }
        else
        {
            KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_Setpoint_SetOffMode, (int16u*) 50, OS_MSTOTICK(KEYB_MENU_HOLD));
        }
#else
        KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_Setpoint_DecSetpointStartTurbo, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
#endif
        KBH_SetKeyHoldProperties(KEY_UP, (FUNC)MNU_Setpoint_IncSetpointStartTurbo, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
        KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_Setpoint_IncSetpoint, (int16u*)50);
        KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_Setpoint_DecSetpoint, (int16u*)50);

        // Stop key repeat monitoring
        KBH_SetKeyRepeatProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
        KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_Setpoint_Exit
*******************************************************************************/
void MNU_Setpoint_Exit(void)
{
    MNU_Setpoint_StopSetpointEdition();
    DBTHRD_UnsubscribeToNotification(hAmbientTemperatureNotification);
	DBTHRD_UnsubscribeToNotification(hTemperatureUnitNotification);
    DBTHRD_UnsubscribeToNotification(hAmbientSetpointNotification);
    DBTHRD_UnsubscribeToNotification(hFloorSetpointNotification);
	DBTHRD_UnsubscribeToNotification(hSetpointHeatDemandNotification);
    DBTHRD_UnsubscribeToNotification(hSetpointOffModeNotification);
}

/*******************************************************************************
    MNU_Setpoint_Confirm
*******************************************************************************/
void MNU_Setpoint_Confirm (void)
{
    SetpointEditState = EDIT_IDLE;
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Setpoint_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_Setpoint_Commit
*******************************************************************************/
void MNU_Setpoint_Commit(KEYB_PARAM keyId)
{
    KBH_ClearAllKeyProperties();
    MNH_SetActiveMenu(MENU_HOME);
    SetpointEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_Setpoint_StopSetpointEdition
*******************************************************************************/
void MNU_Setpoint_StopSetpointEdition(void)
{
    SetpointEditState = EDIT_CONFIRM;
    DPH_DisplaySetpointApplied(SetpointEditState);
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Setpoint_Confirm,  (KEYB_PARAM*)NULL, CONFIRM_TIMEOUT);
}

/*******************************************************************************
    MNU_Setpoint_IncSetpoint

    Increment a setpoint
*******************************************************************************/
void MNU_Setpoint_IncSetpoint(int16u value)
{
#ifndef PCT_HEAT
	UnitFormat_t format;
#endif
	TEMPERATURE_C_t maxsetpoint;

#ifdef PCT_HEAT
    maxsetpoint = 100;
    holdEditValue = MNU_Setpoint_GetHeatPctSetpoint();
#else
    maxsetpoint= STPM_GetSetpointMax();
    holdEditValue = MNU_Setpoint_GetSetpoint();
#endif
    uint8_t floorMode;

    DBTHRD_GetData(DBTYPE_FLOOR_MODE, &floorMode, THIS_THERMOSTAT);

    SetpointEditState = EDIT_IN_PROGRESS;

    if ((holdEditValue < maxsetpoint) && (holdEditValue != DB_SENSOR_INVALID) && (offMode == 0))
    {
#ifdef PCT_HEAT
        holdEditValue += 50;
        DPH_HandleHeatPctSetpointDisplay(holdEditValue);
#else
        DBTHRD_GetData(DBTYPE_TEMPERATURE_FORMAT,(void*)&format, INDEX_DONT_CARE);
        if(format == DEGREE_F)
        {
            holdEditValue = CVT_ConvertCtoF(holdEditValue, 100);
            holdEditValue += (value * 2);
            holdEditValue = CVT_ConvertFtoC(holdEditValue, 0);
        }
        else
        {
            holdEditValue = TLS_Round(holdEditValue, 50);
            holdEditValue += value;
            holdEditValue = TLS_Round(holdEditValue, 50);
        }
        if (holdEditValue > maxsetpoint)
        {
            holdEditValue = maxsetpoint;
        }
        DPH_DisplaySetpointApplied(SetpointEditState);
        if (floorMode == CONTROLMODE_FLOOR)
        {
            DPH_DisplaySetpoint(holdEditValue, FALSE, offMode, SET_FLOOR, TRUE);
        }
        else
        {
            DPH_DisplaySetpoint(holdEditValue, FALSE, offMode, SET_AMBIENT, TRUE);
        }
#endif
    }

#ifdef FLOOR
    if (offMode != 0)
    {
        offMode = 0;
        DBTHRD_SetData(DBTYPE_OFF_MODE_SETTING,(void*)&offMode, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    }
    minReached = 0;
    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_Setpoint_DecSetpointStartTurbo, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
#endif

#ifdef ON_OFF
    ForceOnState = 1;
#endif /* ON_OFF */

    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Setpoint_CommitSetpoint, (U8BIT*)NULL, SETPOINT_EDIT_TIMEOUT);
}

/*******************************************************************************
    MNU_Setpoint_DecSetpoint

    Decrement a setpoint
*******************************************************************************/
void MNU_Setpoint_DecSetpoint(int16u value)
{
#ifndef PCT_HEAT
	UnitFormat_t format;
#endif
	TEMPERATURE_C_t minsetpoint;

#ifdef PCT_HEAT
    minsetpoint = 0;
    holdEditValue = MNU_Setpoint_GetHeatPctSetpoint();
#else
    minsetpoint = STPM_GetSetpointMin();
    holdEditValue = MNU_Setpoint_GetSetpoint();
#endif
    uint8_t floorMode;

    DBTHRD_GetData(DBTYPE_FLOOR_MODE, &floorMode, THIS_THERMOSTAT);

    SetpointEditState = EDIT_IN_PROGRESS;

    if ((holdEditValue > minsetpoint) && (holdEditValue != DB_SENSOR_INVALID))
    {
#ifdef PCT_HEAT
        holdEditValue -= 50;
        DPH_HandleHeatPctSetpointDisplay(holdEditValue);
#else
        DBTHRD_GetData(DBTYPE_TEMPERATURE_FORMAT,(void*)&format, INDEX_DONT_CARE);
        if(format == DEGREE_F)
        {
            holdEditValue = CVT_ConvertCtoF(holdEditValue, 100);
            holdEditValue -= (value * 2);
            holdEditValue = CVT_ConvertFtoC(holdEditValue, 0);
        }
        else
        {
            holdEditValue = TLS_Round(holdEditValue, 50);
            holdEditValue -= value;
            holdEditValue = TLS_Round(holdEditValue, 50);
        }
        if (holdEditValue <= minsetpoint)
        {
            holdEditValue = minsetpoint;
        }
        DPH_DisplaySetpointApplied(SetpointEditState);
        if (floorMode == CONTROLMODE_FLOOR)
        {
            DPH_DisplaySetpoint(holdEditValue, FALSE, offMode, SET_FLOOR, TRUE);
        }
        else
        {
            DPH_DisplaySetpoint(holdEditValue, FALSE, offMode, SET_AMBIENT, TRUE);
        }
#endif
    }
#ifdef FLOOR
    if (holdEditValue == minsetpoint)
    {
        if (minReached == 0)
        {
            minReached = 1;
            // Monitor for hold key to enter off mode
            KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)MNU_Setpoint_SetOffMode, (void*) NULL, OS_MSTOTICK(KEYB_MENU_HOLD));
        }
    }
#endif

#ifdef ON_OFF
    ForceOnState = 0;
#endif /* ON_OFF */

    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Setpoint_CommitSetpoint, (U8BIT*)NULL, SETPOINT_EDIT_TIMEOUT);
}

#ifdef FLOOR
/*******************************************************************************
    MNU_Setpoint_SetOffMode

    Enter in Off Mode
*******************************************************************************/
static void MNU_Setpoint_SetOffMode(void)
{
    uint8_t offMode = 1;
    DBTHRD_SetData(DBTYPE_OFF_MODE_SETTING,(void*)&offMode, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);
}
#endif

/*******************************************************************************
    MNU_Setpoint_DecSetpointStartTurbo

    Decrement a setpoint
*******************************************************************************/
void MNU_Setpoint_DecSetpointStartTurbo(void)
{
    KBH_SetKeyHoldProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

    // Decrement setpoint
    MNU_Setpoint_DecSetpoint(50);
#ifdef FLOOR
    if (minReached == 0)
    {
        // Turn on key repeat notification
        KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)MNU_Setpoint_DecSetpoint, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_DELAY));
    }
#else
    // Turn on key repeat notification
    KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)MNU_Setpoint_DecSetpoint, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_DELAY));
#endif

    KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_Setpoint_SetpointStopTurbo, (void*)NULL);
    KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_Setpoint_SetpointStopTurbo, (void*)NULL);
}

/*******************************************************************************
    MNU_Setpoint_IncSetpointStartTurbo

    Increment a setpoint
*******************************************************************************/
void MNU_Setpoint_IncSetpointStartTurbo(void)
{
    KBH_SetKeyHoldProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

    // Increment setpoint
    MNU_Setpoint_IncSetpoint(50);
    // Turn on key repeat notification
    KBH_SetKeyRepeatProperties(KEY_UP, (FUNC)MNU_Setpoint_IncSetpoint, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_DELAY));

    KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_Setpoint_SetpointStopTurbo, (void*)NULL);
    KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_Setpoint_SetpointStopTurbo, (void*)NULL);
}

/*******************************************************************************
    MNU_Setpoint_SetpointStopTurbo
*******************************************************************************/
void MNU_Setpoint_SetpointStopTurbo(void)
{
    // Stop key repeat monitoring
	KBH_SetKeyRepeatProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

    // Start key hold monitoring
	KBH_SetKeyHoldProperties(KEY_UP, (FUNC)MNU_Setpoint_IncSetpointStartTurbo, (void*) NULL, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
#ifdef FLOOR
    if (minReached == 0)
    {
        KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_Setpoint_DecSetpointStartTurbo, (void*) NULL, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
    }
    else
    {
        KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_Setpoint_SetOffMode, (void*) NULL, OS_MSTOTICK(KEYB_MENU_HOLD));
    }
#else
    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_Setpoint_DecSetpointStartTurbo, (void*) NULL, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
#endif
	KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_Setpoint_IncSetpoint, (int16u*) 50);
    KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_Setpoint_DecSetpoint, (int16u*) 50);
}

/*******************************************************************************
    MNU_Setpoint_GetSetpoint

    Get the current setpoint from the Setpoint Manager
*******************************************************************************/
TEMPERATURE_C_t MNU_Setpoint_GetSetpoint(void)
{
    TEMPERATURE_C_t setpoint;
    uint8_t floorMode;

    // If not currently in setpoint edition mode
    if ( SetpointEditState == EDIT_IDLE )
    {
        DBTHRD_GetData(DBTYPE_FLOOR_MODE,(void*)&floorMode, THIS_THERMOSTAT);
        if (floorMode == CONTROLMODE_FLOOR)
        {
            // Return control ambient setpoint
            DBTHRD_GetData(DBTYPE_FLOOR_SETPOINT,(void*)&setpoint, THIS_THERMOSTAT);
        }
        else
        {
            // Return control floor setpoint
            DBTHRD_GetData(DBTYPE_AMBIENT_SETPOINT,(void*)&setpoint, THIS_THERMOSTAT);
        }
    }
    // Else currently in setpoint edition mode
    else
    {
        // Return current edited setpoint value
        setpoint = holdEditValue;
    }
    return ( setpoint );
}

#ifdef PCT_HEAT
/*******************************************************************************
    MNU_Setpoint_GetHeatPctSetpoint

    Get the current heat percentage setpoint from the Load Manager
*******************************************************************************/
HEAT_DEMAND_PERCENT_t MNU_Setpoint_GetHeatPctSetpoint(void)
{
    HEAT_DEMAND_PERCENT_t setpoint;
    // If not currently in setpoint edition mode
    if ( SetpointEditState == EDIT_IDLE ) {
        // Return control setpoint
        MED_GetControlHeatPercent(&setpoint);
    }
    // Else currently in setpoint edition mode
    else {
        // Return current edited setpoint value
        setpoint = holdEditValue;
    }
    return ( setpoint );
}
#endif

/*******************************************************************************
    MNU_Setpoint_CommitSetpoint

    Update the Setpoint Manager with a new setpoint
*******************************************************************************/
void MNU_Setpoint_CommitSetpoint( void )
{
#ifdef PCT_HEAT
    HEAT_DEMAND_PERCENT_t     currentSetpoint;

    MED_GetControlHeatPercent(&currentSetpoint);
#else
    TEMPERATURE_C_t     currentSetpoint;
    uint8_t floorMode;

    DBTHRD_GetData(DBTYPE_FLOOR_MODE,(void*)&floorMode, THIS_THERMOSTAT);
    if (floorMode == CONTROLMODE_FLOOR)
    {
        DBTHRD_GetData(DBTYPE_FLOOR_SETPOINT,(void*)&currentSetpoint, THIS_THERMOSTAT);
    }
    else
    {
        DBTHRD_GetData(DBTYPE_AMBIENT_SETPOINT,(void*)&currentSetpoint, THIS_THERMOSTAT);
    }
#endif

    if ( SetpointEditState == EDIT_IN_PROGRESS )
    {
        // If user changed setpoint
        if (( holdEditValue != currentSetpoint ) && (holdEditValue != DB_SENSOR_INVALID))
        {
            // Apply user setpoint
#ifdef PCT_HEAT
            MED_SetControlHeatPercent(holdEditValue);
#else
            if (floorMode == CONTROLMODE_FLOOR)
            {
                DBTHRD_SetData(DBTYPE_FLOOR_SETPOINT,(void*)&holdEditValue, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            }
            else
            {
                DBTHRD_SetData(DBTYPE_AMBIENT_SETPOINT,(void*)&holdEditValue, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            }
#endif
            MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Setpoint_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
        }
    }
    MNU_Setpoint_StopSetpointEdition();
}

/*******************************************************************************
* @brief  Function that sets which displayed items must be refreshed
* @inputs refreshBits: each item to be refreshed sets a specific bit
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/08/26
*******************************************************************************/
static void MNU_RefreshSetpointDisplayItems(int16u refreshBits)
{
    if (refreshBits & TEMP_DISPLAY_BIT)
    {
        refreshBits &= ~TEMP_DISPLAY_BIT;
        temp_display = TRUE;
    }

    if (refreshBits & HEAT_DISPLAY_BIT)
    {
        refreshBits &= ~HEAT_DISPLAY_BIT;
        heat_display = TRUE;
    }

    if (refreshBits & SETPOINT_DISPLAY_BIT)
    {
        refreshBits &= ~SETPOINT_DISPLAY_BIT;
        setpoint_display = TRUE;
    }
    refreshBits = 0;
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Simard
* @date   2016/10/20
*******************************************************************************/
DB_NOTIFICATION(MNU_SetpointOnAmbientTemperatureUpdate)
{
	if ((status == DBERR_CHANGEABOVETHRESHOLD) || (status == DBERR_INVALIDVALUE))
	{
		MNU_RefreshSetpointDisplayItems(TEMP_DISPLAY_BIT | HUM_DISPLAY_BIT);
        MNU_NotifyMenuChange();
	}
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Simard
* @date   2016/10/21
*******************************************************************************/
DB_NOTIFICATION(MNU_SetpointOnAmbientSetpointUpdate)
{
    MNU_RefreshSetpointDisplayItems(SETPOINT_DISPLAY_BIT);
    MNU_NotifyMenuChange();
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @inputs
* @retval None
* @author Jean-Fran�ois Simard
* @date   2016/10/24
*******************************************************************************/
DB_NOTIFICATION(MNU_SetpointOnTemperatureUnitUpdate)
{
    MNU_SetpointOnAmbientSetpointUpdate( status, instance, source);
	MNU_SetpointOnAmbientTemperatureUpdate(status, instance, source);
}

/*******************************************************************************
* @brief  Notification when the heat demand is updated
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/23
*******************************************************************************/
static DB_NOTIFICATION(MNU_SetpointOnHeatDemandUpdate)
{
    MNU_RefreshSetpointDisplayItems(HEAT_DISPLAY_BIT);
    MNU_NotifyMenuChange();
}

/*******************************************************************************
* @brief  Notification when the off mode status is updated
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2019/05/10
*******************************************************************************/
static DB_NOTIFICATION(MNU_SetpointOffModeUpdate)
{
    DBTHRD_GetData(DBTYPE_OFF_MODE_SETTING,(void*)&offMode, THIS_THERMOSTAT);
    MNU_RefreshSetpointDisplayItems(SETPOINT_DISPLAY_BIT);
    MNU_NotifyMenuChange();
}
