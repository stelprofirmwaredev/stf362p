/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       AddThermostatMenu.c

    \brief      Source code file for the AddThermostatMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the AddThermostatMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "AddThermostatMenu.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\HAL\inc\Display.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/
#define MAX_ASSOCIATION_DELAY   30  //30 minutes

/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   AddThermostatEditState = EDIT_IDLE;
static U8BIT permitNbMinutes;
static susbscriptionHandle_t hNetworkFormedNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hThermostatAddedNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hOtherDeviceAddedNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static U8BIT numberOfAddedThermostats;
static U8BIT numberOfOtherDevices;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void MenuTimeout(void);
static DB_NOTIFICATION(OnNetworkFormed);
static DB_NOTIFICATION(OnThermostatAdded);
static DB_NOTIFICATION(OnOtherDeviceAdded);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    MenuTimeout
*******************************************************************************/
static void MenuTimeout(void)
{
    permitNbMinutes++;
    if (permitNbMinutes >= MAX_ASSOCIATION_DELAY)
    {
        permitNbMinutes = 0;
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_AddThermostat_Commit, (KEYB_PARAM*)KEY_HOME, IMMEDIATELY);

    }
    else
    {
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MenuTimeout, (KEYB_PARAM*)NULL, ZIGBEE_ASSOCIATION_TIMEOUT);
    }
}

/*******************************************************************************
    OnNetworkFormed notification
*******************************************************************************/
static DB_NOTIFICATION(OnNetworkFormed)
{
    DPH_DisplayAddThermostat(numberOfAddedThermostats, numberOfOtherDevices);
}

/*******************************************************************************
    OnThermostatAdded notification
*******************************************************************************/
static DB_NOTIFICATION(OnThermostatAdded)
{
    numberOfAddedThermostats++;
    DPH_DisplayAddThermostat(numberOfAddedThermostats, numberOfOtherDevices);
}

/*******************************************************************************
    OnOtherDeviceAdded notification
*******************************************************************************/
static DB_NOTIFICATION(OnOtherDeviceAdded)
{
    numberOfOtherDevices++;
    DPH_DisplayAddThermostat(numberOfAddedThermostats, numberOfOtherDevices);
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_AddThermostat_Entry
*******************************************************************************/
void MNU_AddThermostat_Entry(void)
{
    uint8_t zap;
    dbType_ZigBeeJoinInfo_t joinInfo;

    AddThermostatEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);

    permitNbMinutes = 0;
    numberOfAddedThermostats = 0;
    numberOfOtherDevices = 0;
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MenuTimeout, (KEYB_PARAM*)NULL, ZIGBEE_ASSOCIATION_TIMEOUT);

//TODO - enable router mode
    joinInfo.DeviceType = 0;//DEVICE_TYPE_COORDINATOR;
    joinInfo.Channel = CHANNEL_11;
    joinInfo.JoinType = JOIN_AUTO;
    DBTHRD_SetData(DBTYPE_ZIGBEE_JOIN_INFO,(void*)&joinInfo, INDEX_DONT_CARE, DBCHANGE_LOCAL);
    zap = 1;
    DBTHRD_SetData(DBTYPE_ZIGBEE_ASSOCIATION_PERMIT,(void*)&zap, INDEX_DONT_CARE, DBCHANGE_LOCAL);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show Add Thermostat screen
    //
    DPH_DisplayAddThermostat(numberOfAddedThermostats, numberOfOtherDevices);

    hNetworkFormedNotification = DBTHRD_SubscribeToNotification(DBTYPE_ZIGBEE_NETWORK_INFO,
                                                       OnNetworkFormed,
                                                       DBCHANGE_ALLCHANGES,
                                                       INDEX_DONT_CARE);

    hThermostatAddedNotification = DBTHRD_SubscribeToNotification(DBTYPE_THERMOSTAT_ADDED,
                                                       OnThermostatAdded,
                                                       DBCHANGE_ALLCHANGES,
                                                       INDEX_DONT_CARE);

    hOtherDeviceAddedNotification = DBTHRD_SubscribeToNotification(DBTYPE_OTHER_DEVICE_ADDED,
                                                       OnOtherDeviceAdded,
                                                       DBCHANGE_ALLCHANGES,
                                                       INDEX_DONT_CARE);

    //Needed here, because we may go from Idle Menu to this one, and display is disabled in Idle menu.
    DisplayON();
}

/*******************************************************************************
    MNU_AddThermostat_Core

    Allows to add a new thermostat to the network
*******************************************************************************/
void MNU_AddThermostat_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_AddThermostat_Commit, (void*)KEY_BACK);
        //KBH_SetKeyPressProperties(KEY_DONE, (FUNC)MNU_AddThermostat_Commit, (void*)KEY_DONE);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_AddThermostat_Exit
*******************************************************************************/
void MNU_AddThermostat_Exit(void)
{
    uint8_t zap;

    AddThermostatEditState = EDIT_IDLE;

    zap = 0;
    DBTHRD_SetData(DBTYPE_ZIGBEE_ASSOCIATION_PERMIT,(void*)&zap, INDEX_DONT_CARE, DBCHANGE_LOCAL);

    DBTHRD_UnsubscribeToNotification(hNetworkFormedNotification);
    DBTHRD_UnsubscribeToNotification(hThermostatAddedNotification);
    DBTHRD_UnsubscribeToNotification(hOtherDeviceAddedNotification);
}

/*******************************************************************************
    MNU_AddThermostat_Commit
*******************************************************************************/
void MNU_AddThermostat_Commit(KEYB_PARAM keyId)
{
    static KEYB_PARAM commitKey = NULL;

    if ( AddThermostatEditState == EDIT_IN_PROGRESS )
    {
        commitKey = keyId;
        KBH_ClearAllKeyProperties();
        switch (commitKey)
        {
            //case KEY_DONE:
            case KEY_BACK:
            default:
                MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_AddThermostat_Commit, (KEYB_PARAM*)commitKey, IMMEDIATELY);
                break;
        }
        DPH_DisplayAddThermostat(numberOfAddedThermostats, numberOfOtherDevices);
        AddThermostatEditState = EDIT_COMMITTING;
    }
    else if ( AddThermostatEditState == EDIT_COMMITTING )
    {
        AddThermostatEditState = EDIT_IDLE;
        switch (commitKey)
        {
            case KEY_BACK:
            case KEY_DONE:
                MNH_SetActiveMenu(MENU_ZIGBEE_SETUP);
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
    }
}
