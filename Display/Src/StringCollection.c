/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2016, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       StringCollection.c

    \brief      Source code file for the StringCollection module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This module provides project specific string definitions.

    Note: String storage must be in flash (ex IAR: --string_literals_in_flash)

    Table of modifications:

        - Date:         2016-11-16
        - Description:  First version

*******************************************************************************/

/*******************************************************************************
	Includes
*******************************************************************************/
#include "StringCollection.h"

/*******************************************************************************
	Private constants definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
	Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
	Private data types
*******************************************************************************/

    //Empty

/*******************************************************************************
	Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
	Private global variables declarations
*******************************************************************************/

//! English string table
const unsigned char* ENGLISH_STR_TABLE[] = {

    "", //STRID_NULL
    "--", //STRID_NOTEMP

    //Toolbox related strings
    ">", //STRID_NEXT
    "<", //STRID_PREVIOUS
    "DONE", //STRID_DONE
    "OK", //STRID_OK
    "APPLY", //STRID_APPLY
    "YES", //STRID_YES

    //Temperature related strings
    "--", //STRID_ERR
    "Low", //STRID_LOW
    "High", //STRID_HIGH

    //Context related strings
    "AMBIENT TEMPERATURE", //STRID_INDOOR
    "FLOOR TEMPERATURE", //STRID_FLOOR
    "WEATHER CONDITION", //STRID_OUTDOOR
    "SETPOINT", //STRID_SETPOINT
    "AMBIENT SETPOINT", //STRID_SETPOINT_AMBIENT
    "FLOOR SETPOINT", //STRID_SETPOINT_FLOOR
    "GROUP SETPOINT", //STRID_SETPOINT_GROUP
    "GROUP SETPOINT (FLOOR)", //STRID_SETPOINT_GROUP_FLOOR
    "TEMPORARY SETPOINT", //STRID_SETPOINT_TEMPORARY
    "FLOOR LIMIT", //STRID_SETPOINT_LIMIT
    "OFF", //STRID_SETPOINT_OFF
    "Lobby is now", //STRID_RECOVERY_HEATING_CYCLE_1
    "in recovery mode", //STRID_RECOVERY_HEATING_CYCLE_2

    //Settings related strings
    "SETTINGS", //STRID_SETTINGS
    "Date and Time", //STRID_SETTINGS_DATE_TIME
    "Mode and Schedule", //STRID_SETTINGS_PROGRAMS
    "Options", //STRID_SETTINGS_OPTIONS
    "Thermostat list", //STRID_SETTINGS_TSTAT_LIST
    "Alerts", //STRID_SETTINGS_ALERTS
    "Geofencing", //STRID_SETTINGS_GEOFENCING
    "Reset to default", //STRID_SETTINGS_RESET
    "Info �S", //STRID_SETTINGS_INFO_S

    //Groups related strings
    "GROUPS", //STRID_GROUPS
    "GROUPS", //STRID_GROUP_NAME_LEGEND,
    "SETPOINT", //STRID_GROUP_SETPOINT_LEGEND
    "THERMOSTATS", //STRID_GROUP_TSTAT_LEGEND
    "TEMPERATURE.", //STRID_GROUP_TEMP_LEGEND
    "No thermostat", //STRID_GROUP_NO_TSTAT
    "in this group", //STRID_GROUP_IN_GROUP

    //Mode related strings
    "MODE", //STRID_MODE
    "Non-Programmable", //STRID_MODE_MANUAL
    "Follow Schedule", //STRID_MODE_SCHEDULE
    "GO DIRECTLY IN :", //STRID_MODE_DEROGATION
    "Wake", //STRID_MODE_WAKE
    "Leave", //STRID_MODE_LEAVE
    "Return", //STRID_MODE_RETURN
    "Sleep", //STRID_MODE_SLEEP

    //Wi-Fi Setup related strings
    "WI-FI SETUP", //STRID_WIFI_SETUP
    "Not connected", //STRID_WIFI_SETUP_NOT_CONNECTED
    "Current network :", //STRID_WIFI_SETUP_CURRENT_NETWORK
    "Register device", //STRID_WIFI_SETUP_REGISTER_DEVICE
    "Scan networks", //STRID_WIFI_SETUP_SCAN_NETWORKS

    //Wi-Fi networks related strings
    "NETWORKS", //STRID_NETWORKS
    "Scan in progress...", //STRID_NETWORKS_SCAN_IN_PROGRESS

    //Password related strings
    "PASSWORD", //STRID_PASSWORD
    "Selected network :", //STRID_PASSWORD_SELECTED_NETWORK
    "Password :", //STRID_PASSWORD_CURRENT_PASSWORD
    "CONNECT",  //STRID_PASSWORD_CONNECT

    //ZigBee setup related strings
    "ZIGBEE SETUP", //STRID_ZIGBEE_SETUP
    "Add a new thermostat", //STRID_ZIGBEE_SETUP_ADD_NEW_TSTAT
    "Leave network", //STRID_ZIGBEE_SETUP_LEAVE_NETWORK
    "Add with Install Code", //STRID_ZIGBEE_SETUP_INSTALL_CODE
    "Join existing network", //STRID_ZIGBEE_SETUP_JOIN_EXISTING_NETWORK
    "Network ID : 0x", //STRID_ZIGBEE_SETUP_PANID
    "Node ID : 0x", //STRID_ZIGBEE_SETUP_NODEID
    "Channel :", //STRID_ZIGBEE_SETUP_CHANNEL

    //Options related strings
    "OPTIONS", //STRID_OPTIONS
    "Language", //STRID_OPTIONS_LANGUAGE
    "Temperature format", //STRID_OPTIONS_UNIT_FORMAT
    "Time preferences", //STRID_OPTIONS_TIME_FORMAT
    "Heat mode", //STRID_OPTIONS_HEAT_MODE
    "Open window", //STRID_OPTIONS_OPEN_WINDOW
    "Screen lock", //STRID_OPTIONS_LOCK
    "Screen saver", //STRID_OPTIONS_BACKLIGHT
    "Proximity sensor", //STRID_OPTIONS_PIR
    "Control mode", //STRID_OPTIONS_CONTROL_MODE

    //Heat mode related strings
    "HEAT MODE", //STRID_HEAT_MODE_1
    "", //STRID_HEAT_MODE_2
    "Baseboard", //STRID_HEAT_MODE_BASEBOARD
    "Fan-forced heater", //STRID_HEAT_MODE_FAN
    "Lobby Mode", //STRID_LOBBY_SUB_HEADER
    "Standard room", //STRID_STANDARD_ROOM
    "Lobby", //STRID_LOBBY

    //Control mode related strings
    "CONTROL MODE", //STRID_CONTROL_MODE_1
    "", //STRID_CONTROL_MODE_2
    "Ambient", //STRID_CONTROL_MODE_AMBIENT
    "Floor", //STRID_CONTROL_MODE_FLOOR
    "Ambient + floor", //STRID_CONTROL_MODE_AMBIENTFLOOR
    "Floor Type", //STRID_CONTROL_MODE_FLOOR_TYPE
    "Floor Limit", //STRID_CONTROL_MODE_FLOOR_LIMIT

    //Floor type related strings
    "FLOOR TYPE", //STRID_FLOOR_TYPE_1
    "", //STRID_FLOOR_TYPE_2
    "Tile floor", //STRID_FLOOR_TYPE_TILE
    "Engineered wood", //STRID_FLOOR_TYPE_ENGINEERED
    "The maximum setpoint", //STRID_FLOOR_TYPE_ADVISE1
    "is limited to a lower", //STRID_FLOOR_TYPE_ADVISE2
    "value with an", //STRID_FLOOR_TYPE_ADVISE3
    "engineered wood floor", //STRID_FLOOR_TYPE_ADVISE4

    //Floor limit related strings
    "FLOOR", //STRID_FLOOR_LIMIT_TITLE1
    "LIMIT", //STRID_FLOOR_LIMIT_TITLE2
    "When the floor", //STRID_FLOOR_LIMIT_ADVISE1
    "temperature reaches", //STRID_FLOOR_LIMIT_ADVISE2
    "this limit,", //STRID_FLOOR_LIMIT_ADVISE3
    "the heating is limited", //STRID_FLOOR_LIMIT_ADVISE4

    //Window Open Detection related strings
    "OPEN WINDOW", //STRID_OPEN_WINDOW1
    "", //STRID_OPEN_WINDOW2
    "Detection off", //STRID_OPEN_WINDOW_DISABLE
    "Detection on", //STRID_OPEN_WINDOW_ENABLE

    //Lock related strings
    "SCREEN", //STRID_LOCK_1
    "LOCK", //STRID_LOCK_2
    "Unlock", //STRID_LOCK_UNLOCK
    "Lock", //STRID_LOCK_LOCK
    "THIS THERMOSTAT", //STRID_LOCK_THIS_THERMOSTAT
    "ALL THERMOSTATS", //STRID_LOCK_ALL_THERMOSTATS
    "INSTRUCTION",//STRID_LOCK_ADVISE
    "To unlock the screen :", //STRID_LOCK_ADVISE1
    "From the main screen,", //STRID_LOCK_ADVISE2
    "press and hold the       ", //STRID_LOCK_ADVISE3
    "icon for 3 seconds", //STRID_LOCK_ADVISE4

    //Backlight related strings
    "SCREEN SAVER", //STRID_BACKLIGHT1
    "", //STRID_BACKLIGHT2
    "1 minute", //STRID_BACKLIGHT_60_SEC
    "5 minutes", //STRID_BACKLIGHT_300_SEC
    "10 minutes", //STRID_BACKLIGHT_600_SEC,
    "30 minutes", //STRID_BACKLIGHT_1800_SEC,

    //Language related strings
    "LANGUAGE", //STRID_LANGUAGE
    "Fran�ais", //STRID_LANGUAGE_FRANCAIS
    "English", //STRID_LANGUAGE_ENGLISH
    "LANGUE", //STRID_LANGUAGE_TITLE_FR
    "LANGUAGE", //STRID_LANGUAGE_TITLE_EN
    "APPLIQUER", //STRID_LANGUAGE_CONFIRM_FR
    "APPLY", //STRID_LANGUAGE_CONFIRM_EN

    //Unit format related strings
    "TEMPERATURE", //STRID_TEMPERATURE
    "FORMAT", //STRID_FORMAT
    "�Celsius", //STRID_TEMP_FORMAT_CELSIUS
    "�Fahrenheit", //STRID_TEMP_FORMAT_FAHRENHEIT

    //Time format related strings
    "TIME", //STRID_TIME_FORMAT_1
    "PREFERENCES", //STRID_TIME_FORMAT_2
    "24 hours", //STRID_TIME_FORMAT_24_HOURS
    "12 hours", //STRID_TIME_FORMAT_12_HOURS
    "TIME FORMAT", //STRID_TIME_FORMAT
    "AUTOMATIC DAYLIGHT SAVING TIME", //STRID_TIME_FORMAT_DST
    "Enabled", //STRID_TIME_FORMAT_DST_ON
    "Disabled", //STRID_TIME_FORMAT_DST_OFF

    //Reset related strings
    "RESET", //STRID_RESET
    "Reset to default?", //STRID_RESET_RESET_TO_DEFAULT
    "Are you sure?", //STRID_RESET_CONFIRMATION_PROMPT
    "CONFIRM", //STRID_RESET_CONFIRMATION
    "Reset in progress...", //STRID_RESET_IN_PROGRESS_1
    "This will erase your", //STRID_RESET_ADVISE1
    "settings and the device", //STRID_RESET_ADVISE2
    "will revert back to", //STRID_RESET_ADVISE3
    "the factory settings", //STRID_RESET_ADVISE4

    //Info related strings
    "�STELPRO", //STRID_INFO
    "Thermostat version : ", //STRID_INFO_TSTAT_VERSION
    "ZigBee version : ", //STRID_INFO_ZIGBEE_VERSION
    "Wi-Fi version : ", //STRID_INFO_WIFI_VERSION
    "Produced on : ", //STRID_INFO_PRODUCED_ON
    "Control checksum : ", //STRID_INFO_CHECKSUM
    "Short : ", //STRID_INFO_CHECKSUM_SHORT
    "Long : ", //STRID_INFO_CHECKSUM_LONG

    //Add new thermostat related strings
    "ADD A NEW", //STRID_ADD_NEW
    "THERMOSTAT", //STRID_THERMOSTAT
    "Put new thermostat", //STRID_ADD_NEW_TSTAT_PUT_NEW_TSTAT
    "in pairing mode", //STRID_ADD_NEW_TSTAT_IN_PAIRING_MODE
    "on channel : ", //STRID_ADD_NEW_TSTAT_ON_CHANNEL
    "Starting a network", //STRID_ADD_NEW_TSTAT_STARTING_NETWORK
    "Please wait...", //STRID_ADD_NEW_TSTAT_PLEASE_WAIT
    "New thermostat added :", //STRID_ADD_NEW_TSTAT_NEW_TSTAT_ADDED
    "Other device added :", //STRID_ADD_NEW_TSTAT_OTHER_DEVICE_ADDED

    //Install code related strings
    "INSTALL CODE", //STRID_INSTALL_CODE
    "Enter the install code", //STRID_INSTALL_CODE_ENTER_INSTALL_CODE
    "found on thermostat.", //STRID_INSTALL_CODE_FOUND_ON_THERMOSTAT
    "Install code :", //STRID_INSTALL_CODE_CURRENT_CODE
    "ADD WITH", //STRID_INSTALL_CODE_TITLE1
    "INSTALL CODE", //STRID_INSTALL_CODE_TITLE2
    "Oops, wrong install code!", //STRID_INSTALL_CODE_WRONG_CODE1
    "This code is not valid", //STRID_INSTALL_CODE_WRONG_CODE2

    //MAC Address related strings
    "MAC ADDRESS", //STRID_MAC_ADDRESS
    "Enter the MAC Address", //STRID_ENTER_MAC_ADRESS
    "found on thermostat", //STRID_FOUND_ON_THERMOSTAT
    "MAC Address :", //STRID_MAC_ADDRESS_CURRENT_MAC_ADDRESS

    //Join ZigBee network related strings
    "JOIN NETWORK", //STRID_JOIN_NETWORK
    "Automatic", //STRID_JOIN_NETWORK_AUTO
    "By channel", //STRID_JOIN_NETWORK_SPECIFIC
    "SPECIFY CHANNEL", //STRID_JOIN_NETWORK_SPECIFY_CHANNEL
    "JOIN", //STRID_JOIN_NETWORK_JOIN
    "JOINING...", //STRID_JOIN_NETWORK_JOINING
    "SUCCESS", //STRID_JOIN_NETWORK_SUCCESS
    "FAILED", //STRID_JOIN_NETWORK_FAILED

    //Leave ZigBee network related strings
    "LEAVE", //STRID_LEAVE_NETWORK1
    "NETWORK", //STRID_LEAVE_NETWORK2
    "Are you sure?", //STRID_LEAVE_NETWORK_CONFIRMATION_PROMPT
    "Leave now", //STRID_LEAVE_NETWORK_CONFIRMATION
    "Leave in progress...", //STRID_LEAVE_NETWORK_IN_PROGRESS1
    "Please wait", //STRID_LEAVE_NETWORK_IN_PROGRESS2
    "Network left", //STRID_LEAVE_NETWORK_LEFT

    //Alert related strings
    "ALERTS", //STRID_ALERTS
    "Good news!", //STRID_ALERTS_GOOD_NEWS
    "No active alert", //STRID_ALERTS_NO_ALERT
    "Tstat connection lost", //STRID_ALERTS_NO_ZIGBEE
    "Window is open", //STRID_ALERTS_OPEN_WINDOW
    "Overheating appliance", //STRID_ALERTS_OVERHEAT
    "Ground fault detected", //STRID_ALERTS_GFCI
    "Wi-Fi connection lost", //STRID_ALERTS_NO_WIFI
    "Update pending", //STRID_ALERTS_OTA_PENDING
    "Defective ZigBee radio", //STRID_ALERTS_ZIGBEE_ERROR
    "No ZigBee Network", //STRID_ALERTS_NO_ZIGBEE_NETWORK
    "Cloud not available", //STRID_ALERTS_SERVER_PROBLEM
    "End of Life", //STRID_ALERTS_END_OF_LIFE
    "Floor limit reached", //STRID_ALERTS_FLOOR_TEMP_LIMIT_REACHED
    "Relays wearing out", //STRID_ALERTS_RELAYS_WEARING_OUT
    "Missing floor sensor", //STRID_ALERTS_NO_FLOOR_SENSOR
    "Missing floor sensor", //STRID_ALERTS_NO_FLOOR_SENSOR_F
    "Date and time not set", //STRID_ALERTS_TIME_NOT_SET
    "Unknown alert", //STRID_ALERTS_UNDEFINED

    //Alert details related strings
    "ALERT/DETAIL", //STRID_ALERT_DETAILS
    "ALERT:", //STRID_ALERT_DETAILS_ALERT
    "THERMOSTAT:", //STRID_ALERT_DETAILS_THERMOSTAT
    "ADVICE", //STRID_ALERT_DETAILS_ADVISE
    "Tstat connection lost", //STRID_ALERT_DETAILS_NO_ZIGBEE_1
    "Make sure the", //STRID_ALERT_DETAILS_NO_ZIGBEE_2
    "thermostat is powered", //STRID_ALERT_DETAILS_NO_ZIGBEE_3
    "and connected", //STRID_ALERT_DETAILS_NO_ZIGBEE_4
    "Window or door is open", //STRID_ALERT_DETAILS_OPEN_WINDOW_1
    "The setpoint", //STRID_ALERT_DETAILS_OPEN_WINDOW_2
    "has been lowered", //STRID_ALERT_DETAILS_OPEN_WINDOW_3
    "to save energy", //STRID_ALERT_DETAILS_OPEN_WINDOW_4
    "Overheating appliance", //STRID_ALERT_DETAILS_OVERHEAT_1
    "Make sure the heater", //STRID_ALERT_DETAILS_OVERHEAT_2
    "is not blocked", //STRID_ALERT_DETAILS_OVERHEAT_3
    "or obstructed", //STRID_ALERT_DETAILS_OVERHEAT_4
    "Ground fault detected", //STRID_ALERT_DETAILS_GFCI_1
    "The floor protection", //STRID_ALERT_DETAILS_GFCI_2
    "circuit must be reset", //STRID_ALERT_DETAILS_GFCI_3
    "on the thermostat to", //STRID_ALERT_DETAILS_GFCI_4
    "allow heating again", //STRID_ALERT_DETAILS_GFCI_5
    "Wi-Fi connection lost", //STRID_ALERT_DETAILS_NO_WIFI_1
    "Make sure you have", //STRID_ALERT_DETAILS_NO_WIFI_2
    "an Internet connection,", //STRID_ALERT_DETAILS_NO_WIFI_3
    "check your router", //STRID_ALERT_DETAILS_NO_WIFI_4
    "Update pending", //STRID_ALERT_DETAILS_OTA_PENDING_1
    "Update applied in", //STRID_ALERT_DETAILS_OTA_PENDING_2
    " hours and ", //STRID_ALERT_DETAILS_OTA_PENDING_3A
    " minutes", //STRID_ALERT_DETAILS_OTA_PENDING_3B
    "or apply it now", //STRID_ALERT_DETAILS_OTA_PENDING_4
    "Defective ZigBee radio", //STRID_ALERT_DETAILS_ZIGBEE_ERROR_1
    "CRITICAL ERROR", //STRID_ALERT_DETAILS_ZIGBEE_ERROR_2
    "ZigBee communications", //STRID_ALERT_DETAILS_ZIGBEE_ERROR_3
    "are impossible", //STRID_ALERT_DETAILS_ZIGBEE_ERROR_4
    "No ZigBee Network", //STRID_ALERT_DETAILS_NO_ZIGBEE_NETWORK_1
    "Cannot create ZigBee", //STRID_ALERT_DETAILS_NO_ZIGBEE_NETWORK_2
    "network. Perform Reset", //STRID_ALERT_DETAILS_NO_ZIGBEE_NETWORK_3
    "to Factory Default", //STRID_ALERT_DETAILS_NO_ZIGBEE_NETWORK_4
    "Cloud not available", //STRID_ALERT_DETAILS_SERVER_PROBLEM_1
    "Check your Internet", //STRID_ALERT_DETAILS_SERVER_PROBLEM_2
    "If problem persist", //STRID_ALERT_DETAILS_SERVER_PROBLEM_3
    "contact Stelpro", //STRID_ALERT_DETAILS_SERVER_PROBLEM_4
    "End of Life", //STRID_ALERT_DETAILS_EOL_1
    "The GFCI circuit is", //STRID_ALERT_DETAILS_EOL_2
    "no longer functional", //STRID_ALERT_DETAILS_EOL_3
    "Heating is deactivated", //STRID_ALERT_DETAILS_EOL_4
    "Replace the thermostat", //STRID_ALERT_DETAILS_EOL_5
    "Floor limit reached", //STRID_ALERT_DETAILS_FLOOR_TEMP_LIMIT_1
    "The floor temperature", //STRID_ALERT_DETAILS_FLOOR_TEMP_LIMIT_2
    "limit has been reached", //STRID_ALERT_DETAILS_FLOOR_TEMP_LIMIT_3
    "Heating is limited", //STRID_ALERT_DETAILS_FLOOR_TEMP_LIMIT_4
    "to protect your floor", //STRID_ALERT_DETAILS_FLOOR_TEMP_LIMIT_5
    "Relays wearing out", //STRID_ALERT_DETAILS_RELAYS_WEARING_OUT_1
    "The relays reached", //STRID_ALERT_DETAILS_RELAYS_WEARING_OUT_2
    "their theoric lifetime", //STRID_ALERT_DETAILS_RELAYS_WEARING_OUT_3
    "Plan the thermostat", //STRID_ALERT_DETAILS_RELAYS_WEARING_OUT_4
    "replacement soon", //STRID_ALERT_DETAILS_RELAYS_WEARING_OUT_5
    "Missing floor sensor", //STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_1
    "No \"floor sensor\"", //STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_2
    "The floor isn't protected", //STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_3
    "in temperature anymore", //STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_4
    "Check sensor connection", //STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_5
    "Missing floor sensor", //STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_F_1
    "No \"floor sensor\"", //STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_F_2
    "No heating is allowed", //STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_F_3
    "Check the floor sensor", //STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_F_4
    "connection", //STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_F_5
    "Date and time not set", //STRID_ALERT_DETAILS_TIME_NOT_SET_1
    "Please set the date/time", //STRID_ALERT_DETAILS_TIME_NOT_SET_2
    "to enable the schedule.", //STRID_ALERT_DETAILS_TIME_NOT_SET_3
    "Click the button above", //STRID_ALERT_DETAILS_TIME_NOT_SET_4
    "to continue", //STRID_ALERT_DETAILS_TIME_NOT_SET_5
    "Set the date and time", //STRID_ALERT_DETAILS_TIME_NOT_SET_ACTION
    "Unknown alert", //STRID_ALERT_DETAILS_UNDEFINED_1
    "An unknown alert", //STRID_ALERT_DETAILS_UNDEFINED_2
    "is detected, contact", //STRID_ALERT_DETAILS_UNDEFINED_3
    "Stelpro support", //STRID_ALERT_DETAILS_UNDEFINED_4
    "to diagnose this", //STRID_ALERT_DETAILS_UNDEFINED_5

    //Wi-Fi join related strings
    "WI-FI", //STRID_WIFI_JOIN1
    "CONNECTION", //STRID_WIFI_JOIN2
    "Wi-Fi join in progress...", //STRID_WIFI_JOIN_IN_PROGRESS
    "Wi-Fi joined successfully", //STRID_WIFI_JOIN_SUCCESS
    "Oops : Wrong password", //STRID_WIFI_JOIN_BAD_PASSWORD
    "Oops : No network", //STRID_WIFI_JOIN_NO_NETWORK
    "Oops : Unable to connect", //STRID_WIFI_JOIN_ERROR
    "Oops : Server is down", //STRID_WIFI_JOIN_SERVER_ERROR

    //Registration related strings
    "REGISTRATION", //STRID_REGISTRATION
    "Registration completed!", //STRID_REGISTRATION_COMPLETED
    "Registration code :", //STRID_REGISTRATION_CODE
    "Registration in progress...", //STRID_REGISTRATION_IN_PROGRESS
    "This may take a while", //STRID_REGISTRATION_PLEASE_WAIT1
    "Please wait...", //STRID_REGISTRATION_PLEASE_WAIT2
    "ADVICE", //STRID_REGISTER_ADVISE
    "Download the Maestro", //STRID_REGISTER_ADVISE2
    "app and enter this code", //STRID_REGISTER_ADVISE3
    "to register your home", //STRID_REGISTER_ADVISE4
    "to your account", //STRID_REGISTER_ADVISE5

    //Activities related strings
    "ACTIVITIES", //STRID_ACTIVITIES
    "No activity defined", //STRID_ACTIVITIES_NO_ACTIVITY1
    "Use the Stelpro mobile", //STRID_ACTIVITIES_NO_ACTIVITY2
    "application to create", //STRID_ACTIVITIES_NO_ACTIVITY3
    "your own activities", //STRID_ACTIVITIES_NO_ACTIVITY4
    "ADVICE", //STRID_ACTIVITIES_ADVISE

    //Thermostat options related strings
    "RECONFIGURE", //STRID_TSTAT_OPTIONS_CONFIGURE
    "DELETE", //STRID_TSTAT_OPTIONS_REMOVE
    "Delete in progress...", //STRID_TSTAT_OPTIONS_REMOVE_IN_PROGRESS
    "Reconfiguring...", //STRID_TSTAT_OPTIONS_RECONFIGURE_IN_PROGRESS
    "ADVICE", //STRID_TSTAT_OPTIONS_ADVISE
    "Reconfigure in case", //STRID_TSTAT_OPTIONS_ADVISE2
    "of failure.", //STRID_TSTAT_OPTIONS_ADVISE3
    "Delete to remove", //STRID_TSTAT_OPTIONS_ADVISE4
    "device from network", //STRID_TSTAT_OPTIONS_ADVISE5
    "Controler thermostat", //STRID_TSTAT_OPTIONS_CTRL2
    "cannot be removed", //STRID_TSTAT_OPTIONS_CTRL3
    "from network", //STRID_TSTAT_OPTIONS_CTRL4

    //Geofencing related strings
    "GEOFENCING", //STRID_GEOFENCING_1
    "", //STRID_GEOFENCING_2
    "Geofencing off", //STRID_GEOFENCING_OFF
    "Geofencing on", //STRID_GEOFENCING_ON
    "Clear mobile device list", //STRID_GEOFENCING_CLEAR

    //Geofencing clear related strings
    "CLEAR MOBILE", //STRID_GEOFENCINGCLEAR_1
    "DEVICE LIST", //STRID_GEOFENCINGCLEAR_2
    "CLEAR NOW", //STRID_GEOFENCINGCLEAR_CLEARNOW
    "ADVICE", //STRID_GEOFENCINGCLEAR_ADVISE
    "Clear the list if your", //STRID_GEOFENCINGCLEAR_ADVISE2
    "mobile device is lost or", //STRID_GEOFENCINGCLEAR_ADVISE3
    "is not used anymore", //STRID_GEOFENCINGCLEAR_ADVISE4

    //PIR setting related strings
    "PROXIMITY", //STRID_PIR_1,
    "SENSOR", //STRID_PIR_2,
    "Sensor off", //STRID_PIR_DISABLED,
    "Sensor on", //STRID_PIR_ENABLED,

    //Date/Time related strings
    "Date and Time", //STRID_DATETIME_1
    "", //STRID_DATETIME_2
    "January", //STRID_JANUARY
    "February", //STRID_FEBRUARY
    "March", //STRID_MARCH
    "April", //STRID_APRIL
    "May", //STRID_MAY
    "June", //STRID_JUNE
    "July", //STRID_JULY
    "August", //STRID_AUGUST
    "September", //STRID_SEPTEMBER
    "October", //STRID_OCTOBER
    "November", //STRID_NOVEMBER
    "December", //STRID_DECEMBER
    "Sunday", //STRID_SUNDAY
    "Monday", //STRID_MONDAY
    "Tuesday", //STRID_TUESDAY
    "Wednesday", //STRID_WEDNESDAY
    "Thursday", //STRID_THURSDAY
    "Friday", //STRID_FRIDAY
    "Saturday", //STRID_SATURDAY

    //Setpoint program related strings
    "WAKE", //STRID_WAKE_TITLE1
    "SETPOINT", //STRID_WAKE_TITLE2
    "LEAVE", //STRID_LEAVE_TITLE1
    "SETPOINT", //STRID_LEAVE_TITLE2
    "RETURN", //STRID_RETURN_TITLE1
    "SETPOINT", //STRID_RETURN_TITLE2
    "SLEEP", //STRID_SLEEP_TITLE1
    "SETPOINT", //STRID_SLEEP_TITLE2
    "Edit Schedule", //STRID_SCHEDULE

    //Schedule program related strings
    "Wake Schedule", //STRID_WAKESCHEDULE
    "Leave Schedule", //STRID_LEAVESCHEDULE
    "Return Schedule", //STRID_RETURNSCHEDULE
    "Sleep Schedule", //STRID_SLEEPSCHEDULE
    "Mon", //STRID_MON
    "Tue", //STRID_TUE
    "Wed", //STRID_WED
    "Thu", //STRID_THU
    "Fri", //STRID_FRI
    "Sat", //STRID_SAT
    "Sun", //STRID_SUN

    //Update related strings
    "MAESTRO", //STRID_MAESTRO
    "Your Maestro is updating", //STRID_UPDATE1
    "Please wait...", //STRID_UPDATE2
};

//! French string table
const unsigned char* FRENCH_STR_TABLE[] = {

    "", //STRID_NULL
    "--", //STRID_NOTEMP

    //Toolbox related strings
    ">", //STRID_NEXT
    "<", //STRID_PREVIOUS
    "TERMINER", //STRID_DONE
    "OK", //STRID_OK
    "APPLIQUER", //STRID_APPLY
    "OUI", //STRID_YES

    //Temperature related strings
    "--", //STRID_ERR
    "Froid", //STRID_LOW
    "Chaud", //STRID_HIGH

    //Context related strings
    "TEMP�RATURE AMBIANTE", //STRID_INDOOR
    "TEMP�RATURE DU PLANCHER", //STRID_FLOOR
    "CONDITIONS M�T�O", //STRID_OUTDOOR
    "CONSIGNE", //STRID_SETPOINT
    "CONSIGNE AMBIANTE", //STRID_SETPOINT_AMBIENT
    "CONSIGNE DE PLANCHER", //STRID_SETPOINT_FLOOR
    "CONSIGNE DE GROUPE", //STRID_SETPOINT_GROUP
    "CONSIGNE DE GROUPE (PLANCHER)", //STRID_SETPOINT_GROUP_FLOOR
    "CONSIGNE TEMPORAIRE", //STRID_SETPOINT_TEMPORARY
    "LIMITE DE PLANCHER", //STRID_SETPOINT_LIMIT
    "ARR�T", //STRID_SETPOINT_OFF
    "Vestibule en mode", //STRID_RECOVERY_HEATING_CYCLE
    "r�cup�ration", //STRID_RECOVERY_HEATING_CYCLE_2

    //Settings related strings
    "R�GLAGES", //STRID_SETTINGS
    "Date et heure", //STRID_SETTINGS_DATE_TIME
    "Mode et horaire", //STRID_SETTINGS_PROGRAMS
    "Options", //STRID_SETTINGS_OPTIONS
    "Liste de vos thermostats", //STRID_SETTINGS_TSTAT_LIST
    "Alertes", //STRID_SETTINGS_ALERTS
    "G�o-barri�re", //STRID_SETTINGS_GEOFENCING
    "R�initialiser par d�faut", //STRID_SETTINGS_RESET
    "Info �S", //STRID_SETTINGS_INFO_S

    //Groups related strings
    "GROUPES", //STRID_GROUPS
    "GROUPES", //STRID_GROUP_NAME_LEGEND,
    "CONSIGNE", //STRID_GROUP_SETPOINT_LEGEND
    "THERMOSTATS", //STRID_GROUP_TSTAT_LEGEND
    "TEMP�RATURE", //STRID_GROUP_TEMP_LEGEND
    "Aucun thermostat", //STRID_GROUP_NO_TSTAT
    "dans ce groupe", //STRID_GROUP_IN_GROUP

    //Mode related strings
    "MODE", //STRID_MODE
    "Non-Programmable", //STRID_MODE_MANUAL
    "Suivre l'horaire", //STRID_MODE_SCHEDULE
    "PASSER DIRECTEMENT AU :", //STRID_MODE_DEROGATION
    "R�veil", //STRID_MODE_WAKE
    "D�part", //STRID_MODE_LEAVE
    "Retour", //STRID_MODE_RETURN
    "Coucher", //STRID_MODE_SLEEP

    //Wi-Fi Setup related strings
    "CONFIG WI-FI", //STRID_WIFI_SETUP
    "Non connect�", //STRID_WIFI_SETUP_NOT_CONNECTED
    "R�seau actuel :", //STRID_WIFI_SETUP_CURRENT_NETWORK
    "Enregistrer l'appareil", //STRID_WIFI_SETUP_REGISTER_DEVICE
    "Recherche de r�seaux", //STRID_WIFI_SETUP_SCAN_NETWORKS

    //Wi-Fi networks related strings
    "R�SEAUX", //STRID_NETWORKS
    "Recherche en cours...", //STRID_NETWORKS_SCAN_IN_PROGRESS

    //Password related strings
    "MOT DE PASSE", //STRID_PASSWORD
    "R�seau choisi :", //STRID_PASSWORD_SELECTED_NETWORK
    "Mot de passe :", //STRID_PASSWORD_CURRENT_PASSWORD
    "CONNECTER",  //STRID_PASSWORD_CONNECT

    //ZigBee setup related strings
    "CONFIG ZIGBEE", //STRID_ZIGBEE_SETUP
    "Ajouter un thermostat", //STRID_ZIGBEE_SETUP_ADD_NEW_TSTAT
    "Quitter le r�seau", //STRID_ZIGBEE_SETUP_LEAVE_NETWORK
    "Ajouter avec un code", //STRID_ZIGBEE_SETUP_INSTALL_CODE
    "Rejoindre un r�seau", //STRID_ZIGBEE_SETUP_JOIN_EXISTING_NETWORK
    "ID R�seau : 0x", //STRID_ZIGBEE_SETUP_PANID
    "ID Contr�leur : 0x", //STRID_ZIGBEE_SETUP_NODEID
    "Canal :", //STRID_ZIGBEE_SETUP_CHANNEL

    //Options related strings
    "OPTIONS", //STRID_OPTIONS
    "Langue", //STRID_OPTIONS_LANGUAGE
    "Format de temp�rature", //STRID_OPTIONS_UNIT_FORMAT
    "Pr�f�rences de l'heure", //STRID_OPTIONS_TIME_FORMAT
    "Mode de chauffage", //STRID_OPTIONS_HEAT_MODE
    "Fen�tre ouverte", //STRID_OPTIONS_OPEN_WINDOW
    "Verrouillage d'�cran", //STRID_OPTIONS_LOCK
    "�cran de veille", //STRID_OPTIONS_BACKLIGHT
    "Capteur de proximit�", //STRID_OPTIONS_PIR
    "Mode de contr�le", //STRID_OPTIONS_CONTROL_MODE

    //Heat mode related strings
    "MODE DE", //STRID_HEAT_MODE_1
    "CHAUFFAGE", //STRID_HEAT_MODE_2
    "Plinthe", //STRID_HEAT_MODE_BASEBOARD
    "A�roconvecteur", //STRID_HEAT_MODE_FAN
    "Mode vestibule", //STRID_LOBBY_SUB_HEADER
    "Pi�ce standard", //STRID_STANDARD_ROOM
    "Vestibule", //STRID_LOBBY

    //Control mode related strings
    "MODE DE", //STRID_CONTROL_MODE_1
    "CONTR�LE", //STRID_CONTROL_MODE_2
    "Ambiant", //STRID_CONTROL_MODE_AMBIENT
    "Plancher", //STRID_CONTROL_MODE_FLOOR
    "Ambiant + plancher", //STRID_CONTROL_MODE_AMBIENTFLOOR
    "Type de plancher", //STRID_CONTROL_MODE_FLOOR_TYPE
    "Limite de plancher", //STRID_CONTROL_MODE_FLOOR_LIMIT

    //Floor type related strings
    "TYPE DE", //STRID_FLOOR_TYPE_1
    "PLANCHER", //STRID_FLOOR_TYPE_2
    "Plancher de tuile", //STRID_FLOOR_TYPE_TILE
    "Bois d'ing�nierie", //STRID_FLOOR_TYPE_ENGINEERED
    "La consigne maximum", //STRID_FLOOR_TYPE_ADVISE1
    "est limit�e � une", //STRID_FLOOR_TYPE_ADVISE2
    "valeur inf�rieure avec", //STRID_FLOOR_TYPE_ADVISE3
    "du bois d'ing�nierie", //STRID_FLOOR_TYPE_ADVISE4

    //Floor limit related strings
    "LIMITE DE", //STRID_FLOOR_LIMIT_TITLE1
    "PLANCHER", //STRID_FLOOR_LIMIT_TITLE2
    "Lorsque la temp�rature", //STRID_FLOOR_LIMIT_ADVISE1
    "du plancher atteint", //STRID_FLOOR_LIMIT_ADVISE2
    "cette limite, le", //STRID_FLOOR_LIMIT_ADVISE3
    "chauffage est limit�", //STRID_FLOOR_LIMIT_ADVISE4

    //Window Open Detection related strings
    "FEN�TRE", //STRID_OPEN_WINDOW1
    "OUVERTE", //STRID_OPEN_WINDOW2
    "Pas de d�tection", //STRID_OPEN_WINDOW_DISABLE
    "D�tection", //STRID_OPEN_WINDOW_ENABLE

    //Lock related strings
    "VERROUILLAGE", //STRID_LOCK_1
    "D'�CRAN", //STRID_LOCK_2
    "D�verrouiller", //STRID_LOCK_UNLOCK
    "Verrouiller", //STRID_LOCK_LOCK
    "CE THERMOSTAT", //STRID_LOCK_THIS_THERMOSTAT
    "TOUS LES THERMOSTATS", //STRID_LOCK_ALL_THERMOSTATS
    "INSTRUCTION",//STRID_LOCK_ADVISE
    "Pour d�verrouiller :", //STRID_LOCK_ADVISE1
    "Sur l'�cran principal,", //STRID_LOCK_ADVISE2
    "appuyer sur l'ic�ne      ", //STRID_LOCK_ADVISE3
    "pendant 3 secondes", //STRID_LOCK_ADVISE4

    //Backlight related strings
    "�CRAN DE", //STRID_BACKLIGHT1
    "VEILLE", //STRID_BACKLIGHT2
    "1 minute", //STRID_BACKLIGHT_60_SEC
    "5 minutes", //STRID_BACKLIGHT_300_SEC
    "10 minutes", //STRID_BACKLIGHT_600_SEC,
    "30 minutes", //STRID_BACKLIGHT_1800_SEC,

    //Language related strings
    "LANGUE", //STRID_LANGUAGE
    "Fran�ais", //STRID_LANGUAGE_FRANCAIS
    "English", //STRID_LANGUAGE_ENGLISH
    "LANGUE", //STRID_LANGUAGE_TITLE_FR
    "LANGUAGE", //STRID_LANGUAGE_TITLE_EN
    "APPLIQUER", //STRID_LANGUAGE_CONFIRM_FR
    "APPLY", //STRID_LANGUAGE_CONFIRM_EN

    //Unit format related strings
    "FORMAT DE", //STRID_TEMPERATURE
    "TEMP�RATURE", //STRID_FORMAT
    "�Celsius", //STRID_TEMP_FORMAT_CELSIUS
    "�Fahrenheit", //STRID_TEMP_FORMAT_FAHRENHEIT

    //Time format related strings
    "PR�F�RENCES", //STRID_TIME_FORMAT_1
    "DE L'HEURE", //STRID_TIME_FORMAT_2
    "24 heures", //STRID_TIME_FORMAT_24_HOURS
    "12 heures", //STRID_TIME_FORMAT_12_HOURS
    "FORMAT DE L'HEURE", //STRID_TIME_FORMAT
    "CHANGEMENT D'HEURE AUTO.", //STRID_TIME_FORMAT_DST
    "Activ�e", //STRID_TIME_FORMAT_DST_ON
    "D�sactiv�e", //STRID_TIME_FORMAT_DST_OFF

    //Reset related strings
    "R�INITIALISER", //STRID_RESET
    "R�initialiser par d�faut?", //STRID_RESET_RESET_TO_DEFAULT
    "�tes-vous s�r?", //STRID_RESET_CONFIRMATION_PROMPT
    "CONFIRMER", //STRID_RESET_CONFIRMATION
    "R�initialisation...", //STRID_RESET_IN_PROGRESS_1
    "Cela effacera vos", //STRID_RESET_ADVISE1
    "r�glages et l'appareil", //STRID_RESET_ADVISE2
    "retournera aux valeurs", //STRID_RESET_ADVISE3
    "programm�es en usine", //STRID_RESET_ADVISE4

    //Info related strings
    "�STELPRO", //STRID_INFO
    "Version du thermostat : ", //STRID_INFO_TSTAT_VERSION
    "Version ZigBee : ", //STRID_INFO_ZIGBEE_VERSION
    "Version Wi-Fi : ", //STRID_INFO_WIFI_VERSION
    "Produit le : ", //STRID_INFO_PRODUCED_ON
    "Code de contr�le : ", //STRID_INFO_CHECKSUM
    "Court : ", //STRID_INFO_CHECKSUM_SHORT
    "Long : ", //STRID_INFO_CHECKSUM_LONG

    //Add new thermostat related strings
    "AJOUTER UN", //STRID_ADD_NEW
    "THERMOSTAT", //STRID_THERMOSTAT
    "Mettre le thermostat", //STRID_ADD_NEW_TSTAT_PUT_NEW_TSTAT
    "en mode association", //STRID_ADD_NEW_TSTAT_IN_PAIRING_MODE
    "sur le canal : ", //STRID_ADD_NEW_TSTAT_ON_CHANNEL
    "Cr�ation du r�seau", //STRID_ADD_NEW_TSTAT_STARTING_NETWORK
    "Veuillez patienter...", //STRID_ADD_NEW_TSTAT_PLEASE_WAIT
    "Thermostat(s) ajout�(s) :", //STRID_ADD_NEW_TSTAT_NEW_TSTAT_ADDED
    "Autre appareil ajout� :", //STRID_ADD_NEW_TSTAT_OTHER_DEVICE_ADDED

    //Install code related strings
    "CODE D'AJOUT", //STRID_INSTALL_CODE
    "Entrez le code d'ajout", //STRID_INSTALL_CODE_ENTER_INSTALL_CODE
    "inscrit sur le thermostat.", //STRID_INSTALL_CODE_FOUND_ON_THERMOSTAT
    "Code d'ajout :", //STRID_INSTALL_CODE_CURRENT_CODE
    "AJOUTER AVEC", //STRID_INSTALL_CODE_TITLE1
    "UN CODE", //STRID_INSTALL_CODE_TITLE2
    "Oups, mauvais code d'ajout", //STRID_INSTALL_CODE_WRONG_CODE1
    "Ce code n'est pas valide", //STRID_INSTALL_CODE_WRONG_CODE2

    //MAC Address related strings
    "ADRESSE MAC", //STRID_MAC_ADDRESS
    "Entrez l'adresse MAC", //STRID_MAC_ADDRESS_ENTER_MAC_ADRESS
    "inscrit sur le thermostat", //STRID_MAC_ADDRESS_FOUND_ON_THERMOSTAT
    "Adresse MAC :", //STRID_MAC_ADDRESS_CURRENT_MAC_ADDRESS

    //Join ZigBee network related strings
    "ASSOCIATION", //STRID_JOIN_NETWORK
    "Automatique", //STRID_JOIN_NETWORK_AUTO
    "Par canal", //STRID_JOIN_NETWORK_SPECIFIC
    "SP�CIFIER LE CANAL", //STRID_JOIN_NETWORK_SPECIFY_CHANNEL
    "REJOINDRE", //STRID_JOIN_NETWORK_JOIN
    "CONNEXION EN COURS...", //STRID_JOIN_NETWORK_JOINING
    "SUCC�S", //STRID_JOIN_NETWORK_SUCCESS
    "�CHEC", //STRID_JOIN_NETWORK_FAILED

    //Leave ZigBee network related strings
    "QUITTER", //STRID_LEAVE_NETWORK1
    "LE R�SEAU", //STRID_LEAVE_NETWORK2
    "�tes-vous s�r?", //STRID_LEAVE_NETWORK_CONFIRMATION_PROMPT
    "Quitter maintenant", //STRID_LEAVE_NETWORK_CONFIRMATION
    "D�connexion en cours...", //STRID_LEAVE_NETWORK_IN_PROGRESS1
    "Veuillez patienter", //STRID_LEAVE_NETWORK_IN_PROGRESS2
    "Thermostat d�connect�", //STRID_LEAVE_NETWORK_LEFT

    //Alert related strings
    "ALERTES", //STRID_ALERTS
    "Bonne nouvelle!", //STRID_ALERTS_GOOD_NEWS
    "Aucune alerte active", //STRID_ALERTS_NO_ALERT
    "Thermostat d�connect�", //STRID_ALERTS_NO_ZIGBEE
    "Fen�tre ouverte", //STRID_ALERTS_OPEN_WINDOW
    "Appareil en surchauffe", //STRID_ALERTS_OVERHEAT
    "Fuite � la terre", //STRID_ALERTS_GFCI
    "Pas de connexion Wi-Fi", //STRID_ALERTS_NO_WIFI
    "Mise-�-jour en attente", //STRID_ALERTS_OTA_PENDING
    "Radio ZigBee en panne", //STRID_ALERTS_ZIGBEE_ERROR
    "Pas de r�seau ZigBee", //STRID_ALERTS_NO_ZIGBEE_NETWORK
    "Nuage non disponible", //STRID_ALERTS_SERVER_PROBLEM
    "Fin de vie", //STRID_ALERTS_END_OF_LIFE
    "Limite de plancher", //STRID_ALERTS_FLOOR_TEMP_LIMIT_REACHED
    "Relais tr�s us�s", //STRID_ALERTS_RELAYS_WEARING_OUT
    "Sonde plancher absente", //STRID_ALERTS_NO_FLOOR_SENSOR
    "Sonde plancher absente", //STRID_ALERTS_NO_FLOOR_SENSOR_F
    "Heure non r�gl�e", //STRID_ALERTS_TIME_NOT_SET
    "Alerte inconnue", //STRID_ALERTS_UNDEFINED

    //Alert details related strings
    "ALERTE/D�TAIL", //STRID_ALERT_DETAILS
    "ALERTE:", //STRID_ALERT_DETAILS_ALERT
    "THERMOSTAT:", //STRID_ALERT_DETAILS_THERMOSTAT
    "CONSEIL", //STRID_ALERT_DETAILS_ADVISE
    "Thermostat d�connect�", //STRID_ALERT_DETAILS_NO_ZIGBEE_1
    "Assurez-vous que", //STRID_ALERT_DETAILS_NO_ZIGBEE_2
    "le thermostat est", //STRID_ALERT_DETAILS_NO_ZIGBEE_3
    "aliment� et connect�", //STRID_ALERT_DETAILS_NO_ZIGBEE_4
    "Fen�tre ou porte ouverte", //STRID_ALERT_DETAILS_OPEN_WINDOW_1
    "La consigne a �t�", //STRID_ALERT_DETAILS_OPEN_WINDOW_2
    "abaiss�e pour", //STRID_ALERT_DETAILS_OPEN_WINDOW_3
    "�conomiser l'�nergie", //STRID_ALERT_DETAILS_OPEN_WINDOW_4
    "Appareil en surchauffe", //STRID_ALERT_DETAILS_OVERHEAT_1
    "Assurez-vous que", //STRID_ALERT_DETAILS_OVERHEAT_2
    "l'appareil de chauffage", //STRID_ALERT_DETAILS_OVERHEAT_3
    "n'est pas obstru�", //STRID_ALERT_DETAILS_OVERHEAT_4
    "Fuite � la terre", //STRID_ALERT_DETAILS_GFCI_1
    "Le circuit de protection", //STRID_ALERT_DETAILS_GFCI_2
    "du plancher doit �tre", //STRID_ALERT_DETAILS_GFCI_3
    "r�initialis� afin de", //STRID_ALERT_DETAILS_GFCI_4
    "r�tablir le chauffage", //STRID_ALERT_DETAILS_GFCI_5
    "Perte de connexion Wi-Fi", //STRID_ALERT_DETAILS_NO_WIFI_1
    "Assurez-vous d'avoir", //STRID_ALERT_DETAILS_NO_WIFI_2
    "une connexion Internet,", //STRID_ALERT_DETAILS_NO_WIFI_3
    "v�rifiez votre router", //STRID_ALERT_DETAILS_NO_WIFI_4
    "Mise-�-jour en attente", //STRID_ALERT_DETAILS_OTA_PENDING_1
    "Elle sera appliqu�e dans", //STRID_ALERT_DETAILS_OTA_PENDING_2
    " heures et ", //STRID_ALERT_DETAILS_OTA_PENDING_3A
    " minutes", //STRID_ALERT_DETAILS_OTA_PENDING_3B
    "ou appliqu�e maintenant", //STRID_ALERT_DETAILS_OTA_PENDING_4
    "Radio ZigBee en panne", //STRID_ALERT_DETAILS_ZIGBEE_ERROR_1
    "ERREUR CRITIQUE", //STRID_ALERT_DETAILS_ZIGBEE_ERROR_2
    "Toute communication", //STRID_ALERT_DETAILS_ZIGBEE_ERROR_3
    "ZigBee est impossible", //STRID_ALERT_DETAILS_ZIGBEE_ERROR_4
    "Pas de r�seau ZigBee", //STRID_ALERT_DETAILS_NO_ZIGBEE_NETWORK_1
    "Impossible de cr�er un", //STRID_ALERT_DETAILS_NO_ZIGBEE_NETWORK_2
    "r�seau ZigBee. Veuillez", //STRID_ALERT_DETAILS_NO_ZIGBEE_NETWORK_3
    "R�initialiser par d�faut", //STRID_ALERT_DETAILS_NO_ZIGBEE_NETWORK_4
    "Nuage non disponible", //STRID_ALERT_DETAILS_SERVER_PROBLEM_1
    "V�rifiez votre Internet", //STRID_ALERT_DETAILS_SERVER_PROBLEM_2
    "Si le probl�me persiste", //STRID_ALERT_DETAILS_SERVER_PROBLEM_3
    "contactez Stelpro", //STRID_ALERT_DETAILS_SERVER_PROBLEM_4
    "Fin de vie", //STRID_ALERT_DETAILS_EOL_1
    "Circuit de protection de", //STRID_ALERT_DETAILS_EOL_2
    "plancher d�fectueux", //STRID_ALERT_DETAILS_EOL_3
    "Chauffage d�sactiv�", //STRID_ALERT_DETAILS_EOL_4
    "Remplacez le thermostat", //STRID_ALERT_DETAILS_EOL_5
    "Limite de plancher", //STRID_ALERT_DETAILS_FLOOR_TEMP_LIMIT_1
    "Temp�rature limite du", //STRID_ALERT_DETAILS_FLOOR_TEMP_LIMIT_2
    "plancher atteinte.", //STRID_ALERT_DETAILS_FLOOR_TEMP_LIMIT_3
    "Chauffage limit� afin", //STRID_ALERT_DETAILS_FLOOR_TEMP_LIMIT_4
    "de prot�ger le plancher", //STRID_ALERT_DETAILS_FLOOR_TEMP_LIMIT_5
    "Relais tr�s us�s", //STRID_ALERT_DETAILS_RELAYS_WEARING_OUT_1
    "Dur�e de vie th�orique", //STRID_ALERT_DETAILS_RELAYS_WEARING_OUT_2
    "des relais atteinte", //STRID_ALERT_DETAILS_RELAYS_WEARING_OUT_3
    "Planifier remplacement", //STRID_ALERT_DETAILS_RELAYS_WEARING_OUT_4
    "du thermostat bient�t", //STRID_ALERT_DETAILS_RELAYS_WEARING_OUT_5
    "Sonde plancher absente", //STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_1
    "Pas de \"sonde plancher\"", //STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_2
    "Le plancher n'est plus", //STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_3
    "prot�g� en temp�rature", //STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_4
    "V�rifiez la connexion", //STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_5
    "Sonde plancher absente", //STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_F_1
    "Pas de \"sonde plancher\"", //STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_F_2
    "Aucun chauffage permis", //STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_F_3
    "V�rifiez la connexion", //STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_F_4
    "de la sonde de plancher", //STRID_ALERT_DETAILS_NO_FLOOR_SENSOR_F_5
    "Heure non r�gl�e", //STRID_ALERT_DETAILS_TIME_NOT_SET_1
    "Veuillez r�gler l'heure", //STRID_ALERT_DETAILS_TIME_NOT_SET_2
    "pour utiliser l'horaire.", //STRID_ALERT_DETAILS_TIME_NOT_SET_3
    "Appuyez sur le bouton", //STRID_ALERT_DETAILS_TIME_NOT_SET_4
    "ci-dessus pour continuer", //STRID_ALERT_DETAILS_TIME_NOT_SET_5
    "R�gler la date et l'heure", //STRID_ALERT_DETAILS_TIME_NOT_SET_ACTION
    "Alerte inconnue", //STRID_ALERT_DETAILS_UNDEFINED_1
    "Une alerte inconnue", //STRID_ALERT_DETAILS_UNDEFINED_2
    "est d�tect�e, contactez", //STRID_ALERT_DETAILS_UNDEFINED_3
    "le support de Stelpro", //STRID_ALERT_DETAILS_UNDEFINED_4
    "pour un diagnostic", //STRID_ALERT_DETAILS_UNDEFINED_5

    //Wi-Fi join related strings
    "CONNEXION", //STRID_WIFI_JOIN1
    "WI-FI", //STRID_WIFI_JOIN2
    "Connexion Wi-Fi en cours...", //STRID_WIFI_JOIN_IN_PROGRESS
    "Connexion Wi-Fi compl�t�e", //STRID_WIFI_JOIN_SUCCESS
    "Oups : Mauvais mot de passe", //STRID_WIFI_JOIN_BAD_PASSWORD
    "Oups : R�seau introuvable", //STRID_WIFI_JOIN_NO_NETWORK
    "Oups : Connexion �chou�e", //STRID_WIFI_JOIN_ERROR
    "Oups : Serveur en panne", //STRID_WIFI_JOIN_SERVER_ERROR

    //Registration related strings
    "ENREGISTREMENT", //STRID_REGISTRATION
    "Enregistrement compl�t�!", //STRID_REGISTRATION_COMPLETED
    "Code d'enregistrement :", //STRID_REGISTRATION_CODE
    "Enregistrement en cours...", //STRID_REGISTRATION_IN_PROGRESS
    "Cela prendra quelque temps", //STRID_REGISTRATION_PLEASE_WAIT1
    "Veuillez patienter...", //STRID_REGISTRATION_PLEASE_WAIT2
    "CONSEIL", //STRID_REGISTER_ADVISE
    "T�l�chargez l'application", //STRID_REGISTER_ADVISE2
    "Maestro et entrez le", //STRID_REGISTER_ADVISE3
    "code pour lier votre", //STRID_REGISTER_ADVISE4
    "domicile � votre compte", //STRID_REGISTER_ADVISE5

    //Activities related strings
    "ACTIVIT�S", //STRID_ACTIVITIES
    "Aucune activit� d�finie", //STRID_ACTIVITIES_NO_ACTIVITY1
    "Utilisez l'application", //STRID_ACTIVITIES_NO_ACTIVITY2
    "mobile de Stelpro afin", //STRID_ACTIVITIES_NO_ACTIVITY3
    "de cr�er les v�tres", //STRID_ACTIVITIES_NO_ACTIVITY4
    "CONSEIL", //STRID_ACTIVITIES_ADVISE

    //Thermostat details related strings
    "RECONFIGURER", //STRID_TSTAT_OPTIONS_CONFIGURE
    "SUPPRIMER", //STRID_TSTAT_OPTIONS_REMOVE
    "Suppression en cours...", //STRID_TSTAT_OPTIONS_REMOVE_IN_PROGRESS
    "Reconfiguration...", //STRID_TSTAT_OPTIONS_RECONFIGURE_IN_PROGRESS
    "CONSEIL", //STRID_TSTAT_OPTIONS_ADVISE
    "Reconfigurez en cas", //STRID_TSTAT_OPTIONS_ADVISE2
    "de panne.", //STRID_TSTAT_OPTIONS_ADVISE3
    "Supprimer afin de retirer", //STRID_TSTAT_OPTIONS_ADVISE4
    "le thermostat du r�seau", //STRID_TSTAT_OPTIONS_ADVISE5
    "Le thermostat contr�leur", //STRID_TSTAT_OPTIONS_CTRL2
    "ne peut �tre retir�", //STRID_TSTAT_OPTIONS_CTRL3
    "du r�seau", //STRID_TSTAT_OPTIONS_CTRL4

    //Geofencing related strings
    "G�O-BARRI�RE", //STRID_GEOFENCING_1
    "", //STRID_GEOFENCING_2
    "Barri�re d�sactiv�e", //STRID_GEOFENCING_OFF
    "Barri�re activ�e", //STRID_GEOFENCING_ON
    "Supprimer les appareils", //STRID_GEOFENCING_CLEAR

    //Geofencing clear related strings
    "SUPPRIMER LES", //STRID_GEOFENCINGCLEAR_1
    "APPAREILS MOBILES", //STRID_GEOFENCINGCLEAR_2
    "SUPPRIMER", //STRID_GEOFENCINGCLEAR_CLEARNOW
    "CONSEIL", //STRID_GEOFENCINGCLEAR_ADVISE
    "Supprimer la liste si", //STRID_GEOFENCINGCLEAR_ADVISE2
    "un appareil est perdu ou", //STRID_GEOFENCINGCLEAR_ADVISE3
    "est devenu inactif", //STRID_GEOFENCINGCLEAR_ADVISE4

    //PIR setting related strings
    "CAPTEUR DE", //STRID_PIR_1,
    "PROXIMIT�", //STRID_PIR_2,
    "Capteur d�sactiv�", //STRID_PIR_DISABLED,
    "Capteur activ�", //STRID_PIR_ENABLED,

    //Date/Time related strings
    "Date et heure", //STRID_DATETIME_1
    "", //STRID_DATETIME_2
    "Janvier", //STRID_JANUARY
    "F�vrier", //STRID_FEBRUARY
    "Mars", //STRID_MARCH
    "Avril", //STRID_APRIL
    "Mai", //STRID_MAY
    "Juin", //STRID_JUNE
    "Juillet", //STRID_JULY
    "Ao�t", //STRID_AUGUST
    "Septembre", //STRID_SEPTEMBER
    "Octobre", //STRID_OCTOBER
    "Novembre", //STRID_NOVEMBER
    "D�cembre", //STRID_DECEMBER
    "Dimanche", //STRID_SUNDAY
    "Lundi", //STRID_MONDAY
    "Mardi", //STRID_TUESDAY
    "Mercredi", //STRID_WEDNESDAY
    "Jeudi", //STRID_THURSDAY
    "Vendredi", //STRID_FRIDAY
    "Samedi", //STRID_SATURDAY

    //Setpoint program related strings
    "CONSIGNE", //STRID_WAKE_TITLE1
    "R�VEIL", //STRID_WAKE_TITLE2
    "CONSIGNE", //STRID_LEAVE_TITLE1
    "D�PART", //STRID_LEAVE_TITLE2
    "CONSIGNE", //STRID_RETURN_TITLE1
    "RETOUR", //STRID_RETURN_TITLE2
    "CONSIGNE", //STRID_SLEEP_TITLE1
    "COUCHER", //STRID_SLEEP_TITLE2
    "�dition de l'horaire", //STRID_SCHEDULE

    //Schedule program related strings
    "Horaire r�veil", //STRID_WAKESCHEDULE
    "Horaire d�part", //STRID_LEAVESCHEDULE
    "Horaire retour", //STRID_RETURNSCHEDULE
    "Horaire coucher", //STRID_SLEEPSCHEDULE
    "Lun", //STRID_MON
    "Mar", //STRID_TUE
    "Mer", //STRID_WED
    "Jeu", //STRID_THU
    "Ven", //STRID_FRI
    "Sam", //STRID_SAT
    "Dim", //STRID_SUN

    //Update related strings
    "MAESTRO", //STRID_MAESTRO
    "Maestro se met � jour", //STRID_UPDATE1
    "Veuillez patienter...", //STRID_UPDATE2
};

/*******************************************************************************
	External functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
	Private functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
	Private functions definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
	Public functions definitions
*******************************************************************************/

    //Empty
