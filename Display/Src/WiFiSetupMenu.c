/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       WiFiSetupMenu.c

    \brief      Source code file for the WiFiSetupMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the WiFiSetupMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "WiFiSetupMenu.h"

#include ".\WiFi\inc\THREAD_WiFi.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/


/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;


/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   WiFiSetupEditState = EDIT_IDLE;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
void UpdateCurrentNetwork(void);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
void UpdateCurrentNetwork(void)
{
    if (MNH_GetActiveMenu() == MENU_WIFI_SETUP)
    {
        DPH_DisplayWiFiOptions();
    }
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/
/*******************************************************************************
    MNU_WiFiSetup_Entry
*******************************************************************************/
void MNU_WiFiSetup_Entry(void)
{
    WiFiSetupEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show Wi-Fi options
    //
    DPH_DisplayWiFiOptions();

	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_WiFiSetup_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    WIFITHRD_GenerateNewRegistrationToken();

}

/*******************************************************************************
    MNU_WiFiSetup_Core

    Allow to perform the Wi-Fi Setup
*******************************************************************************/
void MNU_WiFiSetup_Core(void)
{
    dbType_WiFiProfile_t activeProfile;

    //OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(5000));
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_WiFiSetup_Commit, (void*)KEY_BACK);
        KBH_SetKeyPressProperties(KEY_OPTIONS_4, (FUNC)MNU_WiFiSetup_Commit, (void*)KEY_OPTIONS_4);

        DBTHRD_GetData(DBTYPE_ACTIVEWIFIPROFILE, (void*)&activeProfile, INDEX_DONT_CARE);
        if (activeProfile.profileId != 0xff)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_5, (FUNC)MNU_WiFiSetup_Commit, (void*)KEY_OPTIONS_5);
        }

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_WiFiSetup_Exit
*******************************************************************************/
void MNU_WiFiSetup_Exit(void)
{
    WiFiSetupEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_WiFiSetup_Commit
*******************************************************************************/
void MNU_WiFiSetup_Commit(KEYB_PARAM keyId)
{
	if (WiFiSetupEditState == EDIT_IN_PROGRESS)
	{
        KBH_ClearAllKeyProperties();
        switch (keyId)
        {
            case KEY_BACK:
                MNH_SetActiveMenu(MENU_ADV_SETTINGS);
                break;

            case KEY_OPTIONS_4:
                MNH_SetActiveMenu(MENU_SCAN);
                break;

            case KEY_OPTIONS_5:
                MNH_SetActiveMenu(MENU_REGISTER);
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
		WiFiSetupEditState = EDIT_IDLE;
	}
}
