/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2019, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       FloorTypeMenu.c

    \brief      Source code file for the FloorTypeMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the FloorTypeMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "FloorTypeMenu.h"
#include ".\DB\inc\THREAD_DB.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e           FloorTypeEditState = EDIT_IDLE;
static FloorType_t              FloorTypeEditValue;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void ChangeFloorType(KEYB_PARAM keyId);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    ChangeFloorType

    Change the floor type to either tile or engineered floor
*******************************************************************************/
static void ChangeFloorType(KEYB_PARAM keyId)
{
    uint8_t onboardingDone;

    switch (keyId)
    {
        case KEY_OPTIONS_2:
            FloorTypeEditValue = FLOORTYPE_TILE;
            break;

        case KEY_OPTIONS_3:
            FloorTypeEditValue = FLOORTYPE_ENGINEERED;
            break;

        default:
            break;
    }

    DPH_DisplayFloorType(FloorTypeEditValue, FALSE);

    DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);
    if (onboardingDone == 1)
    {
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_FloorType_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    }
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_FloorType_Entry
*******************************************************************************/
void MNU_FloorType_Entry(void)
{
    uint8_t onboardingDone;

    DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);
    DBTHRD_GetData(DBTYPE_FLOOR_TYPE,(void*)&FloorTypeEditValue, INDEX_DONT_CARE);
    FloorTypeEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show floor type
    //
    DPH_DisplayFloorType(FloorTypeEditValue, TRUE);

    if (onboardingDone == 1)
    {
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_FloorType_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    }
}

/*******************************************************************************
    MNU_FloorType_Core

    Allows to change the floor type
*******************************************************************************/
void MNU_FloorType_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_FloorType_Commit, (void*)KEY_BACK);
        KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)ChangeFloorType, (void*)KEY_OPTIONS_2);
        KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)ChangeFloorType, (void*)KEY_OPTIONS_3);
        KBH_SetKeyPressProperties(KEY_DONE, (FUNC)MNU_FloorType_Commit, (void*)KEY_DONE);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_FloorType_Exit
*******************************************************************************/
void MNU_FloorType_Exit(void)
{
    FloorTypeEditState = EDIT_IDLE;
    OS_StartTask(TSK_LIMIT_CHECK);
}

/*******************************************************************************
    MNU_FloorType_Init
*******************************************************************************/
void MNU_FloorType_Init(void)
{
    MNU_FloorType_SetFloorType(FLOORTYPE_TILE);
}

/*******************************************************************************
    MNU_FloorType_Commit
*******************************************************************************/
void MNU_FloorType_Commit(KEYB_PARAM keyId)
{
    static KEYB_PARAM commitKey = NULL;

    if ( FloorTypeEditState == EDIT_IN_PROGRESS )
    {
        commitKey = keyId;
        switch (commitKey)
        {
            case KEY_DONE:
                MNU_FloorType_SetFloorType(FloorTypeEditValue);
                //No break on purpose

            case KEY_BACK:
            default:
                MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_FloorType_Commit, (KEYB_PARAM*)commitKey, IMMEDIATELY);
                break;
        }
        FloorTypeEditState = EDIT_COMMITTING;
		KBH_ClearAllKeyProperties();
    }
    else if ( FloorTypeEditState == EDIT_COMMITTING )
    {
        FloorTypeEditState = EDIT_IDLE;

        switch (commitKey)
        {
            case KEY_BACK:
            case KEY_DONE:
                MNH_SetActiveMenu(MENU_CONTROL_MODE);
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
    }
}


/*******************************************************************************
    MNU_FloorType_SetFloorType

    Set the floor type to Tile or Engineered floor
*******************************************************************************/
void MNU_FloorType_SetFloorType(FloorType_t type)
{
    DBTHRD_SetData(DBTYPE_FLOOR_TYPE,(void*)&type, THIS_THERMOSTAT, DBCHANGE_LOCAL);
}
