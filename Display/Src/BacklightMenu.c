/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       BacklightMenu.c

    \brief      Source code file for the BacklightMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the BacklightMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "BacklightMenu.h"
#include ".\DB\inc\THREAD_DB.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e           BacklightEditState = EDIT_IDLE;
static BacklightTimeout_t       BacklightEditValue;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void ChangeBacklight(KEYB_PARAM keyId);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    ChangeBacklight

    Change the backlight operation mode to either Alway On or Automatic
*******************************************************************************/
static void ChangeBacklight(KEYB_PARAM keyId)
{
    switch (keyId)
    {
        case KEY_OPTIONS_2:
            BacklightEditValue = BL_60_SEC;
            break;

        case KEY_OPTIONS_3:
            BacklightEditValue = BL_300_SEC;
            break;

        case KEY_OPTIONS_4:
            BacklightEditValue = BL_600_SEC;
            break;

        case KEY_OPTIONS_5:
            BacklightEditValue = BL_1800_SEC;
            break;

        default:
            break;
    }

    DPH_DisplayBacklight(BacklightEditValue, FALSE);
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Backlight_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_Backlight_Entry
*******************************************************************************/
void MNU_Backlight_Entry(void)
{
    DBTHRD_GetData(DBTYPE_BACKLIGHT_SETTING,(void*)&BacklightEditValue, INDEX_DONT_CARE);
    BacklightEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show backlight options
    //
    DPH_DisplayBacklight(BacklightEditValue, TRUE);

    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Backlight_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_Backlight_Core

    Allows to change the backlight operation
*******************************************************************************/
void MNU_Backlight_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_Backlight_Commit, (void*)KEY_BACK);
        KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)ChangeBacklight, (void*)KEY_OPTIONS_2);
        KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)ChangeBacklight, (void*)KEY_OPTIONS_3);
        KBH_SetKeyPressProperties(KEY_OPTIONS_4, (FUNC)ChangeBacklight, (void*)KEY_OPTIONS_4);
        KBH_SetKeyPressProperties(KEY_OPTIONS_5, (FUNC)ChangeBacklight, (void*)KEY_OPTIONS_5);
        KBH_SetKeyPressProperties(KEY_DONE, (FUNC)MNU_Backlight_Commit, (void*)KEY_DONE);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_Backlight_Exit
*******************************************************************************/
void MNU_Backlight_Exit(void)
{
    BacklightEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_Backlight_Commit
*******************************************************************************/
void MNU_Backlight_Commit(KEYB_PARAM keyId)
{
    static KEYB_PARAM commitKey = NULL;

    if ( BacklightEditState == EDIT_IN_PROGRESS )
    {
        commitKey = keyId;
        KBH_ClearAllKeyProperties();
        switch (commitKey)
        {
            case KEY_DONE:
                MNU_Backlight_SetBacklight(BacklightEditValue);
                //No break on purpose

            case KEY_BACK:
            default:
                MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Backlight_Commit, (KEYB_PARAM*)commitKey, IMMEDIATELY);
                break;
        }
        BacklightEditState = EDIT_COMMITTING;
    }
    else if ( BacklightEditState == EDIT_COMMITTING )
    {
        BacklightEditState = EDIT_IDLE;
        switch (commitKey)
        {
            case KEY_BACK:
            case KEY_DONE:
                MNH_SetActiveMenu(MENU_OPTIONS);
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
    }
}

/*******************************************************************************
    MNU_Backlight_SetBacklight

    Set the backlight operation mode to Always on or Auto
*******************************************************************************/
void MNU_Backlight_SetBacklight(BacklightTimeout_t bl_mode)
{
    DBTHRD_SetData(DBTYPE_BACKLIGHT_SETTING,(void*)&bl_mode, INDEX_DONT_CARE, DBCHANGE_LOCAL);
}
