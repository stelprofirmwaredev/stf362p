/********************************************************************************************
*    tools.c
*
*                                    Module Outils
*
*    Regroupe des fonctions g�n�rales pratiques.
*
*    \author     Jean-Fran�ois Many
*
********************************************************************************************/

/********************************************************************************************
* Inclusions
********************************************************************************************/
#include "typedef.h"
#include "tools.h"


/********************************************************************************************
*    D�finition des variables globales publiques
********************************************************************************************/

/*** Variables globales sans initialisation ************************************************/

/*** Variables globale initialis�es lors d�claration ***************************************/

/*** Variables globale initialis�es � z�ro *************************************************/


/********************************************************************************************
* D�finition des constantes priv�es
********************************************************************************************/


/********************************************************************************************
* D�claration des variables globales priv�es
********************************************************************************************/

/*** Variables globale initialis�es � z�ro *************************************************/


/********************************************************************************************
* D�claration des fonctions priv�es
********************************************************************************************/


/********************************************************************************************
* D�finition des fonctions priv�es
********************************************************************************************/


/********************************************************************************************
* D�finition des fonctions publiques
********************************************************************************************/

/********************************************************************************************
*  Arrondis
*
*    Arrondis une temp�rature selon le param�tre fournis.
*
*    Param�tres :
*        temp_to_round : Temp�rature � arrondir (�C ou �F).
*        rounding : Valeur � laquelle l'arrondissement doit �tre fait.
*
*        Restrictions : Param�tre 'rounding' doit supp�rieur � 1
*                     et inf�rieur � la valeur absolue de 'temp_to_round'.
*
*    Retourne :
*        Temp�rature arrondie.
*
********************************************************************************************/
int16s TLS_Round(int16s temp_to_round, int16s rounding)
{
    int16s temp_result;
    int8u odd_rounding = 0;
                                                                //    Si doit arrondir ET temp�rature valide
    if (rounding && (temp_to_round >= -19999) && (temp_to_round <= 19999)) {
        temp_result = temp_to_round % rounding;                 //        Calcul le reste de la division
        if (temp_result < 0) {                                  //        Valeur absolue du reste
            temp_result = -temp_result;
        }
        if (rounding & 0x01)
        {
            odd_rounding = 1;
        }
        if (temp_result >= ((rounding >> 1) + odd_rounding)) {  //        Si reste > Valeur Arrondissment / 2
            temp_result -= rounding;                            //            reste = reste - Valeur Arrondissment
        }
        if (temp_to_round >= 0) {                               //        si temp�rature positive
            temp_to_round -= temp_result;                       //            temp�rature - reste
        } else {                                                //        sinon (n�gative)
            temp_to_round += temp_result;                       //            temp�rature + reste
        }
    }

    return temp_to_round;
}


/********************************************************************************************
*  Hyst�r�sis
*
*    Applique un hyst�r�sis sur une variable (temp�rature).
*
*                        - - - - - - - - - - - - - -  <-- Seuil pour latcher nouvelle valeur
*                                                   } Hyst�r�sis
*    Valeur latch�e -->  ---------------------------
*                                                   } Hyst�r�sis
*                        - - - - - - - - - - - - - -  <-- Seuil pour latcher nouvelle valeur
*
*    Param�tres :
*        new_value : Valeur � v�rifier si ext�rieure de l'hyst�r�sis.
*        *old_value : Pointeur sur valeur m�diane de l'hyst�r�sis.
*
*    Retourne : NULL
*
********************************************************************************************/
void TLS_Hysteresis(int16s new_value, int16s * old_value)
{
                                                    //    Si temp�ratures (latch et actuelle) valides
    if ((*old_value >= -19999) && (new_value >= -19999) && (new_value <= 19999) && (*old_value <= 19999)) {
                                                    //        Si temp�rature >= Tlatch + hyst�r�sis
        if ((new_value >= (*old_value + TLS_HYSTERESIS)) || (new_value <= (*old_value - TLS_HYSTERESIS))) {
            *old_value = new_value;                    //            Valeur latch�e = valeur actuelle
        }
    } else {                                        //    Sinon
        *old_value = new_value;                        //        Valeur latch�e = valeur actuelle
    }
}


/********************************************************************************************
*  Bit faible actif
*
*    Retourne la position du premier bit trouv� de b0 a b7
*
*    Param�tres :
*        data_in : temp�rature � convertir en BCD. (0 @ 9999)
*
*    Retourne :
*        0 - 7 position du bit
*        0 = bit0 set ou tous les bit a 0
*
********************************************************************************************/
int8u TLS_GetLSBitSet(int8u byte)
{
    int8u  i;

    for (i = 0; i < 8; i++) {
        if (byte & 0x01) {
            return (i);
        }
        byte = byte >> 1;
    }
    return(0);
}


/********************************************************************************************
*  Inversion bits
*
*    Inverse la postion des bits dans un byte.
*     msb         lsb       msb         lsb
*     A B C D E F G H  -->  H G F E D C B A
*     | | | | | | | |       ^ ^ ^ ^ ^ ^ ^ ^
*     | | | | | | | +-------+ | | | | | | |
*     | | | | | | +-----------+ | | | | | |
*     | | | | | +---------------+ | | | | |
*     | | | | +-------------------+ | | | |
*     | | | +-----------------------+ | | |
*     | | +---------------------------+ | |
*     | +-------------------------------+ |
*     +-----------------------------------+
*
*    Param�tres :
*            data_in : byte � inverser
*
*    Retourne :
*            byte invers�
*
********************************************************************************************/
int8u TLS_BitFlip(int8u data_in)
{
    int8u data_flip = 0;
    int8u i;

    for (i = 0; i < 8; i++) {
        data_flip <<= 1;
        if (data_in & 0x01) {
            data_flip |= 0x01;
        }
        data_in >>= 1;
    }
    return data_flip;
}


/********************************************************************************************
*  Calcul checksum
*
*    Calcul le checksum des param�tres en RAM.
*        Checksum = Compl�ment deux de la somme de tous les bytes avec 0xF0.
*                = ~(0xF0 + Byte 0 + Byte n) + 1
*
*    Param�tres :
*            *addr : Adresse de d�part pour la calcul.
*            length : Nombre de bytes utilis�s pour le calcul
*
*    Retourne :
*        Checksum calcul�.
*
********************************************************************************************/
int8u TLS_CalculChecksumRam(const void * addr, int8u length)
{
    int8u temp_check = 0xF0;
    const int8u * address = (const int8u *)addr;
    const int8u * const end_address = address + length;

                                                    //    checksum = 0xF0 + Byte(1) + Byte(n)...
    for (; address < end_address; address++) {
        temp_check += *address;
    }
    temp_check = (~temp_check) + 1;                    //    compl�ment 2

    return temp_check;
}

#ifdef MINUNITTEST
#include  "STP_Tools.c"
#endif // MINUNITTEST
