/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2017, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       TstatDetailsMenu.c

    \brief      Source code file for the TstatDetailsMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the TstatDetailsMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "convert.h"
#include "tools.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "GroupDetailsMenu.h"
#include "TstatDetailsMenu.h"
#include "APP_SetpointManager.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\HAL\inc\Display.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/


/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_CONFIRM,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e           TstatDetailsEditState = EDIT_IDLE;
static TEMPERATURE_C_t          TstatDetailsEditValue;
static TEMPERATURE_C_t          TstatTemperature;
static HEAT_DEMAND_PERCENT_t    TstatHeatDemand;
static RelativeHumidity_t       TstatHumidity;
static uint8_t                  SelectedTstat;
static uint8_t                  TstatFloorMode;
static uint8_t                  localTstat;
#ifdef FLOOR
    static uint8_t minReached;
#endif
static uint8_t                  offMode;
static FloorType_t              floorType;
static susbscriptionHandle_t hTstatSetpointNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hTstatFloorSetpointNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hTstatTemperatureNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hTstatFloorTemperatureNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hTstatHeatDemandNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hTstatHumidityNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hTstatOffModeNotification = DBTHRD_NOTIFHANDLE_NOTSET;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/
    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
DB_NOTIFICATION(MNU_OnTstatSetpointUpdate);
DB_NOTIFICATION(MNU_OnTstatTemperatureUpdate);
DB_NOTIFICATION(MNU_OnTstatHeatDemandUpdate);
DB_NOTIFICATION(MNU_OnTstatHumidityUpdate);
DB_NOTIFICATION(MNU_OnTstatOffModeUpdate);
#ifdef FLOOR
static void MNU_TstatDetails_SetOffMode(void);
#endif

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief    Display the Tstat Setpoint when updated
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/05/04
*******************************************************************************/
DB_NOTIFICATION(MNU_OnTstatSetpointUpdate)
{
    TstatDetailsEditValue = MNU_TstatDetails_GetSetpoint();
    if (TstatFloorMode == CONTROLMODE_FLOOR)
    {
        DPH_DisplaySetpoint(TstatDetailsEditValue, FALSE, offMode, SET_FLOOR, localTstat);
    }
    else
    {
        DPH_DisplaySetpoint(TstatDetailsEditValue, FALSE, offMode, SET_AMBIENT, localTstat);
    }
}

/*******************************************************************************
* @brief    Display the Tstat Temperature when updated
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/05/04
*******************************************************************************/
DB_NOTIFICATION(MNU_OnTstatTemperatureUpdate)
{
    DBTHRD_GetData(DBTYPE_FLOOR_MODE, &TstatFloorMode, SelectedTstat);
    if (TstatFloorMode == CONTROLMODE_FLOOR)
    {
        DBTHRD_GetData(DBTYPE_FLOOR_TEMPERATURE, &TstatTemperature, SelectedTstat);
    }
    else
    {
        DBTHRD_GetData(DBTYPE_AMBIENT_TEMPERATURE, &TstatTemperature, SelectedTstat);
    }
    DPH_DisplayAmbientRoomTemperature(TstatTemperature, TstatFloorMode);
}

/*******************************************************************************
* @brief    Display the Tstat Heat Demand when updated
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/05/04
*******************************************************************************/
DB_NOTIFICATION(MNU_OnTstatHeatDemandUpdate)
{
    DBTHRD_GetData(DBTYPE_HEAT_DEMAND, &TstatHeatDemand, SelectedTstat);
    DPH_DisplayHeatingStatus(TstatHeatDemand, FALSE);
}

/*******************************************************************************
* @brief    Display the Tstat Humidity when updated
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/05/04
*******************************************************************************/
DB_NOTIFICATION(MNU_OnTstatHumidityUpdate)
{
    DBTHRD_GetData(DBTYPE_AMBIENT_HUMIDITY, &TstatHumidity, SelectedTstat);
    DPH_DisplayIndoorHumidity(TstatHumidity);
}

/*******************************************************************************
* @brief  Notification when the off mode status is updated
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2019/05/10
*******************************************************************************/
static DB_NOTIFICATION(MNU_OnTstatOffModeUpdate)
{
    DBTHRD_GetData(DBTYPE_OFF_MODE_SETTING,(void*)&offMode, THIS_THERMOSTAT);
    if (TstatFloorMode == CONTROLMODE_FLOOR)
    {
        DPH_DisplaySetpoint(TstatDetailsEditValue, TRUE, offMode, SET_FLOOR, localTstat);
    }
    else
    {
        DPH_DisplaySetpoint(TstatDetailsEditValue, TRUE, offMode, SET_AMBIENT, localTstat);
    }
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_TstatDetails_Entry
*******************************************************************************/
#pragma optimize = none //TODO - Optimization removed, otherwise the notification does not get set properly
void MNU_TstatDetails_Entry(void)
{
    TstatDetailsEditState = EDIT_IDLE;

    MNU_SetKeyAssigned(FALSE);
    MNU_GroupDetails_GetSelectedTstat(&SelectedTstat);
    offMode = 0;

    TstatDetailsEditValue = MNU_TstatDetails_GetSetpoint();

    localTstat = 0;
    if (SelectedTstat == THIS_THERMOSTAT)
    {
        localTstat = 1;
        DBTHRD_GetData(DBTYPE_OFF_MODE_SETTING,(void*)&offMode, THIS_THERMOSTAT);
        DBTHRD_GetData(DBTYPE_FLOOR_TYPE,(void*)&floorType, THIS_THERMOSTAT);
#ifdef FLOOR
        if (TstatDetailsEditValue == STPM_GetSetpointMin())
        {
            minReached = 1;
        }
        else
        {
            minReached = 0;
        }
#endif
    }

    DBTHRD_GetData(DBTYPE_FLOOR_MODE, &TstatFloorMode, SelectedTstat);
    if (TstatFloorMode == CONTROLMODE_FLOOR)
    {
        DBTHRD_GetData(DBTYPE_FLOOR_TEMPERATURE, &TstatTemperature, SelectedTstat);
    }
    else
    {
        DBTHRD_GetData(DBTYPE_AMBIENT_TEMPERATURE, &TstatTemperature, SelectedTstat);
    }
    DBTHRD_GetData(DBTYPE_HEAT_DEMAND, &TstatHeatDemand, SelectedTstat);
    DBTHRD_GetData(DBTYPE_AMBIENT_HUMIDITY, &TstatHumidity, SelectedTstat);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show Tstat name
    //
    DPH_DisplayTstatDetails(SelectedTstat);

    //
    //  Show settings button
    //
    DPH_DisplaySettingsButton();

    //
    // Show heat demand
    //
    DPH_DisplayHeatingStatus(TstatHeatDemand, FALSE);

    //
    //	Show ambient temperature
    //
    DPH_DisplayAmbientRoomTemperature(TstatTemperature, TstatFloorMode);

    //
    //	Show indoor humidity
    //
    DPH_DisplayIndoorHumidity(TstatHumidity);

    //
    //	Show setpoint
    //
    DPH_DisplaySetpointApplied(TstatDetailsEditState);
    if (TstatFloorMode == CONTROLMODE_FLOOR)
    {
        DPH_DisplaySetpoint(TstatDetailsEditValue, TRUE, offMode, SET_FLOOR, localTstat);
    }
    else
    {
        DPH_DisplaySetpoint(TstatDetailsEditValue, TRUE, offMode, SET_AMBIENT, localTstat);
    }

    hTstatSetpointNotification = DBTHRD_SubscribeToNotification(DBTYPE_AMBIENT_SETPOINT,
                                                       MNU_OnTstatSetpointUpdate,
                                                       DBCHANGE_ALLCHANGES,
                                                       SelectedTstat);

    hTstatFloorSetpointNotification = DBTHRD_SubscribeToNotification(DBTYPE_FLOOR_SETPOINT,
                                                       MNU_OnTstatSetpointUpdate,
                                                       DBCHANGE_ALLCHANGES,
                                                       SelectedTstat);

    hTstatTemperatureNotification = DBTHRD_SubscribeToNotification(DBTYPE_AMBIENT_TEMPERATURE,
                                                       MNU_OnTstatTemperatureUpdate,
                                                       DBCHANGE_ALLCHANGES,
                                                       SelectedTstat);

    hTstatFloorTemperatureNotification = DBTHRD_SubscribeToNotification(DBTYPE_FLOOR_TEMPERATURE,
                                                       MNU_OnTstatTemperatureUpdate,
                                                       DBCHANGE_ALLCHANGES,
                                                       SelectedTstat);

    hTstatHeatDemandNotification = DBTHRD_SubscribeToNotification(DBTYPE_HEAT_DEMAND,
                                                       MNU_OnTstatHeatDemandUpdate,
                                                       DBCHANGE_ALLCHANGES,
                                                       SelectedTstat);

    hTstatHumidityNotification = DBTHRD_SubscribeToNotification(DBTYPE_AMBIENT_HUMIDITY,
                                                       MNU_OnTstatHumidityUpdate,
                                                       DBCHANGE_ALLCHANGES,
                                                       SelectedTstat);

    hTstatOffModeNotification = DBTHRD_SubscribeToNotification( DBTYPE_OFF_MODE_SETTING,
                                                       MNU_OnTstatOffModeUpdate,
                                                       DBCHANGE_LOCAL,
                                                       THIS_THERMOSTAT);

	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_TstatDetails_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_TstatDetails_Core

    Show a specific tstat details
*******************************************************************************/
void MNU_TstatDetails_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure key in the TstatDetails menu
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_TstatDetails_Commit, (void*)KEY_BACK);
        KBH_SetKeyPressProperties(KEY_SETTINGS, (FUNC)MNU_TstatDetails_Commit, (void*)KEY_SETTINGS);

#ifdef FLOOR
        if (minReached == 0)
        {
            KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_TstatDetails_DecSetpointStartTurbo, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
        }
        else
        {
            KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_TstatDetails_SetOffMode, (int16u*) 50, OS_MSTOTICK(KEYB_MENU_HOLD));
        }
#else
        KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_TstatDetails_DecSetpointStartTurbo, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
#endif
        KBH_SetKeyHoldProperties(KEY_UP, (FUNC)MNU_TstatDetails_IncSetpointStartTurbo, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
        KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_TstatDetails_IncSetpoint, (int16u*)50);
        KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_TstatDetails_DecSetpoint, (int16u*)50);

        // Stop key repeat monitoring
        KBH_SetKeyRepeatProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
        KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_TstatDetails_Exit
*******************************************************************************/
void MNU_TstatDetails_Exit(void)
{
    TstatDetailsEditState = EDIT_IDLE;
    DBTHRD_UnsubscribeToNotification(hTstatSetpointNotification);
    DBTHRD_UnsubscribeToNotification(hTstatFloorSetpointNotification);
    DBTHRD_UnsubscribeToNotification(hTstatTemperatureNotification);
    DBTHRD_UnsubscribeToNotification(hTstatFloorTemperatureNotification);
    DBTHRD_UnsubscribeToNotification(hTstatHeatDemandNotification);
    DBTHRD_UnsubscribeToNotification(hTstatHumidityNotification);
    DBTHRD_UnsubscribeToNotification(hTstatOffModeNotification);
}

/*******************************************************************************
    MNU_TstatDetails_Confirm
*******************************************************************************/
void MNU_TstatDetails_Confirm (void)
{
    TstatDetailsEditState = EDIT_IDLE;
    DPH_DisplaySetpointApplied(TstatDetailsEditState);
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_TstatDetails_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_TstatDetails_Commit
*******************************************************************************/
void MNU_TstatDetails_Commit(KEYB_PARAM keyId)
{
    KBH_ClearAllKeyProperties();
    switch(keyId)
    {
        case KEY_BACK:
            MNH_SetActiveMenu(MENU_GROUPDETAILS);
            break;

        case KEY_SETTINGS:
            MNH_SetActiveMenu(MENU_TSTAT_OPTIONS);
            break;

        default:
            MNH_SetActiveMenu(MENU_HOME);
            break;
    }
}

/*******************************************************************************
    MNU_TstatDetails_StopSetpointEdition
*******************************************************************************/
void MNU_TstatDetails_StopSetpointEdition(void)
{
    TstatDetailsEditState = EDIT_CONFIRM;
    DPH_DisplaySetpointApplied(TstatDetailsEditState);
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_TstatDetails_Confirm,  (KEYB_PARAM*)NULL, CONFIRM_TIMEOUT);
}

/*******************************************************************************
    MNU_TstatDetails_IncSetpoint

    Increment a tstat setpoint
*******************************************************************************/
void MNU_TstatDetails_IncSetpoint(int16u value)
{
	UnitFormat_t format;
	TEMPERATURE_C_t maxsetpoint;

    if (TstatFloorMode == CONTROLMODE_FLOOR)
    {
        if (SelectedTstat == THIS_THERMOSTAT)
        {
            if (floorType == FLOORTYPE_TILE)
            {
                maxsetpoint = MAXIMUM_FLOOR_SETPOINT;
            }
            else
            {
                maxsetpoint = MAXIMUM_ENGINEERED_FLOOR_SETPOINT;
            }
        }
        else
        {
            maxsetpoint = MAXIMUM_FLOOR_SETPOINT;
        }
    }
    else
    {
        maxsetpoint = MAXIMUM_SETPOINT;
    }
    TstatDetailsEditValue = MNU_TstatDetails_GetSetpoint();

    TstatDetailsEditState = EDIT_IN_PROGRESS;

    if ((TstatDetailsEditValue < maxsetpoint) && (TstatDetailsEditValue != DB_SENSOR_INVALID) && (offMode == 0))
    {
        DBTHRD_GetData(DBTYPE_TEMPERATURE_FORMAT,(void*)&format, INDEX_DONT_CARE);
        if(format == DEGREE_F)
        {
            TstatDetailsEditValue = CVT_ConvertCtoF(TstatDetailsEditValue, 100);
            TstatDetailsEditValue += (value * 2);
            TstatDetailsEditValue = CVT_ConvertFtoC(TstatDetailsEditValue, 0);
        }
        else
        {
            TstatDetailsEditValue = TLS_Round(TstatDetailsEditValue, 50);
            TstatDetailsEditValue += value;
            TstatDetailsEditValue = TLS_Round(TstatDetailsEditValue, 50);
        }
        if (TstatDetailsEditValue > maxsetpoint)
        {
            TstatDetailsEditValue = maxsetpoint;
        }
        DPH_DisplaySetpointApplied(TstatDetailsEditState);
        if (TstatFloorMode == CONTROLMODE_FLOOR)
        {
            DPH_DisplaySetpoint(TstatDetailsEditValue, FALSE, offMode, SET_FLOOR, localTstat);
        }
        else
        {
            DPH_DisplaySetpoint(TstatDetailsEditValue, FALSE, offMode, SET_AMBIENT, localTstat);
        }
    }

#ifdef FLOOR
    if (offMode != 0)
    {
        offMode = 0;
        DBTHRD_SetData(DBTYPE_OFF_MODE_SETTING,(void*)&offMode, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    }
    minReached = 0;
    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_TstatDetails_DecSetpointStartTurbo, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
#endif

	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_TstatDetails_CommitSetpoint, (U8BIT*)NULL, SETPOINT_EDIT_TIMEOUT);
}

/*******************************************************************************
    MNU_TstatDetails_DecSetpoint

    Decrement a tstat setpoint
*******************************************************************************/
void MNU_TstatDetails_DecSetpoint(int16u value)
{
	UnitFormat_t format;
	TEMPERATURE_C_t minsetpoint;

    minsetpoint = STPM_GetSetpointMin();
    TstatDetailsEditValue = MNU_TstatDetails_GetSetpoint();

    TstatDetailsEditState = EDIT_IN_PROGRESS;

    if ((TstatDetailsEditValue > minsetpoint) && (TstatDetailsEditValue != DB_SENSOR_INVALID))
    {
        DBTHRD_GetData(DBTYPE_TEMPERATURE_FORMAT,(void*)&format, INDEX_DONT_CARE);
        if(format == DEGREE_F)
        {
            TstatDetailsEditValue = CVT_ConvertCtoF(TstatDetailsEditValue, 100);
            TstatDetailsEditValue -= (value * 2);
            TstatDetailsEditValue = CVT_ConvertFtoC(TstatDetailsEditValue, 0);
        }
        else
        {
            TstatDetailsEditValue = TLS_Round(TstatDetailsEditValue, 50);
            TstatDetailsEditValue -= value;
            TstatDetailsEditValue = TLS_Round(TstatDetailsEditValue, 50);
        }
        if (TstatDetailsEditValue < minsetpoint)
        {
            TstatDetailsEditValue = minsetpoint;
        }
        DPH_DisplaySetpointApplied(TstatDetailsEditState);
        if (TstatFloorMode == CONTROLMODE_FLOOR)
        {
            DPH_DisplaySetpoint(TstatDetailsEditValue, FALSE, offMode, SET_FLOOR, localTstat);
        }
        else
        {
            DPH_DisplaySetpoint(TstatDetailsEditValue, FALSE, offMode, SET_AMBIENT, localTstat);
        }
    }
#ifdef FLOOR
    if ((TstatDetailsEditValue == minsetpoint) && (SelectedTstat == THIS_THERMOSTAT))
    {
        if (minReached == 0)
        {
            minReached = 1;
            // Monitor for hold key to enter off mode
            KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)MNU_TstatDetails_SetOffMode, (void*) NULL, OS_MSTOTICK(KEYB_MENU_HOLD));
        }
    }
#endif

    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_TstatDetails_CommitSetpoint, (U8BIT*)NULL, SETPOINT_EDIT_TIMEOUT);
}

#ifdef FLOOR
/*******************************************************************************
    MNU_TstatDetails_SetOffMode

    Enter in Off Mode
*******************************************************************************/
static void MNU_TstatDetails_SetOffMode(void)
{
    uint8_t offMode = 1;
    DBTHRD_SetData(DBTYPE_OFF_MODE_SETTING,(void*)&offMode, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);
    if (TstatFloorMode == CONTROLMODE_FLOOR)
    {
        DPH_DisplaySetpoint(TstatDetailsEditValue, TRUE, offMode, SET_FLOOR, localTstat);
    }
    else
    {
        DPH_DisplaySetpoint(TstatDetailsEditValue, TRUE, offMode, SET_AMBIENT, localTstat);
    }
}
#endif

/*******************************************************************************
    MNU_TstatDetails_DecSetpointStartTurbo

    Decrement a tstat setpoint
*******************************************************************************/
void MNU_TstatDetails_DecSetpointStartTurbo(void)
{
    KBH_SetKeyHoldProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

    // Decrement setpoint
    MNU_TstatDetails_DecSetpoint(50);
#ifdef FLOOR
    if (minReached == 0)
    {
        // Turn on key repeat notification
        KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)MNU_TstatDetails_DecSetpoint, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_DELAY));
    }
#else
    // Turn on key repeat notification
    KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)MNU_TstatDetails_DecSetpoint, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_DELAY));
#endif

    KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_TstatDetails_SetpointStopTurbo, (void*)NULL);
    KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_TstatDetails_SetpointStopTurbo, (void*)NULL);
}

/*******************************************************************************
    MNU_TstatDetails_IncSetpointStartTurbo

    Increment a tstat setpoint
*******************************************************************************/
void MNU_TstatDetails_IncSetpointStartTurbo(void)
{
    KBH_SetKeyHoldProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

    // Increment setpoint
    MNU_TstatDetails_IncSetpoint(50);
    // Turn on key repeat notification
    KBH_SetKeyRepeatProperties(KEY_UP, (FUNC)MNU_TstatDetails_IncSetpoint, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_DELAY));

    KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_TstatDetails_SetpointStopTurbo, (void*)NULL);
    KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_TstatDetails_SetpointStopTurbo, (void*)NULL);
}

/*******************************************************************************
    MNU_TstatDetails_SetpointStopTurbo
*******************************************************************************/
void MNU_TstatDetails_SetpointStopTurbo(void)
{
    // Stop key repeat monitoring
	KBH_SetKeyRepeatProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

    // Start key hold monitoring
	KBH_SetKeyHoldProperties(KEY_UP, (FUNC)MNU_TstatDetails_IncSetpointStartTurbo, (void*) NULL, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
#ifdef FLOOR
    if (minReached == 0)
    {
        KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_TstatDetails_DecSetpointStartTurbo, (void*) NULL, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
    }
    else
    {
        KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_TstatDetails_SetOffMode, (void*) NULL, OS_MSTOTICK(KEYB_MENU_HOLD));
    }
#else
    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_TstatDetails_DecSetpointStartTurbo, (void*) NULL, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
#endif
	KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_TstatDetails_IncSetpoint, (int16u*) 50);
    KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_TstatDetails_DecSetpoint, (int16u*) 50);
}

/*******************************************************************************
    MNU_TstatDetails_GetSetpoint

    Get the tstat setpoint from the Setpoint Manager
*******************************************************************************/
TEMPERATURE_C_t MNU_TstatDetails_GetSetpoint(void)
{
    TEMPERATURE_C_t setpoint;

    // If not currently in setpoint edition mode
    if ( TstatDetailsEditState == EDIT_IDLE )
    {
        DBTHRD_GetData(DBTYPE_FLOOR_MODE,(void*)&TstatFloorMode, SelectedTstat);
        if (TstatFloorMode == CONTROLMODE_FLOOR)
        {
            // Return control floor setpoint
            DBTHRD_GetData(DBTYPE_FLOOR_SETPOINT,(void*)&setpoint, SelectedTstat);
        }
        else
        {
            // Return control ambient setpoint
            DBTHRD_GetData(DBTYPE_AMBIENT_SETPOINT,(void*)&setpoint, SelectedTstat);
        }
    }
    // Else currently in setpoint edition mode
    else
    {
        // Return current edited setpoint value
        setpoint = TstatDetailsEditValue;
    }
    return ( setpoint );
}

/*******************************************************************************
    MNU_TstatDetails_CommitSetpoint

    Update the Setpoint Manager with a new tstat setpoint
*******************************************************************************/
void MNU_TstatDetails_CommitSetpoint( void )
{
    TEMPERATURE_C_t     currentSetpoint;

    DBTHRD_GetData(DBTYPE_FLOOR_MODE,(void*)&TstatFloorMode, SelectedTstat);
    if (TstatFloorMode == CONTROLMODE_FLOOR)
    {
        DBTHRD_GetData(DBTYPE_FLOOR_SETPOINT,(void*)&currentSetpoint, SelectedTstat);
    }
    else
    {
        DBTHRD_GetData(DBTYPE_AMBIENT_SETPOINT,(void*)&currentSetpoint, SelectedTstat);
    }

    if ( TstatDetailsEditState == EDIT_IN_PROGRESS )
    {
        // If user changed tstat setpoint
        if (( TstatDetailsEditValue != currentSetpoint ) && (TstatDetailsEditValue != DB_SENSOR_INVALID))
        {
            if (TstatFloorMode == CONTROLMODE_FLOOR)
            {
                // Apply user tstat floor setpoint
                DBTHRD_SetData(DBTYPE_FLOOR_SETPOINT,(void*)&TstatDetailsEditValue, SelectedTstat, DBCHANGE_LOCAL);
            }
            else
            {
                // Apply user tstat ambient setpoint
                DBTHRD_SetData(DBTYPE_AMBIENT_SETPOINT,(void*)&TstatDetailsEditValue, SelectedTstat, DBCHANGE_LOCAL);
            }
            MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_TstatDetails_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
        }
    }
    MNU_TstatDetails_StopSetpointEdition();
}
