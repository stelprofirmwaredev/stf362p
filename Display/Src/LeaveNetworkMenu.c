/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       LeaveNetworkMenu.c

    \brief      Source code file for the LeaveNetworkMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the LeaveNetworkMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "LeaveNetworkMenu.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\ZigBee\inc\THREAD_ZigBee.h"
#include ".\DB\classes\inc\class_Thermostats.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   LeaveNetworkEditState = EDIT_IDLE;
static LeaveIntention_t LeaveIntention = NO_LEAVE;
static susbscriptionHandle_t hNetworkLeftNotification = DBTHRD_NOTIFHANDLE_NOTSET;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void ChangeLeaveIntention(KEYB_PARAM keyId);
static DB_NOTIFICATION(OnNetworkLeft);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  Change the Leave Intention
* @inputs keyId: key pressed
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/08/04
*******************************************************************************/
static void ChangeLeaveIntention(KEYB_PARAM keyId)
{
//    dbType_ZigBeeNetworkInfo_t nwInfo;

    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_LeaveNetwork_Commit, (KEYB_PARAM*)KEY_HOME, DESTROY_NETWORK_TIMEOUT);

    if (LeaveIntention == NO_LEAVE)
    {
        LeaveIntention = LEAVE_IN_PROGRESS;
        LeaveNetworkEditState = EDIT_COMMITTING;
        KBH_ClearAllKeyProperties();
        ZBTHRD_ResetRadioToFactoryNew();
        DPH_DisplayLeaveNetwork(LeaveIntention);
//TODO - Implement router mode
    }
    else if (LeaveIntention == LEAVE_CONFIRMED)
    {
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_LeaveNetwork_Commit, (KEYB_PARAM*)KEY_DONE, IMMEDIATELY);
    }

    DPH_DisplayLeaveNetwork(LeaveIntention);
}

/*******************************************************************************
    OnNetworkLeft notification
*******************************************************************************/
static DB_NOTIFICATION(OnNetworkLeft)
{
    dbType_ZigBeeNetworkInfo_t nwInfo;

    DBTHRD_GetData(DBTYPE_ZIGBEE_NETWORK_INFO,(void*)&nwInfo, INDEX_DONT_CARE);
    //TODO - We are currently reforming the network after leaving it, implement router mode
#ifdef ZIGBEE_CERTIFICATION_INTERFACE
#else    
    if (nwInfo.InANetwork == 1)
#endif
    {
        LeaveIntention = LEAVE_CONFIRMED;
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_LeaveNetwork_Commit, (void*)KEY_BACK);
        KBH_SetKeyPressProperties(KEY_DONE, (FUNC)ChangeLeaveIntention, (void*)KEY_DONE);
        MNU_SetKeyAssigned(TRUE);
    }
    DPH_DisplayLeaveNetwork(LeaveIntention);
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_LeaveNetwork_Commit, (KEYB_PARAM*)KEY_HOME, DESTROY_NETWORK_TIMEOUT);
}


/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_LeaveNetwork_Entry
*******************************************************************************/
void MNU_LeaveNetwork_Entry(void)
{
    LeaveNetworkEditState = EDIT_IN_PROGRESS;
    LeaveIntention = NO_LEAVE;

    MNU_SetKeyAssigned(FALSE);

    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_LeaveNetwork_Commit, (KEYB_PARAM*)KEY_HOME, DESTROY_NETWORK_TIMEOUT);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show Leave Network options
    //
    DPH_DisplayLeaveNetwork(LeaveIntention);

    hNetworkLeftNotification = DBTHRD_SubscribeToNotification(DBTYPE_ZIGBEE_NETWORK_INFO,
                                                       OnNetworkLeft,
                                                       DBCHANGE_ALLCHANGES,
                                                       INDEX_DONT_CARE);
}

/*******************************************************************************
    MNU_LeaveNetwork_Core

    Allows to leave a ZigBee network
*******************************************************************************/
void MNU_LeaveNetwork_Core(void)
{
    if (LeaveNetworkEditState == EDIT_IN_PROGRESS)
    {
        if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
        {
            //
            // Configure keys
            //
            KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_LeaveNetwork_Commit, (void*)KEY_BACK);
            KBH_SetKeyPressProperties(KEY_DONE, (FUNC)ChangeLeaveIntention, (void*)KEY_DONE);
            MNU_SetKeyAssigned(TRUE);
        }
        else
        {
            if (IsKeyAssigned() == FALSE)
            {
                OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
            }
        }
    }
}

/*******************************************************************************
    MNU_LeaveNetwork_Exit
*******************************************************************************/
void MNU_LeaveNetwork_Exit(void)
{
    LeaveNetworkEditState = EDIT_IDLE;
    DBTHRD_UnsubscribeToNotification(hNetworkLeftNotification);
}

/*******************************************************************************
    MNU_LeaveNetwork_Commit
*******************************************************************************/
void MNU_LeaveNetwork_Commit(KEYB_PARAM keyId)
{
    static KEYB_PARAM commitKey = NULL;

    if ( LeaveNetworkEditState == EDIT_IN_PROGRESS )
    {
        commitKey = keyId;
        KBH_ClearAllKeyProperties();
        switch (commitKey)
        {
            case KEY_DONE:
            case KEY_BACK:
            default:
                MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_LeaveNetwork_Commit, (KEYB_PARAM*)commitKey, IMMEDIATELY);
                break;
        }
        DPH_DisplayLeaveNetwork(LeaveIntention);
        LeaveNetworkEditState = EDIT_COMMITTING;
    }
    else if ( LeaveNetworkEditState == EDIT_COMMITTING )
    {
        LeaveNetworkEditState = EDIT_IDLE;
        switch (commitKey)
        {
            case KEY_BACK:
            case KEY_DONE:
                MNH_SetActiveMenu(MENU_ZIGBEE_SETUP);
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
    }
}
