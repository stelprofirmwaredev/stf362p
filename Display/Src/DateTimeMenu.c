/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2019, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    DateTimeMenu.c
* @date    2019/07/29
* @authors Jean-Fran�ois Many
* @brief   Date & Time Menu
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include ".\DB\inc\THREAD_DB.h"
#include "DateTimeMenu.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define MINIMUM_YEAR    2019
#define MAXIMUM_YEAR    2099

#define MAXIMUM_MINUTES_OF_THE_DAY  1439

#define KEY_VALUE_JAN   1
#define KEY_VALUE_FEB   4
#define KEY_VALUE_MAR   4
#define KEY_VALUE_APR   0
#define KEY_VALUE_MAY   2
#define KEY_VALUE_JUNE  5
#define KEY_VALUE_JULY  0
#define KEY_VALUE_AUG   3
#define KEY_VALUE_SEPT  6
#define KEY_VALUE_OCT   1
#define KEY_VALUE_NOV   4
#define KEY_VALUE_DEC   6

#define KEY_VALUE_2000  6

#define DELAY_BEFORE_15MIN_STEP 15
#define DELAY_BEFORE_60MIN_STEP 30
#define LIGHT_SPEED             1
#define RIDICULOUS_SPEED        15
#define LUDICROUS_SPEED         60

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

typedef enum
{
    MTH_JANUARY = 1,
    MTH_FEBRUARY,
    MTH_MARCH,
    MTH_APRIL,
    MTH_MAY,
    MTH_JUNE,
    MTH_JULY,
    MTH_AUGUST,
    MTH_SEPTEMBER,
    MTH_OCTOBER,
    MTH_NOVEMBER,
    MTH_DECEMBER
} Month_e;

static uint16_t         DateTime_Year;
static Month_e          DateTime_Month;
static uint8_t          DateTime_Day;
static uint16_t         DateTime_MinutesOfDay;
static uint8_t          DateTime_DayOfWeek;
static uint8_t          maxDayOfMonth;
static uint8_t          TurboChange;
static uint8_t          TurboIncrement;
static uint8_t          previousMenu;

/*  rtc day of week start at 1(monday) to 7(sunday)
  while key value day start at 0(saturday) to 6 (friday)   */
static const uint8_t key_value_day_to_rtc_day[7] =
{
    6,                  // key value 0 is saturday (6 in rtc day)
    7,                  // key value sunday
    1,                  // key value monday
    2,                  // key value tuesday
    3,                  // key value wednesday
    4,                  // key value thursday
    5,                  // key value friday
};


/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static void ChangeYear(KEYB_PARAM keyId);
static void ChangeMonth(KEYB_PARAM keyId);
static void ChangeDay(KEYB_PARAM keyId);
static void ChangeTime(KEYB_PARAM keyId);
static void ChangeTimeTurbo(KEYB_PARAM keyId);
static uint8_t CalculateMaxDayOfMonth(Month_e mth, uint16_t year);
static uint8_t CalculateDayOfWeek(uint16_t year, uint8_t month, uint8_t day);
static void ReassessDateInfo(void);
static void StartTurbo(KEYB_PARAM keyId);
static void ConfigureKeys(void);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
static void ChangeYear(KEYB_PARAM keyId)
{
    uint8_t onboardingDone;

    switch (keyId)
    {
        case KEY_OPTIONS_1:
            DateTime_Year--;
            if (DateTime_Year < MINIMUM_YEAR)
            {
                DateTime_Year = MAXIMUM_YEAR;
            }
            break;

        case KEY_OPTIONS_2:
            DateTime_Year++;
            if (DateTime_Year > MAXIMUM_YEAR)
            {
                DateTime_Year = MINIMUM_YEAR;
            }
            break;

        default:
            break;
    }

    ReassessDateInfo();

    DPH_DisplayDateTime(FALSE, DateTime_Year, DateTime_Month, DateTime_Day, DateTime_MinutesOfDay, DateTime_DayOfWeek);

    DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);
    if (onboardingDone == 1)
    {
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_DateTime_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
static void ChangeMonth(KEYB_PARAM keyId)
{
    uint8_t onboardingDone;

    switch (keyId)
    {
        case KEY_OPTIONS_3:
            DateTime_Month--;
            if (DateTime_Month < MTH_JANUARY)
            {
                DateTime_Month = MTH_DECEMBER;
            }
            break;

        case KEY_OPTIONS_4:
            DateTime_Month++;
            if (DateTime_Month > MTH_DECEMBER)
            {
                DateTime_Month = MTH_JANUARY;
            }
            break;

        default:
            break;
    }

    ReassessDateInfo();

    DPH_DisplayDateTime(FALSE, DateTime_Year, DateTime_Month, DateTime_Day, DateTime_MinutesOfDay, DateTime_DayOfWeek);

    DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);
    if (onboardingDone == 1)
    {
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_DateTime_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
static void ChangeDay(KEYB_PARAM keyId)
{
    uint8_t onboardingDone;

    switch (keyId)
    {
        case KEY_OPTIONS_5:
            DateTime_Day--;
            if (DateTime_Day < 1)
            {
                DateTime_Day = maxDayOfMonth;
            }
            break;

        case KEY_OPTIONS_6:
            DateTime_Day++;
            if (DateTime_Day > maxDayOfMonth)
            {
                DateTime_Day = 1;
            }
            break;

        default:
            break;
    }

    ReassessDateInfo();

    DPH_DisplayDateTime(FALSE, DateTime_Year, DateTime_Month, DateTime_Day, DateTime_MinutesOfDay, DateTime_DayOfWeek);

    DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);
    if (onboardingDone == 1)
    {
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_DateTime_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
static void ChangeTime(KEYB_PARAM keyId)
{
    uint8_t onboardingDone;
    uint8_t changeStep;

    changeStep = 1;
    if (TurboChange != 0)
    {
        if (TurboIncrement < DELAY_BEFORE_15MIN_STEP)
        {
            TurboIncrement++;
            changeStep = LIGHT_SPEED;
        }
        else if (TurboIncrement < DELAY_BEFORE_60MIN_STEP)
        {
            TurboIncrement++;
            changeStep = RIDICULOUS_SPEED;
        }
        else
        {
            changeStep = LUDICROUS_SPEED;
        }
        DateTime_MinutesOfDay -= DateTime_MinutesOfDay % changeStep;
    }

    switch (keyId)
    {
        case KEY_OPTIONS_7:
            if (DateTime_MinutesOfDay >= changeStep)
            {
                DateTime_MinutesOfDay -= changeStep;
            }
            else
            {
                DateTime_MinutesOfDay = MAXIMUM_MINUTES_OF_THE_DAY;
            }
            break;

        case KEY_OPTIONS_8:
            DateTime_MinutesOfDay += changeStep;
            if (DateTime_MinutesOfDay > MAXIMUM_MINUTES_OF_THE_DAY)
            {
                DateTime_MinutesOfDay = 0;
            }
            break;

        default:
            break;
    }

    DPH_DisplayDateTime(FALSE, DateTime_Year, DateTime_Month, DateTime_Day, DateTime_MinutesOfDay, DateTime_DayOfWeek);

    DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);
    if (onboardingDone == 1)
    {
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_DateTime_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
static void ChangeTimeTurbo(KEYB_PARAM keyId)
{
    TurboChange = 1;
    ChangeTime(keyId);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
static uint8_t CalculateMaxDayOfMonth(Month_e mth, uint16_t year)
{
    uint8_t maxDay;

    switch (mth)
    {
        case MTH_JANUARY:
        case MTH_MARCH:
        case MTH_MAY:
        case MTH_JULY:
        case MTH_AUGUST:
        case MTH_OCTOBER:
        case MTH_DECEMBER:
            maxDay = 31;
            break;

        case MTH_FEBRUARY:
            if (year % 4)
            {
                maxDay = 28;
            }
            else
            {
                maxDay = 29;
            }
            break;

        case MTH_APRIL:
        case MTH_JUNE:
        case MTH_SEPTEMBER:
        case MTH_NOVEMBER:
            maxDay = 30;
            break;
    }
    return maxDay;
}

/*******************************************************************************
* @brief Function that calculates the Day of Week based on the "Key Value Method"
* http://mathforum.org/dr.math/faq/faq.calendar.html
* @inputs uint16_t year, uint8_t month, uint8_t day
* @retval uint8_t dayOfWeek
* @author Jean-Fran�ois Many
* @date 12/12/2018
*******************************************************************************/
#pragma optimize = none
static uint8_t CalculateDayOfWeek(uint16_t year, uint8_t month, uint8_t day)
{
    uint32_t keyValue;
    uint32_t month_key_value;

    switch (month)
    {
        case 1:
            month_key_value = KEY_VALUE_JAN;
            if ((year % 4) == 0)
            {
                month_key_value -= 1;
            }
            break;

        case 2:
            month_key_value = KEY_VALUE_FEB;
            if ((year % 4) == 0)
            {
                month_key_value -= 1;
            }
            break;

        case 3:
            month_key_value = KEY_VALUE_MAR;
            break;

        case 4:
            month_key_value = KEY_VALUE_APR;
            break;

        case 5:
            month_key_value = KEY_VALUE_MAY;
            break;

        case 6:
            month_key_value = KEY_VALUE_JUNE;
            break;

        case 7:
            month_key_value = KEY_VALUE_JULY;
            break;

        case 8:
            month_key_value = KEY_VALUE_AUG;
            break;

        case 9:
            month_key_value = KEY_VALUE_SEPT;
            break;

        case 10:
            month_key_value = KEY_VALUE_OCT;
            break;

        case 11:
            month_key_value = KEY_VALUE_NOV;
            break;

        case 12:
            month_key_value = KEY_VALUE_DEC;
            break;

        default:
            break;
    }
    keyValue = year - 2000;
    keyValue /= 4;
    keyValue += day;
    keyValue += month_key_value;
    keyValue += KEY_VALUE_2000;
    keyValue += year - 2000;
    keyValue = keyValue % 7;

    return key_value_day_to_rtc_day[keyValue];
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
static void ReassessDateInfo(void)
{
    //Check max day of the month
    maxDayOfMonth = CalculateMaxDayOfMonth(DateTime_Month, DateTime_Year);
    if (DateTime_Day > maxDayOfMonth)
    {
        DateTime_Day = maxDayOfMonth;
    }

    //Determine week day from date by using the Key Value Method
    DateTime_DayOfWeek = CalculateDayOfWeek(DateTime_Year,DateTime_Month,DateTime_Day);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
static void StartTurbo(KEYB_PARAM keyId)
{
    switch(keyId)
    {
        case KEY_OPTIONS_1:
            KBH_SetKeyHoldProperties(KEY_OPTIONS_1, (FUNC)NULL, (void*)NULL, 0);
            KBH_SetKeyHoldProperties(KEY_OPTIONS_2, (FUNC)NULL, (void*)NULL, 0);
            ChangeYear(keyId);
            KBH_SetKeyRepeatProperties(KEY_OPTIONS_1, (FUNC)ChangeYear, (void*) KEY_OPTIONS_1, OS_MSTOTICK(KEYB_SLOWER_REPEAT_DELAY));
            KBH_SetKeyReleaseProperties(KEY_OPTIONS_1, (FUNC)ConfigureKeys, (void*)NULL);
            KBH_SetKeyReleaseProperties(KEY_OPTIONS_2, (FUNC)ConfigureKeys, (void*)NULL);
            break;

        case KEY_OPTIONS_2:
            KBH_SetKeyHoldProperties(KEY_OPTIONS_1, (FUNC)NULL, (void*)NULL, 0);
            KBH_SetKeyHoldProperties(KEY_OPTIONS_2, (FUNC)NULL, (void*)NULL, 0);
            ChangeYear(keyId);
            KBH_SetKeyRepeatProperties(KEY_OPTIONS_2, (FUNC)ChangeYear, (void*) KEY_OPTIONS_2, OS_MSTOTICK(KEYB_SLOWER_REPEAT_DELAY));
            KBH_SetKeyReleaseProperties(KEY_OPTIONS_1, (FUNC)ConfigureKeys, (void*)NULL);
            KBH_SetKeyReleaseProperties(KEY_OPTIONS_2, (FUNC)ConfigureKeys, (void*)NULL);
            break;

        case KEY_OPTIONS_3:
            KBH_SetKeyHoldProperties(KEY_OPTIONS_3, (FUNC)NULL, (void*)NULL, 0);
            KBH_SetKeyHoldProperties(KEY_OPTIONS_4, (FUNC)NULL, (void*)NULL, 0);
            ChangeYear(keyId);
            KBH_SetKeyRepeatProperties(KEY_OPTIONS_3, (FUNC)ChangeMonth, (void*) KEY_OPTIONS_3, OS_MSTOTICK(KEYB_SLOWER_REPEAT_DELAY));
            KBH_SetKeyReleaseProperties(KEY_OPTIONS_3, (FUNC)ConfigureKeys, (void*)NULL);
            KBH_SetKeyReleaseProperties(KEY_OPTIONS_4, (FUNC)ConfigureKeys, (void*)NULL);
            break;

        case KEY_OPTIONS_4:
            KBH_SetKeyHoldProperties(KEY_OPTIONS_3, (FUNC)NULL, (void*)NULL, 0);
            KBH_SetKeyHoldProperties(KEY_OPTIONS_4, (FUNC)NULL, (void*)NULL, 0);
            ChangeYear(keyId);
            KBH_SetKeyRepeatProperties(KEY_OPTIONS_4, (FUNC)ChangeMonth, (void*) KEY_OPTIONS_4, OS_MSTOTICK(KEYB_SLOWER_REPEAT_DELAY));
            KBH_SetKeyReleaseProperties(KEY_OPTIONS_3, (FUNC)ConfigureKeys, (void*)NULL);
            KBH_SetKeyReleaseProperties(KEY_OPTIONS_4, (FUNC)ConfigureKeys, (void*)NULL);
            break;

        case KEY_OPTIONS_5:
            KBH_SetKeyHoldProperties(KEY_OPTIONS_5, (FUNC)NULL, (void*)NULL, 0);
            KBH_SetKeyHoldProperties(KEY_OPTIONS_6, (FUNC)NULL, (void*)NULL, 0);
            ChangeYear(keyId);
            KBH_SetKeyRepeatProperties(KEY_OPTIONS_5, (FUNC)ChangeDay, (void*) KEY_OPTIONS_5, OS_MSTOTICK(KEYB_SLOWER_REPEAT_DELAY));
            KBH_SetKeyReleaseProperties(KEY_OPTIONS_5, (FUNC)ConfigureKeys, (void*)NULL);
            KBH_SetKeyReleaseProperties(KEY_OPTIONS_6, (FUNC)ConfigureKeys, (void*)NULL);
            break;

        case KEY_OPTIONS_6:
            KBH_SetKeyHoldProperties(KEY_OPTIONS_5, (FUNC)NULL, (void*)NULL, 0);
            KBH_SetKeyHoldProperties(KEY_OPTIONS_6, (FUNC)NULL, (void*)NULL, 0);
            ChangeYear(keyId);
            KBH_SetKeyRepeatProperties(KEY_OPTIONS_6, (FUNC)ChangeDay, (void*) KEY_OPTIONS_6, OS_MSTOTICK(KEYB_SLOWER_REPEAT_DELAY));
            KBH_SetKeyReleaseProperties(KEY_OPTIONS_5, (FUNC)ConfigureKeys, (void*)NULL);
            KBH_SetKeyReleaseProperties(KEY_OPTIONS_6, (FUNC)ConfigureKeys, (void*)NULL);
            break;

        case KEY_OPTIONS_7:
            KBH_SetKeyHoldProperties(KEY_OPTIONS_7, (FUNC)NULL, (void*)NULL, 0);
            KBH_SetKeyHoldProperties(KEY_OPTIONS_8, (FUNC)NULL, (void*)NULL, 0);
            ChangeYear(keyId);
            KBH_SetKeyRepeatProperties(KEY_OPTIONS_7, (FUNC)ChangeTimeTurbo, (void*) KEY_OPTIONS_7, OS_MSTOTICK(KEYB_SLOWER_REPEAT_DELAY));
            KBH_SetKeyReleaseProperties(KEY_OPTIONS_7, (FUNC)ConfigureKeys, (void*)NULL);
            KBH_SetKeyReleaseProperties(KEY_OPTIONS_8, (FUNC)ConfigureKeys, (void*)NULL);
            break;

        case KEY_OPTIONS_8:
            KBH_SetKeyHoldProperties(KEY_OPTIONS_7, (FUNC)NULL, (void*)NULL, 0);
            KBH_SetKeyHoldProperties(KEY_OPTIONS_8, (FUNC)NULL, (void*)NULL, 0);
            ChangeYear(keyId);
            KBH_SetKeyRepeatProperties(KEY_OPTIONS_8, (FUNC)ChangeTimeTurbo, (void*) KEY_OPTIONS_8, OS_MSTOTICK(KEYB_SLOWER_REPEAT_DELAY));
            KBH_SetKeyReleaseProperties(KEY_OPTIONS_7, (FUNC)ConfigureKeys, (void*)NULL);
            KBH_SetKeyReleaseProperties(KEY_OPTIONS_8, (FUNC)ConfigureKeys, (void*)NULL);
            break;
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
static void ConfigureKeys(void)
{
    //
    // Configure keys
    //
    KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_DateTime_Commit, (void*)KEY_BACK);
    KBH_SetKeyPressProperties(KEY_DONE, (FUNC)MNU_DateTime_Commit, (void*)KEY_DONE);

    KBH_SetKeyHoldProperties(KEY_OPTIONS_1, (FUNC)StartTurbo, (void*) KEY_OPTIONS_1, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
    KBH_SetKeyReleaseProperties(KEY_OPTIONS_1, (FUNC)ChangeYear, (void*)KEY_OPTIONS_1);
    KBH_SetKeyHoldProperties(KEY_OPTIONS_2, (FUNC)StartTurbo, (void*) KEY_OPTIONS_2, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
    KBH_SetKeyReleaseProperties(KEY_OPTIONS_2, (FUNC)ChangeYear, (void*)KEY_OPTIONS_2);

    KBH_SetKeyHoldProperties(KEY_OPTIONS_3, (FUNC)StartTurbo, (void*) KEY_OPTIONS_3, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
    KBH_SetKeyReleaseProperties(KEY_OPTIONS_3, (FUNC)ChangeMonth, (void*)KEY_OPTIONS_3);
    KBH_SetKeyHoldProperties(KEY_OPTIONS_4, (FUNC)StartTurbo, (void*) KEY_OPTIONS_4, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
    KBH_SetKeyReleaseProperties(KEY_OPTIONS_4, (FUNC)ChangeMonth, (void*)KEY_OPTIONS_4);

    KBH_SetKeyHoldProperties(KEY_OPTIONS_5, (FUNC)StartTurbo, (void*) KEY_OPTIONS_5, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
    KBH_SetKeyReleaseProperties(KEY_OPTIONS_5, (FUNC)ChangeDay, (void*)KEY_OPTIONS_5);
    KBH_SetKeyHoldProperties(KEY_OPTIONS_6, (FUNC)StartTurbo, (void*) KEY_OPTIONS_6, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
    KBH_SetKeyReleaseProperties(KEY_OPTIONS_6, (FUNC)ChangeDay, (void*)KEY_OPTIONS_6);

    KBH_SetKeyHoldProperties(KEY_OPTIONS_7, (FUNC)StartTurbo, (void*) KEY_OPTIONS_7, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
    KBH_SetKeyReleaseProperties(KEY_OPTIONS_7, (FUNC)ChangeTime, (void*)KEY_OPTIONS_7);
    KBH_SetKeyHoldProperties(KEY_OPTIONS_8, (FUNC)StartTurbo, (void*) KEY_OPTIONS_8, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
    KBH_SetKeyReleaseProperties(KEY_OPTIONS_8, (FUNC)ChangeTime, (void*)KEY_OPTIONS_8);

    // Stop key repeat monitoring
    KBH_SetKeyRepeatProperties(KEY_OPTIONS_1, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyRepeatProperties(KEY_OPTIONS_2, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyRepeatProperties(KEY_OPTIONS_3, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyRepeatProperties(KEY_OPTIONS_4, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyRepeatProperties(KEY_OPTIONS_5, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyRepeatProperties(KEY_OPTIONS_6, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyRepeatProperties(KEY_OPTIONS_7, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyRepeatProperties(KEY_OPTIONS_8, (FUNC)NULL, (void*)NULL, 0);

    TurboChange = 0;
    TurboIncrement = 0;
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
void MNU_DateTime_Entry(void)
{
    dbType_Date_t localDate;
    RtcTime_t time;
    uint8_t onboardingDone;

    MNU_SetKeyAssigned(FALSE);

    previousMenu = MNH_GetPreviousMenu();

    DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);
    if (onboardingDone == 1)
    {
        //
        //  Show Back arrow
        //
        DPH_DisplayBack();
    }

    //Get Date
    DBTHRD_GetData(DBTYPE_DATE, (void*)&localDate, THIS_THERMOSTAT);
    //Get Time
    DBTHRD_GetData(DBTYPE_LOCALTIME, (void*)&time, THIS_THERMOSTAT);
    DateTime_Year = localDate.Year;
    DateTime_Month = (Month_e)localDate.Month;
    DateTime_Day = localDate.Day;
    DateTime_MinutesOfDay = time.MinutesOfDay;

    ReassessDateInfo();

    //
    //  Show date and time
    //
    DPH_DisplayDateTime(TRUE, DateTime_Year, DateTime_Month, DateTime_Day, DateTime_MinutesOfDay, DateTime_DayOfWeek);

    if (onboardingDone == 1)
    {
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_DateTime_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
void MNU_DateTime_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        ConfigureKeys();
        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
void MNU_DateTime_Exit(void)
{
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
void MNU_DateTime_Commit(KEYB_PARAM keyId)
{
    static KEYB_PARAM commitKey = NULL;
    uint8_t onboardingDone;
    dbType_Date_t localDate;
    RtcTime_t time;

    commitKey = keyId;
    KBH_ClearAllKeyProperties();
    switch (commitKey)
    {
        case KEY_DONE:
            localDate.Year = DateTime_Year;
            localDate.Month = DateTime_Month;
            localDate.Day = DateTime_Day;
            localDate.DayOfWeek = DateTime_DayOfWeek;
            time.Hours = DateTime_MinutesOfDay / 60;
            time.Minutes = DateTime_MinutesOfDay % 60;
            time.Seconds = 0;
            DBTHRD_SetData(DBTYPE_DATE,(void*)&localDate, INDEX_DONT_CARE, DBCHANGE_LOCAL);
            DBTHRD_SetData(DBTYPE_LOCALTIME,(void*)&time, INDEX_DONT_CARE, DBCHANGE_LOCAL);
            break;

        default:
            break;
    }

    DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);

    switch (commitKey)
    {
        case KEY_BACK:
        case KEY_DONE:
            if (onboardingDone == 1)
            {
                if (previousMenu == MENU_HOME)
                {
                    MNH_SetActiveMenu(MENU_HOME);
                }
                else
                {
                    MNH_SetActiveMenu(MENU_ADV_SETTINGS);
                }
            }
            else
            {
                MNH_SetActiveMenu(MENU_UNIT_FORMAT);
            }
            break;

        default:
            MNH_SetActiveMenu(MENU_HOME);
            break;
    }
}

/** Copyright(C) 2019 Stelpro Design, All Rights Reserved**/
