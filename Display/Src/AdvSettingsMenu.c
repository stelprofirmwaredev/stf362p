/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       AdvSettingsMenu.c

    \brief      Source code file for the AdvSettingsMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the AdvSettingsMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "AdvSettingsMenu.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/


/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   AdvSettingsEditState = EDIT_IDLE;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/
    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/

/*******************************************************************************
    Private functions definitions
*******************************************************************************/

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_AdvSettings_Entry
*******************************************************************************/
void MNU_AdvSettings_Entry(void)
{
    AdvSettingsEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show Root Settings
    //
    DPH_DisplayRootSettings();

	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_AdvSettings_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_AdvSettings_Core

    Allow scrolling into advanced settings menus
*******************************************************************************/
void MNU_AdvSettings_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure key in the Advanced settings menu
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_AdvSettings_Commit, (void*)KEY_BACK);
        KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)MNU_AdvSettings_Commit, (void*)KEY_OPTIONS_2);
        KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)MNU_AdvSettings_Commit, (void*)KEY_OPTIONS_3);
        KBH_SetKeyPressProperties(KEY_OPTIONS_4, (FUNC)MNU_AdvSettings_Commit, (void*)KEY_OPTIONS_4);
        KBH_SetKeyPressProperties(KEY_OPTIONS_5, (FUNC)MNU_AdvSettings_Commit, (void*)KEY_OPTIONS_5);
        KBH_SetKeyPressProperties(KEY_OPTIONS_6, (FUNC)MNU_AdvSettings_Commit, (void*)KEY_OPTIONS_6);
        KBH_SetKeyPressProperties(KEY_OPTIONS_7, (FUNC)MNU_AdvSettings_Commit, (void*)KEY_OPTIONS_7);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_AdvSettings_Exit
*******************************************************************************/
void MNU_AdvSettings_Exit(void)
{
    AdvSettingsEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_AdvSettings_Commit
*******************************************************************************/
void MNU_AdvSettings_Commit(KEYB_PARAM keyId)
{
	if (AdvSettingsEditState == EDIT_IN_PROGRESS)
	{
        KBH_ClearAllKeyProperties();
        switch(keyId)
        {
            case KEY_BACK:
                MNH_SetActiveMenu(MENU_HOME);
                break;

            case KEY_OPTIONS_2:
                MNH_SetActiveMenu(MENU_DATE_TIME);
                break;

            case KEY_OPTIONS_3:
                MNH_SetActiveMenu(MENU_MODE_SELECTION);
                break;

            case KEY_OPTIONS_4:
                MNH_SetActiveMenu(MENU_OPTIONS);
                break;

            case KEY_OPTIONS_5:
                MNH_SetActiveMenu(MENU_ALERTS);
                break;

            case KEY_OPTIONS_6:
                MNH_SetActiveMenu(MENU_RESET);
                break;

            case KEY_OPTIONS_7:
                MNH_SetActiveMenu(MENU_INFO);
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
		AdvSettingsEditState = EDIT_IDLE;
	}
}
