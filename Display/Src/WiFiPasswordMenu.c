/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       WiFiPasswordMenu.c

    \brief      Source code file for the WiFiPasswordMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the WiFiPasswordMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "WiFiPasswordMenu.h"
#include "WiFiScanMenu.h"

#include "StringObjects.h"
#include "images.h"
//#include "stm32f429i_discovery_lcd.h"
#include ".\HAL\inc\Display.h"
#include ".\WiFi\inc\THREAD_WiFi.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/


/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;


/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e       WiFiPasswordEditState = EDIT_IDLE;
static KbhKeyboardPage_e    Page;
static U8BIT                Password[33];

/*******************************************************************************
    External functions prototypes
*******************************************************************************/
extern void WiFiJoin_Callback (WiFiJoinStatus_t status);

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void ManageKeyboard(void);
static void AddCharacter(U8BIT character);
static void RemoveLastCharacter(void);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    Manage keyboard touch

    Add a character to the password string
*******************************************************************************/
#pragma optimize = medium   //optimization decreased, otherwise the password is corrupted
static void ManageKeyboard(void)
{
    TS_StateTypeDef touchstruct;
    KbhKeyboard_e keyId;

    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_WiFiPassword_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    touchstruct = HAL_GetTouch();
    keyId = Keyb_DetectKeyboardTouchZone(touchstruct, Page);
    switch (keyId)
    {
        case KEY_ROW1_COL1:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('Q');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('q');
                    break;

                case KEY_PAGE_NUMBER:
                case KEY_PAGE_SYMBOL:
                    AddCharacter('1');
                    break;

                default:
                    break;
            }
            break;

        case KEY_ROW1_COL2:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('W');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('w');
                    break;

                case KEY_PAGE_NUMBER:
                case KEY_PAGE_SYMBOL:
                    AddCharacter('2');
                    break;

                default:
                    break;
            }
            break;

        case KEY_ROW1_COL3:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('E');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('e');
                    break;

                case KEY_PAGE_NUMBER:
                case KEY_PAGE_SYMBOL:
                    AddCharacter('3');
                    break;

                default:
                    break;
            }
            break;

        case KEY_ROW1_COL4:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('R');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('r');
                    break;

                case KEY_PAGE_NUMBER:
                case KEY_PAGE_SYMBOL:
                    AddCharacter('4');
                    break;

                default:
                    break;
            }
            break;

        case KEY_ROW1_COL5:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('T');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('t');
                    break;

                case KEY_PAGE_NUMBER:
                case KEY_PAGE_SYMBOL:
                    AddCharacter('5');
                    break;

                default:
                    break;
            }
            break;

        case KEY_ROW1_COL6:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('Y');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('y');
                    break;

                case KEY_PAGE_NUMBER:
                case KEY_PAGE_SYMBOL:
                    AddCharacter('6');
                    break;

                default:
                    break;
            }
            break;

        case KEY_ROW1_COL7:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('U');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('u');
                    break;

                case KEY_PAGE_NUMBER:
                case KEY_PAGE_SYMBOL:
                    AddCharacter('7');
                    break;

                default:
                    break;
            }
            break;

        case KEY_ROW1_COL8:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('I');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('i');
                    break;

                case KEY_PAGE_NUMBER:
                case KEY_PAGE_SYMBOL:
                    AddCharacter('8');
                    break;

                default:
                    break;
            }
            break;

        case KEY_ROW1_COL9:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('O');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('o');
                    break;

                case KEY_PAGE_NUMBER:
                case KEY_PAGE_SYMBOL:
                    AddCharacter('9');
                    break;

                default:
                    break;
            }
            break;

        case KEY_ROW1_COL10:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('P');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('p');
                    break;

                case KEY_PAGE_NUMBER:
                case KEY_PAGE_SYMBOL:
                    AddCharacter('0');
                    break;

                default:
                    break;
            }
            break;

        case KEY_ROW2_COL1:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('A');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('a');
                    break;

                case KEY_PAGE_NUMBER:
                AddCharacter('#');
                    break;

                case KEY_PAGE_SYMBOL:
                    AddCharacter('|');
                    break;

                default:
                    break;
            }
            break;

        case KEY_ROW2_COL2:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('S');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('s');
                    break;

                case KEY_PAGE_NUMBER:
                    AddCharacter('!');
                    break;

                case KEY_PAGE_SYMBOL:
                    AddCharacter(':');
                    break;

                default:
                    break;
            }
            break;

        case KEY_ROW2_COL3:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('D');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('d');
                    break;

                case KEY_PAGE_NUMBER:
                AddCharacter('"');
                    break;

                case KEY_PAGE_SYMBOL:
                    AddCharacter(';');
                    break;

                default:
                    break;
            }
            break;

        case KEY_ROW2_COL4:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('F');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('f');
                    break;

                case KEY_PAGE_NUMBER:
                    AddCharacter('/');
                    break;

                case KEY_PAGE_SYMBOL:
                    AddCharacter('.');
                    break;

                default:
                    break;
            }
            break;

        case KEY_ROW2_COL5:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('G');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('g');
                    break;

                case KEY_PAGE_NUMBER:
                    AddCharacter('$');
                    break;

                case KEY_PAGE_SYMBOL:
                    AddCharacter(',');
                    break;

                default:
                    break;
            }
            break;

        case KEY_ROW2_COL6:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('H');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('h');
                    break;

                case KEY_PAGE_NUMBER:
                    AddCharacter('%');
                    break;

                case KEY_PAGE_SYMBOL:
                    AddCharacter('\'');
                    break;

                default:
                    break;
            }
            break;

        case KEY_ROW2_COL7:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('J');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('j');
                    break;

                case KEY_PAGE_NUMBER:
                    AddCharacter('?');
                    break;

                case KEY_PAGE_SYMBOL:
                    AddCharacter('`');
                    break;

                default:
                    break;
            }
            break;

        case KEY_ROW2_COL8:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('K');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('k');
                    break;

                case KEY_PAGE_NUMBER:
                    AddCharacter('&');
                    break;

                case KEY_PAGE_SYMBOL:
                    AddCharacter('\\');
                    break;

                default:
                    break;
            }
            break;

        case KEY_ROW2_COL9:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('L');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('l');
                    break;

                case KEY_PAGE_NUMBER:
                    AddCharacter('*');
                    break;

                case KEY_PAGE_SYMBOL:
                    AddCharacter('~');
                    break;

                default:
                    break;
            }
            break;

        case KEY_ROW3_COL1:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('Z');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('z');
                    break;

                case KEY_PAGE_NUMBER:
                    AddCharacter('(');
                    break;

                case KEY_PAGE_SYMBOL:
                    AddCharacter('[');
                    break;

                default:
                    break;
            }
            break;

        case KEY_ROW3_COL2:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('X');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('x');
                    break;

                case KEY_PAGE_NUMBER:
                    AddCharacter(')');
                    break;

                case KEY_PAGE_SYMBOL:
                    AddCharacter(']');
                    break;

                default:
                    break;
            }
            break;

        case KEY_ROW3_COL3:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('C');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('c');
                    break;

                case KEY_PAGE_NUMBER:
                    AddCharacter('-');
                    break;

                case KEY_PAGE_SYMBOL:
                    AddCharacter('{');
                    break;

                default:
                    break;
            }
            break;

        case KEY_ROW3_COL4:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('V');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('v');
                    break;

                case KEY_PAGE_NUMBER:
                    AddCharacter('_');
                    break;

                case KEY_PAGE_SYMBOL:
                    AddCharacter('}');
                    break;

                default:
                    break;
            }
            break;

        case KEY_ROW3_COL5:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('B');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('b');
                    break;

                case KEY_PAGE_NUMBER:
                    AddCharacter('+');
                    break;

                case KEY_PAGE_SYMBOL:
                    AddCharacter('<');
                    break;

                default:
                    break;
            }
            break;

        case KEY_ROW3_COL6:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('N');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('n');
                    break;

                case KEY_PAGE_NUMBER:
                    AddCharacter('=');
                    break;

                case KEY_PAGE_SYMBOL:
                    AddCharacter('>');
                    break;

                default:
                    break;
            }
            break;

        case KEY_ROW3_COL7:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    AddCharacter('M');
                    break;

                case KEY_PAGE_LOWER:
                    AddCharacter('m');
                    break;

                case KEY_PAGE_NUMBER:
                    AddCharacter('@');
                    break;

                case KEY_PAGE_SYMBOL:
                    AddCharacter('^');
                    break;

                default:
                    break;
            }
            break;

        case KEY_SPACE:
            AddCharacter(' ');
            break;

        case KEY_CAP_SYMBOL:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                    Page = KEY_PAGE_LOWER;
                    break;

                case KEY_PAGE_LOWER:
                    Page = KEY_PAGE_CAPITAL;
                    break;

                case KEY_PAGE_NUMBER:
                    Page = KEY_PAGE_SYMBOL;
                    break;

                case KEY_PAGE_SYMBOL:
                    Page = KEY_PAGE_NUMBER;
                    break;

                default:
                    break;
            }
            DPH_DisplayKeyboard(Page);
            break;

        case KEY_BACKSPACE:
            RemoveLastCharacter();
            break;

        case KEY_LETTER:
            switch (Page)
            {
                case KEY_PAGE_NUMBER:
                case KEY_PAGE_SYMBOL:
                    Page = KEY_PAGE_LOWER;
                    break;

                default:
                    break;
            }
            DPH_DisplayKeyboard(Page);
            break;

        case KEY_NUMBER:
            switch (Page)
            {
                case KEY_PAGE_CAPITAL:
                case KEY_PAGE_LOWER:
                    Page = KEY_PAGE_NUMBER;
                    break;

                default:
                    break;
            }
            DPH_DisplayKeyboard(Page);
            break;

        default:
            break;
    }
    //
    //  Display the keyboard
    //
    DPH_DisplayKeyboard(Page);

    //
    //  Display the password
    //
    DPH_DisplayPassword(&Password[0], 0);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();
}

/*******************************************************************************
    AddCharacter

    Add a character to the password string
*******************************************************************************/
static void AddCharacter(U8BIT character)
{
    if (STR_StrLen(Password) < sizeof(Password))
    {
        STR_StrCat(Password, &character);
    }
}

/*******************************************************************************
    RemoveLastCharacter

    Remove the last character of the password string
*******************************************************************************/
static void RemoveLastCharacter(void)
{
    STR_StrNCpy(&Password[0], &Password[0], (STR_StrLen(&Password[0]) - 1));
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_WiFiPassword_Entry
*******************************************************************************/
void MNU_WiFiPassword_Entry(void)
{
    WiFiPasswordEditState = EDIT_IN_PROGRESS;
    Page = KEY_PAGE_CAPITAL;
    Password[0] = 0;

    //
    //  Display the keyboard
    //
    DPH_DisplayKeyboard(Page);

    //
    //  Display the password
    //
    DPH_DisplayPassword(&Password[0], 1);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    MNU_SetKeyAssigned(FALSE);

	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_WiFiPassword_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_WiFiPassword_Core

    Enter the Wi-Fi password
*******************************************************************************/
void MNU_WiFiPassword_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_WiFiPassword_Commit, (void*)KEY_BACK);
        KBH_SetKeyPressProperties(KEY_DONE, (FUNC)MNU_WiFiPassword_Commit, (void*)KEY_DONE);
        KBH_SetKeyPressProperties(KEY_KEYBOARD, (FUNC)ManageKeyboard, (void*)NULL);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_WiFiPassword_Exit
*******************************************************************************/
void MNU_WiFiPassword_Exit(void)
{
    WiFiPasswordEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_WiFiPassword_Commit
*******************************************************************************/
void MNU_WiFiPassword_Commit(KEYB_PARAM keyId)
{
    U8BIT scanNumber;

	if (WiFiPasswordEditState == EDIT_IN_PROGRESS)
	{
        KBH_ClearAllKeyProperties();
        switch(keyId)
        {
            case KEY_BACK:
                MNH_SetActiveMenu(MENU_SCAN);
                break;

            case KEY_DONE:
                scanNumber = MNU_WiFiScan_GetSelectedSSIDNumber();
                WIFITHRD_JoinNetwork (scanNumber, &Password[0], WiFiJoin_Callback);
                MNH_SetActiveMenu(MENU_WIFI_JOIN);
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
		WiFiPasswordEditState = EDIT_IDLE;
	}
}
