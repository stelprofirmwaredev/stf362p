/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2019, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    ProgramScheduleMenu.c
* @date    2019/07/29
* @authors Jean-Fran�ois Many
* @brief   Program Schedule Menu
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "KeyboardHandler.h"
#include "MenuTable.h"
#include "MenuHandlerIf.h"
#include "APP_Display.h"
#include ".\DB\inc\THREAD_DB.h"
#include "ModeSelectionMenu.h"
#include "ProgramScheduleMenu.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_CONFIRM,
    EDIT_COMMITTING
} MnuEditState_e;

#define NO_EDIT 2000
#define NO_SELECT 0xFF

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
static MnuEditState_e   ProgramScheduleEditState = EDIT_IDLE;
static uint16_t ProgramScheduleEditValue;
static Program_t ProgramToEdit;
static PgmSchedule_t ScheduleToEdit;
static uint8_t ScheduleSelected[7];
static uint8_t firstSelectionMade;
static uint8_t menuChangePending;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static void MNU_ProgramSchedule_Toggle(KEYB_PARAM keyId);
static void MNU_ProgramSchedule_Select(KEYB_PARAM keyId);


/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
static void MNU_ProgramSchedule_Toggle(KEYB_PARAM keyId)
{
    uint8_t onboardingDone;

    switch (keyId)
    {
        case KEY_OPTIONS_1:
            ScheduleToEdit.Day[0].ScheduleEnabled ^= 1;
            ScheduleSelected[0] = 0;
            break;

        case KEY_OPTIONS_2:
             ScheduleToEdit.Day[1].ScheduleEnabled ^= 1;
             ScheduleSelected[1] = 0;
            break;

        case KEY_OPTIONS_3:
             ScheduleToEdit.Day[2].ScheduleEnabled ^= 1;
             ScheduleSelected[2] = 0;
            break;

        case KEY_OPTIONS_4:
             ScheduleToEdit.Day[3].ScheduleEnabled ^= 1;
             ScheduleSelected[3] = 0;
            break;

        case KEY_OPTIONS_5:
             ScheduleToEdit.Day[4].ScheduleEnabled ^= 1;
             ScheduleSelected[4] = 0;
            break;

        case KEY_OPTIONS_6:
             ScheduleToEdit.Day[5].ScheduleEnabled ^= 1;
             ScheduleSelected[5] = 0;
            break;

        case KEY_OPTIONS_7:
             ScheduleToEdit.Day[6].ScheduleEnabled ^= 1;
             ScheduleSelected[6] = 0;
            break;
    }
    menuChangePending = 1;
    MNU_SetKeyAssigned(FALSE);
    DPH_DisplayProgramSchedule(FALSE, ProgramScheduleEditValue, ProgramToEdit, ScheduleSelected, &ScheduleToEdit, (CHANGE_TOGGLE | CHANGE_SELECT), menuChangePending);

    DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);
    if (onboardingDone == 1)
    {
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_ProgramSchedule_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
static void MNU_ProgramSchedule_Select(KEYB_PARAM keyId)
{
    uint8_t onboardingDone;

    if (firstSelectionMade == NO_SELECT)
    {
        firstSelectionMade = keyId - KEY_OPTIONS_8;
        ProgramScheduleEditValue = ScheduleToEdit.Day[firstSelectionMade].ScheduleTime;
    }
    switch (keyId)
    {
        case KEY_OPTIONS_8:
            ScheduleSelected[0] ^= 1;
            break;

        case KEY_OPTIONS_9:
            ScheduleSelected[1] ^= 1;
            break;

        case KEY_OPTIONS_10:
            ScheduleSelected[2] ^= 1;
            break;

        case KEY_OPTIONS_11:
            ScheduleSelected[3] ^= 1;
            break;

        case KEY_OPTIONS_12:
            ScheduleSelected[4] ^= 1;
            break;

        case KEY_OPTIONS_13:
            ScheduleSelected[5] ^= 1;
            break;

        case KEY_OPTIONS_14:
            ScheduleSelected[6] ^= 1;
            break;
    }
    menuChangePending = 1;
    MNU_SetKeyAssigned(FALSE);
    DPH_DisplayProgramSchedule(FALSE, ProgramScheduleEditValue, ProgramToEdit, ScheduleSelected, &ScheduleToEdit, (CHANGE_EDIT | CHANGE_SELECT), menuChangePending);

    DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);
    if (onboardingDone == 1)
    {
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_ProgramSchedule_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    }
}


/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
void MNU_ProgramSchedule_Entry(void)
{
    ProgramScheduleEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);

    for (uint8_t i = 0; i < 7; i++)
    {
        ScheduleSelected[i] = 0;
    }
    firstSelectionMade = NO_SELECT;
    ProgramScheduleEditValue = NO_EDIT;
    menuChangePending = 0;

    ProgramToEdit = MNU_GetSelectedProgram();
    switch (ProgramToEdit)
    {
        case PGM_WAKE:
            DBTHRD_GetData(DBTYPE_WAKE_SCHEDULE,(void*)&ScheduleToEdit, THIS_THERMOSTAT);
            break;

        case PGM_LEAVE:
            DBTHRD_GetData(DBTYPE_LEAVE_SCHEDULE,(void*)&ScheduleToEdit, THIS_THERMOSTAT);
            break;

        case PGM_RETURN:
            DBTHRD_GetData(DBTYPE_RETURN_SCHEDULE,(void*)&ScheduleToEdit, THIS_THERMOSTAT);
            break;

        case PGM_SLEEP:
            DBTHRD_GetData(DBTYPE_SLEEP_SCHEDULE,(void*)&ScheduleToEdit, THIS_THERMOSTAT);
            break;

        default:
            break;
    }

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show Program Schedule
    //
    DPH_DisplayProgramSchedule(TRUE, ProgramScheduleEditValue, ProgramToEdit, ScheduleSelected, &ScheduleToEdit, (CHANGE_EDIT | CHANGE_TOGGLE | CHANGE_SELECT), menuChangePending);

	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_ProgramSchedule_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
void MNU_ProgramSchedule_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_ProgramSchedule_Commit, (void*)KEY_BACK);
        KBH_SetKeyPressProperties(KEY_DONE, (FUNC)MNU_ProgramSchedule_Commit, (void*)KEY_DONE);
        if (ProgramScheduleEditValue != NO_EDIT)
        {
            KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_ProgramSchedule_DecProgramScheduleStartTurbo, (int16u*) 15, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
            KBH_SetKeyHoldProperties(KEY_UP, (FUNC)MNU_ProgramSchedule_IncProgramScheduleStartTurbo, (int16u*) 15, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
            KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_ProgramSchedule_IncProgramSchedule, (int16u*)15);
            KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_ProgramSchedule_DecProgramSchedule, (int16u*)15);
        }
        else
        {
            KBH_SetKeyHoldProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
            KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);
            KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)NULL, (int16u*)NULL);
            KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)NULL, (int16u*)NULL);
        }
        KBH_SetKeyPressProperties(KEY_OPTIONS_1, (FUNC)MNU_ProgramSchedule_Toggle, (void*)KEY_OPTIONS_1);
        KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)MNU_ProgramSchedule_Toggle, (void*)KEY_OPTIONS_2);
        KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)MNU_ProgramSchedule_Toggle, (void*)KEY_OPTIONS_3);
        KBH_SetKeyPressProperties(KEY_OPTIONS_4, (FUNC)MNU_ProgramSchedule_Toggle, (void*)KEY_OPTIONS_4);
        KBH_SetKeyPressProperties(KEY_OPTIONS_5, (FUNC)MNU_ProgramSchedule_Toggle, (void*)KEY_OPTIONS_5);
        KBH_SetKeyPressProperties(KEY_OPTIONS_6, (FUNC)MNU_ProgramSchedule_Toggle, (void*)KEY_OPTIONS_6);
        KBH_SetKeyPressProperties(KEY_OPTIONS_7, (FUNC)MNU_ProgramSchedule_Toggle, (void*)KEY_OPTIONS_7);
        if (ScheduleToEdit.Day[0].ScheduleEnabled != 0)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_8, (FUNC)MNU_ProgramSchedule_Select, (void*)KEY_OPTIONS_8);
        }
        else
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_8, (FUNC)NULL, (void*)NULL);
        }
        if (ScheduleToEdit.Day[1].ScheduleEnabled != 0)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_9, (FUNC)MNU_ProgramSchedule_Select, (void*)KEY_OPTIONS_9);
        }
        else
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_9, (FUNC)NULL, (void*)NULL);
        }
        if (ScheduleToEdit.Day[2].ScheduleEnabled != 0)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_10, (FUNC)MNU_ProgramSchedule_Select, (void*)KEY_OPTIONS_10);
        }
        else
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_10, (FUNC)NULL, (void*)NULL);
        }
        if (ScheduleToEdit.Day[3].ScheduleEnabled != 0)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_11, (FUNC)MNU_ProgramSchedule_Select, (void*)KEY_OPTIONS_11);
        }
        else
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_11, (FUNC)NULL, (void*)NULL);
        }
        if (ScheduleToEdit.Day[4].ScheduleEnabled != 0)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_12, (FUNC)MNU_ProgramSchedule_Select, (void*)KEY_OPTIONS_12);
        }
        else
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_12, (FUNC)NULL, (void*)NULL);
        }
        if (ScheduleToEdit.Day[5].ScheduleEnabled != 0)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_13, (FUNC)MNU_ProgramSchedule_Select, (void*)KEY_OPTIONS_13);
        }
        else
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_13, (FUNC)NULL, (void*)NULL);
        }
        if (ScheduleToEdit.Day[6].ScheduleEnabled != 0)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_14, (FUNC)MNU_ProgramSchedule_Select, (void*)KEY_OPTIONS_14);
        }
        else
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_14, (FUNC)NULL, (void*)NULL);
        }

        // Stop key repeat monitoring
        KBH_SetKeyRepeatProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
        KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
void MNU_ProgramSchedule_Exit(void)
{
    ProgramScheduleEditState = EDIT_IDLE;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
void MNU_ProgramSchedule_Commit(KEYB_PARAM keyId)
{
    static KEYB_PARAM commitKey = NULL;

    if ( ProgramScheduleEditState == EDIT_IN_PROGRESS )
    {
        commitKey = keyId;
        KBH_ClearAllKeyProperties();
        switch (commitKey)
        {
            case KEY_DONE:
                //Apply edited time to selected programs
                if ((ProgramScheduleEditValue != NO_EDIT) && (firstSelectionMade != NO_SELECT))
                {
                    for (uint8_t day = 0; day < 7; day++)
                    {
                        if (ScheduleSelected[day] != 0)
                        {
                            ScheduleToEdit.Day[day].ScheduleTime = ProgramScheduleEditValue;
                        }
                    }
                }
                //Save schedule
                switch (ProgramToEdit)
                {
                    case PGM_WAKE:
                        DBTHRD_SetData(DBTYPE_WAKE_SCHEDULE,(void*)&ScheduleToEdit, THIS_THERMOSTAT, DBCHANGE_LOCAL);
                        break;

                    case PGM_LEAVE:
                        DBTHRD_SetData(DBTYPE_LEAVE_SCHEDULE,(void*)&ScheduleToEdit, THIS_THERMOSTAT, DBCHANGE_LOCAL);
                        break;

                    case PGM_RETURN:
                        DBTHRD_SetData(DBTYPE_RETURN_SCHEDULE,(void*)&ScheduleToEdit, THIS_THERMOSTAT, DBCHANGE_LOCAL);
                        break;

                    case PGM_SLEEP:
                        DBTHRD_SetData(DBTYPE_SLEEP_SCHEDULE,(void*)&ScheduleToEdit, THIS_THERMOSTAT, DBCHANGE_LOCAL);
                        break;

                    default:
                        break;
                }
                //No break on purpose

            case KEY_BACK:
            default:
                ProgramScheduleEditState = EDIT_COMMITTING;
                MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_ProgramSchedule_Commit, (KEYB_PARAM*)commitKey, IMMEDIATELY);
                break;
        }
    }
    else if ( ProgramScheduleEditState == EDIT_COMMITTING )
    {
        ProgramScheduleEditState = EDIT_IDLE;
        switch (commitKey)
        {
            case KEY_BACK:
                MNH_SetActiveMenu(MENU_PROGRAM_SETPOINT);
                break;

            case KEY_DONE:
                MNU_ProgramSchedule_Entry();
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
void MNU_ProgramSchedule_IncProgramSchedule(int16u value)
{
    uint8_t onboardingDone;

    ProgramScheduleEditValue += value;
    if (ProgramScheduleEditValue >= 1440)
    {
        ProgramScheduleEditValue = 0;
    }
    menuChangePending = 1;
    DPH_DisplayProgramSchedule(FALSE, ProgramScheduleEditValue, ProgramToEdit, ScheduleSelected, &ScheduleToEdit, (CHANGE_EDIT), menuChangePending);

    DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);
    if (onboardingDone == 1)
    {
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_ProgramSchedule_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
void MNU_ProgramSchedule_DecProgramSchedule(int16u value)
{
    uint8_t onboardingDone;

    if (ProgramScheduleEditValue >= value)
    {
        ProgramScheduleEditValue -= value;
    }
    else
    {
        ProgramScheduleEditValue = 1425;
    }
    menuChangePending = 1;
    DPH_DisplayProgramSchedule(FALSE, ProgramScheduleEditValue, ProgramToEdit, ScheduleSelected, &ScheduleToEdit, (CHANGE_EDIT), menuChangePending);

    DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);
    if (onboardingDone == 1)
    {
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_ProgramSchedule_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
void MNU_ProgramSchedule_DecProgramScheduleStartTurbo(void)
{
    KBH_SetKeyHoldProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

    // Decrement the program schedule
    MNU_ProgramSchedule_DecProgramSchedule(15);
    // Turn on key repeat notification
    KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)MNU_ProgramSchedule_DecProgramSchedule, (int16u*) 15, OS_MSTOTICK(KEYB_REPEAT_DELAY));

    KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_ProgramSchedule_ProgramScheduleStopTurbo, (void*)NULL);
    KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_ProgramSchedule_ProgramScheduleStopTurbo, (void*)NULL);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
void MNU_ProgramSchedule_IncProgramScheduleStartTurbo(void)
{
    KBH_SetKeyHoldProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

    // Increment the program schedule
    MNU_ProgramSchedule_IncProgramSchedule(15);
    // Turn on key repeat notification
    KBH_SetKeyRepeatProperties(KEY_UP, (FUNC)MNU_ProgramSchedule_IncProgramSchedule, (int16u*) 15, OS_MSTOTICK(KEYB_REPEAT_DELAY));

    KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_ProgramSchedule_ProgramScheduleStopTurbo, (void*)NULL);
    KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_ProgramSchedule_ProgramScheduleStopTurbo, (void*)NULL);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
void MNU_ProgramSchedule_ProgramScheduleStopTurbo(void)
{
    // Stop key repeat monitoring
	KBH_SetKeyRepeatProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

    // Start key hold monitoring
	KBH_SetKeyHoldProperties(KEY_UP, (FUNC)MNU_ProgramSchedule_IncProgramScheduleStartTurbo, (void*) NULL, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_ProgramSchedule_DecProgramScheduleStartTurbo, (void*) NULL, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
	KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_ProgramSchedule_IncProgramSchedule, (int16u*) 15);
    KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_ProgramSchedule_DecProgramSchedule, (int16u*) 15);
}


/** Copyright(C) 2019 Stelpro Design, All Rights Reserved**/
