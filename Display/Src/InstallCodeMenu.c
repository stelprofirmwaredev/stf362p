/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       InstallCodeMenu.c

    \brief      Source code file for the InstallCodeMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the InstallCodeMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "InstallCodeMenu.h"

#include "StringObjects.h"
#include ".\HAL\inc\Display.h"
#include <string.h>

/*******************************************************************************
    Private constants definitions
*******************************************************************************/


/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;


/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e       InstallCodeEditState = EDIT_IDLE;
static KbhKeyboardPage_e    Page;
static U8BIT                InstallCode[37];

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void ManageKeyboard(void);
static void AddCharacter(U8BIT character);
static void RemoveLastInstallCodeCharacter(void);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    Manage keyboard touch

    Add a character to the password string
*******************************************************************************/
static void ManageKeyboard(void)
{
    TS_StateTypeDef touchstruct;
    KbhKeyboard_e keyId;

    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_InstallCode_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    touchstruct = HAL_GetTouch();
    keyId = Keyb_DetectKeyboardTouchZone(touchstruct, Page);
    switch (keyId)
    {
        case KEY_ROW1_COL1:
            AddCharacter('1');
            break;

        case KEY_ROW1_COL2:
            AddCharacter('2');
            break;

        case KEY_ROW1_COL3:
            AddCharacter('3');
            break;

        case KEY_ROW1_COL4:
            AddCharacter('4');
            break;

        case KEY_ROW1_COL5:
            AddCharacter('5');
            break;

        case KEY_ROW1_COL6:
            AddCharacter('6');
            break;

        case KEY_ROW1_COL7:
            AddCharacter('7');
            break;

        case KEY_ROW1_COL8:
            AddCharacter('8');
            break;

        case KEY_ROW1_COL9:
            AddCharacter('9');
            break;

        case KEY_ROW1_COL10:
            AddCharacter('0');
            break;

        case KEY_ROW3_COL1:
            AddCharacter('a');
            break;

        case KEY_ROW3_COL2:
            AddCharacter('b');
            break;

        case KEY_ROW3_COL3:
            AddCharacter('c');
            break;

        case KEY_ROW3_COL4:
            AddCharacter('d');
            break;

        case KEY_ROW3_COL5:
            AddCharacter('e');
            break;

        case KEY_ROW3_COL6:
            AddCharacter('f');
            break;

        case KEY_BACKSPACE:
            RemoveLastInstallCodeCharacter();
            break;

        default:
            break;
    }
    //
    //  Display the keyboard
    //
    DPH_DisplayKeyboard(Page);

    //
    //  Display the install code
    //
    DPH_DisplayInstallCode(&InstallCode[0], 0);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();
}

/*******************************************************************************
    AddCharacter

    Add a character to the install code string
*******************************************************************************/
static void AddCharacter(U8BIT character)
{
    if (STR_StrLen(InstallCode) < (sizeof(InstallCode) - 1))
    {
        STR_StrCat(InstallCode, &character);
        if (STR_StrLen(InstallCode) == (sizeof(InstallCode) - 1))
        {
            KBH_SetKeyPressProperties(KEY_DONE, (FUNC)MNU_InstallCode_Commit, (void*)KEY_DONE);
        }
    }
}

/*******************************************************************************
    RemoveLastInstallCodeCharacter

    Remove the last character of the install code string
*******************************************************************************/
static void RemoveLastInstallCodeCharacter(void)
{
    STR_StrNCpy(&InstallCode[0], &InstallCode[0], (STR_StrLen(&InstallCode[0]) - 1));
    KBH_SetKeyPressProperties(KEY_DONE, (FUNC)NULL, (void*)KEY_DONE);
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_InstallCode_Entry
*******************************************************************************/
void MNU_InstallCode_Entry(void)
{
    InstallCodeEditState = EDIT_IN_PROGRESS;
    Page = KEY_PAGE_0_F;
    InstallCode[0] = 0;

    //
    //  Display the keyboard
    //
    DPH_DisplayKeyboard(Page);

    //
    //  Display the install code
    //
    DPH_DisplayInstallCode(&InstallCode[0], 1);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    MNU_SetKeyAssigned(FALSE);

	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_InstallCode_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_InstallCode_Core

    Enter the Install Code
*******************************************************************************/
void MNU_InstallCode_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_InstallCode_Commit, (void*)KEY_BACK);
        KBH_SetKeyPressProperties(KEY_KEYBOARD, (FUNC)ManageKeyboard, (void*)NULL);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_InstallCode_Exit
*******************************************************************************/
void MNU_InstallCode_Exit(void)
{
    InstallCodeEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_InstallCode_Commit
*******************************************************************************/
void MNU_InstallCode_Commit(KEYB_PARAM keyId)
{
	if (InstallCodeEditState == EDIT_IN_PROGRESS)
	{
        KBH_ClearAllKeyProperties();
        switch(keyId)
        {
            case KEY_BACK:
                MNH_SetActiveMenu(MENU_ZIGBEE_SETUP);
                break;

            case KEY_DONE:
                MNH_SetActiveMenu(MENU_MAC_ADDRESS);
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
		InstallCodeEditState = EDIT_IDLE;
	}
}

uint8_t* GetInstallCode()
{
//    memcpy(&InstallCode[0], "0f080f0000050f0f0f0f0f04080205008f25", 36);
    return &InstallCode[0];
}