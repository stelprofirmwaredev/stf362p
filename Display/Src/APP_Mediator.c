/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2015, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    APP_Mediator.c
* @date    2016/07/14
* @authors Jean-Fran�ois Many
* @brief   Mediator Module
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include "APP_SetpointManager.h"
#include "MenuTable.h"
#include "MenuHandlerIf.h"
#include "HomeMenu.h"
#include "BacklightMenu.h"
#include "AlertsMenu.h"
#include ".\HAL\inc\Display.h"
#include ".\DB\inc\THREAD_DB.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/


/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/


/*******************************************************************************
*    Public functions definitions
*******************************************************************************/














/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
