/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       MACAddressMenu.c

    \brief      Source code file for the MACAddressMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the MACAddressMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "MACAddressMenu.h"

#include "StringObjects.h"
#include ".\HAL\inc\Display.h"
#include ".\ZigBee\inc\THREAD_ZigBee.h"
#include <string.h>
#include ".\DB\inc\THREAD_DB.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/


/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;


/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e       MACAddressEditState = EDIT_IDLE;
static KbhKeyboardPage_e    Page;
static U8BIT                MACAddress[17];

static susbscriptionHandle_t hInstallCodeFailedNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hInstallCodeSuccessNotification = DBTHRD_NOTIFHANDLE_NOTSET;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void ManageKeyboard(void);
static void AddCharacter(U8BIT character);
static void RemoveLastMACAddressCharacter(void);
DB_NOTIFICATION(InstallCodeFailed_Callback);
DB_NOTIFICATION(InstallCodeSuccess_Callback);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    Manage keyboard touch

    Add a character to the password string
*******************************************************************************/
static void ManageKeyboard(void)
{
    TS_StateTypeDef touchstruct;
    KbhKeyboard_e keyId;

    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_MACAddress_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    touchstruct = HAL_GetTouch();
    keyId = Keyb_DetectKeyboardTouchZone(touchstruct, Page);
    switch (keyId)
    {
        case KEY_ROW1_COL1:
            AddCharacter('1');
            break;

        case KEY_ROW1_COL2:
            AddCharacter('2');
            break;

        case KEY_ROW1_COL3:
            AddCharacter('3');
            break;

        case KEY_ROW1_COL4:
            AddCharacter('4');
            break;

        case KEY_ROW1_COL5:
            AddCharacter('5');
            break;

        case KEY_ROW1_COL6:
            AddCharacter('6');
            break;

        case KEY_ROW1_COL7:
            AddCharacter('7');
            break;

        case KEY_ROW1_COL8:
            AddCharacter('8');
            break;

        case KEY_ROW1_COL9:
            AddCharacter('9');
            break;

        case KEY_ROW1_COL10:
            AddCharacter('0');
            break;

        case KEY_ROW3_COL1:
            AddCharacter('a');
            break;

        case KEY_ROW3_COL2:
            AddCharacter('b');
            break;

        case KEY_ROW3_COL3:
            AddCharacter('c');
            break;

        case KEY_ROW3_COL4:
            AddCharacter('d');
            break;

        case KEY_ROW3_COL5:
            AddCharacter('e');
            break;

        case KEY_ROW3_COL6:
            AddCharacter('f');
            break;

        case KEY_BACKSPACE:
            RemoveLastMACAddressCharacter();
            break;

        default:
            break;
    }
    //
    //  Display the keyboard
    //
    DPH_DisplayKeyboard(Page);

    //
    //  Display the MAC Address
    //
    DPH_DisplayMACAddress(&MACAddress[0], 0);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();
}

/*******************************************************************************
    AddCharacter

    Add a character to the MAC Address string
*******************************************************************************/
static void AddCharacter(U8BIT character)
{
    if (STR_StrLen(MACAddress) < (sizeof(MACAddress) - 1))
    {
        STR_StrCat(MACAddress, &character);
        if (STR_StrLen(MACAddress) == (sizeof(MACAddress) - 1))
        {
            KBH_SetKeyPressProperties(KEY_DONE, (FUNC)MNU_MACAddress_Commit, (void*)KEY_DONE);
        }
    }
}

/*******************************************************************************
    RemoveLastMACAddressCharacter

    Remove the last character of the MAC Address string
*******************************************************************************/
static void RemoveLastMACAddressCharacter(void)
{
    STR_StrNCpy(&MACAddress[0], &MACAddress[0], (STR_StrLen(&MACAddress[0]) - 1));
    KBH_SetKeyPressProperties(KEY_DONE, (FUNC)NULL, (void*)KEY_DONE);
}

DB_NOTIFICATION(InstallCodeFailed_Callback)
{
    HAL_ClearScreen();
    DPH_DisplayMACAddressError();
    KBH_SetKeyPressProperties(KEY_DONE, (FUNC)MNH_SetActiveMenu, (void*)MENU_INSTALL_CODE);
}

DB_NOTIFICATION(InstallCodeSuccess_Callback)
{
    MNH_SetActiveMenu(MENU_ADD);
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_MACAddress_Entry
*******************************************************************************/
void MNU_MACAddress_Entry(void)
{
    MACAddressEditState = EDIT_IN_PROGRESS;
    Page = KEY_PAGE_0_F;
    MACAddress[0] = 0;

    //
    //  Display the keyboard
    //
    DPH_DisplayKeyboard(Page);

    //
    //  Display the MAC Address
    //
    DPH_DisplayMACAddress(&MACAddress[0], 1);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    MNU_SetKeyAssigned(FALSE);

    hInstallCodeFailedNotification = DBTHRD_SubscribeToNotification( DBTYPE_INSTALLCODE_FAILED,
                                                       InstallCodeFailed_Callback,
                                                       DBCHANGE_ZIGBEE,
                                                       INDEX_DONT_CARE);

    hInstallCodeSuccessNotification = DBTHRD_SubscribeToNotification( DBTYPE_INSTALLCODE_SUCCESS,
                                                       InstallCodeSuccess_Callback,
                                                       DBCHANGE_ZIGBEE,
                                                       INDEX_DONT_CARE);

	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_MACAddress_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_MACAddress_Core

    Enter the MAC Address
*******************************************************************************/
void MNU_MACAddress_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_MACAddress_Commit, (void*)KEY_BACK);
        KBH_SetKeyPressProperties(KEY_KEYBOARD, (FUNC)ManageKeyboard, (void*)NULL);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_MACAddress_Exit
*******************************************************************************/
void MNU_MACAddress_Exit(void)
{
    MACAddressEditState = EDIT_IDLE;
    DBTHRD_UnsubscribeToNotification(hInstallCodeFailedNotification);
    DBTHRD_UnsubscribeToNotification(hInstallCodeSuccessNotification);
}

/*******************************************************************************
    MNU_MACAddress_Commit
*******************************************************************************/
void MNU_MACAddress_Commit(KEYB_PARAM keyId)
{
	if (MACAddressEditState == EDIT_IN_PROGRESS)
	{
        KBH_ClearAllKeyProperties();
        switch(keyId)
        {
            case KEY_BACK:
                MNH_SetActiveMenu(MENU_INSTALL_CODE);
                break;

            case KEY_DONE:
                ZBTHRD_SetUseOfInstallCode();
                //MNH_SetActiveMenu(MENU_ADD);
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
		MACAddressEditState = EDIT_IDLE;
	}
}

uint8_t* GetMacAddress(void)
{
//    memcpy(&MACAddress[0], "f8f005fffff48250", 16);
    return &MACAddress[0];
}
