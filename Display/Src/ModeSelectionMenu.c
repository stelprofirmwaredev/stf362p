/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       ModeSelectionMenu.c

    \brief      Source code file for the ModeSelectionMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the ModeSelectionMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "Program_Tracking.h"
#include "ModeSelectionMenu.h"

#include ".\DB\inc\THREAD_DB.h"
/*******************************************************************************
    Private constants definitions
*******************************************************************************/


/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;


/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   ModeSelectionEditState = EDIT_IDLE;
static Program_t SelectedProgram;
static uint8_t previousMenu;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/
    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void ChangeMode( KEYB_PARAM keyId );
static void EditProgram( KEYB_PARAM keyId );

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  Change the current mode
* @inputs keyId: pressed key (selected mode)
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/07/19
*******************************************************************************/
static void ChangeMode( KEYB_PARAM keyId )
{
    ScheduleModeEnum_t mode;
    Program_t pgm;
    Setpoint100_t pgmSetpoint;
    uint8_t floorMode;

    FindActiveProgram();
    DBTHRD_GetData(DBTYPE_ACTIVE_PGM,(void*)&pgm, INDEX_DONT_CARE);
    switch (keyId)
    {
        case KEY_OPTIONS_1:
            mode = MODE_MANUAL;
            break;

        case KEY_OPTIONS_2:
            if (pgm == PGM_WAKE)
            {
                mode = MODE_PROG;
            }
            else
            {
                mode = MODE_DEROGATION;
            }
            pgm = PGM_WAKE;
            break;

        case KEY_OPTIONS_3:
            if (pgm == PGM_LEAVE)
            {
                mode = MODE_PROG;
            }
            else
            {
                mode = MODE_DEROGATION;
            }
            pgm = PGM_LEAVE;
            break;

        case KEY_OPTIONS_4:
            if (pgm == PGM_RETURN)
            {
                mode = MODE_PROG;
            }
            else
            {
                mode = MODE_DEROGATION;
            }
            pgm = PGM_RETURN;
            break;

        case KEY_OPTIONS_5:
            if (pgm == PGM_SLEEP)
            {
                mode = MODE_PROG;
            }
            else
            {
                mode = MODE_DEROGATION;
            }
            pgm = PGM_SLEEP;
            break;

        case KEY_OPTIONS_10:
            mode = MODE_PROG;
            break;

        default:
            break;
    }
    DBTHRD_SetData(DBTYPE_SCHEDULE_MODE,(void*)&mode, INDEX_DONT_CARE, DBCHANGE_LOCAL);
    if ((mode == MODE_PROG) || (mode == MODE_DEROGATION))
    {
        DBTHRD_SetData(DBTYPE_ACTIVE_PGM,(void*)&pgm, INDEX_DONT_CARE, DBCHANGE_LOCAL);

        //Set the program setpoint to either the Floor or Ambient setpoint
        pgmSetpoint = DeterminePgmSetpoint(pgm);
        DBTHRD_GetData(DBTYPE_FLOOR_MODE,(void*)&floorMode, THIS_THERMOSTAT);
        if (floorMode == CONTROLMODE_FLOOR)
        {
            DBTHRD_SetData(DBTYPE_FLOOR_SETPOINT,(void*)&pgmSetpoint, THIS_THERMOSTAT, DBCHANGE_LOCAL);
        }
        else
        {
            DBTHRD_SetData(DBTYPE_AMBIENT_SETPOINT,(void*)&pgmSetpoint, THIS_THERMOSTAT, DBCHANGE_LOCAL);
        }
    }
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_ModeSelection_Commit,  (KEYB_PARAM*)keyId, IMMEDIATELY);
}

/*******************************************************************************
* @brief  Edit the selected program (setpoint and or schedule)
* @inputs keyId: pressed key (selected program)
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/07/31
*******************************************************************************/
static void EditProgram( KEYB_PARAM keyId )
{
    switch (keyId)
    {
        case KEY_OPTIONS_6:
            SelectedProgram = PGM_WAKE;
            break;

        case KEY_OPTIONS_7:
            SelectedProgram = PGM_LEAVE;
            break;

        case KEY_OPTIONS_8:
            SelectedProgram = PGM_RETURN;
            break;

        case KEY_OPTIONS_9:
            SelectedProgram = PGM_SLEEP;
            break;

        default:
            break;
    }
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_ModeSelection_Commit,  (KEYB_PARAM*)keyId, IMMEDIATELY);
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_ModeSelection_Entry
*******************************************************************************/
void MNU_ModeSelection_Entry(void)
{
    ModeSelectionEditState = EDIT_IN_PROGRESS;

    previousMenu = MNH_GetPreviousMenu();

    MNU_SetKeyAssigned(FALSE);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show Mode list
    //
    DPH_DisplayModeList();

	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_ModeSelection_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_ModeSelection_Core

    Allow to select the mode
*******************************************************************************/
void MNU_ModeSelection_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_ModeSelection_Commit, (void*)KEY_BACK);
        KBH_SetKeyPressProperties(KEY_OPTIONS_1, (FUNC)ChangeMode, (void*) KEY_OPTIONS_1);
        KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)ChangeMode, (void*) KEY_OPTIONS_2);
        KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)ChangeMode, (void*) KEY_OPTIONS_3);
        KBH_SetKeyPressProperties(KEY_OPTIONS_4, (FUNC)ChangeMode, (void*) KEY_OPTIONS_4);
        KBH_SetKeyPressProperties(KEY_OPTIONS_5, (FUNC)ChangeMode, (void*) KEY_OPTIONS_5);
        KBH_SetKeyPressProperties(KEY_OPTIONS_6, (FUNC)EditProgram, (void*) KEY_OPTIONS_6);
        KBH_SetKeyPressProperties(KEY_OPTIONS_7, (FUNC)EditProgram, (void*) KEY_OPTIONS_7);
        KBH_SetKeyPressProperties(KEY_OPTIONS_8, (FUNC)EditProgram, (void*) KEY_OPTIONS_8);
        KBH_SetKeyPressProperties(KEY_OPTIONS_9, (FUNC)EditProgram, (void*) KEY_OPTIONS_9);
        KBH_SetKeyPressProperties(KEY_OPTIONS_10, (FUNC)ChangeMode, (void*) KEY_OPTIONS_10);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_ModeSelection_Exit
*******************************************************************************/
void MNU_ModeSelection_Exit(void)
{
    ModeSelectionEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_ModeSelection_Commit
*******************************************************************************/
void MNU_ModeSelection_Commit(KEYB_PARAM keyId)
{
	if (ModeSelectionEditState == EDIT_IN_PROGRESS)
	{
        switch (keyId)
        {
            case KEY_BACK:
                KBH_ClearAllKeyProperties();
                if (previousMenu == MENU_HOME)
                {
                    MNH_SetActiveMenu(MENU_HOME);
                }
                else
                {
                    MNH_SetActiveMenu(MENU_ADV_SETTINGS);
                }
                ModeSelectionEditState = EDIT_IDLE;
                break;

            case KEY_OPTIONS_6:
            case KEY_OPTIONS_7:
            case KEY_OPTIONS_8:
            case KEY_OPTIONS_9:
                MNH_SetActiveMenu(MENU_PROGRAM_SETPOINT);
                break;

            default:
                KBH_ClearAllKeyProperties();
                MNH_SetActiveMenu(MENU_HOME);
                ModeSelectionEditState = EDIT_IDLE;
                break;
        }
	}
}

/*******************************************************************************
    MNU_GetSelectedProgram
*******************************************************************************/
Program_t MNU_GetSelectedProgram(void)
{
    return SelectedProgram;
}
