/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       StringObjects.c

    \brief      Source code file for the StringObjects module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This module provides a string collection with tools and accessors.

    Requirement: Strings definitions specific to each project are expected
                in StringCollection.c.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/

#include "typedef.h"
#include "StringObjects.h"
#ifdef DATABASE
#include ".\DB\inc\THREAD_DB.h"
#endif

/*******************************************************************************
    Private constants definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

//#include "StringCollection.c"

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/

    //Empty

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/

//static int8u* StrI32toA(int8u* str, int32u num);
static int16s StrRoundTemp(int16s temp_to_round, int16s rounding);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
///*******************************************************************************
//    StrI32toA
//
//    Converts a 32 unsigned integer to string
//
//    \param str string pointer
//
//    \param num number to convert
//
//    \return pointer to NULL terminated string
//*******************************************************************************/
//
//static int8u* StrI32toA(int8u* str, int32u num)
//{
//    int8u nbchar = 0;
//    int32u nbmem;
//
//    for (nbmem = num; nbmem || !nbchar; nbchar++) {
//
//        nbmem = nbmem / 10;
//    }
//    str[nbchar] = NULL;
//    do {
//
//        nbchar--;
//        str[nbchar] = (int8u) (num % 10) + 0x30;
//        num = num / 10;
//    } while(nbchar);
//    return str;
//}

/*******************************************************************************
    StrRoundTemp

    Rounds a temperature

    \param temp_to_round Input temperature

    \param rounding Value to round to

    \return Rounded temperature
*******************************************************************************/

static int16s StrRoundTemp(int16s temp_to_round, int16s rounding)
{
    int16s temp_result;

    if (rounding && (temp_to_round >= -30000) && (temp_to_round <= 30000)) {

        temp_result = temp_to_round % rounding;
        if (temp_result < 0) {

            temp_result = -temp_result;
        }
        if (temp_result >= (rounding >> 1)) {

            temp_result -= rounding;
        }
        if (temp_to_round >= 0) {

            temp_to_round -= temp_result;
        } else {

            temp_to_round += temp_result;
        }
    }

    return temp_to_round;
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    STR_Init

    Module initialization
*******************************************************************************/

void STR_Init(void)
{
    //Empty
}

/*******************************************************************************
    STR_StrItoA

    Converts a 16 unsigned integer to string

    \param str string pointer

    \param num number to convert

    \return pointer to NULL terminated string
*******************************************************************************/

int8u* STR_StrItoA(int8u* str, int16u num)
{
    int8u nbchar = 0;
    int16u nbmem;

    for(nbmem = num; nbmem || !nbchar; nbchar++) {
        nbmem = nbmem / 10;
    }

    str[nbchar] = NULL;
    do {
        nbchar--;
        str[nbchar] = num % 10 + 0x30;
        num = num / 10;
    } while(nbchar);
    return str;
}

/*******************************************************************************
    STR_StrItoA_Hex

    Converts a 16 unsigned integer to string in hexadecimal format

    \param str string pointer

    \param num number to convert

    \return pointer to NULL terminated string
*******************************************************************************/

int8u* STR_StrItoA_Hex(int8u* str, int16u num)
{
    if (((num>>12)& 0x000f) >= 10)
    {
        str[0] = (((num>>12) & 0x000f)-0x0a) + 0x41;
    }
    else
    {
        str[0] = ((num>>12) & 0x000f) + 0x30;
    }

    if (((num>>8)& 0x000f) >= 10)
    {
        str[1] = (((num>>8) & 0x000f)-0x0a) + 0x41;
    }
    else
    {
        str[1] = ((num>>8) & 0x000f) + 0x30;
    }

    if (((num>>4)& 0x000f) >= 10)
    {
        str[2] = (((num>>4)& 0x000f)-0x0a) + 0x41;
    }
    else
    {
        str[2] = ((num>>4)& 0x000f) + 0x30;
    }

    if ((num & 0x000f) >= 10)
    {
        str[3] = ((num & 0x000f)-0x0a) + 0x41;
    }
    else
    {
        str[3] = (num & 0x000f) + 0x30;
    }
    str[4] = 0;
    return str;
}

///*******************************************************************************
//    STR_GetString
//
//    Returns the value of a string
//
//    \param id String ID
//
//    \return Pointer on the identified string
//*******************************************************************************/
#ifdef DATABASE
int8u const* STR_GetString(STRID id)
{
    DisplayLang_t lang;

    DBTHRD_GetData(DBTYPE_LANGUAGE_SETTING,(void*)&lang, INDEX_DONT_CARE);
    if (id < MAX_STRID)
    {
        if (lang == LANG_FRANCAIS)
        {
            return FRENCH_STR_TABLE[id];
        }
        else
        {
            return ENGLISH_STR_TABLE[id];
        }
    }

    return NULL;
}
#endif /* DATABASE */

/*******************************************************************************
    STR_StrLen

    Returns length of a string

    \param str Pointer to a NULL terminated string

    \return 0 - 255 Number of characters
*******************************************************************************/

int8u STR_StrLen(int8u GENERIC * str)
{
    int8u cnt = 0;
    if (str != NULL) {

        while ((str[cnt] != NULL) && (cnt != 0xff)) {

            cnt++;
        }
    }
    return cnt;
}

/*******************************************************************************
    STR_StrPut

    Appends a single char at the end of a string and add a terminating NULL
    character.

    \param sdest Pointer to a destination string in RAM
    \param ssrc Character to add

    \return Pointer to NULL terminated string with added character
*******************************************************************************/

int8u* STR_StrPut(int8u* sdest, int8u ssrc)
{
    int8u num;
    if (sdest != NULL) {

        num = STR_StrLen(sdest);
        sdest[num] = ssrc;
        sdest[num + 1] = NULL;
    }
    return sdest;
}

/*******************************************************************************
    STR_StrCat

    Appends a string (ssrc) at the end of another string (sdest).

    \param sdest Pointer to a NULL terminated destination string
    \param ssrc Pointer to a NULL terminated source string to append

    \return Pointer to the appended string
*******************************************************************************/

int8u* STR_StrCat(int8u* sdest, int8u GENERIC * ssrc)
{
    int8u cnt = 0;
    if ((sdest != NULL) && (ssrc != NULL)) {

        while ((sdest[cnt] != NULL) && (cnt != 0xff)) {

            cnt++;
        }
        while((*ssrc != NULL) && (cnt != 0xff)) {

            sdest[cnt] = *ssrc;
            cnt++;
            ssrc++;
        }
        sdest[cnt] = NULL;
    }
    return sdest;
}

/*******************************************************************************
    STR_StrCpy

    Copy a string (ssrc) to another string (sdest).

    \param sdest Pointer to a destination string
    \param ssrc Pointer to a NULL terminated source string to be copied

    \return Pointer to the copy string
*******************************************************************************/

int8u* STR_StrCpy(int8u* sdest, int8u GENERIC * ssrc)
{
    int8u prot = 0xff;          //Protection to avoid looping
    int8u* ptr = sdest;
    if ((sdest != NULL) && (ssrc != NULL)) {

        while ((*ssrc != NULL) && (prot > 0)) {

            *ptr = *ssrc;
            ssrc++;
            ptr++;
            prot--;
        }
        *ptr = NULL;
    }
    return sdest;
}

/*******************************************************************************
    STR_StrNCpy

    Appends n characters from a string (ssrc) to another string (sdest).

    \param sdest Pointer to a destination string

    \param ssrc Pointer to a NULL terminated source string

    \param num Maximum number of charaters to append

    \return Pointer to the appended string
*******************************************************************************/

int8u* STR_StrNCpy(int8u* sdest, int8u GENERIC * ssrc, int8u num)
{
    int8u* ptr = sdest;
    if ((sdest != NULL) && (ssrc != NULL)) {

        while ((*ssrc != NULL) && (num > 0)) {

            *ptr = *ssrc;
            ssrc++;
            ptr++;
            num--;
        }
        *ptr = NULL;
    }
    return sdest;
}

/*******************************************************************************
    STR_Time2Str

    Format a time string (hh:mm).

    \param time Time in minutes (0 - 1439)
    \param sdest Pointer to a destination string
    \param options Time display format options

    \return Pointer to the formatted string
*******************************************************************************/

int8u* STR_Time2Str(int16u time, int8u* sdest, int8u options)
{
    int8u hour = time / 60;
    boolean pm = FALSE;
    int8u i = 0;

    if (sdest != NULL)
    {
        if (time >= 1440)
        {
            STR_StrCpy(sdest, "--:--");
        }
        else
        {
            if ((options & USE_24H_FORMAT) == 0)
            {
                if (hour >= 12)
                {
                    pm = TRUE;
                }
                if (hour == 0)
                {
                    hour = 12;
                }
                if (hour > 12)
                {
                    hour -= 12;
                }
            }

            if ((hour > 9))
            {
                sdest[i++] = (hour / 10) + 0x30;
            }
            else if ((options & NO_SPACE) != 0)
            {
                    sdest[i++] = 0x20;
            }
            else
            {
                //Empty
            }

            sdest[i++] = (hour % 10) + 0x30;
            sdest[i++] = ':';
            sdest[i++] = (time % 60) / 10 + 0x30;
            sdest[i++] = (time % 60) % 10 + 0x30;

            sdest[i] = NULL;

            if ((options & USE_24H_FORMAT) == 0)
            {
                sdest[i++] = ' ';
                if (pm)
                {
                    sdest[i++] = 'p';
                }
                else
                {
                    sdest[i++] = 'a';
                }
                sdest[i] = NULL;
                if ((options & LONG_12H) != 0)
                {
                    sdest[i++] = 'm';
                    sdest[i] = NULL;
                }
            }
        }
    }
    return sdest;
}

/*******************************************************************************
    STR_Temp2Str

    Format a temperature string.

    \param temp Temperature in �C (1 = 0.01�C)
    \param sdest Pointer to a destination string
    \param options Temperature display format options

    \return Pointer to the formatted string
*******************************************************************************/

int8u* STR_Temp2Str(int16s temp, int8u* sdest, int16u options)
{
    int8u strtmp[4];
    int8u round;

    sdest[0] = NULL;

    if ((options & ABS_TEMP) != 0) {

        if (temp < 0) {

            temp = -temp;
        }
    }

    if ((options & FAHRENHEIT) != 0) {

        temp = ((int32s)temp * 9) / 5;
        if ((options & RAW_F_TEMP) == 0) {

            temp += 3200;
        }
        temp = StrRoundTemp(temp, 100);
    } else {
        if ((options & TEMP_DOT) != 0)
        {
            temp = StrRoundTemp(temp, 50);
        }
        else
        {
            temp = StrRoundTemp(temp, 100);
        }
    }

    if ((options & TEMP_DOT) != 0)
    {
        round = 50;
    }
    else
    {
        round = 100;
    }
    if ((temp / round) < 0)
    {
        sdest[0] = '-';
        sdest[1] = NULL;
        options &= ~ADD_SPACING;
    }

    if (temp < 0) {

        temp = ~(temp - 1);
    }

    if ((options & ADD_SPACING) != 0)
    {
        if ((temp < 1000) && (temp > -1000))
        {
            STR_StrPut(sdest, ' ');
            STR_StrPut(sdest, ' ');
        }
    }

    STR_StrCat(sdest, STR_StrItoA(strtmp, (temp/100)));

    //Replace the '1' in the string with '|' to turn On the stand alone 1 hundred segment
    if ((options & HUNDRED) != 0)
    {
        if (temp >= 10000)
        {
            sdest[0] = '|';
        }
    }

    if ((options & FAHRENHEIT) == 0) {

        if (((options & TEMP_DOT) != 0) && ((options & HIDE_DECIMAL) == 0))
        {

            STR_StrPut(sdest, '.');
			STR_StrPut(sdest, 0x30 + (temp % 100) / 10);
        }
    }

    if ((options & ADD_DEG) != 0) {
        STR_StrCat(sdest, "�");
    }

	if ((options & ADD_FORMAT) != 0) {

        if ((options & FAHRENHEIT) != 0) {

            STR_StrCat(sdest, "F");
        } else {

            STR_StrCat(sdest, "C");
        }
    }

    if ((options & DECIMAL_ONLY) != 0)
    {
        sdest[0] = 0x30 + (temp % 100) / 10;
        sdest[1] = 0;
    }

    return sdest;
}

/*******************************************************************************
    STR_Delay2Str

    Format a delay string (hh:mm) from a time interval in seconds.

    \param delay Delay in seconds (0 - 65535)
    \param sdest Pointer to a destination string (6 characters required)

    \return Pointer to the formatted string
*******************************************************************************/

int8u* STR_Delay2Str(int16u delay, int8u* sdest)
{

    int8u minute = delay / 60;

    if (minute > 9) {

        sdest[0] = (minute / 10) + 0x30;
    } else {

        sdest[0] = 0x20;
    }
    sdest[1] = (minute % 10) + 0x30;
    sdest[2] = ':';
    sdest[3] = (delay % 60) / 10 + 0x30;
    sdest[4] = (delay % 60) % 10 + 0x30;
    sdest[5] = NULL;

    return sdest;
}


