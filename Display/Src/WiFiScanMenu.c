/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       WiFiScanMenu.c

    \brief      Source code file for the WiFiScanMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the WiFiScanMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "WiFiScanMenu.h"
#include "string.h"

#include ".\WiFi\inc\THREAD_WiFi.h"
#include ".\HAL\inc\HAL_WiFiTypes.h"
#include ".\HAL\inc\Display.h"

#include ".\strings\inc\strings_tool.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/


/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;


/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e       WiFiScanEditState = EDIT_IDLE;
static U8BIT                NetworkNumbers = 0;
static U8BIT                Page = 0;
static wifiScanResult_t*    scanResults;
static U8BIT                scanDone;
static U8BIT                SelectedSSIDNumber;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/
    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void NextPage(void);
static void PreviousPage(void);
static void MNU_WiFiScan_UpdateList(void* pData);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    NextPage

    Switch to the next page of available networks
*******************************************************************************/
static void NextPage(void)
{
    Page++;
    HAL_ClearScreen();
    KBH_ClearAllKeyProperties();
}

/*******************************************************************************
    PreviousPage

    Switch to the previous page of available networks
*******************************************************************************/
static void PreviousPage(void)
{
    Page--;
    HAL_ClearScreen();
    KBH_ClearAllKeyProperties();
}

/*******************************************************************************
* @brief  Callback function when the Wi-Fi scan is completed
* @inputs pData: pointer to the scan results
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/09/14
*******************************************************************************/
static void MNU_WiFiScan_UpdateList(void* pData)
{
    uint8_t number;

    scanResults = (wifiScanResult_t *)pData;
    NetworkNumbers = scanResults->numberOfResults;
    for (number = 0; number < NetworkNumbers; number++)
    {
        ConvertUTF8ToASCII((char*)scanResults->scanTable[number].SSID,
                           (char*)scanResults->scanTable[number].SSID,
                            sizeof(scanResults->scanTable[number].SSID));
    }
    scanDone = TRUE;
    KBH_ClearAllKeyProperties();
    OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
}


/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_WiFiScan_Entry
*******************************************************************************/
void MNU_WiFiScan_Entry(void)
{
    WiFiScanEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);

    //Perform a Wi-Fi Scan
    Page = 0;
    SelectedSSIDNumber = 0;
    scanDone = FALSE;
    WIFITHRD_StartWiFiScan(MNU_WiFiScan_UpdateList);
	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_WiFiScan_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_WiFiScan_Core

    Scans the available Wi-Fi networks
*******************************************************************************/
void MNU_WiFiScan_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        if (scanDone == TRUE)
        {
            //
            // Configure keys
            //
            KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_WiFiScan_Commit, (void*)KEY_BACK);
            if ((NetworkNumbers - (Page * 7)) > 0)
            {
                KBH_SetKeyPressProperties(KEY_OPTIONS_1, (FUNC)MNU_WiFiScan_Commit, (void*) KEY_OPTIONS_1);
            }
            if ((NetworkNumbers - (Page * 7)) > 1)
            {
                KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)MNU_WiFiScan_Commit, (void*) KEY_OPTIONS_2);
            }
            if ((NetworkNumbers - (Page * 7)) > 2)
            {
                KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)MNU_WiFiScan_Commit, (void*) KEY_OPTIONS_3);
            }
            if ((NetworkNumbers - (Page * 7)) > 3)
            {
                KBH_SetKeyPressProperties(KEY_OPTIONS_4, (FUNC)MNU_WiFiScan_Commit, (void*) KEY_OPTIONS_4);
            }
            if ((NetworkNumbers - (Page * 7)) > 4)
            {
                KBH_SetKeyPressProperties(KEY_OPTIONS_5, (FUNC)MNU_WiFiScan_Commit, (void*) KEY_OPTIONS_5);
            }
            if ((NetworkNumbers - (Page * 7)) > 5)
            {
                KBH_SetKeyPressProperties(KEY_OPTIONS_6, (FUNC)MNU_WiFiScan_Commit, (void*) KEY_OPTIONS_6);
            }
            if ((NetworkNumbers - (Page * 7)) > 6)
            {
                KBH_SetKeyPressProperties(KEY_OPTIONS_7, (FUNC)MNU_WiFiScan_Commit, (void*) KEY_OPTIONS_7);
            }
            if (NetworkNumbers > (7 * (Page + 1)))
            {
                KBH_SetKeyPressProperties(KEY_NEXT, (FUNC)NextPage, (void*) KEY_NEXT);
            }
            if (Page > 0)
            {
                KBH_SetKeyPressProperties(KEY_PREVIOUS, (FUNC)PreviousPage, (void*) KEY_PREVIOUS);
            }

            MNU_SetKeyAssigned(TRUE);
        }
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show available Wi-Fi networks
    //

    DPH_DisplayWiFiNetworks(Page, scanResults, scanDone);
}

/*******************************************************************************
    MNU_WiFiScan_Exit
*******************************************************************************/
void MNU_WiFiScan_Exit(void)
{
    WiFiScanEditState = EDIT_IDLE;
    WIFITHRD_StopWiFiScan();
}

/*******************************************************************************
    MNU_WiFiScan_Commit
*******************************************************************************/
void MNU_WiFiScan_Commit(KEYB_PARAM keyId)
{
	if (WiFiScanEditState == EDIT_IN_PROGRESS)
	{
        KBH_ClearAllKeyProperties();
        switch(keyId)
        {
            case KEY_BACK:
                Page = 0;
                WIFITHRD_CancelWiFiScan();
                MNH_SetActiveMenu(MENU_WIFI_SETUP);
                break;

            case KEY_OPTIONS_1:
            case KEY_OPTIONS_2:
            case KEY_OPTIONS_3:
            case KEY_OPTIONS_4:
            case KEY_OPTIONS_5:
            case KEY_OPTIONS_6:
            case KEY_OPTIONS_7:
                SelectedSSIDNumber = Page * 7 + (keyId - KEY_OPTIONS_1);
                MNH_SetActiveMenu(MENU_PASSWORD);
                break;

            default:
                Page = 0;
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
		WiFiScanEditState = EDIT_IDLE;
	}
}

/*******************************************************************************
    MNU_WiFiScan_GetSelectedSSID
*******************************************************************************/
U8BIT* MNU_WiFiScan_GetSelectedSSID(void)
{
    return scanResults->scanTable[SelectedSSIDNumber].SSID;
}

/*******************************************************************************
    MNU_WiFiScan_GetSelectedSSIDNumber
*******************************************************************************/
U8BIT MNU_WiFiScan_GetSelectedSSIDNumber(void)
{
    return SelectedSSIDNumber;
}