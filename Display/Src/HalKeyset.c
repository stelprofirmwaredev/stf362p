/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       HalKeyset.c

    \brief      HAL keyset interface implementation file.

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the implementation of the HAL keyset interface.

*******************************************************************************/

/*-------------------------------------------------------------------------------
    INCLUDE
-------------------------------------------------------------------------------*/
#include "typedef.h"
#include "OsTask.h"
#include "HalKeyset.h"
#include "KeyboardHandler.h"
#include "MenuTable.h"
#include ".\HAL\inc\HAL_Keypad.h"
#include ".\DB\inc\THREAD_DB.h"

#include ".\GuiFrameWork\inc\class_Button.h"

#include ".\Console\inc\THREAD_Console.h"

/*-------------------------------------------------------------------------------
    DEFINITIONS
-------------------------------------------------------------------------------*/
#define LCD_WIDTH   320
#define LCD_HEIGHT  480

#define MAX_WAKE_UP_COUNT   1

//
//! Key event timers
// \typedef <HalKeyTimers_t>
//
typedef struct
{
    int16u    repeat;   //! timer used for UI fast scrolling feature
    int16u    hold;     //! timer used for key hold feature
} HalKeyTimers_t;

FLASH_MEM const TOUCH_ZONE TouchZones[] = {
    //startX        //startY        //width     //height    //bindedKey             //activeMenu
    128,            262,            64,         64,         KEY_MODE,               MENU_HOME,
    98,             17,             135,        45,         KEY_OPTIONS_1,          MENU_HOME,
    255,            0,              64,         64,         KEY_SETTINGS,           MENU_HOME,
    9,              9,              64,         64,         KEY_ALERTS,             MENU_HOME,
    25,             336,            100,        100,        KEY_DOWN,               MENU_HOME,
    195,            336,            100,        100,        KEY_UP,                 MENU_HOME,
    0,              0,              64,         64,         KEY_BACK,               MENU_ADV_SETTINGS,
    25,             119,            270,        42,         KEY_OPTIONS_2,          MENU_ADV_SETTINGS,
    25,             169,            270,        42,         KEY_OPTIONS_3,          MENU_ADV_SETTINGS,
    25,             219,            270,        42,         KEY_OPTIONS_4,          MENU_ADV_SETTINGS,
    25,             269,            270,        42,         KEY_OPTIONS_5,          MENU_ADV_SETTINGS,
    25,             319,            270,        42,         KEY_OPTIONS_6,          MENU_ADV_SETTINGS,
    25,             369,            270,        42,         KEY_OPTIONS_7,          MENU_ADV_SETTINGS,
    0,              0,              64,         64,         KEY_BACK,               MENU_MODE_SELECTION,
    25,             119,            270,        42,         KEY_OPTIONS_1,          MENU_MODE_SELECTION,
    25,             269,            210,        42,         KEY_OPTIONS_2,          MENU_MODE_SELECTION,
    25,             319,            210,        42,         KEY_OPTIONS_3,          MENU_MODE_SELECTION,
    25,             369,            210,        42,         KEY_OPTIONS_4,          MENU_MODE_SELECTION,
    25,             419,            210,        42,         KEY_OPTIONS_5,          MENU_MODE_SELECTION,
    236,            258,            64,         64,         KEY_OPTIONS_6,          MENU_MODE_SELECTION,
    236,            308,            64,         64,         KEY_OPTIONS_7,          MENU_MODE_SELECTION,
    236,            358,            64,         64,         KEY_OPTIONS_8,          MENU_MODE_SELECTION,
    236,            408,            64,         64,         KEY_OPTIONS_9,          MENU_MODE_SELECTION,
    25,             169,            270,        42,         KEY_OPTIONS_10,         MENU_MODE_SELECTION,
    0,              0,              64,         64,         KEY_BACK,               MENU_OPTIONS,
    25,             119,            270,        42,         KEY_OPTIONS_1,          MENU_OPTIONS,
    25,             169,            270,        42,         KEY_OPTIONS_2,          MENU_OPTIONS,
    25,             219,            270,        42,         KEY_OPTIONS_3,          MENU_OPTIONS,
    25,             269,            270,        42,         KEY_OPTIONS_4,          MENU_OPTIONS,
    25,             319,            270,        42,         KEY_OPTIONS_5,          MENU_OPTIONS,
    25,             369,            270,        42,         KEY_OPTIONS_6,          MENU_OPTIONS,
    0,              0,              64,         64,         KEY_BACK,               MENU_DATE_TIME,
    21,             106,            64,         64,         KEY_OPTIONS_1,          MENU_DATE_TIME,
    241,            106,            64,         64,         KEY_OPTIONS_2,          MENU_DATE_TIME,
    21,             156,            64,         64,         KEY_OPTIONS_3,          MENU_DATE_TIME,
    241,            156,            64,         64,         KEY_OPTIONS_4,          MENU_DATE_TIME,
    21,             206,            64,         64,         KEY_OPTIONS_5,          MENU_DATE_TIME,
    241,            206,            64,         64,         KEY_OPTIONS_6,          MENU_DATE_TIME,
    21,             256,            64,         64,         KEY_OPTIONS_7,          MENU_DATE_TIME,
    241,            256,            64,         64,         KEY_OPTIONS_8,          MENU_DATE_TIME,
    25,             420,            270,        42,         KEY_DONE,               MENU_DATE_TIME,
    0,              0,              64,         64,         KEY_BACK,               MENU_PROGRAM_SETPOINT,
    25,             194,            270,        42,         KEY_OPTIONS_1,          MENU_PROGRAM_SETPOINT,
    25,             336,            100,        100,        KEY_DOWN,               MENU_PROGRAM_SETPOINT,
    195,            336,            100,        100,        KEY_UP,                 MENU_PROGRAM_SETPOINT,
    0,              0,              64,         64,         KEY_BACK,               MENU_PROGRAM_SCHEDULE,
    0,              76,             88,         32,         KEY_OPTIONS_1,          MENU_PROGRAM_SCHEDULE,
    0,              116,            88,         32,         KEY_OPTIONS_2,          MENU_PROGRAM_SCHEDULE,
    0,              156,            88,         32,         KEY_OPTIONS_3,          MENU_PROGRAM_SCHEDULE,
    0,              196,            88,         32,         KEY_OPTIONS_4,          MENU_PROGRAM_SCHEDULE,
    0,              236,            88,         32,         KEY_OPTIONS_5,          MENU_PROGRAM_SCHEDULE,
    0,              276,            88,         32,         KEY_OPTIONS_6,          MENU_PROGRAM_SCHEDULE,
    0,              316,            88,         32,         KEY_OPTIONS_7,          MENU_PROGRAM_SCHEDULE,
    130,            76,             164,        32,         KEY_OPTIONS_8,          MENU_PROGRAM_SCHEDULE,
    130,            116,            164,        32,         KEY_OPTIONS_9,          MENU_PROGRAM_SCHEDULE,
    130,            156,            164,        32,         KEY_OPTIONS_10,         MENU_PROGRAM_SCHEDULE,
    130,            196,            164,        32,         KEY_OPTIONS_11,         MENU_PROGRAM_SCHEDULE,
    130,            236,            164,        32,         KEY_OPTIONS_12,         MENU_PROGRAM_SCHEDULE,
    130,            276,            164,        32,         KEY_OPTIONS_13,         MENU_PROGRAM_SCHEDULE,
    130,            316,            164,        32,         KEY_OPTIONS_14,         MENU_PROGRAM_SCHEDULE,
    6,              357,            100,        52,         KEY_DOWN,               MENU_PROGRAM_SCHEDULE,
    195,            357,            100,        52,         KEY_UP,                 MENU_PROGRAM_SCHEDULE,
    25,             420,            270,        42,         KEY_DONE,               MENU_PROGRAM_SCHEDULE,
    0,              0,              64,         64,         KEY_BACK,               MENU_CONTROL_MODE,
    25,             119,            270,        42,         KEY_OPTIONS_2,          MENU_CONTROL_MODE,
    25,             169,            270,        42,         KEY_OPTIONS_3,          MENU_CONTROL_MODE,
    25,             219,            270,        42,         KEY_OPTIONS_4,          MENU_CONTROL_MODE,
    25,             269,            270,        42,         KEY_OPTIONS_5,          MENU_CONTROL_MODE,
    25,             319,            270,        42,         KEY_OPTIONS_6,          MENU_CONTROL_MODE,
    25,             420,            270,        42,         KEY_DONE,               MENU_CONTROL_MODE,
    0,              0,              64,         64,         KEY_BACK,               MENU_FLOOR_TYPE,
    25,             119,            270,        42,         KEY_OPTIONS_2,          MENU_FLOOR_TYPE,
    25,             169,            270,        42,         KEY_OPTIONS_3,          MENU_FLOOR_TYPE,
    25,             420,            270,        42,         KEY_DONE,               MENU_FLOOR_TYPE,
    0,              0,              64,         64,         KEY_BACK,               MENU_FLOOR_LIMIT,
    25,             336,            100,        100,        KEY_DOWN,               MENU_FLOOR_LIMIT,
    195,            336,            100,        100,        KEY_UP,                 MENU_FLOOR_LIMIT,
    0,              0,              64,         64,         KEY_BACK,               MENU_LOCK,
    25,             119,            270,        42,         KEY_OPTIONS_2,          MENU_LOCK,
    25,             169,            270,        42,         KEY_OPTIONS_3,          MENU_LOCK,
    25,             420,            270,        42,         KEY_DONE,               MENU_LOCK,
    0,              0,              64,         64,         KEY_BACK,               MENU_BACKLIGHT,
    25,             119,            270,        42,         KEY_OPTIONS_2,          MENU_BACKLIGHT,
    25,             169,            270,        42,         KEY_OPTIONS_3,          MENU_BACKLIGHT,
    25,             219,            270,        42,         KEY_OPTIONS_4,          MENU_BACKLIGHT,
    25,             269,            270,        42,         KEY_OPTIONS_5,          MENU_BACKLIGHT,
    25,             420,            270,        42,         KEY_DONE,               MENU_BACKLIGHT,
    0,              0,              64,         64,         KEY_BACK,               MENU_LANGUAGE,
    25,             119,            270,        42,         KEY_OPTIONS_2,          MENU_LANGUAGE,
    25,             169,            270,        42,         KEY_OPTIONS_3,          MENU_LANGUAGE,
    25,             420,            270,        42,         KEY_DONE,               MENU_LANGUAGE,
    0,              0,              64,         64,         KEY_BACK,               MENU_UNIT_FORMAT,
    25,             119,            270,        42,         KEY_OPTIONS_2,          MENU_UNIT_FORMAT,
    25,             169,            270,        42,         KEY_OPTIONS_3,          MENU_UNIT_FORMAT,
    25,             420,            270,        42,         KEY_DONE,               MENU_UNIT_FORMAT,
    0,              0,              64,         64,         KEY_BACK,               MENU_TIME_FORMAT,
    25,             124,            270,        42,         KEY_OPTIONS_2,          MENU_TIME_FORMAT,
    25,             174,            270,        42,         KEY_OPTIONS_3,          MENU_TIME_FORMAT,
    25,             274,            270,        42,         KEY_OPTIONS_4,          MENU_TIME_FORMAT,
    25,             324,            270,        42,         KEY_OPTIONS_5,          MENU_TIME_FORMAT,
    25,             420,            270,        42,         KEY_DONE,               MENU_TIME_FORMAT,
    0,              0,              64,         64,         KEY_BACK,               MENU_RESET,
    25,             420,            270,        42,         KEY_DONE,               MENU_RESET,
    0,              0,              64,         64,         KEY_BACK,               MENU_INFO,
    0,              0,              64,         64,         KEY_BACK,               MENU_ALERTS,
    241,            408,            64,         64,         KEY_NEXT,               MENU_ALERTS,
    14,             408,            64,         64,         KEY_PREVIOUS,           MENU_ALERTS,
    25,             69,             270,        42,         KEY_OPTIONS_1,          MENU_ALERTS,
    25,             119,            270,        42,         KEY_OPTIONS_2,          MENU_ALERTS,
    25,             169,            270,        42,         KEY_OPTIONS_3,          MENU_ALERTS,
    25,             219,            270,        42,         KEY_OPTIONS_4,          MENU_ALERTS,
    25,             269,            270,        42,         KEY_OPTIONS_5,          MENU_ALERTS,
    25,             319,            270,        42,         KEY_OPTIONS_6,          MENU_ALERTS,
    25,             369,            270,        42,         KEY_OPTIONS_7,          MENU_ALERTS,
    0,              0,              64,         64,         KEY_BACK,               MENU_ALERT_DETAILS,
    25,             169,            270,        42,         KEY_DONE,               MENU_ALERT_DETAILS,
    241,            208,            64,         64,         KEY_NEXT,               MENU_LCDTEST,
    14,             208,            64,         64,         KEY_PREVIOUS,           MENU_LCDTEST,
    0,              0,              320,        480,        KEY_SCREEN,             MENU_LCDTEST,
    5,              5,              64,         64,         KEY_OPTIONS_1,          MENU_TESTMODE,
    250,            5,              64,         64,         KEY_OPTIONS_2,          MENU_TESTMODE,
    128,            208,            64,         64,         KEY_OPTIONS_3,          MENU_TESTMODE,
    5,              410,            64,         64,         KEY_OPTIONS_4,          MENU_TESTMODE,
    250,            410,            64,         64,         KEY_OPTIONS_5,          MENU_TESTMODE,
};

FLASH_MEM const KEYBOARD_ZONE KeyboardZone[] = {
    //startX        //startY        //width     //height    //keyboardKey           //activePage
    0,              329,            31,         36,         KEY_ROW1_COL1,          KEY_PAGE_CAPITAL,
    33,             329,            30,         36,         KEY_ROW1_COL2,          KEY_PAGE_CAPITAL,
    65,             329,            30,         36,         KEY_ROW1_COL3,          KEY_PAGE_CAPITAL,
    97,             329,            30,         36,         KEY_ROW1_COL4,          KEY_PAGE_CAPITAL,
    129,            329,            30,         36,         KEY_ROW1_COL5,          KEY_PAGE_CAPITAL,
    161,            329,            30,         36,         KEY_ROW1_COL6,          KEY_PAGE_CAPITAL,
    193,            329,            30,         36,         KEY_ROW1_COL7,          KEY_PAGE_CAPITAL,
    225,            329,            30,         36,         KEY_ROW1_COL8,          KEY_PAGE_CAPITAL,
    257,            329,            30,         36,         KEY_ROW1_COL9,          KEY_PAGE_CAPITAL,
    289,            329,            30,         36,         KEY_ROW1_COL10,         KEY_PAGE_CAPITAL,
    0,              367,            47,         36,         KEY_ROW2_COL1,          KEY_PAGE_CAPITAL,
    49,             367,            30,         36,         KEY_ROW2_COL2,          KEY_PAGE_CAPITAL,
    81,             367,            30,         36,         KEY_ROW2_COL3,          KEY_PAGE_CAPITAL,
    113,            367,            30,         36,         KEY_ROW2_COL4,          KEY_PAGE_CAPITAL,
    145,            367,            30,         36,         KEY_ROW2_COL5,          KEY_PAGE_CAPITAL,
    177,            367,            30,         36,         KEY_ROW2_COL6,          KEY_PAGE_CAPITAL,
    209,            367,            30,         36,         KEY_ROW2_COL7,          KEY_PAGE_CAPITAL,
    241,            367,            30,         36,         KEY_ROW2_COL8,          KEY_PAGE_CAPITAL,
    273,            367,            46,         36,         KEY_ROW2_COL9,          KEY_PAGE_CAPITAL,
    49,             405,            30,         36,         KEY_ROW3_COL1,          KEY_PAGE_CAPITAL,
    81,             405,            30,         36,         KEY_ROW3_COL2,          KEY_PAGE_CAPITAL,
    113,            405,            30,         36,         KEY_ROW3_COL3,          KEY_PAGE_CAPITAL,
    145,            405,            30,         36,         KEY_ROW3_COL4,          KEY_PAGE_CAPITAL,
    177,            405,            30,         36,         KEY_ROW3_COL5,          KEY_PAGE_CAPITAL,
    209,            405,            30,         36,         KEY_ROW3_COL6,          KEY_PAGE_CAPITAL,
    241,            405,            30,         36,         KEY_ROW3_COL7,          KEY_PAGE_CAPITAL,
    0,              405,            47,         36,         KEY_CAP_SYMBOL,         KEY_PAGE_CAPITAL,
    273,            405,            46,         36,         KEY_BACKSPACE,          KEY_PAGE_CAPITAL,
    0,              443,            79,         36,         KEY_NUMBER,             KEY_PAGE_CAPITAL,
    81,             443,            238,        36,         KEY_SPACE,              KEY_PAGE_CAPITAL,
    0,              329,            31,         36,         KEY_ROW1_COL1,          KEY_PAGE_LOWER,
    33,             329,            30,         36,         KEY_ROW1_COL2,          KEY_PAGE_LOWER,
    65,             329,            30,         36,         KEY_ROW1_COL3,          KEY_PAGE_LOWER,
    97,             329,            30,         36,         KEY_ROW1_COL4,          KEY_PAGE_LOWER,
    129,            329,            30,         36,         KEY_ROW1_COL5,          KEY_PAGE_LOWER,
    161,            329,            30,         36,         KEY_ROW1_COL6,          KEY_PAGE_LOWER,
    193,            329,            30,         36,         KEY_ROW1_COL7,          KEY_PAGE_LOWER,
    225,            329,            30,         36,         KEY_ROW1_COL8,          KEY_PAGE_LOWER,
    257,            329,            30,         36,         KEY_ROW1_COL9,          KEY_PAGE_LOWER,
    289,            329,            30,         36,         KEY_ROW1_COL10,         KEY_PAGE_LOWER,
    0,              367,            47,         36,         KEY_ROW2_COL1,          KEY_PAGE_LOWER,
    49,             367,            30,         36,         KEY_ROW2_COL2,          KEY_PAGE_LOWER,
    81,             367,            30,         36,         KEY_ROW2_COL3,          KEY_PAGE_LOWER,
    113,            367,            30,         36,         KEY_ROW2_COL4,          KEY_PAGE_LOWER,
    145,            367,            30,         36,         KEY_ROW2_COL5,          KEY_PAGE_LOWER,
    177,            367,            30,         36,         KEY_ROW2_COL6,          KEY_PAGE_LOWER,
    209,            367,            30,         36,         KEY_ROW2_COL7,          KEY_PAGE_LOWER,
    241,            367,            30,         36,         KEY_ROW2_COL8,          KEY_PAGE_LOWER,
    273,            367,            46,         36,         KEY_ROW2_COL9,          KEY_PAGE_LOWER,
    49,             405,            30,         36,         KEY_ROW3_COL1,          KEY_PAGE_LOWER,
    81,             405,            30,         36,         KEY_ROW3_COL2,          KEY_PAGE_LOWER,
    113,            405,            30,         36,         KEY_ROW3_COL3,          KEY_PAGE_LOWER,
    145,            405,            30,         36,         KEY_ROW3_COL4,          KEY_PAGE_LOWER,
    177,            405,            30,         36,         KEY_ROW3_COL5,          KEY_PAGE_LOWER,
    209,            405,            30,         36,         KEY_ROW3_COL6,          KEY_PAGE_LOWER,
    241,            405,            30,         36,         KEY_ROW3_COL7,          KEY_PAGE_LOWER,
    0,              405,            47,         36,         KEY_CAP_SYMBOL,         KEY_PAGE_LOWER,
    273,            405,            46,         36,         KEY_BACKSPACE,          KEY_PAGE_LOWER,
    0,              443,            79,         36,         KEY_NUMBER,             KEY_PAGE_LOWER,
    81,             443,            238,        36,         KEY_SPACE,              KEY_PAGE_LOWER,
    0,              329,            31,         36,         KEY_ROW1_COL1,          KEY_PAGE_NUMBER,
    33,             329,            30,         36,         KEY_ROW1_COL2,          KEY_PAGE_NUMBER,
    65,             329,            30,         36,         KEY_ROW1_COL3,          KEY_PAGE_NUMBER,
    97,             329,            30,         36,         KEY_ROW1_COL4,          KEY_PAGE_NUMBER,
    129,            329,            30,         36,         KEY_ROW1_COL5,          KEY_PAGE_NUMBER,
    161,            329,            30,         36,         KEY_ROW1_COL6,          KEY_PAGE_NUMBER,
    193,            329,            30,         36,         KEY_ROW1_COL7,          KEY_PAGE_NUMBER,
    225,            329,            30,         36,         KEY_ROW1_COL8,          KEY_PAGE_NUMBER,
    257,            329,            30,         36,         KEY_ROW1_COL9,          KEY_PAGE_NUMBER,
    289,            329,            30,         36,         KEY_ROW1_COL10,         KEY_PAGE_NUMBER,
    0,              367,            47,         36,         KEY_ROW2_COL1,          KEY_PAGE_NUMBER,
    49,             367,            30,         36,         KEY_ROW2_COL2,          KEY_PAGE_NUMBER,
    81,             367,            30,         36,         KEY_ROW2_COL3,          KEY_PAGE_NUMBER,
    113,            367,            30,         36,         KEY_ROW2_COL4,          KEY_PAGE_NUMBER,
    145,            367,            30,         36,         KEY_ROW2_COL5,          KEY_PAGE_NUMBER,
    177,            367,            30,         36,         KEY_ROW2_COL6,          KEY_PAGE_NUMBER,
    209,            367,            30,         36,         KEY_ROW2_COL7,          KEY_PAGE_NUMBER,
    241,            367,            30,         36,         KEY_ROW2_COL8,          KEY_PAGE_NUMBER,
    273,            367,            46,         36,         KEY_ROW2_COL9,          KEY_PAGE_NUMBER,
    49,             405,            30,         36,         KEY_ROW3_COL1,          KEY_PAGE_NUMBER,
    81,             405,            30,         36,         KEY_ROW3_COL2,          KEY_PAGE_NUMBER,
    113,            405,            30,         36,         KEY_ROW3_COL3,          KEY_PAGE_NUMBER,
    145,            405,            30,         36,         KEY_ROW3_COL4,          KEY_PAGE_NUMBER,
    177,            405,            30,         36,         KEY_ROW3_COL5,          KEY_PAGE_NUMBER,
    209,            405,            30,         36,         KEY_ROW3_COL6,          KEY_PAGE_NUMBER,
    241,            405,            30,         36,         KEY_ROW3_COL7,          KEY_PAGE_NUMBER,
    0,              405,            47,         36,         KEY_CAP_SYMBOL,         KEY_PAGE_NUMBER,
    273,            405,            46,         36,         KEY_BACKSPACE,          KEY_PAGE_NUMBER,
    0,              443,            79,         36,         KEY_LETTER,             KEY_PAGE_NUMBER,
    81,             443,            238,        36,         KEY_SPACE,              KEY_PAGE_NUMBER,
    0,              329,            31,         36,         KEY_ROW1_COL1,          KEY_PAGE_SYMBOL,
    33,             329,            30,         36,         KEY_ROW1_COL2,          KEY_PAGE_SYMBOL,
    65,             329,            30,         36,         KEY_ROW1_COL3,          KEY_PAGE_SYMBOL,
    97,             329,            30,         36,         KEY_ROW1_COL4,          KEY_PAGE_SYMBOL,
    129,            329,            30,         36,         KEY_ROW1_COL5,          KEY_PAGE_SYMBOL,
    161,            329,            30,         36,         KEY_ROW1_COL6,          KEY_PAGE_SYMBOL,
    193,            329,            30,         36,         KEY_ROW1_COL7,          KEY_PAGE_SYMBOL,
    225,            329,            30,         36,         KEY_ROW1_COL8,          KEY_PAGE_SYMBOL,
    257,            329,            30,         36,         KEY_ROW1_COL9,          KEY_PAGE_SYMBOL,
    289,            329,            30,         36,         KEY_ROW1_COL10,         KEY_PAGE_SYMBOL,
    0,              367,            47,         36,         KEY_ROW2_COL1,          KEY_PAGE_SYMBOL,
    49,             367,            30,         36,         KEY_ROW2_COL2,          KEY_PAGE_SYMBOL,
    81,             367,            30,         36,         KEY_ROW2_COL3,          KEY_PAGE_SYMBOL,
    113,            367,            30,         36,         KEY_ROW2_COL4,          KEY_PAGE_SYMBOL,
    145,            367,            30,         36,         KEY_ROW2_COL5,          KEY_PAGE_SYMBOL,
    177,            367,            30,         36,         KEY_ROW2_COL6,          KEY_PAGE_SYMBOL,
    209,            367,            30,         36,         KEY_ROW2_COL7,          KEY_PAGE_SYMBOL,
    241,            367,            30,         36,         KEY_ROW2_COL8,          KEY_PAGE_SYMBOL,
    273,            367,            46,         36,         KEY_ROW2_COL9,          KEY_PAGE_SYMBOL,
    49,             405,            30,         36,         KEY_ROW3_COL1,          KEY_PAGE_SYMBOL,
    81,             405,            30,         36,         KEY_ROW3_COL2,          KEY_PAGE_SYMBOL,
    113,            405,            30,         36,         KEY_ROW3_COL3,          KEY_PAGE_SYMBOL,
    145,            405,            30,         36,         KEY_ROW3_COL4,          KEY_PAGE_SYMBOL,
    177,            405,            30,         36,         KEY_ROW3_COL5,          KEY_PAGE_SYMBOL,
    209,            405,            30,         36,         KEY_ROW3_COL6,          KEY_PAGE_SYMBOL,
    241,            405,            30,         36,         KEY_ROW3_COL7,          KEY_PAGE_SYMBOL,
    0,              405,            47,         36,         KEY_CAP_SYMBOL,         KEY_PAGE_SYMBOL,
    273,            405,            46,         36,         KEY_BACKSPACE,          KEY_PAGE_SYMBOL,
    0,              443,            79,         36,         KEY_LETTER,             KEY_PAGE_SYMBOL,
    81,             443,            238,        36,         KEY_SPACE,              KEY_PAGE_SYMBOL,
    0,              329,            31,         36,         KEY_ROW1_COL1,          KEY_PAGE_0_F,
    33,             329,            30,         36,         KEY_ROW1_COL2,          KEY_PAGE_0_F,
    65,             329,            30,         36,         KEY_ROW1_COL3,          KEY_PAGE_0_F,
    97,             329,            30,         36,         KEY_ROW1_COL4,          KEY_PAGE_0_F,
    129,            329,            30,         36,         KEY_ROW1_COL5,          KEY_PAGE_0_F,
    161,            329,            30,         36,         KEY_ROW1_COL6,          KEY_PAGE_0_F,
    193,            329,            30,         36,         KEY_ROW1_COL7,          KEY_PAGE_0_F,
    225,            329,            30,         36,         KEY_ROW1_COL8,          KEY_PAGE_0_F,
    257,            329,            30,         36,         KEY_ROW1_COL9,          KEY_PAGE_0_F,
    289,            329,            30,         36,         KEY_ROW1_COL10,         KEY_PAGE_0_F,
    0,              367,            47,         36,         KEY_ROW2_COL1,          KEY_PAGE_0_F,
    49,             367,            30,         36,         KEY_ROW2_COL2,          KEY_PAGE_0_F,
    81,             367,            30,         36,         KEY_ROW2_COL3,          KEY_PAGE_0_F,
    113,            367,            30,         36,         KEY_ROW2_COL4,          KEY_PAGE_0_F,
    145,            367,            30,         36,         KEY_ROW2_COL5,          KEY_PAGE_0_F,
    177,            367,            30,         36,         KEY_ROW2_COL6,          KEY_PAGE_0_F,
    209,            367,            30,         36,         KEY_ROW2_COL7,          KEY_PAGE_0_F,
    241,            367,            30,         36,         KEY_ROW2_COL8,          KEY_PAGE_0_F,
    273,            367,            46,         36,         KEY_ROW2_COL9,          KEY_PAGE_0_F,
    49,             405,            30,         36,         KEY_ROW3_COL1,          KEY_PAGE_0_F,
    81,             405,            30,         36,         KEY_ROW3_COL2,          KEY_PAGE_0_F,
    113,            405,            30,         36,         KEY_ROW3_COL3,          KEY_PAGE_0_F,
    145,            405,            30,         36,         KEY_ROW3_COL4,          KEY_PAGE_0_F,
    177,            405,            30,         36,         KEY_ROW3_COL5,          KEY_PAGE_0_F,
    209,            405,            30,         36,         KEY_ROW3_COL6,          KEY_PAGE_0_F,
    241,            405,            30,         36,         KEY_ROW3_COL7,          KEY_PAGE_0_F,
    0,              405,            47,         36,         KEY_CAP_SYMBOL,         KEY_PAGE_0_F,
    273,            405,            46,         36,         KEY_BACKSPACE,          KEY_PAGE_0_F,
    0,              443,            79,         36,         KEY_LETTER,             KEY_PAGE_0_F,
    81,             443,            238,        36,         KEY_SPACE,              KEY_PAGE_0_F,

};
/*-------------------------------------------------------------------------------
    DATA
-------------------------------------------------------------------------------*/
//! Upper layer keyset state structure
static HalKeysetState_t     halKeysetAppStates;

//! Upper layer callback
static HalKeysetCallback_t  halKeysetAppCallback;

//! Event timer reload values
static HalKeyTimers_t       halKeyTimerReloads[NB_OF_KEYS];

//! Event timers current values
static HalKeyTimers_t       halKeyTimers[NB_OF_KEYS];

static TS_StateTypeDef activeTouch;

static int8u swipeDetected = 0;
static int8u SwipeInProgress = FALSE;


/*-------------------------------------------------------------------------------
    DECLARATIONS
-------------------------------------------------------------------------------*/
static void HalProcessKeyStates(int32u debouncedStates);
void HAL_KeyClearCurrentState ( KbhKeyId_e keyId );
static int32u Keyb_DetectTouchZone(void);
static int8u GetDefinedKeyCount(void);
static int8u GetDefinedKeyboardKeyCount(void);
static void BSP_TS_GetState(TS_StateTypeDef* TsState);
#ifdef SMCD362
static void Manage_Off_Button(GPIO_PinState pinState);
#endif

/*-------------------------------------------------------------------------------
    IMPLEMENTATION
-------------------------------------------------------------------------------*/
/**
 * HAL_KeysetInit: Initializes the keyset interface and driver in a default
 * state.
 */
void HAL_KeysetInit ( void )
{
    int8u i=0;

    // Clear upper layer callback
    halKeysetAppCallback = NULL;

    // Clear upper keyset states
    for ( i = 0; i < NB_OF_KEYS; i++ ) {
        HAL_KeyClearCurrentState((KbhKeyId_e)i);
    }
    GUI_ButtonInit();

#ifdef SMCD362
    //In Double Pole, configure the Tact switch for the Off Mode toggling
    {
        GPIO_InitTypeDef GPIO_InitStruct;

        GPIO_InitStruct.Pin = OFF_MODE_SWITCH_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
        GPIO_InitStruct.Pull = GPIO_PULLUP;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
        HAL_GPIO_Init(OFF_MODE_SWITCH_Port, &GPIO_InitStruct);
    }
#endif
}

/**
 * HAL_KeyClearCurrentState: Clear the current keys state
 */
void HAL_KeyClearCurrentState ( KbhKeyId_e keyId )
{
    halKeysetAppStates[keyId].press = 0;
    halKeysetAppStates[keyId].hold = 0;
    halKeysetAppStates[keyId].repeat = 0;
    halKeysetAppStates[keyId].swipe = 0;
    HAL_KeysetClearKeyTimers(keyId);
}

/**
 * HAL_TouchScreenTask
 */
OSTASK_DEF(HAL_TouchScreenTask)
{
    static int32u debounced_states;
    static int8u swipe = 0;
    static TS_StateTypeDef origin;
    static uint8_t wakeUpCnt = 0;
    int16s deltaX;
    int16s deltaY;
    int8u directionX;
    int8u directionY;

    activeTouch.TouchDetected = 0;
    BSP_TS_GetState(&activeTouch);
    if ((activeTouch.TouchDetected != 0) && (activeTouch.TouchDetected != 0xFF))
    {
#ifndef UI_ENABLE
    HAL_NVIC_SystemReset();
#endif
        if (MENU_IDLE == MNH_GetActiveMenu())
        {
            wakeUpCnt++;
            if (wakeUpCnt >= MAX_WAKE_UP_COUNT)
            {
                CONSOLE_EVENT_MESSAGE("Exit IDLE from display touch event");
                DBTHRD_SetData(DBTYPE_USER_ACTION,(void*)0, INDEX_DONT_CARE, DBCHANGE_WAKEUP_FROM_SLEEP);
            }
        }
        else
        {
            wakeUpCnt = 0;
            debounced_states = Keyb_DetectTouchZone();
            DBTHRD_SetData(DBTYPE_USER_ACTION,(void*)0, INDEX_DONT_CARE, DBCHANGE_LOCAL);
            if (debounced_states != 0)
            {
                if (swipe == 0)
                {
                    origin.X = activeTouch.X;
                    origin.Y = activeTouch.Y;
                }
                if (swipe < 3)
                {
                    swipe++;
                }
            }
            ParseButton (activeTouch.X, activeTouch.Y);
        }
    }
    else
    {
        debounced_states = 0;
        swipe = 0;
        wakeUpCnt = 0;
        ParseButton ((coordinate_t)-1,(coordinate_t) -1);

    }

    if (swipe >= 3)
    {
        deltaX = activeTouch.X - origin.X;
        deltaY = activeTouch.Y - origin.Y;
        if (deltaX < 0)
        {
            deltaX = -deltaX;
            directionX = LEFT_SWIPE;
        }
        else
        {
            directionX = RIGHT_SWIPE;
        }
        if (deltaY < 0)
        {
            deltaY = -deltaY;
            directionY = UP_SWIPE;
        }
        else
        {
            directionY = DOWN_SWIPE;
        }
        if (deltaX > deltaY)
        {
            //Horizontal swipe
            if (deltaX > 25)
            {
                swipeDetected = directionX;
            }
            else
            {
                swipeDetected = NO_SWIPE;
            }
        }
        else
        {
            //Vertical swipe
            if (deltaY > 50)
            {
                swipeDetected = directionY;
            }
            else
            {
                swipeDetected = NO_SWIPE;
            }
        }
    }
    else
    {
        if (SwipeInProgress == FALSE)
        {
            swipeDetected = NO_SWIPE;
        }
    }
    HalProcessKeyStates(debounced_states);

#ifdef SMCD362
    Manage_Off_Button(HAL_GPIO_ReadPin(OFF_MODE_SWITCH_Port, OFF_MODE_SWITCH_Pin));
#endif

    return (OSTASK_IS_IDLE);
}

#ifdef SMCD362
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void Manage_Off_Button(GPIO_PinState pinState)
{
    static GPIO_PinState previousPinState = GPIO_PIN_RESET;
    if (pinState != previousPinState)
    {
        if (pinState == GPIO_PIN_RESET)
        {
            uint8_t offMode;
            DBTHRD_GetData(DBTYPE_OFF_MODE_SETTING,(void*)&offMode, THIS_THERMOSTAT);
            offMode ^= 1;
            DBTHRD_SetData(DBTYPE_OFF_MODE_SETTING,(void*)&offMode, THIS_THERMOSTAT, DBCHANGE_LOCAL);
        }
        previousPinState = pinState;
    }
}
#endif

/*******************************************************************************
* @brief  Detects which active zone as been touched
* @inputs None
* @retval int32u: detected zone
* @author Jean-Fran�ois Many
* @date   2016/07/15
*******************************************************************************/
static int32u Keyb_DetectTouchZone(void)
{
    int8u i;
    int8u activeMenu;
    int8u definedKeyNumber;

    definedKeyNumber = GetDefinedKeyCount();
    activeMenu = MNH_GetActiveMenu();
    for (i = 0; i < definedKeyNumber; i++)
    {
        if ((activeTouch.X >= TouchZones[i].startX) && (activeTouch.X <= (TouchZones[i].startX + TouchZones[i].width)) && (activeTouch.Y >= TouchZones[i].startY) && (activeTouch.Y <= (TouchZones[i].startY + TouchZones[i].height)))
        {
            if (activeMenu == TouchZones[i].activeMenu)
            {
                return (0x01 << TouchZones[i].bindedKey);
            }
        }
    }

    return 0;
}

/*******************************************************************************
* @brief  Detects which active keyboard key as been touched
* @inputs None
* @retval int32u: detected zone
* @author Jean-Fran�ois Many
* @date   2016/08/05
*******************************************************************************/
KbhKeyboard_e Keyb_DetectKeyboardTouchZone(TS_StateTypeDef keyboard_touch, KbhKeyboardPage_e page)
{
    int8u i;
    int8u definedKeyboardKeyNumber;

    definedKeyboardKeyNumber = GetDefinedKeyboardKeyCount();
    for (i = 0; i < definedKeyboardKeyNumber; i++)
    {
        if ((keyboard_touch.X >= KeyboardZone[i].startX) && (keyboard_touch.X <= (KeyboardZone[i].startX + KeyboardZone[i].width)) && (keyboard_touch.Y >= KeyboardZone[i].startY) && (keyboard_touch.Y <= (KeyboardZone[i].startY + KeyboardZone[i].height)))
        {
            if (page == KeyboardZone[i].activePage)
            {
                return (KeyboardZone[i].bindedKey);
            }
        }
    }

    return INVALID_KEY;
}

/*******************************************************************************
* @brief  Returns the number of defined keys
* @inputs None
* @retval int8u: number of defined keys
* @author Jean-Fran�ois Many
* @date   2016/07/28
*******************************************************************************/
static int8u GetDefinedKeyCount(void)
{
    return (sizeof(TouchZones) / sizeof(TOUCH_ZONE));
}

/*******************************************************************************
* @brief  Returns the number of defined keyboard keys
* @inputs None
* @retval int8u: number of defined keyboard keys
* @author Jean-Fran�ois Many
* @date   2016/05/05
*******************************************************************************/
static int8u GetDefinedKeyboardKeyCount(void)
{
    return (sizeof(KeyboardZone) / sizeof(KEYBOARD_ZONE));
}

/*******************************************************************************
* @brief  Retrieve the touch coordinates
* @inputs None
* @retval Touch_t: touch structure
* @author Jean-Fran�ois Many
* @date   2016/07/19
*******************************************************************************/
TS_StateTypeDef HAL_GetTouch(void)
{
    return (activeTouch);
}

/**
 * HAL_KeysetClearKeyTimers: Clears all key timers and reload values.
 *
 * @param keyId
 *   Key identifier
 *
 * @return
 *   none
 */
void HAL_KeysetClearKeyTimers ( KbhKeyId_e keyId )
{
    if ( keyId < NB_OF_KEYS ) {
        // Clear timers
        halKeyTimers[keyId].repeat = 0;
        halKeyTimers[keyId].hold = 0;

        // Clear reload values
        halKeyTimerReloads[keyId].repeat = 0;
        halKeyTimerReloads[keyId].hold = 0;
    }
}


/**
* HAL_KeysetSetKeyRepeatDelay: Configures the repeat event delay for the
 * specified key.
 *
 * @param keyId
 *   Key identifier
 * @param delay
 *   Delay value in tick value
 *
 * @return
 *   none
 */
void HAL_KeysetSetKeyRepeatDelay ( KbhKeyId_e keyId, int16u delay )
{
    if ( keyId < NB_OF_KEYS ) {
        // Set key repeat timer and reload value
        halKeyTimerReloads[keyId].repeat = delay;
        halKeyTimers[keyId].repeat = delay;
    }
}


/**
* HAL_KeysetSetKeyHoldDelay: Configures the hold event delay for the specified
 * key.
 *
 * @param keyId
 *   Key identifier
 * @param delay
 *   Delay value in tick value
 *
 * @return
 *   none
 */
void HAL_KeysetSetKeyHoldDelay ( KbhKeyId_e keyId, int16u delay )
{
    if ( keyId < NB_OF_KEYS ) {
        // Set key hold timer and reload value
        halKeyTimerReloads[keyId].hold = delay;
        halKeyTimers[keyId].hold = delay;
    }
}


/**
 * HAL_KeysetBind: Registers upper layer callback to be invoked on keyset state
 * change.
 *
 * @param callback
 *   Callback to be invoked upon state changes.
 *
 * @return
 *   none
 */
void HAL_KeysetBind ( HalKeysetCallback_t callback )
{
    // Set upper layer callback
    halKeysetAppCallback = callback;
}


/**
 * HAL_KeysetGetStatus:
 *
 * @return
 *
 */
HalKeyState_t HAL_KeysetGetStatus (KbhKeyId_e keyId)
{
    return (halKeysetAppStates[keyId]);
}

/**
 * HAL_AreAllKeysReleased:
 *
 * @return
 *
 */
int8u HAL_AreAllKeysReleased(void)
{
    //If key released or never pressed
    if ((activeTouch.TouchDetected == 0) || (activeTouch.TouchDetected == 0xFF))
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

/**
 * HalProcessKeyStates: Manage key tiemrs and events based on received
 * debounced key inputs.
 */
static void HalProcessKeyStates ( int32u debouncedStates )
{
    FLAG notifyUpperLayer = FALSE;
    KbhKeyId_e i = KEY_FIRST;

    // For all supported keys
    for ( i = KEY_FIRST; i < NB_OF_KEYS; i++ ) {

        // If key currenty pressed
        if ( debouncedStates & (0x01 << i) ) {

            // If transitioning from released to pressed
            if ( halKeysetAppStates[i].press == 0 ) {
                //
                // KEY PRESSED EVENT !!!
                //
                halKeysetAppStates[i].press = 1;

                // Start hold and repeat timers
                halKeyTimers[i].repeat = halKeyTimerReloads[i].repeat;
                halKeyTimers[i].hold = halKeyTimerReloads[i].hold;

                // We will notify the upper layer
                notifyUpperLayer = TRUE;
            }
            // Else key was already pressed
            else {
                // Check for swipe
                if (swipeDetected != 0)
                {
                    //
                    // KEY SWIPED EVENT !!!
                    //
                    SwipeInProgress = 1;
//                    halKeysetAppStates[i].swipeDirection = swipeDetected;
//
//                    // We will notify the upper layer
//                    notifyUpperLayer = TRUE;
                }
                // If repeat timer is running
                if ( halKeyTimers[i].repeat > 0 ) {
                    // Manage repeat timer
                    if ( halKeyTimers[i].repeat > HAL_KEYSET_TICK ) {
                        halKeyTimers[i].repeat -= HAL_KEYSET_TICK;
                    } else {
                        halKeyTimers[i].repeat = 0;
                    }
                    // If repeat timer just elapsed
                    if ( halKeyTimers[i].repeat == 0 ) {
                        //
                        // KEY REPEAT EVENT !!!
                        //
                        halKeysetAppStates[i].repeat = 1;

                        // Reset repeat timer for next repeat detection
                        halKeyTimers[i].repeat = halKeyTimerReloads[i].repeat;

                        // We will notify the upper layer
                        notifyUpperLayer = TRUE;
                    }
                    // Else repeat timer still running
                    else {
                        // Repeat is transitory event but it can be detected
                        // multiple times. Clear state once app has been
                        // notified no next repeat will be considered as a state
                        // change.
                        if ( halKeysetAppStates[i].repeat == 1 ) {
                            halKeysetAppStates[i].repeat = 0;
                            // We will notify the upper layer
                            notifyUpperLayer = TRUE;
                        }
                    // Endif
                    }
                // Endif
                }

                // If hold timer is running
                if ( halKeyTimers[i].hold > 0 ) {
                    // Manage hold timer
                    if ( halKeyTimers[i].hold > HAL_KEYSET_TICK ) {
                        halKeyTimers[i].hold -= HAL_KEYSET_TICK;
                    } else {
                        halKeyTimers[i].hold = 0;
                    }
                    // If hold timer just elapsed
                    if ( halKeyTimers[i].hold == 0 ) {
                        //
                        // KEY HOLD EVENT !!!
                        //
                        halKeysetAppStates[i].hold = 1;

                        // Do not reset timer, this is a 'one shot' type of event
                        // Hold state will be cleared in the release event.

                        // We will notify the upper layer
                        notifyUpperLayer = TRUE;
                    // Endif
                    }
                // Endif
                }
            // Endif
            }
        }
        // Else key currenty released
        else {
            // If transitionning from pressed to released
            if ( halKeysetAppStates[i].press == 1 ) {
                //
                // KEY RELEASE EVENT !!!
                //
                halKeysetAppStates[i].press = 0;
                if (SwipeInProgress)
                {
                    KBH_IgnoreKeyRelease(i);
                    if (HAL_AreAllKeysReleased())
                    {
                        halKeysetAppStates[KEY_SWIPE].swipe = 1;
                        halKeysetAppStates[KEY_SWIPE].swipeDirection = swipeDetected;
                        SwipeInProgress = 0;
                    }
                }
                else
                {
                    halKeysetAppStates[KEY_SWIPE].swipe = 0;
                }
                halKeysetAppStates[i].hold = 0;
                halKeysetAppStates[i].repeat = 0;

                // Stop hold and repeat timers
                halKeyTimers[i].repeat = 0;
                halKeyTimers[i].hold = 0;

                // We will notify the upper layer
                notifyUpperLayer = TRUE;
            // Endif
            }
        // Endif
        }
    // Endfor
    }

    // If changes to report
    if ( notifyUpperLayer == TRUE ) {
        // If upper layer listening to changes
        if ( halKeysetAppCallback != NULL ) {
            // Invoke registered callback
            halKeysetAppCallback(halKeysetAppStates);
        // Endif
        }
    // Endif
    }
}

/**
  * @brief  Returns status and positions of the touch screen.
  * @param  TsState: Pointer to touch screen current state structure
  */
static void BSP_TS_GetState(TS_StateTypeDef* TsState)
{
  uint8_t screenOrientation;
  KeyPad_ReadPanelState(TsState, &screenOrientation);

  TsState->X = TsState->X;
  if (screenOrientation == 0)
  {
    TsState->Y = 480 - TsState->Y;
  }
  else
  {
    TsState->Y = TsState->Y;
  }
}
