/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    images.c
* @date    2016/08/25
* @authors Jean-Fran�ois Many
* @brief   Images stored in external Flash
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include "images.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/



/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
const image_t advise_box = {
    272, 152, 4, ADVISE_BOX_ADD
};

const image_t advise_box_small = {
    272, 100, 4, ADVISE_BOX_SMALL_ADD
};

const image_t box_setpoint = {
    270, 100, 4, SETPOINT_BOX_ADD
};

const image_t logo_stelpro = {
    112, 151, 4, STELPRO_LOGO_ADD
};

const image_t blue_btn = {
    270, 42, 4, BLUE_BTN_ADD
};

const image_t grey_btn = {
    270, 42, 4, GREY_BTN_ADD
};

const image_t stelpro_btn = {
    270, 42, 4, STELPRO_BTN_ADD
};

const image_t grey_btn_short = {
    206, 42, 4, GREY_BTN_SHORT_ADD
};

const image_t transparent_box = {
    164, 30, 4, TRANSPARENT_BOX_ADD
};

const image_t grey_btn_reduced = {
    150, 30, 4, GREY_BTN_REDUCED_ADD
};

const image_t leave_btn = {
    65, 65, 4, LEAVE_BTN_ADD
};

const image_t manual_btn = {
    65, 65, 4, MANUAL_BTN_ADD
};

const image_t return_btn = {
    65, 65, 4, RETURN_BTN_ADD
};

const image_t sleep_btn = {
    65, 65, 4, SLEEP_BTN_ADD
};

const image_t wake_btn = {
    65, 65, 4, WAKE_BTN_ADD
};

const image_t blue_small_btn = {
    42, 42, 4, BLUE_SQUARE_BTN_ADD
};

const image_t pen_big_icon = {
    42, 42, 4, PEN_BIG_ADD
};

const image_t alert_icon = {
    32, 32, 4, ALERT_ICON_ADD
};

const image_t check_icon = {
    32, 32, 4, CHECK_ICON_ADD
};

const image_t settings_btn = {
    31, 32, 4, SETTINGS_BTN_ADD
};

const image_t leave_icon = {
    34, 28, 4, LEAVE_ICON_ADD
};

const image_t foot_icon = {
    20, 46, 4, FOOT_ICON_ADD
};

const image_t return_icon = {
    29, 28, 4, RETURN_ICON_ADD
};

const image_t pen_small_icon = {
    28, 28, 4, PEN_SMALL_ADD
};

const image_t sleep_icon = {
    28, 28, 4, SLEEP_ICON_ADD
};

const image_t lock_icon = {
    24, 32, 4, LOCK_ICON_ADD
};

const image_t manual_icon = {
    24, 32, 4, MANUAL_ICON_ADD
};

const image_t wake_icon = {
    27, 28, 4, WAKE_ICON_ADD
};

const image_t toggle_white = {
    32, 20, 4, TOGGLE_WHITE_ADD
};

const image_t toggle_blue = {
    32, 20, 4, TOGGLE_BLUE_ADD
};

const image_t back_icon = {
    16, 32, 4, BACK_ICON_ADD
};

const image_t next_icon = {
    16, 32, 4, NEXT_ICON_ADD
};

const image_t heat_off = {
    8, 24, 4, HEAT_OFF_ICON_ADD
};

const image_t heat_on = {
    8, 24, 4, HEAT_ON_ICON_ADD
};



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/



/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
