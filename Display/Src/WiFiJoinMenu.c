/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       WiFiJoinMenu.c

    \brief      Source code file for the WiFiJoinMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the WiFiJoinMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "WiFiJoinMenu.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   WiFiJoinEditState = EDIT_IDLE;
static WiFiJoinStatus_t WiFiJoinOutcome;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private functions definitions
*******************************************************************************/

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_WiFiJoin_Entry
*******************************************************************************/
void MNU_WiFiJoin_Entry(void)
{
    WiFiJoinEditState = EDIT_IN_PROGRESS;
    WiFiJoinOutcome = WIFI_JOIN_UNKNOWN;

    MNU_SetKeyAssigned(FALSE);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show Wi-Fi join outcome
    //
    DPH_DisplayWiFiJoin(WiFiJoinOutcome);

    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_WiFiJoin_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    WIFITHRD_GenerateNewRegistrationToken();
}

/*******************************************************************************
    MNU_WiFiJoin_Core

    Indicates the outcome of the WiFi Join process
*******************************************************************************/
void MNU_WiFiJoin_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_WiFiJoin_Commit, (void*)KEY_BACK);
        if (WiFiJoinOutcome != WIFI_JOIN_UNKNOWN)
        {
            KBH_SetKeyPressProperties(KEY_DONE, (FUNC)MNU_WiFiJoin_Commit, (void*)KEY_DONE);
        }
        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_WiFiJoin_Exit
*******************************************************************************/
void MNU_WiFiJoin_Exit(void)
{
    WiFiJoinEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_WiFiJoin_Commit
*******************************************************************************/
void MNU_WiFiJoin_Commit(KEYB_PARAM keyId)
{
    static KEYB_PARAM commitKey = NULL;

    if ( WiFiJoinEditState == EDIT_IN_PROGRESS )
    {
        commitKey = keyId;
        KBH_ClearAllKeyProperties();
        switch (commitKey)
        {
            case KEY_DONE:
            case KEY_BACK:
            default:
                MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_WiFiJoin_Commit, (KEYB_PARAM*)commitKey, IMMEDIATELY);
                break;
        }
        DPH_DisplayWiFiJoin(WiFiJoinOutcome);
        WiFiJoinEditState = EDIT_COMMITTING;
    }
    else if ( WiFiJoinEditState == EDIT_COMMITTING )
    {
        WiFiJoinEditState = EDIT_IDLE;
        switch (commitKey)
        {
            case KEY_BACK:
                MNH_SetActiveMenu(MENU_WIFI_SETUP);
                break;

            case KEY_DONE:
                if (WiFiJoinOutcome == WIFI_JOIN_SUCCESS)
                {
                    MNH_SetActiveMenu(MENU_REGISTER);
                }
                else
                {
                    MNH_SetActiveMenu(MENU_WIFI_SETUP);
                }
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
    }
}

/*******************************************************************************
    WiFiJoin_Callback
    Callback by the WiFi module after joining attempt
*******************************************************************************/
void WiFiJoin_Callback (WiFiJoinStatus_t status)
{
    if (MNH_GetActiveMenu() == MENU_WIFI_JOIN)
    {
        switch (status)
        {
            case WIFI_JOIN_SUCCESS:
                WiFiJoinOutcome = WIFI_JOIN_SUCCESS;
                break;

            case WIFI_JOIN_BAD_PASSWORD:
                WiFiJoinOutcome = WIFI_JOIN_BAD_PASSWORD;
                break;

            case WIFI_JOIN_NO_NETWORK:
                WiFiJoinOutcome = WIFI_JOIN_NO_NETWORK;
                break;

            case WIFI_JOIN_GENERIC_ERROR:
            default:
                WiFiJoinOutcome = WIFI_JOIN_GENERIC_ERROR;
                break;
        }
        KBH_ClearAllKeyProperties();
        MNU_SetKeyAssigned(FALSE);
        DPH_DisplayWiFiJoin(WiFiJoinOutcome);
        OS_StartTask(TSK_APP_MNH_MENU);
    }
}