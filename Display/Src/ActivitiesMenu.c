/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2016, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       ActivitiesMenu.c

    \brief      Source code file for the ActivitiesMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the ActivitiesMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include <ctype.h>
#include <string.h>
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "APP_SetpointManager.h"
#include "ActivitiesMenu.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\HAL\inc\Display.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/


/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_CONFIRM,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    extern dbType_AllActivities_t       DefinedActivities;

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   ActivitiesEditState = EDIT_IDLE;
static uint8_t          ActivityNumber;
static uint8_t          Page;
static uint8_t          SelectedActivity;
static uint8_t          CheckPosition;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/
    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void NextPage(void);
static void PreviousPage(void);
static int8_t my_character_compare(const char a, const char b);
int8_t my_string_compare(const char* a, const char* b);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    NextPage

    Switch to the next page of the connected thermostats
*******************************************************************************/
static void NextPage(void)
{
    Page++;
    HAL_ClearScreen();
    KBH_ClearAllKeyProperties();

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show Activities list
    //
    DPH_DisplayActivities(Page, CheckPosition);
}

/*******************************************************************************
    PreviousPage

    Switch to the previous page of the connected thermostats
*******************************************************************************/
static void PreviousPage(void)
{
    Page--;
    HAL_ClearScreen();
    KBH_ClearAllKeyProperties();

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show Activities list
    //
    DPH_DisplayActivities(Page, CheckPosition);
}

/*******************************************************************************
    my_character_compare
    Compare two characters between them (case insensitive)
*******************************************************************************/
static int8_t my_character_compare(const char a, const char b)
{
    int8_t my_result;

    my_result = tolower(a) - tolower(b);

    if ((my_result == 0) && (a != b))
    {
        if (islower(a))
        {
            return (-1);
        }
        else
        {
            return (1);
        }
    }

    return (my_result);
}

/*******************************************************************************
    my_string_compare
    Compare two strings between them
*******************************************************************************/
int8_t my_string_compare(const char* a, const char* b)
{
    int8_t my_result;

    my_result = my_character_compare(*a, *b);

    while ((my_result == 0) && (*a != 0))
    {
        my_result = my_character_compare(*++a, *++b);
    }

    return my_result;
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_Activities_Entry
*******************************************************************************/
void MNU_Activities_Entry(void)
{
    ActivitiesEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);
    ActivityNumber = 0;
    SelectedActivity = 0;
    CheckPosition = 0;
    Page = 0;

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show Activities list
    //
    DPH_DisplayActivities(Page, CheckPosition);

	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Activities_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_Activities_Core

    Show the activity list
*******************************************************************************/
void MNU_Activities_Core(void)
{
    uint8_t activity;
    uint8_t activityName[NAME_SIZE_AND_NULL];

    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure key in the Activities menu
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_Activities_Commit, (void*)KEY_BACK);
        ActivityNumber = 0;
        for (activity = 0; activity < ACTIVITY_INSTANCES; activity++)
        {
            DBTHRD_GetData(DBTYPE_ACTIVITY_NAME, &activityName, activity);
            if (activityName[0] != NULL)
            {
                ActivityNumber++;
            }
        }

        if ((ActivityNumber - (Page * 7)) > 0)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_1, (FUNC)MNU_Activities_Commit, (void*) KEY_OPTIONS_1);
        }
        if ((ActivityNumber - (Page * 7)) > 1)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)MNU_Activities_Commit, (void*) KEY_OPTIONS_2);
        }
        if ((ActivityNumber - (Page * 7)) > 2)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)MNU_Activities_Commit, (void*) KEY_OPTIONS_3);
        }
        if ((ActivityNumber - (Page * 7)) > 3)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_4, (FUNC)MNU_Activities_Commit, (void*) KEY_OPTIONS_4);
        }
        if ((ActivityNumber - (Page * 7)) > 4)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_5, (FUNC)MNU_Activities_Commit, (void*) KEY_OPTIONS_5);
        }
        if ((ActivityNumber - (Page * 7)) > 5)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_6, (FUNC)MNU_Activities_Commit, (void*) KEY_OPTIONS_6);
        }
        if ((ActivityNumber - (Page * 7)) > 6)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_7, (FUNC)MNU_Activities_Commit, (void*) KEY_OPTIONS_7);
        }
        if (ActivityNumber > (7 * (Page + 1)))
        {
            KBH_SetKeyPressProperties(KEY_NEXT, (FUNC)NextPage, (void*) KEY_NEXT);
        }
        if (Page > 0)
        {
            KBH_SetKeyPressProperties(KEY_PREVIOUS, (FUNC)PreviousPage, (void*) KEY_PREVIOUS);
        }

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_Activities_Exit
*******************************************************************************/
void MNU_Activities_Exit(void)
{
    ActivitiesEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_Activities_Commit
*******************************************************************************/
void MNU_Activities_Commit(KEYB_PARAM keyId)
{
    uint8_t i;
    MnuSortedActivities_t sortedActivities[ACTIVITY_INSTANCES];
    uint8_t ActivityStartArray[ACTIVITY_INSTANCES] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    for (i = 0; i < ACTIVITY_INSTANCES; i++)
    {
        memset(&sortedActivities[i].position, 0, 1);
    }

    if (ActivitiesEditState == EDIT_IN_PROGRESS)
    {
        KBH_ClearAllKeyProperties();
        switch(keyId)
        {
            case KEY_BACK:
                Page = 0;
                MNH_SetActiveMenu(MENU_MODE_SELECTION);
                break;

            case KEY_OPTIONS_1:
            case KEY_OPTIONS_2:
            case KEY_OPTIONS_3:
            case KEY_OPTIONS_4:
            case KEY_OPTIONS_5:
            case KEY_OPTIONS_6:
            case KEY_OPTIONS_7:
                MNU_Activities_SortActivities(&sortedActivities[0]);
                SelectedActivity = Page * 7 + (keyId - KEY_OPTIONS_1);
                CheckPosition = keyId - KEY_OPTIONS_1 + 1;
                ActivityStartArray[sortedActivities[SelectedActivity].position] = 1;
                DBTHRD_SetData(DBTYPE_ACTIVITY_START,(void*)&ActivityStartArray, INDEX_DONT_CARE, DBCHANGE_ACTIVITY_LOCAL);
                ActivitiesEditState = EDIT_CONFIRM;
                //
                //  Show Activities list
                //
                DPH_DisplayActivities(Page, CheckPosition);
                MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Activities_Commit,  (KEYB_PARAM*)KEY_HOME, CONFIRM_TIMEOUT);
                break;

            default:
                Page = 0;
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
    }
    else if (ActivitiesEditState == EDIT_CONFIRM)
    {
        MNH_SetActiveMenu(MENU_HOME);
    }
}

/*******************************************************************************
    MNU_Activities_SortActivities
    Sort the activities alphabetically
*******************************************************************************/
uint8_t MNU_Activities_SortActivities(MnuSortedActivities_t* pSortedActivities)
{
    uint8_t activity;
    uint8_t definedActivityNumber = 0;
    uint8_t activityName[NAME_SIZE_AND_NULL];
    uint8_t posToCheck;
    uint8_t othersToCompare;

    for (activity = 0; activity < ACTIVITY_INSTANCES; activity++)
    {
        DBTHRD_GetData(DBTYPE_ACTIVITY_NAME, &activityName, activity);
        if (activityName[0] != 0)
        {
            pSortedActivities[definedActivityNumber].position = activity;
            strcpy((char*)pSortedActivities[definedActivityNumber].str, (char const*)activityName);
            definedActivityNumber++;
        }
    }
    if (definedActivityNumber != 0)
    {
        //Sort activities alphabetically
        for (posToCheck = 0; posToCheck < definedActivityNumber; posToCheck++)
        {
            for (othersToCompare = (posToCheck + 1); othersToCompare < definedActivityNumber; othersToCompare++)
            {
                if (my_string_compare((char const*)pSortedActivities[posToCheck].str, (char const*)pSortedActivities[othersToCompare].str) > 0)
                {
                    activity = pSortedActivities[posToCheck].position;
                    strcpy((char*)activityName, (char const*)pSortedActivities[posToCheck].str);
                    pSortedActivities[posToCheck].position = pSortedActivities[othersToCompare].position;
                    strcpy((char*)pSortedActivities[posToCheck].str, (char const*)pSortedActivities[othersToCompare].str);
                    pSortedActivities[othersToCompare].position = activity;
                    strcpy((char*)pSortedActivities[othersToCompare].str, (char const*)activityName);
                }
            }
        }
    }
    return definedActivityNumber;
}
