/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       OsQueue.c

    \brief      Source code file for the OS queue module.

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the OS queue implementation.

*******************************************************************************/

/*-------------------------------------------------------------------------------
    INCLUDE
-------------------------------------------------------------------------------*/
#include "typedef.h"
#include "OsQueue.h"
#include "HAL_Interrupt.h"


/*-------------------------------------------------------------------------------
    DEFINITIONS
-------------------------------------------------------------------------------*/
// none


/*-------------------------------------------------------------------------------
    DATA
-------------------------------------------------------------------------------*/
// none


/*-------------------------------------------------------------------------------
    DECLARATIONS
-------------------------------------------------------------------------------*/
// none


/*-------------------------------------------------------------------------------
    IMPLEMENTATION
-------------------------------------------------------------------------------*/

/**
 * @brief Initializes a queue structure so it is ready to be managed.
 *
 * @param q
 * Queue instance pointer
 * @param array
 * Queue data buffer address
 * @param sizeOfElement
 * Size of the element that will stored in this queue (all the same size)
 * @param maxNumOfElement
 * Max depth of the queue
 */
void OS_QueueInit ( OsQueue_t *q, void *array, U8BIT sizeOfElement, U16BIT maxNumOfElement)
{
    // Enter critical
    BSP_InterruptDeclareCritical();
    BSP_InterruptEnterCritical();
    {
        q->array = array;
        q->numOfElement = 0;
        q->sizeOfElement = sizeOfElement;
        q->maxNumOfElement = maxNumOfElement;
        q->head = array;
        q->tail = array;
    }
    // Leave critical
    BSP_InterruptExitCritical();
}


/**
 * @brief Adds an element at the tail of the queue.
 *
 * @param q
 *   Queue instance pointer
 * @param element
 *   Pointer to data element to be added
 *
 * @return bool
 *   Indicate if element has been added or not
 */
FLAG OS_QueuePush ( OsQueue_t *q, void *element)
{
    U8BIT i, *p_src, *p_dst;
    FLAG pushed = FALSE;

    // Enter critical
    BSP_InterruptDeclareCritical();
    BSP_InterruptEnterCritical();
    {
        // If room in the queue to store this element
        if ( q->numOfElement < q->maxNumOfElement)
        {
            // Copy element in queue array
            p_src = ( U8BIT * ) element;
            p_dst = ( U8BIT * ) q->tail;
            for ( i = 0; i < q->sizeOfElement; i++ )
            {
                *p_dst = *p_src;
                p_src++;
                p_dst++;
            }

            // Queue now contains one more element
            q->numOfElement++;

            // Increment tail pointer and consider circular wrap around
            q->tail = p_dst;
            if ( ( U8BIT * ) q->tail >= ( ( U8BIT * ) q->array + ( q->sizeOfElement * q->maxNumOfElement ) ) )
            {
                q->tail = q->array;
            }

            // Indicate that the element has been added
            pushed = TRUE;
        // Endif
        }
    }
    // Leave critical
    BSP_InterruptExitCritical();

    return ( pushed );
}


/**
 * @brief Returns the number of element sitting in the queue.
 *
 * @param q
 *   Queue instance pointer
 *
 * @return numOfElements
 *   The number of element sitting in the queue
 */
U16BIT OS_QueueChk ( OsQueue_t *q )
{
    U16BIT numOfElement;

    // Enter critical
    BSP_InterruptDeclareCritical();
    BSP_InterruptEnterCritical();
    {
        numOfElement = q->numOfElement;
    }
    // Leave critical
    BSP_InterruptExitCritical();

    return ( numOfElement );
}


/**
 * @brief Retrieves an element from the front of the queue.
 *
 * @param q
 *   Queue instance pointer
 * @param element
 *   Pointer where to copy retrieved element.
 *
 * @return bool
 *   Indicate if an element has ben retrieved or not
 */
FLAG OS_QueuePop ( OsQueue_t *q, void *element )
{
    U8BIT i, *p_src, *p_dst;
    FLAG popped = FALSE;

    // Enter critical
    BSP_InterruptDeclareCritical();
    BSP_InterruptEnterCritical();
    {
        // If there is at least 1 element to pop
        if (q->numOfElement != 0)
        {
            // Copy element in caller's buffer
            p_src = (U8BIT*)q->head;
            p_dst = (U8BIT*)element;
            for ( i = 0; i < q->sizeOfElement; i++ )
            {
                *p_dst = *p_src;
                p_dst++;
                p_src++;
            }

            // One element has been removed from the queue
            q->numOfElement --;

            // Increment head pointer and consider circular wrap around
            q->head = p_src;
            if ( ( U8BIT *) q->head >= ( ( U8BIT *) q->array + ( q->sizeOfElement * q->maxNumOfElement ) ) )
            {
                q->head = q->array;
            }

            // Indicate that an element has been removed
            popped = TRUE;
        // Endif
        }
    }
    // Leave critical
    BSP_InterruptExitCritical();

    return ( popped );
}


/**
 * @brief Removes all items from a queue.
 *
 * @param q
 *   Queue instance pointer
 */
void OS_QueueFlush ( OsQueue_t *q )
{
    // Enter critical
    BSP_InterruptDeclareCritical();
    BSP_InterruptEnterCritical();
    {
        // Clear element counter and reset queue tail and head pointers
        q->numOfElement = 0;
        q->head = q->array;
        q->tail = q->array;
    }
    // Leave critical
    BSP_InterruptExitCritical();
}



