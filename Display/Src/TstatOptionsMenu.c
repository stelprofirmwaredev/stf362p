/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2017, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       TstatOptionsMenu.c

    \brief      Source code file for the TstatOptionsMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the TstatOptionsMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "TstatOptionsMenu.h"
#include "GroupDetailsMenu.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\ZigBee\inc\THREAD_ZigBee.h"

#include ".\DB\classes\inc\class_Thermostats.h"


/*******************************************************************************
    Private constants definitions
*******************************************************************************/


/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;


/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   TstatOptionsEditState = EDIT_IDLE;
static susbscriptionHandle_t hThermostatRemovedNotification = DBTHRD_NOTIFHANDLE_NOTSET;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/
    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static DB_NOTIFICATION(OnThermostatRemoved);
static void RemoveTstatTimeout(void);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    OnThermostatRemoved notification
    Called when there is change in the Thermostat Model
    Need to check if the Model has been cleared (Thermostat removed)
    Or not (Thermostat reconfigured)
*******************************************************************************/
static DB_NOTIFICATION(OnThermostatRemoved)
{
    uint8_t tstat;
    uint8_t model[33];

    MNU_GroupDetails_GetSelectedTstat(&tstat);
    DBTHRD_GetData(DBTYPE_TSTAT_MODEL, &model, tstat);
    if (model[0] == NULL)
    {
        MNH_SetActiveMenu(MENU_GROUPDETAILS);
    }
}

/*******************************************************************************
    RemoveTstatTimeout
*******************************************************************************/
static void RemoveTstatTimeout(void)
{
    uint8_t tstat;

    MNU_GroupDetails_GetSelectedTstat(&tstat);
    MNH_SetActiveMenu(MENU_GROUPDETAILS);
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_TstatOptions_Entry
*******************************************************************************/
void MNU_TstatOptions_Entry(void)
{
    uint8_t tstat;
    TstatOptionsEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show tstat details
    //
    MNU_GroupDetails_GetSelectedTstat(&tstat);
    DPH_DisplayTstatOptions(tstat, FALSE, FALSE);

    hThermostatRemovedNotification = DBTHRD_SubscribeToNotification(DBTYPE_THERMOSTAT_REMOVED,
                                                       OnThermostatRemoved,
                                                       DBCHANGE_LOCAL,
                                                       INDEX_DONT_CARE);

	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_TstatOptions_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_TstatOptions_Core

    Allows to view the selected thermostat options
*******************************************************************************/
void MNU_TstatOptions_Core(void)
{
    uint8_t tstat;

    if (TstatOptionsEditState == EDIT_IN_PROGRESS)
    {
        if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
        {
            //
            // Configure keys
            //
            KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_TstatOptions_Commit, (void*)KEY_BACK);
            MNU_GroupDetails_GetSelectedTstat(&tstat);
            if (tstat != THIS_THERMOSTAT)
            {
                KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)MNU_TstatOptions_Commit, (void*)KEY_OPTIONS_2);
                KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)MNU_TstatOptions_Commit, (void*)KEY_OPTIONS_3);
            }

            MNU_SetKeyAssigned(TRUE);
        }
        else
        {
            if (IsKeyAssigned() == FALSE)
            {
                OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
            }
        }
    }
}

/*******************************************************************************
    MNU_TstatOptions_Exit
*******************************************************************************/
void MNU_TstatOptions_Exit(void)
{
    TstatOptionsEditState = EDIT_IDLE;
    DBTHRD_UnsubscribeToNotification(hThermostatRemovedNotification);
}

/*******************************************************************************
    MNU_TstatOptions_Commit
*******************************************************************************/
void MNU_TstatOptions_Commit(KEYB_PARAM keyId)
{
    uint8_t tstat;

	if (TstatOptionsEditState != EDIT_IDLE)
	{
        KBH_ClearAllKeyProperties();
        switch(keyId)
        {
            case KEY_BACK:
                MNH_SetActiveMenu(MENU_TSTAT_DETAILS);
                break;

            case KEY_OPTIONS_2:
                {
                    TstatOptionsEditState = EDIT_COMMITTING;
                    MNU_GroupDetails_GetSelectedTstat(&tstat);
                    ZBTHRD_ReconfigureZBStat(tstat);
                    DPH_DisplayTstatOptions(tstat, FALSE, TRUE);
                    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_TstatOptions_Commit, (KEYB_PARAM*)KEY_BACK, REMOVE_TSTAT_TIMEOUT);
                }
                break;

            case KEY_OPTIONS_3:
                {
                    TstatOptionsEditState = EDIT_COMMITTING;
                    MNU_GroupDetails_GetSelectedTstat(&tstat);
                    ZBTHRD_RemoveZBStat(tstat);
                    DPH_DisplayTstatOptions(tstat, TRUE, FALSE);
                    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)RemoveTstatTimeout, (KEYB_PARAM*)NULL, REMOVE_TSTAT_TIMEOUT);
                }
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
	}
}
