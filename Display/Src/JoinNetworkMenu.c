/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       JoinNetworkMenu.c

    \brief      Source code file for the JoinNetworkMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the JoinNetworkMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "JoinNetworkMenu.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\ZigBee\inc\THREAD_ZigBee.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   JoinNetworkEditState = EDIT_IDLE;
static JoinNetwork_t	JoinNetworkEditValue;
static JoinNetwork_t	JoinNetwork;
static NetworkChannel_t NetworkChannelEditValue;
static NetworkChannel_t NetworkChannel;
static uint8_t          JoinAttempt;
static susbscriptionHandle_t hNetworkJoinedNotification = DBTHRD_NOTIFHANDLE_NOTSET;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void ChangeJoinNetwork(KEYB_PARAM keyId);
static void ChangeNetworkChannel(KEYB_PARAM keyId);
static DB_NOTIFICATION(OnNetworkJoined);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    ChangeJoinNetwork

    Select the ZigBee joining method
*******************************************************************************/
static void ChangeJoinNetwork(KEYB_PARAM keyId)
{
    switch (keyId)
    {
        case KEY_OPTIONS_2:
            JoinNetworkEditValue = JOIN_AUTO;
            break;

        case KEY_OPTIONS_3:
            JoinNetworkEditValue = JOIN_SPECIFIC;
            break;

        default:
            break;
    }

    MNU_SetKeyAssigned(FALSE);
    DPH_DisplayJoinNetwork(JoinNetworkEditValue, NetworkChannelEditValue, JoinAttempt, FALSE);
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_JoinNetwork_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    ChangeNetworkChannel

    Change the network channel to join in
*******************************************************************************/
static void ChangeNetworkChannel(KEYB_PARAM keyId)
{
    switch (keyId)
    {
        case KEY_UP:
            if (NetworkChannelEditValue < CHANNEL_26)
            {
                NetworkChannelEditValue++;
            }
            break;

        case KEY_DOWN:
            if (NetworkChannelEditValue > CHANNEL_11)
            {
                NetworkChannelEditValue--;
            }
            break;

        default:
            break;
    }

    DPH_DisplayJoinNetwork(JoinNetworkEditValue, NetworkChannelEditValue, JoinAttempt, FALSE);
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_JoinNetwork_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    OnNetworkJoined notification
*******************************************************************************/
static DB_NOTIFICATION(OnNetworkJoined)
{
    dbType_ZigBeeNetworkInfo_t nwInfo;

    MNU_SetKeyAssigned(FALSE);

    DBTHRD_GetData(DBTYPE_ZIGBEE_NETWORK_INFO, &nwInfo, INDEX_DONT_CARE);
    if (nwInfo.InANetwork != 0)
    {
        JoinAttempt = JOIN_SUCCESS;
        DPH_DisplayJoinNetwork(JoinNetworkEditValue, (NetworkChannel_t)nwInfo.ActiveChannel, JoinAttempt, FALSE);
    }
    else
    {
        JoinAttempt = JOIN_FAILURE;
        DPH_DisplayJoinNetwork(JoinNetworkEditValue, NetworkChannelEditValue, JoinAttempt, FALSE);
    }

    KBH_SetKeyPressProperties(KEY_DONE, (FUNC)MNU_JoinNetwork_Commit, (void*)KEY_DONE);
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_JoinNetwork_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/
/*******************************************************************************
    MNU_JoinNetwork_Entry
*******************************************************************************/
void MNU_JoinNetwork_Entry(void)
{
    JoinNetworkEditValue = JoinNetwork;
    NetworkChannelEditValue = NetworkChannel;
    JoinAttempt = NOT_JOINED;
    JoinNetworkEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);

    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_JoinNetwork_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show Join Network options
    //
    DPH_DisplayJoinNetwork(JoinNetworkEditValue, NetworkChannelEditValue, JoinAttempt, TRUE);

    hNetworkJoinedNotification = DBTHRD_SubscribeToNotification(DBTYPE_ZIGBEE_NETWORK_INFO,
                                                       OnNetworkJoined,
                                                       DBCHANGE_ALLCHANGES,
                                                       INDEX_DONT_CARE);
}

/*******************************************************************************
    MNU_JoinNetwork_Core

    Allows to join a ZigBee network
*******************************************************************************/
void MNU_JoinNetwork_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_JoinNetwork_Commit, (void*)KEY_BACK);

        switch (JoinAttempt)
        {
            case NOT_JOINED:
                KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)ChangeJoinNetwork, (void*)KEY_OPTIONS_2);
                KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)ChangeJoinNetwork, (void*)KEY_OPTIONS_3);
                KBH_SetKeyPressProperties(KEY_OPTIONS_4, (FUNC)MNU_JoinNetwork_Commit, (void*)KEY_OPTIONS_4);
                if (JoinNetworkEditValue == JOIN_SPECIFIC)
                {
                    KBH_SetKeyPressProperties(KEY_UP, (FUNC)ChangeNetworkChannel, (void*)KEY_UP);
                    KBH_SetKeyPressProperties(KEY_DOWN, (FUNC)ChangeNetworkChannel, (void*)KEY_DOWN);
                }
                else
                {
                    KBH_SetKeyPressProperties(KEY_UP, (FUNC)NULL, (void*)NULL);
                    KBH_SetKeyPressProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL);
                }
                KBH_SetKeyPressProperties(KEY_DONE, (FUNC)NULL, (void*)NULL);
                break;

            case JOINING:
                KBH_SetKeyPressProperties(KEY_OPTIONS_4, (FUNC)NULL, (void*)NULL);
                KBH_SetKeyPressProperties(KEY_UP, (FUNC)NULL, (void*)NULL);
                KBH_SetKeyPressProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL);
                KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)NULL, (void*)NULL);
                KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)NULL, (void*)NULL);
                KBH_SetKeyPressProperties(KEY_DONE, (FUNC)NULL, (void*)NULL);
                break;

            case JOIN_SUCCESS:
                KBH_SetKeyPressProperties(KEY_OPTIONS_4, (FUNC)NULL, (void*)NULL);
                KBH_SetKeyPressProperties(KEY_UP, (FUNC)NULL, (void*)NULL);
                KBH_SetKeyPressProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL);
                KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)NULL, (void*)NULL);
                KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)NULL, (void*)NULL);
                break;

            case JOIN_FAILURE:
                KBH_SetKeyPressProperties(KEY_UP, (FUNC)NULL, (void*)NULL);
                KBH_SetKeyPressProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL);
                KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)NULL, (void*)NULL);
                KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)NULL, (void*)NULL);
                break;

            default:
                break;
        }

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_JoinNetwork_Exit
*******************************************************************************/
void MNU_JoinNetwork_Exit(void)
{
    JoinNetworkEditState = EDIT_IDLE;
    DBTHRD_UnsubscribeToNotification(hNetworkJoinedNotification);
}

/*******************************************************************************
    MNU_JoinNetwork_Init
*******************************************************************************/
void MNU_JoinNetwork_Init(void)
{
    MNU_JoinNetwork_SetJoinNetwork(JOIN_AUTO);
    MNU_JoinNetwork_SetNetworkChannel(CHANNEL_11);
}

/*******************************************************************************
    MNU_JoinNetwork_Commit
*******************************************************************************/
void MNU_JoinNetwork_Commit(KEYB_PARAM keyId)
{
    static KEYB_PARAM commitKey = NULL;
    dbType_ZigBeeJoinInfo_t joinInfo;

    if ( JoinNetworkEditState == EDIT_IN_PROGRESS )
    {
        commitKey = keyId;
        KBH_ClearAllKeyProperties();
        switch (commitKey)
        {
            case KEY_OPTIONS_4:
                MNU_JoinNetwork_SetJoinNetwork(JoinNetworkEditValue);
                MNU_JoinNetwork_SetNetworkChannel(NetworkChannelEditValue);
//TODO - enable router mode
                joinInfo.DeviceType = 1/*DEVICE_TYPE_ROUTER*/;
                joinInfo.Channel = NetworkChannelEditValue;
                joinInfo.JoinType = JoinNetworkEditValue;
                JoinAttempt = JOINING;
                DPH_DisplayJoinNetwork(JoinNetworkEditValue, NetworkChannelEditValue, JoinAttempt, FALSE);
                MNU_SetKeyAssigned(FALSE);
                DBTHRD_SetData(DBTYPE_ZIGBEE_JOIN_INFO,(void*)&joinInfo, INDEX_DONT_CARE, DBCHANGE_LOCAL);
                MNH_SetTimeoutCallback((MnhTimeoutCallback_t)OnNetworkJoined, (KEYB_PARAM*)KEY_HOME, ZIGBEE_ASSOCIATION_TIMEOUT);
                break;

            case KEY_DONE:
            case KEY_BACK:
            default:
                MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_JoinNetwork_Commit, (KEYB_PARAM*)commitKey, IMMEDIATELY);
                break;
        }
        JoinNetworkEditState = EDIT_COMMITTING;
    }
    else if ( JoinNetworkEditState == EDIT_COMMITTING )
    {
        JoinNetworkEditState = EDIT_IDLE;
        switch (keyId)
        {
            case KEY_BACK:
            case KEY_DONE:
                MNH_SetActiveMenu(MENU_ZIGBEE_SETUP);
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
    }
}

/*******************************************************************************
    MNU_JoinNetwork_GetJoinNetwork

    Return the current selected joining method
*******************************************************************************/
JoinNetwork_t MNU_JoinNetwork_GetJoinNetwork(void)
{
    return JoinNetwork;
}

/*******************************************************************************
    MNU_JoinNetwork_SetJoinNetwork

    Set the current joining method
*******************************************************************************/
void MNU_JoinNetwork_SetJoinNetwork(JoinNetwork_t join)
{
     JoinNetwork = join;
}

/*******************************************************************************
    MNU_JoinNetwork_GetNetworkChannel

    Return the current selected ZigBee channel
*******************************************************************************/
NetworkChannel_t MNU_JoinNetwork_GetNetworkChannel(void)
{
    return NetworkChannel;
}

/*******************************************************************************
    MNU_JoinNetwork_SetNetworkChannel

    Set the ZigBee network channel to join in
*******************************************************************************/
void MNU_JoinNetwork_SetNetworkChannel(NetworkChannel_t channel)
{
     NetworkChannel = channel;
}
