/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       RegisterMenu.c

    \brief      Source code file for the RegisterMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the RegisterMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include <string.h>

#include "typedef.h"

#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "RegisterMenu.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\WiFi\inc\THREAD_Wifi.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/
#define MAX_REGISTER_DELAY  5

/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   RegisterEditState = EDIT_IDLE;
static U8BIT            RegistrationToken[7];
static RegisterStatus_t RegistrationState;
static susbscriptionHandle_t hRegistrationChangeNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static U8BIT            registerNbMinutes;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static DB_NOTIFICATION(OnRegistrationChange);
static void MenuTimeout(void);
static void TokenShouldBeGenerated(void);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    MenuTimeout
*******************************************************************************/
static void MenuTimeout(void)
{
    registerNbMinutes++;
    if (registerNbMinutes >= MAX_REGISTER_DELAY)
    {
        registerNbMinutes = 0;
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Register_Commit, (KEYB_PARAM*)KEY_HOME, IMMEDIATELY);

    }
    else
    {
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MenuTimeout, (KEYB_PARAM*)NULL, WIFI_REGISTER_TIMEOUT);
    }
}

/*******************************************************************************
* @brief  Notification update when the registration status is changed
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/26
*******************************************************************************/
#pragma optimize = none
static DB_NOTIFICATION(OnRegistrationChange)
{
    dbType_CloudConnectivity_t cloudInfo;
    DBTHRD_GetData(DBTYPE_CLOUDCONNECTIVITY, (void *)&cloudInfo, INDEX_DONT_CARE);


    //When registered, we display registration success to the user
    if ((cloudInfo.isRegistered == CLOUD_REGISTERED) /*&& (RegisterStatusRequested == 1)*/)
    {
        RegistrationState = REG_REGISTERED;

        if (MNH_GetActiveMenu() == MENU_REGISTER)
        {
            KBH_ClearAllKeyProperties();
            OS_StartTask(TSK_APP_MNH_MENU);

            //
            //  Show Back arrow
            //
            DPH_DisplayBack();

            //
            //  Show registration token
            //
            DPH_DisplayRegister(RegistrationState, &RegistrationToken[0], FALSE);
        }
    }
    else
    {
        //Not registered, request a registration token
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)TokenShouldBeGenerated, (KEYB_PARAM*)NULL, GENERATE_TOKEN_TIMEOUT);
    }
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_Register_Entry
*******************************************************************************/
void MNU_Register_Entry(void)
{
    uint8_t previousMenu;
//    dbType_CloudConnectivity_t cloudInfo;

    previousMenu = MNH_GetPreviousMenu();
    registerNbMinutes = 0;
    RegistrationState = REG_UNKNOWN;

    RegisterEditState = EDIT_IN_PROGRESS;


    MNU_SetKeyAssigned(FALSE);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show registration token
    //
    DPH_DisplayRegister(RegistrationState, &RegistrationToken[0], TRUE);

    if (previousMenu == MENU_WIFI_SETUP)
    {
        //If we came from WiFi setup menu, we want a new registration token
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)TokenShouldBeGenerated, (KEYB_PARAM*)NULL, GENERATE_TOKEN_TIMEOUT);
    }
    else
    {
        //Otherwise we came from the WiFi join menu, wait for the registration status
        //Subscribe for a registration change
        hRegistrationChangeNotification = DBTHRD_SubscribeToNotification(DBTYPE_CLOUDCONNECTIVITY,
                                                                OnRegistrationChange,
                                                                DBCHANGE_REGISTER,
                                                                INDEX_DONT_CARE);

//        DBTHRD_GetData(DBTYPE_CLOUDCONNECTIVITY, (void *)&cloudInfo, INDEX_DONT_CARE);
//        if (cloudInfo.isRegistered == CLOUD_REGISTERED)
//        {
//            //We already received the registration status (registered)
//            OnRegistrationChange(DBERR_OK, INDEX_DONT_CARE, DBCHANGE_NOCHANGES);
//        }
//        else if (cloudInfo.isRegistered == CLOUD_UNREGISTERED)
//        {
//            //We already received the registration status (unregistered)
//            MNH_SetTimeoutCallback((MnhTimeoutCallback_t)TokenShouldBeGenerated, (KEYB_PARAM*)NULL, GENERATE_TOKEN_TIMEOUT);
//        }
    }
}

/*******************************************************************************
    MNU_Register_Core

    Display the registration token from Ayla
*******************************************************************************/
void MNU_Register_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_Register_Commit, (void*)KEY_BACK);
        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
    if ((RegistrationState == REG_REGISTERED) || (RegistrationState == REG_NOT_REGISTERED))
    {
        KBH_SetKeyPressProperties(KEY_DONE, (FUNC)MNU_Register_Commit, (void*)KEY_DONE);
    }
    else
    {
        KBH_SetKeyPressProperties(KEY_DONE, (FUNC)NULL, (void*)NULL);
    }
}

/*******************************************************************************
    MNU_Register_Exit
*******************************************************************************/
void MNU_Register_Exit(void)
{
    RegisterEditState = EDIT_IDLE;
    if (hRegistrationChangeNotification != DBTHRD_NOTIFHANDLE_NOTSET)
    {
        DBTHRD_UnsubscribeToNotification(hRegistrationChangeNotification);
    }
}

/*******************************************************************************
    MNU_Register_Commit
*******************************************************************************/
void MNU_Register_Commit(KEYB_PARAM keyId)
{
    static KEYB_PARAM commitKey = NULL;

    if ( RegisterEditState == EDIT_IN_PROGRESS )
    {
        commitKey = keyId;
        KBH_ClearAllKeyProperties();
        switch (commitKey)
        {
            case KEY_BACK:
            case KEY_DONE:
            default:
                MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Register_Commit, (KEYB_PARAM*)commitKey, IMMEDIATELY);
                break;
        }
        RegisterEditState = EDIT_COMMITTING;
    }
    else if ( RegisterEditState == EDIT_COMMITTING )
    {
        RegisterEditState = EDIT_IDLE;
        switch (commitKey)
        {
            case KEY_BACK:
            case KEY_DONE:
                MNH_SetActiveMenu(MENU_WIFI_SETUP);
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
    }
}

/*******************************************************************************
    TokenShouldBeGenerated
*******************************************************************************/
static void TokenShouldBeGenerated(void)
{
    WIFITHRD_GetRegistrationToken(RegistrationToken_Callback);
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MenuTimeout, (KEYB_PARAM*)NULL, WIFI_REGISTER_TIMEOUT);
}

/*******************************************************************************
    RegistrationToken_Callback
    Callback by the WiFi module when Registration Token is received
*******************************************************************************/
void RegistrationToken_Callback(RegisterStatus_t status, uint8_t* pToken)
{
    switch (status)
    {
        case REG_TOKEN_RECEIVED:
            //Make sure the string is NULL terminated
            pToken[sizeof(RegistrationToken) - 1] = NULL;
            strcpy((char*)RegistrationToken, (char const *)pToken);
            if (*RegistrationToken != NULL)
            {
                RegistrationState = REG_TOKEN_RECEIVED;
                if (MNH_GetActiveMenu() == MENU_REGISTER)
                {
                    KBH_ClearAllKeyProperties();
                    OS_StartTask(TSK_APP_MNH_MENU);

                    //
                    //  Show Back arrow
                    //
                    DPH_DisplayBack();

                    //
                    //  Show registration token
                    //
                    DPH_DisplayRegister(RegistrationState, &RegistrationToken[0], FALSE);

                    if (hRegistrationChangeNotification != DBTHRD_NOTIFHANDLE_NOTSET)
                    {
                        //Subscribe for a registration change, if not done already
                        hRegistrationChangeNotification = DBTHRD_SubscribeToNotification(DBTYPE_CLOUDCONNECTIVITY,
                                                                OnRegistrationChange,
                                                                DBCHANGE_REGISTER,
                                                                INDEX_DONT_CARE);
                    }
                }
            }
            break;

        default:
            RegistrationState = REG_UNKNOWN;
            break;
    }
}

