/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       MenuTable.c

    \brief      Source code file for the MenuTable module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the MenuTable core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/

#include "typedef.h"
#include "MenuHandler.h"
#include "MenuTable.h"

//Include separate context handlers
#include "SplashScreenMenu.h"
#include "HomeMenu.h"
#include "SetpointMenu.h"
#include "AdvSettingsMenu.h"
#include "ModeSelectionMenu.h"
#include "OptionsMenu.h"
#include "InfoMenu.h"
#include "LockMenu.h"
#include "BacklightMenu.h"
#include "LanguageMenu.h"
#include "UnitFormatMenu.h"
#include "TimeFormatMenu.h"
#include "ResetMenu.h"
#include "AlertsMenu.h"
#include "AlertsDetailsMenu.h"
#include "LCDTestMenu.h"
#include "TestModeMenu.h"
#include "IdleMenu.h"
#include "ControlModeMenu.h"
#include "FloorLimitMenu.h"
#include "FloorTypeMenu.h"
#include "DateTimeMenu.h"
#include "ProgramSetpointMenu.h"
#include "ProgramScheduleMenu.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

//! Top level contexts structure
FLASH_MEM const MENUS Menus[] = {
    //Entry                                 //Core                              //Exit
    NULL,                                   NULL,                               NULL,                               //!< MENU_INIT
    MNU_SplashScreen_Entry,                 MNU_SplashScreen_Core,              MNU_SplashScreen_Exit,              //!< MENU_SPLASH_SCREEN
    MNU_Home_Entry,                         MNU_Home_Core,                      MNU_Home_Exit,                      //!< MENU_HOME
    MNU_AdvSettings_Entry,                  MNU_AdvSettings_Core,               MNU_AdvSettings_Exit,               //!< MENU_ADV_SETTINGS
    MNU_DateTime_Entry,                     MNU_DateTime_Core,                  MNU_DateTime_Exit,                  //!< MENU_DATE_TIME
    MNU_ProgramSetpoint_Entry,              MNU_ProgramSetpoint_Core,           MNU_ProgramSetpoint_Exit,           //!< MENU_PROGRAM_SETPOINT
    MNU_ProgramSchedule_Entry,              MNU_ProgramSchedule_Core,           MNU_ProgramSchedule_Exit,           //!< MENU_PROGRAM_SCHEDULE
    MNU_ModeSelection_Entry,                MNU_ModeSelection_Core,             MNU_ModeSelection_Exit,             //!< MENU_MODE_SELECTION
    MNU_Options_Entry,                      MNU_Options_Core,                   MNU_Options_Exit,                   //!< MENU_OPTIONS
    MNU_Info_Entry,                         MNU_Info_Core,                      MNU_Info_Exit,                      //!< MENU_INFO
    MNU_ControlMode_Entry,                  MNU_ControlMode_Core,               MNU_ControlMode_Exit,               //!< MENU_CONTROL_MODE
    MNU_FloorType_Entry,                    MNU_FloorType_Core,                 MNU_FloorType_Exit,                 //!< MENU_FLOOR_TYPE
    MNU_FloorLimit_Entry,                   MNU_FloorLimit_Core,                MNU_FloorLimit_Exit,                //!< MENU_FLOOR_LIMIT
    MNU_Lock_Entry,                         MNU_Lock_Core,                      MNU_Lock_Exit,                      //!< MENU_LOCK
    MNU_Backlight_Entry,                    MNU_Backlight_Core,                 MNU_Backlight_Exit,                 //!< MENU_BACKLIGHT
    MNU_Language_Entry,                     MNU_Language_Core,                  MNU_Language_Exit,                  //!< MENU_LANGUAGE
    MNU_UnitFormat_Entry,                   MNU_UnitFormat_Core,                MNU_UnitFormat_Exit,                //!< MENU_UNIT_FORMAT
    MNU_TimeFormat_Entry,                   MNU_TimeFormat_Core,                MNU_TimeFormat_Exit,                //!< MENU_TIME_FORMAT
    MNU_Reset_Entry,                        MNU_Reset_Core,                     MNU_Reset_Exit,                     //!< MENU_RESET
    MNU_Alerts_Entry,                       MNU_Alerts_Core,                    MNU_Alerts_Exit,                    //!< MENU_ALERTS
    MNU_AlertsDetails_Entry,                MNU_AlertsDetails_Core,             MNU_AlertsDetails_Exit,             //!< MENU_ALERT_DETAILS
    MNU_LCDTest_Entry,                      MNU_LCDTest_Core,                   MNU_LCDTest_Exit,                   //!< MENU_LCDTEST
    MNU_TestMode_Entry,                     MNU_TestMode_Core,                  MNU_TestMode_Exit,                  //!< MENU_TESTMODE
    MNU_Black_Entry,                        MNU_Black_Core,                     MNU_Black_Exit,                     //!< MENU_BLACK
    MNU_White_Entry,                        MNU_White_Core,                     MNU_White_Exit,                     //!< MENU_WHITE
    MNU_Idle_Entry,                         MNU_Idle_Core,                      MNU_Idle_Exit,                      //!< MENU_IDLE
    NULL,                                   NULL,                               NULL,                               //!< LAST_MNU
};

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/

U8BIT KeyAlreadyAssigned = FALSE;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private functions definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    GetMenuCount

    Returns the number of menus in the list

    \return Number of menus
*******************************************************************************/
int8u GetMenuCount(void)
{
    return (sizeof(Menus) / sizeof(MENUS));
}

/*******************************************************************************
    MNU_SetKeyAssigned

    Set the assigned status of the keys

    \input Status (TRUE/FALSE)
*******************************************************************************/
void MNU_SetKeyAssigned(U8BIT status)
{
    KeyAlreadyAssigned = status;
}

/*******************************************************************************
    IsKeyAssigned

    Retrieve the assigned status of keys

    \return Status (TRUE/FALSE)
*******************************************************************************/
U8BIT IsKeyAssigned(void)
{
    return KeyAlreadyAssigned;
}
