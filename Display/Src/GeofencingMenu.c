/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       GeofencingMenu.c

    \brief      Source code file for the GeofencingMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the GeofencingMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "GeofencingMenu.h"
#include ".\DB\inc\THREAD_DB.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   GeofencingEditState = EDIT_IDLE;
static Geofencing_t	    GeofencingEditValue;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void ChangeGeofencing(KEYB_PARAM keyId);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    ChangeGeofencing

    Change the Geofencing on/off
*******************************************************************************/
static void ChangeGeofencing(KEYB_PARAM keyId)
{
    switch (keyId)
    {
        case KEY_OPTIONS_2:
            GeofencingEditValue = GEOFENCING_DISABLED;
            break;

        case KEY_OPTIONS_3:
            GeofencingEditValue = GEOFENCING_ENABLED;
            break;

        default:
            break;
    }

    DPH_DisplayGeofencing(GeofencingEditValue, FALSE);
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Geofencing_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_Geofencing_Entry
*******************************************************************************/
void MNU_Geofencing_Entry(void)
{
    DBTHRD_GetData(DBTYPE_GEOFENCING_ENABLE,(void*)&GeofencingEditValue, INDEX_DONT_CARE);
    GeofencingEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show Geofencing options
    //
    DPH_DisplayGeofencing(GeofencingEditValue, TRUE);

    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Geofencing_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_Geofencing_Core

    Allow changing the geofencing mode and clear geofenced devices
*******************************************************************************/
void MNU_Geofencing_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_Geofencing_Commit, (void*)KEY_BACK);
        KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)ChangeGeofencing, (void*)KEY_OPTIONS_2);
        KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)ChangeGeofencing, (void*)KEY_OPTIONS_3);
        KBH_SetKeyPressProperties(KEY_OPTIONS_5, (FUNC)MNU_Geofencing_Commit, (void*)KEY_OPTIONS_5);
        KBH_SetKeyPressProperties(KEY_DONE, (FUNC)MNU_Geofencing_Commit, (void*)KEY_DONE);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_Geofencing_Exit
*******************************************************************************/
void MNU_Geofencing_Exit(void)
{
    GeofencingEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_Geofencing_Commit
*******************************************************************************/
void MNU_Geofencing_Commit(KEYB_PARAM keyId)
{
    static KEYB_PARAM commitKey = NULL;

    if ( GeofencingEditState == EDIT_IN_PROGRESS )
    {
        commitKey = keyId;
        KBH_ClearAllKeyProperties();
        switch (commitKey)
        {
            case KEY_DONE:
                DBTHRD_SetData(DBTYPE_GEOFENCING_ENABLE,(void*)&GeofencingEditValue, INDEX_DONT_CARE, DBCHANGE_LOCAL);
                //No break on purpose

            case KEY_BACK:
            default:
                MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Geofencing_Commit, (KEYB_PARAM*)commitKey, IMMEDIATELY);
                break;
        }
        GeofencingEditState = EDIT_COMMITTING;
    }
    else if ( GeofencingEditState == EDIT_COMMITTING )
    {
        GeofencingEditState = EDIT_IDLE;
        switch (commitKey)
        {
            case KEY_BACK:
            case KEY_DONE:
                MNH_SetActiveMenu(MENU_ADV_SETTINGS);
                break;

            case KEY_OPTIONS_5:
                MNH_SetActiveMenu(MENU_CLEARGEOFENCING);
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
    }
}
