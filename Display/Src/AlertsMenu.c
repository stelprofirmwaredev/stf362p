/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       AlertsMenu.c

    \brief      Source code file for the AlertsMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the AlertsMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "AlertsMenu.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\HAL\inc\Display.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/


/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;


/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   AlertsEditState = EDIT_IDLE;
static U8BIT            Page = 0;
static U8BIT            AlertNumber = 0;
static U8BIT            SelectedAlertNumber;
static U8BIT            previousMenu;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/
    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void NextPage(void);
static void PreviousPage(void);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    NextPage

    Switch to the next page of available networks
*******************************************************************************/
static void NextPage(void)
{
    Page++;
    HAL_ClearScreen();
    KBH_ClearAllKeyProperties();

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show alerts
    //
    DPH_DisplayAlerts(Page);
}

/*******************************************************************************
    PreviousPage

    Switch to the previous page of available networks
*******************************************************************************/
static void PreviousPage(void)
{
    Page--;
    HAL_ClearScreen();
    KBH_ClearAllKeyProperties();

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show alerts
    //
    DPH_DisplayAlerts(Page);
}
/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_Alerts_Entry
*******************************************************************************/
void MNU_Alerts_Entry(void)
{
    AlertsEditState = EDIT_IN_PROGRESS;
    Page = 0;
    AlertNumber = MNU_Alerts_GetActiveAlertNumber();

    previousMenu = MNH_GetPreviousMenu();

    MNU_SetKeyAssigned(FALSE);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show alerts
    //
    DPH_DisplayAlerts(Page);

	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Alerts_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_Alerts_Core

    Allows to view the active alerts
*******************************************************************************/
void MNU_Alerts_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_Alerts_Commit, (void*)KEY_BACK);
        if ((AlertNumber - (Page * 7)) > 0)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_1, (FUNC)MNU_Alerts_Commit, (void*) KEY_OPTIONS_1);
        }
        if ((AlertNumber - (Page * 7)) > 1)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)MNU_Alerts_Commit, (void*) KEY_OPTIONS_2);
        }
        if ((AlertNumber - (Page * 7)) > 2)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)MNU_Alerts_Commit, (void*) KEY_OPTIONS_3);
        }
        if ((AlertNumber - (Page * 7)) > 3)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_4, (FUNC)MNU_Alerts_Commit, (void*) KEY_OPTIONS_4);
        }
        if ((AlertNumber - (Page * 7)) > 4)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_5, (FUNC)MNU_Alerts_Commit, (void*) KEY_OPTIONS_5);
        }
        if ((AlertNumber - (Page * 7)) > 5)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_6, (FUNC)MNU_Alerts_Commit, (void*) KEY_OPTIONS_6);
        }
        if ((AlertNumber - (Page * 7)) > 6)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_7, (FUNC)MNU_Alerts_Commit, (void*) KEY_OPTIONS_7);
        }
        if (AlertNumber > (7 * (Page + 1)))
        {
            KBH_SetKeyPressProperties(KEY_NEXT, (FUNC)NextPage, (void*) KEY_NEXT);
        }
        if (Page > 0)
        {
            KBH_SetKeyPressProperties(KEY_PREVIOUS, (FUNC)PreviousPage, (void*) KEY_PREVIOUS);
        }

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_Alerts_Exit
*******************************************************************************/
void MNU_Alerts_Exit(void)
{
    AlertsEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_Alerts_Commit
*******************************************************************************/
void MNU_Alerts_Commit(KEYB_PARAM keyId)
{
	if (AlertsEditState == EDIT_IN_PROGRESS)
	{
        KBH_ClearAllKeyProperties();
        switch(keyId)
        {
            case KEY_BACK:
                Page = 0;
                if (previousMenu == MENU_HOME)
                {
                    MNH_SetActiveMenu(MENU_HOME);
                }
                else
                {
                    MNH_SetActiveMenu(MENU_ADV_SETTINGS);
                }
                break;

            case KEY_OPTIONS_1:
            case KEY_OPTIONS_2:
            case KEY_OPTIONS_3:
            case KEY_OPTIONS_4:
            case KEY_OPTIONS_5:
            case KEY_OPTIONS_6:
            case KEY_OPTIONS_7:
                SelectedAlertNumber = Page * 7 + (keyId - KEY_OPTIONS_1);
                MNH_SetActiveMenu(MENU_ALERT_DETAILS);
                break;

            default:
                Page = 0;
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
		AlertsEditState = EDIT_IDLE;
	}
}

/*******************************************************************************
    MNU_Alerts_GetActiveAlertNumber
*******************************************************************************/
U8BIT MNU_Alerts_GetActiveAlertNumber(void)
{
    uint8_t alertInstance;
    dbType_Alerts_t alert;

    AlertNumber = 0;

    DBTHRD_GetData(DBTYPE_ACTIVE_ALERTS, (void*)&alert, THIS_THERMOSTAT);
    for (alertInstance = 0; alertInstance < ALERT_INSTANCES; alertInstance++)
    {
        if (alert.ActiveAlerts[alertInstance] != 0)
        {
            AlertNumber++;
        }
    }

    return AlertNumber;
}

/*******************************************************************************
    MNU_Alerts_GetSelectedAlert
*******************************************************************************/
void MNU_Alerts_GetSelectedAlert(uint8_t* pTstat, uint8_t* pAlertId)
{
    uint8_t index = 0;
    uint8_t activeAlertNumber = 0;
    dbType_Alerts_t activeAlerts;
    dbType_AlertDisplay_t alertToDisplay[100];

    DBTHRD_GetData(DBTYPE_ACTIVE_ALERTS, &activeAlerts, THIS_THERMOSTAT);
    for (index = 0; index < ALERT_INSTANCES; index++)
    {
        if (activeAlerts.ActiveAlerts[index] != 0)
        {
            alertToDisplay[activeAlertNumber].tstat = THIS_THERMOSTAT;
            alertToDisplay[activeAlertNumber].alertId = activeAlerts.ActiveAlerts[index];
            activeAlertNumber++;
        }
    }

    *pTstat = alertToDisplay[SelectedAlertNumber].tstat;
    *pAlertId = alertToDisplay[SelectedAlertNumber].alertId;
}
