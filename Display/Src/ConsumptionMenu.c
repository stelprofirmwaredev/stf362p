/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       ConsumptionMenu.c

    \brief      Source code file for the ConsumptionMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the ConsumptionMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "ConsumptionMenu.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/


/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e       ConsumptionEditState = EDIT_IDLE;
static ConsumptionFormat_t  ConsumptionFormatEditValue;
static ConsumptionFormat_t  ConsumptionFormat;
static U16BIT               SystemConsumption;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/
    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void ChangeConsumptionFormat(KEYB_PARAM keyId);
/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    ChangeConsumptionFormat

    Change the consumption format to 1 day or 7 days
*******************************************************************************/
static void ChangeConsumptionFormat(KEYB_PARAM keyId)
{
    switch (keyId)
    {
        case KEY_OPTIONS_2:
            ConsumptionFormatEditValue = CONSUMPTION_1_DAY;
            break;

        case KEY_OPTIONS_3:
            ConsumptionFormatEditValue = CONSUMPTION_7_DAYS;
            break;

        default:
            break;
    }

    DPH_DisplayConsumption(SystemConsumption, ConsumptionFormatEditValue);
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Consumption_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_Consumption_Entry
*******************************************************************************/
void MNU_Consumption_Entry(void)
{
    ConsumptionEditState = EDIT_IN_PROGRESS;
    ConsumptionFormat = CONSUMPTION_1_DAY;
    SystemConsumption = 1234;

    MNU_SetKeyAssigned(FALSE);

	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Consumption_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_Consumption_Core

    Display the thermostat consumption
*******************************************************************************/
void MNU_Consumption_Core(void)
{
    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show the thermostat consumption
    //
    //Todo - implement consumption history
    if (ConsumptionFormatEditValue == CONSUMPTION_7_DAYS)
    {
        DPH_DisplayConsumption(SystemConsumption * 7, ConsumptionFormatEditValue);
    }
    else
    {
        DPH_DisplayConsumption(SystemConsumption, ConsumptionFormatEditValue);
    }

    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure key in the Consumption menu
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_Consumption_Commit, (void*)KEY_BACK);
        KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)ChangeConsumptionFormat, (void*)KEY_OPTIONS_2);
        KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)ChangeConsumptionFormat, (void*)KEY_OPTIONS_3);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_Consumption_Exit
*******************************************************************************/
void MNU_Consumption_Exit(void)
{
    ConsumptionEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_Consumption_Commit
*******************************************************************************/
void MNU_Consumption_Commit(KEYB_PARAM keyId)
{
    static KEYB_PARAM commitKey = NULL;

    if ( ConsumptionEditState == EDIT_IN_PROGRESS )
    {
        commitKey = keyId;
        KBH_ClearAllKeyProperties();
        switch (commitKey)
        {
            case KEY_DONE:
                MNU_Consumption_SetConsumptionFormat(ConsumptionFormatEditValue);
                //No break on purpose

            case KEY_BACK:
            default:
                MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Consumption_Commit, (KEYB_PARAM*)commitKey, IMMEDIATELY);
                break;
        }
        DPH_DisplayConsumption(SystemConsumption, ConsumptionFormatEditValue);
        ConsumptionEditState = EDIT_COMMITTING;
    }
    else if ( ConsumptionEditState == EDIT_COMMITTING )
    {
        ConsumptionEditState = EDIT_IDLE;
        switch (commitKey)
        {
            case KEY_BACK:
                MNH_SetActiveMenu(MENU_REPORTS);
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
    }
}

/*******************************************************************************
    MNU_Consumption_GetConsumptionFormat

    Return the current time format
*******************************************************************************/
ConsumptionFormat_t MNU_Consumption_GetConsumptionFormat(void)
{
    return ConsumptionFormat;
}

/*******************************************************************************
    MNU_Consumption_SetConsumptionFormat

    Set the time format to 12h (am/pm) or 24h
*******************************************************************************/
void MNU_Consumption_SetConsumptionFormat(ConsumptionFormat_t format)
{
     ConsumptionFormat = format;
//Todo - Notify consumption format
//	 MED_NotifyConsumptionFormat(ConsumptionFormat);
}
