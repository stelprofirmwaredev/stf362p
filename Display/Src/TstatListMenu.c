/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       TstatListMenu.c

    \brief      Source code file for the TstatListMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the TstatListMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "TstatListMenu.h"

#include ".\HAL\inc\Display.h"
/*******************************************************************************
    Private constants definitions
*******************************************************************************/


/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   TstatListEditState = EDIT_IDLE;
static U8BIT            Page = 0;
static U8BIT            TstatNumber = 0;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/
    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void NextPage(void);
static void PreviousPage(void);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    NextPage

    Switch to the next page of the connected thermostats
*******************************************************************************/
static void NextPage(void)
{
    Page++;
    HAL_ClearScreen();
    KBH_ClearAllKeyProperties();
}

/*******************************************************************************
    PreviousPage

    Switch to the previous page of the connected thermostats
*******************************************************************************/
static void PreviousPage(void)
{
    Page--;
    HAL_ClearScreen();
    KBH_ClearAllKeyProperties();
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_TstatList_Entry
*******************************************************************************/
void MNU_TstatList_Entry(void)
{
    TstatListEditState = EDIT_IN_PROGRESS;
    Page = 0;
    TstatNumber = 16;

    MNU_SetKeyAssigned(FALSE);

	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_TstatList_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_TstatList_Core

    Display the list of connected thermostats
*******************************************************************************/
void MNU_TstatList_Core(void)
{
    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show the thermostat list
    //
    DPH_DisplayThermostatList(TstatNumber, Page);

    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure key in the Thermostat list menu
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_TstatList_Commit, (void*)KEY_BACK);
        if (TstatNumber > 0)
        {

        }
        if (TstatNumber > 1)
        {

        }
        if (TstatNumber > 2)
        {

        }
        if (TstatNumber > 3)
        {

        }
        if (TstatNumber > 4)
        {

        }
        if (TstatNumber > 5)
        {

        }
        if (TstatNumber > 6)
        {

        }
        if (TstatNumber > 7)
        {

        }
        if (TstatNumber > (8 * (Page + 1)))
        {
            KBH_SetKeyPressProperties(KEY_NEXT, (FUNC)NextPage, (void*) KEY_NEXT);
        }
        if (Page > 0)
        {
            KBH_SetKeyPressProperties(KEY_PREVIOUS, (FUNC)PreviousPage, (void*) KEY_PREVIOUS);
        }

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_TstatList_Exit
*******************************************************************************/
void MNU_TstatList_Exit(void)
{
    TstatListEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_TstatList_Commit
*******************************************************************************/
void MNU_TstatList_Commit(KEYB_PARAM keyId)
{
	if (TstatListEditState == EDIT_IN_PROGRESS)
	{
        KBH_ClearAllKeyProperties();
        switch (keyId)
        {
            case KEY_BACK:
                MNH_SetActiveMenu(MENU_ADV_SETTINGS);
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
		TstatListEditState = EDIT_IDLE;
	}
}

/*******************************************************************************
    MNU_TstatList_GetThermostatNumber
*******************************************************************************/
U8BIT MNU_TstatList_GetThermostatNumber(void)
{
    return TstatNumber;
}
