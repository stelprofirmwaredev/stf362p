/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       TimeFormatMenu.c

    \brief      Source code file for the TimeFormatMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the TimeFormatMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "TimeFormatMenu.h"
#include ".\DB\inc\THREAD_DB.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e           TimeFormatEditState = EDIT_IDLE;
static TimeFormat_t             TimeFormatEditValue;
static uint8_t                  DSTEditValue;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void ChangeTimeFormat(KEYB_PARAM keyId);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    ChangeTimeFormat

    Change the time format to either 12h (am/pm) or 24h
*******************************************************************************/
static void ChangeTimeFormat(KEYB_PARAM keyId)
{
    uint8_t onboardingDone;

    switch (keyId)
    {
        case KEY_OPTIONS_2:
            TimeFormatEditValue = TIME_24H;
            break;

        case KEY_OPTIONS_3:
            TimeFormatEditValue = TIME_12H;
            break;

        case KEY_OPTIONS_4:
            DSTEditValue = 1;
            break;

        case KEY_OPTIONS_5:
            DSTEditValue = 0;
            break;

        default:
            break;
    }

    DPH_DisplayTimeFormat(TimeFormatEditValue, DSTEditValue, FALSE);

    DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);
    if (onboardingDone == 1)
    {
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_TimeFormat_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    }
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_TimeFormat_Entry
*******************************************************************************/
void MNU_TimeFormat_Entry(void)
{
    uint8_t onboardingDone;
    dbType_Time_t localTime;

    DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);
    DBTHRD_GetData(DBTYPE_TIME_FORMAT,(void*)&TimeFormatEditValue, INDEX_DONT_CARE);
    DBTHRD_GetData(DBTYPE_LOCALTIME,(void*)&localTime, INDEX_DONT_CARE);
    DSTEditValue = localTime.IsDstActive;
    TimeFormatEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show time format
    //
    DPH_DisplayTimeFormat(TimeFormatEditValue, DSTEditValue, TRUE);

    if (onboardingDone == 1)
    {
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_TimeFormat_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    }
}

/*******************************************************************************
    MNU_TimeFormat_Core

    Allows to change the time format
*******************************************************************************/
void MNU_TimeFormat_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_TimeFormat_Commit, (void*)KEY_BACK);
        KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)ChangeTimeFormat, (void*)KEY_OPTIONS_2);
        KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)ChangeTimeFormat, (void*)KEY_OPTIONS_3);
        KBH_SetKeyPressProperties(KEY_OPTIONS_4, (FUNC)ChangeTimeFormat, (void*)KEY_OPTIONS_4);
        KBH_SetKeyPressProperties(KEY_OPTIONS_5, (FUNC)ChangeTimeFormat, (void*)KEY_OPTIONS_5);
        KBH_SetKeyPressProperties(KEY_DONE, (FUNC)MNU_TimeFormat_Commit, (void*)KEY_DONE);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_TimeFormat_Exit
*******************************************************************************/
void MNU_TimeFormat_Exit(void)
{
    TimeFormatEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_TimeFormat_Init
*******************************************************************************/
void MNU_TimeFormat_Init(void)
{
    MNU_TimeFormat_SetTimeFormat(TIME_24H, 0);
}

/*******************************************************************************
    MNU_TimeFormat_Commit
*******************************************************************************/
void MNU_TimeFormat_Commit(KEYB_PARAM keyId)
{
    static KEYB_PARAM commitKey = NULL;
    uint8_t onboardingDone;

    if ( TimeFormatEditState == EDIT_IN_PROGRESS )
    {
        commitKey = keyId;
        switch (commitKey)
        {
            case KEY_DONE:
                MNU_TimeFormat_SetTimeFormat(TimeFormatEditValue, DSTEditValue);
                //No break on purpose

            case KEY_BACK:
            default:
                MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_TimeFormat_Commit, (KEYB_PARAM*)commitKey, IMMEDIATELY);
                break;
        }
        TimeFormatEditState = EDIT_COMMITTING;
		KBH_ClearAllKeyProperties();
    }
    else if ( TimeFormatEditState == EDIT_COMMITTING )
    {
        TimeFormatEditState = EDIT_IDLE;
        DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);

        switch (commitKey)
        {
            case KEY_BACK:
                if (onboardingDone == 1)
                {
                    MNH_SetActiveMenu(MENU_OPTIONS);
                }
                else
                {
                    MNH_SetActiveMenu(MENU_UNIT_FORMAT);
                }
                break;

            case KEY_DONE:
                if (onboardingDone == 1)
                {
                    MNH_SetActiveMenu(MENU_OPTIONS);
                }
                else
                {
                    MNH_SetActiveMenu(MENU_CONTROL_MODE);
                }
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
    }
}


/*******************************************************************************
    MNU_TimeFormat_SetTimeFormat

    Set the time format to 12h (am/pm) or 24h
    Set the dst management on or off
*******************************************************************************/
void MNU_TimeFormat_SetTimeFormat(TimeFormat_t format, uint8_t dst)
{
    dbType_Time_t localTime;

    DBTHRD_SetData(DBTYPE_TIME_FORMAT,(void*)&format, INDEX_DONT_CARE, DBCHANGE_LOCAL);
    localTime.IsDstActive = dst;
    DBTHRD_SetData(DBTYPE_DST_ACTIVE,(void*)&localTime, INDEX_DONT_CARE, DBCHANGE_LOCAL);
}
