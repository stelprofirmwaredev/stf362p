/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2019, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       FloorLimitMenu.c

    \brief      Source code file for the FloorLimitMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the FloorLimitMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "convert.h"
#include "tools.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "FloorLimitMenu.h"
#include ".\DB\inc\THREAD_DB.h"
#include "AlertsMenu.h"
#include "APP_SetpointManager.h"
#include "HomeMenu.h"
/*******************************************************************************
    Private constants definitions
*******************************************************************************/


/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_CONFIRM,
    EDIT_COMMITTING
} MnuEditState_e;


/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   FloorLimitEditState = EDIT_IDLE;
static TEMPERATURE_C_t FloorLimitEditValue;
static susbscriptionHandle_t hFloorLimitNotification = DBTHRD_NOTIFHANDLE_NOTSET;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/
    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
DB_NOTIFICATION(MNU_OnFloorLimitUpdate);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/

/*******************************************************************************
* @brief    Display the Floor Limit when updated
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2019/04/09
*******************************************************************************/
DB_NOTIFICATION(MNU_OnFloorLimitUpdate)
{
    FloorLimitEditValue = MNU_FloorLimit_GetFloorLimit();
    DPH_DisplayFloorLimit(FloorLimitEditValue, FALSE);
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_FloorLimit_Entry
*******************************************************************************/
void MNU_FloorLimit_Entry(void)
{
    FloorLimitEditState = EDIT_IDLE;

    MNU_SetKeyAssigned(FALSE);

    FloorLimitEditValue = MNU_FloorLimit_GetFloorLimit();

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show Floor Limit
    //
    DPH_DisplaySetpointApplied(FloorLimitEditState);
    DPH_DisplayFloorLimit(FloorLimitEditValue, TRUE);

    hFloorLimitNotification = DBTHRD_SubscribeToNotification(DBTYPE_FLOOR_LIMIT,
                                                       MNU_OnFloorLimitUpdate,
                                                       DBCHANGE_ALLCHANGES,
                                                       INDEX_DONT_CARE);

	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_FloorLimit_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_FloorLimit_Core

    Allow to view/change the Floor Limit
*******************************************************************************/
void MNU_FloorLimit_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_FloorLimit_Commit, (void*)KEY_BACK);
        KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_FloorLimit_DecFloorLimitStartTurbo, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
        KBH_SetKeyHoldProperties(KEY_UP, (FUNC)MNU_FloorLimit_IncFloorLimitStartTurbo, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
        KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_FloorLimit_IncFloorLimit, (int16u*)50);
        KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_FloorLimit_DecFloorLimit, (int16u*)50);

        // Stop key repeat monitoring
        KBH_SetKeyRepeatProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
        KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_Setpoint_Exit
*******************************************************************************/
void MNU_FloorLimit_Exit(void)
{
    MNU_FloorLimit_StopFloorLimitEdition();
    DBTHRD_UnsubscribeToNotification(hFloorLimitNotification);
}

/*******************************************************************************
    MNU_FloorLimit_Confirm
*******************************************************************************/
void MNU_FloorLimit_Confirm (void)
{
    FloorLimitEditState = EDIT_IDLE;
    DPH_DisplaySetpointApplied(FloorLimitEditState);
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_FloorLimit_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_FloorLimit_Commit
*******************************************************************************/
void MNU_FloorLimit_Commit(KEYB_PARAM keyId)
{
    KBH_ClearAllKeyProperties();
    switch(keyId)
    {
        case KEY_BACK:
            MNH_SetActiveMenu(MENU_CONTROL_MODE);
            break;

        default:
            MNH_SetActiveMenu(MENU_HOME);
            break;
    }
}

/*******************************************************************************
    MNU_FloorLimit_StopFloorLimitEdition
*******************************************************************************/
void MNU_FloorLimit_StopFloorLimitEdition(void)
{
    FloorLimitEditState = EDIT_CONFIRM;
    DPH_DisplaySetpointApplied(FloorLimitEditState);
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_FloorLimit_Confirm,  (KEYB_PARAM*)NULL, CONFIRM_TIMEOUT);
}

/*******************************************************************************
    MNU_FloorLimit_IncFloorLimit

    Increment the Floor Limit
*******************************************************************************/
void MNU_FloorLimit_IncFloorLimit(int16u value)
{
	UnitFormat_t format;
	TEMPERATURE_C_t maxsetpoint;
    FloorType_t floorType;

    DBTHRD_GetData(DBTYPE_FLOOR_TYPE,(void*)&floorType, THIS_THERMOSTAT);
    if (floorType == FLOORTYPE_TILE)
    {
        maxsetpoint = MAXIMUM_FLOOR_SETPOINT;
    }
    else
    {
        maxsetpoint = MAXIMUM_ENGINEERED_FLOOR_SETPOINT;
    }
    FloorLimitEditValue = MNU_FloorLimit_GetFloorLimit();

    FloorLimitEditState = EDIT_IN_PROGRESS;

    if ((FloorLimitEditValue < maxsetpoint) && (FloorLimitEditValue != DB_SENSOR_INVALID))
    {
        DBTHRD_GetData(DBTYPE_TEMPERATURE_FORMAT,(void*)&format, INDEX_DONT_CARE);
        if(format == DEGREE_F)
        {
            FloorLimitEditValue = CVT_ConvertCtoF(FloorLimitEditValue, 100);
            FloorLimitEditValue += (value * 2);
            FloorLimitEditValue = CVT_ConvertFtoC(FloorLimitEditValue, 0);
        }
        else
        {
            FloorLimitEditValue = TLS_Round(FloorLimitEditValue, 50);
            FloorLimitEditValue += value;
            FloorLimitEditValue = TLS_Round(FloorLimitEditValue, 50);
        }
        if (FloorLimitEditValue > maxsetpoint)
        {
            FloorLimitEditValue = maxsetpoint;
        }
        DPH_DisplaySetpointApplied(FloorLimitEditState);
        DPH_DisplayFloorLimit(FloorLimitEditValue, FALSE);
    }

    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_FloorLimit_CommitFloorLimit, (U8BIT*)NULL, SETPOINT_EDIT_TIMEOUT);
}

/*******************************************************************************
    MNU_FloorLimit_DecFloorLimit

    Decrement the Floor Limit
*******************************************************************************/
void MNU_FloorLimit_DecFloorLimit(int16u value)
{
	UnitFormat_t format;
	TEMPERATURE_C_t minsetpoint;

    minsetpoint = MINIMUM_FLOOR_LIMIT;
    FloorLimitEditValue = MNU_FloorLimit_GetFloorLimit();

    FloorLimitEditState = EDIT_IN_PROGRESS;

    if ((FloorLimitEditValue > minsetpoint) && (FloorLimitEditValue != DB_SENSOR_INVALID))
    {
        DBTHRD_GetData(DBTYPE_TEMPERATURE_FORMAT,(void*)&format, INDEX_DONT_CARE);
        if(format == DEGREE_F)
        {
            FloorLimitEditValue = CVT_ConvertCtoF(FloorLimitEditValue, 100);
            FloorLimitEditValue -= (value * 2);
            FloorLimitEditValue = CVT_ConvertFtoC(FloorLimitEditValue, 0);
        }
        else
        {
            FloorLimitEditValue = TLS_Round(FloorLimitEditValue, 50);
            FloorLimitEditValue -= value;
            FloorLimitEditValue = TLS_Round(FloorLimitEditValue, 50);
        }
        if (FloorLimitEditValue < minsetpoint)
        {
            FloorLimitEditValue = minsetpoint;
        }
        DPH_DisplaySetpointApplied(FloorLimitEditState);
        DPH_DisplayFloorLimit(FloorLimitEditValue, FALSE);
    }

    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_FloorLimit_CommitFloorLimit, (U8BIT*)NULL, SETPOINT_EDIT_TIMEOUT);
}

/*******************************************************************************
    MNU_FloorLimit_DecFloorLimitStartTurbo

    Decrement the Floor Limit
*******************************************************************************/
void MNU_FloorLimit_DecFloorLimitStartTurbo(void)
{
    KBH_SetKeyHoldProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

    // Decrement the Floor Limit
    MNU_FloorLimit_DecFloorLimit(50);
    // Turn on key repeat notification
    KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)MNU_FloorLimit_DecFloorLimit, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_DELAY));

    KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_FloorLimit_FloorLimitStopTurbo, (void*)NULL);
    KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_FloorLimit_FloorLimitStopTurbo, (void*)NULL);
}

/*******************************************************************************
    MNU_FloorLimit_IncFloorLimitStartTurbo

    Increment the Floor Limit
*******************************************************************************/
void MNU_FloorLimit_IncFloorLimitStartTurbo(void)
{
    KBH_SetKeyHoldProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

    // Increment the Floor Limit
    MNU_FloorLimit_IncFloorLimit(50);
    // Turn on key repeat notification
    KBH_SetKeyRepeatProperties(KEY_UP, (FUNC)MNU_FloorLimit_IncFloorLimit, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_DELAY));

    KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_FloorLimit_FloorLimitStopTurbo, (void*)NULL);
    KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_FloorLimit_FloorLimitStopTurbo, (void*)NULL);
}

/*******************************************************************************
    MNU_FloorLimit_FloorLimitStopTurbo
*******************************************************************************/
void MNU_FloorLimit_FloorLimitStopTurbo(void)
{
    // Stop key repeat monitoring
	KBH_SetKeyRepeatProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

    // Start key hold monitoring
	KBH_SetKeyHoldProperties(KEY_UP, (FUNC)MNU_FloorLimit_IncFloorLimitStartTurbo, (void*) NULL, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_FloorLimit_DecFloorLimitStartTurbo, (void*) NULL, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
	KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_FloorLimit_IncFloorLimit, (int16u*) 50);
    KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_FloorLimit_DecFloorLimit, (int16u*) 50);
}

/*******************************************************************************
    MNU_FloorLimit_GetFloorLimit

    Get the current Floor Limit
*******************************************************************************/
TEMPERATURE_C_t MNU_FloorLimit_GetFloorLimit(void)
{
    TEMPERATURE_C_t setpoint;

    // If not currently in Floor Limit edition mode
    if ( FloorLimitEditState == EDIT_IDLE )
    {
        // Return floor limit
        DBTHRD_GetData(DBTYPE_FLOOR_LIMIT,(void*)&setpoint, THIS_THERMOSTAT);
    }
    // Else currently in setpoint edition mode
    else
    {
        // Return current edited floor limit value
        setpoint = FloorLimitEditValue;
    }
    return ( setpoint );
}

/*******************************************************************************
    MNU_FloorLimit_CommitFloorLimit

    Update the Setpoint Manager with a new Floor Limit
*******************************************************************************/
void MNU_FloorLimit_CommitFloorLimit( void )
{
    TEMPERATURE_C_t     currentFloorLimit;

    DBTHRD_GetData(DBTYPE_FLOOR_LIMIT,(void*)&currentFloorLimit, THIS_THERMOSTAT);

    if ( FloorLimitEditState == EDIT_IN_PROGRESS )
    {
        // If user changed setpoint
        if (( FloorLimitEditValue != currentFloorLimit ) && (FloorLimitEditValue != DB_SENSOR_INVALID))
        {
            DBTHRD_SetData(DBTYPE_FLOOR_LIMIT,(void*)&FloorLimitEditValue, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_FloorLimit_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
        }
    }
    MNU_FloorLimit_StopFloorLimitEdition();
}
