/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
//  \file       OsTaskSetup.c
//
//  \brief      Os task scheduler task declaration implementation file.
//
//  \author     Jean-Fran�ois Many
//
********************************************************************************
    \section Info   Module Information

    This file contains the os scheduler task table to be used
    for this application.

    Table of modifications:

        - Version:      0.0.1
        - Date:         2015-01-23
        - Description:  First version
        .

*******************************************************************************/

/*-------------------------------------------------------------------------------
    INCLUDE
-------------------------------------------------------------------------------*/
#include "typedef.h"

//#include "HAL_Touch.h"
#include "APP_Display.h"
#include "MenuHandler.h"
#include "HalKeyset.h"
#include "APP_SetpointManager.h"
#include ".\HAL\inc\app_test_bench.h"

#include "OsTask.h"
/*-------------------------------------------------------------------------------
    DEFINITIONS
-------------------------------------------------------------------------------*/
// none


/*-------------------------------------------------------------------------------
    DECLARATIONS
-------------------------------------------------------------------------------*/

//
//! OS global task watchdog timeout value
// The OS will force a reset if ever a task with wdog flag enabled has not
// ran at least once over a period of 'n' ticks.
// Currently set at 60 seconds
const OsTaskDelay_t OS_TASK_WDOG_PERIOD = OS_MSTOTICK(60000);


//
//! OS task table
// BEWARE: *** Task must be declared following ID ordering ***
//
FLASH_MEM const OsTaskDescriptor_t OS_TASK_TABLE[MAX_OS_TASK_ID] =
{
    // taskId                       taskEntry                       taskAutoReloadDelay(in tick units)  taskWdogEnable
    { TSK_OS_SCHEDULER,             OS_TaskScheduler,               0,                                  FALSE   },  // MANDATORY SYSTEM TASK AND POSITION - DO NOT CHANGE OR MOVE
    { TSK_HAL_TOUCH,                HAL_TouchScreenTask,            HAL_KEYSET_TICK,                    FALSE   },
    { TSK_APP_MNH_MENU,             MNH_tCallMenu,                  0,                                  FALSE   },  // UI menu handler task
    { TSK_APP_MNH_EVENTS,           MNH_tUiTimedEvents,             0,                                  FALSE   },
    { TSK_APP_RTC,                  NULL/*RTCIF_Run*/,                      OS_MSTOTICK(1000),                  FALSE   },  // Time tracking task
    { TSK_LIMIT_CHECK,              STPIF_LimitCheck,               0,                                  FALSE   },
    { TSK_HAL_DEBUG_ACTION,         UART_DEBUG_Action,              0,                                  FALSE },  // Debug task action
#ifdef DEVKIT_PLATFORM
    { TSK_SIM_ALERT,                Simulate_Alert,                 HAL_KEYSET_TICK,                    FALSE },
#endif /* DEVKIT_PLATFORM */
    { TSK_OS_IDLE,                  OS_TaskIdle,                    0,                                  FALSE },  // MANDATORY SYSTEM TASK AND POSITION - DO NOT CHANGE OR MOVE
};
