/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2016, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       GroupDetailsMenu.c

    \brief      Source code file for the GroupDetailsMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the GroupDetailsMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "convert.h"
#include "tools.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "GroupDetailsMenu.h"
#include "GroupsMenu.h"
#include "APP_SetpointManager.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\HAL\inc\Display.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/


/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_CONFIRM,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   GroupDetailsEditState = EDIT_IDLE;
static uint8_t          TstatNumber;
static uint8_t          Page;
static TEMPERATURE_C_t  GroupDetailsEditValue;
static uint8_t          SelectedTstatNumber;
static susbscriptionHandle_t hGroupSetpointNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static uint8_t          hasMember;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/
extern uint8_t isGroupFilledOnlyWithTstatInFloorMode(uint8_t groupIndex);

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void NextPage(void);
static void PreviousPage(void);
static uint8_t FindSelectedTstat(uint8_t positionInGroup, uint8_t group);
DB_NOTIFICATION(MNU_OnGroupSetpointUpdate);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    NextPage

    Switch to the next page of the connected thermostats
*******************************************************************************/
static void NextPage(void)
{
    Page++;
    HAL_ClearScreen();
    KBH_ClearAllKeyProperties();

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show Group Details
    //
    DPH_DisplaySetpointApplied(GroupDetailsEditState);
    hasMember = DPH_DisplayGroupDetails(MNU_Groups_GetSelectedGroup(), Page);
    if (hasMember != 0)
    {
        if (FALSE == isGroupFilledOnlyWithTstatInFloorMode(MNU_Groups_GetSelectedGroup()))
        {
            DPH_DisplaySetpoint(GroupDetailsEditValue, TRUE, FALSE, SET_GROUP, FALSE);
        }
        else
        {
            DPH_DisplaySetpoint(GroupDetailsEditValue, TRUE, FALSE, SET_GROUP_FLOOR, FALSE);
        }
    }
}

/*******************************************************************************
    PreviousPage

    Switch to the previous page of the connected thermostats
*******************************************************************************/
static void PreviousPage(void)
{
    Page--;
    HAL_ClearScreen();
    KBH_ClearAllKeyProperties();

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show Group Details
    //
    DPH_DisplaySetpointApplied(GroupDetailsEditState);
    hasMember = DPH_DisplayGroupDetails(MNU_Groups_GetSelectedGroup(), Page);
    if (hasMember != 0)
    {
        if (FALSE == isGroupFilledOnlyWithTstatInFloorMode(MNU_Groups_GetSelectedGroup()))
        {
            DPH_DisplaySetpoint(GroupDetailsEditValue, TRUE, FALSE, SET_GROUP, FALSE);
        }
        else
        {
            DPH_DisplaySetpoint(GroupDetailsEditValue, TRUE, FALSE, SET_GROUP_FLOOR, FALSE);
        }
    }
}

/*******************************************************************************
    FindSelectedTstat

    Find the thermostat position from the selected group position
*******************************************************************************/
static uint8_t FindSelectedTstat(uint8_t positionInGroup, uint8_t group)
{
    dbType_GroupMembers_t groupMembers;
    uint8_t tstat;
    uint8_t groupMemberList[THERMOSTAT_INSTANCES];
    uint8_t memberPosition;

    memberPosition = 0;
    DBTHRD_GetData(DBTYPE_GROUP_MEMBERS, &groupMembers, group);
    for (tstat = 0; tstat < THERMOSTAT_INSTANCES; tstat++)
    {
        //Check group membership
        if (groupMembers.MembersList & (1<<tstat))
        {
            groupMemberList[memberPosition] = tstat;
            memberPosition++;
        }
    }
    return groupMemberList[positionInGroup];
}

/*******************************************************************************
* @brief    Display the Group Setpoint when updated
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/13
*******************************************************************************/
DB_NOTIFICATION(MNU_OnGroupSetpointUpdate)
{
    GroupDetailsEditValue = MNU_GroupDetails_GetSetpoint();
    if (FALSE == isGroupFilledOnlyWithTstatInFloorMode(MNU_Groups_GetSelectedGroup()))
    {
        DPH_DisplaySetpoint(GroupDetailsEditValue, FALSE, FALSE, SET_GROUP, FALSE);
    }
    else
    {
        DPH_DisplaySetpoint(GroupDetailsEditValue, FALSE, FALSE, SET_GROUP_FLOOR, FALSE);
    }
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_GroupDetails_Entry
*******************************************************************************/
#pragma optimize = none //Optimization removed, otherwise the notification does not get set properly
void MNU_GroupDetails_Entry(void)
{
    uint8_t previousMenu;

    GroupDetailsEditState = EDIT_IDLE;

    MNU_SetKeyAssigned(FALSE);
    TstatNumber = 0;
    SelectedTstatNumber = 0;
    previousMenu = MNH_GetPreviousMenu();
    if (previousMenu == MENU_GROUPS)
    {
        Page = 0;
    }

    GroupDetailsEditValue = MNU_GroupDetails_GetSetpoint();

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show Group Details
    //
    DPH_DisplaySetpointApplied(GroupDetailsEditState);
    hasMember = DPH_DisplayGroupDetails(MNU_Groups_GetSelectedGroup(), Page);
    if (hasMember != 0)
    {
        if (FALSE == isGroupFilledOnlyWithTstatInFloorMode(MNU_Groups_GetSelectedGroup()))
        {
            DPH_DisplaySetpoint(GroupDetailsEditValue, TRUE, FALSE, SET_GROUP, FALSE);
        }
        else
        {
            DPH_DisplaySetpoint(GroupDetailsEditValue, TRUE, FALSE, SET_GROUP_FLOOR, FALSE);
        }
    }

    hGroupSetpointNotification = DBTHRD_SubscribeToNotification(DBTYPE_GROUP_SETPOINT,
                                                       MNU_OnGroupSetpointUpdate,
                                                       DBCHANGE_ALLCHANGES,
                                                       INDEX_DONT_CARE);

	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_GroupDetails_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_GroupDetails_Core

    Show a specific group details
*******************************************************************************/
void MNU_GroupDetails_Core(void)
{
    uint8_t tstat;
    dbType_GroupMembers_t groupMembers;

    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure key in the GroupDetails menu
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_GroupDetails_Commit, (void*)KEY_BACK);
        TstatNumber = 0;
        DBTHRD_GetData(DBTYPE_GROUP_MEMBERS, &groupMembers, MNU_Groups_GetSelectedGroup());
        for (tstat = 0; tstat < THERMOSTAT_INSTANCES; tstat++)
        {
            //Check group membership
            if (groupMembers.MembersList & (1<<tstat))
            {
                TstatNumber++;
            }
        }
        if ((TstatNumber - (Page * 3)) > 0)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_1, (FUNC)MNU_GroupDetails_Commit, (void*) KEY_OPTIONS_1);
        }
        if ((TstatNumber - (Page * 3)) > 1)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)MNU_GroupDetails_Commit, (void*) KEY_OPTIONS_2);
        }
        if ((TstatNumber - (Page * 3)) > 2)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)MNU_GroupDetails_Commit, (void*) KEY_OPTIONS_3);
        }
        if (TstatNumber > (3 * (Page + 1)))
        {
            KBH_SetKeyPressProperties(KEY_NEXT, (FUNC)NextPage, (void*) KEY_NEXT);
        }
        if (Page > 0)
        {
            KBH_SetKeyPressProperties(KEY_PREVIOUS, (FUNC)PreviousPage, (void*) KEY_PREVIOUS);
        }

        if (hasMember != 0)
        {
            KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_GroupDetails_DecSetpointStartTurbo, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
            KBH_SetKeyHoldProperties(KEY_UP, (FUNC)MNU_GroupDetails_IncSetpointStartTurbo, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
            KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_GroupDetails_IncSetpoint, (int16u*)50);
            KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_GroupDetails_DecSetpoint, (int16u*)50);
        }

        // Stop key repeat monitoring
        KBH_SetKeyRepeatProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
        KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_GroupDetails_Exit
*******************************************************************************/
void MNU_GroupDetails_Exit(void)
{
    GroupDetailsEditState = EDIT_IDLE;
    DBTHRD_UnsubscribeToNotification(hGroupSetpointNotification);
}

/*******************************************************************************
    MNU_GroupDetails_Confirm
*******************************************************************************/
void MNU_GroupDetails_Confirm (void)
{
    GroupDetailsEditState = EDIT_IDLE;
    DPH_DisplaySetpointApplied(GroupDetailsEditState);
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_GroupDetails_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_GroupDetails_Commit
*******************************************************************************/
void MNU_GroupDetails_Commit(KEYB_PARAM keyId)
{
    KBH_ClearAllKeyProperties();
    switch(keyId)
    {
        case KEY_BACK:
            Page = 0;
            MNH_SetActiveMenu(MENU_GROUPS);
            break;

        case KEY_OPTIONS_1:
        case KEY_OPTIONS_2:
        case KEY_OPTIONS_3:
            SelectedTstatNumber = FindSelectedTstat(Page * 3 + (keyId - KEY_OPTIONS_1), MNU_Groups_GetSelectedGroup());
            MNH_SetActiveMenu(MENU_TSTAT_DETAILS);
            break;

        default:
            Page = 0;
            MNH_SetActiveMenu(MENU_HOME);
            break;
    }
}

/*******************************************************************************
    MNU_GroupDetails_StopSetpointEdition
*******************************************************************************/
void MNU_GroupDetails_StopSetpointEdition(void)
{
    GroupDetailsEditState = EDIT_CONFIRM;
    DPH_DisplaySetpointApplied(GroupDetailsEditState);
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_GroupDetails_Confirm,  (KEYB_PARAM*)NULL, CONFIRM_TIMEOUT);
}

/*******************************************************************************
    MNU_GroupDetails_IncSetpoint

    Increment a group setpoint
*******************************************************************************/
void MNU_GroupDetails_IncSetpoint(int16u value)
{
	UnitFormat_t format;
	TEMPERATURE_C_t maxsetpoint;

    if (FALSE == isGroupFilledOnlyWithTstatInFloorMode(MNU_Groups_GetSelectedGroup()))
    {
        maxsetpoint = MAXIMUM_SETPOINT;
    }
    else
    {
        maxsetpoint = MAXIMUM_FLOOR_SETPOINT;
    }
    GroupDetailsEditValue = MNU_GroupDetails_GetSetpoint();

    GroupDetailsEditState = EDIT_IN_PROGRESS;

    if ((GroupDetailsEditValue < maxsetpoint) && (GroupDetailsEditValue != DB_SENSOR_INVALID))
    {
        DBTHRD_GetData(DBTYPE_TEMPERATURE_FORMAT,(void*)&format, INDEX_DONT_CARE);
        if(format == DEGREE_F)
        {
            GroupDetailsEditValue = CVT_ConvertCtoF(GroupDetailsEditValue, 100);
            GroupDetailsEditValue += (value * 2);
            GroupDetailsEditValue = CVT_ConvertFtoC(GroupDetailsEditValue, 0);
        }
        else
        {
            GroupDetailsEditValue = TLS_Round(GroupDetailsEditValue, 50);
            GroupDetailsEditValue += value;
            GroupDetailsEditValue = TLS_Round(GroupDetailsEditValue, 50);
        }
        if (GroupDetailsEditValue > maxsetpoint)
        {
            GroupDetailsEditValue = maxsetpoint;
        }
        DPH_DisplaySetpointApplied(GroupDetailsEditState);
        if (FALSE == isGroupFilledOnlyWithTstatInFloorMode(MNU_Groups_GetSelectedGroup()))
        {
            DPH_DisplaySetpoint(GroupDetailsEditValue, FALSE, FALSE, SET_GROUP, FALSE);
        }
        else
        {
            DPH_DisplaySetpoint(GroupDetailsEditValue, FALSE, FALSE, SET_GROUP_FLOOR, FALSE);
        }
    }

	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_GroupDetails_CommitSetpoint, (U8BIT*)NULL, SETPOINT_EDIT_TIMEOUT);
}

/*******************************************************************************
    MNU_GroupDetails_DecSetpoint

    Decrement a group setpoint
*******************************************************************************/
void MNU_GroupDetails_DecSetpoint(int16u value)
{
	UnitFormat_t format;
	TEMPERATURE_C_t minsetpoint;

    minsetpoint = STPM_GetSetpointMin();
    GroupDetailsEditValue = MNU_GroupDetails_GetSetpoint();

    GroupDetailsEditState = EDIT_IN_PROGRESS;

    if ((GroupDetailsEditValue > minsetpoint) && (GroupDetailsEditValue != DB_SENSOR_INVALID))
    {
        DBTHRD_GetData(DBTYPE_TEMPERATURE_FORMAT,(void*)&format, INDEX_DONT_CARE);
        if(format == DEGREE_F)
        {
            GroupDetailsEditValue = CVT_ConvertCtoF(GroupDetailsEditValue, 100);
            GroupDetailsEditValue -= (value * 2);
            GroupDetailsEditValue = CVT_ConvertFtoC(GroupDetailsEditValue, 0);
        }
        else
        {
            GroupDetailsEditValue = TLS_Round(GroupDetailsEditValue, 50);
            GroupDetailsEditValue -= value;
            GroupDetailsEditValue = TLS_Round(GroupDetailsEditValue, 50);
        }
        if (GroupDetailsEditValue < minsetpoint)
        {
            GroupDetailsEditValue = minsetpoint;
        }
        DPH_DisplaySetpointApplied(GroupDetailsEditState);
        if (FALSE == isGroupFilledOnlyWithTstatInFloorMode(MNU_Groups_GetSelectedGroup()))
        {
            DPH_DisplaySetpoint(GroupDetailsEditValue, FALSE, FALSE, SET_GROUP, FALSE);
        }
        else
        {
            DPH_DisplaySetpoint(GroupDetailsEditValue, FALSE, FALSE, SET_GROUP_FLOOR, FALSE);
        }
    }

    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_GroupDetails_CommitSetpoint, (U8BIT*)NULL, SETPOINT_EDIT_TIMEOUT);
}

/*******************************************************************************
    MNU_GroupDetails_DecSetpointStartTurbo

    Decrement a group setpoint
*******************************************************************************/
void MNU_GroupDetails_DecSetpointStartTurbo(void)
{
    KBH_SetKeyHoldProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

    // Decrement setpoint
    MNU_GroupDetails_DecSetpoint(50);
    // Turn on key repeat notification
    KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)MNU_GroupDetails_DecSetpoint, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_DELAY));

    KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_GroupDetails_SetpointStopTurbo, (void*)NULL);
    KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_GroupDetails_SetpointStopTurbo, (void*)NULL);
}

/*******************************************************************************
    MNU_GroupDetails_IncSetpointStartTurbo

    Increment a setpoint
*******************************************************************************/
void MNU_GroupDetails_IncSetpointStartTurbo(void)
{
    KBH_SetKeyHoldProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

    // Increment setpoint
    MNU_GroupDetails_IncSetpoint(50);
    // Turn on key repeat notification
    KBH_SetKeyRepeatProperties(KEY_UP, (FUNC)MNU_GroupDetails_IncSetpoint, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_DELAY));

    KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_GroupDetails_SetpointStopTurbo, (void*)NULL);
    KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_GroupDetails_SetpointStopTurbo, (void*)NULL);
}

/*******************************************************************************
    MNU_GroupDetails_SetpointStopTurbo
*******************************************************************************/
void MNU_GroupDetails_SetpointStopTurbo(void)
{
    // Stop key repeat monitoring
	KBH_SetKeyRepeatProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

    // Start key hold monitoring
	KBH_SetKeyHoldProperties(KEY_UP, (FUNC)MNU_GroupDetails_IncSetpointStartTurbo, (void*) NULL, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_GroupDetails_DecSetpointStartTurbo, (void*) NULL, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
	KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_GroupDetails_IncSetpoint, (int16u*) 50);
    KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_GroupDetails_DecSetpoint, (int16u*) 50);
}

/*******************************************************************************
    MNU_GroupDetails_GetSetpoint

    Get the group setpoint from the Setpoint Manager
*******************************************************************************/
TEMPERATURE_C_t MNU_GroupDetails_GetSetpoint(void)
{
    TEMPERATURE_C_t setpoint;

    // If not currently in setpoint edition mode
    if ( GroupDetailsEditState == EDIT_IDLE )
    {
        // Return control setpoint
		DBTHRD_GetData(DBTYPE_GROUP_SETPOINT,(void*)&setpoint, MNU_Groups_GetSelectedGroup());
    }
    // Else currently in setpoint edition mode
    else
    {
        // Return current edited setpoint value
        setpoint = GroupDetailsEditValue;
    }
    return ( setpoint );
}

/*******************************************************************************
    MNU_GroupDetails_CommitSetpoint

    Update the Setpoint Manager with a new group setpoint
*******************************************************************************/
void MNU_GroupDetails_CommitSetpoint( void )
{
    TEMPERATURE_C_t     currentSetpoint;

	DBTHRD_GetData(DBTYPE_GROUP_SETPOINT,(void*)&currentSetpoint, MNU_Groups_GetSelectedGroup());

    if ( GroupDetailsEditState == EDIT_IN_PROGRESS )
    {
        // If user changed group setpoint
        if (( GroupDetailsEditValue != currentSetpoint ) && (GroupDetailsEditValue != DB_SENSOR_INVALID))
        {
            // Apply user group setpoint
			DBTHRD_SetData(DBTYPE_GROUP_SETPOINT,(void*)&GroupDetailsEditValue, MNU_Groups_GetSelectedGroup(), DBCHANGE_LOCAL);
            MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_GroupDetails_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
        }
    }
    MNU_GroupDetails_StopSetpointEdition();
}

/*******************************************************************************
    MNU_GroupDetails_GetSelectedTstat

    Retrieve the selected thermostat
*******************************************************************************/
void MNU_GroupDetails_GetSelectedTstat(uint8_t* tstat)
{
    *tstat = SelectedTstatNumber;
}
