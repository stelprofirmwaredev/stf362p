/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       KeyboardHandler.c

    \brief      Source code file for the KeyboardHandler module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the KeyboardHandler core implementation.

*******************************************************************************/

/*******************************************************************************
    INCLUDES
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "HalKeyset.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"

/*******************************************************************************
    DEFINITIONS
*******************************************************************************/
typedef struct
{
    FUNC    hdlrPress;    //! key pressed event handler
    void *  argsPress;    //! key pressed event handler argument
    FUNC    hdlrRelease;  //! key released event handler
    void *  argsRelease;  //! key released event handler argument
    FUNC    hdlrRepeat;   //! key repeat event handler
    void *  argsRepeat;   //! key repeat event handler argument
    FUNC    hdlrHold;     //! key hold event handler
    void *  argsHold;     //! key hold event handler argument
    FUNC    hdlrSwipe;    //! key swipe event handler
    void *  argsSwipe;    //! key swipe event handler argument
    U8BIT   ignoreRelease;
} KeyProperties_t;

/*******************************************************************************
    DATA
*******************************************************************************/
//! Table containing all key properties
static KeyProperties_t kbhKeyProperties[NB_OF_KEYS];
static HalKeysetState_t previousKeysetState;

/*******************************************************************************
    DECLARATIONS
*******************************************************************************/
static void KbhNotifyKeyPressEvent( KbhKeyId_e keyId );
static void KbhNotifyKeySwipeEvent( KbhKeyId_e keyId, int8u swipeDirection );
static void KbhNotifyKeyReleaseEvent( KbhKeyId_e keyId );
static void KbhNotifyKeyRepeatEvent( KbhKeyId_e keyId );
static void KbhNotifyKeyHoldEvent( KbhKeyId_e keyId );

/*******************************************************************************
    IMPLEMENTATION
*******************************************************************************/

/**
 * KBH_Init: Initializes the keyboard handler, bind to HAL keyset event handler
 * and start HAL keyset polling task.
 */
void KBH_Init(void)
{
#ifdef DEVKIT_PLATFORM
    GPIO_InitTypeDef GPIO_InitStruct;
#endif /* DEVKIT_PLATFORM */

    // Clear all key properties
    KBH_ClearAllKeyProperties();

    // Bind hal keyset event callback
    HAL_KeysetBind(KBH_KeysetEventHandler);

    // Start hal keyset polling task
    OS_StartTask(TSK_HAL_TOUCH);

//    GPIO_InitStruct.Pin = BTN_DOWN_Pin;
//    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
//    GPIO_InitStruct.Pull = GPIO_PULLUP;
//    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
//    HAL_GPIO_Init(BTN_DOWN_GPIO_Port, &GPIO_InitStruct);

#ifdef DEVKIT_PLATFORM
    GPIO_InitStruct.Pin = BTN_UP_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(BTN_UP_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = BTN_ENTER_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(BTN_ENTER_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = BTN_LEFT_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(BTN_LEFT_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = BTN_RIGHT_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(BTN_RIGHT_GPIO_Port, &GPIO_InitStruct);

    OS_StartTask(TSK_SIM_ALERT);
#endif /* DEVKIT_PLATFORM */
}


/**
 * KBH_KeysetEventHandler: Processes key state change received from HAL keyset
 * interface.
 *
 * @param newKeysetState
 *   New keyset state as set by the HAL keyset interface
 */
void KBH_KeysetEventHandler ( HalKeysetState_t newKeysetState )
{
    KbhKeyId_e i;

    // For all used keys
    for ( i = KEY_FIRST; i < NB_OF_KEYS; i = (KbhKeyId_e)(i + 1) ) {

        if ( previousKeysetState[i].press != newKeysetState[i].press ) {
            //
            // CASE key pressed event
            //
            if ( previousKeysetState[i].press == 0 && newKeysetState[i].press == 1 ) {
                // Notify registered observer
                KbhNotifyKeyPressEvent(i);
            }

            //
            // CASE key released event
            //
            if (HAL_AreAllKeysReleased())
            {
                if ( previousKeysetState[i].press == 1 && newKeysetState[i].press == 0 ) {
                    if (kbhKeyProperties[i].ignoreRelease == FALSE)
                    {
                        // Notify registered observer
                        KbhNotifyKeyReleaseEvent(i);
                    }
                    kbhKeyProperties[i].ignoreRelease = FALSE;
                }
            }
        }

        //
        // CASE key swiped event
        //
        if ( newKeysetState[i].swipe == 1 ) {
            // Notify registered observer
            KbhNotifyKeySwipeEvent(i, newKeysetState[i].swipeDirection);
            newKeysetState[i].swipe = 0;
        }

        //
        // CASE button repeated event
        //
        if ( previousKeysetState[i].repeat != newKeysetState[i].repeat ) {
            if ( previousKeysetState[i].repeat == 0 && newKeysetState[i].repeat == 1 ) {
                KbhNotifyKeyRepeatEvent(i);
            }
        }

        //
        // CASE button hold event
        //
        if ( previousKeysetState[i].hold != newKeysetState[i].hold ) {
            if ( previousKeysetState[i].hold == 0 && newKeysetState[i].hold == 1 ) {
                KbhNotifyKeyHoldEvent(i);
            }
        }

        // Update previous for later comparison
        previousKeysetState[i] = newKeysetState[i];
    // Endfor
    }
}


/**
 * KBH_KeyClearPreviousState: Clear the previous keys state
 */
void KBH_KeyClearPreviousState ( KbhKeyId_e keyId )
{
    previousKeysetState[keyId].press = 0;
    previousKeysetState[keyId].swipe = 0;
    previousKeysetState[keyId].hold = 0;
    previousKeysetState[keyId].repeat = 0;
}


/**
 * KBH_SetKeyPressProperties: Registers key press handler and arguments.
 *
 * @param keyId
 *   Key identifier
 * @param handler
 *   Address of key press event handler
 * @param args
 *   Address of key press event handler arguments
 */
void KBH_SetKeyPressProperties( KbhKeyId_e keyId, FUNC handler, void *args)
{
    if (keyId < NB_OF_KEYS) {
        kbhKeyProperties[keyId].hdlrPress = handler;
        kbhKeyProperties[keyId].argsPress = args;
    }
}

extern void KBH_SetKeySwipeProperties(KbhKeyId_e keyId, FUNC handler, void *args)
{
    if (keyId < NB_OF_KEYS) {
        kbhKeyProperties[keyId].hdlrSwipe = handler;
        kbhKeyProperties[keyId].argsSwipe = args;
    }
}

void KBH_IgnoreKeyRelease(KbhKeyId_e keyId)
{
    if (keyId < NB_OF_KEYS) {
        kbhKeyProperties[keyId].ignoreRelease = TRUE;
    }
}

/**
 * KBH_SetKeyReleaseProperties: Registers key release handler and arguments.
 *
 * @param keyId
 *   Key identifier
 * @param handler
 *   Address of key release event handler
 * @param args
 *   Address of key release event handler arguments
 */
void KBH_SetKeyReleaseProperties( KbhKeyId_e keyId, FUNC handler, void *args )
{
//    HalKeyState_t keyStatus;

    if (keyId < NB_OF_KEYS) {
//        keyStatus = HAL_KeysetGetStatus((KbhKeyId_e)keyId);
//        if (keyStatus.press == 0)
//        {
            kbhKeyProperties[keyId].hdlrRelease = handler;
            kbhKeyProperties[keyId].argsRelease = args;
//        }
    }
}


/**
 * KBH_SetKeyRepeatProperties: Registers key repeat handler and arguments.
 *
 * @param keyId
 *   Key identifier
 * @param handler
 *   Address of key repeat event handler
 * @param args
 *   Address of key repeat event handler arguments
 */
void KBH_SetKeyRepeatProperties( KbhKeyId_e keyId, FUNC handler, void *args, int16u delay )
{
    if (keyId < NB_OF_KEYS) {
        kbhKeyProperties[keyId].hdlrRepeat = handler;
		kbhKeyProperties[keyId].argsRepeat = args;
        HAL_KeysetSetKeyRepeatDelay(TO_HAL_KEY_ID(keyId), delay);
    }
}


/**
 * KBH_SetKeyHoldProperties: Registers key hold handler and arguments.
 *
 * @param keyId
 *   Key identifier
 * @param handler
 *   Address of key hold event handler
 * @param args
 *   Address of key hold event handler arguments
 */
void KBH_SetKeyHoldProperties( KbhKeyId_e keyId, FUNC handler, void *args, int16u delay )
{
    if (keyId < NB_OF_KEYS) {
        kbhKeyProperties[keyId].hdlrHold = handler;
        kbhKeyProperties[keyId].argsHold = args;
        HAL_KeysetSetKeyHoldDelay(TO_HAL_KEY_ID(keyId), delay);
    }
}


/**
 * KBH_ClearAllKeyProperties: Unregister all key timers and event handlers.
 */
void KBH_ClearAllKeyProperties( void )
{
    KbhKeyId_e i;
    for ( i = KEY_FIRST; i < NB_OF_KEYS; i = (KbhKeyId_e)(i + 1) ) {
        KBH_ClearKeyProperties(i);
    }
    MNU_SetKeyAssigned(FALSE);
}


/**
 * KBH_ClearKeyProperties: Unregister key timers and event handlers.
 *
 * @param keyId
 *   Key identifier
 */
void KBH_ClearKeyProperties( KbhKeyId_e keyId )
{
    if ( keyId < NB_OF_KEYS ) {
        kbhKeyProperties[keyId].hdlrPress = NULL;
        kbhKeyProperties[keyId].argsPress = NULL;

        kbhKeyProperties[keyId].hdlrRelease = NULL;
        kbhKeyProperties[keyId].argsRelease = NULL;

        kbhKeyProperties[keyId].hdlrRepeat = NULL;
        kbhKeyProperties[keyId].argsRepeat = NULL;

        kbhKeyProperties[keyId].hdlrHold = NULL;
        kbhKeyProperties[keyId].argsHold = NULL;

        kbhKeyProperties[keyId].ignoreRelease = FALSE;

        HAL_KeysetClearKeyTimers(TO_HAL_KEY_ID(keyId));
    }
}


/**
 * KbhNotifyKeyPressEvent: Invokes key press event handler if registered.
 *
 * @param keyId
 *   Key identifier
 */
static void KbhNotifyKeyPressEvent( KbhKeyId_e keyId )
{
    // If key press event handler registered
    if ( ( kbhKeyProperties[keyId].hdlrPress ) != NULL ) {
        // Invoke key press event handler
        (*(void(*)(void *)) kbhKeyProperties[keyId].hdlrPress )( kbhKeyProperties[keyId].argsPress );
        // Ask for menu refresh
        OS_StartTask(TSK_APP_MNH_MENU);
    // Endif
    }
}

/**
 * KbhNotifyKeySwipeEvent: Invokes key swipe event handler if registered.
 *
 * @param keyId
 *   Key identifier
 */
static void KbhNotifyKeySwipeEvent( KbhKeyId_e keyId, int8u swipeDirection )
{
    // If key swipe event handler registered
    if ( ( kbhKeyProperties[keyId].hdlrSwipe ) != NULL ) {
        // Invoke key swipe event handler
        (*(void(*)(void *)) kbhKeyProperties[keyId].hdlrSwipe )( (void*)swipeDirection );
        // Ask for menu refresh
        OS_StartTask(TSK_APP_MNH_MENU);
    // Endif
    }
}

/**
 * KbhNotifyKeyReleaseEvent: Invokes key release event handler if registered.
 * the services.
 *
 * @param keyId
 *   Key identifier
 */
static void KbhNotifyKeyReleaseEvent( KbhKeyId_e keyId )
{
    // If key release event handler registered
    if ( ( kbhKeyProperties[keyId].hdlrRelease ) != NULL ) {
        // Invoke key release event handler
        (*(void(*)(void *)) kbhKeyProperties[keyId].hdlrRelease )( kbhKeyProperties[keyId].argsRelease );
        // Ask for menu refresh
    // Endif
    }
        OS_StartTask(TSK_APP_MNH_MENU);
}


/**
 * KbhNotifyKeyRepeatEvent: Invokes key repeat event handler if registered.
 * the services.
 *
 * @param keyId
 *   Key identifier
 */
static void KbhNotifyKeyRepeatEvent( KbhKeyId_e keyId )
{
    // If key repeat event handler registered
    if ( ( kbhKeyProperties[keyId].hdlrRepeat ) != NULL ) {
        // Invoke key repeat event handler
        (*(void(*)(void *)) kbhKeyProperties[keyId].hdlrRepeat )( kbhKeyProperties[keyId].argsRepeat );
        // Ask for menu refresh
        OS_StartTask(TSK_APP_MNH_MENU);
    // Endif
    }
}


/**
 * KbhNotifyKeyHoldEvent: Invokes key hold event handler if registered.
 *
 * @param keyId
 *   Key identifier
 */
static void KbhNotifyKeyHoldEvent( KbhKeyId_e keyId )
{
    // If key hold event handler registered
    if ( ( kbhKeyProperties[keyId].hdlrHold ) != NULL ) {
        // Invoke key hold event handler
        (*(void(*)(void *)) kbhKeyProperties[keyId].hdlrHold )( kbhKeyProperties[keyId].argsHold );
        // Ask for menu refresh
        OS_StartTask(TSK_APP_MNH_MENU);
    // Endif
    }
}
