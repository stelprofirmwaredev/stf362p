/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       LCDTestMenu.c

    \brief      Source code file for the LCDTestMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the LCDTestMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "LCDTestMenu.h"
#include "WiFiScanMenu.h"

#include "StringObjects.h"
#include "images.h"
#include ".\HAL\inc\Display.h"
#include ".\WiFi\inc\THREAD_WiFi.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/


/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;


/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e       LCDTestEditState = EDIT_IDLE;
uint8_t              TargetButton = 0;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void ManageTouchPanel(void);
static void LCDTestTimeout(void);
static void ChangePage(KEYB_PARAM keyId);
static void GetTargetCoordinates(uint16_t* tx, uint16_t* ty);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    Manage LCDTestTimeout panel

    Timeout function that erases the screen
*******************************************************************************/
static void LCDTestTimeout(void)
{
    uint16_t targetX, targetY;

    HAL_ClearScreen();
    if (TargetButton == 0)
    {
        //DPH_DisplayKeyboard(KEY_PAGE_CAPITAL);
    }
    else
    {
        //DPH_DisplayKeyboard(KEY_PAGE_TEST);
    }
    GetTargetCoordinates(&targetX, &targetY);
    DPH_DisplayLCDTest(0, 0, targetX, targetY);
}

static void GetTargetCoordinates(uint16_t* tx, uint16_t* ty)
{
    switch (TargetButton)
    {
        case 1:
            *tx = 33+15;
            *ty = 329+18;
            break;

        case 2:
            *tx = 49+15;
            *ty = 367+18;
            break;

        case 3:
            *tx = 49+15;
            *ty = 405+18;
            break;

        case 4:
            *tx = 257+15;
            *ty = 329+18;
            break;

        case 5:
            *tx = 241+15;
            *ty = 367+18;
            break;

        case 6:
            *tx = 241+15;
            *ty = 405+18;
            break;

        case 7:
            *tx = 145+15;
            *ty = 367+18;
            break;

        case 8:
            *tx = 100+15;
            *ty = 100+18;
            break;

        case 9:
            *tx = 275+15;
            *ty = 110+18;
            break;

        default:
            *tx = 0;
            *ty = 0;
            break;
    }
}

/*******************************************************************************
    Manage touch panel

    Detect the touch panel coordinates
*******************************************************************************/
#pragma optimize = none
static void ManageTouchPanel(void)
{
    uint16_t targetX, targetY;
    TS_StateTypeDef touchstruct;

    //MNH_SetTimeoutCallback((MnhTimeoutCallback_t)LCDTestTimeout,  (KEYB_PARAM*)KEY_HOME, REMOVE_TSTAT_TIMEOUT);
    touchstruct = HAL_GetTouch();

    GetTargetCoordinates(&targetX, &targetY);

    DPH_DisplayLCDTest(touchstruct.X, touchstruct.Y, targetX, targetY);
}

static void ChangePage(KEYB_PARAM keyId)
{
    HAL_ClearScreen();
    if (keyId == KEY_NEXT)
    {
        TargetButton++;
        if (TargetButton > 9)
        {
            TargetButton = 0;
        }
    }
    else if (keyId == KEY_PREVIOUS)
    {
        if (TargetButton > 0)
        {
            TargetButton--;
        }
        else
        {
            TargetButton = 9;
        }
    }
    ManageTouchPanel();
    if (TargetButton == 0)
    {
        //DPH_DisplayKeyboard(KEY_PAGE_CAPITAL);
    }
    else
    {
        //DPH_DisplayKeyboard(KEY_PAGE_TEST);
    }
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_LCDTest_Entry
*******************************************************************************/
void MNU_LCDTest_Entry(void)
{
    LCDTestEditState = EDIT_IN_PROGRESS;

    LCDTestTimeout();

    MNU_SetKeyAssigned(FALSE);

	//MNH_SetTimeoutCallback((MnhTimeoutCallback_t)HAL_ClearScreen,  (KEYB_PARAM*)KEY_HOME, REMOVE_TSTAT_TIMEOUT);
}

/*******************************************************************************
    MNU_LCDTest_Core

    Test the LCD and touch panel alignment
*******************************************************************************/
void MNU_LCDTest_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_NEXT, (FUNC)ChangePage, (void*) KEY_NEXT);
        KBH_SetKeyPressProperties(KEY_PREVIOUS, (FUNC)ChangePage, (void*) KEY_PREVIOUS);
        KBH_SetKeyPressProperties(KEY_SCREEN, (FUNC)ManageTouchPanel, (void*)NULL);
        KBH_SetKeyRepeatProperties(KEY_SCREEN, (FUNC)ManageTouchPanel, (void*)NULL, OS_MSTOTICK(KEYB_REPEAT_DELAY));

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_LCDTest_Exit
*******************************************************************************/
void MNU_LCDTest_Exit(void)
{
    LCDTestEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_LCDTest_Commit
*******************************************************************************/
void MNU_LCDTest_Commit(KEYB_PARAM keyId)
{
	if (LCDTestEditState == EDIT_IN_PROGRESS)
	{
        KBH_ClearAllKeyProperties();
        MNH_SetActiveMenu(MENU_HOME);
		LCDTestEditState = EDIT_IDLE;
	}
}
