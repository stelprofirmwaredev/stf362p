/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2015, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    HAL_Interrupt.c
* @date    2016/07/14
* @authors Jean-Fran�ois Many
* @brief   Interrupt module
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include "stm32f4xx.h"
#include "HAL_Interrupt.h"
#include "OSTask.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/



/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/**
  * @brief  SYSTICK callback.
  * @retval None
  */
void HAL_SYSTICK_Callback(void)
{
    OS_HandleTickInterrupt();
}

/**
 * HAL_InterruptDisableGlobalInterrupt:
 */
void HAL_InterruptDisableGlobalInterrupt(void)
{
    __disable_interrupt();
}


/** Copyright(C) 2014 Stelpro Design, All Rights Reserved**/
