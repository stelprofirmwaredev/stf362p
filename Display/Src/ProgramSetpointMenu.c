/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2019, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    ProgramSetpointMenu.c
* @date    2019/07/29
* @authors Jean-Fran�ois Many
* @brief   Program Setpoint Menu
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include "typedef.h"
#include "convert.h"
#include "tools.h"
#include "KeyboardConfig.h"
#include "KeyboardHandler.h"
#include "MenuTable.h"
#include "MenuHandlerIf.h"
#include "APP_Display.h"
#include "APP_SetpointManager.h"
#include ".\DB\inc\THREAD_DB.h"
#include "ModeSelectionMenu.h"
#include "ProgramSetpointMenu.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_CONFIRM,
    EDIT_COMMITTING
} MnuEditState_e;


/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
static MnuEditState_e   ProgramSetpointEditState = EDIT_IDLE;
static TEMPERATURE_C_t ProgramSetpointEditValue;
static Program_t ProgramToEdit;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/


/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
void MNU_ProgramSetpoint_Entry(void)
{
    ProgramSetpointEditState = EDIT_IDLE;

    MNU_SetKeyAssigned(FALSE);

    ProgramToEdit = MNU_GetSelectedProgram();
    switch (ProgramToEdit)
    {
        case PGM_WAKE:
            DBTHRD_GetData(DBTYPE_WAKE_SETPOINT,(void*)&ProgramSetpointEditValue, THIS_THERMOSTAT);
            break;

        case PGM_LEAVE:
            DBTHRD_GetData(DBTYPE_LEAVE_SETPOINT,(void*)&ProgramSetpointEditValue, THIS_THERMOSTAT);
            break;

        case PGM_RETURN:
            DBTHRD_GetData(DBTYPE_RETURN_SETPOINT,(void*)&ProgramSetpointEditValue, THIS_THERMOSTAT);
            break;

        case PGM_SLEEP:
            DBTHRD_GetData(DBTYPE_SLEEP_SETPOINT,(void*)&ProgramSetpointEditValue, THIS_THERMOSTAT);
            break;

        default:
            break;
    }

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show Program Setpoint
    //
    DPH_DisplaySetpointApplied(ProgramSetpointEditState);
    DPH_DisplayProgramSetpoint(TRUE,ProgramSetpointEditValue, ProgramToEdit);

	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_ProgramSetpoint_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
void MNU_ProgramSetpoint_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_ProgramSetpoint_Commit, (void*)KEY_BACK);
        KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_ProgramSetpoint_DecProgramSetpointStartTurbo, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
        KBH_SetKeyHoldProperties(KEY_UP, (FUNC)MNU_ProgramSetpoint_IncProgramSetpointStartTurbo, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
        KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_ProgramSetpoint_IncProgramSetpoint, (int16u*)50);
        KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_ProgramSetpoint_DecProgramSetpoint, (int16u*)50);
        KBH_SetKeyPressProperties(KEY_OPTIONS_1, (FUNC)MNU_ProgramSetpoint_Commit, (void*)KEY_OPTIONS_1);

        // Stop key repeat monitoring
        KBH_SetKeyRepeatProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
        KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date
*******************************************************************************/
void MNU_ProgramSetpoint_Exit(void)
{
    MNU_ProgramSetpoint_StopProgramSetpointEdition();
}

/*******************************************************************************
    MNU_ProgramSetpoint_Confirm
*******************************************************************************/
void MNU_ProgramSetpoint_Confirm (void)
{
    ProgramSetpointEditState = EDIT_IDLE;
    DPH_DisplaySetpointApplied(ProgramSetpointEditState);
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_ProgramSetpoint_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_ProgramSetpoint_Commit
*******************************************************************************/
void MNU_ProgramSetpoint_Commit(KEYB_PARAM keyId)
{
    KBH_ClearAllKeyProperties();
    switch(keyId)
    {
        case KEY_BACK:
            MNH_SetActiveMenu(MENU_MODE_SELECTION);
            break;

        case KEY_OPTIONS_1:
            MNH_SetActiveMenu(MENU_PROGRAM_SCHEDULE);
            break;

        default:
            MNH_SetActiveMenu(MENU_HOME);
            break;
    }
}

/*******************************************************************************
    MNU_ProgramSetpoint_StopProgramSetpointEdition
*******************************************************************************/
void MNU_ProgramSetpoint_StopProgramSetpointEdition(void)
{
    ProgramSetpointEditState = EDIT_CONFIRM;
    DPH_DisplaySetpointApplied(ProgramSetpointEditState);
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_ProgramSetpoint_Confirm,  (KEYB_PARAM*)NULL, CONFIRM_TIMEOUT);
}

/*******************************************************************************
    MNU_ProgramSetpoint_IncProgramSetpoint

    Increment the Program Setpoint
*******************************************************************************/
void MNU_ProgramSetpoint_IncProgramSetpoint(int16u value)
{
	UnitFormat_t format;
	TEMPERATURE_C_t maxsetpoint;

    maxsetpoint= STPM_GetSetpointMax();

    ProgramSetpointEditState = EDIT_IN_PROGRESS;

    if (ProgramSetpointEditValue < maxsetpoint)
    {
        DBTHRD_GetData(DBTYPE_TEMPERATURE_FORMAT,(void*)&format, INDEX_DONT_CARE);
        if(format == DEGREE_F)
        {
            ProgramSetpointEditValue = CVT_ConvertCtoF(ProgramSetpointEditValue, 100);
            ProgramSetpointEditValue += (value * 2);
            ProgramSetpointEditValue = CVT_ConvertFtoC(ProgramSetpointEditValue, 0);
        }
        else
        {
            ProgramSetpointEditValue = TLS_Round(ProgramSetpointEditValue, 50);
            ProgramSetpointEditValue += value;
            ProgramSetpointEditValue = TLS_Round(ProgramSetpointEditValue, 50);
        }
        if (ProgramSetpointEditValue > maxsetpoint)
        {
            ProgramSetpointEditValue = maxsetpoint;
        }
        DPH_DisplaySetpointApplied(ProgramSetpointEditState);
        DPH_DisplayProgramSetpoint(FALSE,ProgramSetpointEditValue, ProgramToEdit);
    }

    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_ProgramSetpoint_CommitProgramSetpoint, (U8BIT*)NULL, SETPOINT_EDIT_TIMEOUT);
}

/*******************************************************************************
    MNU_ProgramSetpoint_DecProgramSetpoint

    Decrement the Program Setpoint
*******************************************************************************/
void MNU_ProgramSetpoint_DecProgramSetpoint(int16u value)
{
	UnitFormat_t format;
	TEMPERATURE_C_t minsetpoint;

    minsetpoint = STPM_GetSetpointMin();

    ProgramSetpointEditState = EDIT_IN_PROGRESS;

    if (ProgramSetpointEditValue > minsetpoint)
    {
        DBTHRD_GetData(DBTYPE_TEMPERATURE_FORMAT,(void*)&format, INDEX_DONT_CARE);
        if(format == DEGREE_F)
        {
            ProgramSetpointEditValue = CVT_ConvertCtoF(ProgramSetpointEditValue, 100);
            ProgramSetpointEditValue -= (value * 2);
            ProgramSetpointEditValue = CVT_ConvertFtoC(ProgramSetpointEditValue, 0);
        }
        else
        {
            ProgramSetpointEditValue = TLS_Round(ProgramSetpointEditValue, 50);
            ProgramSetpointEditValue -= value;
            ProgramSetpointEditValue = TLS_Round(ProgramSetpointEditValue, 50);
        }
        if (ProgramSetpointEditValue < minsetpoint)
        {
            ProgramSetpointEditValue = minsetpoint;
        }
        DPH_DisplaySetpointApplied(ProgramSetpointEditState);
        DPH_DisplayProgramSetpoint(FALSE,ProgramSetpointEditValue, ProgramToEdit);
    }

    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_ProgramSetpoint_CommitProgramSetpoint, (U8BIT*)NULL, SETPOINT_EDIT_TIMEOUT);
}

/*******************************************************************************
    MNU_ProgramSetpoint_DecProgramSetpointStartTurbo

    Decrement the Program Setpoint
*******************************************************************************/
void MNU_ProgramSetpoint_DecProgramSetpointStartTurbo(void)
{
    KBH_SetKeyHoldProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

    // Decrement the Program Setpoint
    MNU_ProgramSetpoint_DecProgramSetpoint(50);
    // Turn on key repeat notification
    KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)MNU_ProgramSetpoint_DecProgramSetpoint, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_DELAY));

    KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_ProgramSetpoint_ProgramSetpointStopTurbo, (void*)NULL);
    KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_ProgramSetpoint_ProgramSetpointStopTurbo, (void*)NULL);
}

/*******************************************************************************
    MNU_ProgramSetpoint_IncProgramSetpointStartTurbo

    Increment the Program Setpoint
*******************************************************************************/
void MNU_ProgramSetpoint_IncProgramSetpointStartTurbo(void)
{
    KBH_SetKeyHoldProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

    // Increment the Program Setpoint
    MNU_ProgramSetpoint_IncProgramSetpoint(50);
    // Turn on key repeat notification
    KBH_SetKeyRepeatProperties(KEY_UP, (FUNC)MNU_ProgramSetpoint_IncProgramSetpoint, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_DELAY));

    KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_ProgramSetpoint_ProgramSetpointStopTurbo, (void*)NULL);
    KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_ProgramSetpoint_ProgramSetpointStopTurbo, (void*)NULL);
}

/*******************************************************************************
    MNU_ProgramSetpoint_ProgramSetpointStopTurbo
*******************************************************************************/
void MNU_ProgramSetpoint_ProgramSetpointStopTurbo(void)
{
    // Stop key repeat monitoring
	KBH_SetKeyRepeatProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

    // Start key hold monitoring
	KBH_SetKeyHoldProperties(KEY_UP, (FUNC)MNU_ProgramSetpoint_IncProgramSetpointStartTurbo, (void*) NULL, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_ProgramSetpoint_DecProgramSetpointStartTurbo, (void*) NULL, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
	KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_ProgramSetpoint_IncProgramSetpoint, (int16u*) 50);
    KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_ProgramSetpoint_DecProgramSetpoint, (int16u*) 50);
}

/*******************************************************************************
    MNU_ProgramSetpoint_CommitProgramSetpoint

    Update the Setpoint Manager with a new Program Setpoint
*******************************************************************************/
void MNU_ProgramSetpoint_CommitProgramSetpoint( void )
{
    TEMPERATURE_C_t     currentProgramSetpoint;

    switch (ProgramToEdit)
    {
        case PGM_WAKE:
            DBTHRD_GetData(DBTYPE_WAKE_SETPOINT,(void*)&currentProgramSetpoint, THIS_THERMOSTAT);
            break;

        case PGM_LEAVE:
            DBTHRD_GetData(DBTYPE_LEAVE_SETPOINT,(void*)&currentProgramSetpoint, THIS_THERMOSTAT);
            break;

        case PGM_RETURN:
            DBTHRD_GetData(DBTYPE_RETURN_SETPOINT,(void*)&currentProgramSetpoint, THIS_THERMOSTAT);
            break;

        case PGM_SLEEP:
            DBTHRD_GetData(DBTYPE_SLEEP_SETPOINT,(void*)&currentProgramSetpoint, THIS_THERMOSTAT);
            break;

        default:
            break;
    }

    if ( ProgramSetpointEditState == EDIT_IN_PROGRESS )
    {
        // If user changed setpoint
        if ( ProgramSetpointEditValue != currentProgramSetpoint )
        {
            switch (ProgramToEdit)
            {
                case PGM_WAKE:
                    DBTHRD_SetData(DBTYPE_WAKE_SETPOINT,(void*)&ProgramSetpointEditValue, THIS_THERMOSTAT, DBCHANGE_LOCAL);
                    break;

                case PGM_LEAVE:
                    DBTHRD_SetData(DBTYPE_LEAVE_SETPOINT,(void*)&ProgramSetpointEditValue, THIS_THERMOSTAT, DBCHANGE_LOCAL);
                    break;

                case PGM_RETURN:
                    DBTHRD_SetData(DBTYPE_RETURN_SETPOINT,(void*)&ProgramSetpointEditValue, THIS_THERMOSTAT, DBCHANGE_LOCAL);
                    break;

                case PGM_SLEEP:
                    DBTHRD_SetData(DBTYPE_SLEEP_SETPOINT,(void*)&ProgramSetpointEditValue, THIS_THERMOSTAT, DBCHANGE_LOCAL);
                    break;

                default:
                    break;
            }
            MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_ProgramSetpoint_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
        }
    }
    MNU_ProgramSetpoint_StopProgramSetpointEdition();
}


/** Copyright(C) 2019 Stelpro Design, All Rights Reserved**/
