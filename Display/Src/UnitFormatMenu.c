/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       UnitFormatMenu.c

    \brief      Source code file for the UnitFormatMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the UnitFormatMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "UnitFormatMenu.h"
#include ".\DB\inc\THREAD_DB.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e           UnitFormatEditState = EDIT_IDLE;
static UnitFormat_t             UnitFormatEditValue;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void ChangeUnitFormat(KEYB_PARAM keyId);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    ChangeUnitFormat

    Change the temperature unit format to either �C or �F
*******************************************************************************/
static void ChangeUnitFormat(KEYB_PARAM keyId)
{
    uint8_t onboardingDone;

    switch (keyId)
    {
        case KEY_OPTIONS_2:
            UnitFormatEditValue = DEGREE_C;
            break;

        case KEY_OPTIONS_3:
            UnitFormatEditValue = DEGREE_F;
            break;

        default:
            break;
    }

    DPH_DisplayUnitFormat(UnitFormatEditValue, FALSE);

    DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);
    if (onboardingDone == 1)
    {
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_UnitFormat_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    }
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_UnitFormat_Entry
*******************************************************************************/
void MNU_UnitFormat_Entry(void)
{
    uint8_t onboardingDone;

    DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);
    DBTHRD_GetData(DBTYPE_TEMPERATURE_FORMAT,(void*)&UnitFormatEditValue, INDEX_DONT_CARE);
    UnitFormatEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show unit format
    //
    DPH_DisplayUnitFormat(UnitFormatEditValue, TRUE);

    if (onboardingDone == 1)
    {
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_UnitFormat_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    }
}

/*******************************************************************************
    MNU_UnitFormat_Core

    Allows to change the temperature unit format
*******************************************************************************/
void MNU_UnitFormat_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_UnitFormat_Commit, (void*)KEY_BACK);
        KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)ChangeUnitFormat, (void*)KEY_OPTIONS_2);
        KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)ChangeUnitFormat, (void*)KEY_OPTIONS_3);
        KBH_SetKeyPressProperties(KEY_DONE, (FUNC)MNU_UnitFormat_Commit, (void*)KEY_DONE);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_UnitFormat_Exit
*******************************************************************************/
void MNU_UnitFormat_Exit(void)
{
    UnitFormatEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_UnitFormat_Init
*******************************************************************************/
void MNU_UnitFormat_Init(void)
{
    MNU_UnitFormat_SetUnitFormat(DEGREE_C);
}

/*******************************************************************************
    MNU_UnitFormat_Commit
*******************************************************************************/
void MNU_UnitFormat_Commit(KEYB_PARAM keyId)
{
    static KEYB_PARAM commitKey = NULL;
    uint8_t onboardingDone;

    if ( UnitFormatEditState == EDIT_IN_PROGRESS )
    {
        KBH_ClearAllKeyProperties();
        commitKey = keyId;
        switch (commitKey)
        {
            case KEY_DONE:
                MNU_UnitFormat_SetUnitFormat(UnitFormatEditValue);
                //No break on purpose

            case KEY_BACK:
            default:
                MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_UnitFormat_Commit, (KEYB_PARAM*)commitKey, IMMEDIATELY);
                break;
        }
        UnitFormatEditState = EDIT_COMMITTING;
    }
    else if ( UnitFormatEditState == EDIT_COMMITTING )
    {
        UnitFormatEditState = EDIT_IDLE;
        DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);

        switch (commitKey)
        {
            case KEY_BACK:
                if (onboardingDone == 1)
                {
                    MNH_SetActiveMenu(MENU_OPTIONS);
                }
                else
                {
                    MNH_SetActiveMenu(MENU_LANGUAGE);
                }
                break;

            case KEY_DONE:
                if (onboardingDone == 1)
                {
                    MNH_SetActiveMenu(MENU_OPTIONS);
                }
                else
                {
                    MNH_SetActiveMenu(MENU_TIME_FORMAT);
                }
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
    }
}

/*******************************************************************************
    MNU_UnitFormat_SetUnitFormat

    Set the temperature unit format to �C or �F
*******************************************************************************/
void MNU_UnitFormat_SetUnitFormat(UnitFormat_t format)
{
    DBTHRD_SetData(DBTYPE_TEMPERATURE_FORMAT,(void*)&format, INDEX_DONT_CARE, DBCHANGE_LOCAL);
}
