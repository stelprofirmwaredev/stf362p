/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2017, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       UpdateMenu.c

    \brief      Source code file for the UpdateMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the UpdateMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "UpdateMenu.h"
#include ".\DB\inc\THREAD_DB.h"
#include "SwVersion.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/


/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;


/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   UpdateEditState = EDIT_IDLE;
static susbscriptionHandle_t hZigBeeVersionChangeNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hZigBeeAlertNotification = DBTHRD_NOTIFHANDLE_NOTSET;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/
    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  Notification update when the zigbee version is changed
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/07/12
*******************************************************************************/
static DB_NOTIFICATION(OnZigBeeVersionChange)
{
    uint8_t zigbee_version;
    uint8_t onboardingDone;

    DBTHRD_GetData(DBTYPE_ZIGBEE_VERSION,(void*)&zigbee_version, THIS_THERMOSTAT);
    if (zigbee_version == ZIGBEE_STACK_VERSION)
    {
        DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);
        if (onboardingDone == 1)
        {
            MNH_SetActiveMenu(MENU_HOME);
        }
        else
        {
            MNH_SetActiveMenu(MENU_LANGUAGE);
        }
    }
}

/*******************************************************************************
* @brief  Notification update when the zigbee version is changed
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/07/12
*******************************************************************************/
static DB_NOTIFICATION(OnZigBeeAlert)
{
    dbType_Alerts_t alerts;

    DBTHRD_GetData(DBTYPE_ACTIVE_ALERTS,(void*)&alerts, THIS_THERMOSTAT);
    for (uint8_t i = 0; i < ALERT_INSTANCES; i++)
    {
        if (alerts.ActiveAlerts[i] == DBALERT_ZIGBEE_ERROR)
        {
            MNH_SetActiveMenu(MENU_HOME);
        }
    }
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_Update_Entry
*******************************************************************************/
void MNU_Update_Entry(void)
{
    UpdateEditState = EDIT_IN_PROGRESS;

    //
    //  Show update notification
    //
    DPH_DisplayUpdate();
}

/*******************************************************************************
    MNU_Update_Core

    Allows to view the update's information
*******************************************************************************/
void MNU_Update_Core(void)
{
    if (UpdateEditState == EDIT_IN_PROGRESS)
	{
         hZigBeeVersionChangeNotification = DBTHRD_SubscribeToNotification(DBTYPE_ZIGBEE_VERSION,
                                                                OnZigBeeVersionChange,
                                                                DBCHANGE_ZIGBEE,
                                                                THIS_THERMOSTAT);
         hZigBeeAlertNotification = DBTHRD_SubscribeToNotification(DBTYPE_ACTIVE_ALERTS,
                                                                OnZigBeeAlert,
                                                                DBCHANGE_LOCAL,
                                                                THIS_THERMOSTAT);
    }
}

/*******************************************************************************
    MNU_Update_Exit
*******************************************************************************/
void MNU_Update_Exit(void)
{
    UpdateEditState = EDIT_IDLE;
    if (hZigBeeVersionChangeNotification != DBTHRD_NOTIFHANDLE_NOTSET)
    {
        DBTHRD_UnsubscribeToNotification(hZigBeeVersionChangeNotification);
        DBTHRD_UnsubscribeToNotification(hZigBeeAlertNotification);
    }
}
