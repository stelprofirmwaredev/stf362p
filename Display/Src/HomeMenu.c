/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       HomeMenu.c

    \brief      Source code file for the HomeMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the HomeMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/

#include "typedef.h"
#include "convert.h"
#include "tools.h"
#include "common_types.h"
#include "KeyboardHandler.h"
#include "KeyBoardConfig.h"
#include "APP_Display.h"
#include "MenuHandler.h"
#include "MenuHandlerIf.h"
#include "MenuTable.h"
#include "AlertsMenu.h"
#include ".\HAL\inc\Display.h"
#include "APP_SetpointManager.h"

#include ".\DB\inc\THREAD_DB.h"
#include ".\WiFi\inc\THREAD_WiFi.h"

#include "HomeMenu.h"

#ifdef CURRENT_DISPLAY
    #include ".\HAL\inc\HAL_CurrentSensing.h"
#endif /* CURRENT_DISPLAY */
#ifdef PIR_COUNT
    #include ".\HAL\inc\ProximitySensor.h"
#endif /* PIR_COUNT */


/*******************************************************************************
    Private constants definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_CONFIRM
} MnuEditState_e;

#ifdef FREERTOS_SUPPORT
    #include "trcRecorder.h"
    extern char* hmiEventChannel;
#endif

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   SetpointEditState = EDIT_IDLE;

static U8BIT time_display;
static U8BIT mode_display;
static U8BIT temp_display;
static U8BIT lock_display;
static U8BIT alert_display;
static U8BIT heat_display;
static U8BIT setpoint_display;
static U8BIT button_display;

static TEMPERATURE_C_t setpointEditValue;
static uint8_t minReached;
uint8_t offMode;

static susbscriptionHandle_t hTimeNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hAmbientTemperatureNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hFloorTemperatureNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hTimeFormatNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hAlertsNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hTemperatureUnitNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hLockStateNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hHeatDemandNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hUserActionNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hOffModeNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hAmbientSetpointNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hFloorSetpointNotification = DBTHRD_NOTIFHANDLE_NOTSET;
static susbscriptionHandle_t hActiveProgramNotification = DBTHRD_NOTIFHANDLE_NOTSET;

static TimerHandle_t BacklightTimeout = NULL;

#ifdef PIR_COUNT
static U8BIT adc_read_done;
#endif /* PIR_COUNT */

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
DB_NOTIFICATION(MNU_OnTimeUpdate);
DB_NOTIFICATION(MNU_OnAmbientTemperatureUpdate);
DB_NOTIFICATION(MNU_OnHomeStateUpdate);
DB_NOTIFICATION(MNU_OnAlertUpdate);
DB_NOTIFICATION(MNU_OnTemperatureUnitUpdate);
DB_NOTIFICATION(MNU_OnHeatDemandUpdate);
DB_NOTIFICATION(MNU_OnOffModeUpdate);
DB_NOTIFICATION(MNU_OnAmbientSetpointUpdate);
DB_NOTIFICATION(MNU_OnActiveProgramUpdate);
static DB_NOTIFICATION( ReceivedLockState_Callback);
static DB_NOTIFICATION( OnUserAction );

static void OnBacklightTimeout (TimerHandle_t xTimer);
static void MNU_RefreshDisplayItems(int16u refreshBits);

#ifdef LOAD_CYCLING
static void MNH_SetLoadCyclingMode(uint8_t mode);
#endif /* LOAD_CYCLING */
/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/10/26
*******************************************************************************/
static DB_NOTIFICATION( OnUserAction )
{
    int8u backlightState;

    backlightState = DSP_Backlight_GetBacklightState();

    if (backlightState != TRUE)
    {
        MNU_NotifyMenuChange();
        MNU_RefreshDisplayItems(ALL_DISPLAY_BIT);
    }

    xTimerReset( BacklightTimeout, 0);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/10/26
*******************************************************************************/
static void OnBacklightTimeout (TimerHandle_t xTimer)
{
    MNH_SetActiveMenu(MENU_IDLE);
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_Home_Entry
*******************************************************************************/
void MNU_Home_Entry(void)
{
    uint32_t bl_mode;
    ControlMode_t floorMode;
    ScheduleModeEnum_t scheduleMode;

    SetpointEditState = EDIT_IDLE;

    DBTHRD_GetData(DBTYPE_BACKLIGHT_SETTING,(void*)&bl_mode, INDEX_DONT_CARE);
    if (bl_mode < BL_60_SEC)
    {
        bl_mode = BL_60_SEC;
    }
    if (BacklightTimeout == NULL)
    {
        BacklightTimeout = xTimerCreate ("Backlight Timeout",
                                     pdMS_TO_TICKS(bl_mode),
                                     pdFALSE,               //one shot timer
                                     (void*)0,
                                     OnBacklightTimeout);
    }
    else
    {
        xTimerChangePeriod(BacklightTimeout, pdMS_TO_TICKS(bl_mode), 0);
    }

    xTimerReset(BacklightTimeout, 0);
    xTimerStart( BacklightTimeout, 0);

    MNU_SetKeyAssigned(FALSE);

    MNU_RefreshDisplayItems(ALL_DISPLAY_BIT);

    setpointEditValue = MNU_Setpoint_GetSetpoint();

    DBTHRD_GetData(DBTYPE_FLOOR_MODE, &floorMode, THIS_THERMOSTAT);
    DBTHRD_GetData(DBTYPE_OFF_MODE_SETTING,(void*)&offMode, THIS_THERMOSTAT);
    if (setpointEditValue == STPM_GetSetpointMin())
    {
        minReached = 1;
    }
    else
    {
        minReached = 0;
    }

    DBTHRD_GetData(DBTYPE_SCHEDULE_MODE,(void*)&scheduleMode, THIS_THERMOSTAT);
    if (scheduleMode == MODE_DEROGATION)
    {
        DPH_DisplaySetpoint(setpointEditValue, TRUE, offMode, SET_TEMPORARY, TRUE);
    }
    else
    {
        if (floorMode == CONTROLMODE_FLOOR)
        {
            DPH_DisplaySetpoint(setpointEditValue, TRUE, offMode, SET_FLOOR, TRUE);
        }
        else
        {
            DPH_DisplaySetpoint(setpointEditValue, TRUE, offMode, SET_AMBIENT, TRUE);
        }
    }

    hTimeNotification = DBTHRD_SubscribeToNotification(DBTYPE_MINUTE_CHANGE,
                                                       MNU_OnTimeUpdate,
                                                       DBCHANGE_ALLCHANGES,
                                                       INDEX_DONT_CARE);

	hAmbientTemperatureNotification = DBTHRD_SubscribeToNotification(DBTYPE_AMBIENT_TEMPERATURE,
                                                       MNU_OnAmbientTemperatureUpdate,
                                                       DBCHANGE_ALLCHANGES,
                                                       THIS_THERMOSTAT);

    hFloorTemperatureNotification = DBTHRD_SubscribeToNotification(DBTYPE_FLOOR_TEMPERATURE,
                                                       MNU_OnAmbientTemperatureUpdate,
                                                       DBCHANGE_ALLCHANGES,
                                                       THIS_THERMOSTAT);

    hTimeFormatNotification = DBTHRD_SubscribeToNotification( DBTYPE_TIME_FORMAT,
                                                       MNU_OnTimeUpdate,
                                                       DBCHANGE_ALLCHANGES,
                                                       INDEX_DONT_CARE);

    hAlertsNotification = DBTHRD_SubscribeToNotification( DBTYPE_ACTIVE_ALERTS,
                                                       MNU_OnAlertUpdate,
                                                       DBCHANGE_ALLCHANGES,
                                                       INDEX_DONT_CARE);

	hTemperatureUnitNotification =  DBTHRD_SubscribeToNotification( DBTYPE_TEMPERATURE_FORMAT,
                                                       MNU_OnTemperatureUnitUpdate,
                                                       DBCHANGE_ALLCHANGES,
                                                       INDEX_DONT_CARE);

    hLockStateNotification = DBTHRD_SubscribeToNotification( DBTYPE_LOCK_STATE,
                                                       ReceivedLockState_Callback,
                                                       DBCHANGE_ALLCHANGES,
                                                       INDEX_DONT_CARE);

    hHeatDemandNotification = DBTHRD_SubscribeToNotification( DBTYPE_HEAT_DEMAND,
                                                       MNU_OnHeatDemandUpdate,
                                                       DBCHANGE_LOCAL,
                                                       THIS_THERMOSTAT);

    hUserActionNotification = DBTHRD_SubscribeToNotification( DBTYPE_USER_ACTION,
                                                       OnUserAction,
                                                       DBCHANGE_LOCAL,
                                                       INDEX_DONT_CARE);

    hOffModeNotification = DBTHRD_SubscribeToNotification( DBTYPE_OFF_MODE_SETTING,
                                                       MNU_OnOffModeUpdate,
                                                       DBCHANGE_LOCAL,
                                                       THIS_THERMOSTAT);

    hAmbientSetpointNotification = DBTHRD_SubscribeToNotification(DBTYPE_AMBIENT_SETPOINT,
                                                       MNU_OnAmbientSetpointUpdate,
                                                       DBCHANGE_ALLCHANGES,
                                                       THIS_THERMOSTAT);

    hFloorSetpointNotification = DBTHRD_SubscribeToNotification(DBTYPE_FLOOR_SETPOINT,
                                                       MNU_OnAmbientSetpointUpdate,
                                                       DBCHANGE_ALLCHANGES,
                                                       THIS_THERMOSTAT);

    hActiveProgramNotification = DBTHRD_SubscribeToNotification(DBTYPE_ACTIVE_PGM,
                                                       MNU_OnActiveProgramUpdate,
                                                       DBCHANGE_ALLCHANGES,
                                                       THIS_THERMOSTAT);

    DisplayON();
}

/*******************************************************************************
    MNU_Home_Core

    Default menu
*******************************************************************************/
void MNU_Home_Core(void)
{
    TEMPERATURE_C_t temperature;
    Lock_t lock;
    RtcTime_t time;
    U8BIT alert;
    HEAT_DEMAND_PERCENT_t heatDemand;
    ControlMode_t floorMode;
    ScheduleModeEnum_t scheduleMode;

    //
    // Show heat demand
    //
    if (heat_display)
    {
        DBTHRD_GetData(DBTYPE_HEAT_DEMAND,(void*)&heatDemand, THIS_THERMOSTAT);
        DPH_DisplayHeatingStatus(heatDemand, FALSE);
        heat_display = FALSE;
    }

    //
    //	Show ambient temperature
    //
    if (temp_display)
    {
        DBTHRD_GetData(DBTYPE_FLOOR_MODE, &floorMode, THIS_THERMOSTAT);
        if ((floorMode == CONTROLMODE_AMBIENT) || (floorMode == CONTROLMODE_AMBIENTFLOOR))
        {
            DBTHRD_GetData(DBTYPE_AMBIENT_TEMPERATURE,(void*)&temperature, THIS_THERMOSTAT);
        }
        else if (floorMode == CONTROLMODE_FLOOR)
        {
            DBTHRD_GetData(DBTYPE_FLOOR_TEMPERATURE,(void*)&temperature, THIS_THERMOSTAT);
        }
        DPH_DisplayAmbientRoomTemperature(temperature, floorMode);
        temp_display = FALSE;
    }

    //
    //	Show setpoint
    //
    if (setpoint_display)
    {
        temperature = MNU_Setpoint_GetSetpoint();
        DBTHRD_GetData(DBTYPE_OFF_MODE_SETTING,(void*)&offMode, THIS_THERMOSTAT);
        DBTHRD_GetData(DBTYPE_SCHEDULE_MODE,(void*)&scheduleMode, THIS_THERMOSTAT);
        if (scheduleMode == MODE_DEROGATION)
        {
            DPH_DisplaySetpoint(temperature, FALSE, offMode, SET_TEMPORARY, TRUE);
        }
        else
        {
            if (floorMode == CONTROLMODE_FLOOR)
            {
                DPH_DisplaySetpoint(temperature, FALSE, offMode, SET_FLOOR, TRUE);
            }
            else
            {
                DPH_DisplaySetpoint(temperature, FALSE, offMode, SET_AMBIENT, TRUE);
            }
        }
        setpoint_display = FALSE;
    }

    //
    //	Show Lock Icon
    //
    DBTHRD_GetData(DBTYPE_LOCK_STATE,(void*)&lock, THIS_THERMOSTAT);
    if (lock_display)
    {
        DPH_DisplayLock(lock);
        lock_display = FALSE;
    }

    //
    //  Show alert icon
    //
    alert = MNU_Alerts_GetActiveAlertNumber();
    if (alert_display)
    {
        DPH_DisplayAlertIcon(alert);
        alert_display = FALSE;
    }

    //
    //  Show settings icon
    //
    if (button_display)
    {
        DPH_DisplaySettingsButton();
        button_display = FALSE;
    }

    //
    // Show time
    //
    DBTHRD_GetData(DBTYPE_LOCALTIME, (void*)&time, INDEX_DONT_CARE);
    if ((time_display)/* && (time.IsTimeSet)*/)
    {
        DPH_DisplayTime(&time);
        time_display = FALSE;
    }

    //
    // Show mode
    //
    if (mode_display)
    {
        DBTHRD_GetData(DBTYPE_SCHEDULE_MODE,(void*)&scheduleMode, INDEX_DONT_CARE);
        DPH_DisplayMode(scheduleMode);
        mode_display = FALSE;
    }

#ifdef LOAD_CYCLING
    uint8_t load_cycling_mode;
    DBTHRD_GetData(DBTYPE_LOAD_CYCLING_MODE,(void*)&load_cycling_mode, THIS_THERMOSTAT);
    DPH_DisplayLoadCyclingMode(load_cycling_mode);
#endif /* LOAD_CYCLING */

    if (HAL_AreAllKeysReleased() == TRUE)
    {
        //
        //  Key configuration
        //
        if (lock == LOCK_OFF)
        {
            if (IsKeyAssigned() == FALSE)
            {
                //
                // Configure key to enter Advanced settings
                //
                KBH_SetKeyPressProperties(KEY_SETTINGS, (FUNC)MNH_SetActiveMenu, (void*)MENU_ADV_SETTINGS);
#ifdef LOAD_CYCLING
//                KBH_SetKeyPressProperties(KEY_GROUPS, (FUNC)MNH_SetLoadCyclingMode, (void*)1);
                KBH_SetKeyPressProperties(KEY_MODE, (FUNC)MNH_SetLoadCyclingMode, (void*)2);
//                KBH_SetKeyPressProperties(KEY_SETPOINT, (FUNC)MNH_SetLoadCyclingMode, (void*)3);
#else
//                KBH_SetKeyPressProperties(KEY_GROUPS, (FUNC)MNH_SetActiveMenu, (void*)MENU_GROUPS);
                KBH_SetKeyPressProperties(KEY_MODE, (FUNC)MNH_SetActiveMenu, (void*)MENU_MODE_SELECTION);
//                KBH_SetKeyPressProperties(KEY_SETPOINT, (FUNC)MNH_SetActiveMenu, (void*)MENU_SETPOINT);
#endif /* LOAD_CYCLING */
                KBH_SetKeyPressProperties(KEY_OPTIONS_1, (FUNC)MNH_SetActiveMenu, (void*)MENU_DATE_TIME);
                if (minReached == 0)
                {
                    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_Setpoint_DecSetpointStartTurbo, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
                }
                else
                {
                    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_Setpoint_SetOffMode, (int16u*) 50, OS_MSTOTICK(KEYB_MENU_HOLD));
                }
                KBH_SetKeyHoldProperties(KEY_UP, (FUNC)MNU_Setpoint_IncSetpointStartTurbo, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
                KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_Setpoint_IncSetpoint, (int16u*)50);
                KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_Setpoint_DecSetpoint, (int16u*)50);

                // Stop key repeat monitoring
                KBH_SetKeyRepeatProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
                KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

                if (alert != 0)
                {
                    KBH_SetKeyPressProperties(KEY_ALERTS, (FUNC)MNH_SetActiveMenu, (void*)MENU_ALERTS);
                }
                else
                {
                    KBH_SetKeyPressProperties(KEY_ALERTS, (FUNC)NULL, (void*)NULL);
                }
            }
        }
        else
        {
            KBH_SetKeyHoldProperties(KEY_SETTINGS, (FUNC)MNH_SetActiveMenu, (void*) MENU_ADV_SETTINGS, OS_MSTOTICK(3000));
            KBH_SetKeyPressProperties(KEY_ALERTS, (FUNC)NULL, (void*)NULL);
            KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)NULL, (void*) NULL, OS_MSTOTICK(KEYB_MENU_HOLD));
            KBH_SetKeyHoldProperties(KEY_UP, (FUNC)NULL, (void*) NULL, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
            KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)NULL, (void*)NULL);
            KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL);
        }

        MNU_SetKeyAssigned(TRUE);
    }
}

/*******************************************************************************
    MNU_Home_Exit
*******************************************************************************/
void MNU_Home_Exit(void)
{
    KBH_ClearAllKeyProperties();

    DBTHRD_UnsubscribeToNotification(hTimeNotification);
    DBTHRD_UnsubscribeToNotification(hAmbientTemperatureNotification);
    DBTHRD_UnsubscribeToNotification(hFloorTemperatureNotification);
    DBTHRD_UnsubscribeToNotification(hTimeFormatNotification);
    DBTHRD_UnsubscribeToNotification(hAlertsNotification);
	DBTHRD_UnsubscribeToNotification(hTemperatureUnitNotification);
	DBTHRD_UnsubscribeToNotification(hLockStateNotification);
    DBTHRD_UnsubscribeToNotification(hHeatDemandNotification);
    DBTHRD_UnsubscribeToNotification(hUserActionNotification);
    DBTHRD_UnsubscribeToNotification(hOffModeNotification);
    DBTHRD_UnsubscribeToNotification(hAmbientSetpointNotification);
    DBTHRD_UnsubscribeToNotification(hFloorSetpointNotification);
    DBTHRD_UnsubscribeToNotification(hActiveProgramNotification);

    xTimerStop(BacklightTimeout, 0);
}

/*******************************************************************************
    MNU_NotifyMenuChange

    Update the main screen following an event

*******************************************************************************/
void MNU_NotifyMenuChange(void)
{
    OS_StartTask(TSK_APP_MNH_MENU);
}

/*******************************************************************************
* @brief  Function that sets which displayed items must be refreshed
* @inputs refreshBits: each item to be refreshed sets a specific bit
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/08/26
*******************************************************************************/
static void MNU_RefreshDisplayItems(int16u refreshBits)
{
    if (refreshBits & TIME_DISPLAY_BIT)
    {
        refreshBits &= ~TIME_DISPLAY_BIT;
        time_display = TRUE;
    }

    if (refreshBits & TEMP_DISPLAY_BIT)
    {
        refreshBits &= ~TEMP_DISPLAY_BIT;
        temp_display = TRUE;
    }

    if (refreshBits & LOCK_DISPLAY_BIT)
    {
        refreshBits &= ~LOCK_DISPLAY_BIT;
        lock_display = TRUE;
    }

    if (refreshBits & ALERT_DISPLAY_BIT)
    {
        refreshBits &= ~ALERT_DISPLAY_BIT;
        alert_display = TRUE;
    }

    if (refreshBits & HEAT_DISPLAY_BIT)
    {
        refreshBits &= ~HEAT_DISPLAY_BIT;
        heat_display = TRUE;
    }

    if (refreshBits & SETPOINT_DISPLAY_BIT)
    {
        refreshBits &= ~SETPOINT_DISPLAY_BIT;
        setpoint_display = TRUE;
    }

    if (refreshBits & BUTTON_DISPLAY_BIT)
    {
        refreshBits &= ~BUTTON_DISPLAY_BIT;
        button_display = TRUE;
    }

    if (refreshBits & MODE_DISPLAY_BIT)
    {
        refreshBits &= ~MODE_DISPLAY_BIT;
        mode_display = TRUE;
    }

    refreshBits = 0;
}


/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Simard
* @date   2016/10/12
*******************************************************************************/
DB_NOTIFICATION(MNU_OnTimeUpdate)
{
    MNU_RefreshDisplayItems(TIME_DISPLAY_BIT);
    MNU_NotifyMenuChange();
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Simard
* @date   2016/10/20
*******************************************************************************/
DB_NOTIFICATION(MNU_OnAmbientTemperatureUpdate)
{
	if ((status == DBERR_CHANGEABOVETHRESHOLD) || (status == DBERR_INVALIDVALUE))
	{
		MNU_RefreshDisplayItems(TEMP_DISPLAY_BIT);
        MNU_NotifyMenuChange();
	}
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Simard
* @date   2016/10/21
*******************************************************************************/
DB_NOTIFICATION(MNU_OnHomeStateUpdate)
{
    MNU_RefreshDisplayItems(MODE_DISPLAY_BIT);
    MNU_NotifyMenuChange();
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Simard
* @date   2016/10/24
*******************************************************************************/
DB_NOTIFICATION(MNU_OnAlertUpdate)
{
    MNU_RefreshDisplayItems(ALERT_DISPLAY_BIT);
    MNU_SetKeyAssigned(FALSE);
    MNU_NotifyMenuChange();
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @inputs
* @retval None
* @author Jean-Fran�ois Simard
* @date   2016/10/24
*******************************************************************************/
DB_NOTIFICATION(MNU_OnTemperatureUnitUpdate)
{
    MNU_OnAmbientSetpointUpdate( status, instance, source);
	MNU_OnAmbientTemperatureUpdate(status, instance, source);
}

/*******************************************************************************
* @brief  Callback invoked when the lock state is received from the cloud
* @inputs pSetpoint: pointer to the lock state
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
static DB_NOTIFICATION( ReceivedLockState_Callback)
{
    MNU_SetKeyAssigned(FALSE);
    OS_StartTask(TSK_LIMIT_CHECK);
    MNU_RefreshDisplayItems(LOCK_DISPLAY_BIT);
    MNU_NotifyMenuChange();
}

/*******************************************************************************
* @brief  Notification when the heat demand is updated
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/23
*******************************************************************************/
static DB_NOTIFICATION(MNU_OnHeatDemandUpdate)
{
    MNU_RefreshDisplayItems(HEAT_DISPLAY_BIT);
    MNU_NotifyMenuChange();
}

/*******************************************************************************
* @brief  Notification when the off mode is updated
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2019/05/16
*******************************************************************************/
static DB_NOTIFICATION(MNU_OnOffModeUpdate)
{
    MNU_RefreshDisplayItems(HEAT_DISPLAY_BIT | SETPOINT_DISPLAY_BIT);
    MNU_NotifyMenuChange();
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Simard
* @date   2016/10/21
*******************************************************************************/
DB_NOTIFICATION(MNU_OnAmbientSetpointUpdate)
{
    MNU_RefreshDisplayItems(SETPOINT_DISPLAY_BIT);
    MNU_NotifyMenuChange();
}

/*******************************************************************************
* @brief
* @inputs
*       dbErr_t status;
*       uint8_t instance
* @retval None
* @author Jean-Fran�ois Many
* @date   2019/07/31
*******************************************************************************/
DB_NOTIFICATION(MNU_OnActiveProgramUpdate)
{
    MNU_RefreshDisplayItems(MODE_DISPLAY_BIT);
    MNU_NotifyMenuChange();
}

/*******************************************************************************
    MNU_Setpoint_Confirm
*******************************************************************************/
void MNU_Setpoint_Confirm (void)
{
    SetpointEditState = EDIT_IDLE;
    DPH_DisplaySetpointApplied(SetpointEditState);
    MNU_RefreshDisplayItems(MODE_DISPLAY_BIT);
    MNU_NotifyMenuChange();
//    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Setpoint_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_Setpoint_GetSetpoint

    Get the current setpoint from the Setpoint Manager
*******************************************************************************/
TEMPERATURE_C_t MNU_Setpoint_GetSetpoint(void)
{
    TEMPERATURE_C_t setpoint;
    uint8_t floorMode;

    // If not currently in setpoint edition mode
    if ( SetpointEditState == EDIT_IDLE )
    {
        DBTHRD_GetData(DBTYPE_FLOOR_MODE,(void*)&floorMode, THIS_THERMOSTAT);
        if (floorMode == CONTROLMODE_FLOOR)
        {
            // Return control ambient setpoint
            DBTHRD_GetData(DBTYPE_FLOOR_SETPOINT,(void*)&setpoint, THIS_THERMOSTAT);
        }
        else
        {
            // Return control floor setpoint
            DBTHRD_GetData(DBTYPE_AMBIENT_SETPOINT,(void*)&setpoint, THIS_THERMOSTAT);
        }
    }
    // Else currently in setpoint edition mode
    else
    {
        // Return current edited setpoint value
        setpoint = setpointEditValue;
    }
    return ( setpoint );
}

/*******************************************************************************
    MNU_Setpoint_StopSetpointEdition
*******************************************************************************/
void MNU_Setpoint_StopSetpointEdition(void)
{
    SetpointEditState = EDIT_CONFIRM;
    DPH_DisplaySetpointApplied(SetpointEditState);
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Setpoint_Confirm,  (KEYB_PARAM*)NULL, CONFIRM_TIMEOUT);
}

/*******************************************************************************
    MNU_Setpoint_IncSetpoint

    Increment a setpoint
*******************************************************************************/
void MNU_Setpoint_IncSetpoint(int16u value)
{
#ifndef PCT_HEAT
	UnitFormat_t format;
#endif
	TEMPERATURE_C_t maxsetpoint;

#ifdef PCT_HEAT
    maxsetpoint = 100;
    setpointEditValue = MNU_Setpoint_GetHeatPctSetpoint();
#else
    maxsetpoint= STPM_GetSetpointMax();
    setpointEditValue = MNU_Setpoint_GetSetpoint();
#endif
    uint8_t floorMode;

    DBTHRD_GetData(DBTYPE_FLOOR_MODE, &floorMode, THIS_THERMOSTAT);
    DBTHRD_GetData(DBTYPE_OFF_MODE_SETTING,(void*)&offMode, THIS_THERMOSTAT);

    SetpointEditState = EDIT_IN_PROGRESS;

    if ((setpointEditValue < maxsetpoint) && (setpointEditValue != DB_SENSOR_INVALID) && (offMode == 0))
    {
#ifdef PCT_HEAT
        setpointEditValue += 50;
        DPH_HandleHeatPctSetpointDisplay(setpointEditValue);
#else
        DBTHRD_GetData(DBTYPE_TEMPERATURE_FORMAT,(void*)&format, INDEX_DONT_CARE);
        if(format == DEGREE_F)
        {
            setpointEditValue = CVT_ConvertCtoF(setpointEditValue, 100);
            setpointEditValue += (value * 2);
            setpointEditValue = CVT_ConvertFtoC(setpointEditValue, 0);
        }
        else
        {
            setpointEditValue = TLS_Round(setpointEditValue, 50);
            setpointEditValue += value;
            setpointEditValue = TLS_Round(setpointEditValue, 50);
        }
        if (setpointEditValue > maxsetpoint)
        {
            setpointEditValue = maxsetpoint;
        }

        MNU_RefreshDisplayItems(MODE_DISPLAY_BIT);
        MNU_NotifyMenuChange();
        if (floorMode == CONTROLMODE_FLOOR)
        {
            DPH_DisplaySetpoint(setpointEditValue, FALSE, offMode, SET_FLOOR, TRUE);
        }
        else
        {
            DPH_DisplaySetpoint(setpointEditValue, FALSE, offMode, SET_AMBIENT, TRUE);
        }
#endif
    }

#ifdef FLOOR
    if (offMode != 0)
    {
        offMode = 0;
        DBTHRD_SetData(DBTYPE_OFF_MODE_SETTING,(void*)&offMode, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    }
    minReached = 0;
    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_Setpoint_DecSetpointStartTurbo, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
#endif

#ifdef ON_OFF
    ForceOnState = 1;
#endif /* ON_OFF */

    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Setpoint_CommitSetpoint, (U8BIT*)NULL, SETPOINT_EDIT_TIMEOUT);
}

/*******************************************************************************
    MNU_Setpoint_DecSetpoint

    Decrement a setpoint
*******************************************************************************/
void MNU_Setpoint_DecSetpoint(int16u value)
{
#ifndef PCT_HEAT
	UnitFormat_t format;
#endif
	TEMPERATURE_C_t minsetpoint;

#ifdef PCT_HEAT
    minsetpoint = 0;
    setpointEditValue = MNU_Setpoint_GetHeatPctSetpoint();
#else
    minsetpoint = STPM_GetSetpointMin();
    setpointEditValue = MNU_Setpoint_GetSetpoint();
#endif
    uint8_t floorMode;

    DBTHRD_GetData(DBTYPE_FLOOR_MODE, &floorMode, THIS_THERMOSTAT);
    DBTHRD_GetData(DBTYPE_OFF_MODE_SETTING,(void*)&offMode, THIS_THERMOSTAT);

    SetpointEditState = EDIT_IN_PROGRESS;

    if ((setpointEditValue > minsetpoint) && (setpointEditValue != DB_SENSOR_INVALID))
    {
#ifdef PCT_HEAT
        setpointEditValue -= 50;
        DPH_HandleHeatPctSetpointDisplay(setpointEditValue);
#else
        DBTHRD_GetData(DBTYPE_TEMPERATURE_FORMAT,(void*)&format, INDEX_DONT_CARE);
        if(format == DEGREE_F)
        {
            setpointEditValue = CVT_ConvertCtoF(setpointEditValue, 100);
            setpointEditValue -= (value * 2);
            setpointEditValue = CVT_ConvertFtoC(setpointEditValue, 0);
        }
        else
        {
            setpointEditValue = TLS_Round(setpointEditValue, 50);
            setpointEditValue -= value;
            setpointEditValue = TLS_Round(setpointEditValue, 50);
        }
        if (setpointEditValue <= minsetpoint)
        {
            setpointEditValue = minsetpoint;
        }

        MNU_RefreshDisplayItems(MODE_DISPLAY_BIT);
        MNU_NotifyMenuChange();
        if (floorMode == CONTROLMODE_FLOOR)
        {
            DPH_DisplaySetpoint(setpointEditValue, FALSE, offMode, SET_FLOOR, TRUE);
        }
        else
        {
            DPH_DisplaySetpoint(setpointEditValue, FALSE, offMode, SET_AMBIENT, TRUE);
        }
#endif
    }
#ifdef FLOOR
    if (setpointEditValue == minsetpoint)
    {
        if (minReached == 0)
        {
            minReached = 1;
            // Monitor for hold key to enter off mode
            KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)MNU_Setpoint_SetOffMode, (void*) NULL, OS_MSTOTICK(KEYB_MENU_HOLD));
        }
    }
#endif

#ifdef ON_OFF
    ForceOnState = 0;
#endif /* ON_OFF */

    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Setpoint_CommitSetpoint, (U8BIT*)NULL, SETPOINT_EDIT_TIMEOUT);
}

/*******************************************************************************
    MNU_Setpoint_SetOffMode

    Enter in Off Mode
*******************************************************************************/
static void MNU_Setpoint_SetOffMode(void)
{
    uint8_t offMode = 1;
    DBTHRD_SetData(DBTYPE_OFF_MODE_SETTING,(void*)&offMode, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);
}

/*******************************************************************************
    MNU_Setpoint_DecSetpointStartTurbo

    Decrement a setpoint
*******************************************************************************/
void MNU_Setpoint_DecSetpointStartTurbo(void)
{
    KBH_SetKeyHoldProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

    // Decrement setpoint
    MNU_Setpoint_DecSetpoint(50);
#ifdef FLOOR
    if (minReached == 0)
    {
        // Turn on key repeat notification
        KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)MNU_Setpoint_DecSetpoint, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_DELAY));
    }
#else
    // Turn on key repeat notification
    KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)MNU_Setpoint_DecSetpoint, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_DELAY));
#endif

    KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_Setpoint_SetpointStopTurbo, (void*)NULL);
    KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_Setpoint_SetpointStopTurbo, (void*)NULL);
}

/*******************************************************************************
    MNU_Setpoint_IncSetpointStartTurbo

    Increment a setpoint
*******************************************************************************/
void MNU_Setpoint_IncSetpointStartTurbo(void)
{
    KBH_SetKeyHoldProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

    // Increment setpoint
    MNU_Setpoint_IncSetpoint(50);
    // Turn on key repeat notification
    KBH_SetKeyRepeatProperties(KEY_UP, (FUNC)MNU_Setpoint_IncSetpoint, (int16u*) 50, OS_MSTOTICK(KEYB_REPEAT_DELAY));

    KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_Setpoint_SetpointStopTurbo, (void*)NULL);
    KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_Setpoint_SetpointStopTurbo, (void*)NULL);
}

/*******************************************************************************
    MNU_Setpoint_SetpointStopTurbo
*******************************************************************************/
void MNU_Setpoint_SetpointStopTurbo(void)
{
    // Stop key repeat monitoring
	KBH_SetKeyRepeatProperties(KEY_UP, (FUNC)NULL, (void*)NULL, 0);
    KBH_SetKeyRepeatProperties(KEY_DOWN, (FUNC)NULL, (void*)NULL, 0);

    // Start key hold monitoring
	KBH_SetKeyHoldProperties(KEY_UP, (FUNC)MNU_Setpoint_IncSetpointStartTurbo, (void*) NULL, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
#ifdef FLOOR
    if (minReached == 0)
    {
        KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_Setpoint_DecSetpointStartTurbo, (void*) NULL, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
    }
    else
    {
        KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_Setpoint_SetOffMode, (void*) NULL, OS_MSTOTICK(KEYB_MENU_HOLD));
    }
#else
    KBH_SetKeyHoldProperties(KEY_DOWN, (FUNC)MNU_Setpoint_DecSetpointStartTurbo, (void*) NULL, OS_MSTOTICK(KEYB_REPEAT_INITIAL));
#endif
	KBH_SetKeyReleaseProperties(KEY_UP, (FUNC)MNU_Setpoint_IncSetpoint, (int16u*) 50);
    KBH_SetKeyReleaseProperties(KEY_DOWN, (FUNC)MNU_Setpoint_DecSetpoint, (int16u*) 50);
}

/*******************************************************************************
    MNU_Setpoint_CommitSetpoint

    Update the Setpoint Manager with a new setpoint
*******************************************************************************/
void MNU_Setpoint_CommitSetpoint( void )
{
#ifdef PCT_HEAT
    HEAT_DEMAND_PERCENT_t     currentSetpoint;

    MED_GetControlHeatPercent(&currentSetpoint);
#else
    TEMPERATURE_C_t     currentSetpoint;
    uint8_t floorMode;
    ScheduleModeEnum_t scheduleMode;

    DBTHRD_GetData(DBTYPE_FLOOR_MODE,(void*)&floorMode, THIS_THERMOSTAT);
    if (floorMode == CONTROLMODE_FLOOR)
    {
        DBTHRD_GetData(DBTYPE_FLOOR_SETPOINT,(void*)&currentSetpoint, THIS_THERMOSTAT);
    }
    else
    {
        DBTHRD_GetData(DBTYPE_AMBIENT_SETPOINT,(void*)&currentSetpoint, THIS_THERMOSTAT);
    }
#endif

    if ( SetpointEditState == EDIT_IN_PROGRESS )
    {
        // If user changed setpoint
        if (( setpointEditValue != currentSetpoint ) && (setpointEditValue != DB_SENSOR_INVALID))
        {
            // Apply user setpoint
#ifdef PCT_HEAT
            MED_SetControlHeatPercent(holdEditValue);
#else
            //Set in derogation if we were in schedule mode
            DBTHRD_GetData(DBTYPE_SCHEDULE_MODE,(void*)&scheduleMode, THIS_THERMOSTAT);
            if (scheduleMode == MODE_PROG)
            {
                scheduleMode = MODE_DEROGATION;
                DBTHRD_SetData(DBTYPE_SCHEDULE_MODE, (void*)&scheduleMode, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            }

            if (floorMode == CONTROLMODE_FLOOR)
            {
                DBTHRD_SetData(DBTYPE_FLOOR_SETPOINT,(void*)&setpointEditValue, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            }
            else
            {
                DBTHRD_SetData(DBTYPE_AMBIENT_SETPOINT,(void*)&setpointEditValue, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            }
#endif
//            MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Setpoint_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
        }
    }
    MNU_Setpoint_StopSetpointEdition();
}

#ifdef LOAD_CYCLING
#pragma optimize = none
static void MNH_SetLoadCyclingMode(uint8_t mode)
{
    DBTHRD_SetData(DBTYPE_LOAD_CYCLING_MODE, (void*)&mode, THIS_THERMOSTAT, DBCHANGE_LOCAL);
}
#endif /* LOAD_CYCLING */