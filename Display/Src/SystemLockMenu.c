/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       SystemLockMenu.c

    \brief      Source code file for the SystemLockMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the SystemLockMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "SystemLockMenu.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   SystemLockEditState = EDIT_IDLE;
static Lock_t	        SystemLockEditValue;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void ChangeSystemLock(KEYB_PARAM keyId);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    ChangeSystemLock

    Change the lock state for all thermostats in the network
*******************************************************************************/
static void ChangeSystemLock(KEYB_PARAM keyId)
{
    switch (keyId)
    {
        case KEY_OPTIONS_2:
            SystemLockEditValue = LOCK_OFF;
            DBTHRD_SetData(DBTYPE_LOCK_STATE,(void*)&SystemLockEditValue, 0, DBCHANGE_LOCAL);
            break;

        case KEY_OPTIONS_3:
            SystemLockEditValue = LOCK_ON;
            DBTHRD_SetData(DBTYPE_LOCK_STATE,(void*)&SystemLockEditValue, 0, DBCHANGE_LOCAL);
            break;

        default:
            break;
    }

    DPH_DisplaySystemLock(SystemLockEditValue);
    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_SystemLock_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_SystemLock_Entry
*******************************************************************************/
void MNU_SystemLock_Entry(void)
{
    SystemLockEditValue = LOCK_UNKNOWN;
    SystemLockEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);

    MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_SystemLock_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_SystemLock_Core

    Allow locking or unlocking all thermostat in the network
*******************************************************************************/
void MNU_SystemLock_Core(void)
{
    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show system lock option
    //
    DPH_DisplaySystemLock(SystemLockEditValue);

    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_SystemLock_Commit, (void*)KEY_BACK);
        KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)ChangeSystemLock, (void*)KEY_OPTIONS_2);
        KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)ChangeSystemLock, (void*)KEY_OPTIONS_3);
        KBH_SetKeyPressProperties(KEY_DONE, (FUNC)MNU_SystemLock_Commit, (void*)KEY_DONE);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_SystemLock_Exit
*******************************************************************************/
void MNU_SystemLock_Exit(void)
{
    SystemLockEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_SystemLock_Commit
*******************************************************************************/
void MNU_SystemLock_Commit(KEYB_PARAM keyId)
{
    static KEYB_PARAM commitKey = NULL;

    if ( SystemLockEditState == EDIT_IN_PROGRESS )
    {
        commitKey = keyId;
        KBH_ClearAllKeyProperties();
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_SystemLock_Commit, (KEYB_PARAM*)commitKey, IMMEDIATELY);
        DPH_DisplaySystemLock(SystemLockEditValue);
        SystemLockEditState = EDIT_COMMITTING;
    }
    else if ( SystemLockEditState == EDIT_COMMITTING )
    {
        SystemLockEditState = EDIT_IDLE;
        switch (commitKey)
        {
            case KEY_BACK:
            case KEY_DONE:
                MNH_SetActiveMenu(MENU_OPTIONS);
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
    }
}

