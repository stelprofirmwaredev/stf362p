/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2017, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       TestModeMenu.c

    \brief      Source code file for the TestModeMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the TestModeMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "TestModeMenu.h"
#include "WiFiScanMenu.h"
#include ".\HAL\inc\ProximitySensor.h"
#include "StringObjects.h"
#include "images.h"
#include ".\HAL\inc\Display.h"
#include ".\WiFi\inc\THREAD_WiFi.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/


/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;


/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e       TestModeEditState = EDIT_IDLE;
static uint8_t              TestEditValue;
static MnuEditState_e       BlackEditState = EDIT_IDLE;
static MnuEditState_e       WhiteEditState = EDIT_IDLE;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void TouchTest(KEYB_PARAM keyId);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    TouchTest

    Perform the touch test
*******************************************************************************/
static void TouchTest(KEYB_PARAM keyId)
{
    switch (keyId)
    {
        case KEY_OPTIONS_1:
            TestEditValue |= 0x01;
            break;

        case KEY_OPTIONS_2:
            TestEditValue |= 0x02;
            break;

        case KEY_OPTIONS_3:
            TestEditValue |= 0x04;
            break;

        case KEY_OPTIONS_4:
            TestEditValue |= 0x08;
            break;

        case KEY_OPTIONS_5:
            TestEditValue |= 0x10;
            break;

        default:
            TestEditValue = 0;
            break;
    }

    DPH_HandleTestDisplay(TestEditValue);
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_TestMode_Entry
*******************************************************************************/
void MNU_TestMode_Entry(void)
{
    TestEditValue = 0;
    TestModeEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);
}

/*******************************************************************************
    MNU_TestMode_Core

    Test the LCD and touch panel alignment
*******************************************************************************/
void MNU_TestMode_Core(void)
{
    DPH_HandleTestDisplay(TestEditValue);
    if ((TestModeEditState == EDIT_IN_PROGRESS) && (HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_OPTIONS_1, (FUNC)TouchTest, (void*)KEY_OPTIONS_1);
        KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)TouchTest, (void*)KEY_OPTIONS_2);
        KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)TouchTest, (void*)KEY_OPTIONS_3);
        KBH_SetKeyPressProperties(KEY_OPTIONS_4, (FUNC)TouchTest, (void*)KEY_OPTIONS_4);
        KBH_SetKeyPressProperties(KEY_OPTIONS_5, (FUNC)TouchTest, (void*)KEY_OPTIONS_5);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_TestMode_Exit
*******************************************************************************/
void MNU_TestMode_Exit(void)
{
    TestModeEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_Black_Entry
*******************************************************************************/
void MNU_Black_Entry(void)
{
    BlackEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);
}

/*******************************************************************************
    MNU_Black_Core

    Display a black screen
*******************************************************************************/
void MNU_Black_Core(void)
{
    if (BlackEditState == EDIT_IN_PROGRESS)
    {
        DPH_HandleBlackDisplay();
    }
}

/*******************************************************************************
    MNU_Black_Exit
*******************************************************************************/
void MNU_Black_Exit(void)
{
    BlackEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_White_Entry
*******************************************************************************/
void MNU_White_Entry(void)
{
    WhiteEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);
}

/*******************************************************************************
    MNU_White_Core

    Display a white screen
*******************************************************************************/
void MNU_White_Core(void)
{
    if (WhiteEditState == EDIT_IN_PROGRESS)
    {
        DPH_HandleWhiteDisplay();
    }
}

/*******************************************************************************
    MNU_White_Exit
*******************************************************************************/
void MNU_White_Exit(void)
{
    WhiteEditState = EDIT_IDLE;
}
