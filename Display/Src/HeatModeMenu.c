/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       HeatModeMenu.c

    \brief      Source code file for the HeatModeMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the HeatModeMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "HeatModeMenu.h"
#include ".\DB\inc\THREAD_DB.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/

    //Empty

/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   HeatModeEditState = EDIT_IDLE;
static HeatMode_t       HeatModeEditValue;
static LobbyMode_t      LobbyModeEditValue;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/
static void ChangeHeatMode(KEYB_PARAM keyId);

/*******************************************************************************
    Private functions definitions
*******************************************************************************/
/*******************************************************************************
    ChangeHeatMode

    Change the Heat Mode between Baseboard and Fan-Forced Heater
*******************************************************************************/
static void ChangeHeatMode(KEYB_PARAM keyId)
{
    uint8_t onboardingDone;

    switch (keyId)
    {
        case KEY_OPTIONS_2:
            HeatModeEditValue = HEATMODE_BASEBOARD;
            LobbyModeEditValue = ROOM_STANDARD;
            break;

        case KEY_OPTIONS_3:
            HeatModeEditValue = HEATMODE_FAN;
            break;

        default:
            break;
    }

    DPH_DisplayHeatMode(HeatModeEditValue, LobbyModeEditValue, FALSE);

    DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);
    if (onboardingDone == 1)
    {
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_HeatMode_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    }
    MNU_SetKeyAssigned(FALSE);
}

/*******************************************************************************
    ChangeLobbyMode

    Change the Lobby Mode between Standard room and Lobby
*******************************************************************************/
static void ChangeLobbyMode(KEYB_PARAM keyId)
{
    uint8_t onboardingDone;

    switch (keyId)
    {
        case KEY_OPTIONS_4:
            LobbyModeEditValue = ROOM_STANDARD;
            break;

        case KEY_OPTIONS_5:
            LobbyModeEditValue = ROOM_LOBBY;
            break;

        default:
            break;
    }

    DPH_DisplayHeatMode(HeatModeEditValue, LobbyModeEditValue, FALSE);

    DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);
    if (onboardingDone == 1)
    {
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_HeatMode_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    }
}

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_HeatMode_Entry
*******************************************************************************/
void MNU_HeatMode_Entry(void)
{
    uint8_t onboardingDone;

    DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);
    DBTHRD_GetData(DBTYPE_HEAT_MODE_SETTING,(void*)&HeatModeEditValue, INDEX_DONT_CARE);
    DBTHRD_GetData(DBTYPE_LOBBY_MODE_SETTING,(void*)&LobbyModeEditValue, INDEX_DONT_CARE);
    HeatModeEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show Heat modes list
    //
    DPH_DisplayHeatMode(HeatModeEditValue, LobbyModeEditValue, TRUE);

    if (onboardingDone == 1)
    {
        MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_HeatMode_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
    }
}

/*******************************************************************************
    MNU_HeatMode_Core

    Allow changing the heat mode between Baseboard and Fan
*******************************************************************************/
void MNU_HeatMode_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_HeatMode_Commit, (void*)KEY_BACK);
        KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)ChangeHeatMode, (void*)KEY_OPTIONS_2);
        KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)ChangeHeatMode, (void*)KEY_OPTIONS_3);
        if (HeatModeEditValue == HEATMODE_FAN)
        {
            KBH_SetKeyPressProperties(KEY_OPTIONS_4, (FUNC)ChangeLobbyMode, (void*)KEY_OPTIONS_4);
            KBH_SetKeyPressProperties(KEY_OPTIONS_5, (FUNC)ChangeLobbyMode, (void*)KEY_OPTIONS_5);
        }
        KBH_SetKeyPressProperties(KEY_DONE, (FUNC)MNU_HeatMode_Commit, (void*)KEY_DONE);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_HeatMode_Exit
*******************************************************************************/
void MNU_HeatMode_Exit(void)
{
    HeatModeEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_HeatMode_Init
*******************************************************************************/
void MNU_HeatMode_Init(void)
{
    MNU_HeatMode_SetHeatMode(HEATMODE_BASEBOARD);
    MNU_HeatMode_SetLobbyMode(ROOM_STANDARD);
}

/*******************************************************************************
    MNU_HeatMode_Commit
*******************************************************************************/
void MNU_HeatMode_Commit(KEYB_PARAM keyId)
{
    static KEYB_PARAM commitKey = NULL;
    uint8_t onboardingDone;

    if ( HeatModeEditState == EDIT_IN_PROGRESS )
    {
        commitKey = keyId;
        KBH_ClearAllKeyProperties();
        switch (commitKey)
        {
            case KEY_DONE:
                MNU_HeatMode_SetHeatMode(HeatModeEditValue);
                MNU_HeatMode_SetLobbyMode(LobbyModeEditValue);
                //No break on purpose

            case KEY_BACK:
            default:
                MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_HeatMode_Commit, (KEYB_PARAM*)commitKey, IMMEDIATELY);
                break;
        }
        HeatModeEditState = EDIT_COMMITTING;
    }
    else if ( HeatModeEditState == EDIT_COMMITTING )
    {
        HeatModeEditState = EDIT_IDLE;
        DBTHRD_GetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE);

        switch (commitKey)
        {
            case KEY_BACK:
                if (onboardingDone == 1)
                {
                    MNH_SetActiveMenu(MENU_OPTIONS);
                }
                else
                {
                    MNH_SetActiveMenu(MENU_TIME_FORMAT);
                }
                break;

            case KEY_DONE:
                if (onboardingDone == 1)
                {
                    MNH_SetActiveMenu(MENU_OPTIONS);
                }
                else
                {
                    onboardingDone = 1;
                    DBTHRD_SetData(DBTYPE_ONBOARDING_DONE,(void*)&onboardingDone, INDEX_DONT_CARE, DBCHANGE_LOCAL);
                    MNH_SetActiveMenu(MENU_HOME);
                }
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
    }
}

/*******************************************************************************
    MNU_HeatMode_SetHeatMode

    Set the current heat mode
*******************************************************************************/
void MNU_HeatMode_SetHeatMode(HeatMode_t mode)
{
    DBTHRD_SetData(DBTYPE_HEAT_MODE_SETTING,(void*)&mode, INDEX_DONT_CARE, DBCHANGE_LOCAL);
}

/*******************************************************************************
    MNU_HeatMode_SetLobbyMode

    Set the current lobby mode
*******************************************************************************/
void MNU_HeatMode_SetLobbyMode(LobbyMode_t mode)
{
    DBTHRD_SetData(DBTYPE_LOBBY_MODE_SETTING,(void*)&mode, THIS_THERMOSTAT, DBCHANGE_LOCAL);
}
