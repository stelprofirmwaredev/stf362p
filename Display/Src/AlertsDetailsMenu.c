/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       AlertsDetailsMenu.c

    \brief      Source code file for the AlertsDetailsMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the AlertsDetailsMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "AlertsDetailsMenu.h"
#include "AlertsMenu.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\HAL\inc\HAL_CloudInterface.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/


/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;


/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

extern TimerHandle_t OTA_Reset_Timer;

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   AlertsDetailsEditState = EDIT_IDLE;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/
    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/

/*******************************************************************************
    Private functions definitions
*******************************************************************************/

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_AlertsDetails_Entry
*******************************************************************************/
void MNU_AlertsDetails_Entry(void)
{
    uint8_t tstat;
    uint8_t id;

    AlertsDetailsEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show alerts details
    //
    MNU_Alerts_GetSelectedAlert(&tstat, &id);
    DPH_DisplayAlertsDetails(tstat, id);

	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_AlertsDetails_Commit,  (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_AlertsDetails_Core

    Allows to view the active alerts details
*******************************************************************************/
void MNU_AlertsDetails_Core(void)
{
    uint8_t tstat;
    uint8_t id;

    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure keys
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_AlertsDetails_Commit, (void*)KEY_BACK);

        MNU_Alerts_GetSelectedAlert(&tstat, &id);
        if (id == DBALERT_TIME_NOT_SET)
        {
            KBH_SetKeyPressProperties(KEY_DONE, (FUNC)MNU_AlertsDetails_Commit, (void*)KEY_DONE);
        }
        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_AlertsDetails_Exit
*******************************************************************************/
void MNU_AlertsDetails_Exit(void)
{
    AlertsDetailsEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_AlertsDetails_Commit
*******************************************************************************/
void MNU_AlertsDetails_Commit(KEYB_PARAM keyId)
{
	if (AlertsDetailsEditState == EDIT_IN_PROGRESS)
	{
        KBH_ClearAllKeyProperties();
        switch(keyId)
        {
            case KEY_BACK:
                MNH_SetActiveMenu(MENU_ALERTS);
                break;

            case KEY_DONE:
                MNH_SetActiveMenu(MENU_DATE_TIME);
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
		AlertsDetailsEditState = EDIT_IDLE;
	}
}
