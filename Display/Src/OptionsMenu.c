/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       OptionsMenu.c

    \brief      Source code file for the OptionsMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the code of the OptionsMenu core implementation.

*******************************************************************************/

/*******************************************************************************
    Includes
*******************************************************************************/
#include "typedef.h"
#include "KeyboardConfig.h"
#include "APP_Display.h"
#include "MenuHandlerIf.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include "KeyboardHandler.h"
#include "OptionsMenu.h"

/*******************************************************************************
    Private constants definitions
*******************************************************************************/


/*******************************************************************************
    Macros
*******************************************************************************/

    //Empty

/*******************************************************************************
    Private data types
*******************************************************************************/

typedef enum
{
    EDIT_IDLE = 0,
    EDIT_IN_PROGRESS,
    EDIT_COMMITTING
} MnuEditState_e;

/*******************************************************************************
    Public global variables definitions
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Private global variables declarations
*******************************************************************************/
static MnuEditState_e   OptionsEditState = EDIT_IDLE;

/*******************************************************************************
    External functions prototypes
*******************************************************************************/
    //Empty

/*******************************************************************************
    Private functions prototypes
*******************************************************************************/

/*******************************************************************************
    Private functions definitions
*******************************************************************************/

/*******************************************************************************
    Public functions definitions
*******************************************************************************/

/*******************************************************************************
    MNU_Options_Entry
*******************************************************************************/
void MNU_Options_Entry(void)
{
    OptionsEditState = EDIT_IN_PROGRESS;

    MNU_SetKeyAssigned(FALSE);

    //
    //  Show Back arrow
    //
    DPH_DisplayBack();

    //
    //  Show the option list
    //
    DPH_DisplayOptions();

	MNH_SetTimeoutCallback((MnhTimeoutCallback_t)MNU_Options_Commit, (KEYB_PARAM*)KEY_HOME, MENU_TIMEOUT);
}

/*******************************************************************************
    MNU_Options_Core

    Present the options to the user
*******************************************************************************/
void MNU_Options_Core(void)
{
    if ((HAL_AreAllKeysReleased() == TRUE) && (IsKeyAssigned() == FALSE))
    {
        //
        // Configure key in the Options menu
        //
        KBH_SetKeyPressProperties(KEY_BACK, (FUNC)MNU_Options_Commit, (void*)KEY_BACK);
        KBH_SetKeyPressProperties(KEY_OPTIONS_1, (FUNC)MNU_Options_Commit, (void*)KEY_OPTIONS_1);
        KBH_SetKeyPressProperties(KEY_OPTIONS_2, (FUNC)MNU_Options_Commit, (void*)KEY_OPTIONS_2);
        KBH_SetKeyPressProperties(KEY_OPTIONS_3, (FUNC)MNU_Options_Commit, (void*)KEY_OPTIONS_3);
        KBH_SetKeyPressProperties(KEY_OPTIONS_4, (FUNC)MNU_Options_Commit, (void*)KEY_OPTIONS_4);
        KBH_SetKeyPressProperties(KEY_OPTIONS_5, (FUNC)MNU_Options_Commit, (void*)KEY_OPTIONS_5);
        KBH_SetKeyPressProperties(KEY_OPTIONS_6, (FUNC)MNU_Options_Commit, (void*)KEY_OPTIONS_6);

        MNU_SetKeyAssigned(TRUE);
    }
    else
    {
        if (IsKeyAssigned() == FALSE)
        {
            OS_DelayTask(TSK_APP_MNH_MENU, OS_MSTOTICK(50));
        }
    }
}

/*******************************************************************************
    MNU_Options_Exit
*******************************************************************************/
void MNU_Options_Exit(void)
{
    OptionsEditState = EDIT_IDLE;
}

/*******************************************************************************
    MNU_Options_Commit
*******************************************************************************/
void MNU_Options_Commit(KEYB_PARAM keyId)
{
	if (OptionsEditState == EDIT_IN_PROGRESS)
	{
        KBH_ClearAllKeyProperties();
        switch(keyId)
        {
            case KEY_BACK:
                MNH_SetActiveMenu(MENU_ADV_SETTINGS);
                break;

            case KEY_OPTIONS_1:
                MNH_SetActiveMenu(MENU_LANGUAGE);
                break;

            case KEY_OPTIONS_2:
                MNH_SetActiveMenu(MENU_UNIT_FORMAT);
                break;

            case KEY_OPTIONS_3:
                MNH_SetActiveMenu(MENU_TIME_FORMAT);
                break;

            case KEY_OPTIONS_4:
                MNH_SetActiveMenu(MENU_CONTROL_MODE);
                break;

            case KEY_OPTIONS_5:
                MNH_SetActiveMenu(MENU_LOCK);
                break;

            case KEY_OPTIONS_6:
                MNH_SetActiveMenu(MENU_BACKLIGHT);
                break;

            default:
                MNH_SetActiveMenu(MENU_HOME);
                break;
        }
		OptionsEditState = EDIT_IDLE;
	}
}
