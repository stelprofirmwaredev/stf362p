/********************************************************************************************
*    convert.h
*
*                            Module Convertion Donn�es (AC629)
*
*    Regroupe les fonctions de conversion de temp�rature ainsi que les fonctions
*    d'arrondissement.
*
*    \author     Jean-Fran�ois Many
*
********************************************************************************************/

/********************************************************************************************
*    Constantes Publiques
********************************************************************************************/

/*** Compilation Conditionnelle *************************************************************
*
*     Les constantes de compilation conditionnelle devrait �tre d�clar�es dans les param�tre
*    du projets.
*/

/*** Constantes de configuration ***********************************************************/

/*** Configuration des I/Os ****************************************************************/

/*** Statuts d'erreur **********************************************************************/


/********************************************************************************************
*    Structures de data
********************************************************************************************/


/********************************************************************************************
*    Variables Publiques
********************************************************************************************/
/*** Variables globales sans initialisation ************************************************/

/*** Variables globale initialis�es lors d�claration ***************************************/

/*** Variables globale initialis�es � z�ro *************************************************/


/********************************************************************************************
*    Fonctions Publiques
********************************************************************************************/

/********************************************************************************************
*  Conversion Bianire � BCD
*
*    Convertit un nombre binaire 16 bits en BCD.
*
*    Param�tres :
*        data_in : temp�rature � convertir en BCD. (0 @ 9999)
*
*    Retourne :
*        4 chiffres moins significatifs du nombre convertit en BCD (0 @ 0x9999)
*
********************************************************************************************/
int16u CVT_Bin2Bcd16(int16u data_in);


/********************************************************************************************
*  Convertion Temp�rature � BCD
*
*    Convertit une temp�rature en BCD avec gestion des codes d'erreur.
*
*    Param�tres :
*        temp_to_convert : temp�rature � convertir en BCD.
*
*    Retourne :
*        Temp�rature convertie en BCD avec codes de cont�le (n�gatif; unit�s)
*            Codes de cont�les:    bit 15 : Valeur n�gative.
*                                bit 14 : Affichage code (Er Lo Hi --).
*
********************************************************************************************/
int16u CVT_ConvertTempBCD(int16s temp_to_convert);


/********************************************************************************************
*  Conversion �F
*
*    Convertit une temp�rature de �C -> �F.
*
*    Param�tres :
*        temp_to_convert : temp�rature en �C � convertir.
*        round : Valeur de l'arrondissement � effectuer. (0 = pas d'arrondis)
*
*    Retourne :
*        Temp�rature convertie en �F.
*
********************************************************************************************/
int16s CVT_ConvertCtoF(int16s temp_to_convert, int16u round);


/********************************************************************************************
*  Conversion �C
*
*    Convertit une temp�rature de �F -> �C.
*
*    Param�tres :
*        temp_to_convert : temp�rature en �F � convertir.
*        round : Valeur de l'arrondissement � effectuer. (0 = pas d'arrondis)
*
*    Retourne :
*        Temp�rature convertie en �C.
*
********************************************************************************************/
int16s CVT_ConvertFtoC(int16s temp_to_convert, int16u round);
