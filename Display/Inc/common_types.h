/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/


// ****************************************************************************
// FILENAME:      common_types.h
//
// MODULE:        Include files for shared application level types.
//
// DESCRIPTION:   This file contains definitions that are shared among
//                various application level components.
//
// ****************************************************************************

#ifndef __COMMON_TYPES_H__
#define __COMMON_TYPES_H__
#include "typedef.h"

/*-------------------------------------------------------------------------------
    INCLUDE
-------------------------------------------------------------------------------*/
// none


/*-------------------------------------------------------------------------------
    INTERFACES
-------------------------------------------------------------------------------*/
//Macros for easy conversion from decimal to internal data
//Convert decimal �C -> internal data (100th of �C)
#define m_DegC(x)   ((TEMPERATURE_C_t) (x * 100) )

//Decimal �F -> internal data (100th of �C)
#define m_DegF(x)   ((TEMPERATURE_C_t) (((int32s) (x * 100) - 3200) * 5 / 9))

#define m_DegFAbs(x) ((TEMPERATURE_C_t) ((int32s) (x * 500) / 9))

typedef enum
{
  GENERIC_SUCCESS,                      /*!< Function executed successfully                                             */
  GENERIC_FAIL                          /*!< Function not executed correctly, error during process or bad parameters    */
}GenericStatus_t;

//Minimum/maximum setpoint - include setpoint min and setpoint max variables


////////////////////////////////////////////
// Control Heat State interface
////////////////////////////////////////////
// \enum <ControlHeatState_e>
typedef enum
{
    MIN_CONTROL_HEAT_STATE = 0,
    CONTROL_HEAT_STATE_OFF = MIN_CONTROL_HEAT_STATE,
    CONTROL_HEAT_STATE_ON,
    MAX_CONTROL_HEAT_STATE
} ControlHeatState_e;

typedef int8u   HEAT_DEMAND_PERCENT_t;

typedef enum {
    DEGREE_C,
    DEGREE_F,
} UnitFormat_t;

typedef enum {
    TIME_12H,
    TIME_24H,
} TimeFormat_t;

typedef enum {
    CONTROLMODE_AMBIENT,
    CONTROLMODE_FLOOR,
    CONTROLMODE_AMBIENTFLOOR,
} ControlMode_t;

typedef enum {
    FLOORTYPE_TILE,
    FLOORTYPE_ENGINEERED,
}FloorType_t;

typedef enum
{
    NO_RESET = 0,
    RESET_WANTED,
    RESET_CONFIRMED,
}Reset_t;

typedef enum
{
    NO_LEAVE = 0,
    LEAVE_IN_PROGRESS,
    LEAVE_CONFIRMED,
}LeaveIntention_t;

typedef struct
{
  U8BIT TouchDetected;
  U16BIT X;
  U16BIT Y;
}TS_StateTypeDef;

#define NO_SWIPE        0
#define LEFT_SWIPE      1
#define RIGHT_SWIPE     2
#define DOWN_SWIPE      3
#define UP_SWIPE        4

#endif // __COMMON_TYPES_H__
