/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       TimeFormatMenu.h

    \brief      Interface file for the TimeFormatMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains public definitions of the TimeFormatMenu module.

*******************************************************************************/

/*******************************************************************************
    Conditionnal compilation
*******************************************************************************/

#ifndef    __TIMEFORMATMENU_H
#define    __TIMEFORMATMENU_H

#include "common_types.h"
/*******************************************************************************
    Public constants
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public data types
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public variables
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Public functions
*******************************************************************************/

void MNU_TimeFormat_Entry(void);
void MNU_TimeFormat_Exit(void);
void MNU_TimeFormat_Core(void);
void MNU_TimeFormat_Init(void);
void MNU_TimeFormat_Commit(KEYB_PARAM keyId);
void MNU_TimeFormat_SetTimeFormat(TimeFormat_t format, uint8_t dst);

#endif	// __TIMEFORMATMENU_H
