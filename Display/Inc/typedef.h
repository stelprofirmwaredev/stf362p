//******************************************************************************
/// \file        typedef.h
/// \brief        Definit les types de donn�es
///
///    D�finit le type et format des variables utilis�es par le compilateur.
///
///    \author Jean-Fran�ois Many
///
///
/// Revision history at the end of the file.
///
//******************************************************************************
#ifndef __TYPEDEF_H
#define __TYPEDEF_H

/***************************************************************************//*!
*    Constantes Publiques
*******************************************************************************/

/******************************************************************************/
/*! \name Compilation Conditionnelle
*
*     Les constantes de compilation conditionnelle devraient �tre d�clar�es dans
*    les param�tres du projets.
*/
//! @{
#ifdef _DOC_BY_DOXYGEN_
#endif    //    _DOC_BY_DOXYGEN_
//! @}

/*! \name Constantes de configuration *****************************************/
//! @{
#ifndef TRUE
#define TRUE  (1)
#define FALSE (0)
#endif

#ifndef  NULL
 #define  NULL      0
#endif  //  NULL

#ifndef _24BITS
    #define  _24BITS            (16777216U)     //!<    Valeur Max de 24bits
    #define  _16BITS            (65536U)        //!<    Valeur Max de 16bits
    #define  _8BITS             (256U)          //!<    Valeur Max de 8bits
#endif  //  _24BITS
//! @}



//  **** Support Stelpro datatypes ****
/** Selecting high byte from 16-bit data
 * It's short and quite quick, but it can't be on left side of an equation. */
#define HIGH(x) ((U8BIT)((x)>>8))

/** Selecting low byte from 16-bit data
 * It's short and quite quick, but it can't be on left side of an equation. */
#define LOW(x) ((U8BIT)(x))

// The HIGH and LOW macros get the MSByte and LSByte respectively in a two byte
// value.  These may be used in both right and left sides of an equation.
// For microprocessors that use LSB followed by MSB, the macros are defined as
// follows and should be uncommented:

// gets the most significant byte of a U16BIT.  Will by type U8BIT.  Cast to
// S8BIT if desired
#define SET_HIGH(x) (*(((U8BIT*)&x) + 1))

// gets the least significant byte of a U16BIT.  Will be type U8BIT.  Cast to
// S8BIT if desired
#define SET_LOW(x)  (*(U8BIT*)&x)
// For microprocessors that use MSB followed by LSB, the macros are defined as
// follows and should be uncommented:

// gets the least significant byte of a U16BIT.  Will be type U8BIT.  Cast to
// S8BIT if desired
// #define SET_LOW(x) (*(((U8BIT*)&x) + 1))

// gets the most significant byte of a U16BIT.  Will by type U8BIT.  Cast to
// S8BIT if desired
// #define SET_HIGH(x)  (*(U8BIT*)&x)


/*! \name Bit definition ******************************************************/
//@{
#ifndef BIT0
  #define BIT0                    (0x0001U)
  #define BIT1                    (0x0002U)
  #define BIT2                    (0x0004U)
  #define BIT3                    (0x0008U)
  #define BIT4                    (0x0010U)
  #define BIT5                    (0x0020U)
  #define BIT6                    (0x0040U)
  #define BIT7                    (0x0080U)
  #define BIT8                    (0x0100U)
  #define BIT9                    (0x0200U)
  #define BITA                    (0x0400U)
  #define BITB                    (0x0800U)
  #define BITC                    (0x1000U)
  #define BITD                    (0x2000U)
  #define BITE                    (0x4000U)
  #define BITF                    (0x8000U)
#endif  //  BIT0
//@}


/***************************************************************************//*!
*    \name Memory attributes
*******************************************************************************/
//! @{
//! \brief Memory storage keyword.
//!
//! CONST allocate variables in non volatile flash. \n
//! IAR compiler keyword for flash storage.


#if(1 == __ICCSTM8__ )
    #define FLASHSTR    const
    #define FLASH_MEM
    #define PRINTF  printf_P
    #define GENERIC const
#else
    #define FLASH_MEM
    #define FLASHSTR
    #define PRINTF
    #define GENERIC
#endif


//! Visual studio 'C' inline
#ifdef _MSC_VER
#define INLINE __inline
#else // _MSC_VER
#define INLINE static inline
#endif // _MSC_VER


//  **** support for Stelpro datatypes ****
// keywords for declaring a variabale in RAM
#define RAMSECTIONUSED

//! keywords for declaring a ROM variable
#if(1 == __ICCAVR__ )
#define FORCETOCODE __flash
#else
#define FORCETOCODE
#endif

//! keywords for declaring a non-volatile memory variable
#define NVRAM __eeprom

//! special keyword for declaring a ROM based function pointer table
#define FUNCTIONTABLE __flash

//! @}

/***************************************************************************//*!
*    \name Types ind�pendants du compilateur (MISRA-C 6.3)
*******************************************************************************/
//! @{
typedef unsigned char   boolean;      //!<    Expression bool�ene (vrai / faux)
#if(1 == __ICCSTM8__ )
typedef unsigned char   bool;         //!<    Expression bool�ene (vrai / faux)
#endif
typedef char            char_t;
typedef unsigned char   int8u;        //!<    Integer 8 bits non sign�s
typedef signed char     int8s;        //!<    Integer 8 bits sign�s
typedef unsigned short  int16u;       //!<    Integer 16 bits non sign�s
typedef signed short    int16s;       //!<    Integer 16 bits sign�s
typedef unsigned long   int32u;       //!<    Integer 32 bits non sign�s
typedef signed long     int32s;       //!<    Integer 32 bits sign�s
typedef float           fp32;         //!<    Floating point 32 bits
typedef double          fp64;         //!<    Floating point 64 bits
typedef void            (*FUNC)(void);//!<    \brief Pointeur fonction type <tt>void func(void)</tt>

typedef unsigned char   FLAG;       /* boolean data */
typedef unsigned char   U8BIT;      /* unsigned 8 bit data */
typedef signed char     S8BIT;      /* signed 8 bit data */
typedef unsigned short  U16BIT;     /* unsigned 16 bit data */
typedef signed short    S16BIT;     /* signed 16 bit data */
typedef unsigned long   U32BIT;     /* unsigned 32 bit data */
typedef signed long     S32BIT;     /* signed 32 bit data */

typedef U8BIT           BITBYTE;    /* bit optimized byte */

typedef U16BIT          KEYB_PARAM; /* 16 bits to respect calling convention */
typedef U16BIT          calling_conv_t;
//! @}


/***************************************************************************//*!
*    \name Structures de data
*******************************************************************************/
typedef struct BitField8_s {
    int8u bit0 : 1;
    int8u bit1 : 1;
    int8u bit2 : 1;
    int8u bit3 : 1;
    int8u bit4 : 1;
    int8u bit5 : 1;
    int8u bit6 : 1;
    int8u bit7 : 1;
} BitField8_t;


//  **** Support Stelpro datatypes ****
typedef union
{
    struct
    {
        // NOTE;  the order of the two members depends on the microprocessor
        // representation of two byte values
        U8BIT Lsb;  // the least significant byte
        U8BIT Msb;  // the most significant byte
    } Seperate;
    U16BIT Combined;  // combined unsigned MSByte and LSByte
}TWOBYTE;  // format for representing a two byte value as a combined unsigned
    // MSByte and LSByte

typedef union
{
    struct
    {
        // NOTE;  the order of the two members depends on the microprocessor
        // representation of two byte values
        U8BIT Lsb;  // the least significant byte
        S8BIT Msb;  // the most significant byte
    } Seperate;
    S16BIT Combined;  // combined signed MSByte and LSByte
}STWOBYTE;  // format for representing a two byte value as a combined signed
    // MSByte and LSByte

#endif  //__TYPEDEF_H

