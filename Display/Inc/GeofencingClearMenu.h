/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       GeofencingClearMenu.h

    \brief      Interface file for the GeofencingClearMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains public definitions of the GeofencingClearMenu module.

*******************************************************************************/

/*******************************************************************************
    Conditionnal compilation
*******************************************************************************/

#ifndef    __GEOFENCINGCLEARMENU_H
#define    __GEOFENCINGCLEARMENU_H

/*******************************************************************************
    Public constants
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public data types
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public variables
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Public functions
*******************************************************************************/

void MNU_GeofencingClear_Entry(void);
void MNU_GeofencingClear_Exit(void);
void MNU_GeofencingClear_Core(void);
void MNU_GeofencingClear_Commit(KEYB_PARAM keyId);

#endif	// __GEOFENCINGCLEARMENU_H
