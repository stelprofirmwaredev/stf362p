/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2016, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       GroupsMenu.h

    \brief      Interface file for the GroupsMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains public definitions of the GroupsMenu module.

*******************************************************************************/

/*******************************************************************************
    Conditionnal compilation
*******************************************************************************/

#ifndef    __GROUPSMENU_H
#define    __GROUPSMENU_H

/*******************************************************************************
    Public constants
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public data types
*******************************************************************************/
typedef struct
{
    uint8_t position;
    uint8_t str[NAME_SIZE_AND_NULL];
} MnuSortedGroups_t;

/*******************************************************************************
    Public variables
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Public functions
*******************************************************************************/

void MNU_Groups_Core(void);
void MNU_Groups_Entry(void);
void MNU_Groups_Exit(void);
void MNU_Groups_Commit(KEYB_PARAM keyId);
uint8_t MNU_Groups_GetSelectedGroup(void);
uint8_t MNU_Groups_SortGroups(MnuSortedGroups_t* pSortedGroups);

#endif    // __GROUPSMENU_H
