/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       HalKeyset_setup.h

    \brief      Include file for the HAL keyset interface.

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains definitions that are specific to the HAL keyset device.

*******************************************************************************/

#ifndef __HALKEYSETSETUP__H_
#define __HALKEYSETSETUP__H_

/*-------------------------------------------------------------------------------
    INCLUDE
-------------------------------------------------------------------------------*/
#include "OsTask.h"

/*-------------------------------------------------------------------------------
    DEFINITIONS
-------------------------------------------------------------------------------*/
//! Configuration hal keyset task period
// Hal keyset polling time base, used by OS to scheduler periodically and
// used by the hal keyset to measure key hold and repeat durations. It basically
// defines the resolution of the hal keyset but keep in mind that increasing the
// resolution implies having to be scheduled more often. Key hold and repeat
// periods are derived from this constant, changing it should not affect the
// key behavior as long as resolution allows to smallest repeat delays.
#define HAL_KEYSET_TICK   OS_MSTOTICK(30)


//
//! Key identifiers
// \enum <HalKeyId_e>
//
typedef enum
{
    HAL_KEY_BUTTON_1 = 0,
    HAL_KEY_BUTTON_2,
	HAL_KEY_BUTTON_1_AND_2,
    MAX_HAL_KEY,
} HalKeyId_e;

#endif // __HALKEYSETSETUP__H_
