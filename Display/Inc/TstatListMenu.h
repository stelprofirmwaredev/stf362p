/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       TstatListMenu.h

    \brief      Interface file for the TstatListMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains public definitions of the TstatListMenu module.

*******************************************************************************/

/*******************************************************************************
    Conditionnal compilation
*******************************************************************************/

#ifndef    __TSTATLISTMENU_H
#define    __TSTATLISTMENU_H

/*******************************************************************************
    Public constants
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public data types
*******************************************************************************/


/*******************************************************************************
    Public variables
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Public functions
*******************************************************************************/

void MNU_TstatList_Core(void);
void MNU_TstatList_Entry(void);
void MNU_TstatList_Exit(void);
void MNU_TstatList_Commit(KEYB_PARAM keyId);
U8BIT MNU_TstatList_GetThermostatNumber(void);

#endif    // __TSTATLISTMENU_H
