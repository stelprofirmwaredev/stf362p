/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2019, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       FloorLimitMenu.h

    \brief      Interface file for the FloorLimitMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains public definitions of the FloorLimitMenu module.

*******************************************************************************/

/*******************************************************************************
    Conditionnal compilation
*******************************************************************************/

#ifndef    __FLOORLIMITMENU_H
#define    __FLOORLIMITMENU_H

/*******************************************************************************
    Public constants
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public data types
*******************************************************************************/


/*******************************************************************************
    Public variables
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Public functions
*******************************************************************************/

void MNU_FloorLimit_Core(void);
void MNU_FloorLimit_Entry(void);
void MNU_FloorLimit_Exit(void);
void MNU_FloorLimit_Commit(KEYB_PARAM keyId);
void MNU_FloorLimit_IncFloorLimit(int16u value);
void MNU_FloorLimit_DecFloorLimit(int16u value);
void MNU_FloorLimit_DecFloorLimitStartTurbo(void);
void MNU_FloorLimit_IncFloorLimitStartTurbo(void);
void MNU_FloorLimit_FloorLimitStopTurbo(void);
TEMPERATURE_C_t MNU_FloorLimit_GetFloorLimit(void);
void MNU_FloorLimit_CommitFloorLimit( void );
void MNU_FloorLimit_StopFloorLimitEdition(void);
void MNU_FloorLimit_Confirm (void);


#endif    // __FLOORLIMITMENU_H
