/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/


// ****************************************************************************
// FILENAME:      <SwVersion.h>
//
// MODULE:        <Software Version>
//
// DESCRIPTION:   This file contains references to global data and global
//                functions owned by this module.  Source files that interface
//                with this module should include this file.
//
// ****************************************************************************

// sentry so this file is included only once
#ifndef __SWVERSION_H__
#define __SWVERSION_H__

#include "typedef.h"

// ****************************************************************************
// GLOBAL DATA REFERENCES:
//
// References to global data owned by the module are included here. For each
// data item, comments include a description and specify all possible values
// and their meanings.  Any special use of the data item is also specified.

#define OEM_HOST_VERSION    19  //1.9

#define  SW_VER_MAJOR       (01)                 //    01.11
#define  SW_VER_MINOR       (11)

#define  SW_VER             ( (SW_VER_MAJOR * 100)  \
                            + (SW_VER_MINOR) )

#define  BUILD_INFO         "RC15"

#define  HARDWARE_VERSION   (01)

#define  ZIGBEE_STACK_VERSION   0x23

#endif  /* __SWVERSION_H__ */

