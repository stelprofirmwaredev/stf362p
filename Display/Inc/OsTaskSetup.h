/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       OsTaskSetup.h

    \brief      Os task scheduler task declaration include file.

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    This file contains definitions to be shared between the
    application layer and the os task scheduler.

    Table of modifications:

        - Version:      0.0.1
        - Date:         2015-01-23
        - Description:  First version
        .

*******************************************************************************/

#ifndef __OSTASKSETUP_H__
#define __OSTASKSETUP_H__

/*-------------------------------------------------------------------------------
    INCLUDE
-------------------------------------------------------------------------------*/
// none


/*-------------------------------------------------------------------------------
    DEFINITIONS
-------------------------------------------------------------------------------*/
//
//! OS tick settings
//
#define OS_TICK_FREQ            1000    // Hz, tick = 1ms

//
// Task identifier enumeration
// \enum <OsTaskId_e>
//
typedef enum
{
    MIN_OS_TASK_ID = 0,
    TSK_OS_SCHEDULER = MIN_OS_TASK_ID,      // MANDATORY SYSTEM TASK AND POSITION - DO NOT CHANGE OR MOVE
    TSK_HAL_TOUCH,
    TSK_APP_MNH_MENU,                       // UI menu handler task
    TSK_APP_MNH_EVENTS,
    TSK_APP_RTC,                            // Time tracking task
    TSK_LIMIT_CHECK,
    TSK_HAL_DEBUG_ACTION,
#ifdef DEVKIT_PLATFORM
    TSK_SIM_ALERT,
#endif /* DEVKIT_PLATFORM */
    TSK_OS_IDLE,                            // MANDATORY SYSTEM TASK AND POSITION - DO NOT CHANGE OR MOVE
    MAX_OS_TASK_ID,

    OS_NO_TASK = 0xff,
} OsTaskId_e;


/*-------------------------------------------------------------------------------
    DECLARATIONS
-------------------------------------------------------------------------------*/
// none


#endif // __OSTASKSETUP_H__

