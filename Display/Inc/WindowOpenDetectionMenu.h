/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       WindowOpenDetectionMenu.h

    \brief      Interface file for the WindowOpenDetectionMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains public definitions of the WindowOpenDetectionMenu module.

*******************************************************************************/

/*******************************************************************************
    Conditionnal compilation
*******************************************************************************/

#ifndef    __WINDOWOPENDETECTIONMENU_H
#define    __WINDOWOPENDETECTIONMENU_H

#include "common_types.h"
#include ".\DB\inc\THREAD_DBDataTypes.h"
/*******************************************************************************
    Public constants
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public data types
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public variables
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Public functions
*******************************************************************************/

void MNU_WindowOpenDetection_Entry(void);
void MNU_WindowOpenDetection_Exit(void);
void MNU_WindowOpenDetection_Core(void);
void MNU_WindowOpenDetection_Init(void);
void MNU_WindowOpenDetection_Commit(KEYB_PARAM keyId);
void MNU_WindowOpenDetection_SetWindowOpenDetection(WindowOpenDetection_t status);

#endif	// __WINDOWOPENDETECTIONMENU_H
