/********************************************************************************************
*    tools.h
*
*                                    Module Outils
*
*    Regroupe les fonctions de conversion de temp�rature ainsi que les fonctions
*    d'arrondissement.
*
*    Auteur: Jean-Fran�ois Many
*
**** Table des modifications ****************************************************************
*
*        v0.2.3
*    Derni�res modifications : 23/07/2009
*    Voir le fichier convert.c pour l'historique des modifications.
*
********************************************************************************************/

/********************************************************************************************
*    Includes
********************************************************************************************/
#include "typedef.h"

/********************************************************************************************
*    Constantes Publiques
********************************************************************************************/
#define  TLS_HYSTERESIS            10
/*** Compilation Conditionnelle *************************************************************
*
*     Les constantes de compilation conditionnelle devrait �tre d�clar�es dans les param�tre
*    du projets.
*/

/*** Constantes de configuration ***********************************************************/

/*** Configuration des I/Os ****************************************************************/

/*** Statuts d'erreur **********************************************************************/


/********************************************************************************************
*    Structures de data
********************************************************************************************/


/********************************************************************************************
*    Variables Publiques
********************************************************************************************/
/*** Variables globales sans initialisation ************************************************/

/*** Variables globale initialis�es lors d�claration ***************************************/

/*** Variables globale initialis�es � z�ro *************************************************/


/********************************************************************************************
*    Fonctions Publiques
********************************************************************************************/

/********************************************************************************************
*  Arrondis
*
*    Arrondis une temp�rature selon le param�tre fournis.
*
*    Param�tres :
*        temp_to_round : Temp�rature � arrondir (�C ou �F).
*        rounding : Valeur � laquelle l'arrondissement doit �tre fait.
*
*        Restrictions : Param�tre 'rounding' doit �tre supp�rieur � 1
*                     et inf�rieur � la valeur absolue de 'temp_to_round'.
*
*    Retourne :
*        Temp�rature arrondie.
*
********************************************************************************************/
int16s TLS_Round(int16s temp_to_round, int16s rounding);


/********************************************************************************************
*  Hyst�r�sis
*
*    Applique un hyst�r�sis sur une temp�rature.
*
*                        - - - - - - - - - - - - - -  <-- Seuil pour latcher nouvelle valeur
*                                                   } Hyst�r�sis
*    Valeur latch�e -->  ---------------------------
*                                                   } Hyst�r�sis
*                        - - - - - - - - - - - - - -  <-- Seuil pour latcher nouvelle valeur
*
*    Param�tres :
*        new_value : Valeur � v�rifier si ext�rieure de l'hyst�r�sis.
*        *old_value : Pointeur sur valeur m�diane de l'hyst�r�sis.
*
*    Retourne : NULL
*
********************************************************************************************/
void TLS_Hysteresis(int16s new_value, int16s * old_value);


/********************************************************************************************
*  Bit faible actif
*
*    Retourne la position du premier bit trouv� de b0 a b7
*
*    Param�tres :
*        data_in : temp�rature � convertir en BCD. (0 @ 9999)
*
*    Retourne :
*        0 - 7 position du bit
*        0 = bit0 set ou tous les bit a 0
*
********************************************************************************************/
int8u TLS_GetLSBitSet(int8u byte);


/********************************************************************************************
*  Inversion bits
*
*    Inverse la postion des bits dans un byte.
*     msb         lsb       msb         lsb
*     A B C D E F G H  -->  H G F E D C B A
*     | | | | | | | |       ^ ^ ^ ^ ^ ^ ^ ^
*     | | | | | | | +-------+ | | | | | | |
*     | | | | | | +-----------+ | | | | | |
*     | | | | | +---------------+ | | | | |
*     | | | | +-------------------+ | | | |
*     | | | +-----------------------+ | | |
*     | | +---------------------------+ | |
*     | +-------------------------------+ |
*     +-----------------------------------+
*
*    Param�tres :
*            data_in : byte � inverser
*
*    Retourne :
*            byte invers�
*
********************************************************************************************/
int8u TLS_BitFlip(int8u data_in);


/********************************************************************************************
*  Calcul checksum
*
*    Calcul le checksum des param�tres en RAM.
*        Checksum = Compl�ment deux de la somme de tous les bytes avec 0xF0.
*                = ~(0xF0 + Byte 0 + Byte n) + 1
*
*    Param�tres :
*            *addr : Adresse de d�part pour la calcul.
*            length : Nombre de bytes utilis�s pour le calcul
*
*    Retourne :
*        Checksum calcul�.
*
********************************************************************************************/
int8u TLS_CalculChecksumRam(const void * addr, int8u length);
