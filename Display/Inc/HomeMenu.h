/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       HomeMenu.h

    \brief      Interface file for the HomeMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains public definitions of the HomeMenu module.

*******************************************************************************/

/*******************************************************************************
    Conditionnal compilation
*******************************************************************************/

#ifndef    __HOMEMENU_H
#define    __HOMEMENU_H

#include "typedef.h"

#include ".\DB\inc\THREAD_DBDataTypes.h"


/*******************************************************************************
    Public constants
*******************************************************************************/
#define MODE_DISPLAY_BIT        0x0001
#define TIME_DISPLAY_BIT        0x0002
#define FAN_DISPLAY_BIT         0x0004
#define BUTTON_DISPLAY_BIT      0x0008
#define CONNECT_DISPLAY_BIT     0x0010
#define TEMP_DISPLAY_BIT        0x0020
#define HUM_DISPLAY_BIT         0x0040
#define WEATHER_DISPLAY_BIT     0x0080
#define HEAT_DISPLAY_BIT        0x0100
#define SETPOINT_DISPLAY_BIT    0x0200
#define LOCK_DISPLAY_BIT        0x0400
#define ALERT_DISPLAY_BIT       0x0800
#define CURRENT_DISPLAY_BIT     0x1000

#define ALL_DISPLAY_BIT         0xFFFF

/*******************************************************************************
    Public data types
*******************************************************************************/


/*******************************************************************************
    Public variables
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Public functions
*******************************************************************************/

void MNU_NotifyMenuChange(void);

void MNU_Home_Core(void);
void MNU_Home_Entry(void);
void MNU_Home_Exit(void);

void MNU_Setpoint_IncSetpoint(int16u value);
void MNU_Setpoint_DecSetpoint(int16u value);
void MNU_Setpoint_DecSetpointStartTurbo(void);
void MNU_Setpoint_IncSetpointStartTurbo(void);
void MNU_Setpoint_SetpointStopTurbo(void);
TEMPERATURE_C_t MNU_Setpoint_GetSetpoint(void);
void MNU_Setpoint_CommitSetpoint( void );
void MNU_Setpoint_StopSetpointEdition(void);
void MNU_Setpoint_Confirm (void);
void MNU_Setpoint_SetOffMode(void);

#endif    // __HOMEMENU_H
