/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2017, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       TstatDetailsMenu.h

    \brief      Interface file for the TstatDetailsMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains public definitions of the TstatDetailsMenu module.

*******************************************************************************/

/*******************************************************************************
    Conditionnal compilation
*******************************************************************************/

#ifndef    __TSTATDETAILSMENU_H
#define    __TSTATDETAILSMENU_H

/*******************************************************************************
    Public constants
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public data types
*******************************************************************************/


/*******************************************************************************
    Public variables
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Public functions
*******************************************************************************/

void MNU_TstatDetails_Core(void);
void MNU_TstatDetails_Entry(void);
void MNU_TstatDetails_Exit(void);
void MNU_TstatDetails_Commit(KEYB_PARAM keyId);
void MNU_TstatDetails_IncSetpoint(int16u value);
void MNU_TstatDetails_DecSetpoint(int16u value);
void MNU_TstatDetails_DecSetpointStartTurbo(void);
void MNU_TstatDetails_IncSetpointStartTurbo(void);
void MNU_TstatDetails_SetpointStopTurbo(void);
TEMPERATURE_C_t MNU_TstatDetails_GetSetpoint(void);
void MNU_TstatDetails_CommitSetpoint( void );
void MNU_TstatDetails_StopSetpointEdition(void);
void MNU_TstatDetails_Confirm (void);

#endif    // __TSTATDETAILSMENU_H
