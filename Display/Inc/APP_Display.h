/*******************************************************************************
* @file    HAL_Display.h
* @date    2016/07/14
* @authors Jean-Fran�ois Many
* @brief
*******************************************************************************/

#ifndef HAL_Display_H
#define HAL_Display_H

/*******************************************************************************
* Includes
*******************************************************************************/
#include "OsTask.h"
#include "common_types.h"
#include "KeyboardConfig.h"
#include ".\HAL\inc\HAL_WiFiTypes.h"
#include "typedef.h"
#include ".\DB\inc\THREAD_DBDataTypes.h"


/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define MIN_TEMP            (0 * 100)
#define MAX_TEMP            (50 * 100)
#define MAX_TEMP_F          (93 * 100)

#define SET_AMBIENT     0
#define SET_GROUP       1
#define SET_FLOOR_LIMIT 2
#define SET_FLOOR       3
#define SET_GROUP_FLOOR 4
#define SET_TEMPORARY   5

#define CHANGE_TOGGLE   1
#define CHANGE_SELECT   2
#define CHANGE_EDIT     4

/*******************************************************************************
* Public structures definitions
*******************************************************************************/


/*******************************************************************************
* Public variables declarations
*******************************************************************************/
extern char Network1[10][20];

/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void DPH_InitDisplay(void);
void DPH_DisplaySplashScreen(void);
void DPH_DisplayAmbientRoomTemperature(int16_t temperature, uint8_t isFloorTemp);
void DPH_DisplaySetpointApplied(uint8_t editInProgress);
void DPH_DisplaySetpoint(uint16_t setpoint, uint8_t firstEntry, uint8_t offMode, uint8_t type, uint8_t isLocalTstat);
void DPH_DisplayFloorLimit(uint16_t limit, uint8_t firstEntry);
void DPH_DisplaySettingsButton(void);
void DPH_DisplaySetpointButton(void);
void DPH_DisplayLock(Lock_t lockState);
void DPH_DisplayHeatingStatus(HEAT_DEMAND_PERCENT_t heatDemand, uint8_t offMode);
void DPH_DisplayAlertIcon(U8BIT activeAlert);
void DPH_DisplayTime(RtcTime_t* time);
void DPH_DisplayBack(void);
void DPH_DisplayRootSettings(void);
void DPH_DisplayMode(ScheduleModeEnum_t mode);
void DPH_DisplayModeList(void);
void DPH_DisplayGroups(void);
uint8_t DPH_DisplayGroupDetails(uint8_t group, uint8_t page);
void DPH_DisplayTstatDetails(uint8_t tstat);
void DPH_DisplayOptions(void);
void DPH_DisplayControlMode(ControlMode_t mode, uint8_t firstEntry);
void DPH_DisplayLockSettings(Lock_t lock, uint8_t firstEntry);
void DPH_DisplayBacklight(BacklightTimeout_t bl_mode, uint8_t firstEntry);
void DPH_DisplayLanguage(DisplayLang_t lang);
void DPH_DisplayUnitFormat(UnitFormat_t format, uint8_t firstEntry);
void DPH_DisplayTimeFormat(TimeFormat_t format, uint8_t dst, uint8_t firstEntry);
void DPH_DisplayFloorType(FloorType_t type, uint8_t firstEntry);
void DPH_DisplayReset(Reset_t reset);
void DPH_DisplayInfo(void);
void DPH_DisplayAlerts(U8BIT Page);
void DPH_DisplayAlertsDetails(uint8_t tstatIndex, uint8_t alertId);
void DPH_DisplayDateTime(uint8_t firstEntry, uint16_t year, uint8_t month, uint8_t day, uint16_t minutesOfDay, uint8_t weekDay);
void DPH_DisplayProgramSetpoint(uint8_t firstEntry, uint16_t setpoint, Program_t pgm);
void DPH_DisplayProgramSchedule(uint8_t firstEntry, uint16_t timeEdit, uint8_t pgm, uint8_t* selectedDays, PgmSchedule_t* pgmSchedule, uint8_t changeId, uint8_t changePending);
uint32_t DPH_GetColor(uint16_t setpoint);

#ifdef LOAD_CYCLING
void DPH_DisplayLoadCyclingMode(uint8_t mode);
#endif /* LOAD_CYCLING */
void DPH_DisplayLCDTest(uint16_t x, uint16_t y, uint16_t targetx, uint16_t targety);
void DPH_HandleTestDisplay(uint8_t testValue);
void DPH_HandleBlackDisplay(void);
void DPH_HandleWhiteDisplay(void);

#endif /* HAL_Display_H */
/** Copyright(C) 2014 Stelpro Design, All Rights Reserved**/
