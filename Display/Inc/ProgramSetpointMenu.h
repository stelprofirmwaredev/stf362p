/*******************************************************************************
* @file    ProgramSetpointMenu.h
* @date    2019/07/29
* @authors Jean-Fran�ois Many
* @brief
*******************************************************************************/

#ifndef ProgramSetpointMenu_H
#define ProgramSetpointMenu_H


/*******************************************************************************
* Includes
*******************************************************************************/



/*******************************************************************************
* Public constants definitions
*******************************************************************************/



/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void MNU_ProgramSetpoint_Entry(void);
void MNU_ProgramSetpoint_Core(void);
void MNU_ProgramSetpoint_Exit(void);
void MNU_ProgramSetpoint_Commit(KEYB_PARAM keyId);
void MNU_ProgramSetpoint_IncProgramSetpoint(int16u value);
void MNU_ProgramSetpoint_DecProgramSetpoint(int16u value);
void MNU_ProgramSetpoint_DecProgramSetpointStartTurbo(void);
void MNU_ProgramSetpoint_IncProgramSetpointStartTurbo(void);
void MNU_ProgramSetpoint_ProgramSetpointStopTurbo(void);
void MNU_ProgramSetpoint_CommitProgramSetpoint( void );
void MNU_ProgramSetpoint_StopProgramSetpointEdition(void);
void MNU_ProgramSetpoint_Confirm (void);


#endif /* ProgramSetpointMenu_H */
/** Copyright(C) 2019 Stelpro Design, All Rights Reserved**/
