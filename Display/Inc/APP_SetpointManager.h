/*******************************************************************************
* @file    APP_SetpointManager.h
* @date    2016/10/25
* @authors Jean-Fran�ois Many
* @brief
*******************************************************************************/

#ifndef APP_SetpointManager_H
#define APP_SetpointManager_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>
#include "OsTask.h"
#include "common_types.h"
#include ".\DB\inc\THREAD_DBDataTypes.h"

/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define OPEN_WINDOW_SETPOINT                m_DegC(7)
#define MINIMUM_SETPOINT                    m_DegC(5)
#define MAXIMUM_SETPOINT                    m_DegC(30)
#define MAXIMUM_FLOOR_SETPOINT              m_DegC(40)
#define MAXIMUM_ENGINEERED_FLOOR_SETPOINT   m_DegC(28)
#define MINIMUM_FLOOR_LIMIT                 m_DegC(20)

#define DEFAULT_HOME_SETPOINT       m_DegC(21)
#define DEFAULT_AWAY_SETPOINT       DEFAULT_HOME_SETPOINT - m_DegC(4)
#define DEFAULT_VACATION_SETPOINT   DEFAULT_AWAY_SETPOINT - m_DegC(4)
#define DEFAULT_STANDBY_SETPOINT    m_DegC(5)


#define MINIMUM_DEFAULT_SETPOINT    DEFAULT_STANDBY_SETPOINT + m_DegC(4) + m_DegC(4)

/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void STPM_Init(void);
uint16_t STPM_GetSetpointMin(void);
uint16_t STPM_GetSetpointMax(void);
OSTASK_DEF(STPIF_LimitCheck);

#endif /* APP_SetpointManager_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
