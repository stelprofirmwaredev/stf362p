/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       OsTask.h

    \brief      Interface file for the OS task scheduler module.

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains public definitions of the OS task scheduler module.

*******************************************************************************/

#ifndef __OSTASK_H__
#define __OSTASK_H__

/*-------------------------------------------------------------------------------
    INCLUDE
-------------------------------------------------------------------------------*/
#include "typedef.h"
#include "OsTaskSetup.h"
#include "HAL_Interrupt.h"

#ifdef OS_STATS_ENABLED
    #include "stm8l15x_tim2.h"
#endif //OS_STATS_ENABLED

//#include "BspInterrupt.h"
/*-------------------------------------------------------------------------------
    DEFINITIONS
-------------------------------------------------------------------------------*/
//
//! OS tick count type
// \typedef <OsTickCount_t>
//
typedef int32u      OsTickCount_t;


//
//! OS task identifier type
// \typedef <OsTaskId_t>
//
typedef OsTaskId_e       OsTaskId_t;


//
//! OS task delay type
// \typedef <OsTaskDelay_t>
//
typedef int16u      OsTaskDelay_t;


//
//! Task descriptor type
// \typedef <OsTaskDescriptor_t>
//
typedef enum
{
    OSTASK_IS_IDLE = 0,
    OSTASK_IS_WAITING_FOR_PERIPHERAL_EVENT,
} OsTask_Status_t;
typedef OsTask_Status_t (*OSTaskPointer)(void);
#define OSTASK_DEF(name)    				OsTask_Status_t name (void)

typedef struct
{
    OsTaskId_t      taskId;                 //! task identifier
    OSTaskPointer   taskEntry;              //! task entry point (callback)
    OsTaskDelay_t   taskAutoReloadDelay;    //! task period in tick units (0 if unused)
    FLAG            taskWdogEnable;         //! task wdog enabled flag
} OsTaskDescriptor_t;


//
// Uncomment this to enable task execution time statistics feature
//
//OS_STATS_ENABLED
//#define OS_STATS_ENABLED    1


//
// Configure which hardware timer is used to measure task execution time
//
#ifdef OS_STATS_ENABLED
#define OS_STATS_TIMER         TIM2_GetCounter()
#endif


//
// Disable task execution time statistics feature on PCSIM build
//
#if defined(PCSIM) && defined(OS_STATS_ENABLED)
  #undef OS_STATS_ENABLED
#endif  //  PCSIM && OS_STATS_ENABLED


//
// OS tasks statistics data type
//
#ifdef OS_STATS_ENABLED
typedef struct {
    OsTaskId_e TaskName;
    int16u  min;            //!<    Min execution time
    int16u  avg;            //!<    Average execution time
    int16u  max;            //!<    Max execution time
    int32u  task_run_cnt;
}OsTaskStats_t;

#define MAX_LOOP_TRACE  10
typedef struct {
    int16u LoopTime;
    int8u TaskTraceCnt;
    OsTaskId_e LoopTrace[MAX_LOOP_TRACE];
} OsMainLoopTiming_t;
#endif  //  OS_STATS_ENABLED


//
//! To adapt upper layer name with definition used by the scheduler code.
// \typedef <OS_NB_TASK>
//
#define OS_NB_TASK              MAX_OS_TASK_ID


//
//! OS_MSTOTICK converts milliseconds into OS tick units
// Partial ticks are rounded up
//
#define OS_MSTOTICK(ms)         (int16u)((int32u) (ms) * (int32u)OS_TICK_FREQ / (int32u)1000 + \
                                (((int32u) (ms) * (int32u)OS_TICK_FREQ % (int32u)1000) ? 1 : 0))


//
//! OS_TICKTOMS converts OS tick units into milliseconds
//
#define OS_TICKTOMS(tick)       ((int32u)tick * (int32u)1000 / (int32u)OS_TICK_FREQ)

/*-------------------------------------------------------------------------------
    DATA
-------------------------------------------------------------------------------*/
//
// Expected to be declared in OsTaskSetup.c
//
extern FLASH_MEM const OsTaskDescriptor_t OS_TASK_TABLE[MAX_OS_TASK_ID];


//
// Expected to be declared in OsTaskSetup.c
//
extern const OsTaskDelay_t OS_TASK_WDOG_PERIOD;


/*-------------------------------------------------------------------------------
    DECLARATIONS
-------------------------------------------------------------------------------*/

//
// Interface used by main loop and application tasks
//
extern void OS_Init(void);
extern void OS_StartTask(OsTaskId_t taskId);
extern void OS_StopTask(OsTaskId_t taskId);
extern void OS_DelayTask(OsTaskId_t taskIdO, OsTaskDelay_t delay);
extern OsTaskDelay_t OS_GetTaskTimer(OsTaskId_t taskId);
extern OsTickCount_t OS_GetTickCount(void);
#ifdef __cplusplus
extern "C" void OS_DoEvents(void);
#else
extern void OS_DoEvents(void);
#endif

INTERRUPTROUTINE(OS_HandleTickInterrupt);


int8u OS_IsTaskReady(void);

//
// Interface used by OsTaskSetup.c
//
extern OSTASK_DEF(OS_TaskScheduler);
extern OSTASK_DEF(OS_TaskIdle);

//
// Interface for OS statistics
//
#ifdef OS_STATS_ENABLED
extern void OS_ResetStats(void);
extern OsTaskStats_t * OS_GetStats(void);
#endif //OS_STATS_ENABLED


#endif // __OSTASK_H__


