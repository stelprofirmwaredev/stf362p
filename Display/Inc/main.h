/*******************************************************************************
* @file    main.h
* @date    2016/08/04
* @authors Jean-Fran�ois Many
* @brief
*******************************************************************************/

#ifndef __MAIN_H
#define __MAIN_H


/*******************************************************************************
* Includes
*******************************************************************************/



/*******************************************************************************
* Public constants definitions
*******************************************************************************/



/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/


#endif /* __MAIN_H */
/** Copyright(C) 2014 Stelpro Design, All Rights Reserved**/
