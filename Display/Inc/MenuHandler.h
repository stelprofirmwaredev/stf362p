/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       MenuHandler.h

    \brief      Interface file for the MenuHandler module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains public definitions of the MenuHandler module.

*******************************************************************************/

/*******************************************************************************
    Conditionnal compilation
*******************************************************************************/

#ifndef    __MENUHANDLER_H
#define    __MENUHANDLER_H

/*******************************************************************************
    INCLUDE
*******************************************************************************/
#include "typedef.h"
#include "OsTask.h"

/*******************************************************************************
    Public constants
*******************************************************************************/

/*******************************************************************************
    Public data types
*******************************************************************************/

//! Properties of a user interface context
typedef struct {

    FUNC    entry;          //!< Function executed on menu entry
    FUNC    core;           //!< Function that display the active menu
    FUNC    exit;           //!< Function executed on menu exit
} MENUS;

typedef void ( *MnhTimeoutCallback_t ) ( void* arg );

/*******************************************************************************
    Public variables
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

//Empty

/*******************************************************************************
    Public functions
*******************************************************************************/
OSTASK_DEF(MNH_tCallMenu);               //!< Periodic task for menu execution
OSTASK_DEF(MNH_tUiTimedEvents);

void MNH_Init(void);                    //!< Module initialization
int8u MNH_GetActiveMenu(void);          //!< Get the active menu
int8u MNH_GetPreviousMenu(void);        //!< Get the previous menu
void MNH_SetActiveMenu(calling_conv_t menu);//!< Set the active menu
int16u MNH_GetUI(void);                 //!< Get the active UI
void MNH_SetTimeoutCallback(MnhTimeoutCallback_t callback,  void* arg, U16BIT delay);
void MNH_ClearTimeoutCallback(void);

#endif    // __MENUHANDLER_H
