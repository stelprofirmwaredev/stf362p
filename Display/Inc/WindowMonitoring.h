/*******************************************************************************
* @file    WindowMonitoring.h
* @date    2016/02/24
* @authors Jean-Fran�ois Many
* @brief   Window Monitoring module header file
*******************************************************************************/

#ifndef WindowMonitoring_H
#define WindowMonitoring_H


/*******************************************************************************
* Includes
*******************************************************************************/



/*******************************************************************************
* Public constants definitions
*******************************************************************************/



/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
OSTASK_DEF(WM_WindowCheckTask);
void WM_SetDelta(S8BIT pct, TEMPERATURE_C_t temp);
void WM_SetWindowOpen(U8BIT window);
U8BIT WM_IsWindowOpen(void);
U8BIT WM_GetOpenWindowDelay(void);
U8BIT WM_GetCloseWindowDelay(void);
#ifdef DEVKIT_PLATFORM
OSTASK_DEF(Simulate_Alert);
#endif /* DEVKIT_PLATFORM */

#endif /* WindowMonitoring_H */
/** Copyright(C) 2014 Stelpro Design, All Rights Reserved**/
