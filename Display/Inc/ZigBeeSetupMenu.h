/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       ZigBeeSetupMenu.h

    \brief      Interface file for the ZigBeeSetupMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains public definitions of the ZigBeeSetupMenu module.

*******************************************************************************/

/*******************************************************************************
    Conditionnal compilation
*******************************************************************************/

#ifndef    __ZIGBEESETUPMENU_H
#define    __ZIGBEESETUPMENU_H

/*******************************************************************************
    Public constants
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public data types
*******************************************************************************/


/*******************************************************************************
    Public variables
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Public functions
*******************************************************************************/

void MNU_ZigBeeSetup_Core(void);
void MNU_ZigBeeSetup_Entry(void);
void MNU_ZigBeeSetup_Exit(void);
void MNU_ZigBeeSetup_Commit(KEYB_PARAM keyId);

#endif    // __ZIGBEESETUPMENU_H
