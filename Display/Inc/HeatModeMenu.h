/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       HeatModeMenu.h

    \brief      Interface file for the HeatModeMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains public definitions of the HeatModeMenu module.

*******************************************************************************/

/*******************************************************************************
    Conditionnal compilation
*******************************************************************************/

#ifndef    __HEATMODEMENU_H
#define    __HEATMODEMENU_H

#include "common_types.h"
/*******************************************************************************
    Public constants
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public data types
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public variables
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Public functions
*******************************************************************************/

void MNU_HeatMode_Entry(void);
void MNU_HeatMode_Exit(void);
void MNU_HeatMode_Core(void);
void MNU_HeatMode_Init(void);
void MNU_HeatMode_Commit(KEYB_PARAM keyId);
HeatMode_t MNU_HeatMode_GetHeatMode(void);
void MNU_HeatMode_SetHeatMode(HeatMode_t mode);
void MNU_HeatMode_SetLobbyMode(LobbyMode_t mode);

#endif	// __HEATMODEMENU_H
