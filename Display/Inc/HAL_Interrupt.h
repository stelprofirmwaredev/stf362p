/*******************************************************************************
* @file    HAL_Interrupt.h
* @date    2016/07/14
* @authors Jean-Fran�ois Many
* @brief
*******************************************************************************/

#ifndef HAL_Interrupt_H
#define HAL_Interrupt_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include "stm32f4xx_hal.h"


/*******************************************************************************
* Public constants definitions
*******************************************************************************/



/*******************************************************************************
* Public structures definitions
*******************************************************************************/
typedef enum
{
    ISR_RESUME_CODE_OP = 0,
    ISR_RETURN_TO_SLEEP,
} ISR_ResumeStatus_t;

//! prefix that declares an interrupt routine
#define INTERRUPTROUTINE(isrName)       ISR_ResumeStatus_t isrName (void)

//
//!	Declare variable required to save interrupt
//
#define BSP_InterruptDeclareCritical()  uint8_t int_backup;

//! Save interrupt config, and disable interupt
#define BSP_InterruptEnterCritical()    int_backup = __get_interrupt_state(); \
                                        __disable_interrupt();

//! Restore saved interrupt config
#define BSP_InterruptExitCritical()     __set_interrupt_state(int_backup);

//! Signal entry and exit of interrupts
#define BSP_InterruptEntry()            ;
#define BSP_InterruptExit()             ;

/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void HAL_InterruptDisableGlobalInterrupt(void);


#endif /* HAL_Interrupt_H */
/** Copyright(C) 2014 Stelpro Design, All Rights Reserved**/
