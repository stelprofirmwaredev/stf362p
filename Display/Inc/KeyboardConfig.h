/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       KeyboardConfig.h

    \brief      Configuration file for the KeyboardHandler module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains project specific keyboard parameters.

*******************************************************************************/

#ifndef    __KEYBOARDCONFIG_H
#define    __KEYBOARDCONFIG_H


/*******************************************************************************
    INCLUDES
*******************************************************************************/
// none


/*******************************************************************************
    DEFINITIONS
*******************************************************************************/
#define HAL_KEYSET_TICK   OS_MSTOTICK(30)

//
//! Key names enum
// \enum <KbhKeyId_e>
//
typedef enum
{
    KEY_FIRST = 0x0000,
    KEY_BACK = KEY_FIRST,
    KEY_SETTINGS,
    KEY_GROUPS,
    KEY_SETPOINT,
    KEY_DOWN,
    KEY_UP,
    KEY_MODE,
    KEY_PREVIOUS,
    KEY_NEXT,
    KEY_SWIPE,
    KEY_OPTIONS_1,
    KEY_OPTIONS_2,
    KEY_OPTIONS_3,
    KEY_OPTIONS_4,
    KEY_OPTIONS_5,
    KEY_OPTIONS_6,
    KEY_OPTIONS_7,
    KEY_OPTIONS_8,
    KEY_OPTIONS_9,
    KEY_OPTIONS_10,
    KEY_OPTIONS_11,
    KEY_OPTIONS_12,
    KEY_OPTIONS_13,
    KEY_OPTIONS_14,
    KEY_KEYBOARD,
    KEY_SCREEN,
    KEY_DONE,
    KEY_ALERTS,
    KEY_HOME,
    NB_OF_KEYS
} KbhKeyId_e;

//
//! Keyboard key names enum
// \enum <KbhKeyboard_e>
//
typedef enum
{
    KEY_KEYBOARD_FIRST = 0x0000,
    KEY_LETTER = KEY_KEYBOARD_FIRST,
    KEY_NUMBER,
    KEY_CAP_SYMBOL,
    KEY_BACKSPACE,
    KEY_SPACE,
    KEY_ROW1_COL1,
    KEY_ROW1_COL2,
    KEY_ROW1_COL3,
    KEY_ROW1_COL4,
    KEY_ROW1_COL5,
    KEY_ROW1_COL6,
    KEY_ROW1_COL7,
    KEY_ROW1_COL8,
    KEY_ROW1_COL9,
    KEY_ROW1_COL10,
    KEY_ROW2_COL1,
    KEY_ROW2_COL2,
    KEY_ROW2_COL3,
    KEY_ROW2_COL4,
    KEY_ROW2_COL5,
    KEY_ROW2_COL6,
    KEY_ROW2_COL7,
    KEY_ROW2_COL8,
    KEY_ROW2_COL9,
    KEY_ROW3_COL1,
    KEY_ROW3_COL2,
    KEY_ROW3_COL3,
    KEY_ROW3_COL4,
    KEY_ROW3_COL5,
    KEY_ROW3_COL6,
    KEY_ROW3_COL7,
    NB_OF_KEYS_ON_KEYBOARD,
    INVALID_KEY
} KbhKeyboard_e;

//
//! Keyboard page names enum
// \enum <KbhKeyboardPage_e>
//
typedef enum
{
    KEY_PAGE_CAPITAL = 0,
    KEY_PAGE_LOWER,
    KEY_PAGE_NUMBER,
    KEY_PAGE_SYMBOL,
    KEY_PAGE_TEST,
    KEY_PAGE_0_F,
    NB_OF_KEYBOARD_PAGE
}KbhKeyboardPage_e;

//! Properties of a touch zone context
typedef struct {

    int16u      startX;
    int16u      startY;
    int16u      width;
    int16u      height;
    KbhKeyId_e  bindedKey;
    int8u       activeMenu;
} TOUCH_ZONE;

//! Properties of a keyboard touch zone context
typedef struct {

    int16u          startX;
    int16u          startY;
    int16u          width;
    int16u          height;
    KbhKeyboard_e   bindedKey;
    int8u           activePage;
} KEYBOARD_ZONE;

//! Interval resolution for key automatic repetition (ms)
#define KEYB_REPEAT_DELAY     50

#define KEYB_SLOWER_REPEAT_DELAY     100


//! Initial delay when key automatic repetition is enabled (ms)
#define KEYB_REPEAT_INITIAL   1000

//! Delay to enter advanced menu (ms)
#define KEYB_MENU_HOLD   3000

// Macro to translate keyboard handler key id to hal keyset key id
// For now, ids are the same but they may change on other platforms
#define TO_HAL_KEY_ID(id)     ((KbhKeyId_e)id)


#endif    // __KEYBOARDCONFIG_H
