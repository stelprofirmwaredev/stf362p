/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2016, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       GroupDetailsMenu.h

    \brief      Interface file for the GroupDetailsMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains public definitions of the GroupDetailsMenu module.

*******************************************************************************/

/*******************************************************************************
    Conditionnal compilation
*******************************************************************************/

#ifndef    __GROUPDETAILSMENU_H
#define    __GROUPDETAILSMENU_H

/*******************************************************************************
    Public constants
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public data types
*******************************************************************************/


/*******************************************************************************
    Public variables
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Public functions
*******************************************************************************/

void MNU_GroupDetails_Core(void);
void MNU_GroupDetails_Entry(void);
void MNU_GroupDetails_Exit(void);
void MNU_GroupDetails_Commit(KEYB_PARAM keyId);
void MNU_GroupDetails_IncSetpoint(int16u value);
void MNU_GroupDetails_DecSetpoint(int16u value);
void MNU_GroupDetails_DecSetpointStartTurbo(void);
void MNU_GroupDetails_IncSetpointStartTurbo(void);
void MNU_GroupDetails_SetpointStopTurbo(void);
TEMPERATURE_C_t MNU_GroupDetails_GetSetpoint(void);
void MNU_GroupDetails_CommitSetpoint( void );
void MNU_GroupDetails_StopSetpointEdition(void);
void MNU_GroupDetails_GetSelectedTstat(uint8_t* tstat);
void MNU_GroupDetails_Confirm (void);

#endif    // __GROUPDETAILSMENU_H
