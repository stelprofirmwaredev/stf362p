/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       HalKeyset.h

    \brief      Include file for the HAL keyset interface.

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains definitions that are specific to the HAL keyset device.

*******************************************************************************/

#ifndef __HALKEYSET__H_
#define __HALKEYSET__H_

/*-------------------------------------------------------------------------------
    INCLUDE
-------------------------------------------------------------------------------*/
#include "typedef.h"
#include "common_types.h"
#include "OsTask.h"
#include "KeyboardConfig.h"

/*-------------------------------------------------------------------------------
    DEFINITIONS
-------------------------------------------------------------------------------*/
//
//! Key state bitfield
// \typedef <HalKeyState_t>
//
typedef struct
{
    int8u   press           :1;   //! key pressed state (0=release, 1=pressed)
    int8u   repeat          :1;   //! key repeated state (0=normal, 1=repeated)
    int8u   hold            :1;   //! key held down state (0=normal, 1=held)
    int8u   swipe           :1;   //! key swiped state (0=normal, 1=swiped)
    int8u   swipeDirection  :2;   //! swipe direction
    int8u   future_use      :2;   //! future expansion
} HalKeyState_t;


typedef int16u		HalKeyPosition_t;

//
//! Keyset state data type
// \typedef <HalKeysetState_t>
//
typedef HalKeyState_t HalKeysetState_t[NB_OF_KEYS];


//
//! Keyset upper layer callback type
// \typedef <HalKeysetCallback_t>
//
typedef void ( *HalKeysetCallback_t ) ( HalKeysetState_t state );


/*-------------------------------------------------------------------------------
    DECLARATIONS
-------------------------------------------------------------------------------*/
extern void HAL_KeysetInit ( void );

extern OSTASK_DEF(HAL_TouchScreenTask);

extern void HAL_KeysetBind ( HalKeysetCallback_t callback );

extern void HAL_KeysetSetKeyRepeatDelay ( KbhKeyId_e keyId, int16u delay );

extern void HAL_KeysetSetKeyHoldDelay ( KbhKeyId_e keyId, int16u delay );

extern void HAL_KeysetClearKeyTimers ( KbhKeyId_e keyId );

extern HalKeyState_t HAL_KeysetGetStatus (KbhKeyId_e keyId);

extern TS_StateTypeDef HAL_GetTouch(void);

extern int8u HAL_AreAllKeysReleased(void);

extern KbhKeyboard_e Keyb_DetectKeyboardTouchZone(TS_StateTypeDef keyboard_touch, KbhKeyboardPage_e page);

#endif // __HALKEYSET__H_
