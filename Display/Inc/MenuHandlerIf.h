/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       MenuHandlerIf.h

    \brief      Interface file for the MenuHandler module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains public definitions of the MenuHandler module.

*******************************************************************************/

/*******************************************************************************
    Conditionnal compilation
*******************************************************************************/

#ifndef    __MENUHANDLERIF_H
#define    __MENUHANDLERIF_H

/*******************************************************************************
    Public constants
*******************************************************************************/

#define MENU_TIMEOUT			    (30000)
#define ZIGBEE_ASSOCIATION_TIMEOUT  (60000)     //1 minute (called multiple times)
#define WIFI_REGISTER_TIMEOUT       (60000)     //1 minute (called multiple times)
#define DESTROY_NETWORK_TIMEOUT     (60000)
#define GENERATE_TOKEN_TIMEOUT		(1000)
#define COMMIT_TIMEOUT			    (1000)
#define CONFIRM_TIMEOUT			    (3000)
#define SEQUENCE_TIMEOUT		    (2000)
#define ANIMATION_TIMEOUT           (250)
#define SETPOINT_EDIT_TIMEOUT	    (2000)
#define BACKLIGHT_TIMEOUT           (15000)
#define REMOVE_TSTAT_TIMEOUT        (5000)
#define EXT_TEMP_TIMEOUT            (60000)     //1 minute (called multiple times)
#define HEARTBEAT_TIMEOUT           (15000)
#define RADIO_RESET_DELAY           (100)
#define IMMEDIATELY                 (1)

/*******************************************************************************
    Public data types
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public variables
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Public functions
*******************************************************************************/

void MNH_MenusInit(void);

#endif    // __MENUHANDLERIF_H
