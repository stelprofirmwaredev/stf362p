/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       DisplayLayoutMenu.h

    \brief      Interface file for the DisplayLayoutMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains public definitions of the DisplayLayoutMenu module.

*******************************************************************************/

/*******************************************************************************
    Conditionnal compilation
*******************************************************************************/

#ifndef    __DISPLAYLAYOUTMENU_H
#define    __DISPLAYLAYOUTMENU_H

/*******************************************************************************
    Public constants
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public data types
*******************************************************************************/


/*******************************************************************************
    Public variables
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Public functions
*******************************************************************************/

void MNU_DisplayLayout_Core(void);
void MNU_DisplayLayout_Entry(void);
void MNU_DisplayLayout_Exit(void);
void MNU_DisplayLayout_Init(void);
void MNU_DisplayLayout_Commit(KEYB_PARAM keyId);
DisplayLayout_t MNU_DisplayLayout_GetDisplayLayout(void);
void MNU_DisplayLayout_SetDisplayLayout(DisplayLayout_t layout);

#endif    // __DISPLAYLAYOUTMENU_H
