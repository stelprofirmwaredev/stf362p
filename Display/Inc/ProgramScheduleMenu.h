/*******************************************************************************
* @file    ProgramScheduleMenu.h
* @date    2019/07/29
* @authors Jean-Fran�ois Many
* @brief
*******************************************************************************/

#ifndef ProgramScheduleMenu_H
#define ProgramScheduleMenu_H


/*******************************************************************************
* Includes
*******************************************************************************/



/*******************************************************************************
* Public constants definitions
*******************************************************************************/



/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void MNU_ProgramSchedule_Entry(void);
void MNU_ProgramSchedule_Core(void);
void MNU_ProgramSchedule_Exit(void);
void MNU_ProgramSchedule_Commit(KEYB_PARAM keyId);
void MNU_ProgramSchedule_IncProgramSchedule(int16u value);
void MNU_ProgramSchedule_DecProgramSchedule(int16u value);
void MNU_ProgramSchedule_DecProgramScheduleStartTurbo(void);
void MNU_ProgramSchedule_IncProgramScheduleStartTurbo(void);
void MNU_ProgramSchedule_ProgramScheduleStopTurbo(void);

#endif /* ProgramScheduleMenu_H */
/** Copyright(C) 2019 Stelpro Design, All Rights Reserved**/
