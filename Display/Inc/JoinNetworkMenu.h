/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       JoinNetworkMenu.h

    \brief      Interface file for the JoinNetworkMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains public definitions of the JoinNetworkMenu module.

*******************************************************************************/

/*******************************************************************************
    Conditionnal compilation
*******************************************************************************/

#ifndef    __JOINNETWORKMENU_H
#define    __JOINNETWORKMENU_H

#include "common_types.h"
/*******************************************************************************
    Public constants
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public data types
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public variables
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Public functions
*******************************************************************************/

void MNU_JoinNetwork_Entry(void);
void MNU_JoinNetwork_Exit(void);
void MNU_JoinNetwork_Core(void);
void MNU_JoinNetwork_Init(void);
void MNU_JoinNetwork_Commit(KEYB_PARAM keyId);
JoinNetwork_t MNU_JoinNetwork_GetJoinNetwork(void);
NetworkChannel_t MNU_JoinNetwork_GetNetworkChannel(void);
void MNU_JoinNetwork_SetJoinNetwork(JoinNetwork_t join);
void MNU_JoinNetwork_SetNetworkChannel(NetworkChannel_t channel);

#endif	// __JOINNETWORKMENU_H
