/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       KeyboardHandler.h

    \brief      Interface file for the KeyboardHandler module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains public definitions of the KeyboardHandler module.

*******************************************************************************/

#ifndef    __KEYBOARDHANDLER_H
#define    __KEYBOARDHANDLER_H

/*******************************************************************************
    INCLUDES
*******************************************************************************/
#include "typedef.h"
#include "HalKeyset.h"
#include "KeyboardConfig.h"

/*******************************************************************************
    DEFINITIONS
*******************************************************************************/
// none


/*******************************************************************************
    DECLARATIONS
*******************************************************************************/
extern void KBH_SetKeyPressProperties(KbhKeyId_e keyId, FUNC handler, void *args);
extern void KBH_SetKeyReleaseProperties(KbhKeyId_e keyId, FUNC handler, void *args);
extern void KBH_SetKeyRepeatProperties(KbhKeyId_e keyId, FUNC handler, void *args, int16u delay);
extern void KBH_SetKeyHoldProperties(KbhKeyId_e keyId, FUNC handler, void *args, int16u delay);
extern void KBH_SetKeySwipeProperties(KbhKeyId_e keyId, FUNC handler, void *args);
extern void KBH_ClearAllKeyProperties(void);
extern void KBH_ClearKeyProperties(KbhKeyId_e keyId);
extern void KBH_IgnoreKeyRelease(KbhKeyId_e keyId);
extern void KBH_KeyClearPreviousState ( KbhKeyId_e keyId );
extern void KBH_Init(void);
extern void KBH_KeysetEventHandler ( HalKeysetState_t newKeysetState );


#endif    // __KEYBOARDHANDLER_H
