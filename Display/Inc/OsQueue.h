/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       OsQueue.h

    \brief      Interface file for the OS queue module.

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains public definitions of the OS queue module.

*******************************************************************************/

#ifndef __OSQUEUE_H__
#define __OSQUEUE_H__

/*-------------------------------------------------------------------------------
    INCLUDE
-------------------------------------------------------------------------------*/
#include "typedef.h"

/*-------------------------------------------------------------------------------
    DEFINITIONS
-------------------------------------------------------------------------------*/

//
//! OS queue instance type
// \typedef <OsQueue_t>
//
typedef struct
{
    void        *array;             //!< pointer to array where the queue contents are stored
    U16BIT      numOfElement;       //!< number of element currently in the queue
    U16BIT      sizeOfElement;      //!< size of element the queue contains
    U16BIT      maxNumOfElement;    //!< size of ther queue in number of elements
    void        *head;              //!< pointer to queue head
    void        *tail;              //!< pointer to queue tail
} OsQueue_t;


/*-------------------------------------------------------------------------------
    DECLARATIONS
-------------------------------------------------------------------------------*/

extern void OS_QueueInit ( OsQueue_t *q, void *array, U8BIT sizeOfElement, U16BIT maxNumOfElement );
extern FLAG OS_QueuePush ( OsQueue_t *q, void *element );
extern U16BIT OS_QueueChk ( OsQueue_t *q );
extern FLAG OS_QueuePop ( OsQueue_t *q, void *element );
extern void OS_QueueFlush ( OsQueue_t *q );


#endif // __OSQUEUE_H__
