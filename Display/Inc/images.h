/*******************************************************************************
* @file    images.h
* @date    2016/07/19
* @authors Jean-Fran�ois Many
* @brief
*******************************************************************************/

#ifndef images_H
#define images_H

/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>

/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define ADVISE_BOX_ADD          0x00000080
#define ADVISE_BOX_SMALL_ADD    0x00028690
#define SETPOINT_BOX_ADD        0x00042fa0
#define STELPRO_LOGO_ADD        0x0005d590
#define BLUE_BTN_ADD            0x0006dde0
#define GREY_BTN_ADD            0x00078f20
#define STELPRO_BTN_ADD         0x00084060
#define GREY_BTN_SHORT_ADD      0x0008f1a0
#define TRANSPARENT_BOX_ADD     0x000978e0
#define GREY_BTN_REDUCED_ADD    0x0009c5d0
#define LEAVE_BTN_ADD           0x000a0c30
#define MANUAL_BTN_ADD          0x000a4e44
#define RETURN_BTN_ADD          0x000a9058
#define SLEEP_BTN_ADD           0x000ad26c
#define WAKE_BTN_ADD            0x000b1480
#define BLUE_SQUARE_BTN_ADD     0x000b5694
#define PEN_BIG_ADD             0x000b7234
#define ALERT_ICON_ADD          0x000b8dd4
#define CHECK_ICON_ADD          0x000b9de4
#define SETTINGS_BTN_ADD        0x000badf4
#define LEAVE_ICON_ADD          0x000bbd84
#define FOOT_ICON_ADD           0x000bcc74
#define SCHEDULE_ICON_ADD       0x000bdae4
#define RETURN_ICON_ADD         0x000be8f4
#define PEN_SMALL_ADD           0x000bf5b4
#define PEN_SMALL_TRANSP_ADD    0x000c0204
#define SLEEP_ICON_ADD          0x000c0e54
#define LOCK_ICON_ADD           0x000c1aa4
#define MANUAL_ICON_ADD         0x000c26b4
#define WAKE_ICON_ADD           0x000c32c4
#define TOGGLE_WHITE_ADD        0x000c3ea4
#define TOGGLE_BLUE_ADD         0x000c48b4
#define BACK_ICON_ADD           0x000c52c4
#define NEXT_ICON_ADD           0x000c5ad4
#define HEAT_OFF_ICON_ADD       0x000c62e4
#define HEAT_ON_ICON_ADD        0x000c65f4

#define END_OF_IMAGES           0x000c6904

#define ADVISE_BOX_SIZE         ADVISE_BOX_SMALL_ADD - ADVISE_BOX_ADD
#define ADVISE_BOX_SMALL_SIZE   SETPOINT_BOX_ADD - ADVISE_BOX_SMALL_ADD
#define SETPOINT_BOX_SIZE       STELPRO_LOGO_ADD - SETPOINT_BOX_ADD
#define STELPRO_LOGO_SIZE       BLUE_BTN_ADD - STELPRO_LOGO_ADD
#define BLUE_BTN_SIZE           GREY_BTN_ADD - BLUE_BTN_ADD
#define GREY_BTN_SIZE           STELPRO_BTN_ADD - GREY_BTN_ADD
#define STELPRO_BTN_SIZE        GREY_BTN_SHORT_ADD - STELPRO_BTN_ADD
#define GREY_BTN_SHORT_SIZE     TRANSPARENT_BOX_ADD - GREY_BTN_SHORT_ADD
#define TRANSPARENT_BOX_SIZE    GREY_BTN_REDUCED_ADD - TRANSPARENT_BOX_ADD
#define GREY_BTN_REDUCED_SIZE   LEAVE_BTN_ADD - GREY_BTN_REDUCED_ADD
#define LEAVE_BTN_SIZE          MANUAL_BTN_ADD - LEAVE_BTN_ADD
#define MANUAL_BTN_SIZE         RETURN_BTN_ADD - MANUAL_BTN_ADD
#define RETURN_BTN_SIZE         SLEEP_BTN_ADD - RETURN_BTN_ADD
#define SLEEP_BTN_SIZE          WAKE_BTN_ADD - SLEEP_BTN_ADD
#define WAKE_BTN_SIZE           BLUE_SQUARE_BTN_ADD - WAKE_BTN_ADD
#define BLUE_SQUARE_BTN_SIZE    PEN_BIG_ADD - BLUE_SQUARE_BTN_ADD
#define PEN_BIG_SIZE            ALERT_ICON_ADD - PEN_BIG_ADD
#define ALERT_ICON_SIZE         CHECK_ICON_ADD - ALERT_ICON_ADD
#define CHECK_ICON_SIZE         SETTINGS_BTN_ADD - CHECK_ICON_ADD
#define SETTINGS_BTN_SIZE       LEAVE_ICON_ADD - SETTINGS_BTN_ADD
#define LEAVE_ICON_SIZE         FOOT_ICON_ADD - LEAVE_ICON_ADD
#define FOOT_ICON_SIZE          SCHEDULE_ICON_ADD - FOOT_ICON_ADD
#define SCHEDULE_ICON_SIZE      RETURN_ICON_ADD - SCHEDULE_ICON_ADD
#define RETURN_ICON_SIZE        PEN_SMALL_ADD - RETURN_ICON_ADD
#define PEN_SMALL_SIZE          PEN_SMALL_TRANSP_ADD - PEN_SMALL_ADD
#define PEN_SMALL_TRANSP_SIZE   SLEEP_ICON_ADD - PEN_SMALL_TRANSP_ADD
#define SLEEP_ICON_SIZE         LOCK_ICON_ADD - SLEEP_ICON_ADD
#define LOCK_ICON_SIZE          MANUAL_ICON_ADD - LOCK_ICON_ADD
#define MANUAL_ICON_SIZE        WAKE_ICON_ADD - MANUAL_ICON_ADD
#define WAKE_ICON_SIZE          TOGGLE_WHITE_ADD - WAKE_ICON_ADD
#define TOGGLE_WHITE_SIZE       TOGGLE_BLUE_ADD - TOGGLE_WHITE_ADD
#define TOGGLE_BLUE_SIZE        BACK_ICON_ADD - TOGGLE_BLUE_ADD
#define BACK_ICON_SIZE          NEXT_ICON_ADD - BACK_ICON_ADD
#define NEXT_ICON_SIZE          HEAT_OFF_ICON_ADD - NEXT_ICON_ADD
#define HEAT_OFF_ICON_SIZE      HEAT_ON_ICON_ADD - HEAT_OFF_ICON_ADD
#define HEAT_ON_ICON_SIZE       END_OF_IMAGES - HEAT_ON_ICON_ADD


/*******************************************************************************
* Public structures definitions
*******************************************************************************/
typedef struct
{
    uint16_t 	 width;
    uint16_t 	 height;
    uint16_t 	 bytes_per_pixel; /* 2:RGB16, 3:RGB, 4:RGBA */
    uint32_t	 address;
}image_t;


/*******************************************************************************
* Public variables declarations
*******************************************************************************/
extern const image_t advise_box;
extern const image_t advise_box_small;
extern const image_t box_setpoint;
extern const image_t logo_stelpro;
extern const image_t blue_btn;
extern const image_t grey_btn;
extern const image_t stelpro_btn;
extern const image_t grey_btn_short;
extern const image_t transparent_box;
extern const image_t grey_btn_reduced;
extern const image_t leave_btn;
extern const image_t manual_btn;
extern const image_t return_btn;
extern const image_t sleep_btn;
extern const image_t wake_btn;
extern const image_t blue_small_btn;
extern const image_t pen_big_icon;
extern const image_t alert_icon;
extern const image_t check_icon;
extern const image_t settings_btn;
extern const image_t leave_icon;
extern const image_t foot_icon;
extern const image_t return_icon;
extern const image_t pen_small_icon;
extern const image_t sleep_icon;
extern const image_t lock_icon;
extern const image_t manual_icon;
extern const image_t wake_icon;
extern const image_t toggle_white;
extern const image_t toggle_blue;
extern const image_t back_icon;
extern const image_t next_icon;
extern const image_t heat_off;
extern const image_t heat_on;


/*******************************************************************************
* Public functions declarations
*******************************************************************************/



#endif /* images_H */
/** Copyright(C) 2014 Stelpro Design, All Rights Reserved**/
