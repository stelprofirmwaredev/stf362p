/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       StringObjects.h

    \brief      Interface file for the StringObjects module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains public definitions of the StringObjects module.

*******************************************************************************/

#include "StringCollection.h"   //!< Expected string collection header file
#include "typedef.h"
/*******************************************************************************
    Conditionnal compilation
*******************************************************************************/

#ifndef    __STRINGOBJECTS_H
#define    __STRINGOBJECTS_H

/*******************************************************************************
    Public constants
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public data types
*******************************************************************************/

//! Time string formatting options
enum TimeFmt_e {

    STD_TIME = 0x00,
    LONG_12H = 0x01,
    USE_24H_FORMAT = 0x02,
    NO_SPACE = 0x04
};

//! Temperature string formatting options
enum TempFmt_e {

    STD_TEMP = 0x00,
    RAW_F_TEMP = 0x01,
    TEMP_BOUND = 0x02,
    TEMP_DOT = 0x04,
    ABS_TEMP = 0x08,
    ADD_DEG = 0x10,
    FAHRENHEIT = 0x20,
	ADD_FORMAT = 0x40,
    HUNDRED = 0x80,
    ADD_SPACING = 0x100,
    DECIMAL_ONLY = 0x200,
    HIDE_DECIMAL = 0x400
};

/*******************************************************************************
    Public variables
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

/*******************************************************************************
    Public functions
*******************************************************************************/

void STR_Init(void);    //!< Module initialization

//! Standards strings functions
int8u STR_StrLen(int8u GENERIC * str);
int8u* STR_StrPut(int8u* sdest, int8u ssrc);
int8u* STR_StrCat(int8u* sdest, int8u GENERIC * ssrc);
int8u* STR_StrCpy(int8u* sdest, int8u GENERIC * ssrc);
int8u* STR_StrNCpy(int8u* sdest, int8u GENERIC * ssrc, int8u num);
int8u* STR_StrItoA(int8u* str, int16u num);
int8u* STR_StrItoA_Hex(int8u* str, int16u num);

//! String collection functions
int8u const* STR_GetString(STRID id);   //!< String read function

//! String formatting functions
int8u* STR_Time2Str(int16u time, int8u* sdest, int8u options);
int8u* STR_Temp2Str(int16s temp, int8u* sdest, int16u options);
int8u* STR_Delay2Str(int16u delay, int8u* sdest);

#endif    // __STRINGOBJECTS_H
