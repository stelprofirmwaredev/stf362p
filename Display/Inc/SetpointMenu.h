/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2016, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       SetpointMenu.h

    \brief      Interface file for the SetpointMenu module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains public definitions of the SetpointMenu module.

*******************************************************************************/

/*******************************************************************************
    Conditionnal compilation
*******************************************************************************/

#ifndef    __SETPOINTMENU_H
#define    __SETPOINTMENU_H

/*******************************************************************************
    Public constants
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public data types
*******************************************************************************/


/*******************************************************************************
    Public variables
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

    //Empty

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Public functions
*******************************************************************************/

void MNU_Setpoint_Core(void);
void MNU_Setpoint_Entry(void);
void MNU_Setpoint_Exit(void);
void MNU_Setpoint_Commit(KEYB_PARAM keyId);
void MNU_Setpoint_IncSetpoint(int16u value);
void MNU_Setpoint_DecSetpoint(int16u value);
void MNU_Setpoint_DecSetpointStartTurbo(void);
void MNU_Setpoint_IncSetpointStartTurbo(void);
void MNU_Setpoint_SetpointStopTurbo(void);
TEMPERATURE_C_t MNU_Setpoint_GetSetpoint(void);
#ifdef PCT_HEAT
HEAT_DEMAND_PERCENT_t MNU_Setpoint_GetHeatPctSetpoint(void);
#endif
void MNU_Setpoint_CommitSetpoint( void );
void MNU_Setpoint_StopSetpointEdition(void);
void MNU_Setpoint_Confirm (void);


#endif    // __SETPOINTMENU_H
