/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       MenuTable.h

    \brief      Interface file for the MenuTable module

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains public definitions of the MenuTable module.

*******************************************************************************/

/*******************************************************************************
    Conditionnal compilation
*******************************************************************************/

#ifndef    __MENUTABLE_H
#define    __MENUTABLE_H

#include "MenuHandler.h"
/*******************************************************************************
    Public constants
*******************************************************************************/

    //Empty

/*******************************************************************************
    Public data types
*******************************************************************************/

//! Top level UI context list
enum menu_list {

    MENU_INIT = 0x0000,
    MENU_SPLASH_SCREEN,
    MENU_HOME,
    MENU_ADV_SETTINGS,
    MENU_DATE_TIME,
    MENU_PROGRAM_SETPOINT,
    MENU_PROGRAM_SCHEDULE,
    MENU_MODE_SELECTION,
    MENU_OPTIONS,
    MENU_INFO,
    MENU_CONTROL_MODE,
    MENU_FLOOR_TYPE,
    MENU_FLOOR_LIMIT,
    MENU_LOCK,
    MENU_BACKLIGHT,
    MENU_LANGUAGE,
    MENU_UNIT_FORMAT,
    MENU_TIME_FORMAT,
    MENU_RESET,
    MENU_ALERTS,
    MENU_ALERT_DETAILS,
    MENU_LCDTEST,
    MENU_TESTMODE,
    MENU_BLACK,
    MENU_WHITE,

    MENU_IDLE,
};

/*******************************************************************************
    Public variables
*******************************************************************************/

/*** Variables not initialized ************************************************/

    //Empty

/*** Variables initialized at declaration *************************************/

extern FLASH_MEM const MENUS Menus[];   //!< Top level contexts structure

/*** Variables initialized to 0 by the compiler *******************************/

    //Empty

/*******************************************************************************
    Public functions
*******************************************************************************/

int8u GetMenuCount(void);
void MNU_SetKeyAssigned(U8BIT status);
U8BIT IsKeyAssigned(void);

#endif    // __MENUTABLE_H
