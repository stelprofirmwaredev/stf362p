﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace OTA_Image_Builder
{
    public class OTAEncryption
    {
        private const int dataBlockSize = 16; // AES-128
        private const int keySize = 32; // 256-bit key
        private byte[] key;
        private byte[] IV;
        private Aes aesAlg;
        ICryptoTransform encryptor;
        ICryptoTransform decryptor;

        public OTAEncryption()
        {
            key = Encoding.ASCII.GetBytes("8VSirM1T5RdVySQt2OJ8NxwLeOZWafYJ");
            IV = Encoding.ASCII.GetBytes("J5tjW8ZsVTjRTuv4");

            aesAlg = Aes.Create();
            aesAlg.Key = key;
            aesAlg.IV = IV;
            aesAlg.Padding = PaddingMode.None;
            aesAlg.BlockSize = 128;
            aesAlg.Mode = CipherMode.ECB;

            // Create a encryptor/decryptor to perform the stream transform.
            encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
            decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

        }

        static public void printByteArr(byte[] arr, String arrayName = "")
        {
            if (arrayName.Length != 0)
            {
                Console.Write("{0} = ", arrayName);
            }

            Console.Write("{");
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write("{0:X2}", arr[i]);
                Console.Write(" ");
            }
            Console.Write("}\n\n");
        }


        public byte[] CreateKey(int len, int startingValue = 0)
        {
            byte[] key = new byte[len];
            for (int ii = 0; ii < key.Length; ii++)
            {
                key[ii] = (byte)((startingValue + ii) % 256);
            }

            return key;
        }

        public int DataBlockSize
        {
            get
            {
                return dataBlockSize;
            }
        }

        public static byte[] PadArrayWithFF(byte [] arr, int startingIndex)
        {
            for(int ii=startingIndex; ii< arr.Length; ii++)
            {
                arr[ii] = 0xFF;
            }
            return arr;
        }

        public byte[] EncryptBlock(byte[] dataIn)
        {
            byte[] encrypted;
            
            // Create the streams used for encryption.
            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    csEncrypt.Write(dataIn, 0, dataBlockSize);
                    encrypted = msEncrypt.ToArray();
                }
            }

            // Return the encrypted bytes from the memory stream.
            return encrypted;

        }

        public byte[] DecryptBlock(byte [] cipherIn)
        {
            // Declare the string used to hold
            // the decrypted text.
            byte[] plainTextArray = new byte[dataBlockSize];

            // Create an Aes object
            // with the specified key and IV.

            // Create the streams used for decryption.
            using (MemoryStream msDecrypt = new MemoryStream(cipherIn))
            {
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                {
                    csDecrypt.Read(plainTextArray, 0, dataBlockSize);
                }
            }

            return plainTextArray;

        }
        
    }
}
