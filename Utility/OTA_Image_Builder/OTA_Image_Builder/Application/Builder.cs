﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using DamienG.Security.Cryptography;


namespace OTA_Image_Builder
{
    class Builder
    {
        public const int STM32FIRMWARE_STARTADDRESS = 0x00000000;
        public const int SAMR21FIRMWARE_STARTADDRESS = 0x00180000;
        public const int IMAGESLIB_STARTADDRESS = 0x00237080;
        public const int IMAGESLIB_ENDADDRESS = 0x003b6ff8;
        public const int IMAGESLIB_CRC_ADDRESS = 0x003b6ff8;
        public const int FILECRC32_ADDRESS = 0x003b6ffc;

        Stream stm32FileContent = null;
        Stream samr21FileContent = null;
        Stream imagesFileContent = null;
        String outputFilePathAndName = null;
        String stm32FilePath = null;
        String samr21FilePath = null;
        String imagesFilePath = null;

        public void SetSTM32File (Stream fileStream, String filePath)
        {
            stm32FileContent = fileStream;
            stm32FilePath = filePath;
        }

        public void SetSAMR21File(Stream fileStream, String filePath)
        {
            samr21FileContent = fileStream;
            samr21FilePath = filePath;
        }

        public void SetImagesFile(Stream fileStream, String filePath)
        {
            imagesFileContent = fileStream;
            imagesFilePath = filePath;
        }

        public void SetOutputFile(String fileName)
        {
            outputFilePathAndName = fileName;
        }

        public TextBox BuildOTAFile ()
        {
            TextBox statusTextBox = new TextBox();

            if (null != outputFilePathAndName)
            {
                FileStream outputFileContent = new FileStream(outputFilePathAndName,
                                    FileMode.Create);
                String crc = String.Empty;
                String imageLibCrc = String.Empty;
                String statusMsg = String.Empty;
                String currentDirectory = Path.GetDirectoryName(outputFilePathAndName);
                String sourcesDirectory = currentDirectory + "\\Sources";
                String stm32FileName = Path.GetFileName(stm32FilePath);
                String samr21FileName = Path.GetFileName(samr21FilePath);
                String imagesFileName = Path.GetFileName(imagesFilePath);

                cpyStreamEncrypted(stm32FileContent, outputFileContent, STM32FIRMWARE_STARTADDRESS, (SAMR21FIRMWARE_STARTADDRESS - STM32FIRMWARE_STARTADDRESS));
                cpyStreamEncrypted(samr21FileContent, outputFileContent, SAMR21FIRMWARE_STARTADDRESS, (IMAGESLIB_STARTADDRESS - SAMR21FIRMWARE_STARTADDRESS));
                cpyStreamNotEncrypted(imagesFileContent, outputFileContent, IMAGESLIB_STARTADDRESS, (IMAGESLIB_ENDADDRESS - IMAGESLIB_STARTADDRESS));
                imageLibCrc = computeAndAppendCRC32(imagesFileContent, outputFileContent, IMAGESLIB_CRC_ADDRESS);
                crc = computeAndAppendCRC32((Stream)outputFileContent, outputFileContent, FILECRC32_ADDRESS);

                statusTextBox.Text = " - Build Completed\r\n";
                statusTextBox.Text += " - CRC32 = 0x" + crc + "\r\n";
                statusTextBox.Text += " - Image library CRC32 = 0x" + imageLibCrc + "\r\n";
                statusTextBox.Text += " - File size = " + outputFileContent.Length + " bytes\r\n";
                statusTextBox.Text += "\n - STM32 source file : " + stm32FilePath + "\r\n";
                statusTextBox.Text += " - SAMR21 source file : " + samr21FilePath + "\r\n";
                statusTextBox.Text += " - Image library source file : " + imagesFilePath + "\r\n";
                statusTextBox.Text += " - Output file : " + outputFilePathAndName + "\r\n\n\n";
                outputFileContent.Close();

                Directory.CreateDirectory(sourcesDirectory);
                Directory.CreateDirectory(sourcesDirectory + "\\STM32\\");
                Directory.CreateDirectory(sourcesDirectory + "\\SAMR21\\");
                Directory.CreateDirectory(sourcesDirectory + "\\Images\\");
                File.Copy(Path.GetFullPath(stm32FilePath), Path.Combine(sourcesDirectory + "\\STM32\\", stm32FileName), true);
                File.Copy(Path.GetFullPath(samr21FilePath), Path.Combine(sourcesDirectory + "\\SAMR21\\", samr21FileName), true);
                File.Copy(Path.GetFullPath(imagesFilePath), Path.Combine(sourcesDirectory + "\\Images\\", imagesFileName), true);

                FileStream logFile = new FileStream(Path.Combine(currentDirectory, "build_log.txt"),
                                    FileMode.Append);

                DateTime timestamp = DateTime.Now;
                String timestampString = "*** " + timestamp.ToString() + " ***\r\n";
                logFile.Write(Encoding.ASCII.GetBytes(timestampString), 0, timestampString.Length);
                logFile.Write(Encoding.ASCII.GetBytes(statusTextBox.Text), 0, statusTextBox.Text.Length);
                logFile.Close();

                /*
                FileInfo newFile = new FileInfo(sourcesDirectory + "\\STM32\\" + stm32FileName);
                FileInfo originalFile = new FileInfo(sourcesDirectory + "\\STM32\\" + stm32FileName);

                if (FilesAreEqual(originalFile, newFile, 0x3B7FFB))
                {
                    Console.WriteLine("Both files are identical");
                }*/

            }
            else
            {
                MessageBox.Show("Aucun fichier de sortie n'est sélectionné");
                statusTextBox.Text = "Build Failed";
            }

            return statusTextBox;
        }
        
        private void OTADecrypt(Stream inputStream, FileStream outputStream, int startAddress, int size)
        {
            OTAEncryption fileEncrypt = new OTAEncryption();

            byte[] dataFromFile = new byte[fileEncrypt.DataBlockSize];
            byte[] decryptedData = new byte[fileEncrypt.DataBlockSize];

            inputStream.Position = 0;
            outputStream.Position = startAddress;

            if (null != inputStream)
            {
                for (int offset = 0; offset < inputStream.Length; offset += fileEncrypt.DataBlockSize)
                {
                    int bytesRead = inputStream.Read(dataFromFile, 0, fileEncrypt.DataBlockSize);
                    decryptedData = fileEncrypt.DecryptBlock(dataFromFile);
                    try
                    {
                        outputStream.Write(decryptedData, 0, bytesRead);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: " + ex.Message);
                    }
                }

                Console.WriteLine("end of copy");
            }
            else
            {
                MessageBox.Show("Aucun fichier source configuré");
            }
        }

        private void cpyStreamEncrypted(Stream inputStream, FileStream outputStream, int startAddress, int size)
        {
            OTAEncryption fileEncrypt = new OTAEncryption();

            byte[] dataFromFile = new byte[fileEncrypt.DataBlockSize];
            byte[] encryptedData = new byte[fileEncrypt.DataBlockSize];
            byte[] paddingBytesBetweenFiles = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

            inputStream.Position = 0;
            outputStream.Position = startAddress;

            if (null != inputStream)
            {
                for (int offset = 0; offset < inputStream.Length; offset += fileEncrypt.DataBlockSize)
                {
                    int bytesRead = inputStream.Read(dataFromFile, 0, fileEncrypt.DataBlockSize);
                    if(bytesRead < fileEncrypt.DataBlockSize)
                    {
                        OTAEncryption.PadArrayWithFF(dataFromFile, bytesRead);
                    }
                    encryptedData = fileEncrypt.EncryptBlock(dataFromFile);
                    try
                    {
                        outputStream.Write(encryptedData, 0, fileEncrypt.DataBlockSize);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: " + ex.Message);
                    }
                }

                while (outputStream.Position < (startAddress + size))
                {
                    encryptedData = fileEncrypt.EncryptBlock(paddingBytesBetweenFiles);
                    outputStream.Write(encryptedData, 0, encryptedData.Length);
                }

                Console.WriteLine("end of copy");
            }
            else
            {
                MessageBox.Show("Aucun fichier source configuré");
            }
        }

        private void cpyStreamNotEncrypted(Stream inputStream, FileStream outputStream, int startAddress, int size)
        {
            outputStream.Position = startAddress;

            try
            {
                inputStream.Position = 0;
                inputStream.CopyTo(outputStream);
            }
            catch
            {
                MessageBox.Show("Aucun fichier source configuré");
            }

            while (outputStream.Position < (startAddress + size))
            {
                outputStream.Write(BitConverter.GetBytes(0xff), 0, 1);
            }

        }

        private String computeAndAppendCRC32 (Stream fromfile, FileStream tofile, int crc32Address)
        {
            Crc32 crc32 = new Crc32();
            String hash = String.Empty;
            byte[] fileCrc = new byte[4];

            fromfile.Position = 0;

            foreach (byte b in crc32.ComputeHash(fromfile))
            {
                hash += b.ToString("x2").ToLower();
            }

            fromfile.Position = 0;
            fileCrc = crc32.ComputeHash(fromfile);
            tofile.Position = crc32Address;
            tofile.Write(fileCrc, 0, 4);

            return hash;
        }

        public bool TestEncryption(String originalFileName, String encDecFileName, String decFileName, int fileSize)
        {

            FileStream originalFile = new FileStream(originalFileName,
                    FileMode.Open);
            FileStream encFile = new FileStream(encDecFileName, FileMode.Create);
            cpyStreamEncrypted(originalFile, encFile, 0, (fileSize));
            encFile.Close();
            originalFile.Close();

            encFile = new FileStream(encDecFileName, FileMode.Open);
            FileStream decFile = new FileStream(decFileName, FileMode.Create);
            OTADecrypt(encFile, decFile, 0, fileSize);
            encFile.Close();
            decFile.Close();

            FileInfo originalFileInfo = new FileInfo(originalFileName);
            FileInfo decodedFileInfo = new FileInfo(decFileName);

            return FilesAreEqual(originalFileInfo, decodedFileInfo, fileSize);

        }

        // Inspired by https://stackoverflow.com/questions/1358510/how-to-compare-2-files-fast-using-net
        static bool FilesAreEqual(FileInfo first, FileInfo second, int numBytesToCheck)
        {
            const int BYTES_TO_READ = sizeof(Int64);
            
            int iterations = (int)Math.Ceiling((double)numBytesToCheck / BYTES_TO_READ);

            using (FileStream fs1 = first.OpenRead())
            using (FileStream fs2 = second.OpenRead())
            {
                byte[] one = new byte[BYTES_TO_READ];
                byte[] two = new byte[BYTES_TO_READ];

                for (int i = 0; i < iterations; i++)
                {
                    fs1.Read(one, 0, BYTES_TO_READ);
                    fs2.Read(two, 0, BYTES_TO_READ);

                    String Chunk1 = BitConverter.ToString(one);
                    String Chunk2 = BitConverter.ToString(two);

                    if (Chunk1 != Chunk2)
                    {
                        Console.WriteLine("Bytes not equal at {0:X8}: {1:X8} != {2:X8}", i*BYTES_TO_READ,
                            Chunk1, Chunk2);
                        return false;
                    }
                }
            }
            return true;
        }




    }
}
