﻿namespace OTA_Image_Builder
{
    partial class OTABuilder_Form
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OTABuilder_Form));
            this.Builder_OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.STM32_Label = new System.Windows.Forms.Label();
            this.STM32_File_TextBox = new System.Windows.Forms.TextBox();
            this.STM32_OpenFile_Button = new System.Windows.Forms.Button();
            this.SAMR21_OpenFile_Button = new System.Windows.Forms.Button();
            this.SAMR21_File_TextBox = new System.Windows.Forms.TextBox();
            this.SAMR21_Label = new System.Windows.Forms.Label();
            this.Images_OpenFile_Button = new System.Windows.Forms.Button();
            this.Images_File_TextBox = new System.Windows.Forms.TextBox();
            this.Image_Label = new System.Windows.Forms.Label();
            this.Output_OpenFile_Button = new System.Windows.Forms.Button();
            this.Output_File_TextBox = new System.Windows.Forms.TextBox();
            this.Output_Label = new System.Windows.Forms.Label();
            this.Build_Button = new System.Windows.Forms.Button();
            this.statusTextBox = new System.Windows.Forms.TextBox();
            this.statusLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Builder_OpenFileDialog
            // 
            this.Builder_OpenFileDialog.InitialDirectory = "C:\\Projects\\TUX Firmwares\\Controleur";
            // 
            // STM32_Label
            // 
            this.STM32_Label.AutoSize = true;
            this.STM32_Label.Location = new System.Drawing.Point(72, 25);
            this.STM32_Label.Name = "STM32_Label";
            this.STM32_Label.Size = new System.Drawing.Size(42, 13);
            this.STM32_Label.TabIndex = 0;
            this.STM32_Label.Text = "STM32";
            // 
            // STM32_File_TextBox
            // 
            this.STM32_File_TextBox.Location = new System.Drawing.Point(120, 22);
            this.STM32_File_TextBox.Name = "STM32_File_TextBox";
            this.STM32_File_TextBox.Size = new System.Drawing.Size(206, 20);
            this.STM32_File_TextBox.TabIndex = 1;
            // 
            // STM32_OpenFile_Button
            // 
            this.STM32_OpenFile_Button.Location = new System.Drawing.Point(332, 17);
            this.STM32_OpenFile_Button.Name = "STM32_OpenFile_Button";
            this.STM32_OpenFile_Button.Size = new System.Drawing.Size(38, 29);
            this.STM32_OpenFile_Button.TabIndex = 2;
            this.STM32_OpenFile_Button.Text = "...";
            this.STM32_OpenFile_Button.UseVisualStyleBackColor = true;
            this.STM32_OpenFile_Button.Click += new System.EventHandler(this.STM32_OpenFile_Button_Click);
            // 
            // SAMR21_OpenFile_Button
            // 
            this.SAMR21_OpenFile_Button.Location = new System.Drawing.Point(332, 54);
            this.SAMR21_OpenFile_Button.Name = "SAMR21_OpenFile_Button";
            this.SAMR21_OpenFile_Button.Size = new System.Drawing.Size(38, 29);
            this.SAMR21_OpenFile_Button.TabIndex = 5;
            this.SAMR21_OpenFile_Button.Text = "...";
            this.SAMR21_OpenFile_Button.UseVisualStyleBackColor = true;
            this.SAMR21_OpenFile_Button.Click += new System.EventHandler(this.SAMR21_OpenFile_Button_Click);
            // 
            // SAMR21_File_TextBox
            // 
            this.SAMR21_File_TextBox.Location = new System.Drawing.Point(120, 59);
            this.SAMR21_File_TextBox.Name = "SAMR21_File_TextBox";
            this.SAMR21_File_TextBox.Size = new System.Drawing.Size(206, 20);
            this.SAMR21_File_TextBox.TabIndex = 4;
            // 
            // SAMR21_Label
            // 
            this.SAMR21_Label.AutoSize = true;
            this.SAMR21_Label.Location = new System.Drawing.Point(64, 62);
            this.SAMR21_Label.Name = "SAMR21_Label";
            this.SAMR21_Label.Size = new System.Drawing.Size(50, 13);
            this.SAMR21_Label.TabIndex = 3;
            this.SAMR21_Label.Text = "SAMR21";
            // 
            // Images_OpenFile_Button
            // 
            this.Images_OpenFile_Button.Location = new System.Drawing.Point(332, 92);
            this.Images_OpenFile_Button.Name = "Images_OpenFile_Button";
            this.Images_OpenFile_Button.Size = new System.Drawing.Size(38, 29);
            this.Images_OpenFile_Button.TabIndex = 8;
            this.Images_OpenFile_Button.Text = "...";
            this.Images_OpenFile_Button.UseVisualStyleBackColor = true;
            this.Images_OpenFile_Button.Click += new System.EventHandler(this.Images_OpenFile_Button_Click);
            // 
            // Images_File_TextBox
            // 
            this.Images_File_TextBox.Location = new System.Drawing.Point(120, 97);
            this.Images_File_TextBox.Name = "Images_File_TextBox";
            this.Images_File_TextBox.Size = new System.Drawing.Size(206, 20);
            this.Images_File_TextBox.TabIndex = 7;
            // 
            // Image_Label
            // 
            this.Image_Label.AutoSize = true;
            this.Image_Label.Location = new System.Drawing.Point(72, 97);
            this.Image_Label.Name = "Image_Label";
            this.Image_Label.Size = new System.Drawing.Size(41, 13);
            this.Image_Label.TabIndex = 6;
            this.Image_Label.Text = "Images";
            // 
            // Output_OpenFile_Button
            // 
            this.Output_OpenFile_Button.Location = new System.Drawing.Point(479, 163);
            this.Output_OpenFile_Button.Name = "Output_OpenFile_Button";
            this.Output_OpenFile_Button.Size = new System.Drawing.Size(38, 29);
            this.Output_OpenFile_Button.TabIndex = 11;
            this.Output_OpenFile_Button.Text = "...";
            this.Output_OpenFile_Button.UseVisualStyleBackColor = true;
            this.Output_OpenFile_Button.Click += new System.EventHandler(this.Output_OpenFile_Button_Click);
            // 
            // Output_File_TextBox
            // 
            this.Output_File_TextBox.Location = new System.Drawing.Point(267, 168);
            this.Output_File_TextBox.Name = "Output_File_TextBox";
            this.Output_File_TextBox.Size = new System.Drawing.Size(206, 20);
            this.Output_File_TextBox.TabIndex = 10;
            // 
            // Output_Label
            // 
            this.Output_Label.AutoSize = true;
            this.Output_Label.Location = new System.Drawing.Point(203, 171);
            this.Output_Label.Name = "Output_Label";
            this.Output_Label.Size = new System.Drawing.Size(58, 13);
            this.Output_Label.TabIndex = 9;
            this.Output_Label.Text = "Output File";
            // 
            // Build_Button
            // 
            this.Build_Button.Location = new System.Drawing.Point(75, 152);
            this.Build_Button.Name = "Build_Button";
            this.Build_Button.Size = new System.Drawing.Size(107, 40);
            this.Build_Button.TabIndex = 12;
            this.Build_Button.Text = "Build";
            this.Build_Button.UseVisualStyleBackColor = true;
            this.Build_Button.Click += new System.EventHandler(this.Build_Button_Click);
            // 
            // statusTextBox
            // 
            this.statusTextBox.AcceptsReturn = true;
            this.statusTextBox.Location = new System.Drawing.Point(332, 201);
            this.statusTextBox.Multiline = true;
            this.statusTextBox.Name = "statusTextBox";
            this.statusTextBox.ReadOnly = true;
            this.statusTextBox.Size = new System.Drawing.Size(230, 100);
            this.statusTextBox.TabIndex = 13;
            this.statusTextBox.Text = "not build yet";
            // 
            // statusLabel
            // 
            this.statusLabel.AutoSize = true;
            this.statusLabel.Location = new System.Drawing.Point(289, 204);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(43, 13);
            this.statusLabel.TabIndex = 14;
            this.statusLabel.Text = "Status :";
            // 
            // OTABuilder_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(589, 313);
            this.Controls.Add(this.statusLabel);
            this.Controls.Add(this.statusTextBox);
            this.Controls.Add(this.Build_Button);
            this.Controls.Add(this.Output_OpenFile_Button);
            this.Controls.Add(this.Output_File_TextBox);
            this.Controls.Add(this.Output_Label);
            this.Controls.Add(this.Images_OpenFile_Button);
            this.Controls.Add(this.Images_File_TextBox);
            this.Controls.Add(this.Image_Label);
            this.Controls.Add(this.SAMR21_OpenFile_Button);
            this.Controls.Add(this.SAMR21_File_TextBox);
            this.Controls.Add(this.SAMR21_Label);
            this.Controls.Add(this.STM32_OpenFile_Button);
            this.Controls.Add(this.STM32_File_TextBox);
            this.Controls.Add(this.STM32_Label);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OTABuilder_Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Maestro OTA Image Builder";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog Builder_OpenFileDialog;
        private System.Windows.Forms.Label STM32_Label;
        private System.Windows.Forms.TextBox STM32_File_TextBox;
        private System.Windows.Forms.Button STM32_OpenFile_Button;
        private System.Windows.Forms.Button SAMR21_OpenFile_Button;
        private System.Windows.Forms.TextBox SAMR21_File_TextBox;
        private System.Windows.Forms.Label SAMR21_Label;
        private System.Windows.Forms.Button Images_OpenFile_Button;
        private System.Windows.Forms.TextBox Images_File_TextBox;
        private System.Windows.Forms.Label Image_Label;
        private System.Windows.Forms.Button Output_OpenFile_Button;
        private System.Windows.Forms.TextBox Output_File_TextBox;
        private System.Windows.Forms.Label Output_Label;
        private System.Windows.Forms.Button Build_Button;
        private System.Windows.Forms.TextBox statusTextBox;
        private System.Windows.Forms.Label statusLabel;
    }
}

