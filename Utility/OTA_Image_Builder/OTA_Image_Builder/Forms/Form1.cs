﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OTA_Image_Builder
{
    public partial class OTABuilder_Form : Form
    {
        Builder newBuild = new Builder();

        public OTABuilder_Form()
        {
            InitializeComponent();
            /*
            String currentDirectory = Directory.GetCurrentDirectory() + "\\..\\..\\..\\..\\..\\OTA\\Version 00.19\\Sources";
            String targetFileName = currentDirectory + "\\Controler\\ControlerV2-DAC.srec";
            FileInfo targetFile = new FileInfo(targetFileName);

            bool testPass;
            testPass = newBuild.TestEncryption((currentDirectory + "\\Controler\\ControlerV2-DAC.srec"),
                            currentDirectory + "\\encSTM32.bin",
                            currentDirectory + "\\decSTM32.bin",
                            (int) targetFile.Length);

            if (testPass)
            {
                Console.WriteLine("STM32 file Enc/Dec is successful");
            }

            targetFileName = currentDirectory + "\\ZigBee\\ATSAMR21E18A.srec";
            targetFile = new FileInfo(targetFileName);

            testPass = newBuild.TestEncryption((currentDirectory + "\\ZigBee\\ATSAMR21E18A.srec"),
                            currentDirectory + "\\encZigbee.bin",
                            currentDirectory + "\\decZigbee.bin",
                            (int)targetFile.Length);

            if (testPass)
            {
                Console.WriteLine("Zigbee file Enc/Dec is successful");
            }*/

        }


        private void STM32_OpenFile_Button_Click(object sender, EventArgs e)
        {
            Stream fileContent = null;
            String openDirectory = Directory.GetCurrentDirectory() + "\\..\\..\\..\\..\\..\\OTA\\";
            this.Builder_OpenFileDialog.Filter = "Motorola S-record (*.srec)|*.srec";
            this.Builder_OpenFileDialog.FilterIndex = 2;
            this.Builder_OpenFileDialog.RestoreDirectory = true;
            this.Builder_OpenFileDialog.InitialDirectory = openDirectory;

            if (this.Builder_OpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((fileContent = this.Builder_OpenFileDialog.OpenFile()) != null)
                    {
                        newBuild.SetSTM32File(fileContent, this.Builder_OpenFileDialog.FileName);
                        this.STM32_File_TextBox.Text = this.Builder_OpenFileDialog.FileName.ToString();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void SAMR21_OpenFile_Button_Click(object sender, EventArgs e)
        {
            Stream fileContent = null;
            String openDirectory = Directory.GetCurrentDirectory() + "\\..\\..\\..\\..\\..\\OTA\\";

            this.Builder_OpenFileDialog.Filter = "Motorola S-record (*.srec)|*.srec";
            this.Builder_OpenFileDialog.FilterIndex = 2;
            this.Builder_OpenFileDialog.RestoreDirectory = true;
            this.Builder_OpenFileDialog.InitialDirectory = openDirectory + "\\Zigbee";

            if (this.Builder_OpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((fileContent = this.Builder_OpenFileDialog.OpenFile()) != null)
                    {
                        newBuild.SetSAMR21File(fileContent, this.Builder_OpenFileDialog.FileName);
                        this.SAMR21_File_TextBox.Text = this.Builder_OpenFileDialog.FileName.ToString();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void Images_OpenFile_Button_Click(object sender, EventArgs e)
        {
            Stream fileContent = null;
            String openDirectory = Directory.GetCurrentDirectory() + "\\..\\..\\..\\..\\..\\OTA\\";

            this.Builder_OpenFileDialog.Filter = "Binary File (*.bin)|*.bin";
            this.Builder_OpenFileDialog.FilterIndex = 2;
            this.Builder_OpenFileDialog.RestoreDirectory = true;
            this.Builder_OpenFileDialog.InitialDirectory = openDirectory + "\\Images";

            if (this.Builder_OpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((fileContent = this.Builder_OpenFileDialog.OpenFile()) != null)
                    {
                        newBuild.SetImagesFile(fileContent, this.Builder_OpenFileDialog.FileName);
                        this.Images_File_TextBox.Text = this.Builder_OpenFileDialog.FileName.ToString();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void Output_OpenFile_Button_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();

            saveDialog.InitialDirectory = Directory.GetCurrentDirectory() + "\\..\\..\\..\\..\\..\\OTA\\";
            saveDialog.Filter = "Binary File (*.bin)|*.bin";
            saveDialog.FilterIndex = 2;
            saveDialog.RestoreDirectory = true;
            saveDialog.FileName = "maestroBuild.bin";

            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
//                    if ((this.Builder_OpenFileDialog.OpenFile()) != null)
                    {
                        newBuild.SetOutputFile(saveDialog.FileName.ToString());
                        this.Output_File_TextBox.Text = saveDialog.FileName.ToString();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not open file from disk. Original error: " + ex.Message);
                }
            }

        }

        private void Build_Button_Click(object sender, EventArgs e)
        {
            this.statusTextBox.Text = newBuild.BuildOTAFile().Text;            
        }

    }
}
