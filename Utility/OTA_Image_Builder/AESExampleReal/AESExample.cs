﻿using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Linq;

namespace Aes_Example
{
    class AesExample
    {

        static void printByteArr(byte[] arr, String arrayName = "")
        {
            if (arrayName.Length != 0)
            {
                Console.Write("{0} = ", arrayName);
            }

            Console.Write("{");
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write("{0:X2}", arr[i]);
                Console.Write(" ");
            }
            Console.Write("}\n\n");
        }

        static public byte[] CreateKey(int len, int startingValue = 0)
        {
            byte[] key = new byte[len];
            for (int ii = 0; ii < key.Length; ii++)
            {
                key[ii] = (byte)((startingValue + ii) % 256);
            }

            return key;
        }

        static IEnumerable<string> Split(string str, int chunkSize)
        {
            return Enumerable.Range(0, str.Length / chunkSize)
                .Select(i => str.Substring(i * chunkSize, chunkSize));
        }


        public static void Main()
        {
            const int blockSize = 16;
            byte[] IV = CreateKey(16, 125);
            printByteArr(IV, "IV");

            byte[] key;
            key = CreateKey(32);
            printByteArr(key, "key");

            //String plainText = "Lieutenant Colonel Anthony Gale had grown weary of the pits of Hades. He was tired of pummeling demons in a drunken stupor. After 174 years even that became tiresome. He was ornery and itching for a real fight. The last minor fiend he had pummeled into submission begged him to stop, offered him information.";
            String plainText = "Here is some datHere is some datHere is some datHere is some datHere is some datHere is some datHere is some dat";
            //String plainText = "Here is some data that needs to be encrypted and decrypted for Stelpro";
            /*IEnumerable<string> plainTextBlocks =  Split(plainText, blockSize);

            byte[] plainTextBytes;
            List<byte[]> encBlocks = new List<byte[]>();
            List<byte[]> decBlocks = new List<byte[]>();

            foreach (string block in plainTextBlocks)
            {

                plainTextBytes = Encoding.ASCII.GetBytes(block);
                printByteArr(plainTextBytes, "plainTextBytes");

                byte[] cipherBytes = EncryptBytes(plainTextBytes, key, IV);
                Console.WriteLine("cipherBytes Len = {0}", cipherBytes.Length);
                printByteArr(cipherBytes, "cipherBytes");

                byte [] decodedBytes = Encoding.ASCII.GetBytes(DecryptBytes(cipherBytes, key, IV));
                printByteArr(decodedBytes, "decBytes");
                Console.WriteLine("{0}", Encoding.ASCII.GetString(decodedBytes));

                encBlocks.Add(cipherBytes);

            }*/

            
            Console.WriteLine("number of bytes in this message = {0}", plainText.Length);
            byte [] plainTextBytes = Encoding.ASCII.GetBytes(plainText);
            Console.WriteLine("plainTextBytes = ");
            printByteArr(plainTextBytes);


            byte[] encBytes = EncryptBytes(plainTextBytes, key, IV);
            printByteArr(encBytes, "encBytes");
    
            byte[] decBytes = DecryptBytes(encBytes, key, IV);
            printByteArr(decBytes, "decBytes");
            Console.WriteLine( Encoding.ASCII.GetString( decBytes ) );

            Console.WriteLine();

            //var name = Console.ReadLine();
        }
  
        static byte[] EncryptBytes(byte[] plainBytes, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (plainBytes == null || plainBytes.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted ;

            string plainText = Encoding.ASCII.GetString(plainBytes);

            // Create an Aes object
            // with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {

                aesAlg.Key = Key;
                aesAlg.IV = IV;
                aesAlg.Padding = PaddingMode.None;
                aesAlg.BlockSize = 128;
                aesAlg.Mode = CipherMode.ECB;

                // Create a decryptor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        Console.WriteLine("plainBytes.Length = {0}", plainBytes.Length);
                        csEncrypt.Write(plainBytes, 0, plainBytes.Length);
                        csEncrypt.FlushFinalBlock();
                        Console.WriteLine("msEncrypt.Length = {0}", msEncrypt.Length);
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            // Return the encrypted bytes from the memory stream.
            return encrypted;

        }
        
        static byte[] DecryptBytes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            // Declare the string used to hold
            // the decrypted text.
            byte[] plainTextArray = null;

            // Create an Aes object
            // with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;
                aesAlg.BlockSize = 128;
                aesAlg.Padding = PaddingMode.None;
                aesAlg.Mode = CipherMode.ECB;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        Console.WriteLine("cipherBytes.Length = {0}", cipherText.Length);
                        csDecrypt.Read(cipherText, 0, cipherText.Length);
                        plainTextArray = msDecrypt.ToArray();
                    }
                }

            }

            return plainTextArray;

        }
    }
}