/*******************************************************************************
* @file    TriacMngmt_200_000069.h
* @date    2018/04/05
* @authors J-F. Simard
* @brief 
*******************************************************************************/
#ifndef TriacMngmt_200_000069
#define TriacMngmt_200_000069
/*******************************************************************************
* Includes
*******************************************************************************/
//#include "standard_includes.h"

/*******************************************************************************
* Public constants definitions
*******************************************************************************/

#define HEATING_PWM_TIMER_BASE_CLOCK_FREQUENCY      16000000
#define HEATING_PWM_RESOLUTION_FREQUENCY            2000000
#define HEATING_PWM_TIMER_PRESCALER                 \
                                ((HEATING_PWM_TIMER_BASE_CLOCK_FREQUENCY/ \
                                    HEATING_PWM_RESOLUTION_FREQUENCY)-1)
#define HEATING_PWM_TIMER_BASE_PERIOD_MS            16.666
#define HEATING_PWM_PULSE_WIDTH_MS                  2.00     //value in ms
#define HEATING_PWM_PULSE_SHIFT                     900       //value in us


/*******************************************************************************
* Public structures definitions
*******************************************************************************/
/*******************************************************************************
* Public variables declarations
*******************************************************************************/
/*******************************************************************************
* Public functions declarations
*******************************************************************************/

#endif /* TriacMngmt_200_000069 */
/** Copyright(C) 2018 Stelpro Design, All Rights Reserved**/
