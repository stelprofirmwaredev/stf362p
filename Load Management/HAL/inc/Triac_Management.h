/*******************************************************************************
* @file    Heat_Management.h
* @date    2018/04/05
* @authors J-F. Simard
* @brief 
*******************************************************************************/
#ifndef Triac_Management_H
#define Triac_Management_H
/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>
//#include "standard_includes.h"

/*******************************************************************************
* Public constants definitions
*******************************************************************************/
/* Driver options */
//#define USE_HEATMNGMT_TRACE_BUFFER

#define TRACE_BUFFER_SIZE                           100

#define EVENT_Q_SIZE                                10

typedef enum
{
    LV_NO_EVENT = 0,
    LV_EVENT_START_HEATING,
    LV_EVENT_STOP_HEATING,
    LV_EVENT_START_TRIGGER_DETECTED,
    LV_EVENT_TIMER_PERIOD_UPDATE,
    LV_EVENT_TRIAC_QIII_PULSE_COMPLETED,
    
    LV_LAST_EVENT,
} line_volt_mngnt_events_t;

typedef enum
{
    LV_STATUS_OK = 0,
    LV_STATUS_ERROR,
} line_volt_status_t;


/*******************************************************************************
* Public structures definitions
*******************************************************************************/
/*******************************************************************************
* Public variables declarations
*******************************************************************************/
/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void Init_Heating_Outputs(void);
void ActivateHeatingDevice(void);
void DeactivateHeatingDevice(void);


#endif /* Triac_Management_H */
/** Copyright(C) 2018 Stelpro Design, All Rights Reserved**/
