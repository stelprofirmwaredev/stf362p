/*******************************************************************************
* @file    HAL_LoadManagement.h
* @date    2017/01/04
* @authors Jean-Fran�ois Many
* @brief
*******************************************************************************/

#ifndef HAL_LoadManagement_H
#define HAL_LoadManagement_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include "typedef.h"
#include "common_types.h"

/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define HEAT_UNLOCKED           0
#define HEAT_LOCKED             1
#define HEAT_RELEASE            2
#define HEAT_PRIORITY           3
#define HEAT_CANCEL_PRIORITY    4

/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void LoadManagement_Init(void);
void LoadManagement_StartNewCycle(uint8_t apply_random_delay);
void LoadManagement_StartNewDay(void);
void HAL_ZeroCrossingManagement(void);
void LoadManagement_SetOutputState(ControlHeatState_e state, U8BIT priority);
uint8_t LoadManagement_ValidateZeroCrossing(void);
U8BIT Get_OutputDemand(void);
void Set_OutputDemand(U8BIT state, U8BIT priority);
uint32_t Get_VcapDropFrequency(void);
uint8_t Get_DetectedVoltage(void);

#endif /* HAL_LoadManagement_H */
/** Copyright(C) 2017 Stelpro Design, All Rights Reserved**/
