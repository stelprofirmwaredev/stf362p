/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2018, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/
/*******************************************************************************
* @file    HeatManagement_Peripherals.c
* @date    2018/04/05
* @authors J-F. Simard
* @brief   Code designed for use with STM32F429 in Maestro CTL configuration.
*******************************************************************************/
/*******************************************************************************
*    Includes
*******************************************************************************/
//#include "standard_includes.h"
#include "typedef.h"
#include "stm32f4xx_hal.h"
#include ".\HAL\inc\TriacMngmt_200_000068.h"

#include ".\HAL\inc\Triac_Management.h"
//#include "stm32l0xx_ll_gpio.h"
//#include "stm32l0xx_ll_tim.h"
//#include "stm32l0xx_ll_comp.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define Triac_QIII_Pin                              GPIO_PIN_12
#define Triac_QIII_GPIO_Port                        GPIOD
#define Triac_QI_Pin                                GPIO_PIN_13
#define Triac_QI_GPIO_Port                          GPIOD
#define Triac_QXXX_GPIO_PORT                        GPIOD
#define ZC_Detect_Pin                               GPIO_PIN_0
#define ZC_Detect_GPIO_Port                         GPIOA
//#define VCap_Pin                                    GPIO_PIN_1
//#define VCap_GPIO_Port                              GPIOG


#define ZERO_CROSSING_TIMER                 TIM2
#define ZERO_CROSSING_TIMER_IRQ             TIM2_IRQn

#define PWM_TRIAC_TIMER                     TIM4
#define PWM_TRIAC_TIMER_IRQ                 TIM4_IRQn
#define PWM_TRIAC_TIMER_QI_CHANNEL          TIM_CHANNEL_2
#define PWM_TRIAC_TIMER_QIII_CHANNEL        TIM_CHANNEL_1

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
TIM_HandleTypeDef zc_timer;
TIM_HandleTypeDef pwm_timer;

#define SET_QI_PULSE(x)                     __HAL_TIM_SET_COMPARE(&pwm_timer, PWM_TRIAC_TIMER_QI_CHANNEL,x)
#define SET_QIII_PULSE(x)                   __HAL_TIM_SET_COMPARE(&pwm_timer, PWM_TRIAC_TIMER_QIII_CHANNEL,x)

#define DEFAULT_CYCLE_LENGTH                16666               //16.6ms based on a 0.5us base timer

/*  Minimum cycle length must take into account the natural drift on the HSI */
#define MIN_CYCLE_LENGTH                    15000               //15ms based on a 0.5us base timer
/*  Maximum cycle length takes into account a "possible" 50Hz power source */
#define MAX_CYCLE_LENGTH                    22000               //22 ms based on a 0.5us base timer
/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
extern void post_lv_events(line_volt_mngnt_events_t line_volt_event);
void setup_io_idle_state(void);
void update_pwm_period(uint16_t tcnt_value, uint8_t last_pulse);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void reset_pwm_timer_registers(void)
{
    HAL_NVIC_DisableIRQ(PWM_TRIAC_TIMER_IRQ);

//    HAL_TIM_PWM_DeInit(&pwm_timer);
    __HAL_RCC_TIM4_FORCE_RESET();
    __HAL_RCC_TIM4_RELEASE_RESET();
}


////////////////////////////////////////////////////////////////////////////////
///                         Zero crossing timer                               //
////////////////////////////////////////////////////////////////////////////////
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
uint16_t get_cycle_length(void)
{
    static uint16_t cycle_length = DEFAULT_CYCLE_LENGTH;
    uint16_t instant_cycle_length = HAL_TIM_ReadCapturedValue(&zc_timer, TIM_CHANNEL_1);
    
    if (instant_cycle_length > MIN_CYCLE_LENGTH)
    {
        if (instant_cycle_length < MAX_CYCLE_LENGTH)
        {
            cycle_length = instant_cycle_length;
        }
    }
    return cycle_length;    
}

/*******************************************************************************
* @brief  The zero crossing timer is used to measure the 60Hz period length
*           from the MCU HClck frequency.
*         - The timer is set in Up count mode
*         - The timer resolution is set to 1us (1MHz)
*         - 1 input capture input is needed to capture the timer value on
*           a ZC signal falling edge
*         - Upon a falling edge capture event, the timer is reset and start
*           counting up again.
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void setup_zc_timer(void)
{
    zc_timer.Instance = TIM2;

    /*************************************************/
    /*          Set timer in up count mode           */
    /*************************************************/
    zc_timer.Init.CounterMode = TIM_COUNTERMODE_UP;

    /* TIM2 clock = CK_INT = APB1 = 90MHZ
            - / 88 = 1MHZ
            - prescale = 88 => 1MHz      */
    zc_timer.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    zc_timer.Init.Prescaler = 88;
    zc_timer.Init.Period = -1;
    zc_timer.Init.RepetitionCounter = 0;
    HAL_TIM_IC_Init(&zc_timer);

    /*************************************************/
    /*              Capture falling edge             */
    /*************************************************/
    {
        TIM_IC_InitTypeDef timer_ic_init;

        timer_ic_init.ICPolarity = TIM_ICPOLARITY_FALLING;
        timer_ic_init.ICFilter = 0;
        timer_ic_init.ICSelection = TIM_ICSELECTION_DIRECTTI;
        timer_ic_init.ICPrescaler = TIM_ICPSC_DIV1;

        HAL_TIM_IC_ConfigChannel(&zc_timer,&timer_ic_init, TIM_CHANNEL_1);
    }
    {
        GPIO_InitTypeDef GPIO_InitStruct;

        GPIO_InitStruct.Pin = ZC_Detect_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Alternate = GPIO_AF1_TIM2;

        HAL_GPIO_Init(ZC_Detect_GPIO_Port, &GPIO_InitStruct);
    }

    /*************************************************/
    /*          Trigger input in reset mode          */
    /*************************************************/
    {
        TIM_SlaveConfigTypeDef slave_config;

        slave_config.InputTrigger = TIM_TS_TI1FP1;
        slave_config.SlaveMode = TIM_SLAVEMODE_RESET;
        slave_config.TriggerFilter = 0;
        slave_config.TriggerPolarity = TIM_TRIGGERPOLARITY_FALLING;
        slave_config.TriggerPrescaler = TIM_TRIGGERPRESCALER_DIV1;

        HAL_TIM_SlaveConfigSynchronization(&zc_timer, &slave_config);
    }

    /*************************************************/
    /*                  Start timer                  */
    /*************************************************/
    HAL_TIM_IC_Start(&zc_timer, TIM_CHANNEL_1);
}



////////////////////////////////////////////////////////////////////////////////
///                             PWM setup                                     //
////////////////////////////////////////////////////////////////////////////////
/*******************************************************************************
* @brief  The pwm timer in up count mode is used to generate the last pulse
*           of the triac sequence on TRIAC_QI
*         - The timer is set in Up count mode
*         - The timer resolution is set to 1us (1MHz)
*
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void setup_pwm_timer_in_up_counter_mode(void)
{
    reset_pwm_timer_registers();

    pwm_timer.Instance = TIM4;

    pwm_timer.Init.CounterMode = TIM_COUNTERMODE_UP;

    /* TIM4 clock = CK_INT = APB1 = 90MHZ
            - / 88 = 1MHZ
            - prescale = 88 => 1MHz      */
    pwm_timer.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    pwm_timer.Init.Prescaler = 88;
    pwm_timer.Init.Period = -1;
    pwm_timer.Init.RepetitionCounter = 0;

    HAL_TIM_PWM_Init(&pwm_timer);
}

/*******************************************************************************
* @brief  The pwm timer in up/down mode is used to generate the pwm pulses
*           of the triac sequence on TRIAC_QI and TRIAC_QIII
*         - The timer is set in Up/down count mode
*         - The timer resolution is set to 1us (1MHz)
*
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void setup_pwm_timer_in_up_down_mode(void)
{
    reset_pwm_timer_registers();

    pwm_timer.Instance = TIM4;

    pwm_timer.Init.CounterMode = TIM_COUNTERMODE_CENTERALIGNED3;

    /* TIM4 clock = CK_INT = APB1 = 90MHZ
            - / 88 = 1MHZ
            - prescale = 88 => 1MHz      */
    pwm_timer.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    pwm_timer.Init.Prescaler = 88;
    pwm_timer.Init.Period = -1;
    pwm_timer.Init.RepetitionCounter = 0;

    HAL_TIM_PWM_Init(&pwm_timer);
}

/*******************************************************************************
* @brief  To precisely start the pulses of the triac sequence, the first pulse
*           on TRIAC_QIII must be synchronized with the falling edge of
*           the ZC_Detect signal
*         - PWM timer trigger mode Reset
*
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void setup_input_capture_for_trigger_start(void)
{
    /*************************************************/
    /*  Select trigger input                         */
    /*    - Zero Crossing Timer TRGO : Reset         */
    /*    - no filtering on input                    */
    /*************************************************/
    {
        TIM_MasterConfigTypeDef master_config;

        master_config.MasterOutputTrigger = TIM_TRGO_RESET;
        master_config.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;

        HAL_TIMEx_MasterConfigSynchronization(&zc_timer, &master_config);
        
        __HAL_TIM_SET_CLOCKDIVISION(&zc_timer, TIM_CLOCKDIVISION_DIV1);
        MODIFY_REG(zc_timer.Instance->CCMR1,TIM_CCMR1_IC1F, 0);

    }

    /*************************************************/
    /*          PWM timer trigger mode reset         */
    /*    - PWM Timer TRGI : Zero Crossing Timer TRGO*/
    /*************************************************/
    {
        TIM_SlaveConfigTypeDef slave_config;

        slave_config.InputTrigger = TIM_TS_ITR1;
        slave_config.SlaveMode = TIM_SLAVEMODE_RESET;
        slave_config.TriggerFilter = 0;
        slave_config.TriggerPolarity = TIM_TRIGGERPOLARITY_RISING;
        slave_config.TriggerPrescaler = TIM_TRIGGERPRESCALER_DIV1;

        HAL_TIM_SlaveConfigSynchronization(&pwm_timer, &slave_config);
    }
}

/*******************************************************************************
* @brief  Before the activation of the triac pulses sequence, only TRIAC_QIII
*           is configured as a pwm output.
*         TRIAC_QI will be configure as a pwm output as soon a the synchronization
*           falling edge will be detected.
*         This method is required because of the asynchronous timing of the
*           configuration and the actual synchronization falling edge.  At this
*           very moment of the configuration, the pwm timer up/down mode is
*           started and could run all the way to the top and count down before
*           the synchronization edge occurs.  In this case, we do not want
*           TRIAC_QI to generate a pulse and activate the triac at a unwanted
*           time during the 60Hz cycle
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void setup_io_for_triac_activation_sequence(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    setup_io_idle_state();

    /*  Triac QIII : Alternate function;   */
    GPIO_InitStruct.Pin = Triac_QIII_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Alternate = GPIO_AF2_TIM4;

    HAL_GPIO_Init(Triac_QIII_GPIO_Port, &GPIO_InitStruct);
}


/*******************************************************************************
* @brief  Configuration of the CCRx Compare registers for pwm outputs
*         The configuration is set to generate 500us (or otherwise specified
*           timing) pulse on both TRIAC_QI and TRIAC_QIII output, but at a
*           different time of the pwm period.
*         In reality, the 500us pulses are separated in symmetric 250us pulse
*           appearing at the bottom and top of up/down counter.
*         - TRIAC_QIII : pulse high 0-0.250ms
*         - TRIAC_QI : pulse high 8.083-8.333ms
*
*         Configuration of the PWM timer ARR corresponding to the last
*         60Hz measured period by the Zero crossing timer
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void update_pwm_period(uint16_t tcnt_value, uint8_t last_pulse)
{
    uint16_t _pulse_width;
    _pulse_width = (uint16_t)((HEATING_PWM_PULSE_WIDTH_MS*get_cycle_length()) /
                                    HEATING_PWM_TIMER_BASE_PERIOD_MS);

    if (last_pulse)
    {
        SET_QI_PULSE((tcnt_value+HEATING_PWM_PULSE_SHIFT) - (_pulse_width));
    }
    else
    {
        SET_QI_PULSE((tcnt_value+HEATING_PWM_PULSE_SHIFT) - (_pulse_width/2));
    }
    SET_QIII_PULSE(_pulse_width);

    __HAL_TIM_SET_AUTORELOAD(&pwm_timer, tcnt_value+HEATING_PWM_PULSE_SHIFT);
}

/*******************************************************************************
* @brief  Configuration of the PWM mode (signal polarity) of each
*           TRIAC_QXXX channel
*           - TRIAC_QI : low level below CCRx value
*           - TRIAC_QIII : high level below CCRx value
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void setup_pwm_output_for_triac_sequence(void)
{
    TIM_OC_InitTypeDef oc_config;

    oc_config.OCMode = TIM_OCMODE_PWM1;
    oc_config.Pulse = 0;
    oc_config.OCPolarity = TIM_OCPOLARITY_HIGH;
    oc_config.OCNPolarity = TIM_OCNPOLARITY_HIGH;
    oc_config.OCFastMode = TIM_OCFAST_DISABLE;
    oc_config.OCIdleState = TIM_OCIDLESTATE_RESET;
    oc_config.OCNIdleState = TIM_OCIDLESTATE_RESET;

    HAL_TIM_OC_ConfigChannel(&pwm_timer, &oc_config,PWM_TRIAC_TIMER_QIII_CHANNEL);

    oc_config.OCMode = TIM_OCMODE_PWM2;
    HAL_TIM_OC_ConfigChannel(&pwm_timer, &oc_config,PWM_TRIAC_TIMER_QI_CHANNEL);

    update_pwm_period(-1,0);
}

/*******************************************************************************
* @brief  GPIOs used for the PWMs signal are defaulted to
*       - Output push-pull
*       - no pull-up/pull-down
*       - no alternate
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void setup_io_idle_state(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    GPIO_InitStruct.Pin = Triac_QIII_Pin | Triac_QI_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Pull = GPIO_NOPULL;

    HAL_GPIO_WritePin(Triac_QI_GPIO_Port, Triac_QIII_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(Triac_QIII_GPIO_Port, Triac_QI_Pin, GPIO_PIN_RESET);
    HAL_GPIO_Init(Triac_QXXX_GPIO_PORT, &GPIO_InitStruct);
}

/*******************************************************************************
* @brief  The last pulse is a pulse on TRIAC_QI that is required to balance
*           the current (dcA) in a capacitor (see hardware design)
*         In order to avoid the latching of the triac for another half-cycle,
*           the last pulse is configured to happen inside the current half-cycle,
*
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void set_last_pulse(void)
{
    reset_pwm_timer_registers();
    setup_io_idle_state();

    /* setup timer in upcount; 1/4 cyle lenght */
    setup_pwm_timer_in_up_counter_mode();
    setup_pwm_output_for_triac_sequence();
    update_pwm_period(get_cycle_length()/4,1);

    /* setup TRIAC_QI IO in alternate Timer CCx */
    {
        GPIO_InitTypeDef GPIO_InitStruct;

        /*  Triac QI : Alternate function;   */
        GPIO_InitStruct.Pin = Triac_QI_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Alternate = GPIO_AF2_TIM4;

        HAL_GPIO_Init(Triac_QI_GPIO_Port, &GPIO_InitStruct);
    }

    /* enable update interrupt  */

    HAL_NVIC_EnableIRQ(PWM_TRIAC_TIMER_IRQ);
    HAL_NVIC_SetPriority(PWM_TRIAC_TIMER_IRQ, 5, 0);

    __HAL_TIM_CLEAR_FLAG(&pwm_timer, TIM_IT_UPDATE);
    __HAL_TIM_ENABLE_IT(&pwm_timer, TIM_IT_UPDATE);

    /* start timer  */

    TIM_CCxChannelCmd(pwm_timer.Instance, PWM_TRIAC_TIMER_QI_CHANNEL, TIM_CCx_ENABLE);
    HAL_TIM_Base_Start(&pwm_timer);
}

/*******************************************************************************
* @brief  During the wait for trigger phase, the pwm timer starts counting up
*           from TRIAC_QIII end of pulse (to avoid generating a false pulse)
*           until a falling edge of the ZC_Detect signal.
*         Upon the falling edge of the ZC_Detect signal, the pwm timer count
*           will reset and the TRIAC_QIII half-pulse will be generated.
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void wait_for_trigger(void)
{
    __HAL_TIM_SET_COUNTER(&pwm_timer, __HAL_TIM_GET_COMPARE(&pwm_timer,PWM_TRIAC_TIMER_QIII_CHANNEL)+1);
    TIM_CCxChannelCmd(pwm_timer.Instance, PWM_TRIAC_TIMER_QI_CHANNEL, TIM_CCx_DISABLE);

    HAL_TIM_Base_Start(&pwm_timer);

    HAL_NVIC_EnableIRQ(ZERO_CROSSING_TIMER_IRQ);
    HAL_NVIC_SetPriority(ZERO_CROSSING_TIMER_IRQ, 5, 0);

    __HAL_TIM_CLEAR_FLAG(&zc_timer, TIM_IT_TRIGGER);
    __HAL_TIM_ENABLE_IT(&zc_timer, TIM_IT_TRIGGER);

    /* to avoid a glitch on the QIII output, the compare enable must be done
        after the initialization of TIM->CNT            */

    TIM_CCxChannelCmd(pwm_timer.Instance, PWM_TRIAC_TIMER_QIII_CHANNEL, TIM_CCx_ENABLE);
}

/*******************************************************************************
* @brief  The triac sequence can be stop after an Update event on the PWM timer
*           when the counter have reached the bottom value (0).
*         At this exact moment, the TRIAC_QIII compare interrupt is activated
*           to notify the system that the TRIAC_QIII pulse have been generated
*         Up this notification, the system will configure the last pulse
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void stop_triac_sequence(void)
{
    __HAL_TIM_CLEAR_FLAG(&pwm_timer, TIM_IT_UPDATE);
    __HAL_TIM_CLEAR_FLAG(&pwm_timer, TIM_IT_CC1);

    __HAL_TIM_ENABLE_IT(&pwm_timer, TIM_IT_CC1);
}

/*******************************************************************************
* @brief  Sequential configuration of the different MCU peripheral to begin
*           a triac pulses sequence
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void config_triac_sequence_with_trigger_start(void)
{
    setup_pwm_timer_in_up_down_mode();
    setup_pwm_output_for_triac_sequence();
    setup_io_for_triac_activation_sequence();
    setup_input_capture_for_trigger_start();

    wait_for_trigger();
}

/*******************************************************************************
* @brief  Once the falling edge of ZC_Detect have synchronized the pwm timer,
*           the TRIAC_QI pwm mode can be activated.
*         The pwm timer update interrupt is activated for system event synchronzation
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void start_pulse_sequence(void)
{
    __HAL_TIM_CLEAR_FLAG(&zc_timer, TIM_IT_TRIGGER);
    __HAL_TIM_DISABLE_IT(&zc_timer, TIM_IT_TRIGGER);
    
    /*    - filtering on zc input                    */    
    __HAL_TIM_SET_CLOCKDIVISION(&zc_timer, TIM_CLOCKDIVISION_DIV2);
    MODIFY_REG(zc_timer.Instance->CCMR1,TIM_CCMR1_IC1F, TIM_CCMR1_IC1F);
    
    /*  Triac QI : Alternate function; */
    {
        GPIO_InitTypeDef GPIO_InitStruct;

        /*  Triac QI : Alternate function;   */
        GPIO_InitStruct.Pin = Triac_QI_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Alternate = GPIO_AF2_TIM4;

        HAL_GPIO_Init(Triac_QI_GPIO_Port, &GPIO_InitStruct);
    }
        
    /* Set the Autoreload value */
    update_pwm_period(get_cycle_length()/2,0);
    TIM_CCxChannelCmd(pwm_timer.Instance, PWM_TRIAC_TIMER_QI_CHANNEL, TIM_CCx_ENABLE);

    HAL_NVIC_EnableIRQ(PWM_TRIAC_TIMER_IRQ);
    HAL_NVIC_SetPriority(PWM_TRIAC_TIMER_IRQ, 5, 0);

    __HAL_TIM_ENABLE_IT(&pwm_timer, TIM_IT_UPDATE);
}



////////////////////////////////////////////////////////////////////////////////
///                             VCap monitoring                               //
////////////////////////////////////////////////////////////////////////////////

/*******************************************************************************
* @brief
* @inputs None
* @retval State of the VCap signal
*           0 : VCap level is ok; power supply recharge is not needed
*           1 : VCap level is low; power supply recharge is needed
* @author
* @date
*******************************************************************************/
uint8_t isVCapLow(void)
{
    return (0);
//    return (HAL_GPIO_ReadPin(VCap_GPIO_Port,VCap_Pin) == 0);
//    static int8_t cnt = 10;
//
//    if (cnt < 0)
//    {
//        cnt = 10;
//    }
//    return (cnt--==0);
}

///*******************************************************************************
//* @brief
//* @inputs None
//* @retval None
//* @author
//* @date
//*******************************************************************************/
//void init_vcap_monitoring(void)
//{
//    GPIO_InitTypeDef GPIO_InitStruct;
//
//    GPIO_InitStruct.Pin = VCap_Pin;
//    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
//    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
//    GPIO_InitStruct.Pull = GPIO_PULLUP;
//
//    HAL_GPIO_Init(VCap_GPIO_Port, &GPIO_InitStruct);
//}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
uint8_t isTimerUpCounting(void)
{
    return (__HAL_TIM_IS_TIM_COUNTING_DOWN(&pwm_timer) == 0);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
uint8_t isTimerDownCounting(void)
{
    return (__HAL_TIM_IS_TIM_COUNTING_DOWN(&pwm_timer) != 0);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void UpdatePeriod(void)
{
    update_pwm_period(get_cycle_length()/2,0);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void Init_HeatingPeripherals(void)
{
    __HAL_RCC_TIM4_CLK_ENABLE();
    __HAL_RCC_TIM2_CLK_ENABLE();

    setup_zc_timer();
//    init_vcap_monitoring();
}

/*******************************************************************************
*    ISR
*******************************************************************************/
/*******************************************************************************
* @brief  This interrupt is fired at the end of TRIAC_QIII pulse when the
*           system needs to end the triac sequence for either recharging
*           or just to stop the heating
*         From a peripheral perspective, this ISR is linked to the
*           Triac_QIII Compare interrupt
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2018/06/14
*******************************************************************************/
void HAL_TIM_OC_DelayElapsedCallback(TIM_HandleTypeDef *htim)
{
//    if ((PWM_TRIAC_TIMER == htim->Instance) && (PWM_TRIAC_TIMER_QIII_CHANNEL == htim->Channel))
    {
        post_lv_events(LV_EVENT_TRIAC_QIII_PULSE_COMPLETED);
    }
}

/*******************************************************************************
* @brief  This interrupt is fired each time the PWM_TRIAC_TIMER reaches the
*           top or the bottom of its counter (in up/down mode)
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2018/04/10
*******************************************************************************/
//void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
//{
//    if (PWM_TRIAC_TIMER == htim->Instance)
//    {
//        post_lv_events(LV_EVENT_TIMER_PERIOD_UPDATE);
//    }
//}
void PwmTimer_PeriodUpdate(void)
{
    post_lv_events(LV_EVENT_TIMER_PERIOD_UPDATE);
}

/*******************************************************************************
* @brief  This interrupt is fired once at the beginning of the heating
*           sequence, on the falling edge of the Zero crossing signal.
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2018/04/10
*******************************************************************************/
void HAL_TIM_TriggerCallback(TIM_HandleTypeDef *htim)
{
    if (ZERO_CROSSING_TIMER == htim->Instance)
    {
        post_lv_events(LV_EVENT_START_TRIGGER_DETECTED);
    }
}

/** Copyright(C) 2018 Stelpro Design, All Rights Reserved**/
