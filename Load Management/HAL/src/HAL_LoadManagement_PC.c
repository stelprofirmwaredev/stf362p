/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2017, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    HAL_LoadManagement.c
* @date    2017/01/04
* @authors Jean-Fran�ois Many
* @brief   Load Management Module
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include "typedef.h"
#include "stm32f4xx_hal.h"
#include "HAL_Interrupt.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\Sensors\inc\THREAD_Sensors.h"
#include ".\HAL\inc\HAL_LoadManagement_PC.h"
#include ".\HAL\inc\TemperatureControl.h"
#include "common_types.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
//Control parameters
typedef struct
{
    int8u PercentMin;           //  PercentMin : Minimum allowed percent to avoid short ON cycle
    int8u PercentMax;           //  PercentMax : Maximum allowed percent to avoid short OFF cycle
    int16u onePercent;          //  OnePercent : number of os tick for 1 percent increment
} CycleParam_t;

//Medium cycle 1 percent increment = 15 minutes * 60 sec/min * 1000 ms/sec / 100 (every 3 seconds)
#define ONE_PERCENT_LONG_CYCLE        (15*60*1000L/100)

//Medium cycle 1 percent increment = 5 minutes * 60 sec/min * 1000 ms/sec / 100 (every 3 seconds)
#define ONE_PERCENT_MEDIUM_CYCLE        (5*60*1000L/100)

//Short cycle 1 percent increment = 15 seconds * 1000 ms/sec / 100 (every 150 miliseconds)
#define ONE_PERCENT_SHORT_CYCLE         (15*1000/100)

#define ONE_HOUR_IN_MS                  (3600000)   //(1000 * 60 * 60)

#ifdef LOAD_CYCLING
    #define LOAD_CYCLING_COUNT_OVERLOAD             50
    #define LOAD_CYCLING_COUNT_ENDURANCE_110_PCT    6000
    #define LOAD_CYCLING_COUNT_ENDURANCE_RATED      30000
#endif

#define TOGGLE_RELAY_CMD()          HAL_GPIO_TogglePin(HEAT_CMD_Port, HEAT_CMD_Pin)
#define RESET_RELAY_CMD()           HAL_GPIO_WritePin(HEAT_CMD_Port, HEAT_CMD_Pin, GPIO_PIN_RESET)

#define HEAT_CMD_PULSE_PERIOD       100
#define GFCI_CMD_PULSE_PERIOD       20
#define LISTEN_INTERVAL_PERIOD      1000
#define END_OF_LIFE_PULSE_PERIOD    500
#define END_OF_LIFE_INITIAL_PERIOD  750
#define END_OF_LIFE_DETECTION_COUNT 20
#define GFCI_DETECTION_COUNT        20
#define DRIVE_PULSE_MAX             10
#define GFCI_DETECT_SENSITIVITY     4
#define GFCI_REPORT_DELAY           100

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
//Control parameters for Triac with short cycle
static CycleParam_t const * param;            //pointer to estimate parameters in flash
static const CycleParam_t CycleParamsRelayCycle =    /* Relay Base Cycles */
{
    5,                                              //  PercentMin : Minimum allowed percent to avoid short ON cycle
    95,                                             //  PercentMax : Maximum allowed percent to avoid short OFF cycle
    ONE_PERCENT_LONG_CYCLE,                         //  OnePercent : number of os tick for 1 percent increment
};

static uint8_t Cycle;          //Cycle Percent
static uint8_t CycleStatus;    // 1 => output has cycled ON-OFF
static ControlHeatState_e OutputState;    //Output State
static uint16_t CycleOnTime;    //Triac On time in ms for 1 cycle
static TimerHandle_t CycleTimeout = NULL;
static U8BIT heatDemandState = FALSE;
static uint32_t RandomStart = 0;

#ifdef FLOOR
static TimerHandle_t HeatPulseTimeout = NULL;
static TimerHandle_t ExtensionTimeout = NULL;
static TimerHandle_t EndOfLifeTimeout = NULL;
static TimerHandle_t GFCIReportTimeout = NULL;
static uint8_t end_of_life_sequence = 0;
static uint8_t NbOfDrivePulses = 0;
static uint8_t LocalGFCI = 0;
static uint8_t LocalEoL = 0;
static uint8_t RemoteGFCI = 0;
static uint8_t RemoteEoL = 0;
#endif

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
extern dbType_Residence_t           CurrentResidence;
TIM_HandleTypeDef htim5;
RNG_HandleTypeDef hrng;

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static void LoadManagement_DoCycle(TimerHandle_t xTimer);
static void LoadLimitMinMax(int8u * value, int8u min, int8u max);
static void TIM5_Init(void);
static void TurnOnHeatOutput(void);
static void TurnOffHeatOutput(void);
#ifdef FLOOR
static void heat_cmd_pulses(TimerHandle_t xTimer);
static void extension_feedback_monitoring(TimerHandle_t xTimer);
static void end_of_life_monitoring(TimerHandle_t xTimer);
static void GFCIAlertReport(TimerHandle_t xTimer);
static void Init_GFCIFaultMonitoring(void);
static void Init_ErrorMonitoring(void);
static void SetLocalGFCI(void);
static void ClearLocalGFCI(void);
static void SetLocalEndOfLife(void);
static void ClearLocalEndOfLife(void);
static void SetFloorExtensionGFCIAlert(void);
static void ClearFloorExtensionGFCIAlert(void);
static void SetFloorExtensionEndOfLifeAlert(void);
static void ClearFloorExtensionEndOfLifeAlert(void);
#endif

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
#ifdef FLOOR
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void heat_cmd_pulses(TimerHandle_t xTimer)
{
    NbOfDrivePulses++;
    if (NbOfDrivePulses < DRIVE_PULSE_MAX)
    {
        TOGGLE_RELAY_CMD();
        xTimerStop(HeatPulseTimeout, 0);
        xTimerChangePeriod(HeatPulseTimeout, HEAT_CMD_PULSE_PERIOD, 0);
        xTimerStart(HeatPulseTimeout, 0);
    }
    else
    {
        TurnOffHeatOutput();
        xTimerStop(HeatPulseTimeout, 0);
        xTimerChangePeriod(HeatPulseTimeout, LISTEN_INTERVAL_PERIOD, 0);
        xTimerStart(HeatPulseTimeout, 0);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void extension_feedback_monitoring(TimerHandle_t xTimer)
{
    static uint8_t end_of_life_detect = 0;
    static uint8_t gfci_detect = 0;
    static uint8_t previous_feedback = 0;
    uint8_t feedback;

    //If we are in the error reporting window
    if (NbOfDrivePulses == 0)
    {
        feedback = HAL_GPIO_ReadPin(Feedback_Port, Feedback_Pin);
        //Heat command should be off, and we expect a low level on the feedback
        if (feedback != 0)
        {
            //Something is wrong, it could be a GFCI fault or an End of Life from an extention thermostat

            //Check if the feedback line state has changed
            if (previous_feedback != feedback)
            {
                if (gfci_detect < GFCI_DETECTION_COUNT)
                {
                    gfci_detect += GFCI_DETECT_SENSITIVITY;
                }
                if (gfci_detect >= GFCI_DETECTION_COUNT)
                {
                    SetFloorExtensionGFCIAlert();
                }
            }
            else
            {
                //Line is still down, one step closer to detect an end of life...
                if (end_of_life_detect < END_OF_LIFE_DETECTION_COUNT)
                {
                    end_of_life_detect++;
                }
                if (end_of_life_detect >=  END_OF_LIFE_DETECTION_COUNT)
                {
                    //End of life detected on one extention
                    SetFloorExtensionEndOfLifeAlert();
                }
            }
        }
        else
        {
            if (end_of_life_detect != 0)
            {
                end_of_life_detect--;
                if (end_of_life_detect == 0)
                {
                    ClearFloorExtensionEndOfLifeAlert();
                }
            }

            if (gfci_detect != 0)
            {
                gfci_detect--;
                if (gfci_detect == 0)
                {
                    ClearFloorExtensionGFCIAlert();
                }
            }
        }
        previous_feedback = feedback;
        xTimerReset(ExtensionTimeout, 0);
    }
    else
    {
        //Nothing to do
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void end_of_life_monitoring(TimerHandle_t xTimer)
{
    if ((end_of_life_sequence % 2) == 0)
    {
        if (0 == HAL_GPIO_ReadPin(GFCI_Error_Port, GFCI_Error_Pin))
        {
            //Ok, so the error signal rise then dropped, let see if it rise again to declare an End of Life
            xTimerStop(EndOfLifeTimeout, 0);
            xTimerChangePeriod(EndOfLifeTimeout, END_OF_LIFE_PULSE_PERIOD, 0);
            xTimerReset(EndOfLifeTimeout, 0);
            end_of_life_sequence++;
        }
        else
        {
            //False alert (GFCI followed by a reset)
            ClearLocalEndOfLife();
            end_of_life_sequence = 0;
        }
    }
    else if ((end_of_life_sequence % 2) != 0)
    {
        if (0 != HAL_GPIO_ReadPin(GFCI_Error_Port, GFCI_Error_Pin))
        {
            //That's it! It's an End Of Life
            SetLocalEndOfLife();
            //...But let's continue to monitor it in case someone is playing with the buttons...
            //Until the counter reached its maximum
            if (end_of_life_sequence < 0xFF)
            {
                xTimerStop(EndOfLifeTimeout, 0);
                xTimerChangePeriod(EndOfLifeTimeout, END_OF_LIFE_PULSE_PERIOD, 0);
                xTimerReset(EndOfLifeTimeout, 0);
                end_of_life_sequence++;
            }
        }
        else
        {
            //False alert (GFCI followed by a reset)
            ClearLocalEndOfLife();
            end_of_life_sequence = 0;
        }
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void Init_GFCIFaultMonitoring(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    __HAL_RCC_GPIOG_CLK_ENABLE();

    GPIO_InitStruct.Pin = GFCI_FAULT_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GFCI_FAULT_Port, &GPIO_InitStruct);

    HAL_NVIC_SetPriority(EXTI1_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(EXTI1_IRQn);

    __HAL_GPIO_EXTI_CLEAR_FLAG(GFCI_FAULT_Pin);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void Init_ErrorMonitoring(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    __HAL_RCC_GPIOD_CLK_ENABLE();

    GPIO_InitStruct.Pin = GFCI_Error_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GFCI_Error_Port, &GPIO_InitStruct);

    HAL_NVIC_SetPriority(EXTI9_5_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

    __HAL_GPIO_EXTI_CLEAR_FLAG(GFCI_Error_Pin);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void GFCIAlertReport(TimerHandle_t xTimer)
{
    uint8_t GFCI_Alert;

    if ((LocalGFCI != 0) || (RemoteGFCI != 0))
    {
        GFCI_Alert = DB_SET_ALERT | DBALERT_GFCI;
    }
    else
    {
        GFCI_Alert = DB_CLEAR_ALERT | DBALERT_GFCI;
    }
    DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&GFCI_Alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
}

/*******************************************************************************
* @brief GFCI Interrupt Callback
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void GFCIFaultDetection_Callback(void)
{
    if (0 == HAL_GPIO_ReadPin(GFCI_FAULT_Port, GFCI_FAULT_Pin))
    {
        ClearLocalGFCI();
    }
    else
    {
        SetLocalGFCI();
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void ErrorDetection_Callback(void)
{
    if (0 != HAL_GPIO_ReadPin(GFCI_Error_Port, GFCI_Error_Pin))
    {
        //Error is set, it is either a GFCI fault or an End of Life
        //Check again in 750ms to see the pin state
        //If the signal dropped, it's because it's an End of Life
        if (end_of_life_sequence == 0)
        {
            xTimerStopFromISR(EndOfLifeTimeout, 0);
            xTimerChangePeriodFromISR(EndOfLifeTimeout, END_OF_LIFE_INITIAL_PERIOD, 0);
            xTimerResetFromISR(EndOfLifeTimeout, 0);
        }
    }
}
#endif /* FLOOR */

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void TurnOnHeatOutput(void)
{
    NbOfDrivePulses = 0;
    heat_cmd_pulses(0);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void TurnOffHeatOutput(void)
{
    NbOfDrivePulses = 0;
    RESET_RELAY_CMD();

    xTimerStop(HeatPulseTimeout, 0);
    xTimerReset(ExtensionTimeout, 0);
}

#ifdef LOAD_CYCLING
/*******************************************************************************
*   \function : void LoadManagement_DoCycle(TimerHandle_t xTimer)
*	\brief :    Cycle management
*	\param :    None
*	\return :   None
********************************************************************************/
#pragma optimize = none
static void LoadManagement_DoCycle(TimerHandle_t xTimer)
{
    static int16u TimeCounter;
    uint8_t load_cycling_mode;
    uint16_t load_cycling_count;
    uint16_t on_time;
    uint16_t off_time;
    uint16_t max_cnt;
    static uint8_t last_load_cycling_mode = 0;

    //Call every 500 ms while is OFF
    xTimerChangePeriod(CycleTimeout, pdMS_TO_TICKS(500), 0);

    DBTHRD_GetData(DBTYPE_LOAD_CYCLING_MODE,(void*)&load_cycling_mode, THIS_THERMOSTAT);
    DBTHRD_GetData(DBTYPE_LOAD_CYCLING_COUNT,(void*)&load_cycling_count, THIS_THERMOSTAT);

    if (load_cycling_mode != last_load_cycling_mode)
    {
        TimeCounter = 0;
        load_cycling_count = 0;
        DBTHRD_SetData(DBTYPE_LOAD_CYCLING_COUNT, (void*)&load_cycling_count, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    }
    last_load_cycling_mode = load_cycling_mode;

    switch (load_cycling_mode)
    {
        case 0:
            on_time = 0;
            off_time = 0;
            break;

        case 1:
            on_time = 30;
            off_time = 30;
            break;

        case 2:
            on_time = 300;
            off_time = 300;
            break;

        case 3:
            on_time = 300;
            off_time = 300;
            break;

        default:
            on_time = 0;
            off_time = 0;
            break;
    }

    BSP_InterruptDeclareCritical();
    BSP_InterruptEnterCritical();

    if(on_time)                  //There is ON Time programmed
    {
        if(off_time == 0)        //No OFF time = Always ON
        {
            Set_OutputDemand(TRUE, HEAT_UNLOCKED);
            LoadManagement_SetOutputState(CONTROL_HEAT_STATE_ON, HEAT_UNLOCKED);
            CTL_SetHeatDemand(100);
        }
        else if(TimeCounter < off_time)   //OFF Time
        {
            Set_OutputDemand(FALSE, HEAT_UNLOCKED);
            LoadManagement_SetOutputState(CONTROL_HEAT_STATE_OFF, HEAT_UNLOCKED);
            CTL_SetHeatDemand(0);
        }
        else                            //ON Time
        {
            Set_OutputDemand(TRUE, HEAT_UNLOCKED);
            LoadManagement_SetOutputState(CONTROL_HEAT_STATE_ON, HEAT_UNLOCKED);
            CTL_SetHeatDemand(100);
        }
        TimeCounter += 5;

        if(TimeCounter >= on_time + off_time)
        {
            TimeCounter = 0;
            load_cycling_count++;
            DBTHRD_SetData(DBTYPE_LOAD_CYCLING_COUNT, (void*)&load_cycling_count, THIS_THERMOSTAT, DBCHANGE_LOCAL);
        }
    }
    else
    {
        //No OnTime programmed - remain OFF
        TimeCounter = 0;
        Set_OutputDemand(FALSE, HEAT_UNLOCKED);
        LoadManagement_SetOutputState(CONTROL_HEAT_STATE_OFF, HEAT_UNLOCKED);
        CTL_SetHeatDemand(0);
    }

    switch(load_cycling_mode)
    {
        case 0:
            //No test started
            load_cycling_count = 0;
            DBTHRD_SetData(DBTYPE_LOAD_CYCLING_COUNT, (void*)&load_cycling_count, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            CTL_SetHeatDemand(0);
            break;

        case 1:
            //Overload test
            max_cnt = LOAD_CYCLING_COUNT_OVERLOAD;
            if (load_cycling_count >= max_cnt)
            {
                CTL_SetHeatDemand(0);
                load_cycling_mode = 0;
                DBTHRD_SetData(DBTYPE_LOAD_CYCLING_MODE, (void*)&load_cycling_mode, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            }
            break;

        case 2:
            //Endurance test (110%)
            max_cnt = LOAD_CYCLING_COUNT_ENDURANCE_110_PCT;
            if (load_cycling_count >= max_cnt)
            {
                CTL_SetHeatDemand(0);
                load_cycling_mode = 0;
                DBTHRD_SetData(DBTYPE_LOAD_CYCLING_MODE, (void*)&load_cycling_mode, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            }
            break;

        case 3:
            //Endurance test (nominal)
            max_cnt = LOAD_CYCLING_COUNT_ENDURANCE_RATED;
            if (load_cycling_count >= max_cnt)
            {
                CTL_SetHeatDemand(0);
                load_cycling_mode = 0;
                DBTHRD_SetData(DBTYPE_LOAD_CYCLING_MODE, (void*)&load_cycling_mode, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            }
            break;

        default:
            break;
    }

    // Leave critical
    BSP_InterruptExitCritical();
}
#elif defined (ON_OFF)
#pragma optimize = none
static void LoadManagement_DoCycle(TimerHandle_t xTimer)
{
    //Call every 500 ms while is OFF
    xTimerChangePeriod(CycleTimeout, pdMS_TO_TICKS(500), 0);

    BSP_InterruptDeclareCritical();
    BSP_InterruptEnterCritical();
    if (ForceOnState)
    {
        Set_OutputDemand(TRUE, HEAT_UNLOCKED);
        LoadManagement_SetOutputState(CONTROL_HEAT_STATE_ON, HEAT_UNLOCKED);
        CTL_SetHeatDemand(100);
    }
    else
    {
        Set_OutputDemand(FALSE, HEAT_UNLOCKED);
        LoadManagement_SetOutputState(CONTROL_HEAT_STATE_OFF, HEAT_UNLOCKED);
        CTL_SetHeatDemand(0);
    }
    // Leave critical
    BSP_InterruptExitCritical();
}

#else

/*******************************************************************************
*   \function : void LoadManagement_DoCycle(TimerHandle_t xTimer)
*	\brief :    Cycle management
*	\param :    None
*	\return :   None
********************************************************************************/
static void LoadManagement_DoCycle(TimerHandle_t xTimer)
{
    uint8_t heatDemand;

    DBTHRD_GetData(DBTYPE_HEAT_DEMAND,(void*)&heatDemand, THIS_THERMOSTAT);
    LoadLimitMinMax(&heatDemand,param->PercentMin,param->PercentMax);
    Cycle += 1;
    if(Cycle >= 100)                                //Restart a new cycle
    {
        //If heat demand = 100%, triac is still on, so set the cycle on time to 100%
        if (heatDemand == 100)
        {
            CycleOnTime = 100;
        }
        Cycle = 0;
        CycleStatus = 0;
        CycleOnTime = 0;
    }

    BSP_InterruptDeclareCritical();
    BSP_InterruptEnterCritical();
    if ((heatDemand > Cycle) && (CycleStatus == 0))
    {
        Set_OutputDemand(TRUE, HEAT_UNLOCKED);
        LoadManagement_SetOutputState(CONTROL_HEAT_STATE_ON, HEAT_UNLOCKED);
    }
    else
    {
        Set_OutputDemand(FALSE, HEAT_UNLOCKED);
        LoadManagement_SetOutputState(CONTROL_HEAT_STATE_OFF, HEAT_UNLOCKED);
        __HAL_TIM_SET_AUTORELOAD(&htim5, 0xFFFFFFFF);

        //Make sure the triac was activated before
        if (CycleOnTime > 0)
        {
            CycleStatus = 1;        //to prevent reactivation of the output until the end of the control cycle
        }
    }
    // Leave critical
    BSP_InterruptExitCritical();
}
#endif /* LOAD_CYCLING / ON_OFF */

/*******************************************************************************
//  \function : void LoadLimitMinMax(int8u * value, int8u min, int8u max){
//	\brief :    Clip value to load minimum and maximum applicable percent
//	\param :    int8u * value = percent requested
//              int8u min = min applicable percent
//              int8u max = max applicable percent
//	\return :   none, modified int8u * value
********************************************************************************/
static void LoadLimitMinMax(int8u * value, int8u min, int8u max)
{
    if(*value < min)                //Below min: 0
    {
        *value = 0;
    }
    else if(*value >= 100)          //Absolute max is 100
    {
        *value = 100;
    }
    else if(*value > max)           //Between max and 100:
    {
        //*value = max;				//use max
        *value = 100;				//use 100
    }
    else
    {
        //otherwise use as is
    }
}

/*******************************************************************************
* @brief  Set the Output desired state
* @inputs state: CONTROL_HEAT_STATE_OFF or CONTROL_HEAT_STATE_ON
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/02/10
*******************************************************************************/
void Set_OutputDemand(U8BIT state, U8BIT priority)
{
    static U8BIT demandLocked = FALSE;

    if (priority == HEAT_PRIORITY)
    {
        demandLocked = TRUE;
    }
    if (priority == HEAT_CANCEL_PRIORITY)
    {
        demandLocked = FALSE;
    }
    if ((demandLocked == FALSE) || (priority == HEAT_PRIORITY))
    {
        if ((heatDemandState == TRUE) && (state == FALSE))
        {
            //End of heating phase, save CycleOnTime
            CycleOnTime = Cycle;
        }

        heatDemandState = state;
    }
}

/*******************************************************************************
* @brief  Timer5 Init Function to sync ZC for Triac activation
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/02/10
*******************************************************************************/
static void TIM5_Init(void)
{
    RCC_ClkInitTypeDef    clkconfig;
    uint32_t              pFLatency;
    uint32_t              uwPrescalerValue = 0;
    uint32_t              uwTimclock = 0;

    /*Configure the TIM5 IRQ priority */
    HAL_NVIC_SetPriority(TIM5_IRQn, 2, 0);

    /* Enable the TIM5 global Interrupt */
    HAL_NVIC_EnableIRQ(TIM5_IRQn);

    /* Enable TIM5 clock */
    __TIM5_CLK_ENABLE();

    /* Get clock configuration */
    HAL_RCC_GetClockConfig(&clkconfig, &pFLatency);

    /* Compute TIM5 clock */
    uwTimclock = HAL_RCC_GetPCLK2Freq();

    /* Compute the prescaler value to have TIM5 counter clock equal to 1MHz */
    uwPrescalerValue = (uint32_t) (uwTimclock / 1000000);

    /* Initialize TIM5 */
    htim5.Instance = TIM5;

    htim5.Init.Period = 0xFFFFFFFF;
    htim5.Init.Prescaler = uwPrescalerValue - 1;
    htim5.Init.ClockDivision = 0;
    htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
    HAL_TIM_Base_Init(&htim5);
    //Start the timer counter
    HAL_TIM_Base_Start(&htim5);
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  Load Management initialization function
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/04
*******************************************************************************/
void LoadManagement_Init(void)
{
    OutputState = CONTROL_HEAT_STATE_OFF;
    Cycle = 0;                      //Cycle Percent
    CycleStatus = 0;                //New cycle
    CycleOnTime = 0;
    CTL_SetHeatDemand(0);           //Applied Percent

    //Enable the Random generator for the random start
    __HAL_RCC_RNG_CLK_ENABLE();
    hrng.Instance = RNG;
    HAL_RNG_Init(&hrng);
    HAL_RNG_GenerateRandomNumber(&hrng, &RandomStart);
    RandomStart = (uint16_t)(15000 * ((float)RandomStart / 0xFFFFFFFF));

    GPIO_InitTypeDef GPIO_InitStruct;

    GPIO_InitStruct.Pin = HEAT_CMD_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(HEAT_CMD_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = Feedback_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(Feedback_Port, &GPIO_InitStruct);

    Init_GFCIFaultMonitoring();
    Init_ErrorMonitoring();

    HeatPulseTimeout = xTimerCreate ("Heat Pulse Timeout",
                                       pdMS_TO_TICKS(HEAT_CMD_PULSE_PERIOD),
                                       pdFALSE,     //one-shot
                                       (void*)0,
                                       heat_cmd_pulses);

    configASSERT(HeatPulseTimeout);

    ExtensionTimeout = xTimerCreate ("Extension Timeout",
                                       pdMS_TO_TICKS(GFCI_CMD_PULSE_PERIOD),
                                       pdFALSE,     //one-shot
                                       (void*)0,
                                       extension_feedback_monitoring);

    configASSERT(ExtensionTimeout);

    EndOfLifeTimeout = xTimerCreate ("End Of Life Timeout",
                                       pdMS_TO_TICKS(END_OF_LIFE_PULSE_PERIOD),
                                       pdFALSE,     //one-shot
                                       (void*)0,
                                       end_of_life_monitoring);

    GFCIReportTimeout = xTimerCreate ("GFCI Report Timeout",
                                       pdMS_TO_TICKS(GFCI_REPORT_DELAY),
                                       pdFALSE,     //one-shot
                                       (void*)0,
                                       GFCIAlertReport);

    configASSERT(EndOfLifeTimeout);

    LoadManagement_StartNewCycle(1);    //start new cycle with random start

    TIM5_Init();
}

/*******************************************************************************
*   \function : void LOAD_SetOutputState(ControlHeatState_e state, U8BIT priority)
*	\brief :    Change the status of the output state
*	\param :    None
*	\return :   None
********************************************************************************/
void LoadManagement_SetOutputState(ControlHeatState_e state, U8BIT priority)
{
    static U8BIT locked = FALSE;
    static U8BIT priorityRequest = FALSE;
    static ControlHeatState_e priorityState = CONTROL_HEAT_STATE_OFF;

    if(state < MAX_CONTROL_HEAT_STATE)
    {
        if (priority == HEAT_PRIORITY)  //Check if a particular state is requested (test mode)
        {
            priorityRequest = TRUE;     //  Remember to return to this state every time we can
            priorityState = state;      //  Save it for later
        }
        if (priority == HEAT_CANCEL_PRIORITY)//End of test mode
        {
            priorityRequest = FALSE;    //  Let the code manage the heat normally
        }
        if (priority == HEAT_RELEASE)   //If normal operation is allowed (Vcap is up), release the lock and apply the requested state, if any
        {
            locked = FALSE;
        }
        if ((locked != TRUE) || (priority == HEAT_LOCKED))
        {
            if ((priorityRequest == TRUE) && (priority != HEAT_LOCKED)) //If no locked state defined AND particular state is requested
            {
                state = priorityState;                                  //  Force return to the requested state
            }
            if ((OutputState != state) || (priority == HEAT_LOCKED))    //If state has changed OR if locked state defined
            {
                if(state == CONTROL_HEAT_STATE_OFF)
                {
                    TurnOffHeatOutput();
                }
                else
                {
                    TurnOnHeatOutput();
                }
                OutputState = state;
            }
        }
        if (priority == HEAT_LOCKED)
        {
            locked = TRUE;
        }
    }
}

/*******************************************************************************
*   \function : void LoadManagement_StartNewCycle(uint8_t apply_random_delay){
*	\brief :    Begin a new heating cycle
*	\param :    None
*	\return :   None
********************************************************************************/
void LoadManagement_StartNewCycle(uint8_t apply_random_delay)
{
    Cycle = 100;
    CycleStatus = 0;
    if (apply_random_delay == TRUE)
    {
        osDelay(RandomStart);
    }

    param = &CycleParamsRelayCycle;

    if (CycleTimeout == NULL)
    {
        CycleTimeout = xTimerCreate ("Cycle Timeout",
                                     pdMS_TO_TICKS(param->onePercent),
                                     pdTRUE,               //one shot timer
                                     (void*)0,
                                     LoadManagement_DoCycle);
        xTimerStart(CycleTimeout, 0);
    }
    else
    {
        xTimerStop(CycleTimeout, 0);
        xTimerChangePeriod(CycleTimeout, pdMS_TO_TICKS(param->onePercent), 0);
    }
    LoadManagement_DoCycle(CycleTimeout);
}

/*******************************************************************************
* @brief  EXTI line detection callbacks
* @inputs GPIO_Pin: Specifies the pins connected EXTI line
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/02/10
*******************************************************************************/
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
#ifdef FLOOR
    if (GPIO_Pin == GFCI_FAULT_Pin)
    {
        GFCIFaultDetection_Callback();
        __HAL_GPIO_EXTI_CLEAR_FLAG(GFCI_FAULT_Pin);
    }

    if (GPIO_Pin == GFCI_Error_Pin)
    {
        ErrorDetection_Callback();
        __HAL_GPIO_EXTI_CLEAR_FLAG(GFCI_Error_Pin);
    }
#endif
}

/*******************************************************************************
* @brief  Get the Output desired state
* @inputs None
* @retval CONTROL_HEAT_STATE_OFF or CONTROL_HEAT_STATE_ON
* @author Jean-Fran�ois Many
* @date 2017/02/10
*******************************************************************************/
U8BIT Get_OutputDemand(void)
{
    return heatDemandState;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void SetLocalGFCI(void)
{
    LocalGFCI = 1;
    xTimerResetFromISR(GFCIReportTimeout, 0);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void ClearLocalGFCI(void)
{
    LocalGFCI = 0;
    xTimerResetFromISR(GFCIReportTimeout, 0);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void SetLocalEndOfLife(void)
{
    uint8_t alert;

    LocalEoL = 1;
    alert = DB_SET_ALERT | DBALERT_END_OF_LIFE;
    DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void ClearLocalEndOfLife(void)
{
    uint8_t alert;

    LocalEoL = 0;
    if (RemoteEoL == 0)
    {
        alert = DB_CLEAR_ALERT | DBALERT_END_OF_LIFE;
        DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void SetFloorExtensionGFCIAlert(void)
{
    RemoteGFCI = 1;
    xTimerReset(GFCIReportTimeout, 0);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void ClearFloorExtensionGFCIAlert(void)
{
    RemoteGFCI = 0;
    xTimerReset(GFCIReportTimeout, 0);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void SetFloorExtensionEndOfLifeAlert(void)
{
    uint8_t alert;

    RemoteEoL = 1;
    alert = DB_SET_ALERT | DBALERT_END_OF_LIFE;
    DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static void ClearFloorExtensionEndOfLifeAlert(void)
{
    uint8_t alert;

    RemoteEoL = 0;
    if (LocalEoL == 0)
    {
        alert = DB_CLEAR_ALERT | DBALERT_END_OF_LIFE;
        DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    }
}



/** Copyright(C) 2017 Stelpro Design, All Rights Reserved**/
