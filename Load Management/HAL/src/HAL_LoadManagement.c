/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2017, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    HAL_LoadManagement.c
* @date    2017/01/04
* @authors Jean-Fran�ois Many
* @brief   Load Management Module
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include "typedef.h"
#include "stm32f4xx_hal.h"
#include "HAL_Interrupt.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\Sensors\inc\THREAD_Sensors.h"
#include ".\HAL\inc\HAL_LoadManagement.h"
#include ".\HAL\inc\HAL_CurrentSensing.h"
#include ".\HAL\inc\TemperatureControl.h"
#include "common_types.h"
#include "WindowMonitoring.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define TurnOnHeatOutput()          (HAL_GPIO_WritePin(TRIAC_GPIO_Port, TRIAC_Pin, GPIO_PIN_SET))
#define TurnOffHeatOutput()         (HAL_GPIO_WritePin(TRIAC_GPIO_Port, TRIAC_Pin, GPIO_PIN_RESET))
#define IsHeatOutputOn()            (HAL_GPIO_ReadPin(TRIAC_GPIO_Port, TRIAC_Pin))


//Control parameters
typedef struct
{
    int8u PercentMin;           //  PercentMin : Minimum allowed percent to avoid short ON cycle
    int8u PercentMax;           //  PercentMax : Maximum allowed percent to avoid short OFF cycle
    int16u onePercent;          //  OnePercent : number of os tick for 1 percent increment
} CycleParam_t;

                                                                                                  
                                                       

//Medium cycle 1 percent increment = 5 minutes * 60 sec/min * 1000 ms/sec / 100 (every 3 seconds)
#define ONE_PERCENT_MEDIUM_CYCLE        (5*60*1000L/100)

//Short cycle 1 percent increment = 15 seconds * 1000 ms/sec / 100 (every 150 miliseconds)
#define ONE_PERCENT_SHORT_CYCLE         (15*1000/100)

#define ONE_HOUR_IN_MS                  (3600000)   //(1000 * 60 * 60)

#ifdef LOAD_CYCLING
//    #define LOAD_CYCLING_ON_TIME        600 //x100ms (60 seconds)
//    #define LOAD_CYCLING_OFF_TIME       600 //x100ms (60 seconds)

    #define LOAD_CYCLING_ON_TIME        10 //x100ms (1 second)
    #define LOAD_CYCLING_OFF_TIME       90 //x100ms (9 seconds)
#endif

#define VOLTAGE_THRESHOLD_240   100   //Vcap drop frequency less than 10Hz = 240V
                                      //100 * 1ms = 0.100 second = 10 Hz
                                      //An higher number means a lower frequency

#define MININUM_CURRENT     1.02    //If the read current is below 1.02A, it's because the load is not active, set the overheating alert
                                    //1.02A is the absolute minimum current that can be considered valid
                                    //300W -10% = 270W @240V +15% = 264V : 270W/264V = 1.02A
                                    //150W -10% = 135W @120V +15% = 132V : 135W/132V = 1.02A

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
//Control parameters for Triac with short cycle
static CycleParam_t const * param;            //pointer to estimate parameters in flash                                                                                       
static const CycleParam_t CycleParamsTriacShortCycle =    /* Triac Base short Cycles */
{
    5,                                              //  PercentMin : Minimum allowed percent to avoid short ON cycle
    95,                                             //  PercentMax : Maximum allowed percent to avoid short OFF cycle
    ONE_PERCENT_SHORT_CYCLE,                        //  OnePercent : number of os tick for 1 percent increment
};

static const CycleParam_t CycleParamsTriacMediumCycle =   /* Fan Base medium Cycles */
{
    10,                                             //  PercentMin : Minimum allowed percent to avoid short ON cycle
    90,                                             //  PercentMax : Maximum allowed percent to avoid short OFF cycle
    ONE_PERCENT_MEDIUM_CYCLE,                       //  OnePercent : number of os tick for 1 percent increment
};

static uint8_t Cycle;          //Cycle Percent
static uint8_t CycleStatus;    // 1 => output has cycled ON-OFF
static ControlHeatState_e OutputState;    //Output State
static uint16_t CycleOnTime;    //Triac On time in ms for 1 cycle
static float DailyOnTime;    //Cumulative Triac On time in ms for 1 day
static uint32_t DailyCycleNumber;   //Number of cycle with the Triac activated
static float DailyMeanCurrent;  //Mean current for the day
static uint16_t LocalPowerConsumption;  //Total consumed power for this thermostat for 1 day
static TimerHandle_t CycleTimeout = NULL;
static U8BIT heatDemandState = FALSE;
#ifndef DEVKIT_PLATFORM
    static uint32_t pulsePeriod = 0;
    static uint32_t pulseStart = 0;
    static uint32_t pulseLastStart = 0;
    static uint32_t pulseEnded = 0;
    static uint32_t pulseCenter = 0;
    static uint8_t FastZCSync = FALSE;
//    static uint32_t newVcapDrop = 0;
//    static uint32_t oldVcapDrop = 0;
#endif /* DEVKIT_PLATFORM */
    static uint32_t VcapDropFrequency = 0;
    static uint8_t DetectedVoltage = 240;
    static uint8_t ZCDetected = 0;
static uint32_t RandomStart = 0;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
extern dbType_Residence_t           CurrentResidence;
TIM_HandleTypeDef htim5;
RNG_HandleTypeDef hrng;

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static void LoadManagement_DoCycle(TimerHandle_t xTimer);
static void LoadLimitMinMax(int8u * value, int8u min, int8u max);
static void TIM5_Init(void);
static void Init_ZC_Vcap_Interrupt(void);
DB_NOTIFICATION(OnHeatModeChange);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
#ifdef LOAD_CYCLING
/*******************************************************************************
*   \function : void LoadManagement_DoCycle(TimerHandle_t xTimer)
*	\brief :    Cycle management
*	\param :    None
*	\return :   None
********************************************************************************/
#pragma optimize = none
static void LoadManagement_DoCycle(TimerHandle_t xTimer)
{
    static int16u TimeCounter;

    //Call every 500 ms while is OFF
    xTimerChangePeriod(CycleTimeout, pdMS_TO_TICKS(500), 0);

    BSP_InterruptDeclareCritical();
    BSP_InterruptEnterCritical();
    if(LOAD_CYCLING_ON_TIME)                  //There is ON Time programmed
    {
        if(LOAD_CYCLING_OFF_TIME == 0)        //No OFF time = Always ON
        {
            Set_OutputDemand(TRUE, HEAT_UNLOCKED);
            LoadManagement_SetOutputState(CONTROL_HEAT_STATE_ON, HEAT_UNLOCKED);
            CTL_SetHeatDemand(100);
        }
        else if(TimeCounter < LOAD_CYCLING_OFF_TIME)   //OFF Time
        {
            Set_OutputDemand(FALSE, HEAT_UNLOCKED);
            LoadManagement_SetOutputState(CONTROL_HEAT_STATE_OFF, HEAT_UNLOCKED);
            CTL_SetHeatDemand(0);
        }
        else                            //ON Time
        {
            Set_OutputDemand(TRUE, HEAT_UNLOCKED);
            LoadManagement_SetOutputState(CONTROL_HEAT_STATE_ON, HEAT_UNLOCKED);
            CTL_SetHeatDemand(100);
        }
        TimeCounter += 5;

        if(TimeCounter >= LOAD_CYCLING_ON_TIME + LOAD_CYCLING_OFF_TIME)
        {
            TimeCounter = 0;
        }

    }
    else
    {
        //No OnTime programmed - remain OFF
        TimeCounter = 0;
        Set_OutputDemand(FALSE, HEAT_UNLOCKED);
        LoadManagement_SetOutputState(CONTROL_HEAT_STATE_OFF, HEAT_UNLOCKED);
        CTL_SetHeatDemand(0);
    }
    // Leave critical
    BSP_InterruptExitCritical();
}
#else
/*******************************************************************************
*   \function : void LoadManagement_DoCycle(TimerHandle_t xTimer)
*	\brief :    Cycle management
*	\param :    None
*	\return :   None
********************************************************************************/
static void LoadManagement_DoCycle(TimerHandle_t xTimer)
{
    uint8_t heatDemand;
    HeatMode_t heatMode;
                                                                                    
    TEMPERATURE_C_t temperature;
    static int8u Pct_startcycle = 0;
    static int8u Pct_endcycle = 0;
    static TEMPERATURE_C_t Temp_startcycle = 0;
    static TEMPERATURE_C_t Temp_endcycle = 0;
    int8s delta_demand = 0;
    TEMPERATURE_C_t delta_temperature = 0;
                        
    float current;
    static float buffer_float;
    uint8_t alert;

    DBTHRD_GetData(DBTYPE_HEAT_MODE_SETTING,(void*)&heatMode, INDEX_DONT_CARE);
                                  
       
                                           
                                  
                                                
                                               
       
          
     
                                        
                                
                                            
                                            
     
                                                                   

    DBTHRD_GetData(DBTYPE_HEAT_DEMAND,(void*)&heatDemand, THIS_THERMOSTAT);
    LoadLimitMinMax(&heatDemand,param->PercentMin,param->PercentMax);

    Cycle += 1;
    if(Cycle >= 100)                                //Restart a new cycle
    {
        //If heat demand = 100%, triac is still on, so set the cycle on time to 100%
        if (heatDemand == 100)
        {
            CycleOnTime = 100;
        }
        //Heating was active during last cycle, so we recalculate the power consumption
        if (CycleOnTime > 0)
        {
            DailyCycleNumber++;

            //Add the On time from this cycle to the cumulative On time for the day in ms
            DailyOnTime += (float)(CycleOnTime * param->onePercent);

            //Calculate the average current for the day in kWh
            current = HAL_CurrentSensor_GetCurrent();

            DailyMeanCurrent = (DailyMeanCurrent * (DailyCycleNumber - 1) + current) / DailyCycleNumber;
            buffer_float = ((DetectedVoltage * DailyMeanCurrent) * (DailyOnTime / ONE_HOUR_IN_MS)) / 1000;
            LocalPowerConsumption = (uint16_t)buffer_float;
            if ((buffer_float - LocalPowerConsumption) >= 0.5)
            {
                LocalPowerConsumption++;
            }
            //Make sure we report something if a valid load has been activated during the day
            if ((LocalPowerConsumption == 0) && (DailyMeanCurrent >= MININUM_CURRENT) && (DailyCycleNumber != 0))
            {
                LocalPowerConsumption = 1;
            }
            DBTHRD_SetData(DBTYPE_LOCAL_DAILY_CONSUMPTION,
                                (void*)&LocalPowerConsumption,
                                THIS_THERMOSTAT,
                                DBCHANGE_LOCAL);
            if (DailyMeanCurrent < MININUM_CURRENT)
            {
                alert = DB_SET_ALERT | DBALERT_OVERHEAT;
                DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            }
            else
            {
                alert = DB_CLEAR_ALERT | DBALERT_OVERHEAT;
                DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            }
        }

        SetCurrentSensingPermit(TRUE);
        Cycle = 0;
        CycleStatus = 0;
        CycleOnTime = 0;
        Pct_endcycle = Pct_startcycle;
        Temp_endcycle = Temp_startcycle;
        Pct_startcycle = heatDemand;
        DBTHRD_GetData(DBTYPE_CONTROL_TEMPERATURE,(void*)&temperature, INDEX_DONT_CARE);
        Temp_startcycle = temperature;
        delta_demand = Pct_startcycle - Pct_endcycle;
        delta_temperature = Temp_startcycle - Temp_endcycle;
        WM_SetDelta(delta_demand, delta_temperature);
    }

    BSP_InterruptDeclareCritical();
    BSP_InterruptEnterCritical();
    if ((heatDemand > Cycle) && (CycleStatus == 0))
    {
        Set_OutputDemand(TRUE, HEAT_UNLOCKED);
        //In fan mode, turn on the Triac immediately
        if(heatMode == HEATMODE_FAN)
        {
            LoadManagement_SetOutputState(CONTROL_HEAT_STATE_ON, HEAT_UNLOCKED);
        }
        else
        {
            //In baseboard mode, the ZC signal manages the Triac activation
        }
    }
    else
    {
        Set_OutputDemand(FALSE, HEAT_UNLOCKED);
        LoadManagement_SetOutputState(CONTROL_HEAT_STATE_OFF, HEAT_UNLOCKED);
        __HAL_TIM_SET_AUTORELOAD(&htim5, 0xFFFFFFFF);

        //Make sure the triac was activated before
        if (CycleOnTime > 0)
        {
            CycleStatus = 1;        //to prevent reactivation of the output until the end of the control cycle
        }
    }
    // Leave critical
    BSP_InterruptExitCritical();
}
#endif /* LOAD_CYCLING */

/*******************************************************************************
* @brief  Notification when heat mode changes (Fan/Baseboard)
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/02/10
*******************************************************************************/
DB_NOTIFICATION(OnHeatModeChange)
{
    LoadManagement_StartNewCycle(FALSE);
}

/*******************************************************************************
//  \function : void LoadLimitMinMax(int8u * value, int8u min, int8u max){
//	\brief :    Clip value to load minimum and maximum applicable percent
//	\param :    int8u * value = percent requested
//              int8u min = min applicable percent
//              int8u max = max applicable percent
//	\return :   none, modified int8u * value
********************************************************************************/
static void LoadLimitMinMax(int8u * value, int8u min, int8u max)
{
    if(*value < min)                //Below min: 0
    {
        *value = 0;
    }
    else if(*value >= 100)          //Absolute max is 100
    {
        *value = 100;
    }
    else if(*value > max)           //Between max and 100:
    {
        //*value = max;				//use max
        *value = 100;				//use 100
    }
    else
    {
        //otherwise use as is
    }
}

/*******************************************************************************
* @brief  Set the Output desired state
* @inputs state: CONTROL_HEAT_STATE_OFF or CONTROL_HEAT_STATE_ON
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/02/10
*******************************************************************************/
void Set_OutputDemand(U8BIT state, U8BIT priority)
{
    static U8BIT demandLocked = FALSE;

    if (priority == HEAT_PRIORITY)
    {
        demandLocked = TRUE;
    }
    if (priority == HEAT_CANCEL_PRIORITY)
    {
        demandLocked = FALSE;
    }
    if ((demandLocked == FALSE) || (priority == HEAT_PRIORITY))
    {
        if ((heatDemandState == TRUE) && (state == FALSE))
        {
            //End of heating phase, save CycleOnTime
            CycleOnTime = Cycle;
        }

        heatDemandState = state;
    }
}

/*******************************************************************************
* @brief  Timer5 Init Function to sync ZC for Triac activation
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/02/10
*******************************************************************************/
static void TIM5_Init(void)
{
    RCC_ClkInitTypeDef    clkconfig;
    uint32_t              pFLatency;
    uint32_t              uwPrescalerValue = 0;
    uint32_t              uwTimclock = 0;

    /*Configure the TIM5 IRQ priority */
    HAL_NVIC_SetPriority(TIM5_IRQn, 2, 0);

    /* Enable the TIM5 global Interrupt */
    HAL_NVIC_EnableIRQ(TIM5_IRQn);

    /* Enable TIM5 clock */
    __TIM5_CLK_ENABLE();

    /* Get clock configuration */
    HAL_RCC_GetClockConfig(&clkconfig, &pFLatency);

    /* Compute TIM5 clock */
    uwTimclock = HAL_RCC_GetPCLK2Freq();

    /* Compute the prescaler value to have TIM5 counter clock equal to 1MHz */
    uwPrescalerValue = (uint32_t) (uwTimclock / 1000000);

    /* Initialize TIM5 */
    htim5.Instance = TIM5;

    htim5.Init.Period = 0xFFFFFFFF;
    htim5.Init.Prescaler = uwPrescalerValue - 1;
    htim5.Init.ClockDivision = 0;
    htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
    HAL_TIM_Base_Init(&htim5);
    //Start the timer counter
    HAL_TIM_Base_Start(&htim5);
}

/*******************************************************************************
* @brief  Init function for Vcap and ZC GPIOs and interrupts
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/02/10
*******************************************************************************/
static void Init_ZC_Vcap_Interrupt(void)
{
#ifndef DEVKIT_PLATFORM
    GPIO_InitTypeDef GPIO_InitStruct;

    /*Configure the EXTI1 IRQ priority */
    HAL_NVIC_SetPriority(EXTI1_IRQn, 4, 0);

    /*Configure the EXTI2 IRQ priority */
    HAL_NVIC_SetPriority(EXTI2_IRQn, 3, 0);

    /* Enable the EXTI1 global Interrupt */
    HAL_NVIC_EnableIRQ(EXTI1_IRQn);

    /* Enable the EXTI2 global Interrupt */
    HAL_NVIC_EnableIRQ(EXTI2_IRQn);

    __GPIOG_CLK_ENABLE();
    __GPIOD_CLK_ENABLE();

    GPIO_InitStruct.Pin = VCAP_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(VCAP_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = ZC_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(ZC_GPIO_Port, &GPIO_InitStruct);
#endif /* DEVKIT_PLATFORM */
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  Load Management initialization function
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/04
*******************************************************************************/
void LoadManagement_Init(void)
{
                        
                        

    OutputState = CONTROL_HEAT_STATE_OFF;
    Cycle = 0;                      //Cycle Percent
    CycleStatus = 0;                //New cycle
    CycleOnTime = 0;
    DailyOnTime = 0;
    DailyMeanCurrent = 0;
    LocalPowerConsumption = 0;
    DailyCycleNumber = 0;
    CTL_SetHeatDemand(0);           //Applied Percent

    //Enable the Random generator for the random start
    __HAL_RCC_RNG_CLK_ENABLE();
    hrng.Instance = RNG;
    HAL_RNG_Init(&hrng);
    HAL_RNG_GenerateRandomNumber(&hrng, &RandomStart);
    RandomStart = (uint16_t)(15000 * ((float)RandomStart / 0xFFFFFFFF));
       
          
     
                                        
                                
                                            
     
                             
     
                                                     
                                                               
                                                                            
                                              
                                                             
     
        
     
                                                                       
     


    DBTHRD_SubscribeToNotification(DBTYPE_HEAT_MODE_SETTING,
                                   OnHeatModeChange,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);

    LoadManagement_StartNewCycle(1);    //start new cycle with random start

    Init_ZC_Vcap_Interrupt();

    TIM5_Init();

                                 
                                 
}

/*******************************************************************************
*   \function : void LOAD_SetOutputState(ControlHeatState_e state, U8BIT priority)
*	\brief :    Change the status of the output state
*	\param :    None
*	\return :   None
********************************************************************************/
void LoadManagement_SetOutputState(ControlHeatState_e state, U8BIT priority)
{
    static U8BIT locked = FALSE;
    static U8BIT priorityRequest = FALSE;
    static ControlHeatState_e priorityState = CONTROL_HEAT_STATE_OFF;

    if(state < MAX_CONTROL_HEAT_STATE)
    {
        if (priority == HEAT_PRIORITY)  //Check if a particular state is requested (test mode)
        {
            priorityRequest = TRUE;     //  Remember to return to this state every time we can
            priorityState = state;      //  Save it for later
        }
        if (priority == HEAT_CANCEL_PRIORITY)//End of test mode
        {
            priorityRequest = FALSE;    //  Let the code manage the heat normally
        }
        if (priority == HEAT_RELEASE)   //If normal operation is allowed (Vcap is up), release the lock and apply the requested state, if any
        {
            locked = FALSE;
        }
        if ((locked != TRUE) || (priority == HEAT_LOCKED))
        {
            if ((priorityRequest == TRUE) && (priority != HEAT_LOCKED)) //If no locked state defined AND particular state is requested
            {
                state = priorityState;                                  //  Force return to the requested state
            }
            if ((OutputState != state) || (priority == HEAT_LOCKED))    //If state has changed OR if locked state defined
            {
                ZCDetected = 0;
                if(state == CONTROL_HEAT_STATE_OFF)
                {
                    TurnOffHeatOutput();
                }
                else
                {
                    TurnOnHeatOutput();
                    if (IsCurrentSensingPermit() == TRUE)
                    {
                        SetCurrentSensingPermit(FALSE);
                        SetCurrentSensingRequest(TRUE);
                    }
                }
                OutputState = state;
            }
        }
        if (priority == HEAT_LOCKED)
        {
            locked = TRUE;
        }
    }
}

/*******************************************************************************
*   \function : void LoadManagement_StartNewCycle(uint8_t fromCloud){
*	\brief :    Begin a new heating cycle
*	\param :    fromCloud: new cycle triggered from cloud (TRUE or FALSE)
*	\return :   None
********************************************************************************/
void LoadManagement_StartNewCycle(uint8_t apply_random_delay)
{
    HeatMode_t heatMode;
    
    Cycle = 0;
    CycleStatus = 0;
    if (apply_random_delay == TRUE)
    {
        osDelay(RandomStart);
    }

    DBTHRD_GetData(DBTYPE_HEAT_MODE_SETTING,(void*)&heatMode, INDEX_DONT_CARE);
    if(heatMode == HEATMODE_FAN)
    {
        //Fan cycle - do 5 minutes cycles
        //at 1 percent increment
        param = &CycleParamsTriacMediumCycle;
    }
    else
    {
        //Short cycle - do 15 sec cycles
        //at 1 percent increment
        param = &CycleParamsTriacShortCycle;
    }
                    
    if (CycleTimeout == NULL)
    {
        CycleTimeout = xTimerCreate ("Cycle Timeout",
                                     pdMS_TO_TICKS(param->onePercent),
                                     pdTRUE,               //one shot timer
                                     (void*)0,
                                     LoadManagement_DoCycle);
        xTimerStart(CycleTimeout, 0);                                                                                   
    }
    else
    {
        xTimerStop(CycleTimeout, 0);   
        xTimerChangePeriod(CycleTimeout, pdMS_TO_TICKS(param->onePercent), 0);                                      
    }
    LoadManagement_DoCycle(CycleTimeout);
}    

/*******************************************************************************
*   \function : void LoadManagement_StartNewDay(void){
*	\brief :    Begin a new day (reset daily on time)
*	\param :    None
*	\return :   None
********************************************************************************/
void LoadManagement_StartNewDay(void)
{
    DailyOnTime = 0;
    DailyCycleNumber = 0;
    DailyMeanCurrent = 0;
    LocalPowerConsumption = 0;
    DBTHRD_SetData(DBTYPE_LOCAL_DAILY_CONSUMPTION, (void*)&LocalPowerConsumption, THIS_THERMOSTAT, DBCHANGE_LOCAL);
}

/*******************************************************************************
* @brief  EXTI line detection callbacks
* @inputs GPIO_Pin: Specifies the pins connected EXTI line
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/02/10
*******************************************************************************/
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
#ifndef DEVKIT_PLATFORM
    GPIO_PinState ZCpinState;
    GPIO_PinState VcappinState;
    HeatMode_t heatMode;
    uint32_t cnt;

    VcappinState = HAL_GPIO_ReadPin(VCAP_GPIO_Port, VCAP_Pin);
    ZCpinState = HAL_GPIO_ReadPin(ZC_GPIO_Port, ZC_Pin);

    switch (GPIO_Pin)
    {
        case VCAP_Pin:
            if (VcappinState == GPIO_PIN_RESET)
            {
                //Vcap drops: Turn off the TRIAC now!
                LoadManagement_SetOutputState(CONTROL_HEAT_STATE_OFF, HEAT_LOCKED);

//                oldVcapDrop = newVcapDrop;
//                newVcapDrop = OS_GetTickCount();
                HAL_TIM_Base_Stop_IT(&htim5);
                __HAL_TIM_SET_AUTORELOAD(&htim5, 0xFFFFFFFF);

                //Start the timer counter, since it was stopped when interrupt was disabled...
                HAL_TIM_Base_Start(&htim5);

//                //Check if there is a timer rollover
//                if (newVcapDrop < oldVcapDrop)
//                {
//                    VcapDropFrequency = newVcapDrop + (0xFFFFFFFF - oldVcapDrop);
//                }
//                else
//                {
//                    VcapDropFrequency = newVcapDrop - oldVcapDrop;
//                }
//
//                if (VcapDropFrequency != 0)
//                {
//                    //If the frequency is below 10Hz, its 240V
//                    if (VcapDropFrequency > VOLTAGE_THRESHOLD_240)
//                    {
//                        DetectedVoltage = 240;
//                    }
//                    else
//                    {
//                        DetectedVoltage = 120;
//                    }
//                }
            }
            else
            {
                if (Get_OutputDemand())
                {
                    DBTHRD_GetData(DBTYPE_HEAT_MODE_SETTING,(void*)&heatMode, INDEX_DONT_CARE);
                    if (heatMode == HEATMODE_FAN)
                    {
                        //Vcap rises in Fan Mode, Turn on the TRIAC
                        LoadManagement_SetOutputState(CONTROL_HEAT_STATE_ON, HEAT_RELEASE);
                    }
                    else
                    {
                        //If Vcap rises in Baseboard mode, we turn on the triac on the next ZC rise
                        FastZCSync = TRUE;
                    }
                }
                else
                {
                    //If the heat demand is gone while the Vcap was low, release the output lock
                    LoadManagement_SetOutputState(CONTROL_HEAT_STATE_OFF, HEAT_RELEASE);
                }
            }
            break;

        case ZC_Pin:
            if (VcappinState == GPIO_PIN_SET)
            {
                //Vcap is up, proceed with ZC management

                DBTHRD_GetData(DBTYPE_HEAT_MODE_SETTING,(void*)&heatMode, INDEX_DONT_CARE);
                if (heatMode == HEATMODE_BASEBOARD)
                {
                    //Baseboard mode is active, proceed with ZC management
                    if (ZCpinState == GPIO_PIN_SET)
                    {
                        if (FastZCSync == FALSE)
                        {
                            //ZC rises in baseboard mode, save the pulse period
                            pulseLastStart = pulseStart;
                            pulseStart = __HAL_TIM_GET_COUNTER(&htim5);
                            //Check if there is a timer rollover
                            if (pulseStart < pulseLastStart)
                            {
                                //Check that ZC rises are not too close from one another, to avoid corruption
                                if ((pulseStart + (0xFFFFFFFF - pulseLastStart)) > 100)
                                {
                                    pulsePeriod = pulseStart + (0xFFFFFFFF - pulseLastStart);
                                }
                                else
                                {
                                    //Two ZC rises close from each other, probably a glitch, do not update the pulsePeriod
                                }
                            }
                            else
                            {
                                //Check that ZC rises are not too close from one another, to avoid corruption
                                if ((pulseStart - pulseLastStart) > 100)
                                {
                                    pulsePeriod = pulseStart - pulseLastStart;
                                }
                                else
                                {
                                    //Two ZC rises close from each other, probably a glitch, do not update the pulsePeriod
                                }
                            }
                        }
                        else
                        {
                            //Fast ZC sync after Vcap rise, turn On Triac in pulseCenter time
                            cnt = __HAL_TIM_GET_COUNTER(&htim5);
                            __HAL_TIM_SET_AUTORELOAD(&htim5, cnt + pulseCenter);
                            __HAL_TIM_CLEAR_FLAG(&htim5, TIM_FLAG_UPDATE);
                            HAL_TIM_Base_Start_IT(&htim5);
                        }
                        ZCDetected |= 0x0F;
                    }
                    else
                    {
                        if (FastZCSync == FALSE)
                        {
                            //ZC drops in baseboard mode
                            if (Get_OutputDemand() == FALSE)
                            {
                                //Triac is off, calculate where the pulse center is
                                pulseEnded = __HAL_TIM_GET_COUNTER(&htim5);
                                if (pulseEnded < pulseStart)
                                {
                                    //Rollover occured
                                    pulseCenter = (pulseEnded + (0xFFFFFFFF - pulseStart)) / 2;
                                }
                                else
                                {
                                    //No rollover
                                    pulseCenter = (pulseEnded - pulseStart) / 2;
                                }
                                __HAL_TIM_SET_AUTORELOAD(&htim5, 0xFFFFFFFF);
                            }
                            else
                            {
                                //Ok, we got a heat demand guys, let sync with the ZC signal!
                                cnt = __HAL_TIM_GET_COUNTER(&htim5);
                                __HAL_TIM_SET_AUTORELOAD(&htim5, cnt + pulsePeriod - pulseCenter);
                                __HAL_TIM_CLEAR_FLAG(&htim5, TIM_FLAG_UPDATE);
                                HAL_TIM_Base_Start_IT(&htim5);
                            }
                        }
                        else
                        {
                            //If fast sync is required, no action is taken on ZC drop
                        }
                        ZCDetected |= 0xF0;
                    }
                }
                else
                {
                    //In Fan mode, make sure the lock is released since Vcap is up
                    if (Get_OutputDemand())
                    {
                        LoadManagement_SetOutputState(CONTROL_HEAT_STATE_ON, HEAT_RELEASE);
                    }
                    else
                    {
                        LoadManagement_SetOutputState(CONTROL_HEAT_STATE_OFF, HEAT_RELEASE);
                    }
                }
            }
            else
            {
                //Vcap is down: make sure the TRIAC is off
                LoadManagement_SetOutputState(CONTROL_HEAT_STATE_OFF, HEAT_LOCKED);
                HAL_TIM_Base_Stop_IT(&htim5);
                __HAL_TIM_SET_AUTORELOAD(&htim5, 0xFFFFFFFF);
                //Start the timer counter, since it was stopped when interrupt was disabled...
                HAL_TIM_Base_Start(&htim5);
            }
            break;

        default:
            break;
    }
#endif /* DEVKIT_PLATFORM */
}

/*******************************************************************************
* @brief  Zero Crossing Timer5 interrupt callbacks
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/02/10
*******************************************************************************/
void HAL_ZeroCrossingManagement(void)
{
#ifndef DEVKIT_PLATFORM
    //Turn On the Triac
    LoadManagement_SetOutputState(CONTROL_HEAT_STATE_ON, HEAT_RELEASE);
    FastZCSync = FALSE;
    HAL_TIM_Base_Stop_IT(&htim5);
    __HAL_TIM_SET_AUTORELOAD(&htim5, 0xFFFFFFFF);
    //Start the timer counter, since it was stopped when interrupt was disabled...
    HAL_TIM_Base_Start(&htim5);
#endif /* DEVKIT_PLATFORM */
}

/*******************************************************************************
*   \function : uint8_t LoadManagement_ValidateZeroCrossing(void)
*	\brief :    Return the Zero Crossing validation state
*	\param :    None
*	\return :   None
********************************************************************************/
uint8_t LoadManagement_ValidateZeroCrossing(void)
{
    uint8_t returned_value = ZCDetected;
    ZCDetected = 0;
    return returned_value;
}

/*******************************************************************************
* @brief  Get the Output desired state
* @inputs None
* @retval CONTROL_HEAT_STATE_OFF or CONTROL_HEAT_STATE_ON
* @author Jean-Fran�ois Many
* @date 2017/02/10
*******************************************************************************/
U8BIT Get_OutputDemand(void)
{
    return heatDemandState;
}

uint32_t Get_VcapDropFrequency(void)
{
    return VcapDropFrequency;
}

uint8_t Get_DetectedVoltage(void)
{
    return DetectedVoltage;
}

/** Copyright(C) 2017 Stelpro Design, All Rights Reserved**/
