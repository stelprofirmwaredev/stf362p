/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2018, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/
/*******************************************************************************
* @file    Heat_Management.c
* @date    2018/04/05
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
* @brief  Configure TIMx peripheral in order to control the hardware for a
*           zero crossing triac gate drive based on the cycle length captured
*           in TIMx ticks.
*                 __
*  ZC_Detect   __|  |_________________.....
*                   ._        ___
*  TRIAC_QIII  _____| |______| . |____.....
*                   .    ___   .   __
*  TRIAC_QI    _____.___| . |__.__|   .....
*                   .     .    .     .
*                   .    /\    .    /
*                   .   /  \   .   /
*  TCNT             .  /    \  .  /
*                   . /      \ . /
*              _____./        \./
*
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <string.h>
#include "stm32f4xx_hal.h"
#include ".\HAL\inc\Triac_Management.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define USE_HEATMNGMT_TRACE_BUFFER
#ifdef FAN_FORCED_HEATING
    #define HEATING_TRIAC_PERIOD               MINUTES_TO_SOFTTIMER_TICK(5)
    #define HEATING_10_PERCENT                 ((HEATING_TRIAC_PERIOD * 10) / 100LL)
    #define HEATING_90_PERCENT                 ((HEATING_TRIAC_PERIOD * 90) / 100LL)
#else
    #define HEATING_TRIAC_PERIOD               SECONDS_TO_SOFTTIMER_TICK(15)
    #define HEATING_10_PERCENT                 0
    #define HEATING_90_PERCENT                 ((HEATING_TRIAC_PERIOD * 90) / 100LL)
#endif

typedef enum
{
    STATUS_IDLE = 0,
    STATUS_WAIT_START_TRIGGER,
    STATUS_HEAT_START_REQUEST,
    STATUS_HEAT_END_REQUEST,
    STATUS_HEAT_ENDS_WAIT_NEXT_TRIGGER,
    STATUS_GHOST_PULSE_COMPLETED,
    STATUS_HEAT_ENDS_GO_TO_IDLE,
    STATUS_HEAT_ENDS_GHOST_PULSE_COMPLETED,
    STATUS_HEAT_ZC_UPDATE,
    STATUS_RECHARGE_NEEDED,
} trace_status_t;

typedef struct
{
    uint32_t time_stamp;
    trace_status_t step_status;
} trace_steps_t;

typedef enum
{
    LV_IDLE = 0,
    LV_CYCLE_MEASUREMENT,
    LV_RECHARGING,
    LV_WAIT_TRIGGER,
    LV_COUNTDOWN,
    LV_HEATING,
    LV_HEATING_ENDS,
    LV_RESYNC,

    LV_LAST_STATE,
} line_volt_mngnt_states_t;

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
static volatile line_volt_mngnt_states_t current_lv_state;

#ifdef USE_HEATMNGMT_TRACE_BUFFER
    trace_steps_t trace_buffer[TRACE_BUFFER_SIZE];
    uint16_t trace_cnt;
#endif
uint8_t output_status;
static line_volt_mngnt_events_t event_q[EVENT_Q_SIZE];
static uint8_t event_q_por = 0;
static uint8_t event_q_pow = 0;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
void Default_HeatingVariables(void);

line_volt_status_t to_next_state_from_idle(line_volt_mngnt_events_t line_volt_event);
line_volt_status_t to_next_state_from_recharging(line_volt_mngnt_events_t line_volt_event);
line_volt_status_t to_next_state_from_wait_trigger(line_volt_mngnt_events_t line_volt_event);
line_volt_status_t to_next_state_from_heating_ends(line_volt_mngnt_events_t line_volt_event);
line_volt_status_t to_next_state_from_heating(line_volt_mngnt_events_t line_volt_event);

extern void config_triac_sequence_with_trigger_start(void);
extern void reset_pwm_timer_registers(void);
extern void setup_io_idle_state(void);
extern void set_last_pulse(void);
extern void start_pulse_sequence(void);
extern uint8_t isTimerUpCounting(void);
extern void stop_triac_sequence(void);
extern void UpdatePeriod(void);
extern uint8_t isVCapLow(void);
extern void Init_HeatingPeripherals(void);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void log_status(trace_status_t step_status)
{
#ifdef USE_HEATMNGMT_TRACE_BUFFER
    trace_buffer[trace_cnt].step_status = step_status;
    trace_buffer[trace_cnt].time_stamp = HAL_GetTick();

    trace_cnt++;
    if (trace_cnt >= TRACE_BUFFER_SIZE)
    {
        trace_cnt = 0;
    }
#endif
}

/*******************************************************************************
* @brief  Line volts (lv) events are received and dispatch here, depending
*           on the actual state of the system.
*
*         This function is made "thread safe", as it can be invoked from the
*           main thread and from interrupt, and at the same time (current
*           execution interrupted by an interrupt event)
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void post_lv_events(line_volt_mngnt_events_t line_volt_event)
{
    static uint8_t sm_busy = 0;

    event_q[event_q_pow] = line_volt_event;
    event_q_pow = (event_q_pow+1)%EVENT_Q_SIZE;

    if (0 == sm_busy)
    {
        sm_busy = 1;
        while (event_q_por != event_q_pow)
        {
            if (event_q[event_q_por] < LV_LAST_EVENT)
            {
                switch (current_lv_state)
                {
                case LV_IDLE:
                    to_next_state_from_idle(event_q[event_q_por]);
                    break;

                case LV_RECHARGING:
                    to_next_state_from_recharging(event_q[event_q_por]);
                    break;

                case LV_WAIT_TRIGGER:
                    to_next_state_from_wait_trigger(event_q[event_q_por]);
                    break;

                case LV_HEATING:
                    to_next_state_from_heating(event_q[event_q_por]);
                    break;

                case LV_HEATING_ENDS:
                    to_next_state_from_heating_ends(event_q[event_q_por]);
                    break;

                default:
                    break;
                }
            }
            event_q_por = (event_q_por+1)%EVENT_Q_SIZE;
        }
        sm_busy = 0;
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
line_volt_status_t to_next_state_from_idle(line_volt_mngnt_events_t line_volt_event)
{
    line_volt_status_t status = LV_STATUS_OK;

    switch (line_volt_event)
    {
    case LV_EVENT_START_HEATING:
        log_status(STATUS_HEAT_START_REQUEST);
        config_triac_sequence_with_trigger_start();

        current_lv_state = LV_WAIT_TRIGGER;
        break;

    default:
        status = LV_STATUS_ERROR;
        break;
    }

    return status;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
line_volt_status_t to_next_state_from_recharging(line_volt_mngnt_events_t line_volt_event)
{
    line_volt_status_t status = LV_STATUS_OK;
    static uint8_t last_pulse_is_set = 0;
    static uint8_t stop_heating_requested = 0;

    switch (line_volt_event)
    {
    case LV_EVENT_TIMER_PERIOD_UPDATE:
        if (last_pulse_is_set)
        {
            last_pulse_is_set = 0;
            if (stop_heating_requested)
            {
                stop_heating_requested = 0;
                reset_pwm_timer_registers();
                setup_io_idle_state();
                current_lv_state = LV_IDLE;  
            }
            else
            {
                log_status(STATUS_HEAT_ENDS_WAIT_NEXT_TRIGGER);
                config_triac_sequence_with_trigger_start();

                current_lv_state = LV_WAIT_TRIGGER;
            }
        }
        break;

    case LV_EVENT_TRIAC_QIII_PULSE_COMPLETED:
        log_status(STATUS_GHOST_PULSE_COMPLETED);

        set_last_pulse();
        last_pulse_is_set = 1;
        break;

    case LV_EVENT_STOP_HEATING:
        log_status(STATUS_HEAT_END_REQUEST);
        stop_heating_requested = 1;
        break;
        
    default:
        status = LV_STATUS_ERROR;
        break;
    }

    return status;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
line_volt_status_t to_next_state_from_wait_trigger(line_volt_mngnt_events_t line_volt_event)
{
    line_volt_status_t status = LV_STATUS_OK;

    switch (line_volt_event)
    {
    case LV_EVENT_START_TRIGGER_DETECTED:
        log_status(STATUS_WAIT_START_TRIGGER);
        start_pulse_sequence();

        current_lv_state = LV_HEATING;
        break;

    case LV_EVENT_STOP_HEATING:
        log_status(STATUS_HEAT_END_REQUEST);
        reset_pwm_timer_registers();
        setup_io_idle_state();
        current_lv_state = LV_IDLE;        
        break;
        
    default:
        status = LV_STATUS_ERROR;
        break;
    }

    return status;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
line_volt_status_t to_next_state_from_heating_ends(line_volt_mngnt_events_t line_volt_event)
{
    line_volt_status_t status = LV_STATUS_OK;
    static uint8_t last_pulse_is_set = 0;
    static uint8_t start_heating_requested = 0;

    switch (line_volt_event)
    {
    case LV_EVENT_TIMER_PERIOD_UPDATE:
        if (last_pulse_is_set)
        {
            last_pulse_is_set = 0;
            if (start_heating_requested)
            {
                start_heating_requested = 0;
                log_status(STATUS_HEAT_START_REQUEST);
                config_triac_sequence_with_trigger_start();

                current_lv_state = LV_WAIT_TRIGGER;
            }
            else
            {
                log_status(STATUS_HEAT_ENDS_GO_TO_IDLE);
                reset_pwm_timer_registers();
                setup_io_idle_state();
                current_lv_state = LV_IDLE;  
            }
        }
        break;

    case LV_EVENT_TRIAC_QIII_PULSE_COMPLETED:
        log_status(STATUS_HEAT_ENDS_GHOST_PULSE_COMPLETED);
        set_last_pulse();
        last_pulse_is_set = 1;
        break;

    case LV_EVENT_START_HEATING:
        start_heating_requested = 1;
        break;
        
    default:
        status = LV_STATUS_ERROR;
        break;
    }

    return status;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
line_volt_status_t to_next_state_from_heating(line_volt_mngnt_events_t line_volt_event)
{
    static uint8_t stop_heating_request = 0;
    line_volt_status_t status = LV_STATUS_OK;

    switch (line_volt_event)
    {
    case LV_EVENT_TIMER_PERIOD_UPDATE:
        if (isTimerUpCounting())
        {
            if (0 != stop_heating_request)
            {
                stop_heating_request = 0;
                stop_triac_sequence();

                current_lv_state = LV_HEATING_ENDS;
            }
            else if (isVCapLow())
            {
                stop_triac_sequence();
                log_status(STATUS_RECHARGE_NEEDED);
                current_lv_state = LV_RECHARGING;
            }
            else
            {
                log_status(STATUS_HEAT_ZC_UPDATE);
                UpdatePeriod();
            }
        }
        break;

    case LV_EVENT_STOP_HEATING:
        log_status(STATUS_HEAT_END_REQUEST);
        stop_heating_request = 1;
        break;

    default:
        status = LV_STATUS_ERROR;
        break;
    }

    return status;
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void Init_Heating_Outputs(void)
{
    Init_HeatingPeripherals();

    current_lv_state = LV_IDLE;

    memset(event_q, LV_NO_EVENT,EVENT_Q_SIZE);
    event_q_por = 0;
    event_q_pow = 0;

#ifdef USE_HEATMNGMT_TRACE_BUFFER
    for (uint8_t i = 0; i < TRACE_BUFFER_SIZE; i++)
    {
        trace_buffer[i].step_status = (trace_status_t)-1;
        trace_buffer[i].time_stamp = -1;
    }
    trace_cnt = 0;
#endif
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void ActivateHeatingDevice(void)
{
    output_status = 1;
    post_lv_events(LV_EVENT_START_HEATING);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void DeactivateHeatingDevice(void)
{
    output_status = 0;
    post_lv_events(LV_EVENT_STOP_HEATING);
}


/** Copyright(C) 2018 Stelpro Design, All Rights Reserved**/
