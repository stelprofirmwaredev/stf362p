/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2017, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    HAL_LoadManagement.c
* @date    2017/01/04
* @authors Jean-Fran�ois Many
* @brief   Load Management Module
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include "typedef.h"
#include "stm32f4xx_hal.h"
#include "HAL_Interrupt.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\Sensors\inc\THREAD_Sensors.h"
#include ".\HAL\inc\HAL_LoadManagement.h"
#include ".\HAL\inc\HAL_CurrentSensing.h"
#include ".\HAL\inc\TemperatureControl.h"
#include ".\HAL\inc\Triac_Management.h"
#include "common_types.h"
#include "WindowMonitoring.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
//Control parameters
typedef struct
{
    int8u PercentMin;           //  PercentMin : Minimum allowed percent to avoid short ON cycle
    int8u PercentMax;           //  PercentMax : Maximum allowed percent to avoid short OFF cycle
    int16u onePercent;          //  OnePercent : number of os tick for 1 percent increment
} CycleParam_t;

//Medium cycle 1 percent increment = 5 minutes * 60 sec/min * 1000 ms/sec / 100 (every 3 seconds)
#define ONE_PERCENT_MEDIUM_CYCLE        (5*60*1000L/100)

//Short cycle 1 percent increment = 15 seconds * 1000 ms/sec / 100 (every 150 miliseconds)
#define ONE_PERCENT_SHORT_CYCLE         (15*1000/100)

#define ONE_HOUR_IN_MS                  (3600000)   //(1000 * 60 * 60)

#define VOLTAGE_THRESHOLD_240   100   //Vcap drop frequency less than 10Hz = 240V
                                      //100 * 1ms = 0.100 second = 10 Hz
                                      //An higher number means a lower frequency

#define MININUM_CURRENT     1.02    //If the read current is below 1.02A, it's because the load is not active, set the overheating alert
                                    //1.02A is the absolute minimum current that can be considered valid
                                    //300W -10% = 270W @240V +15% = 264V : 270W/264V = 1.02A
                                    //150W -10% = 135W @120V +15% = 132V : 135W/132V = 1.02A

#define VCap_Pin                                    GPIO_PIN_1
#define VCap_GPIO_Port                              GPIOG

#ifdef LOAD_CYCLING
    #define LOAD_CYCLING_COUNT_OVERLOAD             50
    #define LOAD_CYCLING_COUNT_ENDURANCE_110_PCT    6000
    #define LOAD_CYCLING_COUNT_ENDURANCE_RATED      30000
#endif

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
//Control parameters for Triac with short cycle
static CycleParam_t const * param;            //pointer to estimate parameters in flash
static const CycleParam_t CycleParamsTriacShortCycle =    /* Triac Base short Cycles */
{
    5,                                              //  PercentMin : Minimum allowed percent to avoid short ON cycle
    95,                                             //  PercentMax : Maximum allowed percent to avoid short OFF cycle
    ONE_PERCENT_SHORT_CYCLE,                        //  OnePercent : number of os tick for 1 percent increment
};

static const CycleParam_t CycleParamsTriacMediumCycle =   /* Fan Base medium Cycles */
{
    10,                                             //  PercentMin : Minimum allowed percent to avoid short ON cycle
    90,                                             //  PercentMax : Maximum allowed percent to avoid short OFF cycle
    ONE_PERCENT_MEDIUM_CYCLE,                       //  OnePercent : number of os tick for 1 percent increment
};

static uint8_t Cycle;          //Cycle Percent
static uint8_t CycleStatus;    // 1 => output has cycled ON-OFF
static ControlHeatState_e OutputState;    //Output State
static uint16_t CycleOnTime;    //Triac On time in ms for 1 cycle
static float DailyOnTime;    //Cumulative Triac On time in ms for 1 day
static uint32_t DailyCycleNumber;   //Number of cycle with the Triac activated
static float DailyMeanCurrent;  //Mean current for the day
static uint16_t LocalPowerConsumption;  //Total consumed power for this thermostat for 1 day
static TimerHandle_t CycleTimeout = NULL;
static U8BIT heatDemandState = FALSE;

static uint32_t VcapDropFrequency = 0;
static uint8_t DetectedVoltage = 240;
static uint32_t RandomStart = 0;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
extern dbType_Residence_t           CurrentResidence;
RNG_HandleTypeDef hrng;

#ifdef ON_OFF
uint8_t ForceOnState;
#endif /* ON_OFF */

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static void LoadManagement_DoCycle(TimerHandle_t xTimer);
static void LoadLimitMinMax(int8u * value, int8u min, int8u max);
DB_NOTIFICATION(OnHeatModeChange);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
#ifdef LOAD_CYCLING
/*******************************************************************************
*   \function : void LoadManagement_DoCycle(TimerHandle_t xTimer)
*	\brief :    Cycle management
*	\param :    None
*	\return :   None
********************************************************************************/
#pragma optimize = none
static void LoadManagement_DoCycle(TimerHandle_t xTimer)
{
    static int16u TimeCounter;
    uint8_t load_cycling_mode;
    uint16_t load_cycling_count;
    uint16_t on_time;
    uint16_t off_time;
    uint16_t max_cnt;
    static uint8_t last_load_cycling_mode = 0;

    //Call every 500 ms while is OFF
    xTimerChangePeriod(CycleTimeout, pdMS_TO_TICKS(500), 0);

    DBTHRD_GetData(DBTYPE_LOAD_CYCLING_MODE,(void*)&load_cycling_mode, THIS_THERMOSTAT);
    DBTHRD_GetData(DBTYPE_LOAD_CYCLING_COUNT,(void*)&load_cycling_count, THIS_THERMOSTAT);

    if (load_cycling_mode != last_load_cycling_mode)
    {
        TimeCounter = 0;
        load_cycling_count = 0;
        DBTHRD_SetData(DBTYPE_LOAD_CYCLING_COUNT, (void*)&load_cycling_count, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    }
    last_load_cycling_mode = load_cycling_mode;

    switch (load_cycling_mode)
    {
        case 0:
            on_time = 0;
            off_time = 0;
            break;

        case 1:
            on_time = 10;
            off_time = 90;
            break;

        case 2:
            on_time = 300;
            off_time = 300;
            break;

        case 3:
            on_time = 300;
            off_time = 300;
            break;

        default:
            on_time = 0;
            off_time = 0;
            break;
    }

    BSP_InterruptDeclareCritical();
    BSP_InterruptEnterCritical();

    if(on_time)                  //There is ON Time programmed
    {
        if(off_time == 0)        //No OFF time = Always ON
        {
            Set_OutputDemand(TRUE, HEAT_UNLOCKED);
            LoadManagement_SetOutputState(CONTROL_HEAT_STATE_ON, HEAT_UNLOCKED);
            CTL_SetHeatDemand(100);
        }
        else if(TimeCounter < off_time)   //OFF Time
        {
            Set_OutputDemand(FALSE, HEAT_UNLOCKED);
            LoadManagement_SetOutputState(CONTROL_HEAT_STATE_OFF, HEAT_UNLOCKED);
            CTL_SetHeatDemand(0);
        }
        else                            //ON Time
        {
            Set_OutputDemand(TRUE, HEAT_UNLOCKED);
            LoadManagement_SetOutputState(CONTROL_HEAT_STATE_ON, HEAT_UNLOCKED);
            CTL_SetHeatDemand(100);
        }
        TimeCounter += 5;

        if(TimeCounter >= on_time + off_time)
        {
            TimeCounter = 0;
            load_cycling_count++;
            DBTHRD_SetData(DBTYPE_LOAD_CYCLING_COUNT, (void*)&load_cycling_count, THIS_THERMOSTAT, DBCHANGE_LOCAL);
        }
    }
    else
    {
        //No OnTime programmed - remain OFF
        TimeCounter = 0;
        Set_OutputDemand(FALSE, HEAT_UNLOCKED);
        LoadManagement_SetOutputState(CONTROL_HEAT_STATE_OFF, HEAT_UNLOCKED);
        CTL_SetHeatDemand(0);
    }

    switch(load_cycling_mode)
    {
        case 0:
            //No test started
            load_cycling_count = 0;
            DBTHRD_SetData(DBTYPE_LOAD_CYCLING_COUNT, (void*)&load_cycling_count, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            CTL_SetHeatDemand(0);
            break;

        case 1:
            //Overload test
            max_cnt = LOAD_CYCLING_COUNT_OVERLOAD;
            if (load_cycling_count >= max_cnt)
            {
                CTL_SetHeatDemand(0);
                load_cycling_mode = 0;
                DBTHRD_SetData(DBTYPE_LOAD_CYCLING_MODE, (void*)&load_cycling_mode, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            }
            break;

        case 2:
            //Endurance test (110%)
            max_cnt = LOAD_CYCLING_COUNT_ENDURANCE_110_PCT;
            if (load_cycling_count >= max_cnt)
            {
                CTL_SetHeatDemand(0);
                load_cycling_mode = 0;
                DBTHRD_SetData(DBTYPE_LOAD_CYCLING_MODE, (void*)&load_cycling_mode, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            }
            break;

        case 3:
            //Endurance test (nominal)
            max_cnt = LOAD_CYCLING_COUNT_ENDURANCE_RATED;
            if (load_cycling_count >= max_cnt)
            {
                CTL_SetHeatDemand(0);
                load_cycling_mode = 0;
                DBTHRD_SetData(DBTYPE_LOAD_CYCLING_MODE, (void*)&load_cycling_mode, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            }
            break;

        default:
            break;
    }

    // Leave critical
    BSP_InterruptExitCritical();
}
#elif defined (ON_OFF)
#pragma optimize = none
static void LoadManagement_DoCycle(TimerHandle_t xTimer)
{
    //Call every 500 ms while is OFF
    xTimerChangePeriod(CycleTimeout, pdMS_TO_TICKS(500), 0);

    BSP_InterruptDeclareCritical();
    BSP_InterruptEnterCritical();
    if (ForceOnState)
    {
        Set_OutputDemand(TRUE, HEAT_UNLOCKED);
        LoadManagement_SetOutputState(CONTROL_HEAT_STATE_ON, HEAT_UNLOCKED);
        CTL_SetHeatDemand(100);
    }
    else
    {
        Set_OutputDemand(FALSE, HEAT_UNLOCKED);
        LoadManagement_SetOutputState(CONTROL_HEAT_STATE_OFF, HEAT_UNLOCKED);
        CTL_SetHeatDemand(0);
    }
    // Leave critical
    BSP_InterruptExitCritical();
}
#else
/*******************************************************************************
*   \function : void LoadManagement_DoCycle(TimerHandle_t xTimer)
*	\brief :    Cycle management
*	\param :    None
*	\return :   None
********************************************************************************/
static void LoadManagement_DoCycle(TimerHandle_t xTimer)
{
    uint8_t heatDemand;

    TEMPERATURE_C_t temperature;
    static int8u Pct_startcycle = 0;
    static int8u Pct_endcycle = 0;
    static TEMPERATURE_C_t Temp_startcycle = 0;
    static TEMPERATURE_C_t Temp_endcycle = 0;
    int8s delta_demand = 0;
    TEMPERATURE_C_t delta_temperature = 0;
    float current;
    static float buffer_float;
    uint8_t alert;

    DBTHRD_GetData(DBTYPE_HEAT_DEMAND,(void*)&heatDemand, THIS_THERMOSTAT);
    LoadLimitMinMax(&heatDemand,param->PercentMin,param->PercentMax);
    Cycle += 1;
    if(Cycle >= 100)                                //Restart a new cycle
    {
        //If heat demand = 100%, triac is still on, so set the cycle on time to 100%
        if (heatDemand == 100)
        {
            CycleOnTime = 100;
        }
        //Heating was active during last cycle, so we recalculate the power consumption
        if (CycleOnTime > 0)
        {
            DailyCycleNumber++;

            //Add the On time from this cycle to the cumulative On time for the day in ms
            DailyOnTime += (float)(CycleOnTime * param->onePercent);

            //Detect the operating voltage
            if (HAL_GPIO_ReadPin(VCap_GPIO_Port,VCap_Pin) != 0)
            {
                DetectedVoltage = 240;
            }
            else
            {
                DetectedVoltage = 120;
            }

            //Calculate the average current for the day
            current = HAL_CurrentSensor_GetCurrent();
            DailyMeanCurrent = (DailyMeanCurrent * (DailyCycleNumber - 1) + current) / DailyCycleNumber;

            //Calculate the daily consumption in kWh
            buffer_float = ((DetectedVoltage * DailyMeanCurrent) * (DailyOnTime / ONE_HOUR_IN_MS)) / 1000;
            LocalPowerConsumption = (uint16_t)buffer_float;
            if ((buffer_float - LocalPowerConsumption) >= 0.5)
            {
                LocalPowerConsumption++;
            }
            //Make sure we report something if a valid load has been activated during the day
            if ((LocalPowerConsumption == 0) && (DailyMeanCurrent >= MININUM_CURRENT) && (DailyCycleNumber != 0))
            {
                LocalPowerConsumption = 1;
            }
            DBTHRD_SetData(DBTYPE_LOCAL_DAILY_CONSUMPTION,
                                (void*)&LocalPowerConsumption,
                                THIS_THERMOSTAT,
                                DBCHANGE_LOCAL);
            if (DailyMeanCurrent < MININUM_CURRENT)
            {
                alert = DB_SET_ALERT | DBALERT_OVERHEAT;
                DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            }
            else
            {
                alert = DB_CLEAR_ALERT | DBALERT_OVERHEAT;
                DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            }
        }

        SetCurrentSensingPermit(TRUE);
        Cycle = 0;
        CycleStatus = 0;
        CycleOnTime = 0;
        Pct_endcycle = Pct_startcycle;
        Temp_endcycle = Temp_startcycle;
        Pct_startcycle = heatDemand;
        DBTHRD_GetData(DBTYPE_CONTROL_TEMPERATURE,(void*)&temperature, INDEX_DONT_CARE);
        Temp_startcycle = temperature;
        delta_demand = Pct_startcycle - Pct_endcycle;
        delta_temperature = Temp_startcycle - Temp_endcycle;
        WM_SetDelta(delta_demand, delta_temperature);
    }

    BSP_InterruptDeclareCritical();
    BSP_InterruptEnterCritical();
    if ((heatDemand > Cycle) && (CycleStatus == 0))
    {
        Set_OutputDemand(TRUE, HEAT_UNLOCKED);
        LoadManagement_SetOutputState(CONTROL_HEAT_STATE_ON, HEAT_UNLOCKED);
    }
    else
    {
        Set_OutputDemand(FALSE, HEAT_UNLOCKED);
        LoadManagement_SetOutputState(CONTROL_HEAT_STATE_OFF, HEAT_UNLOCKED);

        //Make sure the triac was activated before
        if (CycleOnTime > 0)
        {
            CycleStatus = 1;        //to prevent reactivation of the output until the end of the control cycle
        }
    }
    // Leave critical
    BSP_InterruptExitCritical();
}
#endif /* LOAD_CYCLING */

/*******************************************************************************
* @brief  Notification when heat mode changes (Fan/Baseboard)
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/02/10
*******************************************************************************/
DB_NOTIFICATION(OnHeatModeChange)
{
    LoadManagement_StartNewCycle(FALSE);
}

/*******************************************************************************
//  \function : void LoadLimitMinMax(int8u * value, int8u min, int8u max){
//	\brief :    Clip value to load minimum and maximum applicable percent
//	\param :    int8u * value = percent requested
//              int8u min = min applicable percent
//              int8u max = max applicable percent
//	\return :   none, modified int8u * value
********************************************************************************/
static void LoadLimitMinMax(int8u * value, int8u min, int8u max)
{
    if(*value < min)                //Below min: 0
    {
        *value = 0;
    }
    else if(*value >= 100)          //Absolute max is 100
    {
        *value = 100;
    }
    else if(*value > max)           //Between max and 100:
    {
        //*value = max;				//use max
        *value = 100;				//use 100
    }
    else
    {
        //otherwise use as is
    }
}

/*******************************************************************************
* @brief  Set the Output desired state
* @inputs state: CONTROL_HEAT_STATE_OFF or CONTROL_HEAT_STATE_ON
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/02/10
*******************************************************************************/
void Set_OutputDemand(U8BIT state, U8BIT priority)
{
    static U8BIT demandLocked = FALSE;

    if (priority == HEAT_PRIORITY)
    {
        demandLocked = TRUE;
    }
    if (priority == HEAT_CANCEL_PRIORITY)
    {
        demandLocked = FALSE;
    }
    if ((demandLocked == FALSE) || (priority == HEAT_PRIORITY))
    {
        if ((heatDemandState == TRUE) && (state == FALSE))
        {
            //End of heating phase, save CycleOnTime
            CycleOnTime = Cycle;
        }

        heatDemandState = state;
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void init_vcap_monitoring(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    GPIO_InitStruct.Pin = VCap_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Pull = GPIO_PULLUP;

    HAL_GPIO_Init(VCap_GPIO_Port, &GPIO_InitStruct);
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  Load Management initialization function
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/04
*******************************************************************************/
void LoadManagement_Init(void)
{
    Init_Heating_Outputs();

    OutputState = CONTROL_HEAT_STATE_OFF;
    Cycle = 0;                      //Cycle Percent
    CycleStatus = 0;                //New cycle
    CycleOnTime = 0;
    DailyOnTime = 0;
    DailyMeanCurrent = 0;
    LocalPowerConsumption = 0;
    DailyCycleNumber = 0;
    CTL_SetHeatDemand(0);           //Applied Percent

    //Enable the Random generator for the random start
    __HAL_RCC_RNG_CLK_ENABLE();
    hrng.Instance = RNG;
    HAL_RNG_Init(&hrng);
    HAL_RNG_GenerateRandomNumber(&hrng, &RandomStart);
    RandomStart = (uint16_t)(15000 * ((float)RandomStart / 0xFFFFFFFF));


    DBTHRD_SubscribeToNotification(DBTYPE_HEAT_MODE_SETTING,
                                   OnHeatModeChange,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);

    LoadManagement_StartNewCycle(1);    //start new cycle with random start


    init_vcap_monitoring();
}

/*******************************************************************************
*   \function : void LOAD_SetOutputState(ControlHeatState_e state, U8BIT priority)
*	\brief :    Change the status of the output state
*	\param :    None
*	\return :   None
********************************************************************************/
void LoadManagement_SetOutputState(ControlHeatState_e state, U8BIT priority)
{
    static U8BIT locked = FALSE;
    static U8BIT priorityRequest = FALSE;
    static ControlHeatState_e priorityState = CONTROL_HEAT_STATE_OFF;

    if(state < MAX_CONTROL_HEAT_STATE)
    {
        if (priority == HEAT_PRIORITY)  //Check if a particular state is requested (test mode)
        {
            priorityRequest = TRUE;     //  Remember to return to this state every time we can
            priorityState = state;      //  Save it for later
        }
        if (priority == HEAT_CANCEL_PRIORITY)//End of test mode
        {
            priorityRequest = FALSE;    //  Let the code manage the heat normally
        }
        if (priority == HEAT_RELEASE)   //If normal operation is allowed (Vcap is up), release the lock and apply the requested state, if any
        {
            locked = FALSE;
        }
        if ((locked != TRUE) || (priority == HEAT_LOCKED))
        {
            if ((priorityRequest == TRUE) && (priority != HEAT_LOCKED)) //If no locked state defined AND particular state is requested
            {
                state = priorityState;                                  //  Force return to the requested state
            }
            if ((OutputState != state) || (priority == HEAT_LOCKED))    //If state has changed OR if locked state defined
            {
                if(state == CONTROL_HEAT_STATE_OFF)
                {
                    DeactivateHeatingDevice();
                }
                else
                {
                    ActivateHeatingDevice();
                    if (IsCurrentSensingPermit() == TRUE)
                    {
                        SetCurrentSensingPermit(FALSE);
                        SetCurrentSensingRequest(TRUE);
                    }
                }
                OutputState = state;
            }
        }
        if (priority == HEAT_LOCKED)
        {
            locked = TRUE;
        }
    }
}

/*******************************************************************************
*   \function : void LoadManagement_StartNewCycle(uint8_t fromCloud){
*	\brief :    Begin a new heating cycle
*	\param :    fromCloud: new cycle triggered from cloud (TRUE or FALSE)
*	\return :   None
********************************************************************************/
void LoadManagement_StartNewCycle(uint8_t apply_random_delay)
{
    HeatMode_t heatMode;

    Cycle = 0;
    CycleStatus = 0;
    if (apply_random_delay == TRUE)
    {
        osDelay(RandomStart);
    }

    DBTHRD_GetData(DBTYPE_HEAT_MODE_SETTING,(void*)&heatMode, INDEX_DONT_CARE);
    if(heatMode == HEATMODE_FAN)
    {
        //Fan cycle - do 5 minutes cycles
        //at 1 percent increment
        param = &CycleParamsTriacMediumCycle;
    }
    else
    {
        //Short cycle - do 15 sec cycles
        //at 1 percent increment
        param = &CycleParamsTriacShortCycle;
    }

    if (CycleTimeout == NULL)
    {
        CycleTimeout = xTimerCreate ("Cycle Timeout",
                                     pdMS_TO_TICKS(param->onePercent),
                                     pdTRUE,               //one shot timer
                                     (void*)0,
                                     LoadManagement_DoCycle);
        xTimerStart(CycleTimeout, 0);
    }
    else
    {
        xTimerStop(CycleTimeout, 0);
        xTimerChangePeriod(CycleTimeout, pdMS_TO_TICKS(param->onePercent), 0);
    }
    LoadManagement_DoCycle(CycleTimeout);

}

/*******************************************************************************
*   \function : void LoadManagement_StartNewDay(void){
*	\brief :    Begin a new day (reset daily on time)
*	\param :    None
*	\return :   None
********************************************************************************/
void LoadManagement_StartNewDay(void)
{
    DailyOnTime = 0;
    DailyCycleNumber = 0;
    DailyMeanCurrent = 0;
    LocalPowerConsumption = 0;
    DBTHRD_SetData(DBTYPE_LOCAL_DAILY_CONSUMPTION, (void*)&LocalPowerConsumption, THIS_THERMOSTAT, DBCHANGE_LOCAL);
}

/*******************************************************************************
* @brief  Get the Output desired state
* @inputs None
* @retval CONTROL_HEAT_STATE_OFF or CONTROL_HEAT_STATE_ON
* @author Jean-Fran�ois Many
* @date 2017/02/10
*******************************************************************************/
U8BIT Get_OutputDemand(void)
{
    return heatDemandState;
}

uint32_t Get_VcapDropFrequency(void)
{
    return VcapDropFrequency;
}

uint8_t Get_DetectedVoltage(void)
{
    return DetectedVoltage;
}

/** Copyright(C) 2017 Stelpro Design, All Rights Reserved**/
