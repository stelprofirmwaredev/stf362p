/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2019, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/
/*******************************************************************************
* @file    HAL_PowerManagement.c
* @date    2019/11/26
* @authors Santiago P
* @brief   PowerManagement is made to reduce power consumption on supercap
           bas� sur code de Thinc
*******************************************************************************/
/*******************************************************************************
*    Includes
*******************************************************************************/
#include "stm32f4xx_hal.h"
#include "HAL_Interrupt.h"
#include "stm32f4xx_hal_pwr.h"
#include ".\HAL\inc\HAL_PowerManagement.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
/* 11 is an arbitrary value between responsivness of a wakeup event and
    supercap retention capacity.  The longer in standby mode, the longer the
    supercap will last long */
#define WAKE_UP_DELAY       11

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
extern RTC_HandleTypeDef RTC_Handle;
/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
void EnterStandbyMode(void);

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void EnterStandbyMode(void)
{
    HAL_NVIC_SetPriority(RTC_WKUP_IRQn, 0x0, 0);
    HAL_NVIC_EnableIRQ(RTC_WKUP_IRQn);

    HAL_RTCEx_SetWakeUpTimer_IT(&RTC_Handle, WAKE_UP_DELAY, RTC_WAKEUPCLOCK_CK_SPRE_16BITS);
    
    /* Enter Standby mode */
    HAL_PWR_EnterSTANDBYMode();
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void PowerManagement_Init(void)
{
    PWR_PVDTypeDef sConfigPVD;
    /* configure PVD */
    /* Check and handle if the system was resumed from StandBy mode */
    if(__HAL_PWR_GET_FLAG(PWR_FLAG_SB) != RESET)
    {
        /* Clear Standby flag */
        __HAL_PWR_CLEAR_FLAG(PWR_FLAG_SB);
    }

    sConfigPVD.PVDLevel = PWR_PVDLEVEL_6; //compare to 2.8 V
    sConfigPVD.Mode = PWR_PVD_MODE_NORMAL;

    HAL_PWR_ConfigPVD(&sConfigPVD);
    HAL_PWR_EnablePVD();
    
    /* if PVD is below voltage threshold, stop config and enter stby mode
        The RTC wakeup event/interrupt will wake up the MCU on the next event */
    if(__HAL_PWR_GET_FLAG(PWR_FLAG_PVDO) == SET)
    {
        EnterStandbyMode();
    }
    /* else, enable the PVD interrupt */
    else
    {

        sConfigPVD.Mode = PWR_PVD_MODE_IT_RISING;
        HAL_PWR_DisablePVD();

        HAL_PWR_ConfigPVD(&sConfigPVD);
        HAL_PWR_EnablePVD();

        __HAL_PWR_PVD_EXTI_CLEAR_FLAG();
        HAL_NVIC_SetPriority(PVD_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(PVD_IRQn);
    }
}



/*******************************************************************************
*    ISR
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void HAL_PWR_PVDCallback(void)
{
    if(__HAL_PWR_GET_FLAG(PWR_FLAG_PVDO) == SET)
    {
        EnterStandbyMode();
    }
}

/** Copyright(C) 2019 Stelpro Design, All Rights Reserved**/
