/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2017, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    app_test_bench.c
* @date    2017/04/25
* @authors Jean-Fran�ois Many
* @brief   Production Test Bench module
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include ".\DB\inc\THREAD_DB.h"
#include ".\HAL\inc\HAL_Console.h"
#include ".\HAL\inc\app_test_bench.h"
#include "OsTask.h"
#include "MenuHandler.h"
#include "MenuTable.h"
#include ".\HAL\inc\HAL_LoadManagement.h"
#include ".\BSP\TFT\OSD035T2631-65TS\inc\TFT_ST7796.h"
#include "HAL_Interrupt.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/

/*******************************************************************************
*    Private structure definitions
*******************************************************************************/
typedef struct
{
    uint8_t   command;
    uint8_t   byte1;
    uint8_t   byte2;
    uint8_t   byte3;
}DebugBuffer_t;


/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
static DebugBuffer_t DebugBuffer;
static U8BIT DebugCommand;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/


/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/


/*******************************************************************************
    External functions prototypes
*******************************************************************************/


/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
void ProcessTestCommand(uint8_t command)
{
    int16_t temp;
    U16BIT ctrl_ver;

    DebugCommand = command;
    DebugBuffer.command = command;
    switch (command)
    {
        case APP_TBCH_GET_PROTOCOL_VERSION:
            DebugBuffer.byte1 = TBCH_PROTOCOL_VERSION_MAJOR;
            DebugBuffer.byte2 = TBCH_PROTOCOL_VERSION_MINOR;
            DebugBuffer.byte3 = NULL;
            break;

        case APP_TBCH_TRIAC_ON:
            DebugBuffer.byte1 = NULL;
            DebugBuffer.byte2 = NULL;
            DebugBuffer.byte3 = NULL;
            OS_DelayTask(TSK_HAL_DEBUG_ACTION, OS_MSTOTICK(250));
            break;

        case APP_TBCH_TRIAC_OFF:
            DebugBuffer.byte1 = NULL;
            DebugBuffer.byte2 = NULL;
            DebugBuffer.byte3 = NULL;
            OS_DelayTask(TSK_HAL_DEBUG_ACTION, OS_MSTOTICK(250));
            break;

        case APP_TBCH_GET_TEMP_AMBIENT:
            DBTHRD_GetData(DBTYPE_AMBIENT_TEMPERATURE_READING,(void*)&temp, THIS_THERMOSTAT);
            DebugBuffer.byte1 = (uint8_t)((temp & 0xFF00) >> 8);
            DebugBuffer.byte2 = (uint8_t)(temp & 0x00FF);
            DebugBuffer.byte3 = NULL;
            break;

        case APP_TBCH_GET_TEMP_COMPENSATION:
            DBTHRD_GetData(DBTYPE_COMPENSATION_TEMPERATURE_READING,(void*)&temp, THIS_THERMOSTAT);
            DebugBuffer.byte1 = (uint8_t)((temp & 0xFF00) >> 8);
            DebugBuffer.byte2 = (uint8_t)(temp & 0x00FF);
            DebugBuffer.byte3 = NULL;
            break;

        case APP_TBCH_GET_TEMP_TRIAC:
            DBTHRD_GetData(DBTYPE_HIGH_TEMPERATURE_READING,(void*)&temp, THIS_THERMOSTAT);
            DebugBuffer.byte1 = (uint8_t)((temp & 0xFF00) >> 8);
            DebugBuffer.byte2 = (uint8_t)(temp & 0x00FF);
            DebugBuffer.byte3 = NULL;
            break;

        case APP_TBCH_SET_DEFAULT:
            DebugBuffer.byte1 = NULL;
            DebugBuffer.byte2 = NULL;
            DebugBuffer.byte3 = NULL;
            OS_DelayTask(TSK_HAL_DEBUG_ACTION, OS_MSTOTICK(250));
            break;

        case APP_TBCH_SET_LEDS_ON:
            DebugBuffer.byte1 = NULL;
            DebugBuffer.byte2 = NULL;
            DebugBuffer.byte3 = NULL;
            TFT_BACKLIGHT_ON;
            break;

        case APP_TBCH_SET_LEDS_OFF:
            DebugBuffer.byte1 = NULL;
            DebugBuffer.byte2 = NULL;
            DebugBuffer.byte3 = NULL;
            TFT_BACKLIGHT_OFF;
            break;

        case APP_TBCH_ENABLE_TEST_MODE:
            DebugBuffer.byte1 = NULL;
            DebugBuffer.byte2 = NULL;
            DebugBuffer.byte3 = NULL;
            OS_DelayTask(TSK_HAL_DEBUG_ACTION, OS_MSTOTICK(250));
            break;

        case APP_TBCH_DISABLE_TEST_MODE:
            DebugBuffer.byte1 = NULL;
            DebugBuffer.byte2 = NULL;
            DebugBuffer.byte3 = NULL;
            OS_DelayTask(TSK_HAL_DEBUG_ACTION, OS_MSTOTICK(250));
            break;

        case APP_TBCH_DISPLAY_BLACK_SCREEN:
            DebugBuffer.byte1 = NULL;
            DebugBuffer.byte2 = NULL;
            DebugBuffer.byte3 = NULL;
            OS_DelayTask(TSK_HAL_DEBUG_ACTION, OS_MSTOTICK(250));
            break;

        case APP_TBCH_DISPLAY_WHITE_SCREEN:
            DebugBuffer.byte1 = NULL;
            DebugBuffer.byte2 = NULL;
            DebugBuffer.byte3 = NULL;
            OS_DelayTask(TSK_HAL_DEBUG_ACTION, OS_MSTOTICK(250));
            break;

        case APP_TBCH_DISPLAY_TOUCH_ZONES:
            DebugBuffer.byte1 = NULL;
            DebugBuffer.byte2 = NULL;
            DebugBuffer.byte3 = NULL;
            OS_DelayTask(TSK_HAL_DEBUG_ACTION, OS_MSTOTICK(250));
            break;

        case APP_TBCH_GET_MODEL:
            DebugBuffer.byte1 = APP_TBCH_MAESTRO_CONTROLER;
            DebugBuffer.byte2 = NULL;
            DebugBuffer.byte3 = NULL;
            break;

        case APP_TBCH_GET_VERSION:
            DBTHRD_GetData(DBTYPE_CONTROLLER_VERSION,(void*)&ctrl_ver, INDEX_DONT_CARE);
            DebugBuffer.byte1 = (uint8_t)((ctrl_ver >> 8) & 0x00FF);
            DebugBuffer.byte2 = (uint8_t)(ctrl_ver & 0x00FF);
            DebugBuffer.byte3 = NULL;
            break;

        default:
            DebugBuffer.byte1 = NULL;
            DebugBuffer.byte2 = NULL;
            DebugBuffer.byte3 = NULL;
            break;
    }
    CONSOLE_TEST_COMMAND((char*)&DebugBuffer);
}

#pragma optimize = none
OSTASK_DEF(UART_DEBUG_Action)
{
    BSP_InterruptDeclareCritical();

    switch (DebugCommand)
    {
        case APP_TBCH_TRIAC_ON:
            BSP_InterruptEnterCritical();
            Set_OutputDemand(TRUE, HEAT_PRIORITY);
            LoadManagement_SetOutputState(CONTROL_HEAT_STATE_ON, HEAT_PRIORITY);
            BSP_InterruptExitCritical();
            break;

        case APP_TBCH_TRIAC_OFF:
            BSP_InterruptEnterCritical();
            Set_OutputDemand(FALSE, HEAT_PRIORITY);
            LoadManagement_SetOutputState(CONTROL_HEAT_STATE_OFF, HEAT_PRIORITY);
            BSP_InterruptExitCritical();
            break;

        case APP_TBCH_ENABLE_TEST_MODE:
            break;

        case APP_TBCH_DISABLE_TEST_MODE:
            MNH_SetActiveMenu(MENU_HOME);
            BSP_InterruptEnterCritical();
            Set_OutputDemand(FALSE, HEAT_CANCEL_PRIORITY);
            LoadManagement_SetOutputState(CONTROL_HEAT_STATE_OFF, HEAT_CANCEL_PRIORITY);
            BSP_InterruptExitCritical();
            break;

        case APP_TBCH_DISPLAY_BLACK_SCREEN:
            MNH_SetActiveMenu(MENU_BLACK);
            break;

        case APP_TBCH_DISPLAY_WHITE_SCREEN:
            MNH_SetActiveMenu(MENU_WHITE);
            break;

        case APP_TBCH_DISPLAY_TOUCH_ZONES:
            MNH_SetActiveMenu(MENU_TESTMODE);
            break;

        case APP_TBCH_SET_DEFAULT:
            ClearDatabase(CLEAR_DB_REASON_TEST_BENCH);
            MNH_SetActiveMenu(MENU_LANGUAGE);
            break;

        default:
            break;
    }

    return (OSTASK_IS_IDLE);
}

/** Copyright(C) 2017 Stelpro Design, All Rights Reserved**/
