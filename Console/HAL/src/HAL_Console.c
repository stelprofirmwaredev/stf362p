/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    HAL_Console.c
* @date    2016/08/18
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <string.h>

#include ".\HAL\inc\HAL_Console.h"
#include "stm32f4xx_hal.h"
#include "StringObjects.h"

#ifdef FREERTOS_SUPPORT
    #include ".\Console\inc\THREAD_Console.h"
#endif

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/

typedef enum
{
    CONSOLEMSG_TX_STATE_IDLE = 0,
    CONSOLEMSG_TX_STATE_SENDING,
} consoleMsgTxState_t;

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
typedef struct _consoleMsg_t
{
    char msg [MAX_MSG_LENGTH];
    uint8_t msgLength;

    struct _consoleMsg_t *next;
} consoleMsg_t;


/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
UART_HandleTypeDef hConsoleUart;
#ifdef FREERTOS_SUPPORT
extern SemaphoreHandle_t tx_console_msg_cnt;
    static DMA_HandleTypeDef hdma_uart1_rx;
    static DMA_HandleTypeDef hdma_uart1_tx;
#endif

uint8_t integerString[100];
uint8_t consoleInputBuffer[CONSOLE_INPUT_BUFFER_SIZE];
uint16_t consoleInputBuffer_PoR;
uint16_t consoleInputBuffer_PoW;

consoleMsg_t logMsg;

consoleMsg_t *consoleMsgHead;
consoleMsg_t *consoleMsgTail;
consoleMsgTxState_t consoleMsgTxState;

uint8_t InTestMode = 0;

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
#ifdef FREERTOS_SUPPORT
static void AddConsoleMsgToQ(consoleMsg_t *newMsg);
#endif          //#ifdef FREERTOS_SUPPORT


/*******************************************************************************
*    Private functions definitions
*******************************************************************************/

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/17
*******************************************************************************/
void InitConsole (void)
{
#ifdef ENABLE_CONSOLE
    GPIO_InitTypeDef GPIO_InitStruct;

    __HAL_RCC_USART1_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_DMA2_CLK_ENABLE();

    hConsoleUart.Instance = USART1;
    hConsoleUart.Init.BaudRate = 9600;
    hConsoleUart.Init.WordLength = UART_WORDLENGTH_8B;
    hConsoleUart.Init.StopBits = UART_STOPBITS_1;
    hConsoleUart.Init.Parity = UART_PARITY_NONE;
    hConsoleUart.Init.Mode = UART_MODE_TX_RX;
    hConsoleUart.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    hConsoleUart.Init.OverSampling = UART_OVERSAMPLING_16;

#ifdef FREERTOS_SUPPORT

    hdma_uart1_rx.Instance = DMA2_Stream5;
    hdma_uart1_rx.Init.Channel = DMA_CHANNEL_4;
    hdma_uart1_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
    hdma_uart1_rx.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_uart1_rx.Init.MemInc = DMA_MINC_ENABLE;
    hdma_uart1_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma_uart1_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    hdma_uart1_rx.Init.Mode = DMA_CIRCULAR;
    hdma_uart1_rx.Init.Priority = DMA_PRIORITY_HIGH;
    hdma_uart1_rx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;

    hdma_uart1_tx.Instance = DMA2_Stream7;
    hdma_uart1_tx.Init.Channel = DMA_CHANNEL_4;
    hdma_uart1_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
    hdma_uart1_tx.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_uart1_tx.Init.MemInc = DMA_MINC_ENABLE;
    hdma_uart1_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma_uart1_tx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    hdma_uart1_tx.Init.Mode = DMA_NORMAL;
    hdma_uart1_tx.Init.Priority = DMA_PRIORITY_HIGH;
    hdma_uart1_tx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;


    HAL_DMA_Init(&hdma_uart1_rx);
    __HAL_LINKDMA(&hConsoleUart,hdmarx,hdma_uart1_rx);

    HAL_DMA_Init(&hdma_uart1_tx);
    __HAL_LINKDMA(&hConsoleUart,hdmatx,hdma_uart1_tx);

#endif


    HAL_UART_Init(&hConsoleUart);

#ifdef FREERTOS_SUPPORT

    /* UART1_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(USART1_IRQn, 6, 0);
    HAL_NVIC_EnableIRQ(USART1_IRQn);

    HAL_NVIC_SetPriority(DMA2_Stream7_IRQn, 6, 0);
    HAL_NVIC_EnableIRQ(DMA2_Stream7_IRQn);



    HAL_UART_Receive_DMA(&hConsoleUart, consoleInputBuffer, CONSOLE_INPUT_BUFFER_SIZE);
    consoleInputBuffer_PoR = 0;

    consoleMsgHead = NULL;
    consoleMsgTail = NULL;
    consoleMsgTxState = CONSOLEMSG_TX_STATE_IDLE;
#endif





    /**USART1 GPIO Configuration
    PA9     ------> USART1_TX
    PA10     ------> USART1_RX
    */
    GPIO_InitStruct.Pin = GPIO_PIN_9|GPIO_PIN_10;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

#ifndef DEVKIT_PLATFORM
#ifndef BOOTLOADER
    GPIO_InitStruct.Pin = TestLock_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(TestLock_GPIO_Port, &GPIO_InitStruct);
#endif /* BOOTLOADER */
#endif /* DEVKIT_PLATFORM */

#endif
}

#ifdef FREERTOS_SUPPORT
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/04/24
*******************************************************************************/
static void AddConsoleMsgToQ(consoleMsg_t *newMsg)
{
    consoleMsg_t *newConsoleMsg;

    if (pdTRUE == xSemaphoreTake(tx_console_msg_cnt,pdMS_TO_TICKS(100) ))
    {
        newConsoleMsg = pvPortMalloc(sizeof(consoleMsg_t));

        if (NULL != newConsoleMsg)
        {
            strncpy(newConsoleMsg->msg, newMsg->msg, newMsg->msgLength);
            newConsoleMsg->msgLength = newMsg->msgLength;

            newConsoleMsg->next = NULL;

            if (NULL == consoleMsgHead)
            {
                consoleMsgHead = newConsoleMsg;
                consoleMsgTail = consoleMsgHead;
            }
            else
            {
                consoleMsgTail->next = newConsoleMsg;
                consoleMsgTail = consoleMsgTail->next;
            }

            xTaskNotify(consoleTaskHandle,CONSOLE_NOTIFICATION_SEND_NEW_MESSAGE,eSetBits);
        }
        else
        {
            xSemaphoreGive(tx_console_msg_cnt);
        }
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/04/24
*******************************************************************************/
void SendNextConsoleMsg (void)
{
    if (NULL != consoleMsgHead)
    {
        switch (consoleMsgTxState)
        {
        case CONSOLEMSG_TX_STATE_IDLE:
            if (HAL_ERROR != HAL_UART_Transmit_DMA (&hConsoleUart, (uint8_t*)consoleMsgHead->msg, consoleMsgHead->msgLength))
            {
                consoleMsgTxState = CONSOLEMSG_TX_STATE_SENDING;
            }
            else
            {
                FreeLastSentConsoleMsg();
            }
            break;

        case CONSOLEMSG_TX_STATE_SENDING:
        default:
            break;
        }
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/04/24
*******************************************************************************/
void FreeLastSentConsoleMsg (void)
{
/************************************************************************/
/*	Critical section to avoid another thread requests this heap memory  */
/*  before the end of the free operations. The data pointed by          */
/*  consoleMsgHead must remains integral until the end of the loop.	    */
/************************************************************************/
    taskENTER_CRITICAL();

    vPortFree(consoleMsgHead);
    consoleMsgHead = consoleMsgHead->next;
    consoleMsgTxState = CONSOLEMSG_TX_STATE_IDLE;
    if (NULL != consoleMsgHead)
    {
        xTaskNotify(consoleTaskHandle,CONSOLE_NOTIFICATION_SEND_NEW_MESSAGE,eSetBits);
    }

    taskEXIT_CRITICAL();
    xSemaphoreGive(tx_console_msg_cnt);
}


#endif      //#ifdef FREERTOS_SUPPORT


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
uint8_t GetConsoleNextCharacter(uint8_t *nextCharacter)
{
    if (consoleInputBuffer_PoR >= CONSOLE_INPUT_BUFFER_SIZE)
    {
        consoleInputBuffer_PoR = 0;
    }

    consoleInputBuffer_PoW = CONSOLE_INPUT_BUFFER_SIZE - hConsoleUart.hdmarx->Instance->NDTR;

    if (consoleInputBuffer_PoR != consoleInputBuffer_PoW)
    {
        *nextCharacter = consoleInputBuffer[consoleInputBuffer_PoR++];
        return (1);
    }

    return (0);
}




/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/17
*******************************************************************************/
void SendConsoleMessage (char * header, char * message, uint8_t strMessageOption)
{
#if !defined (DEVKIT_PLATFORM) && !defined (BOOTLOADER)
    GPIO_PinState TestLockPinState;

    TestLockPinState = HAL_GPIO_ReadPin(TestLock_GPIO_Port, TestLock_Pin);

    if ((InTestMode == 0) && (TestLockPinState != 0))
#else
    if (InTestMode == 0)
#endif /* DEVKIT_PLATFORM & BOOTLOADER*/
    {
        memset(logMsg.msg, 0, MAX_MSG_LENGTH);
        switch (strMessageOption)
        {
        case OPT_START_OF_LINE:
    //        sprintf(logMsg.msg, "\r%s%s",header,message);
            strcpy(logMsg.msg, "\r");
            break;

        case (OPT_NEW_LINE|OPT_START_OF_LINE):
    //        sprintf(logMsg.msg, "\n\r%s%s",header,message);
            strcpy(logMsg.msg, "\n\r");
            break;

        case OPT_APPEND:
        default:
    //        sprintf(logMsg.msg, "%s%s",header,message);
            break;
        }
        strcpy(&logMsg.msg[strlen(logMsg.msg)],header);
        strcpy(&logMsg.msg[strlen(logMsg.msg)],message);

        logMsg.msgLength = strlen(logMsg.msg);

#ifdef FREERTOS_SUPPORT
        AddConsoleMsgToQ(&logMsg);
#else
        HAL_UART_Transmit(&hConsoleUart, (uint8_t *)logMsg.msg, logMsg.msgLength, (HAL_MAX_DELAY-1));
#endif
    }
}

void SendTestMessage (char *message)
{
    logMsg.msgLength = 4;
    logMsg.msg[0] = message[0];
    logMsg.msg[1] = message[1];
    logMsg.msg[2] = message[2];
    logMsg.msg[3] = message[3];
#ifdef FREERTOS_SUPPORT
    AddConsoleMsgToQ(&logMsg);
#endif
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/17
*******************************************************************************/
void SendInteger(int16_t value)
{
    STR_StrItoA(integerString, value);

    SendConsoleMessage("", (char*)integerString,OPT_APPEND);
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
