/**
********************************************************************************
* @file       app_test_bench.h
* @date       2017-04-14
* @author     Marc-Andre Leblanc, ing./Jean-Fran�ois Many
* @version    2.0
* @brief      This header file describe the command ID and parameters format for
*             the Maestro Stat test bench.
*
*  @verbatim
*
*    This is the scheme of communication between the
*    CLIENT (Test bench)and SERVER (Unit Under test)
*    The client issue a command (1 byte) and wait
*    for the response of the server (4bytes). If
*    parameter is not used, a null byte fill the unused parameter space.
*    The length of response is fixed to 4 bytes. All commands
*    are initiated by the client and MUST be acknowledged by the server.
*
*    If the server can't process the command, he don't have to respond.
*    The Client will repeat three time the command before declaring a error
*    with the Server.
*
*             Client                             Server
*            --------                           --------
*               |                                   |
*               |  [CmdID]                          |
*               |  -------------------------------->|
*               |                                   |
*               |  [CmdID, Param1, Param2, Param3 ] |
*               | <---------------------------------|
*               |                                   |
*
*     This is an example of the commands format:
*
*     //========================================================================
*     //            Command request issued by the client (Test bench)
*     //========================================================================
*             CmdID
*     ---------------------------------
*     [APP_TBCH_GET_PROTOCOL_VERSION    ]
*     [APP_TBCH_TRIAC_ON                ]
*     [APP_TBCH_TRIAC_OFF               ]
*     [APP_TBCH_GET_TEMP_AMBIENT        ]
*     [APP_TBCH_GET_TEMP_COMPENSATION   ]
*     [APP_TBCH_GET_TEMP_TRIAC          ]
*     [APP_TBCH_START_INCLUSION         ]
*     [APP_TBCH_START_EXCLUSION         ]
*     [APP_TBCH_GET_NETWORK_STATE       ]
*     [APP_TBCH_SET_DEFAULT             ]
*     [APP_TBCH_SET_LEDS_ON             ]
*     [APP_TBCH_SET_LEDS_OFF            ]
*     [APP_TBCH_ENABLE_TEST_MODE        ]
*     [APP_TBCH_DISABLE_TEST_MODE       ]
*     [APP_TBCH_GET_CURRENT             ]
*     [APP_TBCH_ASSOCIATE_ON_CHANNEL_11 ]
*     [APP_TBCH_ASSOCIATE_ON_CHANNEL_12 ]
*     [APP_TBCH_ASSOCIATE_ON_CHANNEL_13 ]
*     [APP_TBCH_ASSOCIATE_ON_CHANNEL_14 ]
*     [APP_TBCH_ASSOCIATE_ON_CHANNEL_15 ]
*     [APP_TBCH_ASSOCIATE_ON_CHANNEL_16 ]
*     [APP_TBCH_ASSOCIATE_ON_CHANNEL_17 ]
*     [APP_TBCH_ASSOCIATE_ON_CHANNEL_18 ]
*     [APP_TBCH_ASSOCIATE_ON_CHANNEL_19 ]
*     [APP_TBCH_ASSOCIATE_ON_CHANNEL_20 ]
*     [APP_TBCH_ASSOCIATE_ON_CHANNEL_21 ]
*     [APP_TBCH_ASSOCIATE_ON_CHANNEL_22 ]
*     [APP_TBCH_ASSOCIATE_ON_CHANNEL_23 ]
*     [APP_TBCH_ASSOCIATE_ON_CHANNEL_24 ]
*     [APP_TBCH_ASSOCIATE_ON_CHANNEL_25 ]
*     [APP_TBCH_ASSOCIATE_ON_CHANNEL_26 ]
*     [APP_TBCH_GET_HUMIDITY            ]
*     [APP_TBCH_START_PIR_ACQ           ]
*     [APP_TBCH_GET_MODEL               ]
*     [APP_TBCH_GET_ZERO_CROSSING       ]
*     [APP_TBCH_GET_PIR_MIN             ]
*     [APP_TBCH_GET_PIR_MAX             ]
*     [APP_TBCH_DISPLAY_BLACK_SCREEN    ]
*     [APP_TBCH_DISPLAY_WHITE_SCREEN    ]
*     [APP_TBCH_DISPLAY_TOUCH_ZONES     ]
*     [APP_TBCH_FAN_LOW                 ]
*     [APP_TBCH_FAN_HIGH                ]
*     [APP_TBCH_FAN_OFF                 ]
*     [APP_TBCH_GET_VERSION             ]
*     [APP_TBCH_GET_TEMP_COMPENSATION2  ]
*     [APP_TBCH_GET_INSTALL_CODE_1_OF_6 ]
*     [APP_TBCH_GET_INSTALL_CODE_2_OF_6 ]
*     [APP_TBCH_GET_INSTALL_CODE_3_OF_6 ]
*     [APP_TBCH_GET_INSTALL_CODE_4_OF_6 ]
*     [APP_TBCH_GET_INSTALL_CODE_5_OF_6 ]
*     [APP_TBCH_GET_INSTALL_CODE_6_OF_6 ]
*     [APP_TBCH_GET_MAC_ADDRESS_1_OF_3  ]
*     [APP_TBCH_GET_MAC_ADDRESS_2_OF_3  ]
*     [APP_TBCH_GET_MAC_ADDRESS_3_OF_3  ]
*
*  //====================================================================================
*  //                        Command response issued by the server (Thermostat)
*  //====================================================================================
*             CmdID                      Param1                         Param2                          Param3
*  ---------------------------------------------------------------------------------------------------------
*  [APP_TBCH_GET_PROTOCOL_VERSION       ,TBCH_PROTOCOL_VERSION_MAJOR    ,TBCH_PROTOCOL_VERSION_MINOR    ,NULL               ]
*  [APP_TBCH_TRIAC_ON                   ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_TRIAC_OFF                  ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_GET_TEMP_AMBIENT           ,TEMPERATURE_MSB                ,TEMPERATURE_LSB                ,NULL               ]
*  [APP_TBCH_GET_TEMP_COMPENSATION      ,TEMPERATURE_MSB                ,TEMPERATURE_LSB                ,NULL               ]
*  [APP_TBCH_GET_TEMP_TRIAC             ,TEMPERATURE_MSB                ,TEMPERATURE_LSB                ,NULL               ]
*  [APP_TBCH_START_INCLUSION            ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_START_EXCLUSION            ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_GET_NETWORK_STATE          ,NETWORK_STATE                  ,NULL                           ,NULL               ]
*  [APP_TBCH_SET_DEFAULT                ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_SET_LEDS_ON                ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_SET_LEDS_OFF               ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_ENABLE_TEST_MODE           ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_DISABLE_TEST_MODE          ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_GET_CURRENT                ,CURRENT_INT                    ,CURRENT_DECIMAL_1              ,CURRENT_DECIMAL_2  ]
*  [APP_TBCH_ASSOCIATE_ON_CHANNEL_11    ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_ASSOCIATE_ON_CHANNEL_12    ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_ASSOCIATE_ON_CHANNEL_13    ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_ASSOCIATE_ON_CHANNEL_14    ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_ASSOCIATE_ON_CHANNEL_15    ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_ASSOCIATE_ON_CHANNEL_16    ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_ASSOCIATE_ON_CHANNEL_17    ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_ASSOCIATE_ON_CHANNEL_18    ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_ASSOCIATE_ON_CHANNEL_19    ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_ASSOCIATE_ON_CHANNEL_20    ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_ASSOCIATE_ON_CHANNEL_21    ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_ASSOCIATE_ON_CHANNEL_22    ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_ASSOCIATE_ON_CHANNEL_23    ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_ASSOCIATE_ON_CHANNEL_24    ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_ASSOCIATE_ON_CHANNEL_25    ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_ASSOCIATE_ON_CHANNEL_26    ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_GET_HUMIDITY               ,HUMIDITY                       ,NULL                           ,NULL               ]
*  [APP_TBCH_START_PIR_ACQ              ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_GET_MODEL                  ,MODEL_ID                       ,NULL                           ,NULL               ]
*  [APP_TBCH_GET_ZERO_CROSSING          ,DETECTION_STATE                ,NULL                           ,NULL               ]
*  [APP_TBCH_GET_PIR_MIN                ,PIR_MIN_MSB                    ,PIR_MIN_LSB                    ,NULL               ]
*  [APP_TBCH_GET_PIR_MAX                ,PIR_MAX_MSB                    ,PIR_MAX_LSB                    ,NULL               ]
*  [APP_TBCH_DISPLAY_BLACK_SCREEN       ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_DISPLAY_WHITE_SCREEN       ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_DISPLAY_TOUCH_ZONES        ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_FAN_LOW                    ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_FAN_HIGH                   ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_FAN_OFF                    ,NULL                           ,NULL                           ,NULL               ]
*  [APP_TBCH_GET_VERSION                ,SW_VER_MAJOR                   ,SW_VER_MINOR                   ,NULL               ]
*  [APP_TBCH_GET_TEMP_COMPENSATION2     ,TEMPERATURE_MSB                ,TEMPERATURE_LSB                ,NULL               ]
*  [APP_TBCH_GET_INSTALL_CODE_1_OF_6    ,INSTALL_CODE_0                 ,INSTALL_CODE_1                 ,INSTALL_CODE_2     ]
*  [APP_TBCH_GET_INSTALL_CODE_2_OF_6    ,INSTALL_CODE_3                 ,INSTALL_CODE_4                 ,INSTALL_CODE_5     ]
*  [APP_TBCH_GET_INSTALL_CODE_3_OF_6    ,INSTALL_CODE_6                 ,INSTALL_CODE_7                 ,INSTALL_CODE_8     ]
*  [APP_TBCH_GET_INSTALL_CODE_4_OF_6    ,INSTALL_CODE_9                 ,INSTALL_CODE_10                ,INSTALL_CODE_11    ]
*  [APP_TBCH_GET_INSTALL_CODE_5_OF_6    ,INSTALL_CODE_12                ,INSTALL_CODE_13                ,INSTALL_CODE_14    ]
*  [APP_TBCH_GET_INSTALL_CODE_6_OF_6    ,INSTALL_CODE_15                ,INSTALL_CODE_16                ,INSTALL_CODE_17    ]
*  [APP_TBCH_GET_MAC_ADDRESS_1_OF_3     ,MAC_ADDRESS_0                  ,MAC_ADDRESS_1                  ,MAC_ADDRESS_2      ]
*  [APP_TBCH_GET_MAC_ADDRESS_2_OF_3     ,MAC_ADDRESS_3                  ,MAC_ADDRESS_4                  ,MAC_ADDRESS_5      ]
*  [APP_TBCH_GET_MAC_ADDRESS_3_OF_3     ,MAC_ADDRESS_6                  ,MAC_ADDRESS_7                  ,NULL               ]
*
* @endverbatim
* @addtogroup app
* @{
* @defgroup app_test_bench
* @{
*******************************************************************************/
#ifndef app_test_bench_H
#define app_test_bench_H

/* Dependencies Includes -----------------------------------------------------*/
#include "OsTask.h"


/*******************************************************************************
*                                Define
*******************************************************************************/

// Major and minor version identifier
#define TBCH_PROTOCOL_VERSION_MAJOR      ((uint8_t)0x02)
#define TBCH_PROTOCOL_VERSION_MINOR      ((uint8_t)0x01)

// This is the scaling factor of the temperature returned
// by the thermostat under test.
#define TBCH_TEMPERATURE_SCALING_FACTOR  ((uint8_t)100)

// Baudrate used for the uart communication between the Client and Server
#define TBCH_BAUDRATE                    ((uint16_t)9600)

/*******************************************************************************
*                                Types
*******************************************************************************/

/// This enumerate the identifier of all the command used for
/// the test bench.
typedef enum
{
  APP_TBCH_GET_PROTOCOL_VERSION    = ((uint8_t)0xFF),    /*!< Use to get the version of the protocol used           */
  APP_TBCH_TRIAC_ON                = ((uint8_t)0x01),    /*!< Use to activate power to load using the TRIAC         */
  APP_TBCH_TRIAC_OFF               = ((uint8_t)0x02),    /*!< Use to deactivate the power to load using the TRIAC   */
  APP_TBCH_GET_TEMP_AMBIENT        = ((uint8_t)0x03),    /*!< Use to get the temperature of ambient thermistor      */
  APP_TBCH_GET_TEMP_COMPENSATION   = ((uint8_t)0x04),    /*!< Use to get the temperature of compensation thermistor */
  APP_TBCH_GET_TEMP_TRIAC          = ((uint8_t)0x05),    /*!< Use to get the temperature of TRIAC thermistor        */
  APP_TBCH_START_INCLUSION         = ((uint8_t)0x06),    /*!< Start the inclusion process on the thermostat         */
  APP_TBCH_START_EXCLUSION         = ((uint8_t)0x07),    /*!< Start the exclusion process on the thermostat         */
  APP_TBCH_GET_NETWORK_STATE       = ((uint8_t)0x08),    /*!< Used to get the state of the thermostat with the ZigBee network (Included/Excluded/Error) */
  APP_TBCH_SET_DEFAULT             = ((uint8_t)0x09),    /*!< Used to set the thermostat and ZigBee module to de default  parameters                     */
  APP_TBCH_SET_LEDS_ON             = ((uint8_t)0x0A),    /*!< Used to turn on the backlight and LEDs                */
  APP_TBCH_SET_LEDS_OFF            = ((uint8_t)0x0B),    /*!< Used to turn off the backlight and LEDs               */
  APP_TBCH_ENABLE_TEST_MODE        = ((uint8_t)0x0C),    /*!< Used to enable test mode */
  APP_TBCH_DISABLE_TEST_MODE       = ((uint8_t)0x0D),    /*!< Used to disable test mode */
  APP_TBCH_GET_CURRENT             = ((uint8_t)0x0E),    /*!< Used to get the sensed current    */
  APP_TBCH_ASSOCIATE_ON_CHANNEL_11 = ((uint8_t)0x0F),    /*!< Used to associate on the ZigBee channel 11 */
  APP_TBCH_ASSOCIATE_ON_CHANNEL_12 = ((uint8_t)0x10),    /*!< Used to associate on the ZigBee channel 12 */
  APP_TBCH_ASSOCIATE_ON_CHANNEL_13 = ((uint8_t)0x11),    /*!< Used to associate on the ZigBee channel 13 */
  APP_TBCH_ASSOCIATE_ON_CHANNEL_14 = ((uint8_t)0x12),    /*!< Used to associate on the ZigBee channel 14 */
  APP_TBCH_ASSOCIATE_ON_CHANNEL_15 = ((uint8_t)0x13),    /*!< Used to associate on the ZigBee channel 15 */
  APP_TBCH_ASSOCIATE_ON_CHANNEL_16 = ((uint8_t)0x14),    /*!< Used to associate on the ZigBee channel 16 */
  APP_TBCH_ASSOCIATE_ON_CHANNEL_17 = ((uint8_t)0x15),    /*!< Used to associate on the ZigBee channel 17 */
  APP_TBCH_ASSOCIATE_ON_CHANNEL_18 = ((uint8_t)0x16),    /*!< Used to associate on the ZigBee channel 18 */
  APP_TBCH_ASSOCIATE_ON_CHANNEL_19 = ((uint8_t)0x17),    /*!< Used to associate on the ZigBee channel 19 */
  APP_TBCH_ASSOCIATE_ON_CHANNEL_20 = ((uint8_t)0x18),    /*!< Used to associate on the ZigBee channel 20 */
  APP_TBCH_ASSOCIATE_ON_CHANNEL_21 = ((uint8_t)0x19),    /*!< Used to associate on the ZigBee channel 21 */
  APP_TBCH_ASSOCIATE_ON_CHANNEL_22 = ((uint8_t)0x1A),    /*!< Used to associate on the ZigBee channel 22 */
  APP_TBCH_ASSOCIATE_ON_CHANNEL_23 = ((uint8_t)0x1B),    /*!< Used to associate on the ZigBee channel 23 */
  APP_TBCH_ASSOCIATE_ON_CHANNEL_24 = ((uint8_t)0x1C),    /*!< Used to associate on the ZigBee channel 24 */
  APP_TBCH_ASSOCIATE_ON_CHANNEL_25 = ((uint8_t)0x1D),    /*!< Used to associate on the ZigBee channel 25 */
  APP_TBCH_ASSOCIATE_ON_CHANNEL_26 = ((uint8_t)0x1E),    /*!< Used to associate on the ZigBee channel 26 */
  APP_TBCH_GET_HUMIDITY            = ((uint8_t)0x1F),    /*!< Used to get the sensed humidity */
  APP_TBCH_START_PIR_ACQ           = ((uint8_t)0x20),    /*!< Used to start a new PIR acquisition cycle */
  APP_TBCH_GET_MODEL               = ((uint8_t)0x21),    /*!< Used to get the product model */
  APP_TBCH_GET_ZERO_CROSSING       = ((uint8_t)0x22),    /*!< Used to get the zero crossing state */
  APP_TBCH_GET_PIR_MIN             = ((uint8_t)0x23),    /*!< Used to get the minimum PIR ADC count */
  APP_TBCH_GET_PIR_MAX             = ((uint8_t)0x24),    /*!< Used to get the maximum PIR ADC count */
  APP_TBCH_DISPLAY_BLACK_SCREEN    = ((uint8_t)0x25),    /*!< Used to display a black screen on the LCD */
  APP_TBCH_DISPLAY_WHITE_SCREEN    = ((uint8_t)0x26),    /*!< Used to display a white screen on the LCD */
  APP_TBCH_DISPLAY_TOUCH_ZONES     = ((uint8_t)0x27),    /*!< Used to display the touch zones */
  APP_TBCH_FAN_LOW                 = ((uint8_t)0x28),    /*!< Used to activate the low fan speed */
  APP_TBCH_FAN_HIGH                = ((uint8_t)0x29),    /*!< Used to activate the high fan speed */
  APP_TBCH_FAN_OFF                 = ((uint8_t)0x30),    /*!< Used to deactivate the fan */
  APP_TBCH_GET_VERSION             = ((uint8_t)0x31),    /*!< Used to get the thermostat version */
  APP_TBCH_GET_TEMP_COMPENSATION2  = ((uint8_t)0x32),    /*!< Used to get the temperature of the second compensation thermistor */
  APP_TBCH_GET_INSTALL_CODE_1_OF_6 = ((uint8_t)0x33),    /*!< Used to get the install code bytes 0 to 2 */
  APP_TBCH_GET_INSTALL_CODE_2_OF_6 = ((uint8_t)0x34),    /*!< Used to get the install code bytes 3 to 5 */
  APP_TBCH_GET_INSTALL_CODE_3_OF_6 = ((uint8_t)0x35),    /*!< Used to get the install code bytes 6 to 8 */
  APP_TBCH_GET_INSTALL_CODE_4_OF_6 = ((uint8_t)0x36),    /*!< Used to get the install code bytes 9 to 11 */
  APP_TBCH_GET_INSTALL_CODE_5_OF_6 = ((uint8_t)0x37),    /*!< Used to get the install code bytes 12 to 14 */
  APP_TBCH_GET_INSTALL_CODE_6_OF_6 = ((uint8_t)0x38),    /*!< Used to get the install code bytes 15 to 17 */
  APP_TBCH_GET_MAC_ADDRESS_1_OF_3  = ((uint8_t)0x39),    /*!< Used to get the MAC address bytes 0 to 2 */
  APP_TBCH_GET_MAC_ADDRESS_2_OF_3  = ((uint8_t)0x3A),    /*!< Used to get the MAC address bytes 3 to 5 */
  APP_TBCH_GET_MAC_ADDRESS_3_OF_3  = ((uint8_t)0x3B),    /*!< Used to get the MAC address bytes 6 to 7 */
}APP_TBCH_CmdId_t;

/// This enumeration define the state returned by the DUTB
/// when he receive the "APP_TBCH_GET_NETWORK_STATE" commands.
typedef enum
{
  APP_TBCH_NETWORK_INCLUDED = ((uint8_t)0x01), /*!< Indicate that the thermostat is included in a ZigBee network   */
  APP_TBCH_NETWORK_EXCLUDED = ((uint8_t)0x02), /*!< Indicate that the thermostat is excluded of any ZigBee network */
  APP_TBCH_NETWORK_ERROR    = ((uint8_t)0x03)  /*!< Indicate an error to retrieve the network state                */
}APP_TBCH_NetworkState_t;

/// This enumeration define the recognized model of the Maestro Platform
typedef enum
{
  APP_TBCH_MAESTRO_CONTROLER = ((uint8_t)0x01), /*!< Indicate that the model is a Maestro Controler   */
  APP_TBCH_MAESTRO_THERMOSTAT = ((uint8_t)0x02), /*!< Indicate that the model is a Maestro Thermostat */
  APP_TBCH_FLO_MDG_THERMOSTAT = ((uint8_t)0x03), /*!< Indicate that the model is a FLO MDG Thermostat */
  APP_TBCH_FLO_HDG_THERMOSTAT = ((uint8_t)0x04), /*!< Indicate that the model is a FLO HDG Thermostat */
  APP_TBCH_EVE_MDG_THERMOSTAT = ((uint8_t)0x05), /*!< Indicate that the model is a EVE MDG Thermostat */
  APP_TBCH_EVE_HDG_THERMOSTAT = ((uint8_t)0x06), /*!< Indicate that the model is a EVE HDG Thermostat */
}APP_TBCH_MaestroModel_t;

void ProcessTestCommand(uint8_t command);
OSTASK_DEF(UART_DEBUG_Action);

#endif
/**
* @}
* @}
**/
/** Copyright(C) 2002-2015 Synapse Electronique, All Rights Reserved **END OF FILE**/
