/*******************************************************************************
* @file    HAL_Console.h
* @date    2016/08/18
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef HAL_Console_H
#define HAL_Console_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>

#include "stm32f4xx_hal.h"





/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define OPT_NEW_LINE            (1<<0)
#define OPT_START_OF_LINE       (1<<1)
#define OPT_APPEND              (1<<2)

#define CONSOLE_INPUT_BUFFER_SIZE       128
#define MAX_MSG_LENGTH          256

#ifdef ENABLE_CONSOLE
    #define CONSOLE_MESSAGE_APPEND_INTEGER(intValue)    SendInteger((int16_t)intValue)
    #define CONSOLE_MESSAGE_APPEND_STRING(message)       SendConsoleMessage( \
                                                             "",      \
                                                             message,       \
                                                             OPT_APPEND)


    #define CONSOLE_EVENT_MESSAGE(message)              SendConsoleMessage( \
                                                            "Event: ",      \
                                                             message,       \
                                                             OPT_NEW_LINE|OPT_START_OF_LINE)
    #define CONSOLE_EVENT_MESSAGE_SAMELINE(message)      SendConsoleMessage( \
                                                            "Event: ",      \
                                                             message,       \
                                                             OPT_START_OF_LINE)

    #define CONSOLE_RUNTIME_MESSAGE(message)              SendConsoleMessage( \
                                                            "RunTime: ",      \
                                                             message,       \
                                                             OPT_NEW_LINE|OPT_START_OF_LINE)
    #define CONSOLE_RUNTIME_MESSAGE_SAMELINE(message)      SendConsoleMessage( \
                                                            "RunTime: ",      \
                                                             message,       \
                                                             OPT_START_OF_LINE)

    #define CONSOLE_LOG_MESSAGE(message)              SendConsoleMessage( \
                                                            "Log: ",      \
                                                             message,       \
                                                             OPT_NEW_LINE|OPT_START_OF_LINE)
    #define CONSOLE_LOG_MESSAGE_SAMELINE(message)      SendConsoleMessage( \
                                                            "Log: ",      \
                                                             message,       \
                                                             OPT_START_OF_LINE)
    #define CONSOLE_EMPTY_LINE()                        SendConsoleMessage( \
                                                            "",      \
                                                             "",       \
                                                             OPT_NEW_LINE|OPT_START_OF_LINE)
    #define CONSOLE_TEST_COMMAND(message)               SendTestMessage(message)
#else
    #define CONSOLE_MESSAGE_APPEND_INTEGER(intValue)
    #define CONSOLE_MESSAGE_APPEND_STRING(message)
    #define CONSOLE_EVENT_MESSAGE(message)
    #define CONSOLE_EVENT_MESSAGE_SAMELINE(message)
    #define CONSOLE_RUNTIME_MESSAGE(message)
    #define CONSOLE_RUNTIME_MESSAGE_SAMELINE(message)
    #define CONSOLE_LOG_MESSAGE(message)
    #define CONSOLE_LOG_MESSAGE_SAMELINE(message)
    #define CONSOLE_EMPTY_LINE()
#endif

/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void InitConsole(void);
void SendConsoleMessage (char * header, char *message, uint8_t strMessageOption);
void SendTestMessage (char *message);
void SendInteger(int16_t value);
uint8_t GetConsoleNextCharacter(uint8_t *nextCharacter);
void SendNextConsoleMsg (void);
void FreeLastSentConsoleMsg (void);

#endif /* HAL_Console_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
