/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    HAL_WatchDog.c
* @date    2016/08/23
* @authors J-F Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include ".\HAL\inc\HAL_WatchDog.h"
#include "stm32f4xx_hal.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
IWDG_HandleTypeDef hIWdg;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/23
*******************************************************************************/
void WatchDogInit(void)
{
    hIWdg.Instance = IWDG;
    hIWdg.Init.Prescaler = IWDG_PRESCALER_256;           //Prescaler 256 : 32kHz/256=128Hz (7.8125ms)
    hIWdg.Init.Reload = 0xFFF;                           //Reload Max (12 bits) * 7.8125ms = ~32s
    HAL_IWDG_Init(&hIWdg);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/23
*******************************************************************************/
void RefreshWatchDog(void)
{
    HAL_IWDG_Refresh(&hIWdg);
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
