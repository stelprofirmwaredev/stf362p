/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    MX25L6433F.c
* @date    2016/08/11
* @authors J-F. Simard
* @brief   
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include ".\BSP\FLASH\MX25L6433F\inc\MX25l6433F.H"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/



/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static void FlashWriteEnable (void);
//static void WriteDisable (void);
//static void WriteConfig (void);

static void WaitForFlashReady (void);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/11
*******************************************************************************/
static void FlashWriteEnable (void)
{
    uint8_t command = MX25_FLASH_WREN;
    
    PULL_CS_LINE;    
    WRITE_FLASH_DATA(&command,1);
    RELEASE_CS_LINE
    
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/11
*******************************************************************************/
static void WaitForFlashReady (void)
{
    uint8_t command = MX25_FLASH_RDSR;
    uint8_t statusReg = 0;
    
    PULL_CS_LINE;    
    WRITE_FLASH_DATA(&command,1);
    READ_FLASH_DATA((uint8_t *)&statusReg, 1);
    
    while ((statusReg&FLASH_STATUS_WIP) != 0)
    {        
        READ_FLASH_DATA((uint8_t *)&statusReg, 1);
    }
    
    RELEASE_CS_LINE;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/11
*******************************************************************************/
void EraseSector (uint32_t address)
{
    uint8_t commandBuffer[4];
    
    
    FlashWriteEnable();

    
    PULL_CS_LINE;
    commandBuffer[0] = MX25_FLASH_SE;
    commandBuffer[1] = (uint8_t)((address&0x00ff0000)>>16);
    commandBuffer[2] = (uint8_t)((address&0x0000ff00)>>8);
    commandBuffer[3] = (uint8_t)(address&0x000000ff);

    WRITE_FLASH_DATA(commandBuffer,4);

    RELEASE_CS_LINE;    
    
    WaitForFlashReady();
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/11
*******************************************************************************/
void EraseBlock_32 (uint32_t address)
{
    uint8_t commandBuffer[4];
    
    
    FlashWriteEnable();

    
    PULL_CS_LINE;
    commandBuffer[0] = MX25_FLASH_BE32;
    commandBuffer[1] = (uint8_t)((address&0x00ff0000)>>16);
    commandBuffer[2] = (uint8_t)((address&0x0000ff00)>>8);
    commandBuffer[3] = (uint8_t)(address&0x000000ff);

    WRITE_FLASH_DATA(commandBuffer,4);

    RELEASE_CS_LINE;    
    
    WaitForFlashReady();
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/11
*******************************************************************************/
void EraseBlock_64 (uint32_t address)
{
    uint8_t commandBuffer[4];
    
    
    FlashWriteEnable();

    
    PULL_CS_LINE;
    commandBuffer[0] = MX25_FLASH_BE64;
    commandBuffer[1] = (uint8_t)((address&0x00ff0000)>>16);
    commandBuffer[2] = (uint8_t)((address&0x0000ff00)>>8);
    commandBuffer[3] = (uint8_t)(address&0x000000ff);

    WRITE_FLASH_DATA(commandBuffer,4);

    RELEASE_CS_LINE;    
    
    WaitForFlashReady();
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/


/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/11
*******************************************************************************/
void FLASH_Init (void)
{
    RELEASE_CS_LINE;
    RELEASE_HOLD_LINE;
    RELEASE_WP_LINE;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/11
*******************************************************************************/
void FLASH_Write (uint32_t destination, uint32_t length, uint8_t * buffer)
{
    uint8_t commandBuffer[4];
    
    commandBuffer[0] = MX25_FLASH_PP;
    commandBuffer[1] = (uint8_t)((destination&0x00ff0000)>>16);
    commandBuffer[2] = (uint8_t)((destination&0x0000ff00)>>8);
    commandBuffer[3] = (uint8_t)(destination&0x000000ff);
 
     
    FlashWriteEnable();

    PULL_CS_LINE;
    
    WRITE_FLASH_DATA(commandBuffer,4);

    WRITE_FLASH_DATA(buffer, length);

//    commandBuffer[0] = MX25_FLASH_WRDI;
//
//    WRITE_FLASH_DATA(commandBuffer,1);
    
    RELEASE_CS_LINE;
    
    WaitForFlashReady();
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/11
*******************************************************************************/
void FLASH_Read (uint32_t from, uint32_t length, uint8_t * buffer)
{
    uint8_t commandBuffer[4];
    
    commandBuffer[0] = MX25_FLASH_READ;
    commandBuffer[1] = (from&0x00ff0000)>>16;
    commandBuffer[2] = (from&0x0000ff00)>>8;
    commandBuffer[3] = (from&0x000000ff);
    
    PULL_CS_LINE;
    
    for (uint16_t i = 0; i < length; i++)
    {
        buffer[i] = 0;
    }
    
    WRITE_FLASH_DATA(commandBuffer,4);
    
    READ_FLASH_DATA(buffer, length);
    
    RELEASE_CS_LINE;
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
