/*******************************************************************************
* @file    MX25L6433F.h
* @date    2016/08/11
* @authors J-F. Simard
* @brief 
*******************************************************************************/

#ifndef _MX25L6433F_H
#define _MX25L6433F_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>

#ifdef __ICCARM__
    #include "stm32f4xx_hal.h"
#endif


/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define FLASH_STATUS_WIP        0x01

typedef enum
{
    MX25_FLASH_READ         = 0x03,
    MX25_FLASH_FAST_READ    = 0x0B,
    MX25_FLASH_2READ        = 0xBB,
    MX25_FLASH_DREAD        = 0x3B,
    MX25_FLASH_4READ        = 0xEB,
    MX25_FLASH_QREAD        = 0x6B,
    
    MX25_FLASH_WREN         = 0x06,         //Write Enable
    MX25_FLASH_WRDI         = 0x04,         //Write Disable
    MX25_FLASH_RDSR         = 0x05,         //Read status register
    MX25_FLASH_RDCR         = 0x15,         //Read configuration register
    MX25_FLASH_WRSR         = 0x01,         //Write status/configuration register
    MX25_FLASH_4PP          = 0x38,         //quad page program
    MX25_FLASH_SE           = 0x20,         //sector erase
    
    MX25_FLASH_BE32         = 0x52,         //Block erase 32K
    MX25_FLASH_BE64         = 0xD8,         //Block erase 64K
    MX25_FLASH_CE           = 0x60,         //Chip erase
    MX25_FLASH_PP           = 0x02,         //page program 
    MX25_FLASH_DP           = 0xB9,         //deep power down
    MX25_FLASH_RDP          = 0xAB,         //Release from deep power down
    MX25_FLASH_SUSP_PGM_ERS = 0x75,         //Suspend Program/erase
    
    MX25_FLASH_RES_PGM_ERS  = 0x7A,         //Resumes Program/erase
    MX25_FLASH_RDID         = 0x9F,         //Read ID
    MX25_FLASH_RES          = 0xAB,         //Read Electronic ID
    MX25_FLASH_REMS         = 0x90,         //Read electronic manufacturer & device ID
    MX25_FLASH_ENSO         = 0xB1,         //Enter secured OTP
    MX25_FLASH_EXSO         = 0xC1,         //exit secured otp
    MX25_FLASH_WRSCUR       = 0x2F,         //write security register
    
    MX25_FLASH_RDSCUR       = 0x2B,         //Read security register
    MX25_FLASH_RSTEN        = 0x66,         //Reset Enable
    MX25_FLASH_RST          = 0x99,         //Reset Memory
    MX25_FLASH_RDSFDP       = 0x5A,         
    MX25_FLASH_SBL          = 0xC0,         //Set burst length
    MX25_FLASH_NOP          = 0x00,         //No operation

} MX25L6433F_Command_t;

#define MX25_BLOCK32_SIZE                        0x008000        //32768 bytes
#define MX25_BLOCK64_SIZE                        0x010000        //65536 bytes
#define MX25_SECTOR_SIZE                         0x001000        //4096 bytes

#define MAX_NUMBER_OF_BLOCK_32              256     //block count starts at 0 (0 @ 255)
#define MAX_NUMBER_OF_BLOCK_64              128     //block count starts at 0 (0 @ 128)
#define MAX_NUMBER_OF_SECTOR                2048    //sector count starts at 0 (0 @ 2047)

extern SPI_HandleTypeDef hFlashSPI;

#ifdef __ICCARM__
    #define WRITE_FLASH_DATA(buffer,length)     HAL_SPI_TransmitReceive(&hFlashSPI, buffer, buffer, length, (HAL_MAX_DELAY-1))
    #define READ_FLASH_DATA(buffer,length)      hFlashSPI.State = HAL_SPI_STATE_BUSY_RX;HAL_SPI_TransmitReceive(&hFlashSPI,buffer, buffer, length, (HAL_MAX_DELAY-1))

    #define PULL_CS_LINE                        HAL_GPIO_WritePin(FLASH_SLAVE_SELECT_GPIO_Port, FLASH_SLAVE_SELECT_Pin, GPIO_PIN_RESET);
    #define RELEASE_CS_LINE                     HAL_GPIO_WritePin(FLASH_SLAVE_SELECT_GPIO_Port, FLASH_SLAVE_SELECT_Pin, GPIO_PIN_SET);

    #define PULL_HOLD_LINE                      HAL_GPIO_WritePin(FLASH_HLD_SIO3_GPIO_Port, FLASH_HLD_SIO3_Pin, GPIO_PIN_RESET);
    #define RELEASE_HOLD_LINE                   HAL_GPIO_WritePin(FLASH_HLD_SIO3_GPIO_Port, FLASH_HLD_SIO3_Pin, GPIO_PIN_SET);

    #define PULL_WP_LINE                        HAL_GPIO_WritePin(FLASH_WP_SIO2_GPIO_Port, FLASH_WP_SIO2_Pin, GPIO_PIN_RESET);
    #define RELEASE_WP_LINE                     HAL_GPIO_WritePin(FLASH_WP_SIO2_GPIO_Port, FLASH_WP_SIO2_Pin, GPIO_PIN_SET);
#else       //#ifdef __ICCARM__
    #define WRITE_FLASH_DATA(buffer,length)     
    #define READ_FLASH_DATA(buffer,length)      

    #define PULL_CS_LINE                        
    #define RELEASE_CS_LINE                     

    #define PULL_HOLD_LINE                      
    #define RELEASE_HOLD_LINE                   

    #define PULL_WP_LINE                        
    #define RELEASE_WP_LINE                     
#endif
/*******************************************************************************
* Public structures definitions
*******************************************************************************/


/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void FLASH_Init (void);
void FLASH_Write (uint32_t destination, uint32_t length, uint8_t * buffer);
void FLASH_Read (uint32_t from, uint32_t length, uint8_t * buffer);
void EraseBlock_32 (uint32_t address);
void EraseBlock_64 (uint32_t address);
void EraseSector (uint32_t address);


#endif /* _MX25L6433F_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
