/*******************************************************************************
* @file    HAL_Flash.h
* @date    2016/08/11
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef HAL_Flash_H
#define HAL_Flash_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>

#include ".\BSP\FLASH\MX25L6433F\inc\MX25L6433F.h"

/*******************************************************************************
* Public constants definitions
*******************************************************************************/

/*******************************************************************************
* Public structures definitions
*******************************************************************************/
typedef enum
{
    FLASH_ERASE_BLOCK32   = 0,
    FLASH_ERASE_BLOCK64,
    FLASH_ERASE_SECTOR,
} Erase_type_t;


/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void HAL_FlashInit(void);
void HAL_ReadFlashData (uint32_t from, uint32_t length, uint8_t * buffer);
void HAL_WriteFlashData (uint32_t to, uint32_t length, uint8_t * buffer);
void HAL_EraseFlash (uint32_t address, Erase_type_t type);

#endif /* HAL_Flash_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
