/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    HAL_Flash.c
* @date    2016/08/11
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#ifdef FREERTOS_SUPPORT
#include "cmsis_os.h"
#endif
#include ".\HAL\inc\HAL_Flash.h"
#include "FlashLayout.h"
#include ".\BSP\FLASH\MX25L6433F\inc\MX25L6433F.h"
#include "stm32f4xx_hal.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/

#ifdef FREERTOS_SUPPORT
    #include "trcRecorder.h"
    char* serialFlashChannel;
#endif

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
SPI_HandleTypeDef hFlashSPI;
#ifdef FREERTOS_SUPPORT
    SemaphoreHandle_t serialFlashSemaphore;
#endif


/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static void GPIO_Init(void);
static void SPI_Init(void);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/11
*******************************************************************************/
static void SPI_Init(void)
{

    hFlashSPI.Instance = SPI5;
    hFlashSPI.Init.Mode = SPI_MODE_MASTER;
    hFlashSPI.Init.Direction = SPI_DIRECTION_2LINES;
    hFlashSPI.Init.DataSize = SPI_DATASIZE_8BIT;
    hFlashSPI.Init.CLKPolarity = SPI_POLARITY_LOW;
    hFlashSPI.Init.CLKPhase = SPI_PHASE_1EDGE;
    hFlashSPI.Init.NSS = SPI_NSS_SOFT;
    hFlashSPI.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
    hFlashSPI.Init.FirstBit = SPI_FIRSTBIT_MSB;
    hFlashSPI.Init.TIMode = SPI_TIMODE_DISABLE;
    hFlashSPI.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
    hFlashSPI.Init.CRCPolynomial = 10;
    if (HAL_SPI_Init(&hFlashSPI) != HAL_OK)
    {
    //    Error_Handler();
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/1
*******************************************************************************/
static void GPIO_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(FLASH_SLAVE_SELECT_GPIO_Port, FLASH_SLAVE_SELECT_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(FLASH_WP_SIO2_GPIO_Port, FLASH_WP_SIO2_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(FLASH_HLD_SIO3_GPIO_Port, FLASH_HLD_SIO3_Pin, GPIO_PIN_RESET);

    /*Configure GPIO pins */
    GPIO_InitStruct.Pin = FLASH_SLAVE_SELECT_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(FLASH_SLAVE_SELECT_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = FLASH_WP_SIO2_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(FLASH_WP_SIO2_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = FLASH_HLD_SIO3_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(FLASH_HLD_SIO3_GPIO_Port, &GPIO_InitStruct);



    /* Peripheral clock enable */
    __HAL_RCC_SPI5_CLK_ENABLE();

    /**SPI5 GPIO Configuration
    PF7     ------> SPI5_SCK
    PF8     ------> SPI5_MISO
    PF9     ------> SPI5_MOSI
    */
    GPIO_InitStruct.Pin = FLASH_SPI_CLK_Pin|FLASH_MISO_Pin|FLASH_MOSI_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_MEDIUM;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI5;
    HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/11
*******************************************************************************/
void HAL_FlashInit(void)
{
    GPIO_Init();
    SPI_Init();

    FLASH_Init();
#ifdef FREERTOS_SUPPORT
    serialFlashSemaphore = xSemaphoreCreateBinary();

    xSemaphoreGive(serialFlashSemaphore);

    serialFlashChannel = vTraceStoreUserEventChannelName("Serial Flash");
#endif
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/11
*******************************************************************************/
void HAL_ReadFlashData (uint32_t from, uint32_t length, uint8_t * buffer)
{
#ifdef FREERTOS_SUPPORT
    xSemaphoreTake(serialFlashSemaphore,portMAX_DELAY );

    vTracePrint(serialFlashChannel, "Start Reading");
#endif
    if (length > 0xFFFF)
    {
        while (length > 0xFFFF)
        {
            FLASH_Read (from, 0xFFFF, buffer);
            length -= 0xFFFF;
            from += 0xFFFF;
            buffer += 0xFFFF;
        }
    }


    FLASH_Read (from, length, buffer);

#ifdef FREERTOS_SUPPORT
    vTracePrint(serialFlashChannel, "End Reading");

    xSemaphoreGive(serialFlashSemaphore);
#endif
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/11
*******************************************************************************/
void HAL_WriteFlashData (uint32_t to, uint32_t length, uint8_t * buffer)
{
#ifdef FREERTOS_SUPPORT
    xSemaphoreTake(serialFlashSemaphore,portMAX_DELAY );

    vTracePrint(serialFlashChannel, "Start Writing");
#endif

    while (length > 0)
    {
        uint16_t fractionLength = length;

        if (length > 256)
        {
            fractionLength = 256;
        }
        FLASH_Write (to, fractionLength, buffer);
        to += fractionLength;
        buffer += fractionLength;
        length -= fractionLength;
    }

#ifdef FREERTOS_SUPPORT
    vTracePrint(serialFlashChannel, "End Writing");

    xSemaphoreGive(serialFlashSemaphore);
#endif
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/11
*******************************************************************************/
void HAL_EraseFlash (uint32_t address, Erase_type_t type)
{
#ifdef FREERTOS_SUPPORT
    xSemaphoreTake(serialFlashSemaphore,portMAX_DELAY );

    vTracePrint(serialFlashChannel, "Start Erase");
#endif
    switch (type)
    {
    case FLASH_ERASE_BLOCK32:
        EraseBlock_32(address);
        break;

    case FLASH_ERASE_BLOCK64:
        EraseBlock_64(address);
        break;

    /* Erase external flash by sector is reserved to the NVM application,
        where each sector is dedicated to certain set of data   */
    case FLASH_ERASE_SECTOR:
        if ((NVM_START_PAGE <= address) && (NVM_END >= address))
        {
            EraseSector(address);
        }
        break;

    default:
        break;
    }
#ifdef FREERTOS_SUPPORT
    vTracePrint(serialFlashChannel, "End Erase");

    xSemaphoreGive(serialFlashSemaphore);
#endif
}

/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/




