/*******************************************************************************
* @file    FlashOps.h
* @date    2016/07/07
* @authors J-F. Simard
* @brief 
*******************************************************************************/

#ifndef _FlashOps_H
#define _FlashOps_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include "FlashLayout.h"
#include "loaderStates.h"


/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define BOOTLOADER_VERSION_STRING_POINTER   0x08003ffc

typedef enum
{
    FLASHOP_NOERROR = 0,
    FLASHOP_INTERNALFLASH_CRC_ERROR,
    FLASHOP_SRECORD_RO_CHECKSUM_ERROR,
    FLASHOP_SRECORD_W_CHECKSUM_ERROR,
    FLASHOP_BLOCKCRC_ERROR,
} flashop_error_t;

#define VALIDATE_AND_INSTALL_FIRMWARE   0
#define VALIDATE_FIRMWARE_ONLY          1

/*******************************************************************************
* Public structures definitions
*******************************************************************************/
typedef struct
{
    uint32_t StartAddress;
    FlashSector_Size_t SectorSize;
} FlashSector_Info_t;


/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void EraseFlashArea(FlashLayout_t startPage, uint8_t blockOffset);
void ResetProgressBar (void);
char PingPongBufferPeek(uint16_t increment);
void PingPongBufferIncrement(uint16_t increment);
bool DecryptBlock( uint8_t* encBlockAddr, uint8_t* decBlockAddr, int32_t blockSize);
flashop_error_t InstallFirmware (uint32_t from, uint32_t size, uint8_t validateOnly);
void UpdateBootLoaderState(loaderStates_t newState);
void MarkProvisionnalCounter(uint8_t count);
void WriteStreamToFlash (uint8_t * buf, uint16_t length, uint32_t address);
uint32_t GetBytesLeftInCurrentBuffer(void);
uint8_t FlashOpsUnitTests(void);
flashop_error_t VerifyFlashIntegrity(void);
void ForceInternalFlashCorruption(void);

#endif /* _FlashOps_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
