/**
  ******************************************************************************
  * File Name          : mxconstants.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  *
  * COPYRIGHT(c) 2016 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MXCONSTANT_H
#define __MXCONSTANT_H
  /* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/


/* USER CODE BEGIN Private defines */
#define WIFI__SPI__CLK_Pin              GPIO_PIN_2
#define WIFI__SPI__CLK_GPIO_Port        GPIOE
#define WIFI_SLAVE_SELECT_Pin           GPIO_PIN_4
#define WIFI_SLAVE_SELECT_GPIO_Port     GPIOE
#define WIFI_MISO_Pin                   GPIO_PIN_5
#define WIFI_MISO_GPIO_Port             GPIOE
#define WIFI_MOSI_Pin                   GPIO_PIN_6
#define WIFI_MOSI_GPIO_Port             GPIOE
#define WIFI_RST_Pin                    GPIO_PIN_13
#define WIFI_RST_GPIO_Port              GPIOC
#define WIFI_WIFI_INTR_N_Pin            GPIO_PIN_5
#define WIFI_WIFI_INTR_N_GPIO_Port      GPIOG
#define WIFI_READY_N_Pin                GPIO_PIN_9
#define WIFI_READY_N_GPIO_Port          GPIOG

#define ZIGBEE_TX_Pin                   GPIO_PIN_12
#define ZIGBEE_TX_GPIO_Port             GPIOC
#define ZIGBEE_UART_RX_Pin              GPIO_PIN_2
#define ZIGBEE_UART_RX_GPIO_Port        GPIOD
#define ZIGBEE_POWER_Pin                GPIO_PIN_3
#define ZIGBEE_POWER_GPIO_Port          GPIOE
#define ZIGBEE_BYPASS_Pin               GPIO_PIN_7
#define ZIGBEE_BYPASS_GPIO_Port         GPIOD

#define TestLock_Pin                    GPIO_PIN_8
#define TestLock_GPIO_Port              GPIOC

#define ZC_Pin                          GPIO_PIN_0
#define ZC_GPIO_Port                    GPIOA
#define VCAP_Pin                        GPIO_PIN_1
#define VCAP_GPIO_Port                  GPIOG

#define MOD1_Pin                        GPIO_PIN_5
#define MOD1_GPIO_Port                  GPIOD
#define MOD2_Pin                        GPIO_PIN_4
#define MOD2_GPIO_Port                  GPIOD

#define FLASH_SLAVE_SELECT_Pin          GPIO_PIN_6
#define FLASH_SLAVE_SELECT_GPIO_Port    GPIOF
#define FLASH_SPI_CLK_Pin               GPIO_PIN_7
#define FLASH_SPI_CLK_GPIO_Port         GPIOF
#define FLASH_MISO_Pin                  GPIO_PIN_8
#define FLASH_MISO_GPIO_Port            GPIOF
#define FLASH_MOSI_Pin                  GPIO_PIN_9
#define FLASH_MOSI_GPIO_Port            GPIOF
#define FLASH_HLD_SIO3_Pin              GPIO_PIN_2
#define FLASH_HLD_SIO3_GPIO_Port        GPIOG
#define FLASH_WP_SIO2_Pin               GPIO_PIN_3
#define FLASH_WP_SIO2_GPIO_Port         GPIOG

#define SDRAM_FMC_SDNWE_Pin             GPIO_PIN_0
#define SDRAM_FMC_SDNWE_GPIO_Port       GPIOC
#define SDRAM_FMC_SDNE0_Pin             GPIO_PIN_2
#define SDRAM_FMC_SDNE0_GPIO_Port       GPIOC
#define SDRAM_FMC_SDCKE0_Pin            GPIO_PIN_3
#define SDRAM_FMC_SDCKE0_GPIO_Port      GPIOC
#define SDRAM_FMC_SDNRAS_Pin            GPIO_PIN_11
#define SDRAM_FMC_SDNRAS_GPIO_Port      GPIOF
#define SDRAM_FMC_SDNCAS_Pin            GPIO_PIN_15
#define SDRAM_FMC_SDNCAS_GPIO_Port      GPIOG
#define SDRAM_FMC_SDCLK_Pin             GPIO_PIN_8
#define SDRAM_FMC_SDCLK_GPIO_Port       GPIOG
#define SDRAM_FMC_NBL0_Pin              GPIO_PIN_0
#define SDRAM_FMC_NBL0_GPIO_Port        GPIOE
#define SDRAM_FMC_NBL1_Pin              GPIO_PIN_1
#define SDRAM_FMC_NBL1_GPIO_Port        GPIOE
#define SDRAM_FMC_D0_Pin                GPIO_PIN_14
#define SDRAM_FMC_D0_GPIO_Port          GPIOD
#define SDRAM_FMC_D1_Pin                GPIO_PIN_15
#define SDRAM_FMC_D1_GPIO_Port          GPIOD
#define SDRAM_FMC_D2_Pin                GPIO_PIN_0
#define SDRAM_FMC_D2_GPIO_Port          GPIOD
#define SDRAM_FMC_D3_Pin                GPIO_PIN_1
#define SDRAM_FMC_D3_GPIO_Port          GPIOD
#define SDRAM_FMC_D4_Pin                GPIO_PIN_7
#define SDRAM_FMC_D4_GPIO_Port          GPIOE
#define SDRAM_FMC_D5_Pin                GPIO_PIN_8
#define SDRAM_FMC_D5_GPIO_Port          GPIOE
#define SDRAM_FMC_D6_Pin                GPIO_PIN_9
#define SDRAM_FMC_D6_GPIO_Port          GPIOE
#define SDRAM_FMC_D7_Pin                GPIO_PIN_10
#define SDRAM_FMC_D7_GPIO_Port          GPIOE
#define SDRAM_FMC_D8_Pin                GPIO_PIN_11
#define SDRAM_FMC_D8_GPIO_Port          GPIOE
#define SDRAM_FMC_D9_Pin                GPIO_PIN_12
#define SDRAM_FMC_D9_GPIO_Port          GPIOE
#define SDRAM_FMC_D10_Pin               GPIO_PIN_13
#define SDRAM_FMC_D10_GPIO_Port         GPIOE
#define SDRAM_FMC_D11_Pin               GPIO_PIN_14
#define SDRAM_FMC_D11_GPIO_Port         GPIOE
#define SDRAM_FMC_D12_Pin               GPIO_PIN_15
#define SDRAM_FMC_D12_GPIO_Port         GPIOE
#define SDRAM_FMC_D13_Pin               GPIO_PIN_8
#define SDRAM_FMC_D13_GPIO_Port         GPIOD
#define SDRAM_FMC_D14_Pin               GPIO_PIN_9
#define SDRAM_FMC_D14_GPIO_Port         GPIOD
#define SDRAM_FMC_D15_Pin               GPIO_PIN_10
#define SDRAM_FMC_D15_GPIO_Port         GPIOD
#define SDRAM_FMC_A0_Pin                GPIO_PIN_0
#define SDRAM_FMC_A0_GPIO_Port          GPIOF
#define SDRAM_FMC_A1_Pin                GPIO_PIN_1
#define SDRAM_FMC_A1_GPIO_Port          GPIOF
#define SDRAM_FMC_A2_Pin                GPIO_PIN_2
#define SDRAM_FMC_A2_GPIO_Port          GPIOF
#define SDRAM_FMC_A3_Pin                GPIO_PIN_3
#define SDRAM_FMC_A3_GPIO_Port          GPIOF
#define SDRAM_FMC_A4_Pin                GPIO_PIN_4
#define SDRAM_FMC_A4_GPIO_Port          GPIOF
#define SDRAM_FMC_A5_Pin                GPIO_PIN_5
#define SDRAM_FMC_A5_GPIO_Port          GPIOF
#define SDRAM_FMC_A6_Pin                GPIO_PIN_12
#define SDRAM_FMC_A6_GPIO_Port          GPIOF
#define SDRAM_FMC_A7_Pin                GPIO_PIN_13
#define SDRAM_FMC_A7_GPIO_Port          GPIOF
#define SDRAM_FMC_A8_Pin                GPIO_PIN_14
#define SDRAM_FMC_A8_GPIO_Port          GPIOF
#define SDRAM_FMC_A9_Pin                GPIO_PIN_15
#define SDRAM_FMC_A9_GPIO_Port          GPIOF
#define SDRAM_FMC_A10_Pin               GPIO_PIN_0
#define SDRAM_FMC_A10_GPIO_Port         GPIOG
#define SDRAM_FMC_BA0_Pin               GPIO_PIN_4
#define SDRAM_FMC_BA0_GPIO_Port         GPIOG

#define T_ACQ_REF_Pin                   GPIO_PIN_1
#define T_ACQ_REF_GPIO_Port             GPIOC
#define T_ACQ_AMB_Pin                   GPIO_PIN_1
#define T_ACQ_AMB_GPIO_Port             GPIOA
#define T_ACQ_COMP_Pin                  GPIO_PIN_2
#define T_ACQ_COMP_GPIO_Port            GPIOA
#define T_ACQ_HT_Pin                    GPIO_PIN_5
#define T_ACQ_HT_GPIO_Port              GPIOC
#define T_ACQ_ENABLE_Pin                GPIO_PIN_14
#define T_ACQ_ENABLE_GPIO_Port          GPIOG

#define HUM_ENABLE_Pin                  GPIO_PIN_2
#define HUM_ENABLE_GPIO_Port            GPIOB
#define HUM_PROX_SDA_Pin                GPIO_PIN_9
#define HUM_PROX_SDA_GPIO_Port          GPIOC
#define HUM_PROX_SCL_Pin                GPIO_PIN_8
#define HUM_PROX_SCL_GPIO_Port          GPIOA

#define CURRENT_Pin                     GPIO_PIN_7
#define CURRENT_GPIO_Port               GPIOA

#define PIR_Pin                         GPIO_PIN_4
#define PIR_GPIO_Port                   GPIOC

#define TFT_SPI_SLAVE_SELECT_Pin        GPIO_PIN_12
#define TFT_SPI_SLAVE_SELECT_GPIO_Port  GPIOB
#define TFT_SPI_SCK_Pin                 GPIO_PIN_13
#define TFT_SPI_SCK_GPIO_Port           GPIOB
#define TFT_SPI_MISO_Pin                GPIO_PIN_14
#define TFT_SPI_MISO_GPIO_Port          GPIOB
#define TFT_SPI_MOSI_Pin                GPIO_PIN_15
#define TFT_SPI_MOSI_GPIO_Port          GPIOB
#define TFT_BACKLIGHT_Pin               GPIO_PIN_5
#define TFT_BACKLIGHT_GPIO_Port         GPIOA
#define TFT_DCX_Pin                     GPIO_PIN_11
#define TFT_DCX_GPIO_Port               GPIOD
#define TFT_RESET_Pin                   GPIO_PIN_5
#define TFT_RESET_GPIO_Port             GPIOB
#define TFT_LCD_CLK_Pin                 GPIO_PIN_7
#define TFT_LCD_CLK_GPIO_Port           GPIOG
#define TFT_LCD_VSYNC_Pin               GPIO_PIN_4
#define TFT_LCD_VSYNC_GPIO_Port         GPIOA
#define TFT_LCD_HSYNC_Pin               GPIO_PIN_6
#define TFT_LCD_HSYNC_GPIO_Port         GPIOC
#define TFT_LCD_DE_Pin                  GPIO_PIN_10
#define TFT_LCD_DE_GPIO_Port            GPIOF
#define TFT_LCD_R2_Pin                  GPIO_PIN_10
#define TFT_LCD_R2_GPIO_Port            GPIOC
#define TFT_LCD_R3_Pin                  GPIO_PIN_0
#define TFT_LCD_R3_GPIO_Port            GPIOB
#define TFT_LCD_R4_Pin                  GPIO_PIN_11
#define TFT_LCD_R4_GPIO_Port            GPIOA
#define TFT_LCD_R5_Pin                  GPIO_PIN_12
#define TFT_LCD_R5_GPIO_Port            GPIOA
#define TFT_LCD_R6_Pin                  GPIO_PIN_1
#define TFT_LCD_R6_GPIO_Port            GPIOB
#define TFT_LCD_R7_Pin                  GPIO_PIN_6
#define TFT_LCD_R7_GPIO_Port            GPIOG
#define TFT_LCD_G2_Pin                  GPIO_PIN_6
#define TFT_LCD_G2_GPIO_Port            GPIOA
#define TFT_LCD_G3_Pin                  GPIO_PIN_10
#define TFT_LCD_G3_GPIO_Port            GPIOG
#define TFT_LCD_G4_Pin                  GPIO_PIN_10
#define TFT_LCD_G4_GPIO_Port            GPIOB
#define TFT_LCD_G5_Pin                  GPIO_PIN_11
#define TFT_LCD_G5_GPIO_Port            GPIOB
#define TFT_LCD_G6_Pin                  GPIO_PIN_7
#define TFT_LCD_G6_GPIO_Port            GPIOC
#define TFT_LCD_G7_Pin                  GPIO_PIN_3
#define TFT_LCD_G7_GPIO_Port            GPIOD
#define TFT_LCD_B2_Pin                  GPIO_PIN_6
#define TFT_LCD_B2_GPIO_Port            GPIOD
#define TFT_LCD_B3_Pin                  GPIO_PIN_11
#define TFT_LCD_B3_GPIO_Port            GPIOG
#define TFT_LCD_B4_Pin                  GPIO_PIN_12
#define TFT_LCD_B4_GPIO_Port            GPIOG
#define TFT_LCD_B5_Pin                  GPIO_PIN_3
#define TFT_LCD_B5_GPIO_Port            GPIOA
#define TFT_LCD_B6_Pin                  GPIO_PIN_8
#define TFT_LCD_B6_GPIO_Port            GPIOB
#define TFT_LCD_B7_Pin                  GPIO_PIN_9
#define TFT_LCD_B7_GPIO_Port            GPIOB

#define TS_RESET_Pin                    GPIO_PIN_13
#define TS_RESET_GPIO_Port              GPIOG
#define TS_IRQ_Pin                      GPIO_PIN_4
#define TS_IRQ_GPIO_Port                GPIOB
#define TS_SCL_Pin                      GPIO_PIN_6
#define TS_SCL_GPIO_Port                GPIOB
#define TS_SDA_Pin                      GPIO_PIN_7
#define TS_SDA_GPIO_Port                GPIOB

#define FTDI_HOST_TX_Pin                GPIO_PIN_9
#define FTDI_HOST_TX_GPIO_Port          GPIOA
#define FTDI_HOST_RX_Pin                GPIO_PIN_10
#define FTDI_HOST_TX_GPIO_Port          GPIOA

#define SWDIO_Pin                       GPIO_PIN_13
#define SWDIO_GPIO_Port                 GPIOA
#define SWCLK_Pin                       GPIO_PIN_14
#define SWCLK_GPIO_Port                 GPIOA
#define SWO_Pin                         GPIO_PIN_3
#define SWO_GPIO_Port                   GPIOB

#define OSC_32_IN_Pin                   GPIO_PIN_14
#define OSC_32_IN_GPIO_Port             GPIOC
#define OSC_32_OUT_Pin                  GPIO_PIN_15
#define OSC_32_OUT_GPIO_Port            GPIOC

//Unused GPIOs
#define H0_Pin                          GPIO_PIN_0
#define H0_GPIO_Port                    GPIOH
#define H1_Pin                          GPIO_PIN_1
#define H1_GPIO_Port                    GPIOH


/* USER CODE END Private defines */

/**
  * @}
  */

/**
  * @}
*/

#endif /* __MXCONSTANT_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
