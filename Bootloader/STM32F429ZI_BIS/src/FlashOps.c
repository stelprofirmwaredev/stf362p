/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    FlashOps.c
* @date    2016/07/07
* @authors J-F. Simard & Alexandre Courtemanche
* @brief
*******************************************************************************/
/*******************************************************************************
*    Includes
*******************************************************************************/
#include ".\inc\FlashOps.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

#include "stm32f4xx_hal.h"
#include "FlashLayout.h"
#include "loaderStates.h"
#include "BootLoader_OTA_Control.h"
#include ".\HAL\inc\HAL_Flash.h"
#include ".\HAL\inc\HAL_Console.h"
#include "SREC_Utility.h"
#include "crypto.h"
#include "bootDisplay.h"
#include "..\..\common\crc\inc\CRC_Utility.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define FLASH_SECTOR_CNT        8
#define EXTFLASH_READ_BLOCKSIZE 0x8000      //32768 bytes
#define FLASH_TIMEOUT_VALUE       ((uint32_t)50000U)/* 50 s */
/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
const FlashSector_Info_t FlashSectors_Info [FLASH_SECTOR_CNT] =
{
    [FLASH_SECTOR_0] =
    {
        .StartAddress = 0x8000000,
        .SectorSize = FLASH_SECTOR_16K,
    },

    [FLASH_SECTOR_1] =
    {
        .StartAddress = 0x8004000,
        .SectorSize = FLASH_SECTOR_16K,
    },

    [FLASH_SECTOR_2] =
    {
        .StartAddress = 0x8008000,
        .SectorSize = FLASH_SECTOR_16K,
    },

    [FLASH_SECTOR_3] =
    {
        .StartAddress = 0x800C000,
        .SectorSize = FLASH_SECTOR_16K,
    },

    [FLASH_SECTOR_4] =
    {
        .StartAddress = 0x8010000,
        .SectorSize = FLASH_SECTOR_64K,
    },

    [FLASH_SECTOR_5] =
    {
        .StartAddress = 0x8020000,
        .SectorSize = FLASH_SECTOR_128K,
    },

    [FLASH_SECTOR_6] =
    {
        .StartAddress = 0x8040000,
        .SectorSize = FLASH_SECTOR_128K,
    },

    [FLASH_SECTOR_7] =
    {
        .StartAddress = 0x8060000,
        .SectorSize = FLASH_SECTOR_128K,
    },                                          //end of 512kB

//    [FLASH_SECTOR_8] =
//    {
//        .StartAddress = 0x8080000,
//        .SectorSize = FLASH_SECTOR_128K,
//    },
//
//    [FLASH_SECTOR_9] =
//    {
//        .StartAddress = 0x80A0000,
//        .SectorSize = FLASH_SECTOR_128K,
//    },
//
//    [FLASH_SECTOR_10] =
//    {
//        .StartAddress = 0x80C0000,
//        .SectorSize = FLASH_SECTOR_128K,
//    },
//
//    [FLASH_SECTOR_11] =
//    {
//        .StartAddress = 0x80E0000,
//        .SectorSize = FLASH_SECTOR_128K,
//    },                                          //end of 1MB
//
//    [FLASH_SECTOR_12] =
//    {
//        .StartAddress = 0x8100000,
//        .SectorSize = FLASH_SECTOR_16K,
//    },
//
//    [FLASH_SECTOR_13] =
//    {
//        .StartAddress = 0x8104000,
//        .SectorSize = FLASH_SECTOR_16K,
//    },
//
//    [FLASH_SECTOR_14] =
//    {
//        .StartAddress = 0x8108000,
//        .SectorSize = FLASH_SECTOR_16K,
//    },
//
//    [FLASH_SECTOR_15] =
//    {
//        .StartAddress = 0x810C000,
//        .SectorSize = FLASH_SECTOR_16K,
//    },
//
//    [FLASH_SECTOR_16] =
//    {
//        .StartAddress = 0x8110000,
//        .SectorSize = FLASH_SECTOR_64K,
//    },
//
//    [FLASH_SECTOR_17] =
//    {
//        .StartAddress = 0x8120000,
//        .SectorSize = FLASH_SECTOR_128K,
//    },
//
//    [FLASH_SECTOR_18] =
//    {
//        .StartAddress = 0x8140000,
//        .SectorSize = FLASH_SECTOR_128K,
//    },
//
//    [FLASH_SECTOR_19] =
//    {
//        .StartAddress = 0x8160000,
//        .SectorSize = FLASH_SECTOR_128K,
//    },
//
//    [FLASH_SECTOR_20] =
//    {
//        .StartAddress = 0x8180000,
//        .SectorSize = FLASH_SECTOR_128K,
//    },
//
//    [FLASH_SECTOR_21] =
//    {
//        .StartAddress = 0x81A0000,
//        .SectorSize = FLASH_SECTOR_128K,
//    },
//
//    [FLASH_SECTOR_22] =
//    {
//        .StartAddress = 0x81C0000,
//        .SectorSize = FLASH_SECTOR_128K,
//    },
//
//    [FLASH_SECTOR_23] =
//    {
//        .StartAddress = 0x81E0000,
//        .SectorSize = FLASH_SECTOR_128K,
//    },                                          //end of 2MB
};

const uint8_t AESkey[] =  "8VSirM1T5RdVySQt2OJ8NxwLeOZWafYJ";

const uint8_t IV[] =      "J5tjW8ZsVTjRTuv4";

//const uint8_t AESkey[] =  {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
//                           0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11,
//                           0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A,
//                           0x1B, 0x1C, 0x1D, 0x1E, 0x1F};
//
//const uint8_t IV[] =      {0x7D, 0x7E, 0x7F, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85,
//                           0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B, 0x8C};

extern FLASH_ProcessTypeDef pFlash;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/

/*
*   This is a Ping-Pong Buffer implementation, which is required when data is
*   both consumed and produced concurrently. Data is consumed at bufferOffset
*   and the moved using the PingPongBufferIncrement function. When bufferOffset
*   reaches close to the end of the buffer, an additional block of input bytes
*   is both read and decrypted in an alternative buffer.
*
*/

// Two alternating destination buffers
static char SrecBuffer1[EXTFLASH_READ_BLOCKSIZE];
static char SrecBuffer2[EXTFLASH_READ_BLOCKSIZE];
// A buffer for storing temporarily encrypted recoreds
static char SrecBufferEncrypted[EXTFLASH_READ_BLOCKSIZE];
// Pointer to the currently-being-used pointer
static char* curBufferPtr;
// Offset of where the current byte is being consumed.
static uint16_t bufferOffset;


/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
flashop_error_t VerifyFlashIntegrity(void)
{
    uint32_t calculated_crc;
    CRC_HandleTypeDef crcHandle;
    
    crcHandle.Instance = CRC;
    crcHandle.Lock = HAL_UNLOCKED;
    crcHandle.State = HAL_CRC_STATE_RESET;
    HAL_CRC_Init(&crcHandle);
    HAL_CRC_Calculate(&crcHandle,NULL, 0);        //force a reset of the CRC registers
    
    for (uint32_t i = FLASH_BASE_ACTIVE_IMAGE; i < FLASH_BASE_CRC;i+=4)
    {
        calculated_crc = HAL_CRC_Accumulate(&crcHandle, (uint32_t*)i, 1);
    }

    if (calculated_crc == *(uint32_t*)FLASH_BASE_CRC)
    {
        return FLASHOP_NOERROR;
    }
    else
    {
        return FLASHOP_INTERNALFLASH_CRC_ERROR;
    }
}


/*******************************************************************************
* @brief  Returns the amount of bytes left in the current buffer before a switch
*           to the next buffer has to be made
* @inputs None
* @retval Bytes available in current buffer
* @author Alexandre Courtemanche
* @date   2017/09/25
*******************************************************************************/
uint32_t GetBytesLeftInCurrentBuffer(void)
{
    return (EXTFLASH_READ_BLOCKSIZE - bufferOffset);
}


/*******************************************************************************
* @brief  Decrypts a block of 32768 bytes.
* @inputs
*       - uint8_t* blockReadAddr : The address of the encrypted block of bytes
*       - uint8_t* blockWriteAddr : The address at which to write the decrypted
*                                   block
*       - int32_t inputSize : The size of the block being decrypted
* @retval
*       - bool : Decryption success
* @author Alexandre Courtemanche
* @date   2017/09/25
*******************************************************************************/
bool DecryptBlock( uint8_t* encBlockAddr, uint8_t* decBlockAddr,
    int32_t blockSize)
{

    AESECBctx_stt AESctx;
    AESctx.mFlags = E_SK_DEFAULT;
    AESctx.mKeySize = 32;
    uint32_t err = AES_SUCCESS;
    int32_t bytesDecrypted;
    bool ret;

    // CRC clock enable is necessary to perform decryption with the ST library
    __HAL_RCC_CRC_CLK_ENABLE();

    if( blockSize % 16 != 0)
    {
//        CONSOLE_RUNTIME_MESSAGE("CAN ONLY DECRYPT BLOCK SIZES MULTIPLES OF 16");
        ret = false;
    }
    else
    {
        err |= AES_ECB_Decrypt_Init(&AESctx, AESkey, IV);

        err |= AES_ECB_Decrypt_Append(   &AESctx,
                            (uint8_t*)encBlockAddr,
                            blockSize,
                            decBlockAddr,
                            &bytesDecrypted);

        err |= AES_ECB_Decrypt_Finish(   &AESctx,
                    decBlockAddr,
                    &bytesDecrypted);

        if (err != AES_SUCCESS)
        {
            CONSOLE_RUNTIME_MESSAGE("ERROR PERFORMING DECRYPTION");
        }
        ret = (err == AES_SUCCESS);
    }

    return ret;

}


/**
  * @brief  Program byte, halfword, word or double word at a specified address
  * @param  TypeProgram:  Indicate the way to program at a specified address.
  *                           This parameter can be a value of @ref FLASH_Type_Program
  * @param  Address:  specifies the address to be programmed.
  * @param  Data: specifies the data to be programmed
  *
  * @retval HAL_StatusTypeDef HAL Status
  */
HAL_StatusTypeDef Flash_Program(uint32_t TypeProgram, uint32_t Address, uint64_t Data)
{
    HAL_StatusTypeDef status = HAL_ERROR;

    /* Process Locked */
    __HAL_LOCK(&pFlash);

    /* Check the parameters */
    assert_param(IS_FLASH_TYPEPROGRAM(TypeProgram));

    /* Wait for last operation to be completed */
    status = FLASH_WaitForLastOperation((uint32_t)FLASH_TIMEOUT_VALUE);

    if(status == HAL_OK)
    {
        assert_param(IS_FLASH_ADDRESS(Address));
        if(TypeProgram == FLASH_TYPEPROGRAM_BYTE)
        {
            CLEAR_BIT(FLASH->CR, FLASH_CR_PSIZE);
            FLASH->CR |= FLASH_PSIZE_BYTE;
            FLASH->CR |= FLASH_CR_PG;
            *(__IO uint8_t*)Address = Data;
        }
//        else if(TypeProgram == FLASH_TYPEPROGRAM_HALFWORD)
//        {
//            CLEAR_BIT(FLASH->CR, FLASH_CR_PSIZE);
//            FLASH->CR |= FLASH_PSIZE_HALF_WORD;
//            FLASH->CR |= FLASH_CR_PG;
//            *(__IO uint8_t*)Address = Data;
//        }

        /* Wait for last operation to be completed */
        status = FLASH_WaitForLastOperation((uint32_t)FLASH_TIMEOUT_VALUE);

        /* If the program operation is completed, disable the PG Bit */
        FLASH->CR &= (~FLASH_CR_PG);
    }

    /* Process Unlocked */
    __HAL_UNLOCK(&pFlash);

  return status;
}

/**
  * @brief  Perform a mass erase or erase the specified FLASH memory sectors
  * @param[in]  pEraseInit: pointer to an FLASH_EraseInitTypeDef structure that
  *         contains the configuration information for the erasing.
  *
  * @param[out]  SectorError: pointer to variable  that
  *         contains the configuration information on faulty sector in case of error
  *         (0xFFFFFFFFU means that all the sectors have been correctly erased)
  *
  * @retval HAL Status
  */
HAL_StatusTypeDef FlashEx_Erase(FLASH_EraseInitTypeDef *pEraseInit, uint32_t *SectorError)
{
  HAL_StatusTypeDef status = HAL_ERROR;
  uint32_t index = 0U;

  /* Process Locked */
  __HAL_LOCK(&pFlash);

  /* Check the parameters */
  assert_param(IS_FLASH_TYPEERASE(pEraseInit->TypeErase));

  /* Wait for last operation to be completed */
  status = FLASH_WaitForLastOperation((uint32_t)FLASH_TIMEOUT_VALUE);

  if(status == HAL_OK)
  {
    /*Initialization of SectorError variable*/
    *SectorError = 0xFFFFFFFFU;
    {
      /* Check the parameters */
      assert_param(IS_FLASH_NBSECTORS(pEraseInit->NbSectors + pEraseInit->Sector));

      /* Erase by sector by sector to be done*/
      for(index = pEraseInit->Sector; index < (pEraseInit->NbSectors + pEraseInit->Sector); index++)
      {
        FLASH_Erase_Sector(index, (uint8_t) pEraseInit->VoltageRange);

        /* Wait for last operation to be completed */
        status = FLASH_WaitForLastOperation((uint32_t)FLASH_TIMEOUT_VALUE);

        /* If the erase operation is completed, disable the SER and SNB Bits */
        CLEAR_BIT(FLASH->CR, (FLASH_CR_SER | FLASH_CR_SNB));

        if(status != HAL_OK)
        {
          /* In case of error, stop erase procedure and return the faulty sector*/
          *SectorError = index;
          break;
        }
      }
    }
    /* Flush the caches to be sure of the data consistency */
    FLASH_FlushCaches();
  }

  /* Process Unlocked */
  __HAL_UNLOCK(&pFlash);

  return status;
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/


/*******************************************************************************
* @brief  This function allows to erase an area of the flash memory.
*       An area is defined as X number of flash pages
* @inputs
*       - uint32* startPage : the page adress of the beginning of the section
*       - uint8 blockOffset : 1 block contains X number of Flash pages
*
* @retval None
* @author J-F. Simard
* @date   2016/07/07
*******************************************************************************/
void EraseFlashArea(FlashLayout_t startPage, uint8_t blockOffset)
{
    FLASH_EraseInitTypeDef pageInfo;
    uint32_t pageError;

    switch (startPage)
    {
    case FLASH_BASE_ACTIVE_IMAGE:
        if ((blockOffset+FLASH_SECTOR_OFFSET) <= FLASH_LAST_SECTOR)
        {
            pageInfo.TypeErase = FLASH_TYPEERASE_SECTORS;
            pageInfo.Banks = ((blockOffset+FLASH_SECTOR_OFFSET)<FLASH_SECTOR_12)?FLASH_BANK_1:FLASH_BANK_2;
            pageInfo.Sector = blockOffset+FLASH_SECTOR_OFFSET;
            pageInfo.NbSectors = 1;
            pageInfo.VoltageRange = FLASH_VOLTAGE_RANGE_3;

            HAL_FLASH_Unlock();

            FlashEx_Erase(&pageInfo, &pageError);

            HAL_FLASH_Lock();
        }
        break;

    case FLASH_BASE_BOOTLOADER:
    case FLASH_BASE_OTA_CONTROL:
    default :
        break;
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void ResetProgressBar (void)
{
    FLASH_EraseInitTypeDef pageInfo;
    uint32_t pageError;

    pageInfo.Banks = FLASH_BANK_1;
    pageInfo.Sector = FLASH_SECTOR_1;
    pageInfo.NbSectors = 1;
    pageInfo.VoltageRange = FLASH_VOLTAGE_RANGE_3;

    HAL_FLASH_Unlock();

    FlashEx_Erase(&pageInfo, &pageError);

    HAL_FLASH_Lock();
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/08/03
*******************************************************************************/
void EraseCodeMemory(void)
{
    for (uint8_t i = 0; i <= (FLASH_LAST_SECTOR-FLASH_FIRST_SECTOR); i++)
    {
        CONSOLE_RUNTIME_MESSAGE("Erasing flash sector ");
        CONSOLE_MESSAGE_APPEND_INTEGER(i + FLASH_FIRST_SECTOR);

        EraseFlashArea(FLASH_BASE_ACTIVE_IMAGE, i);
        DrawProgressBar((100*i)/(FLASH_LAST_SECTOR-FLASH_FIRST_SECTOR),0);
    }

    DrawProgressBar(75,1);
}


/*******************************************************************************
* @brief This function increments the character pointer and brings it to the
*           alternative buffer when it reaches the end of the current buffer
* @inputs
*       - uint16 increment : The amount by which to increment the pointer
* @retval None
* @author Alexandre Courtemanche
* @date   2017/09/25
*******************************************************************************/
void PingPongBufferIncrement(uint16_t increment)
{
    if ( (bufferOffset + increment) >= EXTFLASH_READ_BLOCKSIZE)
    {
        // Increment to the alternate buffer
        curBufferPtr = (curBufferPtr == SrecBuffer1) ? SrecBuffer2 : SrecBuffer1;
        bufferOffset = (bufferOffset + increment) % EXTFLASH_READ_BLOCKSIZE;
    }
    else
    {
        bufferOffset += increment;
    }
}

/*******************************************************************************
* @brief  Reads the character at a particular point ahead in the buffer and
*         abstracts the need to switch buffers if looking too far ahead.
* @inputs
*       - uint16 increment : The offset at which to look ahead
* @retval None
* @author Alexandre Courtemanche
* @date   2017/09/25
*******************************************************************************/
char PingPongBufferPeek(uint16_t increment)
{
    char peekedChar;
    char* peekedBuffer = curBufferPtr;
    uint16_t peekOffset = (bufferOffset + increment);

    if ( (bufferOffset + increment) >= EXTFLASH_READ_BLOCKSIZE)
    {
        // Peek to the alternate buffer
        peekedBuffer = (curBufferPtr == SrecBuffer1) ? SrecBuffer2 : SrecBuffer1;
        peekOffset = peekOffset % EXTFLASH_READ_BLOCKSIZE;
    }

    peekedChar = peekedBuffer[peekOffset];

    return peekedChar;
}


/*******************************************************************************
* @brief
* @inputs
*       - uint32_t from : identify the external flash block from where the the
*                           new firmware
*               should be installed
*       - uint32_t size : The size in bytes of the new image to be installed
*       - uint8_t validateOnly: if true, will only validate the SRecords and not
*                                write them to flash
* @retval None
* @author J-F. Simard
* @date   2018/08/07
*******************************************************************************/
flashop_error_t InstallFirmware (uint32_t from, uint32_t size, uint8_t validateOnly)
{
    flashop_error_t flashop_error_status = FLASHOP_NOERROR;
    uint32_t readAddress;
    uint8_t* writeAddr;
    bool freshBlock = false;
    uint16_t numBlocks = (size / EXTFLASH_READ_BLOCKSIZE);
    uint8_t endOfFile = 0;

    // Initialize buffers and offsets
    bufferOffset = 0;
    curBufferPtr = SrecBuffer1;

    if (VALIDATE_AND_INSTALL_FIRMWARE == validateOnly)
    {
        DrawProgressBar(50,1);
        EraseCodeMemory();
    }

    for (uint32_t blockCnt = 0; blockCnt < numBlocks; blockCnt++)
    {
        srec_bin_t nextRecord;

        if (VALIDATE_FIRMWARE_ONLY == validateOnly)
        {
            CONSOLE_RUNTIME_MESSAGE("Validating");
        }
        else
        {
            CONSOLE_RUNTIME_MESSAGE("Applying");
        }
        CONSOLE_MESSAGE_APPEND_STRING(" data block ");
        CONSOLE_MESSAGE_APPEND_INTEGER(blockCnt);
        DrawProgressBar((blockCnt*100)/numBlocks,0);

        readAddress = (uint32_t) ( from + (blockCnt*EXTFLASH_READ_BLOCKSIZE) );

        // Determine if the next block is going to be stored in first or second
        // buffer depending if it is a mean or an even block.
        if ((blockCnt % 2) == 0)
        {
            writeAddr = (uint8_t*) SrecBuffer1;
        }
        else
        {
            writeAddr = (uint8_t*) SrecBuffer2;
        }

        HAL_ReadFlashData(readAddress, EXTFLASH_READ_BLOCKSIZE,
            (uint8_t*)SrecBufferEncrypted);

        freshBlock = DecryptBlock( (uint8_t*)SrecBufferEncrypted,
                                    writeAddr,
                                    EXTFLASH_READ_BLOCKSIZE );

        while(PPBufIsNextSRecordComplete(&freshBlock) == SREC_OK )
        {
            PPBufParseNextSrecord(&nextRecord);

            if (ComputeRecordChecksum(&nextRecord) == nextRecord.checksum)
            {
                switch (nextRecord.type)
                {
                case SREC_16BIT_DATA_CODE:
                case SREC_24BIT_DATA_CODE:
                case SREC_32BIT_DATA_CODE:
                    if (VALIDATE_AND_INSTALL_FIRMWARE == validateOnly)
                    {
                        WriteSrecordToFlash(&nextRecord);
                    }
                    break;
                    
                case SREC_16BIT_TERMINATION_CODE:
                case SREC_24BIT_TERMINATION_CODE:
                case SREC_32BIT_TERMINATION_CODE:
                    if (VALIDATE_AND_INSTALL_FIRMWARE == validateOnly)
                    {
                        flashop_error_status = VerifyFlashIntegrity();
                    }
                    endOfFile = 1;
                    break;

                case SREC_HEADER_CODE:
                case 4:
                case SREC_COUNT_CODE:
                case 6:
                default:
                    break;
                }
            }
            else
            {
                if (VALIDATE_AND_INSTALL_FIRMWARE == validateOnly)
                {
                    flashop_error_status = FLASHOP_SRECORD_W_CHECKSUM_ERROR;
                }
                else
                {
                    flashop_error_status = FLASHOP_SRECORD_RO_CHECKSUM_ERROR;
                }
                break;
            }            
            if (0 != endOfFile)
            {
                break;
            }
        }
        if ((FLASHOP_NOERROR != flashop_error_status) ||
            (0 != endOfFile))
        {
            break;
        }        
    }

    if (VALIDATE_FIRMWARE_ONLY == validateOnly)
    {
        DrawProgressBar(50,1);
    }
    else
    {
        DrawProgressBar(100,1);
    }

    //if code reaches this point, the validation of all the records is OK
    return flashop_error_status;
}

/*******************************************************************************
* @brief  Update the bootloader's progress bar after a significative bootloader
*           action.
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void UpdateBootLoaderState(loaderStates_t newState)
{
    loaderStates_t flashLoaderState = GetBootLoaderState();
    loaderStates_t current_state = flashLoaderState;

    uint8_t ignoreNewState;

    char image_name[6] = "\"B\"";

    if (newState >= flashLoaderState)
    {
        ResetProgressBar();
    }

    ignoreNewState = 0;
    switch (newState)
    {
    case LOADER_STATE_1:
        image_name[1]='A';
    case LOADER_STATE_5:
        switch (current_state)
        {
        case LOADER_STATE_4:
        case LOADER_STATE_8:
            CONSOLE_EVENT_MESSAGE("Reverted to image ");
            CONSOLE_MESSAGE_APPEND_STRING(image_name);
            break;

        case LOADER_STATE_7:
        case LOADER_STATE_3:
            CONSOLE_EVENT_MESSAGE("Provisionning of image ");
            CONSOLE_MESSAGE_APPEND_STRING(image_name);
            CONSOLE_MESSAGE_APPEND_STRING(" successfull");
            break;

        case LOADER_STATE_1:
        case LOADER_STATE_5:
            CONSOLE_EVENT_MESSAGE("Image Quick Switch to ");
            CONSOLE_MESSAGE_APPEND_STRING(image_name);
            break;
            
        default:
            CONSOLE_EVENT_MESSAGE("Loading State ");
            CONSOLE_MESSAGE_APPEND_INTEGER (newState);
            CONSOLE_EVENT_MESSAGE(" from bad loader state ");
            CONSOLE_MESSAGE_APPEND_INTEGER (current_state);
            break;
        }
        ResetProgressBar();         //reset provisional counter
        break;

    case LOADER_STATE_6:
        image_name[1]='A';
    case LOADER_STATE_2:
        CONSOLE_EVENT_MESSAGE("New image ");
        CONSOLE_MESSAGE_APPEND_STRING(image_name);
        CONSOLE_MESSAGE_APPEND_STRING(" ready");
        break;

    case LOADER_STATE_7:
        image_name[1]='A';
    case LOADER_STATE_3:
        CONSOLE_EVENT_MESSAGE("Provisionnal image ");
        CONSOLE_MESSAGE_APPEND_STRING(image_name);
        CONSOLE_MESSAGE_APPEND_STRING(" loaded");
        break;

    case LOADER_STATE_8:
        image_name[1]='A';
    case LOADER_STATE_4:
        CONSOLE_EVENT_MESSAGE("Bad image ");
        CONSOLE_MESSAGE_APPEND_STRING(image_name);
        CONSOLE_MESSAGE_APPEND_STRING(" . Fallback needed");
        break;

    default:
        ignoreNewState = 1;
        CONSOLE_EVENT_MESSAGE("Unknown Event");
        break;
    }

    if (ignoreNewState == 0)
    {
        OTAControl_t * flashLoaderState = (OTAControl_t*)FLASH_BASE_OTA_CONTROL;
        loaderStateValidation_t validationWord = 
        {
            .bootldState_validation0 = newState,
            .bootldState_validation1 = (newState^0xff),
        };
        int8_t historyFile = FindLastBootloaderHistory();
        
        if ((historyFile+1) >= SIZEOF_VALIDATION_HISTORY)
        {
            ResetProgressBar();
            historyFile = -1;
        }
        
        HAL_FLASH_Unlock();
        Flash_Program(FLASH_TYPEPROGRAM_BYTE,
                                  (uint32_t)&flashLoaderState->bootLoaderState,
                                  (uint64_t)newState);
        Flash_Program(FLASH_TYPEPROGRAM_BYTE,
                                  (uint32_t)&flashLoaderState->loaderStateHistory[historyFile+1].bootldState_validation0,
                                  validationWord.bootldState_validation0);
        Flash_Program(FLASH_TYPEPROGRAM_BYTE,
                                  (uint32_t)&flashLoaderState->loaderStateHistory[historyFile+1].bootldState_validation1,
                                  validationWord.bootldState_validation1);
        HAL_FLASH_Lock();
#ifdef DEVKIT_PLATFORM
        HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin);
#endif /* DEVKIT_PLATFORM */
    }
}

/*******************************************************************************
* @brief  Update the bootloader's progress bar after a significative bootloader
*           action.
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void MarkProvisionnalCounter(uint8_t count)
{
    OTAControl_t * flashLoaderState = (OTAControl_t*)FLASH_BASE_OTA_CONTROL;    
    
    if (count < PROVISIONAL_RESET_CNT_BEFORE_FALLBACK)
    {
        HAL_FLASH_Unlock();
        Flash_Program(FLASH_TYPEPROGRAM_BYTE,
                      (uint32_t)&flashLoaderState->ProvisionalCounter[count],
                      0);

        HAL_FLASH_Lock();
    }
}

/*******************************************************************************
 * @brief
 * @inputs None
 * @retval None
 * @author
 * @date
 *******************************************************************************/
void WriteStreamToFlash (uint8_t * buf, uint16_t length, uint32_t address)
{
    HAL_FLASH_Unlock();

    for (uint8_t i = 0; i < length; i++)
    {
        Flash_Program(FLASH_TYPEPROGRAM_BYTE,
                        address+i,
                        (uint64_t)(buf[i]));
    }

    HAL_FLASH_Lock();

}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2018/02/19
*******************************************************************************/
void ForceInternalFlashCorruption(void)
{
    HAL_FLASH_Unlock();

    for (uint8_t i = 0; i < 4; i++)
    {
        Flash_Program(FLASH_TYPEPROGRAM_BYTE,
                        FLASH_BASE_CRC+i,
                        0);
    }

    HAL_FLASH_Lock();
}



/*------------------------------------------------------------------------------
                                    UNIT TESTS
------------------------------------------------------------------------------*/

/*******************************************************************************
 * @brief  Tests the PPBufIsNextSRecordComplete() function in various scenarios
 * @inputs None
 * @retval testResult (True if all the tests pass)
 * @author Alexandre Courtemanche
 * @date   18/10/2017
 *******************************************************************************/
bool Test_PPBufAsciiToHex()
{
    bool ret = true;
    char* srec1End = (SrecBuffer1 + EXTFLASH_READ_BLOCKSIZE);
    char* srec2End = (SrecBuffer2 + EXTFLASH_READ_BLOCKSIZE);
    uint32_t parsedValue = 0x0;
    char* testAsciiHexValues = "0102030405060708090A0B0C0D0E0F";
    uint32_t reference32BitValues[] =
        {0x01020304, 0x02030405, 0x03040506, 0x04050607, 0x05060708, 0x06070809,
         0x0708090A, 0x08090A0B, 0x090A0B0C, 0x0A0B0C0D, 0x0B0C0D0E, 0x0C0D0E0F };

    // Case 1
    // Srec is at the end of buffer 1
    int len = strlen(testAsciiHexValues);
    bufferOffset = (EXTFLASH_READ_BLOCKSIZE - len);
    curBufferPtr = SrecBuffer1;
    memcpy(SrecBuffer1 + bufferOffset, testAsciiHexValues, len);

    // Case 1a Test 32 bit hex values parsing
    for (int ii = 0; ii < 12; ii++)
    {
        parsedValue = PPBufAsciiToHex(0, CHARS_PER_BYTE * sizeof(uint32_t));
        PingPongBufferIncrement(CHARS_PER_BYTE);

        ret &= (parsedValue == reference32BitValues[ii]);
    }

    // Case 1b Test 8 bit value parsing
    bufferOffset = (EXTFLASH_READ_BLOCKSIZE - len);
    for (int ii = 1; ii < len/CHARS_PER_BYTE; ii++)
    {
        parsedValue = PPBufAsciiToHex(0, CHARS_PER_BYTE);
        PingPongBufferIncrement(CHARS_PER_BYTE);

        ret &= (parsedValue == ii);
    }

    // Case 1c Test 32 bit hex values parsing
    bufferOffset = (EXTFLASH_READ_BLOCKSIZE - len);
    for (int ii = 0; ii < 12; ii++)
    {
        parsedValue = PPBufAsciiToHex(ii*CHARS_PER_BYTE, CHARS_PER_BYTE * sizeof(uint32_t));
        ret &= (parsedValue == reference32BitValues[ii]);
    }

    // Case 1d Test 8 bit value parsing
    bufferOffset = (EXTFLASH_READ_BLOCKSIZE - len);
    for (int ii = 0; ii < len/CHARS_PER_BYTE; ii++)
    {
        parsedValue = PPBufAsciiToHex(ii*CHARS_PER_BYTE, CHARS_PER_BYTE);
        ret &= (parsedValue == (ii+1));
    }

    // Case 2a Test 32 bit hex value parsing across buffer 1 and buffer 2
    uint32_t testIntHexValue = 0x01020304;
    char* testASCIIHexValue = "01020304";
    len = strlen(testASCIIHexValue);

    memset(SrecBuffer1, 0, EXTFLASH_READ_BLOCKSIZE);
    memset(SrecBuffer2, 0, EXTFLASH_READ_BLOCKSIZE);
    for(int ii = 1; ii < len; ii++)
    {
        curBufferPtr = SrecBuffer1;
        bufferOffset = (EXTFLASH_READ_BLOCKSIZE - len + ii);

        memcpy( srec1End - len + ii, testASCIIHexValue, len );
        memcpy( SrecBuffer2, testASCIIHexValue + len - ii, ii );

        // Parse the Srecord across two buffers
        parsedValue = PPBufAsciiToHex(0, CHARS_PER_BYTE * sizeof(uint32_t));
        ret &= (parsedValue == testIntHexValue);
    }

    // Case 2b Test 32 bit hex value parsing across buffer 2 and buffer 1
    memset(SrecBuffer1, 0, EXTFLASH_READ_BLOCKSIZE);
    memset(SrecBuffer2, 0, EXTFLASH_READ_BLOCKSIZE);
    for(int ii = 1; ii < len; ii++)
    {
        curBufferPtr = SrecBuffer2;
        bufferOffset = (EXTFLASH_READ_BLOCKSIZE - len + ii);

        memcpy( srec2End - len + ii, testASCIIHexValue, len );
        memcpy( SrecBuffer1, testASCIIHexValue + len - ii, ii );

        // Parse the Srecord across two buffers
        parsedValue = PPBufAsciiToHex(0, CHARS_PER_BYTE * sizeof(uint32_t));
        ret &= (parsedValue == testIntHexValue);
    }


    return ret;
}

/*******************************************************************************
 * @brief  Tests the PPBufIsNextSRecordComplete() function in various scenarios
 * @inputs None
 * @retval testResult (True if all the tests pass)
 * @author Alexandre Courtemanche
 * @date   18/10/2017
 *******************************************************************************/
bool Test_PPBufIsNextSRecordComplete()
{
    srecParsing_Err_t parseRet;
    bool ret = true;
    char* testSrec = "S31508008050E9620508ED620508F162050811320508AE\r\n";
    int srecLen = strlen(testSrec);
    bool block_state = false;

    // Case 1
    // Srec is at the end of buffer 1
    bufferOffset = (EXTFLASH_READ_BLOCKSIZE - srecLen);
    curBufferPtr = SrecBuffer1;
    memcpy(SrecBuffer1 + bufferOffset, testSrec, srecLen);

    parseRet = PPBufIsNextSRecordComplete(&block_state);

    ret &= (parseRet == SREC_OK);

    // Case 2a
    // SRec is split between buffer 1 and buffer 2
    char* srec1End = (SrecBuffer1 + EXTFLASH_READ_BLOCKSIZE);
    curBufferPtr = SrecBuffer1;
    for(int ii = 1; ii < srecLen; ii++)
    {
        bufferOffset = (EXTFLASH_READ_BLOCKSIZE - srecLen + ii);

        memcpy( srec1End - srecLen + ii, testSrec, srecLen - ii );

        parseRet = PPBufIsNextSRecordComplete(&block_state);
        // Make sure it fails every time there are not
        // enough bytes left in the buffer
        ret &= (parseRet == SREC_SHORT_BUFFER);
    }

    // Case 2b
    // SRec is split between buffer 2 and buffer 1
    char* srec2End = (SrecBuffer2 + EXTFLASH_READ_BLOCKSIZE);
    curBufferPtr = SrecBuffer2;
    for(int ii = 1; ii < srecLen; ii++)
    {
        bufferOffset = (EXTFLASH_READ_BLOCKSIZE - srecLen + ii);

        memcpy( srec2End - srecLen + ii, testSrec, srecLen - ii );

        parseRet = PPBufIsNextSRecordComplete(&block_state);
        // Make sure it fails every time there are not
        // enough bytes left in the buffer
        ret &= (parseRet == SREC_SHORT_BUFFER);
    }


    return ret;
}

/*******************************************************************************
 * @brief  Tests the Test_PPBufParseNextSrecord() function in various scenarios
 * @inputs None
 * @retval testResult (True if all the tests pass)
 * @author Alexandre Courtemanche
 * @date   19/10/2017
 *******************************************************************************/
bool Test_PPBufParseNextSrecord()
{
    srec_bin_t srecBin;
    srecParsing_Err_t parseRet;
    bool ret = true;

    char* testSrec = "S31508008060934F05087180050875800508798005080D\r\n";
    // Contents of the srecord data
    uint8_t testData[] = {0x93, 0x4F, 0x05, 0x08, 0x71, 0x80, 0x05, 0x08,
                        0x75, 0x80, 0x05, 0x08, 0x79, 0x80, 0x05, 0x08};

    int srecLen = strlen(testSrec);

    // Case 1
    // Srec is at the end of buffer 1
    bufferOffset = (EXTFLASH_READ_BLOCKSIZE - srecLen);
    curBufferPtr = SrecBuffer1;
    memcpy(SrecBuffer1 + bufferOffset, testSrec, srecLen);

    parseRet = PPBufParseNextSrecord(&srecBin);

    // Verify that the contents of the binary srecords match
    ret &= ( srecBin.type == 0x03 );
    ret &= ( srecBin.record_size == 0x15 );
    ret &= ( srecBin.address_of_data == 0x08008060 );
    ret &= ( srecBin.checksum == 0x0D );
    ret &= (memcmp(
        testData,
        srecBin.data, 16) == 0);

    // Case 2a
    // SRec is split between buffer 1 and buffer 2
    char* curSrecBufEnd = (SrecBuffer1 + EXTFLASH_READ_BLOCKSIZE);
    for(int ii = 1; ii < srecLen; ii++)
    {
        curBufferPtr = SrecBuffer1;
        bufferOffset = (EXTFLASH_READ_BLOCKSIZE - srecLen + ii);

        memcpy( curSrecBufEnd - srecLen + ii, testSrec, srecLen );
        memcpy( SrecBuffer2, testSrec + srecLen - ii, ii );
        memset((void*) &srecBin, 0, sizeof(srec_bin_t));

        // Parse the Srecord across two buffers
        parseRet = PPBufParseNextSrecord(&srecBin);
        ret &= (parseRet == SREC_OK);

        // Verify that the contents of the binary srecords match with reference
        ret &= ( srecBin.type == 0x03 );
        ret &= ( srecBin.record_size == 0x15 );
        ret &= ( srecBin.address_of_data == 0x08008060 );
        ret &= ( srecBin.checksum == 0x0D );
        ret &= ( memcmp(testData, srecBin.data, 16) == 0 );
    }

    // Case 2b
    // Srec buffer split between buffer 2 and buffer 1
    curSrecBufEnd = (SrecBuffer2 + EXTFLASH_READ_BLOCKSIZE);
    for(int ii = 1; ii < srecLen; ii++)
    {
        curBufferPtr = SrecBuffer2;
        bufferOffset = (EXTFLASH_READ_BLOCKSIZE - srecLen + ii);

        memcpy( curSrecBufEnd - srecLen + ii, testSrec, srecLen );
        memcpy( SrecBuffer1, testSrec + srecLen - ii, ii );
        memset((void*) &srecBin, 0, sizeof(srec_bin_t));

        // Parse the Srecord across two buffers
        parseRet = PPBufParseNextSrecord(&srecBin);
        ret &= (parseRet == SREC_OK);

        // Verify that the contents of the binary srecords match with reference
        ret &= ( srecBin.type == 0x03 );
        ret &= ( srecBin.record_size == 0x15 );
        ret &= ( srecBin.address_of_data == 0x08008060 );
        ret &= ( srecBin.checksum == 0x0D );
        ret &= ( memcmp(testData, srecBin.data, 16) == 0 );
    }


    return ret;
}


/*******************************************************************************
 * @brief  Tests most the PP buffer functions to make sure they function correctly
 * @inputs None
 * @retval testResult (True if all the tests pass)
 * @author Alexandre Courtemanche
 * @date   18/10/2017
 *******************************************************************************/
uint8_t FlashOpsUnitTests(void)
{
    uint8_t testResult = 1;
#ifdef ENABLE_FLASHOPSUNITTESTS    

    testResult &= Test_PPBufAsciiToHex();
//    testResult &= Test_PPBufIsNextSRecordComplete();
    testResult &= Test_PPBufParseNextSrecord();

    if(testResult == 0)
    {
        CONSOLE_LOG_MESSAGE("Error in PPBuf tests");
    }
    else
    {
        CONSOLE_LOG_MESSAGE("PPBuf tests are a Success");
    }
    
#endif
    return testResult;
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
