/*******************************************************************************
* @file    BootLoader_ProgressBar.h
* @date    2016/06/15
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef _BootLoader_OTA_Control_H
#define _BootLoader_OTA_Control_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>
#include "FlashLayout.h"
#include "loaderStates.h"


/*******************************************************************************
* Public constants definitions
*******************************************************************************/

#define PROVISIONAL_RESET_CNT_BEFORE_FALLBACK       5
#define SIZEOF_VALIDATION_HISTORY                   10
#define VALIDATION_HISTORY_RESET_VALUE              0xFFFF

typedef enum
{
    BOOT_ACTIVE_IMAGE_RUNNING_OK                    = 0,


} BootProgress_t;


typedef union
{
    uint16_t bootldState_validation;
    struct
    {
        uint8_t bootldState_validation0;
        uint8_t bootldState_validation1;
    };
} loaderStateValidation_t;

typedef struct
{
    loaderStates_t bootLoaderState;
    uint8_t ProvisionalCounter[PROVISIONAL_RESET_CNT_BEFORE_FALLBACK];
    /**************************************************************************/
    /*  to ease retro compatibility, the history must be implemented after    */
    /*  the provisional counter so that older version of the bootloader       */
    /*  that does not use history validation will not provide false indication*/
    /*  of the provisional counter.                                           */
    /**************************************************************************/
    loaderStateValidation_t loaderStateHistory[SIZEOF_VALIDATION_HISTORY]; 
} OTAControl_t;


/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void BootLoader_ClearProvisionalState(void);
void NewOTAImageReceived (void);
loaderStates_t GetBootLoaderState (void);
void CountProvisionnalTries(void);
void NewImageLoaded (void);
void FallBackCompleted(void);
void ForceFallBack (void);
void NewImageCorrupted_StayingInCurrentState (void);
void FallbackImageCorrupted_StayingInCurrentState (void);
void LoaderStateCorrupted_ResetToFactory (void);
void ImageQuickSwitch(void);
uint8_t FindLastBootloaderHistory(void);


#endif /* _BootLoader_ProgressBar_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
