/*******************************************************************************
* @file    APP_loaderStates.h
* @date    2016/06/14
* @authors J-F. Simard
* @brief 
*******************************************************************************/

#ifndef APP_LOADERSTATES_H
#define APP_LOADERSTATES_H


/*******************************************************************************
* Includes
*******************************************************************************/



/*******************************************************************************
* Public constants definitions
*******************************************************************************/
typedef enum
{
    LOADER_STATE_1 = 0xff,          //Image A = Active; Image B = Inactive
    LOADER_STATE_2 = 0xfe,          //Image A = Active; Image B = New
    LOADER_STATE_3 = 0xfc,          //Image A = Inactive; Image B = Provisional
    LOADER_STATE_4 = 0xf8,          //Image A = Inactive; Image B = Fallback
    LOADER_STATE_5 = 0xf0,          //Image A = Inactive; Image B = Active
    LOADER_STATE_6 = 0xe0,          //Image A = New; Image B = Active
    LOADER_STATE_7 = 0xc0,          //Image A = Provisional; Image B = Inactive
    LOADER_STATE_8 = 0x80,          //Image A = Fallback; Image B = Inactive
    
    LOADER_INVALID_STATE = 0x00,
} loaderStates_t;

typedef enum
{
    LOADER_OK = 0,
    LOADER_WRONG_STATE,
    LOADER_UNDEFINED_STATE,
} loaderErrStatus_t;

#define LOAD_A_IMAGE        EXTFLASH_HOST_IMAGE_A_START
#define LOAD_B_IMAGE        EXTFLASH_HOST_IMAGE_B_START


/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void RunState (void);



#endif /* APP_loaderStates_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
