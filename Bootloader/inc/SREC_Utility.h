/*******************************************************************************
* @file    SREC_Utility.h
* @date    2017/08/07
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef SREC_Utility_H
#define SREC_Utility_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>


/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define SREC_TYPE_SIZE          2
#define SREC_COUNT_SIZE         2
#define SREC_CHECKSUM_SIZE      2
#define SREC_FOOTER_SIZE        2       //'\r' + 'n'
#define SREC_HEADERFOOTER_SIZE        (SREC_TYPE_SIZE+SREC_COUNT_SIZE+SREC_FOOTER_SIZE)
#define SREC_MAX_DATA_LENGTH        64

// SREC Type Codes
#define SREC_HEADER_CODE            0
#define SREC_16BIT_DATA_CODE        1
#define SREC_24BIT_DATA_CODE        2
#define SREC_32BIT_DATA_CODE        3
#define SREC_COUNT_CODE             5
#define SREC_16BIT_TERMINATION_CODE 7
#define SREC_24BIT_TERMINATION_CODE 8
#define SREC_32BIT_TERMINATION_CODE 9

#define CHARS_PER_BYTE          2

typedef enum
{
    SREC_OK = 0,
    SREC_SHORT_BUFFER,
    SREC_BAD_HEADER,
}srecParsing_Err_t;

typedef struct
{
    uint8_t type;
    uint8_t record_size;
    uint32_t address_of_data;
    uint8_t data[SREC_MAX_DATA_LENGTH];
    uint8_t checksum;
} srec_bin_t;


/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void WriteSrecordToFlash(srec_bin_t * srecord);
uint8_t ComputeRecordChecksum(srec_bin_t * srecord);
srecParsing_Err_t PPBufIsNextSRecordComplete(bool *freshBlock);
srecParsing_Err_t PPBufParseNextSrecord(srec_bin_t * srecord);
uint32_t PPBufAsciiToHex(uint16_t peekOffset, uint8_t size);
srecParsing_Err_t IsSrecordComplete(uint8_t* srecPtr, uint32_t bytesLeft);
uint16_t ParseSrecord(uint8_t* srecPtr, srec_bin_t * srecord);

#endif /* SREC_Utility_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
