/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    BootLoaderWdt.c
* @date    2016/07/14
* @authors J-F. Simard
* @brief   This module enables the watchdog functionalities to the bootloader.  
*           Its purpose is to allow the MCU to reboot on its own in the 
*           eventuallity that the new firmware does not works properly
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include "BootLoaderWdt.h"

#include "stm32f3xx_hal_conf.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/



/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void InitBootWdt (void)
{
    IWDG_HandleTypeDef watchdogInit;
    
    watchdogInit.Instance = IWDG;
    watchdogInit.Init.Prescaler = IWDG_PRESCALER_4;             //Prescaler 4 : 40kHz/4=10kHz
    watchdogInit.Init.Reload = 0xFFF;                           //Reload Max (12 bits)
    watchdogInit.Init.Window = 0xFFF;                           //Window Disabled
    watchdogInit.Lock = HAL_UNLOCKED;
    
    HAL_IWDG_Start(&watchdogInit);
}



/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
