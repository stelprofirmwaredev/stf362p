/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    FlashOps.c
* @date    2016/07/07
* @authors J-F. Simard
* @brief   
*******************************************************************************/
/*******************************************************************************
*    Includes
*******************************************************************************/
#include "FlashOps.h"
#include <stdint.h>

#include "stm32f3xx_hal_conf.h"
#include "stm32f3xx_hal_flash_ex.h"
#include "FlashLayout.h"
#include "loaderStates.h"
#include "BootLoader_ProgressBar.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/



/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  This function copies flash data from one location to another location
* @inputs 
*       - uint32_t from, to : adresses to flash memory
*       - uint8_t size : size in number of pages to copy.
* @retval None
* @author J-F. Simard
* @date   2016/07/08
*******************************************************************************/
void CopyFlashArea(uint32_t * fromFlash, uint32_t * toFlash, uint8_t size)
{
    uint8_t pageCnt;
    uint16_t i;
    
    for (pageCnt = 0; pageCnt < size; pageCnt ++)
    {
        for (i = 0; i < (FLASH_PAGE_SIZE/NATIVE_ADRESS_BUS_SIZE); i++)           //32 bit data transfer; FLASH_PAGE_SIZE in bytes; 4 byte per 32 bits
        {
            HAL_FLASH_Unlock();
            
            HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD,
                              (uint32_t)toFlash,
                              (uint64_t)*fromFlash);

            HAL_FLASH_Lock();
            *fromFlash++;
            *toFlash++;
        }
    }
}


/*******************************************************************************
* @brief  This function allows to erase an area of the flash memory.  
*       An area is defined as X number of flash pages
* @inputs 
*       - uint32* startPage : the page adress of the beginning of the section
*       - uint8 blockOffset : 1 block contains X number of Flash pages
*
* @retval None
* @author J-F. Simard
* @date   2016/07/07
*******************************************************************************/
void EraseFlashArea(FlashLayout_t startPage, uint8_t blockOffset)
{
    switch (startPage)
    {
    case FLASH_BASE_SCRATCH_AREA:
        blockOffset = 0;                    //forcing blockOffset @ 0 since the length of the scratch area
                                            //is used to determine the length of a block.
    case FLASH_BASE_ACTIVE_IMAGE:
    case FLASH_BASE_INACTIVE_IMAGE:
        {
            FLASH_EraseInitTypeDef pageInfo;
            uint32_t pageError;
            
            pageInfo.TypeErase = FLASH_TYPEERASE_PAGES;
            pageInfo.PageAddress = startPage + ((uint32_t)blockOffset * FLASH_PAGE_PER_BLOCK * FLASH_PAGE_SIZE);
            pageInfo.NbPages = FLASH_PAGE_PER_BLOCK;

            HAL_FLASH_Unlock();
            
            HAL_FLASHEx_Erase(&pageInfo, &pageError);
            
            HAL_FLASH_Lock();
        }
        break;
        
    case FLASH_BASE_BOOTLOADER:
    case FLASH_BASE_PROGRESS_BAR:
    default :
        break;
    }
}    

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void ResetProgressBar (void)
{
    FLASH_EraseInitTypeDef pageInfo;
    uint32_t pageError;
    
    pageInfo.TypeErase = FLASH_TYPEERASE_PAGES;
    pageInfo.PageAddress = FLASH_BASE_PROGRESS_BAR;
    pageInfo.NbPages = 1;

    HAL_FLASH_Unlock();
    
    HAL_FLASHEx_Erase(&pageInfo, &pageError);
    
    HAL_FLASH_Lock();
}


/*******************************************************************************
* @brief  This function swaps data between FLASH area in 3 steps:
*           1. From Inactive area to Scratch
*           2. From Active area to Inactive Area
*           3. From Scratch to Active
*         This way of doing the swap insure that there is always a copy
*         of the Active and the Inactive area that are valid.
*         To be able to resume the process at any block and steps, the process
*         marks the progress bar after each signicant action.
* @inputs 
*       - blockOffset : the scratch area defines the size of a block, and an image
*                       contains X number of block.  This input value allows to 
*                       resume the swap process at whatever block transfers
*       - swapSteps : because a swap can be interrupted by a power failure or
*                       or any other disruption, this input allows to resume 
*                       the swap process at whatever step.
* @retval None
* @author J-F. Simard
* @date   2016/06/14
*******************************************************************************/
void ImageSwap (uint8_t blockOffset, Swap_steps_t swapSteps)
{
    uint32_t blockOffsetInBytes;
    
    blockOffsetInBytes = blockOffset * FLASH_PAGE_SIZE * FLASH_PAGE_PER_BLOCK;
    switch (swapSteps)
    {
    case SWAP_STEP_INACTIVE_TO_SCRATCH:
        EraseFlashArea(FLASH_BASE_SCRATCH_AREA,0);
        CopyFlashArea((uint32_t *)(FLASH_BASE_INACTIVE_IMAGE + blockOffsetInBytes),
                      (uint32_t *)FLASH_BASE_SCRATCH_AREA,
                      FLASH_PAGE_PER_BLOCK);
        MarkProgress();
        //break;                    //the break statement is voluntarily commented out to allow
                                    // sequential operation AND recover any step in case of uncontrolled reset
    case SWAP_STEP_ACTIVE_TO_INACTIVE:
        EraseFlashArea(FLASH_BASE_INACTIVE_IMAGE, blockOffset);
        CopyFlashArea((uint32_t *)(FLASH_BASE_ACTIVE_IMAGE + blockOffsetInBytes),
                      (uint32_t *)(FLASH_BASE_INACTIVE_IMAGE + blockOffsetInBytes),
                      FLASH_PAGE_PER_BLOCK);
        MarkProgress();
        //break;                    //the break statement is voluntarily commented out to allow
                                    // sequential operation AND recover any step in case of uncontrolled reset
    case SWAP_STEP_SCRATCH_TO_ACTIVE:
        EraseFlashArea(FLASH_BASE_ACTIVE_IMAGE, blockOffset);
        CopyFlashArea((uint32_t *)FLASH_BASE_SCRATCH_AREA,
                      (uint32_t *)(FLASH_BASE_ACTIVE_IMAGE + blockOffsetInBytes),
                      FLASH_PAGE_PER_BLOCK);
        MarkProgress();
        break;
    default:
        break;
    }
}

/*******************************************************************************
* @brief  Update the bootloader's progress bar after a significative bootloader
*           action.
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void MarkProgress(void)
{
    int16_t currentProgress;
    uint16_t * progress;

    currentProgress = GetProgressBar();
    
    progress = (uint16_t *)FLASH_BASE_PROGRESS_BAR + currentProgress + 1;
                                            // +1 to update next bar
    
    HAL_FLASH_Unlock();
    HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD,
                              (uint32_t)progress,
                              0);    
    HAL_FLASH_Lock();


}
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
