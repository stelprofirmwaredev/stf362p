/*******************************************************************************
* @file    FlashOps.h
* @date    2016/07/07
* @authors J-F. Simard
* @brief 
*******************************************************************************/

#ifndef _FlashOps_H
#define _FlashOps_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>
#include "FlashLayout.h"
#include "loaderStates.h"

/*******************************************************************************
* Public constants definitions
*******************************************************************************/



/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void EraseFlashArea(FlashLayout_t startPage, uint8_t blockOffset);
void CopyFlashArea(uint32_t * fromFlash, uint32_t * toFlash, uint8_t size);
void ResetProgressBar (void);
void ImageSwap (uint8_t blockOffset, Swap_steps_t swapSteps);
void MarkProgress(void);

#endif /* _FlashOps_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
