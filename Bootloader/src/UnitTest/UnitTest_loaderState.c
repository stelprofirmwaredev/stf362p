/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    UnitTest_loaderState.c
* @date    
* @authors J-F. Simard
* @brief   
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include "unit_test_configuration.h"

#include "BootLoader_OTA_Control.h"
#include ".\inc\FlashOps.h"
#include "stm32f4xx_hal.h"
#include ".\HAL\inc\HAL_Console.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/

typedef struct
{
    OTAControl_t loaderState_testData;
    int8_t result;
} history_testData_t;


typedef struct
{
    OTAControl_t loaderState_testData;
    loaderStates_t result;
} loaderState_testData_t;

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
#define NUMBER_OF_HISTORY_TESTS         3
const history_testData_t historyTestData[NUMBER_OF_HISTORY_TESTS] =
{
    [0] = 
    {
        .loaderState_testData =
        {
            /****************************************************/
            /*  Test 0 : history [0] only; Should return 0      */
            /****************************************************/
            .loaderStateHistory[0].bootldState_validation = 0xff00,
            .loaderStateHistory[1].bootldState_validation = 0xffff,
            .loaderStateHistory[2].bootldState_validation = 0xffff,
            .loaderStateHistory[3].bootldState_validation = 0xffff,
            .loaderStateHistory[4].bootldState_validation = 0xffff,
            .loaderStateHistory[5].bootldState_validation = 0xffff,
            .loaderStateHistory[6].bootldState_validation = 0xffff,
            .loaderStateHistory[7].bootldState_validation = 0xffff,
            .loaderStateHistory[8].bootldState_validation = 0xffff,
            .loaderStateHistory[9].bootldState_validation = 0xffff,
        },
        .result = 0,
    },
    [1] = 
    {
        .loaderState_testData =
        {
            /****************************************************/
            /*  Test 1 : history [0] and history[9]; 
                      Should return 9                         */
            /****************************************************/
            .loaderStateHistory[0].bootldState_validation = 0xff00,
            .loaderStateHistory[1].bootldState_validation = 0xffff,
            .loaderStateHistory[2].bootldState_validation = 0xffff,
            .loaderStateHistory[3].bootldState_validation = 0xffff,
            .loaderStateHistory[4].bootldState_validation = 0xffff,
            .loaderStateHistory[5].bootldState_validation = 0xffff,
            .loaderStateHistory[6].bootldState_validation = 0xffff,
            .loaderStateHistory[7].bootldState_validation = 0xffff,
            .loaderStateHistory[8].bootldState_validation = 0xffff,
            .loaderStateHistory[9].bootldState_validation = 0xff00,
        },
        .result = 9,
    },
    [2] = 
    {
        .loaderState_testData =
        {
            /****************************************************/
            /*  Test 2 : no history file written; 
                                Should return -1                */
            /****************************************************/
            .loaderStateHistory[0].bootldState_validation = 0xffff,
            .loaderStateHistory[1].bootldState_validation = 0xffff,
            .loaderStateHistory[2].bootldState_validation = 0xffff,
            .loaderStateHistory[3].bootldState_validation = 0xffff,
            .loaderStateHistory[4].bootldState_validation = 0xffff,
            .loaderStateHistory[5].bootldState_validation = 0xffff,
            .loaderStateHistory[6].bootldState_validation = 0xffff,
            .loaderStateHistory[7].bootldState_validation = 0xffff,
            .loaderStateHistory[8].bootldState_validation = 0xffff,
            .loaderStateHistory[9].bootldState_validation = 0xffff,
        },
        .result = -1,
    },
};

#define NUMBER_OF_LOADERSTATE_TESTS         11
const loaderState_testData_t loaderStateTestData[NUMBER_OF_LOADERSTATE_TESTS] =
{
    [0] = 
    {
        .loaderState_testData =
        {
            /****************************************************/
            /*  Test 0 : loaderState 1; no history file
                            Should return LOADER_INVALID_STATE  */
            /****************************************************/
            .bootLoaderState = LOADER_STATE_1,
            .loaderStateHistory[0].bootldState_validation = 0xffff,
            .loaderStateHistory[1].bootldState_validation = 0xffff,
            .loaderStateHistory[2].bootldState_validation = 0xffff,
            .loaderStateHistory[3].bootldState_validation = 0xffff,
            .loaderStateHistory[4].bootldState_validation = 0xffff,
            .loaderStateHistory[5].bootldState_validation = 0xffff,
            .loaderStateHistory[6].bootldState_validation = 0xffff,
            .loaderStateHistory[7].bootldState_validation = 0xffff,
            .loaderStateHistory[8].bootldState_validation = 0xffff,
            .loaderStateHistory[9].bootldState_validation = 0xffff,
        },
        .result = LOADER_INVALID_STATE,
    },
    [1] = 
    {
        .loaderState_testData =
        {
            /****************************************************/
            /*  Test 1 : loaderState 1; bad validation code
                            Should return LOADER_INVALID_STATE  */
            /****************************************************/
            .bootLoaderState = LOADER_STATE_1,
            .loaderStateHistory[0].bootldState_validation = 0xff00,     //the error is injected here.
            .loaderStateHistory[1].bootldState_validation = 0xffff,
            .loaderStateHistory[2].bootldState_validation = 0xffff,
            .loaderStateHistory[3].bootldState_validation = 0xffff,
            .loaderStateHistory[4].bootldState_validation = 0xffff,
            .loaderStateHistory[5].bootldState_validation = 0xffff,
            .loaderStateHistory[6].bootldState_validation = 0xffff,
            .loaderStateHistory[7].bootldState_validation = 0xffff,
            .loaderStateHistory[8].bootldState_validation = 0xffff,
            .loaderStateHistory[9].bootldState_validation = 0xffff,
        },
        .result = LOADER_INVALID_STATE,
    },
    [2] = 
    {
        .loaderState_testData =
        {
            /****************************************************/
            /*  Test 2 : loaderState 1; validation code OK
                            Should return LOADER_STATE_1  */
            /****************************************************/
            .bootLoaderState = LOADER_STATE_1,
            .loaderStateHistory[0].bootldState_validation = 0x00ff,     //correct validation code
            .loaderStateHistory[1].bootldState_validation = 0xffff,
            .loaderStateHistory[2].bootldState_validation = 0xffff,
            .loaderStateHistory[3].bootldState_validation = 0xffff,
            .loaderStateHistory[4].bootldState_validation = 0xffff,
            .loaderStateHistory[5].bootldState_validation = 0xffff,
            .loaderStateHistory[6].bootldState_validation = 0xffff,
            .loaderStateHistory[7].bootldState_validation = 0xffff,
            .loaderStateHistory[8].bootldState_validation = 0xffff,
            .loaderStateHistory[9].bootldState_validation = 0xffff,
        },
        .result = LOADER_STATE_1,
    },
    [3] = 
    {
        .loaderState_testData =
        {
            .bootLoaderState = LOADER_STATE_2,
            .loaderStateHistory[0].bootldState_validation = 0x01fe,     //correct validation code
            .loaderStateHistory[1].bootldState_validation = 0xffff,
            .loaderStateHistory[2].bootldState_validation = 0xffff,
            .loaderStateHistory[3].bootldState_validation = 0xffff,
            .loaderStateHistory[4].bootldState_validation = 0xffff,
            .loaderStateHistory[5].bootldState_validation = 0xffff,
            .loaderStateHistory[6].bootldState_validation = 0xffff,
            .loaderStateHistory[7].bootldState_validation = 0xffff,
            .loaderStateHistory[8].bootldState_validation = 0xffff,
            .loaderStateHistory[9].bootldState_validation = 0xffff,
        },
        .result = LOADER_STATE_2,
    },
    [4] = 
    {
        .loaderState_testData =
        {
            .bootLoaderState = LOADER_STATE_3,
            .loaderStateHistory[0].bootldState_validation = 0x03fc,     //correct validation code
            .loaderStateHistory[1].bootldState_validation = 0xffff,
            .loaderStateHistory[2].bootldState_validation = 0xffff,
            .loaderStateHistory[3].bootldState_validation = 0xffff,
            .loaderStateHistory[4].bootldState_validation = 0xffff,
            .loaderStateHistory[5].bootldState_validation = 0xffff,
            .loaderStateHistory[6].bootldState_validation = 0xffff,
            .loaderStateHistory[7].bootldState_validation = 0xffff,
            .loaderStateHistory[8].bootldState_validation = 0xffff,
            .loaderStateHistory[9].bootldState_validation = 0xffff,
        },
        .result = LOADER_STATE_3,
    },
    [5] = 
    {
        .loaderState_testData =
        {
            .bootLoaderState = LOADER_STATE_4,
            .loaderStateHistory[0].bootldState_validation = 0x07f8,     //correct validation code
            .loaderStateHistory[1].bootldState_validation = 0xffff,
            .loaderStateHistory[2].bootldState_validation = 0xffff,
            .loaderStateHistory[3].bootldState_validation = 0xffff,
            .loaderStateHistory[4].bootldState_validation = 0xffff,
            .loaderStateHistory[5].bootldState_validation = 0xffff,
            .loaderStateHistory[6].bootldState_validation = 0xffff,
            .loaderStateHistory[7].bootldState_validation = 0xffff,
            .loaderStateHistory[8].bootldState_validation = 0xffff,
            .loaderStateHistory[9].bootldState_validation = 0xffff,
        },
        .result = LOADER_STATE_4,
    },
    [6] = 
    {
        .loaderState_testData =
        {
            .bootLoaderState = LOADER_STATE_5,
            .loaderStateHistory[0].bootldState_validation = 0x0ff0,     //correct validation code
            .loaderStateHistory[1].bootldState_validation = 0xffff,
            .loaderStateHistory[2].bootldState_validation = 0xffff,
            .loaderStateHistory[3].bootldState_validation = 0xffff,
            .loaderStateHistory[4].bootldState_validation = 0xffff,
            .loaderStateHistory[5].bootldState_validation = 0xffff,
            .loaderStateHistory[6].bootldState_validation = 0xffff,
            .loaderStateHistory[7].bootldState_validation = 0xffff,
            .loaderStateHistory[8].bootldState_validation = 0xffff,
            .loaderStateHistory[9].bootldState_validation = 0xffff,
        },
        .result = LOADER_STATE_5,
    },
    [7] = 
    {
        .loaderState_testData =
        {
            .bootLoaderState = LOADER_STATE_6,
            .loaderStateHistory[0].bootldState_validation = 0x1fe0,     //correct validation code
            .loaderStateHistory[1].bootldState_validation = 0xffff,
            .loaderStateHistory[2].bootldState_validation = 0xffff,
            .loaderStateHistory[3].bootldState_validation = 0xffff,
            .loaderStateHistory[4].bootldState_validation = 0xffff,
            .loaderStateHistory[5].bootldState_validation = 0xffff,
            .loaderStateHistory[6].bootldState_validation = 0xffff,
            .loaderStateHistory[7].bootldState_validation = 0xffff,
            .loaderStateHistory[8].bootldState_validation = 0xffff,
            .loaderStateHistory[9].bootldState_validation = 0xffff,
        },
        .result = LOADER_STATE_6,
    },
    [8] = 
    {
        .loaderState_testData =
        {
            .bootLoaderState = LOADER_STATE_7,
            .loaderStateHistory[0].bootldState_validation = 0x3fc0,     //correct validation code
            .loaderStateHistory[1].bootldState_validation = 0xffff,
            .loaderStateHistory[2].bootldState_validation = 0xffff,
            .loaderStateHistory[3].bootldState_validation = 0xffff,
            .loaderStateHistory[4].bootldState_validation = 0xffff,
            .loaderStateHistory[5].bootldState_validation = 0xffff,
            .loaderStateHistory[6].bootldState_validation = 0xffff,
            .loaderStateHistory[7].bootldState_validation = 0xffff,
            .loaderStateHistory[8].bootldState_validation = 0xffff,
            .loaderStateHistory[9].bootldState_validation = 0xffff,
        },
        .result = LOADER_STATE_7,
    },
    [9] = 
    {
        .loaderState_testData =
        {
            .bootLoaderState = LOADER_STATE_8,
            .loaderStateHistory[0].bootldState_validation = 0x7f80,     //correct validation code
            .loaderStateHistory[1].bootldState_validation = 0xffff,
            .loaderStateHistory[2].bootldState_validation = 0xffff,
            .loaderStateHistory[3].bootldState_validation = 0xffff,
            .loaderStateHistory[4].bootldState_validation = 0xffff,
            .loaderStateHistory[5].bootldState_validation = 0xffff,
            .loaderStateHistory[6].bootldState_validation = 0xffff,
            .loaderStateHistory[7].bootldState_validation = 0xffff,
            .loaderStateHistory[8].bootldState_validation = 0xffff,
            .loaderStateHistory[9].bootldState_validation = 0xffff,
        },
        .result = LOADER_STATE_8,
    },
    [10] = 
    {
        .loaderState_testData =
        {
            /****************************************************/
            /*  Test 10 : loaderState invalid(0x00); "correct" validation code (according to loader state value)
                            Should return LOADER_INVALID_STATE  */
            /****************************************************/
            .bootLoaderState = LOADER_INVALID_STATE,
            .loaderStateHistory[0].bootldState_validation = 0xff00,     //correct validation code
            .loaderStateHistory[1].bootldState_validation = 0xffff,
            .loaderStateHistory[2].bootldState_validation = 0xffff,
            .loaderStateHistory[3].bootldState_validation = 0xffff,
            .loaderStateHistory[4].bootldState_validation = 0xffff,
            .loaderStateHistory[5].bootldState_validation = 0xffff,
            .loaderStateHistory[6].bootldState_validation = 0xffff,
            .loaderStateHistory[7].bootldState_validation = 0xffff,
            .loaderStateHistory[8].bootldState_validation = 0xffff,
            .loaderStateHistory[9].bootldState_validation = 0xffff,
        },
        .result = LOADER_INVALID_STATE,
    },    
};

    
/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
extern HAL_StatusTypeDef Flash_Program(uint32_t TypeProgram, uint32_t Address, uint64_t Data);


/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void loadTestData(OTAControl_t const* testData)
{
    uint32_t startAddress = FLASH_BASE_OTA_CONTROL;
    uint8_t *source_data = (uint8_t*)testData;
    ResetProgressBar();             //erase all loaderstate data
    
    HAL_FLASH_Unlock();
    for (uint8_t i = 0; i < sizeof(OTAControl_t); i++)
    {
        Flash_Program(FLASH_TYPEPROGRAM_BYTE,
                          startAddress++,
                          (uint64_t)*source_data++);
    }
    HAL_FLASH_Lock();
}


/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void functionTest_FindLastBootloaderHistory(void)
{
    for (uint8_t i=0; i<NUMBER_OF_HISTORY_TESTS; i++)
    {
        int8_t result;
        loadTestData(&historyTestData[i].loaderState_testData);
        result = FindLastBootloaderHistory();
        
        if (result != historyTestData[i].result)
        {
            CONSOLE_LOG_MESSAGE("Error in FindLastBootloaderHistory() unit test #");
            CONSOLE_MESSAGE_APPEND_INTEGER(i);
        }
        else
        {
            CONSOLE_LOG_MESSAGE("FindLastBootloaderHistory() unit test #");
            CONSOLE_MESSAGE_APPEND_INTEGER(i);
            CONSOLE_MESSAGE_APPEND_STRING(" is Success");
        }
    }
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void functionTest_GetBootLoaderState(void)
{
    for (uint8_t i=0; i<NUMBER_OF_LOADERSTATE_TESTS; i++)
    {
        loaderStates_t result;
        loadTestData(&loaderStateTestData[i].loaderState_testData);
        result = GetBootLoaderState();
        
        if (result != loaderStateTestData[i].result)
        {
            CONSOLE_LOG_MESSAGE("Error in GetLoaderState() unit test #");
            CONSOLE_MESSAGE_APPEND_INTEGER(i);
            CONSOLE_MESSAGE_APPEND_STRING("; Returned ");
            CONSOLE_MESSAGE_APPEND_INTEGER(result);
        }
        else
        {
            CONSOLE_LOG_MESSAGE("GetLoaderState() unit tests #");
            CONSOLE_MESSAGE_APPEND_INTEGER(i);
            CONSOLE_MESSAGE_APPEND_STRING(" is Success");
        }
    }
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void test_loaderState(void)
{
#ifdef ENABLE_LOADERSTATEUNITTESTS    
    functionTest_FindLastBootloaderHistory();
    functionTest_GetBootLoaderState();
#endif
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
