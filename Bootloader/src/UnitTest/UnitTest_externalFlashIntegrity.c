/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    UnitTest_externalFlashIntegrity.c
* @date    
* @authors J-F. Simard
* @brief   
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include "unit_test_configuration.h"

#include "FlashOps.h"
#include ".\HAL\inc\HAL_Console.h"
#include "..\..\common\crc\inc\CRC_Utility.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/



/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void test_externalFlashIntegrity()
{
#ifdef ENABLE_EXTERNALFLASHINTEGRITY_TESTS
    crc_error_t flashop_error_status;   
    
    CONSOLE_LOG_MESSAGE("Starting Bank A external flash validation test");
    flashop_error_status = VerifyBlockCRC (LOAD_A_IMAGE);
    CONSOLE_LOG_MESSAGE("Bank A of external flash validation test returned ");
    CONSOLE_MESSAGE_APPEND_STRING(flashop_error_status==0?"Success":"Error");
    
    CONSOLE_LOG_MESSAGE("Starting Bank B external flash validation test");
    flashop_error_status = VerifyBlockCRC (LOAD_B_IMAGE);    
    CONSOLE_LOG_MESSAGE("Bank B of external flash validation test returned ");
    CONSOLE_MESSAGE_APPEND_STRING(flashop_error_status==0?"Success":"Error");
#endif
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
