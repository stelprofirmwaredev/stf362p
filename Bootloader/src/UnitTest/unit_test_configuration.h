/*******************************************************************************
* @file    unit_test_configuration.h
* @date    
* @authors J-F. Simard
* @brief 
*******************************************************************************/

#ifndef unit_test_configuration_H
#define unit_test_configuration_H


/*******************************************************************************
* Includes
*******************************************************************************/



/*******************************************************************************
* Public constants definitions
*******************************************************************************/

//#define ENABLE_FLASHOPSUNITTESTS
//#define ENABLE_LOADERSTATEUNITTESTS
//#define ENABLE_LOADER_NORMAL_SEQUENCE_UNITTESTS
#define ENABLE_LOADER_FALLBACK_SEQUENCE_UNITTESTS
//#define ENABLE_EXTERNALFLASHINTEGRITY_TESTS
//#define ENABLE_SRECFILE_VALIDATION_TESTS
//#define ENABLE_INTERNALFLASH_VALIDATION_TESTS


/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/



#endif /* unit_test_configuration_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
