/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    UnitTest_loaderState.c
* @date    
* @authors J-F. Simard
* @brief   
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include "unit_test_configuration.h"
#include "BootLoader_OTA_Control.h"
#include ".\inc\FlashOps.h"
#include "stm32f4xx_hal.h"
#include ".\HAL\inc\HAL_Console.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void normalLoaderSequenceTest(void)
{
#ifdef ENABLE_LOADER_NORMAL_SEQUENCE_UNITTESTS
    RunState();    
    
    loaderStates_t loaderState = GetBootLoaderState();
    
    switch (loaderState)
    {
    case LOADER_STATE_1:
    case LOADER_STATE_5:
        NewOTAImageReceived();
        break;
    case LOADER_STATE_3:
    case LOADER_STATE_7:
        BootLoader_ClearProvisionalState();
        break;
    default:
        break;
    }

    NVIC_SystemReset();
    
#endif
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void fallbackLoaderSequenceTest(void)
{
#ifdef ENABLE_LOADER_FALLBACK_SEQUENCE_UNITTESTS
    RunState();    
    
    loaderStates_t loaderState = GetBootLoaderState();
    
    switch (loaderState)
    {
    case LOADER_STATE_1:
        UpdateBootLoaderState (LOADER_STATE_7);
        break;
    case LOADER_STATE_5:
        UpdateBootLoaderState (LOADER_STATE_3);
        break;
    default:
        break;
    }

    NVIC_SystemReset();
    
#endif
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/


/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void test_LoaderSequences(void)
{
    normalLoaderSequenceTest();
    fallbackLoaderSequenceTest();
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
