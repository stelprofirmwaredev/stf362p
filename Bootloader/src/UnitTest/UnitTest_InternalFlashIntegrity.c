/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    UnitTest_externalFlashIntegrity.c
* @date    
* @authors J-F. Simard
* @brief   
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include "unit_test_configuration.h"

#include "FlashOps.h"
#include ".\HAL\inc\HAL_Console.h"
#include "..\..\common\crc\inc\CRC_Utility.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/



/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
extern flashop_error_t loadFirmwareImage (uint32_t imageExternalAddress);
extern HAL_StatusTypeDef Flash_Program(uint32_t TypeProgram, uint32_t Address, uint64_t Data);
extern void EraseCodeMemory(void);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void test_InternalFlashValidation()
{
#ifdef ENABLE_INTERNALFLASH_VALIDATION_TESTS
    flashop_error_t flashop_error_status;
    
    CONSOLE_LOG_MESSAGE("Loading test image in internal flash");
    flashop_error_status = loadFirmwareImage (LOAD_A_IMAGE);
    
    flashop_error_status = VerifyFlashIntegrity();
    CONSOLE_LOG_MESSAGE("VerifyFlashIntegrity() returns with ");
    CONSOLE_MESSAGE_APPEND_STRING(flashop_error_status==0?"Success":"Error");
    
    HAL_FLASH_Unlock();
    Flash_Program(FLASH_TYPEPROGRAM_BYTE,
                    0x08010000,
                    0);
    HAL_FLASH_Lock();    
    
    flashop_error_status = VerifyFlashIntegrity();
    CONSOLE_LOG_MESSAGE("After corrupting internal flash VerifyFlashIntegrity() returns with ");
    CONSOLE_MESSAGE_APPEND_STRING(flashop_error_status==0?"Success":"Error");
    
    CONSOLE_LOG_MESSAGE("Erasing code memory");
    EraseCodeMemory();
    
    {
        uint32_t * internalFlash = (uint32_t*)FLASH_BASE_ACTIVE_IMAGE;
        CONSOLE_LOG_MESSAGE("Verifying code memory erase status");
        while (internalFlash <= (uint32_t*)FLASH_BASE_LAST)
        {
            if (((uint32_t)internalFlash%65536) == 0)
            {
                CONSOLE_MESSAGE_APPEND_STRING(".");
            }
            if (*internalFlash++ != 0xffffffff)
            {
                CONSOLE_LOG_MESSAGE("Error erasing code memory");
                break;
            }
        }
        
        if (internalFlash > (uint32_t*)FLASH_BASE_LAST)
        {
            CONSOLE_LOG_MESSAGE("Code memory erased successfully");
        }
    }
    
#endif
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
