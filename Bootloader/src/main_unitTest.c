/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    main.c
* @date    2016-06-13
* @authors J-F. Simard
* @brief   Bootloader application
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include "loaderStates.h"
#include "FlashOps.h"
#include ".\HAL\inc\HAL_Flash.h"
#include ".\HAL\inc\HAL_Console.h"
#include "stm32f4xx_hal.h"
#include "stdbool.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/


/*******************************************************************************
*    Private variables definitions
*******************************************************************************/



/*******************************************************************************
*    Public variables definitions
*******************************************************************************/


/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
#pragma location=BOOTLOADER_VERSION_STRING_POINTER
__root char* const bootloaderVersion = "1.0.0RC1";
extern void test_loaderState(void);
extern void test_externalFlashIntegrity();
extern void test_srecFileValidation();
extern void test_InternalFlashValidation();
extern void test_LoaderSequences(void);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;

  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 180;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
//    Error_Handler();
  }

  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
//    Error_Handler();
  }

  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
//    Error_Handler();
  }

  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_LTDC;
  PeriphClkInitStruct.PLLSAI.PLLSAIN = 50;
  PeriphClkInitStruct.PLLSAI.PLLSAIR = 4;
  PeriphClkInitStruct.PLLSAIDivR = RCC_PLLSAIDIVR_2;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
//    Error_Handler();
  }

  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}



/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/23
*******************************************************************************/
void GetResetCause (void)
{
//    if (__HAL_RCC_GET_FLAG(RCC_FLAG_BORRST) != 0)
//    {
//        CONSOLE_MESSAGE_APPEND_STRING(" Brown Out Reset");
//        CONSOLE_EMPTY_LINE();
//    }
//    if (__HAL_RCC_GET_FLAG(RCC_FLAG_PINRST) != 0)
//    {
//        CONSOLE_MESSAGE_APPEND_STRING(" Pin Reset");
//        CONSOLE_EMPTY_LINE();
//    }
//    if (__HAL_RCC_GET_FLAG(RCC_FLAG_PORRST) != 0)
//    {
//        CONSOLE_MESSAGE_APPEND_STRING(" Power On Reset");
//        CONSOLE_EMPTY_LINE();
//    }
    if (__HAL_RCC_GET_FLAG(RCC_FLAG_SFTRST) != 0)
    {
        CONSOLE_MESSAGE_APPEND_STRING(" Software Reset");
        CONSOLE_EMPTY_LINE();
    }
    if (__HAL_RCC_GET_FLAG(RCC_FLAG_IWDGRST) != 0)
    {
        CONSOLE_MESSAGE_APPEND_STRING(" Independant Watch Dog Reset");
        CONSOLE_EMPTY_LINE();
    }
    if (__HAL_RCC_GET_FLAG(RCC_FLAG_WWDGRST) != 0)
    {
        CONSOLE_MESSAGE_APPEND_STRING(" Window Watch Dog Reset");
        CONSOLE_EMPTY_LINE();
    }
//    if (__HAL_RCC_GET_FLAG(RCC_FLAG_LPWRRST) != 0)
//    {
//        CONSOLE_MESSAGE_APPEND_STRING(" Low Power Reset");
//        CONSOLE_EMPTY_LINE();
//    }


    __HAL_RCC_CLEAR_RESET_FLAGS();
}


/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
void main (void)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    HAL_Init();

    SystemClock_Config();
    HAL_FlashInit();
    InitConsole();

    __HAL_RCC_GPIOA_CLK_ENABLE();
    HAL_GPIO_WritePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin, GPIO_PIN_RESET);
    GPIO_InitStruct.Pin = LED_GREEN_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(LED_GREEN_GPIO_Port, &GPIO_InitStruct);

    FlashOpsUnitTests();    
    test_loaderState();
    test_externalFlashIntegrity();
    test_srecFileValidation();
    test_InternalFlashValidation();
    test_LoaderSequences();
}





/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/



