/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    SREC_Utility.c
* @date    2017/08/07
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>
#include "SREC_Utility.h"
#include ".\inc\FlashOps.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define BYTES_PER_HEX_VALUE 2


/*******************************************************************************
*    Private variables definitions
*******************************************************************************/



/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
uint8_t GetAddressLenght(uint8_t type)
{
    switch (type)
    {
    case 2:
    case 8:
        return 6;

    case 3:
    case 7:
        return 8;

    case 1:
    case 9:
    default:
        return 4;
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
uint8_t GetDataLenght(srec_bin_t * srecord)
{
    if (NULL != srecord)
    {
        uint16_t size = srecord->record_size - ((GetAddressLenght(srecord->type)/2) + 1);   //removing address lenght and size of checksum
        if (size > SREC_MAX_DATA_LENGTH)
        {
            size = SREC_MAX_DATA_LENGTH;
        }
        return size;
    }
    else
    {
        return 0;
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
uint8_t boot_asciiLookup(char asciiChar)
{
    const char lookup [16] = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};

    for (uint8_t i = 0; i < sizeof(lookup); i++)
    {
        if (lookup[i] == tolower(asciiChar))
        {
            return i;
        }
    }
    return 0;
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
uint32_t PPBufAsciiToHex(uint16_t peekOffset, uint8_t size)
{
    uint32_t hexValue = 0;
    char asciiChar = '\0';

    for (uint8_t sizeCnt = 0; sizeCnt < size; sizeCnt++)
    {
        hexValue <<= 4;
        asciiChar = PingPongBufferPeek(peekOffset+sizeCnt);
        hexValue |= boot_asciiLookup(asciiChar);
    }

    return (hexValue);
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static uint32_t asciiToHex(char * asciiStr, uint8_t size)
{
    uint32_t hexValue = 0;
    if (NULL !=  asciiStr)
    {
        for (uint8_t sizeCnt = 0; sizeCnt < size; sizeCnt++)
        {
            hexValue <<= 4;

            hexValue |= boot_asciiLookup(*asciiStr);
            asciiStr++;
        }
    }
    return (hexValue);
}



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/

/*******************************************************************************
* @brief  Determines if the SRecord at the current buffer is complete & valid.
* @inputs
*           - uint8_t* srecPtr: Pointer to the first character of the srec
*           - uint16_t bytesLeft: The number of bytes the srecord has to fit into
* @retval :
*           - 0 = parsing successfull
*           - 1 = incomplete parsing due to a shortage in the data
*           - 2 = Bad start of record (!= 'S')
* @author J-F. Simard & Alexandre Courtemanche
* @date   2017/08/03
*******************************************************************************/
srecParsing_Err_t IsSrecordComplete(uint8_t* srecPtr, uint32_t bytesLeft)
{
    srecParsing_Err_t status = SREC_OK;
    uint8_t record_size;
    char firstChar = (char) *srecPtr;

    if(firstChar == 'S')
    {
        if ( bytesLeft < SREC_TYPE_SIZE+SREC_COUNT_SIZE )
        {
            status = SREC_SHORT_BUFFER;
        }
        else
        {
            record_size = asciiToHex((char*)(srecPtr+2), 2);
            if (bytesLeft < (SREC_HEADERFOOTER_SIZE + (record_size*2)))
            {
                status = SREC_SHORT_BUFFER;
            }
        }
    }
    else
    {
        status = SREC_BAD_HEADER;
    }

    return status;
}


uint16_t ParseSrecord(uint8_t* srecPtr, srec_bin_t * srecord)
{
    uint16_t bytesParsed = 0;
    uint8_t dataLength = 0;
    uint8_t addressLength = 0;
    uint8_t* headOfSrecPtr = srecPtr;

    srecPtr++;
    srecord->type = asciiToHex((char*)srecPtr, 1);
    srecPtr++;
    srecord->record_size = asciiToHex((char*)srecPtr, SREC_COUNT_SIZE);
    srecPtr += (SREC_COUNT_SIZE);

    addressLength = GetAddressLenght(srecord->type);
    srecord->address_of_data = asciiToHex((char*)srecPtr, addressLength);
    srecPtr += addressLength;

    dataLength = GetDataLenght(srecord);
    for (uint8_t i = 0; i < dataLength; i++)
    {
        srecord->data[i] = asciiToHex((char*)srecPtr, 2);
        srecPtr += 2;
    }

    srecord->checksum = asciiToHex((char*)srecPtr, SREC_CHECKSUM_SIZE);
    srecPtr+=(SREC_CHECKSUM_SIZE + SREC_FOOTER_SIZE);

    bytesParsed = srecPtr - headOfSrecPtr;

    return bytesParsed;
}



/*******************************************************************************
* @brief  Determines if the SRecord at the current buffer is complete & valid.
* @inputs
*           - bool freshBlock: Indicates that there is currently a fresh buffer
* @outputs srec_bin_t * srecord The address at which to store the SRecord parsed
* @retval :
*           - 0 = parsing successfull
*           - 1 = incomplete parsing due to a shortage in the data
*           - 2 = Bad start of record (!= 'S')
* @author J-F. Simard & Alexandre Courtemanche
* @date   2017/08/03
*******************************************************************************/
srecParsing_Err_t PPBufIsNextSRecordComplete(bool *freshBlock)
{
    srecParsing_Err_t status = SREC_OK;
    uint32_t bytesLeft = GetBytesLeftInCurrentBuffer();

    if (bytesLeft < SREC_HEADERFOOTER_SIZE+SREC_MAX_DATA_LENGTH+SREC_CHECKSUM_SIZE)
    {
        if (!*freshBlock)
        {
            status = SREC_SHORT_BUFFER;
        }
    }
    else
    {
        *freshBlock = 0;
    }


    return status;

}



/*******************************************************************************
* @brief  Parses the SRecord and increments the buffer pointer accordingly
* @inputs None
* @outputs srec_bin_t * srecord The address at which to store the SRecord parsed
* @retval :
*           - 0 = parsing successfull
*           - 1 = incomplete parsing due to a shortage in the data
*           - 2 = Bad start of record (!= 'S')
* @author J-F. Simard & Alexandre Courtemanche
* @date   2017/08/03
*******************************************************************************/
srecParsing_Err_t PPBufParseNextSrecord(srec_bin_t * srecord)
{
    srecParsing_Err_t status = SREC_OK;
    uint8_t dataLength = 0;
    uint8_t addressLength = 0;

    PingPongBufferIncrement(1);
    srecord->type = PPBufAsciiToHex(0, 1);
    PingPongBufferIncrement(1);
    srecord->record_size = PPBufAsciiToHex(0, SREC_COUNT_SIZE);
    PingPongBufferIncrement(SREC_COUNT_SIZE);

    addressLength = GetAddressLenght(srecord->type);
    srecord->address_of_data = PPBufAsciiToHex(0, addressLength);
    PingPongBufferIncrement(addressLength);

    dataLength = GetDataLenght(srecord);
    for (uint8_t i = 0; i < dataLength; i++)
    {
        srecord->data[i] = PPBufAsciiToHex(0, 2);
        PingPongBufferIncrement(2);
    }

    srecord->checksum = PPBufAsciiToHex(0, SREC_CHECKSUM_SIZE);
    PingPongBufferIncrement(SREC_CHECKSUM_SIZE + SREC_FOOTER_SIZE);

    return status;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
uint8_t ComputeRecordChecksum(srec_bin_t * srecord)
{
    uint8_t checksum = srecord->record_size;
    uint8_t *pData = (uint8_t *)&srecord->address_of_data;
    uint8_t addressLength = GetAddressLenght(srecord->type)/2;
    uint8_t dataBufLength = srecord->record_size - addressLength - 1;

    //TODO : v�rifier que le pointeur srecord != NULL
    for (uint8_t i = 0; i < addressLength; i++)
    {
        checksum += *pData;
        pData++;
    }

    pData = srecord->data;
    if (dataBufLength > SREC_MAX_DATA_LENGTH)
    {
        dataBufLength = SREC_MAX_DATA_LENGTH;
    }
    for (uint8_t i = 0; i < dataBufLength; i++)
    {
        checksum += *pData;
        pData++;
    }

    return ~checksum;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void WriteSrecordToFlash(srec_bin_t * srecord)
{
    if ((NULL != srecord) && (srecord->address_of_data >= FLASH_BASE_ACTIVE_IMAGE) && (srecord->address_of_data <= FLASH_BASE_LAST))
    {
        WriteStreamToFlash(srecord->data, GetDataLenght(srecord), srecord->address_of_data);
    }
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
