/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    APP_loaderstates.c
* @date    2016/06/14
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include "loaderStates.h"
#include "bootDisplay.h"
#include "FlashLayout.h"
#include "BootLoader_OTA_Control.h"
#include "FlashOps.h"
#include "BootLoaderWdt.h"
#include ".\HAL\inc\HAL_Console.h"
#include "..\..\common\crc\inc\CRC_Utility.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#ifndef BOOTLOADER_DEFAULT_STATE
    #define BOOTLOADER_DEFAULT_STATE        LOADER_STATE_1
#endif


/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
__root const OTAControl_t flashLoaderState @ FLASH_BASE_OTA_CONTROL = 
{
    .bootLoaderState = BOOTLOADER_DEFAULT_STATE,
    .loaderStateHistory[0].bootldState_validation0 = BOOTLOADER_DEFAULT_STATE,
    .loaderStateHistory[0].bootldState_validation1 = BOOTLOADER_DEFAULT_STATE^0xff,
    .loaderStateHistory[1].bootldState_validation = 0xffff,
    .loaderStateHistory[2].bootldState_validation = 0xffff,
    .loaderStateHistory[3].bootldState_validation = 0xffff,
    .loaderStateHistory[4].bootldState_validation = 0xffff,
    .loaderStateHistory[5].bootldState_validation = 0xffff,
    .loaderStateHistory[6].bootldState_validation = 0xffff,
    .loaderStateHistory[7].bootldState_validation = 0xffff,
    .loaderStateHistory[8].bootldState_validation = 0xffff,
    .loaderStateHistory[9].bootldState_validation = 0xffff,
    .ProvisionalCounter[0] = 0xff,
    .ProvisionalCounter[1] = 0xff,
    .ProvisionalCounter[2] = 0xff,
    .ProvisionalCounter[3] = 0xff,
    .ProvisionalCounter[4] = 0xff,
};
    


/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
void BootApp (void);
void LoaderState_Running (void);
void LoaderState_Provisional (void);
extern void DisplayInit(void);


/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
typedef void (application_t) (void);

typedef struct vector
{
    uint32_t        stack_addr;     // intvec[0] is initial Stack Pointer
    application_t   *func_p;        // intvec[1] is initial Program Counter
} vector_t;

extern const uint32_t app_vector;   // Application vector address symbol from
                                    // the linker configuration file

/*******************************************************************************
* @brief  __stackless BootApp()
*
*       __stackless keyword is an IAR keyword that informs the compiler that the
*       function will not return.  It must be use here to prevent unintended use
*       of the stack pointer (SP) after it has been initialized for the ACTIVE
*       image. (e.g. SP = 0x200016F0. without __stackless keyword, the core of
*       function would "POP" the link register (LR) and then the SP would equal
*       0x200016F8, which is out of the compiled boundaries)
*
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/06/14
*******************************************************************************/
__stackless void BootApp (void)
{
#ifdef DISABLE_BOOT_APP
#else
    const vector_t *vector_p            = (vector_t*) &app_vector;
    DisplayDeInit();
    InitBootWdt();
    __disable_interrupt();
    __set_SP(vector_p->stack_addr);
    SCB->VTOR = (uint32_t) &app_vector;
    vector_p->func_p();
#endif
}

/*******************************************************************************
* @brief  No update in progress or to be.  Boot the application
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/06/14
*******************************************************************************/
void LoaderState_Running (void)
{
    BootApp();
}

/*******************************************************************************
* @brief  The bootloader has detetected a new image to be activated
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/06/14
*******************************************************************************/
flashop_error_t loadFirmwareImage (uint32_t imageExternalAddress)
{
    flashop_error_t flashop_error_status;
    
    flashop_error_status = InstallFirmware (imageExternalAddress, SMT32RECORD_MAX_SIZE, VALIDATE_FIRMWARE_ONLY);
    if (FLASHOP_NOERROR == flashop_error_status) //validate only
    {
        return (InstallFirmware (imageExternalAddress, SMT32RECORD_MAX_SIZE, VALIDATE_AND_INSTALL_FIRMWARE));
    }
    else
    {
        return flashop_error_status;
    }
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/06/14
*******************************************************************************/
void LoaderState_Provisional (void)
{
    CountProvisionnalTries();
    BootApp();
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
flashop_error_t UpdateInternalFlash(char *image_name, uint32_t image_address)
{
    DisplayInit();
    DrawProgressBar(0,1);

    CONSOLE_RUNTIME_MESSAGE("Verifying CRC of image ");
    CONSOLE_MESSAGE_APPEND_STRING(image_name);
    if (CRC_NOERROR == VerifyBlockCRC(image_address))
    {
        DrawProgressBar(25,1);

        CONSOLE_RUNTIME_MESSAGE("Loading firmware ");
        CONSOLE_MESSAGE_APPEND_STRING(image_name);
        return (loadFirmwareImage(image_address));
    }
    else
    {
        CONSOLE_RUNTIME_MESSAGE("Firmware ");
        CONSOLE_MESSAGE_APPEND_STRING(image_name);
        CONSOLE_MESSAGE_APPEND_STRING(" in external flash is corrupted.");
        return FLASHOP_BLOCKCRC_ERROR;
    }
}



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/06/14
*******************************************************************************/
void RunState (void)
{
    loaderStates_t state = GetBootLoaderState();
    char image_name[4] = "\" \"\0";
    uint32_t image_address = LOAD_B_IMAGE;

    
    
    /************************************************/
    /*  establish names and images based on states  */
    switch (state)
    {
    case LOADER_STATE_1:
    case LOADER_STATE_4:
    case LOADER_STATE_6:
    case LOADER_STATE_7:
        image_name[1] = 'A';
        image_address = LOAD_A_IMAGE;
        break;

    case LOADER_STATE_2:
    case LOADER_STATE_3:
    case LOADER_STATE_5:
    case LOADER_STATE_8:
        image_name[1] = 'B';
        image_address = LOAD_B_IMAGE;
        break;
        
    default:
        CONSOLE_RUNTIME_MESSAGE("Loader State is corrupted; ");
        CONSOLE_RUNTIME_MESSAGE("Resetting loader state to factory default");

        /*******************************************************
        *   At this point, the firmware loaded in the internal
        *   mcu flash is assumed to be non valid since there is no way
        *   (as per design) to know which image is loaded in the internal
        *   flash.  
        *   The bootloader state is resetted to LOADER_STATE_1 with a
        *   corruption of the internal flash memory, meaning :
        *   1. The state working "A";
        *   2. The bootloader will detect an internal memory corruption
        *       and force a reflash of firmware "A"
        *   3. The bootloader will reflash with the firmware from bank "A"
        *       if the integrity check is OK
        *   4. The next OTA will be stored in section "B" of 
        *       the external flash
        */
        LoaderStateCorrupted_ResetToFactory();
        break;
    }
        
    /************************************/
    /*  do actions based on states      */
    switch (state)
    {
    case LOADER_STATE_1:        //Image A = Active; Image B = Inactive
    case LOADER_STATE_5:        //Image A = Inactive; Image B = Active
        {
            if (FLASHOP_NOERROR == VerifyFlashIntegrity())
            {
                CONSOLE_RUNTIME_MESSAGE("Starting Application with image ");
                CONSOLE_MESSAGE_APPEND_STRING(image_name);
                LoaderState_Running();
            }
            else
            {
                CONSOLE_RUNTIME_MESSAGE("Reflashing with image ");
                CONSOLE_MESSAGE_APPEND_STRING(image_name);
                /********************************************************
                *   the internal mcu flash is corrupted, the bootloader
                *   will try to reflash with the same image as the loader
                *   state indicate.  
                *   In case of a failure, the bootloader will "quick switch"
                *   to the other image and will try to reflash with it.
                *   In case the external flash has neither "A" or "B" valid
                *   image, the bootloader will constantly try to "quick" switch. 
                */
                if (FLASHOP_NOERROR != UpdateInternalFlash(image_name, image_address))
                {
                    ImageQuickSwitch();
                }
                NVIC_SystemReset();
            }
        }
        break;

    case LOADER_STATE_6:        //Image A = New; Image B = Active
    case LOADER_STATE_2:        //Image A = Active; Image B = New
        {
            flashop_error_t updatedInternalFlash_errorStatus;
            
            updatedInternalFlash_errorStatus = UpdateInternalFlash(image_name, image_address);
            if (FLASHOP_NOERROR == updatedInternalFlash_errorStatus)
            {
                NewImageLoaded();
            }
            else
            {
                /**********************************************************
                * an error occurs during the "UpdateInternalFlash()" sequence;
                *   The error can be :
                *       - FLASHOP_BLOCKCRC_ERROR : the external flash image is not usable
                *       - FLASHOP_SRECORD_RO_CHECKSUM_ERROR : at least one SREC of the
                *                   external flash image is not usable
                *       - FLASHOP_SRECORD_W_CHECKSUM_ERROR :at least one SREC of the
                *                   external flash image is not usable AND the internal
                *                   flash memory has started to be reflash.
                *                   At this point the internal flash has a partial
                *                   firmware (in theory, this should never happen)
                *       - FLASHOP_INTERNALFLASH_CRC_ERROR : the firmware has been
                *               flash correctly, but the CRC of the internal flash
                *               is not good.
                */
                switch (updatedInternalFlash_errorStatus)
                {
                case FLASHOP_BLOCKCRC_ERROR:
                case FLASHOP_SRECORD_RO_CHECKSUM_ERROR:
                case FLASHOP_SRECORD_W_CHECKSUM_ERROR:
                case FLASHOP_INTERNALFLASH_CRC_ERROR:
                default:              
                    NewImageCorrupted_StayingInCurrentState();
                    break;
                }
            }
            NVIC_SystemReset();
        }
        break;

    case LOADER_STATE_7:        //Image A = Provisional; Image B = Inactive
    case LOADER_STATE_3:        //Image A = Inactive; Image B = Provisional
        {
            if (FLASHOP_NOERROR == VerifyFlashIntegrity())
            {
                CONSOLE_RUNTIME_MESSAGE("Starting with provisional firmware ");
                CONSOLE_MESSAGE_APPEND_STRING(image_name);
                LoaderState_Provisional();
            }
            else
            {
                CountProvisionnalTries();
                UpdateInternalFlash(image_name, image_address);
            }
        }
        break;

    case LOADER_STATE_4:        //Image A = Inactive; Image B = Fallback
    case LOADER_STATE_8:        //Image A = Fallback; Image B = Inactive
        {
            flashop_error_t updatedInternalFlash_errorStatus;
            
            CONSOLE_RUNTIME_MESSAGE("Fallback; Reloading image ");
            CONSOLE_MESSAGE_APPEND_STRING(image_name);
            
            updatedInternalFlash_errorStatus = UpdateInternalFlash(image_name, image_address);
            if (FLASHOP_NOERROR == updatedInternalFlash_errorStatus)
            {
                FallBackCompleted();
            }
            else
            {
                /**********************************************************
                * an error occurs during the "UpdateInternalFlash()" sequence;
                *   The error can be :
                *       - FLASHOP_BLOCKCRC_ERROR : the external flash image is not usable
                *       - FLASHOP_SRECORD_RO_CHECKSUM_ERROR : at least one SREC of the
                *                   external flash image is not usable
                *       - FLASHOP_SRECORD_W_CHECKSUM_ERROR :at least one SREC of the
                *                   external flash image is not usable AND the internal
                *                   flash memory has started to be reflash.
                *                   At this point the internal flash has a partial
                *                   firmware (in theory, this should never happen)
                *       - FLASHOP_INTERNALFLASH_CRC_ERROR : the firmware has been
                *               flash correctly, but the CRC of the internal flash
                *               is not good.
                */
                switch (updatedInternalFlash_errorStatus)
                {
                case FLASHOP_BLOCKCRC_ERROR:
                case FLASHOP_SRECORD_RO_CHECKSUM_ERROR:
                case FLASHOP_SRECORD_W_CHECKSUM_ERROR:
                case FLASHOP_INTERNALFLASH_CRC_ERROR:
                default:                
                    FallbackImageCorrupted_StayingInCurrentState();
                }
            }
            NVIC_SystemReset();
        }
        break;

    default:
        break;
    }
}



/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
