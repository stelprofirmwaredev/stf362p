/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    BootLoader_ProgressBar.c
* @date    2016/06/15
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include "BootLoader_OTA_Control.h"
#include "FlashOps.h"

#ifdef STM32F429xx
    #include "stm32f4xx_hal.h"
#elif defined STM32F303xC
    #include "stm32f3xx_hal_conf.h"
#else
    #error("Please choose a valid uc model")
#endif

__stackless void BootApp (void);

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/07/26
*******************************************************************************/
void ForceFallBack (void)
{
    loaderStates_t flashLoaderState = GetBootLoaderState();

    switch (flashLoaderState)
    {
    case LOADER_STATE_6:
    case LOADER_STATE_3:
        UpdateBootLoaderState(LOADER_STATE_4);
        break;

    case LOADER_STATE_1:
    case LOADER_STATE_7:
        UpdateBootLoaderState(LOADER_STATE_8);
        break;

    default:
        break;
    }

    NVIC_SystemReset();
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/11
*******************************************************************************/
void CountProvisionnalTries(void)
{
    OTAControl_t * flashLoaderState = (OTAControl_t*)FLASH_BASE_OTA_CONTROL;
    for (uint8_t i = 0; i < PROVISIONAL_RESET_CNT_BEFORE_FALLBACK; i++)
    {
        if (flashLoaderState->ProvisionalCounter[i] == 0xFF)
        {
            MarkProvisionnalCounter(i);
            return;
        }
    }
    ForceFallBack ();
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/11
*******************************************************************************/
void FallBackCompleted(void)
{
    loaderStates_t flashLoaderState = GetBootLoaderState();

    UpdateBootLoaderState ((flashLoaderState == LOADER_STATE_4)?LOADER_STATE_1:LOADER_STATE_5);
}


/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void ImageQuickSwitch(void)
{
    loaderStates_t flashLoaderState = GetBootLoaderState();
    
    if (flashLoaderState == LOADER_STATE_1)
    {
        UpdateBootLoaderState (LOADER_STATE_5);
    }
    else if (flashLoaderState == LOADER_STATE_5)
    {
        UpdateBootLoaderState (LOADER_STATE_1);
    }
    else
    {
        ;           //current state not allowed
    }
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/10
*******************************************************************************/
void BootLoader_ClearProvisionalState(void)
{
    loaderStates_t flashLoaderState = GetBootLoaderState();

    UpdateBootLoaderState ((flashLoaderState == LOADER_STATE_3)?LOADER_STATE_5:LOADER_STATE_1);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/10
*******************************************************************************/
void NewOTAImageReceived (void)
{
    loaderStates_t flashLoaderState = GetBootLoaderState();

    UpdateBootLoaderState ((flashLoaderState == LOADER_STATE_1)?LOADER_STATE_2:LOADER_STATE_6);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/11
*******************************************************************************/
void NewImageLoaded (void)
{
    loaderStates_t flashLoaderState = GetBootLoaderState();

    UpdateBootLoaderState ((flashLoaderState == LOADER_STATE_2)?LOADER_STATE_3:LOADER_STATE_7);
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/11
*******************************************************************************/
void FallbackImageCorrupted_StayingInCurrentState (void)
{
    loaderStates_t flashLoaderState = GetBootLoaderState();

    UpdateBootLoaderState ((flashLoaderState == LOADER_STATE_4)?LOADER_STATE_5:LOADER_STATE_1);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/11
*******************************************************************************/
void NewImageCorrupted_StayingInCurrentState (void)
{
    loaderStates_t flashLoaderState = GetBootLoaderState();

    UpdateBootLoaderState ((flashLoaderState == LOADER_STATE_2)?LOADER_STATE_1:LOADER_STATE_5);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2018/02/01
*******************************************************************************/
void LoaderStateCorrupted_ResetToFactory (void)
{
    char** bootloader_version = (char**)BOOTLOADER_VERSION_STRING_POINTER;
    
    
    /*************************************************************************/
    /* to ensure retro compatibility with older version (0.4.1 and below)    */
    /* we must validate the version of the bootloader.  For this, if the     */
    /* version string pointer BOOTLOADER_VERSION_STRING_POINTER = 0xffffffff,*/
    /* this means we have an old version of the bootloader and no internal   */
    /* integrity check is implemented.  For these version, the state 6 must  */
    /* must be forced in order to reflash with firmware "A".                 */
    /* Else, the internal flash is corrupted and loader state is force to    */
    /* state 1.                                                              */
    /*************************************************************************/
    if (*bootloader_version == (char*)0xFFFFFFFF)
    {    
        UpdateBootLoaderState (LOADER_STATE_6);
    }
    else
    {
        UpdateBootLoaderState (LOADER_STATE_1);
        ForceInternalFlashCorruption();
    }
    NVIC_SystemReset();
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/01/10
*******************************************************************************/
loaderStates_t GetBootLoaderState (void)
{
    int8_t i;
    OTAControl_t* otaControl = (OTAControl_t*)FLASH_BASE_OTA_CONTROL;
    loaderStates_t returned_state = LOADER_INVALID_STATE;
    char** bootloader_version = (char**)BOOTLOADER_VERSION_STRING_POINTER;
    
    
    /*************************************************************************/
    /* to ensure retro compatibility with older version (0.4.1 and below)    */
    /* we must validate the version of the bootloader.  For this, if the     */
    /* version string pointer BOOTLOADER_VERSION_STRING_POINTER = 0xffffffff,*/
    /* this means we have an old version of the bootloader and no loaderstate*/
    /* integrity is implemented, thus must not be validated                  */
    /*************************************************************************/
    if (*bootloader_version != (char*)0xFFFFFFFF)
    {
        i = FindLastBootloaderHistory();
        
        if (i >= 0)
        {
            if (otaControl->bootLoaderState == 
                otaControl->loaderStateHistory[i].bootldState_validation0)
            {
                if ((otaControl->bootLoaderState^0xff) == 
                    otaControl->loaderStateHistory[i].bootldState_validation1)
                {
                    returned_state = otaControl->bootLoaderState;
                }
            }
        }
    }
    else
    {
        switch (otaControl->bootLoaderState)
        {
        case LOADER_STATE_1:
        case LOADER_STATE_2:
        case LOADER_STATE_3:
        case LOADER_STATE_4:
        case LOADER_STATE_5:
        case LOADER_STATE_6:
        case LOADER_STATE_7:
        case LOADER_STATE_8:
            returned_state = otaControl->bootLoaderState;
            break;
            
        default:
            break;
        }
    }
    
    return returned_state;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
uint8_t FindLastBootloaderHistory(void)
{
    OTAControl_t* otaControl = (OTAControl_t*)FLASH_BASE_OTA_CONTROL;
    for (int8_t i = (SIZEOF_VALIDATION_HISTORY-1); i >= 0; i--)
    {
        if (otaControl->loaderStateHistory[i].bootldState_validation != 
            VALIDATION_HISTORY_RESET_VALUE)
        {
            return i;
        }
    }
    
    return -1;
}



/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
