#include "stm32f4xx_hal.h"
#include ".\BSP\TFT\OSD035T2631-65TS\inc\TFT_ST7796.h"
#include "bootDisplay.h"

#define PROGRESS_BAR_WIDTH      240     //value in pixels
#define PROGRESS_BAR_HEIGHT     35      //value in pixels
#define PROGRESS_BAR1_X_START    40      //value in pixels
#define PROGRESS_BAR1_Y_START    200      //value in pixels
#define PROGRESS_BAR2_X_START    40      //value in pixels
#define PROGRESS_BAR2_Y_START    240      //value in pixels
#define PROGRESS_BAR_FILL_COLOR     0xf00f      //Alpha=16;R=16;G=0;B=0 (note R and B are reversed because of TFT configuration)
#define PROGRESS_BAR_CLEAR_COLOR     0xf888      //Alpha=16;R=8;G=8;B=8 (note R and B are reversed because of TFT configuration)
#define PROGRESS_BAR_NO_COLOR     0x0000      //Alpha=0;R=0;G=0;B=0 (note R and B are reversed because of TFT configuration)

SPI_HandleTypeDef hTFT_SPI;
LTDC_HandleTypeDef hTFT_LTDC;


__no_init uint16_t progress_bar1_image[PROGRESS_BAR_WIDTH*PROGRESS_BAR_HEIGHT + 1];
__no_init uint16_t progress_bar2_image[PROGRESS_BAR_WIDTH*PROGRESS_BAR_HEIGHT + 1];

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/09
*******************************************************************************/
static void GPIO_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    /*  Write default output state  */
    HAL_GPIO_WritePin(TFT_SPI_SLAVE_SELECT_GPIO_Port,
                      TFT_SPI_SLAVE_SELECT_Pin,
                      GPIO_PIN_RESET);

    HAL_GPIO_WritePin(TFT_RESET_GPIO_Port,
                      TFT_RESET_Pin,
                      GPIO_PIN_RESET);

    HAL_GPIO_WritePin(TFT_BACKLIGHT_GPIO_Port,
                      TFT_BACKLIGHT_Pin,
                      GPIO_PIN_RESET);

    HAL_GPIO_WritePin(TFT_DCX_GPIO_Port,
                      TFT_DCX_Pin,
                      GPIO_PIN_SET);


    /*Configure GPIO pins */
    GPIO_InitStruct.Pin = TFT_SPI_SLAVE_SELECT_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(TFT_SPI_SLAVE_SELECT_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = TFT_RESET_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(TFT_RESET_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = TFT_BACKLIGHT_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(TFT_BACKLIGHT_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = TFT_DCX_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(TFT_DCX_GPIO_Port, &GPIO_InitStruct);
}



/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/08
*******************************************************************************/
static void SPI_Init(void)
{
__HAL_RCC_SPI2_CLK_ENABLE();
  hTFT_SPI.Instance = SPI2;
  hTFT_SPI.Init.Mode = SPI_MODE_MASTER;
  hTFT_SPI.Init.Direction = SPI_DIRECTION_2LINES;
  hTFT_SPI.Init.DataSize = SPI_DATASIZE_8BIT;
  hTFT_SPI.Init.CLKPolarity = SPI_POLARITY_LOW;
  hTFT_SPI.Init.CLKPhase = SPI_PHASE_1EDGE;
  hTFT_SPI.Init.NSS = SPI_NSS_SOFT;
  hTFT_SPI.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hTFT_SPI.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hTFT_SPI.Init.TIMode = SPI_TIMODE_DISABLE;
  hTFT_SPI.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hTFT_SPI.Init.CRCPolynomial = 10;
  HAL_SPI_Init(&hTFT_SPI);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/08
*******************************************************************************/
static void LTDC_Init(void)
{
  LTDC_LayerCfgTypeDef pLayerCfg;
  LTDC_LayerCfgTypeDef pLayerCfg1;

  hTFT_LTDC.Instance = LTDC;
  hTFT_LTDC.Init.HSPolarity = LTDC_HSPOLARITY_AL;
  hTFT_LTDC.Init.VSPolarity = LTDC_VSPOLARITY_AL;
  hTFT_LTDC.Init.DEPolarity = LTDC_DEPOLARITY_AL;
  hTFT_LTDC.Init.PCPolarity = LTDC_PCPOLARITY_IIPC;

  hTFT_LTDC.Init.HorizontalSync = 49;
  hTFT_LTDC.Init.AccumulatedHBP = 51;
  hTFT_LTDC.Init.AccumulatedActiveW = 371;
  hTFT_LTDC.Init.TotalWidth = 409;

  hTFT_LTDC.Init.VerticalSync = 3;
  hTFT_LTDC.Init.AccumulatedVBP = 5;
  hTFT_LTDC.Init.AccumulatedActiveH = 485;
  hTFT_LTDC.Init.TotalHeigh = 487;

  //Set the RGB color the same as the background
  hTFT_LTDC.Init.Backcolor.Blue = 0;
  hTFT_LTDC.Init.Backcolor.Green = 0;
  hTFT_LTDC.Init.Backcolor.Red = 0;
  HAL_LTDC_Init(&hTFT_LTDC);
//  HAL_LTDC_EnableDither(&hTFT_LTDC);

  pLayerCfg.WindowX0 = PROGRESS_BAR1_X_START;
  pLayerCfg.WindowX1 = PROGRESS_BAR1_X_START+PROGRESS_BAR_WIDTH;
  pLayerCfg.WindowY0 = PROGRESS_BAR1_Y_START;
  pLayerCfg.WindowY1 = PROGRESS_BAR1_Y_START+PROGRESS_BAR_HEIGHT;
  pLayerCfg.Alpha = 255;
  pLayerCfg.PixelFormat = LTDC_PIXEL_FORMAT_ARGB4444;

  pLayerCfg.BlendingFactor1 = LTDC_BLENDING_FACTOR1_PAxCA;
  pLayerCfg.BlendingFactor2 = LTDC_BLENDING_FACTOR2_PAxCA;
  pLayerCfg.FBStartAdress = (uint32_t)&progress_bar1_image;

  pLayerCfg.ImageWidth = PROGRESS_BAR_WIDTH;
  pLayerCfg.ImageHeight = PROGRESS_BAR_HEIGHT;
  pLayerCfg.Backcolor.Blue = 0;
  pLayerCfg.Backcolor.Green = 0;
  pLayerCfg.Backcolor.Red = 0;
  pLayerCfg.Alpha0 = 0;
  HAL_LTDC_ConfigLayer(&hTFT_LTDC, &pLayerCfg, 0);

  pLayerCfg1.WindowX0 = PROGRESS_BAR2_X_START;
  pLayerCfg1.WindowX1 = PROGRESS_BAR2_X_START+PROGRESS_BAR_WIDTH;
  pLayerCfg1.WindowY0 = PROGRESS_BAR2_Y_START;
  pLayerCfg1.WindowY1 = PROGRESS_BAR2_Y_START+PROGRESS_BAR_HEIGHT;
  pLayerCfg1.Alpha = 255;
  pLayerCfg1.PixelFormat = LTDC_PIXEL_FORMAT_ARGB4444;

  pLayerCfg1.BlendingFactor1 = LTDC_BLENDING_FACTOR1_PAxCA;
  pLayerCfg1.BlendingFactor2 = LTDC_BLENDING_FACTOR2_PAxCA;
  pLayerCfg1.FBStartAdress = (uint32_t)&progress_bar2_image;

  pLayerCfg1.ImageWidth = PROGRESS_BAR_WIDTH;
  pLayerCfg1.ImageHeight = PROGRESS_BAR_HEIGHT;
  pLayerCfg1.Backcolor.Blue = 0;
  pLayerCfg1.Backcolor.Green = 0;
  pLayerCfg1.Backcolor.Red = 0;
  pLayerCfg1.Alpha0 = 0;
  HAL_LTDC_ConfigLayer(&hTFT_LTDC, &pLayerCfg1, 1);
}

void HAL_LTDC_MspInit(LTDC_HandleTypeDef* hltdc)
{

  GPIO_InitTypeDef GPIO_InitStruct;
  if(hltdc->Instance==LTDC)
  {
  /* USER CODE BEGIN LTDC_MspInit 0 */

  /* USER CODE END LTDC_MspInit 0 */
    /* Peripheral clock enable */
    __HAL_RCC_LTDC_CLK_ENABLE();

    /**LTDC GPIO Configuration
    PF10     ------> LTDC_DE
    PA3     ------> LTDC_B5
    PA4     ------> LTDC_VSYNC
    PA6     ------> LTDC_G2
    PB0     ------> LTDC_R3
    PB1     ------> LTDC_R6
    PB10     ------> LTDC_G4
    PB11     ------> LTDC_G5
    PG6     ------> LTDC_R7
    PG7     ------> LTDC_CLK
    PC6     ------> LTDC_HSYNC
    PC7     ------> LTDC_G6
    PA11     ------> LTDC_R4
    PA12     ------> LTDC_R5
    PC10     ------> LTDC_R2
    PD3     ------> LTDC_G7
    PD6     ------> LTDC_B2
    PG10     ------> LTDC_G3
    PG11     ------> LTDC_B3
    PG12     ------> LTDC_B4
    PB8     ------> LTDC_B6
    PB9     ------> LTDC_B7
    */
    GPIO_InitStruct.Pin = GPIO_PIN_10;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF14_LTDC;
    HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_6|GPIO_PIN_11
                          |GPIO_PIN_12;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF14_LTDC;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF9_LTDC;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_8|GPIO_PIN_9;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF14_LTDC;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_11;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF14_LTDC;
    HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_10;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF14_LTDC;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_3|GPIO_PIN_6;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF14_LTDC;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_12;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF9_LTDC;
    HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /* USER CODE BEGIN LTDC_MspInit 1 */

  /* USER CODE END LTDC_MspInit 1 */
  }

}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/08
*******************************************************************************/
void DisplayInit(void)
{
    ClearProgressBar(0);
    ClearProgressBar(1);
    LTDC_Init();            //Init. LTDC peripheral of the STM32
    SPI_Init();             //Init. SPI peripheral of the STM32.
                            // SPI used to communicate with the TFT Controller/Driver
    GPIO_Init();

    TFT_PanelInit();

    TFT_Display_ON();
    TFT_BACKLIGHT_ON;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/08
*******************************************************************************/
void DisplayDeInit(void)
{
  LTDC_LayerCfgTypeDef pLayerCfg;

  pLayerCfg.WindowX0 = 0;
  pLayerCfg.WindowX1 = 0;
  pLayerCfg.WindowY0 = 0;
  pLayerCfg.WindowY1 = 0;
  pLayerCfg.Alpha = 0;
  pLayerCfg.PixelFormat = 0;

  pLayerCfg.BlendingFactor1 = 0;
  pLayerCfg.BlendingFactor2 = 0;
  pLayerCfg.FBStartAdress = 0;

  pLayerCfg.ImageWidth = 0;
  pLayerCfg.ImageHeight = 0;
  pLayerCfg.Backcolor.Blue = 0;
  pLayerCfg.Backcolor.Green = 0;
  pLayerCfg.Backcolor.Red = 0;
  pLayerCfg.Alpha0 = 0;
  HAL_LTDC_ConfigLayer(&hTFT_LTDC, &pLayerCfg, 0);
  HAL_LTDC_ConfigLayer(&hTFT_LTDC, &pLayerCfg, 1);
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/10/24
*******************************************************************************/
uint8_t isCorner(uint16_t height, uint16_t width)
{
    uint8_t corner_pixel = 0;
    switch (height)
    {
    case PROGRESS_BAR_HEIGHT-1:
    case 0:
        if ((width < 5) || (width > (PROGRESS_BAR_WIDTH-6)))
        {
            corner_pixel = 1;
        }
        break;

    case PROGRESS_BAR_HEIGHT-2:
    case 1:
        if ((width < 3) || (width > (PROGRESS_BAR_WIDTH-4)))
        {
            corner_pixel = 1;
        }
        break;

    case PROGRESS_BAR_HEIGHT-3:
    case 2:
        if ((width < 2) || (width > (PROGRESS_BAR_WIDTH-3)))
        {
            corner_pixel = 1;
        }
        break;

    case PROGRESS_BAR_HEIGHT-4:
    case 3:
    case PROGRESS_BAR_HEIGHT-5:
    case 4:
        if ((width < 1) || (width > (PROGRESS_BAR_WIDTH-2)))
        {
            corner_pixel = 1;
        }
        break;

    default:
        break;
    }
    return (corner_pixel);
}




/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F.Simard
* @date   2017/10/24
*******************************************************************************/
void DrawProgressBar(uint8_t percent, uint8_t bar_selection)
{
    float x_steps = (float)PROGRESS_BAR_WIDTH/100;
    uint16_t *barPtr = progress_bar1_image;

    if (bar_selection == 1)
    {
        barPtr = progress_bar2_image;
    }


    for (uint16_t height = 0; height < PROGRESS_BAR_HEIGHT; height++)
    {
        for (uint16_t width = 0; width < PROGRESS_BAR_WIDTH; width++)
        {
            if (isCorner(height, width))
            {
                barPtr[height*PROGRESS_BAR_WIDTH+width] = PROGRESS_BAR_NO_COLOR;
            }
            else
            {
                if (width < ((uint16_t)(x_steps*percent)))
                {
                    barPtr[height*PROGRESS_BAR_WIDTH+width] = PROGRESS_BAR_FILL_COLOR;
                }
                else
                {
                    barPtr[height*PROGRESS_BAR_WIDTH+width] = PROGRESS_BAR_CLEAR_COLOR;
                }
            }
        }
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F.Simard
* @date   2017/10/24
*******************************************************************************/
void ClearProgressBar(uint8_t bar_selection)
{
    uint16_t *barPtr = progress_bar1_image;

    if (bar_selection == 1)
    {
        barPtr = progress_bar2_image;
    }

    for (uint32_t i = 0; i < (PROGRESS_BAR_HEIGHT*PROGRESS_BAR_WIDTH); i++)
    {
        barPtr[i] = PROGRESS_BAR_NO_COLOR;
    }
}

