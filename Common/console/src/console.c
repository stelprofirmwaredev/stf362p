/**************************************************************************//**
  \file console.c

  \brief
    Serial interface console implementation.

  \author
    Atmel Corporation: http://www.atmel.com \n
    Support email: avr@atmel.com

  Copyright (c) 2008-2015, Atmel Corporation. All rights reserved.
  Licensed under Atmel's Limited License Agreement (BitCloudTM).

  \internal
    History:
    22.05.12 N. Fomin - Created.
******************************************************************************/
/******************************************************************************
                    Includes section
******************************************************************************/
#include <ctype.h>
#include <string.h>
#include <stdint.h>
#include ".\console\inc\console.h"

/******************************************************************************
                    Definitions section
******************************************************************************/

/******************************************************************************
                    Types section
******************************************************************************/

/******************************************************************************
                              Local variables
******************************************************************************/

/******************************************************************************
                    Prototypes section
******************************************************************************/

/**************************************************************************//**
\brief Converts decimal string to unsigned long

\param[in] str - string to be converted
\param[out] out - pointer to converted number

\returns 1 if conversation was successful, 0 - otherwise
******************************************************************************/
// No check for overflow
uint8_t decimalStrToUlong(const char *str, uint32_t *out)
{
  uint32_t decCnt = 1;
  uint32_t ret = 0;
  const char *p;

  if (!*str)
    return 0;  // Empty

  for (p = str; *p; p++);                    // Rewind to the end of string

  for (p--; p >= str; p--, decCnt *= 10)     // locStrP points to the last symbol at the loop start
  {
    if (!isdigit((int)*p))
      return 0;

    ret += (*p - '0') * decCnt;
  }

  *out = ret;
  return 1;
}

/**************************************************************************//**
\brief Converts decimal string to signed long

\param[in] str - string to be converted
\param[out] out - pointer to converted number

\returns 1 if conversation was successful, 0 - otherwise
******************************************************************************/
uint8_t decimalStrToSlong(const char *str, int32_t *out)
{
  uint32_t ret;

  if (*str == '-')
  {
    if (decimalStrToUlong(++str, &ret))
    {
      *out = -ret;
      return 1;
    }
  }
  else if (*str)
  {
    if (decimalStrToUlong(str, &ret))
    {
      *out = ret;
      return 1;
    }
  }

  return 0;
}

/**************************************************************************//**
\brief Converts hex string to unsigned long

\param[in] str - string to be converted
\param[out] out - pointer to converted number

\returns 1 if conversation was successful, 0 - otherwise
******************************************************************************/
uint8_t hexStrToUlong(const char *str, uint32_t *out)
{
  uint32_t shift = 0;
  uint32_t ret = 0;
  const char *p;

  if (!*str)
    return 0;  // Empty

  for (p = str; *p; p++);                  // Rewind to the end of string

  for (p--; p >= str && shift < 32; p--, shift += 4)     // locStrP points to the last symbol at the loop start
  {
    char chr = *p;

    if (chr >= 'a' && chr <= 'f')
      chr -= 'a' - 0xA;
    else if (chr >= 'A' && chr <= 'F')
      chr -= 'A' - 0xA;
    else if (chr >= '0' && chr <= '9')
      chr -= '0';
    else
      return 0;

    ret += (uint32_t)chr << shift;
  }

  *out = ret;
  return 1;
}

/**************************************************************************//**
\brief Converts hex string to uint8 array

\param[in] str - string to be converted
\param[out] out - pointer to converted number

\returns 1 if conversation was successful, 0 - otherwise
******************************************************************************/
uint8_t hexStrTouint8array(const char *str, uint8_t *out, uint8_t length)
{
  uint32_t shift = 0;
  uint8_t i = 0U;
  uint8_t local = 0U;
  uint8_t bytecount = 0U;
  const char *p;

  if (!*str)
    return 0;  // Empty

  for (p = str; *p; p++);                  // Rewind to the end of string


  for (p--; p >= str && shift < (length * 8); p--, shift += 4)     // locStrP points to the last symbol at the loop start
  {
    char chr = *p;

    if (chr >= 'a' && chr <= 'f')
      chr -= 'a' - 0xA;
    else if (chr >= 'A' && chr <= 'F')
      chr -= 'A' - 0xA;
    else if (chr >= '0' && chr <= '9')
      chr -= '0';
    else
      return 0;

    ++bytecount;

    if (bytecount == 1)
    {
      local = (uint8_t)chr;
    }
    else
    {
     local += (uint8_t)chr << 4;
     out[i++] = local;
     bytecount = 0U;
     local = 0U;
    }
  }

  return 1;
}



/**************************************************************************//**
\brief Processes single char read from serial interface

\param[in] char - read char
******************************************************************************/
void consoleRx(char chr, consoleProperties_t *consoleInstance)
{
    if (chr != '\n')                             // Not EOL
    {
        if (chr == '\b')                   // Backspace: erase symbol
        {
            if (consoleInstance->cmdBufIdx > 0)                     // There are symbols in buffer - delete'em
            {
                consoleInstance->cmdBuf[consoleInstance->cmdBufIdx] = 0;                      // Rewind and terminate
                consoleInstance->cmdBufIdx--;
                //console_tx_str(VT100_ERASE_LINE"\r" CMD_PROMPT);// Reprint string
                //console_tx_str(cmdBuf);
            }
        }
        else if (chr == VT100_ESCAPE)                  // Escape: flush buffer and erase symbols
        {
            consoleInstance->cmdBufIdx = 0;
            //console_tx_str(VT100_ERASE_LINE"\r" CMD_PROMPT);
        }
        else if (chr >= 128 || isprint((int)(chr & 0xFF)) || chr == ' ')
        {
            if (consoleInstance->cmdBufIdx < CMD_BUF_SIZE)  // Put to buffer
            {
            #if FORCE_LOWCASE
                chr = tolower(chr);
            #endif
                consoleInstance->cmdBuf[consoleInstance->cmdBufIdx++] = chr;
                //consoleTx(chr);                   // Echo
            }
        }
    }
    else                                            // End of command string
    {
        if (consoleInstance->cmdBufIdx > 0)                         // Non-empty command
        {
            consoleInstance->cmdBuf[consoleInstance->cmdBufIdx] = 0;  // Mark an end of string
            if (NULL != consoleInstance->processFunc)
            {
                consoleInstance->processFunc(consoleInstance->cmdBuf);
            }
        }

        consoleInstance->cmdBufIdx = 0;                              // Drop buffer
        //console_tx_str(CRLF CMD_PROMPT);             // Command prompt
    }
}



/**************************************************************************//**
\brief Unpacks arguments according to the format string

\param[in] Cmd - command descriptor;
\param[in] Args - stack with pointers to arguments (some of them would be
               replaced by literal values after unpack)

\returns 1 if case of successful unpacking, 0 - otherwise
******************************************************************************/
uint8_t unpackArgs(const ConsoleCommand_t *cmd, ScanStack_t *args)
{
  // Now we have a command and a stack of arguments
  const char *fmt = cmd->fmt;
  ScanValue_t *val = args->args + 1;

  // Unpack arguments of command according to format specifier
  for (; *fmt; fmt++, val++)
  {
    if (val >= args->top)
      return 0;

    switch (*fmt)
    {
      case 's':                                   // String, leave as is
        break;

      case 'c':                                   // Single char
        if (val->str[1])                        // Check next char - must be zero terminator
          return 0;
        val->chr = val->str[0];
        break;

      case 'd':                                   // Integer
      case 'i':
        // Lookup for hex prefix. Negative hex is not supported
        if (val->str[0] == '0' && (val->str[1] == 'x' || val->str[1] == 'X'))
        {
          if (!hexStrToUlong(&val->str[2], &val->uint32))
            return 0;
        }
        else                                    // Decimal
        {
          if (!decimalStrToSlong(val->str, &val->int32))
            return 0;
        }
        break;

      default:
        break;
    }
  }

  if (val != args->top)                           // Starvation of arguments
    return 0;

  return 1;
}

/**************************************************************************//**
\brief Splits string to white-separated tokens; "quotes" on tokens are supported

\param[in] Str - cinput string; each token(substring) would be zero-terminated;
\param[in] Stk - stack for pointers to tokens

\returns 1 if case of successful unpacking, 0 - otherwise
******************************************************************************/
uint8_t tokenizeStr(char *str, ScanStack_t *stk)
{
  enum tokStates
  {
    WHITE,          // Inside of whitespaces
    TOK,            // Inside of token
    QTOK,           // Inside of quoted token
    POST_QTOK,      // Just after quoted token - waiting for whitespace
  };

  enum tokStates St = WHITE;

  stk->top = stk->args;

  for (; *str; str++)
  {
    switch (St)
    {
      case WHITE:
        if (*str == ' ')
          break;

        if (stk->top >= stk->end)          // No more space in stack
          return 0;

        if (*str == '"')
        {
          St = QTOK;
          (*stk->top++).str = str + 1;    // Store token position in stack (skipping quote)
        }
        else                                // Other symbol
        {
          St = TOK;
          (*stk->top++).str = str;        // Store token position in stack
        }
        break;

      case TOK:
        if (*str == '"')                    // Quotes are forbidden inside of plain token
          return 0;

        if (*str == ' ')
        {
          St = WHITE;
          *str = 0;                       // Put terminator
        }
        break;

      case QTOK:                              // Put terminator
        if (*str == '"')
        {
          St = POST_QTOK;
          *str = 0;
        }
        break;

      case POST_QTOK:
        if (*str != ' ')
          return 0;
        St = WHITE;
        break;
    }
  }

  if (St == QTOK)                             // Scan ended while inside of quote
    return 0;

  return 1;
}

