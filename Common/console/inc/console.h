/**************************************************************************//**
  \file console.h

  \brief
    Serial interface console interface.

  \author
    Atmel Corporation: http://www.atmel.com \n
    Support email: avr@atmel.com

  Copyright (c) 2008-2015, Atmel Corporation. All rights reserved.
  Licensed under Atmel's Limited License Agreement (BitCloudTM).

  \internal
    History:
    22.05.12 N. Fomin - Created.
******************************************************************************/
#ifndef _CONSOLE_H
#define _CONSOLE_H

/******************************************************************************
                    Includes section
******************************************************************************/
#include <stdint.h>

/******************************************************************************
                    Defines section
******************************************************************************/

#define VT100_ESCAPE        0x1B
#define VT100_ERASE_LINE    "\x1B[2K"
#define CMD_PROMPT          "$"

#define CMD_BUF_SIZE        200
#define MAX_NUM_OF_ARGS     17
#define CRLF                "\r\n"
#define FORCE_LOWCASE 0


/**************************************************************************//**
                    Types section
******************************************************************************/
typedef union
{
  int8_t int8;
  uint8_t uint8;
  int16_t int16;
  uint16_t uint16;
  int32_t int32;
  uint32_t uint32;
  char chr;
  char *str;
} ScanValue_t;

/**************************************************************************//**
Valid format codes are:
  d, i - signed integer. '0x' perfix before value treat it like hex, otherwise its decimal.
  Hex couldn't be negative (have a '-' before value);
  s - string;
  c - single character.
******************************************************************************/
typedef struct
{
  const char *const name;              // Command name
  const char *const fmt;               // Format string for arguments
  void (*handler)(const ScanValue_t *);// Process function
} ConsoleCommand_t;

typedef struct
{
  ScanValue_t *top;
  ScanValue_t args[MAX_NUM_OF_ARGS];
  ScanValue_t end[0];
} ScanStack_t;

typedef struct
{
    void (*processFunc)(char *);
    char *cmdBuf;
    uint8_t cmdBufIdx;
} consoleProperties_t;

/******************************************************************************           
                     external variables section
******************************************************************************/
extern ConsoleCommand_t *cmdForHelpCmd;

/**************************************************************************//**
\brief Sends string to serial interface
******************************************************************************/
void consoleTxStr(const char *str);

/**************************************************************************//**
\brief Sends single char to serial interface
******************************************************************************/
void consoleTx(char chr);

/**************************************************************************//**
\brief Processes single char read from serial interface

\param[in] char - read char
******************************************************************************/
void consoleRx(char chr, consoleProperties_t *consoleInstance);

/**************************************************************************//**
\brief Initializes console
******************************************************************************/
void initConsole(void);

/**************************************************************************//**
\brief Processes data received by console
******************************************************************************/
void processConsole(uint16_t length);
uint8_t tokenizeStr(char *str, ScanStack_t *stk);
uint8_t unpackArgs(const ConsoleCommand_t *cmd, ScanStack_t *args);
uint8_t hexStrTouint8array(const char *str, uint8_t *out, uint8_t length);


#endif // _CONSOLE_H
