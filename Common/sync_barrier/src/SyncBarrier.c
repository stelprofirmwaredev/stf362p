/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    SyncBarrier.c
* @date    2017/09/11
* @authors J-F. Simard
* @brief   
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include ".\sync_barrier\inc\SyncBarrier.h"
#include "cmsis_os.h"

    #include "trcRecorder.h"
    extern char* dbEventChannel;

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
uint16_t taskBarrierState = 0;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
const char * sync_barrier_names [] = 
{
    [0] = "Zigbee",
    [1] = "WiFi",
    [2] = "Console",
    [3] = "Notifications",
    [4] = "Analytics",
    [5] = "Database",
    [6] = "Hmi",
    [7] = "Load management",
    [8] = "sensors",
    [9] = "default",
    [10] = "Zigbee StateMachines"
};


/*******************************************************************************
*    Public functions definitions
*******************************************************************************/

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void print_sync_barrier(barriers_name_t barriers)
{
    char barrier_string [100];
    
    sprintf(barrier_string, "Sync barrier waiting for ");
    
    for (uint8_t i = 0; i < 10; i++)
    {
        barriers_name_t locked_and_waiting = barriers;
        
        locked_and_waiting &= ~taskBarrierState;
        if (locked_and_waiting&(1<<i))
        {
            strcat(barrier_string,sync_barrier_names[i]);
            strcat(barrier_string,", ");
        }
    }
    
    vTracePrint(dbEventChannel, barrier_string);
}


/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void print_unlock_barrier(barriers_name_t barriers)
{
    char barrier_string [100];
    
    sprintf(barrier_string, "unlocking barrier ");
    
    for (uint8_t i = 0; i < 10; i++)
    {
        if (barriers&(1<<i))
        {
            strcat(barrier_string,sync_barrier_names[i]);
            strcat(barrier_string,", ");
        }
    }
    
    vTracePrint(dbEventChannel, barrier_string);
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
void print_lock_barrier(barriers_name_t barriers)
{
    char barrier_string [100];
    
    sprintf(barrier_string, "locking barrier ");
    
    for (uint8_t i = 0; i < 10; i++)
    {
        if (barriers&(1<<i))
        {
            strcat(barrier_string,sync_barrier_names[i]);
            strcat(barrier_string,", ");
        }
    }
    
    vTracePrint(dbEventChannel, barrier_string);
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/09/11
*******************************************************************************/
void Sync_Barrier(barriers_name_t barriers)
{
    while ((taskBarrierState & barriers) != barriers)
    {
        print_sync_barrier(barriers);
        vTaskDelay(pdMS_TO_TICKS(10));       //let the RTOS serve lower priority tasks
    }
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/09/11
*******************************************************************************/
void Sync_TaskReady(barriers_name_t barrier)
{
    print_unlock_barrier(barrier);
    taskBarrierState |= barrier;
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2018/08/14
*******************************************************************************/
void Sync_ClearTaskReady(barriers_name_t barrier)
{
    print_lock_barrier(barrier);
    taskBarrierState &= ~barrier;
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
