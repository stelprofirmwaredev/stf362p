/*******************************************************************************
* @file    SyncBarrier.h
* @date    2017/09/11
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef _SyncBarrier_H
#define _SyncBarrier_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>


/*******************************************************************************
* Public constants definitions
*******************************************************************************/
typedef enum
{
    BARRIER_TASK_CONSOLE                = (1<<2),
    BARRIER_TASK_NOTIFICATIONS          = (1<<3),
    BARRIER_TASK_ANALYTICS              = (1<<4),
    BARRIER_TASK_DATABASE               = (1<<5),
    BARRIER_TASK_HMI                    = (1<<6),
    BARRIER_TASK_LOADMNG                = (1<<7),
    BARRIER_TASK_SENSORS                = (1<<8),
    BARRIER_TASK_DEFAULT                = (1<<9),
} barriers_name_t;


/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void Sync_Barrier(barriers_name_t barriers);
void Sync_TaskReady(barriers_name_t barrier);
void Sync_ClearTaskReady(barriers_name_t barrier);

#endif /* _SyncBarrier_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
