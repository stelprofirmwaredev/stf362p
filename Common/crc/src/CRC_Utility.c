/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    Flash_Utility.c
* @date    2017/08/01
* @authors J-F. Simard
* @brief   
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include ".\inc\FlashOps.h"
#include <stdint.h>

#include "stm32f4xx_hal.h"
#include "FlashLayout.h"
#include "loaderStates.h"
#include "BootLoader_OTA_Control.h"
#include ".\HAL\inc\HAL_Flash.h"
#include ".\HAL\inc\HAL_Console.h"
#include "bootDisplay.h"
#include "..\..\common\crc\inc\CRC_Utility.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define FINAL_XOR_VALUE         0xFFFFFFFF


/*******************************************************************************
*    Private variables definitions
*******************************************************************************/



/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
CRC_HandleTypeDef crcHandle;



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/

/**
  * @brief  Initializes the CRC MSP.
  * @param  hcrc: pointer to a CRC_HandleTypeDef structure that contains
  *         the configuration information for CRC
  * @retval None
  */
void HAL_CRC_MspInit(CRC_HandleTypeDef *hcrc)
{
    __HAL_RCC_CRC_CLK_ENABLE();
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/08/02
*******************************************************************************/
uint32_t Reflect32(uint32_t input)
{
    uint32_t output = 0;
    
    for (uint8_t i = 0; i < 32; i++)
    {
        output |= (input&(1<<i)?1<<(32-1-i):0);
    }
    return output;                   
}


/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/08/02
*******************************************************************************/
uint32_t ReverseOrder32(uint32_t input)
{
    uint32_t output = 0;
    
    output |= (input&0x000000FF) << 24;
    output |= (input&0x0000FF00) << 8;
    output |= (input&0x00FF0000) >> 8;
    output |= (input&0xFF000000) >> 24;
    return output;
}

/*******************************************************************************
* @brief  with the help of on http://www.sunshine2k.de/coding/javascript/crc/crc_js.html
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/08/02
*******************************************************************************/
uint32_t ComputeChecksum(uint32_t input)
{
    uint32_t checksum;
    
    input = Reflect32(input);             //Input reflected
    
    checksum = HAL_CRC_Accumulate(&crcHandle, &input, 1);
    
    checksum = Reflect32(checksum);       //Output reflected
    checksum ^= FINAL_XOR_VALUE;             //Final XOR 
    
    return (checksum);
}


/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
crc_error_t VerifyBlockCRC (uint32_t blockStartAddress)
{
    
    uint32_t externalFlashBuffer[EXTFLASH_MAX_BYTE_PER_TRANSACTION/sizeof(uint32_t)];
    uint32_t extFlash_FromAddress = blockStartAddress;
    uint32_t calculatedCrc32 = (int32_t)-1;
    uint32_t blockCrc32 = (int32_t)-1;
    
    crcHandle.Instance = CRC;
    crcHandle.Lock = HAL_UNLOCKED;
    crcHandle.State = HAL_CRC_STATE_RESET;
    HAL_CRC_Init(&crcHandle);
    HAL_CRC_Calculate(&crcHandle,NULL, 0);        //force a reset of the CRC registers
    for(uint16_t i = 0; i < (OTAUIMAGE_MAX_SIZE/EXTFLASH_MAX_BYTE_PER_TRANSACTION);i++)      // size of the sector/256
    {

        HAL_ReadFlashData(extFlash_FromAddress, EXTFLASH_MAX_BYTE_PER_TRANSACTION, (uint8_t*)externalFlashBuffer); //read 256 bytes from external flash; store in table externalFlashBuffer
#ifdef BOOTLOADER
        DrawProgressBar((i*100)/(OTAUIMAGE_MAX_SIZE/EXTFLASH_MAX_BYTE_PER_TRANSACTION),0);
#endif
        for (uint16_t dataIdx = 0; dataIdx < EXTFLASH_MAX_BYTE_PER_TRANSACTION/sizeof(uint32_t); dataIdx++)
        {
            if ((extFlash_FromAddress != EXTFLASH_CRC_A) && ((extFlash_FromAddress != EXTFLASH_CRC_B)))
            {
                calculatedCrc32 = ComputeChecksum(externalFlashBuffer[dataIdx]);
            }
            else
            {
                __no_operation();
                break;
            }
            extFlash_FromAddress += sizeof(uint32_t);
        }        
    }

    blockCrc32 = ReverseOrder32(externalFlashBuffer[(EXTFLASH_MAX_BYTE_PER_TRANSACTION/sizeof(uint32_t))-1]);
    if (calculatedCrc32 == blockCrc32)
    {
        return CRC_NOERROR;
    }
    else
    {
        return CRC_BADCRC;
    }
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
