/*******************************************************************************
* @file    strings_tool.h
* @date    2017/05/01
* @authors J-F. Simard
* @brief 
*******************************************************************************/

#ifndef strings_tool_H
#define strings_tool_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>


/*******************************************************************************
* Public constants definitions
*******************************************************************************/



/*******************************************************************************
* Public structures definitions
*******************************************************************************/
typedef struct _strToken_t
{
	char * strToken;
	struct _strToken_t *next;
} strToken_t;

typedef struct
{
	uint8_t nbOfTokens;
	strToken_t *firstToken;
	strToken_t *lastToken;
} strTokenList_t;


/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void ConvertASCIIToUTF8(char* src, char* dst, uint16_t size);
void ConvertUTF8ToASCII(char* src, char* dst, uint16_t size);
int8_t StringTokenizer (strTokenList_t * list, char * const str,  char * const delimiter);
char* GetStringToken (strTokenList_t * list, uint8_t pos);
void ClearStringTokenList (strTokenList_t * list);
void stringNCopyWithNull(char *dst, char *src, uint16_t size);

#endif /* strings_tool_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
