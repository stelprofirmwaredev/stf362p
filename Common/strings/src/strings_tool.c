/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    strings_tool.c
* @date    2017/05/01
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <string.h>
#include ".\strings\inc\strings_tool.h"

#include "cmsis_os.h"
/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/



/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/


/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  Function is same as strncpy, but this one adds a '\0' at end of the
*       string, where strncpy's litterature is not explicit about it
* @inputs :
*   - char * dst : destination string pointer
*   - char * src : source string pointer
*   - uint8_t size: size of the destination string
* @retval None
* @author J-F. Simard
* @date   2017/05/16
*******************************************************************************/
void stringNCopyWithNull(char *dst, char *src, uint16_t size)
{
    strncpy(dst, src, size);

    //Make sure the string is NULL terminated
    dst[size-1] = '\0';
}


/*******************************************************************************
* @brief  Convert a string from ASCII format info UTF-8 format (accent issue)
* @inputs src: source string containing ASCII characters
* @inputs dst: destination string with converted UTF-8 characters
* @inputs size: source string size
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/03/10
*******************************************************************************/
void ConvertASCIIToUTF8(char* src, char* dst, uint16_t size)
{
    uint16_t character;
    uint16_t dstCharacter;

    dstCharacter = 0;
    for (character = 0; character < size; character++)
    {
        if (src[character] >= 0xC0)
        {
            dst[dstCharacter++] = 0xC3;
            dst[dstCharacter] = src[character] - 0x40;
        }
        else if (src[character] >= 0x80)
        {
            dst[dstCharacter++] = 0xC2;
            dst[dstCharacter] = src[character];
        }
        else
        {
            dst[dstCharacter] = src[character];
        }
        dstCharacter++;
    }
    dst[dstCharacter] = 0;
}

/*******************************************************************************
* @brief  Convert a string from UTF-8 format into ASCII format (accent issue)
* @inputs src: source string containing UTF-8 characters
* @inputs dst: destination string with converted ASCII characters
* @inputs size: source string size
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/12/15
*******************************************************************************/
void ConvertUTF8ToASCII(char* src, char* dst, uint16_t size)
{
    uint16_t character;
    uint16_t dstCharacter;

    character = 0;
    for (dstCharacter = 0; dstCharacter < (size-1); dstCharacter++)
    {
        if (src[character] == 0xC2)
        {
            character++;
            dst[dstCharacter] = src[character];
        }
        else if (src[character] == 0xC3)
        {
            character++;
            dst[dstCharacter] = src[character] + 0x40;
        }
        //Special case for LEFT SINGLE QUOTATION MARK and RIGHT SINGLE QUOTATION MARK
        else if (src[character] == 0xE2)
        {
            character++;
            if (src[character] == 0x80)
            {
                character++;
                if ((src[character] == 0x98) || (src[character] == 0x99))
                {
                    //Convert them into APOSTROPHE
                    dst[dstCharacter] = 0x27;
                }
            }
        }
        else
        {
            dst[dstCharacter] = src[character];
            if (src[character] == '\0')
            {
                break;
            }
        }
        character++;
    }
    dst[dstCharacter] = '\0';
}

/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author 
* @date   
*******************************************************************************/
static void * allocate_token_memory (size_t size)
{
    void * mem_ptr = NULL;
    
    while (NULL == mem_ptr)
    {
        mem_ptr = pvPortMalloc(size);
        
        if (NULL == mem_ptr)
        {
            osDelay(1);
        }
    }
    return mem_ptr;
}

/*******************************************************************************
* @brief  Function that separate a string in multiple shorter string based on
*			on a provided delimiter.  This function dynamically allocate memory,
*			thus, the "ClearStringTokenList()" function must be invoked at the
*			end of the operations to free the memory and avoid memory leak
* @inputs :
*	strTokenList_t * :
*	char * const : pointer to the string that needs to be tokenized. Must be a
*					NULL terminated string
*	char * const : delimiter
* @retval :
*		int8_t : error status
*					 -1 = list ptr is NULL
*					  1 = str ptr is NULL
*					  0 = success
* @author J-F. Simard
* @date   2017/05/08
*******************************************************************************/
int8_t StringTokenizer (strTokenList_t * list, char * const str,  char * const delimiter)
{
	char * tokenStartPtr;

	if (NULL != list)
	{
		list->nbOfTokens = 0;
		list->firstToken = NULL;
		list->lastToken = NULL;
		if ((NULL != str) && (*str != '\0'))
		{
			uint8_t tokenSize;
            uint8_t tokenCnt = 1;   //since str is not NULL, count at least 1 token
            char * strTokenCnt;

			strTokenCnt = strchr(str, *delimiter);

            while (NULL != strTokenCnt)
            {
                strTokenCnt = strchr(strTokenCnt+1, *delimiter);
                tokenCnt++;
            }

            tokenStartPtr = str;
            while (tokenCnt-- > 0)
			{
				strToken_t * newStrToken;

				newStrToken = allocate_token_memory (sizeof(strToken_t));
				if (NULL != newStrToken)
				{
					char * newToken;

					newStrToken->next = NULL;
					newStrToken->strToken = NULL;
                    tokenSize = strcspn(tokenStartPtr, delimiter);
					newToken = allocate_token_memory(tokenSize+1);
					if (NULL != newToken)
					{
                        list->nbOfTokens++;
						strncpy (newToken,tokenStartPtr, tokenSize);
						newToken[tokenSize] = '\0';
						newStrToken->strToken = newToken;
                        if (NULL == list->firstToken)
                        {
                            list->firstToken = newStrToken;
                            list->lastToken = list->firstToken;
                        }
                        else
                        {
                            list->lastToken->next = newStrToken;
                            list->lastToken = list->lastToken->next;
                        }
					}
				}
                else
                {
                    break;
                }
				tokenStartPtr = strchr(tokenStartPtr, *delimiter)+1;
			}

			return 0;
		}
		else
		{
			list->nbOfTokens = 0;
			return 1;				//no valid str in input
		}
	}
	else
	{
		return -1;					//pointer to token list not provided
	}
}

/*******************************************************************************
* @brief  Return a string token contained in the *list.  If pos is greater than
*			the number of token in the list, it will return default '\0'
* @inputs :
*   strTokenList_t * : ptr to a strTokenList_t
*   uint8_t : position requested. Start at 0
* @retval : ptr to the strtoken
* @author
* @date
*******************************************************************************/
char* GetStringToken (strTokenList_t * list, uint8_t pos)
{
    char * strToken;
    uint8_t i = 0;
    strToken_t * strTokenPtr;

    strToken = "\0";
    strTokenPtr = list->firstToken;

    pos += 1;

    if (pos > list->nbOfTokens)
    {
        pos = 0;
    }

    while (i < pos)
    {
        if (NULL != strTokenPtr)
        {
            strToken = strTokenPtr->strToken;

            strTokenPtr = strTokenPtr->next;
        }
        else
        {
            strToken = "\0";
        }
        i++;
    }

    return strToken;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void ClearStringTokenList (strTokenList_t * list)
{
    while (NULL != list->firstToken)
    {
        strToken_t * nextMemToFree;

        nextMemToFree = list->firstToken->next;
        vPortFree(list->firstToken->strToken);
        vPortFree(list->firstToken);
        list->firstToken = nextMemToFree;
    }
}



/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
