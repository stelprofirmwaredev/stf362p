/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2015, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    HAL_CurrentSensing.c
* @date    2016/09/09
* @authors Jean-Fran�ois Many
* @brief   Current Sensing Module based on ACS716
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include "typedef.h"
#include "HAL_Interrupt.h"
#include "stm32f4xx_hal.h"
#include ".\HAL\inc\HAL_CurrentSensing.h"
#include ".\Sensors\inc\THREAD_Sensors.h"
#include <math.h>

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define CURRENT_READ_TIMEOUT  (HAL_MAX_DELAY-1)
#define SENSITIVITY         0.0132   //0.0132V/A
#define CURRENT_ACQ_MAX     200
#ifdef BIS_CONTROLLER
    #define CURRENT_CHANNEL     ADC_CHANNEL_7
#else
    #define CURRENT_CHANNEL     ADC_CHANNEL_1
#endif

#define ADC_MAX             4095

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
static float MeasuredCurrent;
static U16BIT MeasuredSensor;
static ADC_HandleTypeDef CurrentSensing_ADC;
static U16BIT CurrentReadings[CURRENT_ACQ_MAX];

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
TIM_HandleTypeDef htim3;


/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static void MX_TIM3_Init(void);


/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/* TIM3 init function */
static void MX_TIM3_Init(void)
{
    RCC_ClkInitTypeDef    clkconfig;
    uint32_t              uwTimclock = 0;
    uint32_t              uwPrescalerValue = 0;
    uint32_t              pFLatency;

    /*Configure the TIM3 IRQ priority */
    HAL_NVIC_SetPriority(TIM3_IRQn, 15, 0);

    /* Enable the TIM3 global Interrupt */
    HAL_NVIC_EnableIRQ(TIM3_IRQn);

    /* Enable TIM3 clock */
    __TIM3_CLK_ENABLE();

    /* Get clock configuration */
    HAL_RCC_GetClockConfig(&clkconfig, &pFLatency);

    /* Compute TIM3 clock */
    uwTimclock = HAL_RCC_GetPCLK2Freq();

    /* Compute the prescaler value to have TIM3 counter clock equal to 1MHz */
    uwPrescalerValue = (uint32_t) (uwTimclock / 1000000);

    /* Initialize TIM3 */
    htim3.Instance = TIM3;

    /* Initialize TIMx peripheral as follow:
    + Period = [(TIM3CLK/2400) - 1]. to have a 0.416 ms time base.
    + Prescaler = (uwTimclock/1000000 - 1) to have a 1MHz counter clock.
    + ClockDivision = 0
    + Counter direction = Up
    */
    htim3.Init.Period = (1000000 / 2400);
    htim3.Init.Prescaler = uwPrescalerValue;
    htim3.Init.ClockDivision = 0;
    htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
    if (HAL_TIM_Base_Init(&htim3) == HAL_OK)
    {
        //The interrupt is managed after the current acquisition
        //HAL_TIM_Base_Start_IT(&htim3);
    }
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  Current Sensor initialization function
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/09/09
*******************************************************************************/
void CurrentSensor_Init(void)
{
    MeasuredCurrent = 0;

    /**Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
    */
    CurrentSensing_ADC.Instance = ADC1;
    CurrentSensing_ADC.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
    CurrentSensing_ADC.Init.Resolution = ADC_RESOLUTION_12B;
    CurrentSensing_ADC.Init.ScanConvMode = DISABLE;
    CurrentSensing_ADC.Init.ContinuousConvMode = DISABLE;
    CurrentSensing_ADC.Init.DiscontinuousConvMode = DISABLE;
    CurrentSensing_ADC.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
    CurrentSensing_ADC.Init.DataAlign = ADC_DATAALIGN_RIGHT;
    CurrentSensing_ADC.Init.NbrOfConversion = 1;
    CurrentSensing_ADC.Init.DMAContinuousRequests = DISABLE;
    CurrentSensing_ADC.Init.EOCSelection = ADC_EOC_SINGLE_CONV;

    HAL_ADC_MspInit(&CurrentSensing_ADC);

    HAL_ADC_Init(&CurrentSensing_ADC);

    MX_TIM3_Init();
}

/*******************************************************************************
* @brief  Current Sensor monitoring function
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/09/09
*******************************************************************************/
void HAL_CurrentSensorMonitoring(void)
{
    ADC_ChannelConfTypeDef sConfig;
    HAL_StatusTypeDef status = HAL_OK;
    U32BIT sum_current = 0;
    U16BIT CurrentMinADC;
    U16BIT CurrentMaxADC;
    U16BIT CurrentMeanADC;
    float CurrentMinVoltage;
    float CurrentMaxVoltage;
    float CurrentMeanVoltage;
    float CurrentMinAmp;
    float CurrentMaxAmp;
    static U16BIT min = 0xFFFF;
    static U16BIT max = 0;
    static uint8_t AcqCurrent = 0;

    /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
    */
    sConfig.Channel = CURRENT_CHANNEL;
    sConfig.Rank = 1;
    sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
    if (HAL_ADC_ConfigChannel(&CurrentSensing_ADC, &sConfig) != HAL_OK)
    {
//            Error_Handler();
    }
    HAL_ADC_Start(&CurrentSensing_ADC);
    status = HAL_ADC_PollForConversion(&CurrentSensing_ADC, CURRENT_READ_TIMEOUT);
    if (status == HAL_OK)
    {
        HAL_ADC_GetValue(&CurrentSensing_ADC);
        MeasuredSensor = CurrentSensing_ADC.Instance->DR;
    }
    HAL_ADC_Stop(&CurrentSensing_ADC);

    CurrentReadings[AcqCurrent] = MeasuredSensor;
    if (max < MeasuredSensor)
    {
        max = MeasuredSensor;
    }
    if (min > MeasuredSensor)
    {
        min = MeasuredSensor;
    }
    AcqCurrent++;
    if (AcqCurrent < CURRENT_ACQ_MAX)
    {
        HAL_TIM_Base_Start_IT(&htim3);
    }
    else
    {
        HAL_TIM_Base_Stop_IT(&htim3);
        CurrentMeanADC = 0;
        sum_current = 0;
        for (AcqCurrent = 0; AcqCurrent < CURRENT_ACQ_MAX; AcqCurrent++)
        {
            vTracePrintF(sensorEventChannel, "ReadingNb,%d,ReadingValue,%d", AcqCurrent, CurrentReadings[AcqCurrent]);
            sum_current += CurrentReadings[AcqCurrent];
        }
        CurrentMeanADC = sum_current / CURRENT_ACQ_MAX;
        CurrentMinADC = min;
        CurrentMaxADC = max;
        AcqCurrent = 0;
        min = 0xFFFF;
        max = 0;
        CurrentMeanVoltage = (float)CurrentMeanADC * 3.3 / ADC_MAX;
        CurrentMinVoltage = (float)CurrentMinADC * 3.3 / ADC_MAX;
        CurrentMaxVoltage = (float)CurrentMaxADC * 3.3 / ADC_MAX;
        CurrentMaxAmp = (CurrentMaxVoltage - CurrentMeanVoltage) / SENSITIVITY;
        CurrentMinAmp = (CurrentMeanVoltage - CurrentMinVoltage) / SENSITIVITY;
        MeasuredCurrent = ((CurrentMaxAmp + CurrentMinAmp) / 2) / sqrt(2);
        vTracePrintF(sensorEventChannel, ",,,,%d,%d,%d,%d", CurrentMeanVoltage, CurrentMinVoltage, CurrentMaxVoltage, MeasuredCurrent);
    }
}

/*******************************************************************************
* @brief  Retrieve the actual sensed current
* @inputs None
* @retval float: current sensed
* @author Jean-Fran�ois Many
* @date   2016/09/09
*******************************************************************************/
float HAL_CurrentSensor_GetCurrent(void)
{
    return MeasuredCurrent;
}

/*******************************************************************************
* @brief  Retrieve the actual sensed current
* @inputs None
* @retval float: current sensed
* @author Jean-Fran�ois Many
* @date   2016/09/09
*******************************************************************************/
void HAL_CurrentSensor_SetCurrent(float measuredCurrent)
{
    MeasuredCurrent = measuredCurrent;
}

/** Copyright(C) 2014 Stelpro Design, All Rights Reserved**/
