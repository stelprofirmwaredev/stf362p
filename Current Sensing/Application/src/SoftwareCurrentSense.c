/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2017, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    SoftwareCurrentSense.c
* @date    2017/10/05
* @authors J-F. Simard
* @brief   
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include ".\Application\inc\SoftwareCurrentSense.h"
#include <math.h>


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define NUMBER_OF_CURRENT_PROFILE                   5
#define SAMPLE_TABLE_SIZE                           100
#define CONFIDENCE_INTERVAL                         2   //1=68%;2=95%;3=99.7%


#define CURRENTSENSOR_MIN_INPUT                     -0.3
#define CURRENTSENSOR_MAX_INPUT                     1

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
typedef struct
{
    float regression_coeff_A;
    float regression_coeff_B;
    float regression_intercept;
    float measured_current;
} currentProfile_t;


const currentProfile_t currentProfile_table[NUMBER_OF_CURRENT_PROFILE] = 
{
    {
        .regression_coeff_A = -0.1874,
        .regression_coeff_B = -0.1646,
        .regression_intercept = 0.161,
        .measured_current = 0.95,
    },
    {
        .regression_coeff_A = -0.1216,
        .regression_coeff_B = -0.014,
        .regression_intercept = 0.1727,
        .measured_current = 3.7,
    },
    {
        .regression_coeff_A = -0.08,
        .regression_coeff_B = 0.22,
        .regression_intercept = 0.2,
        .measured_current = 7.5,
    },
    {
        .regression_coeff_A = -0.2109,
        .regression_coeff_B = 0.5072,
        .regression_intercept = 0.1912,
        .measured_current = 11.3,
    },
    {
        .regression_coeff_A = -0.4571,
        .regression_coeff_B = 0.9691,
        .regression_intercept = 0.2129,
        .measured_current = 16.64,
    },
};


struct 
{
    float calculated_current;
    float squared_error;
} sample_table[SAMPLE_TABLE_SIZE];

struct
{
    uint8_t sample_size;
    uint8_t table_PoW;
} sample_table_management = {.sample_size = 0, .table_PoW = 0};


/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2017/10/05
*******************************************************************************/
float calc_loadCurrent(float heatRise, int8_t dutyCycle)
{
    float loadCurrent = -1;
    float profileCurrent[NUMBER_OF_CURRENT_PROFILE];
    
    if ((dutyCycle > 0) && (dutyCycle <= 100))
    {
        for (uint8_t i=0; i < NUMBER_OF_CURRENT_PROFILE; i++)
        {
            float _x = (float)dutyCycle/100;
            profileCurrent[i] = 
                    (_x * _x * currentProfile_table[i].regression_coeff_A) + 
                    (_x * currentProfile_table[i].regression_coeff_B) +
                    currentProfile_table[i].regression_intercept;
        }        

        if (heatRise > CURRENTSENSOR_MIN_INPUT)
        {
            uint8_t i=1;
            while ((heatRise > profileCurrent[i]) && (i < NUMBER_OF_CURRENT_PROFILE))
            {
                i++;
            }
            
            if (i < NUMBER_OF_CURRENT_PROFILE)
            {
                float ratio;
                ratio = (heatRise-profileCurrent[i])/
                                    (profileCurrent[i-1]-profileCurrent[i]);
                
                loadCurrent = currentProfile_table[i].measured_current;
                loadCurrent -= (
                                    currentProfile_table[i].measured_current - 
                                    currentProfile_table[i-1].measured_current
                               ) * ratio;
            }
        }
    }
    return loadCurrent;
}


/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2017/10/05
*******************************************************************************/
current_estimation_t computeStatisticalValue (float loadCurrent)
{
    float sum = 0;
    float mean;
    float error;
    float sum_of_square = 0;
    float variance;
    float stdDeviation;
    float high_interval;
    static current_estimation_t estimation = {.value = 0, .confidence_level = 100};

    if (-1 != loadCurrent)
    {
        sample_table[sample_table_management.table_PoW].calculated_current = loadCurrent;
            
        if (sample_table_management.sample_size < SAMPLE_TABLE_SIZE)
        {
            sample_table_management.sample_size++;
        }    
    }

    if (sample_table_management.sample_size > 0)
    {
        for (uint8_t i = 0; i < sample_table_management.sample_size; i++)
        {
            sum += sample_table[i].calculated_current;
        }
        mean = sum/(sample_table_management.sample_size);
        error = loadCurrent - mean;
        sample_table[sample_table_management.table_PoW].squared_error = error*error;

        for (uint8_t i = 0; i < sample_table_management.sample_size; i++)
        {
            sum_of_square += sample_table[i].squared_error;
        }
        
        variance = sum_of_square / sample_table_management.sample_size;
        stdDeviation = sqrt(variance);
        high_interval = mean + (CONFIDENCE_INTERVAL * 
                                (stdDeviation/sqrt(sample_table_management.sample_size)));
        estimation.value = mean;
        estimation.confidence_level = mean / high_interval * 100; 
        
        if (-1 != loadCurrent)
        {
            sample_table_management.table_PoW++;
            sample_table_management.table_PoW %= SAMPLE_TABLE_SIZE;
        }
    }
    
    return estimation;
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2017/10/05
*******************************************************************************/
current_estimation_t LoadCurrentEstimation(float heatRise, uint8_t dutyCycle)
{
    current_estimation_t estimation;
    
    estimation = computeStatisticalValue(calc_loadCurrent(heatRise, dutyCycle));
    
    return estimation;
}




/** Copyright(C) 2017 Stelpro Design, All Rights Reserved**/
