/*******************************************************************************
* @file    HAL_Keypad.h
* @date    2016/08/09
* @authors J-F.Simard
* @brief
*******************************************************************************/

#ifndef HAL_Keypad_H
#define HAL_Keypad_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>

#ifdef TP_FT7401
    #include ".\BSP\FT7401\inc\TP_FT7401.h"
#else
    #include ".\BSP\FT6336U\inc\TP_FT6336U.h"
#endif
#include "common_types.h"

/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define Enable_TouchPanel()           TouchPanel_Init()


/*******************************************************************************
* Public structures definitions
*******************************************************************************/
typedef enum
{
    TA_STATUS_PRESS = 0x00,
    TA_STATUS_RELEASE,
    TA_STATUS_HOLD,
} touchArea_status_t;

typedef struct
{
    uint16_t x_StartPos;
    uint16_t y_StartPos;
    uint16_t x_EndPos;
    uint16_t y_EndPos;
    touchArea_status_t status;
    void (*buttonPress_cb)(void);
    void (*buttonRelease_cb)(void);
    void (*buttonHold_cb)(void);
} touchArea_t;


/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void KeyPadInit(void);
void KeyPad_ReadPanelState(TS_StateTypeDef * touchData, uint8_t* orientation);


#endif /* HAL_Keypad_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
