/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    HAL_KeyPad.c
* @date    2016/08/09
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <stdlib.h>
#include ".\HAL\inc\HAL_Keypad.h"
#include "stm32f4xx_hal.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define LANDSCAPE   0
#define PORTRAIT    1

#define X_PROGRESSIVE_OFFSET_RATIO      18//6.5
#define Y_PROGRESSIVE_OFFSET_RATIO      18//13

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
static FTxxxx_reg_t regMap;


/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
I2C_HandleTypeDef hTS_I2C;



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static void I2C_Init(void);
static void GPIO_Init(void);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/09
*******************************************************************************/
static void I2C_Init(void)
{
  hTS_I2C.Instance = I2C1;
  hTS_I2C.Init.ClockSpeed = 100000;
  hTS_I2C.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hTS_I2C.Init.OwnAddress1 = 0;
  hTS_I2C.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hTS_I2C.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hTS_I2C.Init.OwnAddress2 = 0;
  hTS_I2C.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hTS_I2C.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hTS_I2C) != HAL_OK)
  {
//    Error_Handler();
  }

}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/09
*******************************************************************************/
static void GPIO_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(TS_RESET_GPIO_Port, TS_RESET_Pin, GPIO_PIN_RESET);


    /*Configure GPIO pins */
    GPIO_InitStruct.Pin = TS_RESET_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(TS_RESET_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = TS_IRQ_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(TS_IRQ_GPIO_Port, &GPIO_InitStruct);
}


/*******************************************************************************
*    Public functions definitions
*******************************************************************************/

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/09
*******************************************************************************/
#pragma optimize = none
void KeyPad_ReadPanelState(TS_StateTypeDef * touchData, uint8_t* orientation)
{
    TouchPanel_Read_XY_Position((uint8_t *)&regMap);
    uint16_t touchDetect;
    uint16_t xTouch;
    uint16_t yTouch;
    uint16_t xOffset;
    uint16_t yOffset;

    if (TOUCHPANEL_X_SIZE > TOUCHPANEL_Y_SIZE)
    {
        *orientation = LANDSCAPE;
    }
    else
    {
        *orientation = PORTRAIT;
    }

    touchDetect = regMap.TD_Status;
    if (*orientation == PORTRAIT)
    {
        xTouch = ((regMap.P1_XH&0x0f) << 8) | regMap.P1_XL;
        yTouch = ((regMap.P1_YH&0x0f) << 8) | regMap.P1_YL;

        //Calculate and apply a "Centered Progressive Offset" to match the X Display position with the Touch position
        xOffset = (uint16_t)((float)abs(TOUCHPANEL_X_SIZE / 2 - xTouch) / X_PROGRESSIVE_OFFSET_RATIO);
        if (xTouch > TOUCHPANEL_X_SIZE / 2)
        {
            xTouch -= xOffset;
        }
        else
        {
            xTouch += xOffset;
        }
        //Calculate and apply a "Centered Progressive Offset" to match the Y Display position with the Touch position
        yOffset = (uint16_t)((float)abs(TOUCHPANEL_Y_SIZE / 2 - yTouch) / Y_PROGRESSIVE_OFFSET_RATIO);
        if (yTouch > TOUCHPANEL_X_SIZE / 2)
        {
            yTouch -= yOffset;
        }
        else
        {
            yTouch += yOffset;
        }
    }
    else
    {
        yTouch = ((regMap.P1_XH&0x0f) << 8) | regMap.P1_XL;
        xTouch = ((regMap.P1_YH&0x0f) << 8) | regMap.P1_YL;
    }

    touchData->TouchDetected = touchDetect;
    touchData->X = xTouch;
    touchData->Y = yTouch;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/09
*******************************************************************************/
void KeyPadInit(void)
{
    I2C_Init();
    GPIO_Init();

    Enable_TouchPanel();
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
