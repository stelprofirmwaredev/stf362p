/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    TP_FT6336U.c
* @date    2016/08/09
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>

#include ".\BSP\FT6336U\inc\TP_FT6336U.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/



/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/09
*******************************************************************************/
void TouchPanel_Read_XY_Position (uint8_t * buffer)
{
    uint8_t commandBuffer = FT6336U_GEST_ID;
    WRITE_TP_DATA(&commandBuffer, 1);
    READ_TP_DATA(buffer,FT6336U_LAST_ID);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/09
*******************************************************************************/
void TouchPanel_Init(void)
{
    uint8_t commandBuffer[2];
    static uint8_t buf_TH_GROUP;
    static uint8_t buf_TH_DIFF;
    static uint8_t buf_CTRL;
    static uint8_t buf_TIMEENTERMONITOR;
    static uint8_t buf_PERIODACTIVE;
    static uint8_t buf_PERIODMONITOR;
    static uint8_t buf_RADIAN_VALUE;
    static uint8_t buf_OFFSET_LEFT_RIGHT;
    static uint8_t buf_OFFSET_UP_DOWN;
    static uint8_t buf_DISTANCE_LEFT_RIGHT;
    static uint8_t buf_DISTANCE_UP_DOWN;
    static uint8_t buf_DISTANCE_ZOOM;
    static uint8_t buf_G_MODE;
    static uint8_t buf_PWR_MODE;
    static uint8_t buf_ID_G_VIRTUAL_KEY_THRES;
    static uint8_t buf_ID_G_FACTOR_Y_MODE;
    static uint8_t buf_ID_G_FACE_DEC_MODE;
    static uint8_t buf_STATE;
    static uint8_t buf_ID_G_SPEC_GESTRUE_ENABLE;
    static uint8_t buf_ID_G_SPEC_GESTURE_ENABLE_SIGN;

    RELEASE_RESET;
    commandBuffer[0] = FT6336U_TH_GROUP;
    WRITE_TP_DATA(commandBuffer, 1);
    READ_TP_DATA(&buf_TH_GROUP,1);
    commandBuffer[0] = FT6336U_TH_DIFF;
    WRITE_TP_DATA(commandBuffer, 1);
    READ_TP_DATA(&buf_TH_DIFF,1);
    commandBuffer[0] = FT6336U_CTRL;
    WRITE_TP_DATA(commandBuffer, 1);
    READ_TP_DATA(&buf_CTRL,1);
    commandBuffer[0] = FT6336U_TIMEENTERMONITOR;
    WRITE_TP_DATA(commandBuffer, 1);
    READ_TP_DATA(&buf_TIMEENTERMONITOR,1);
    commandBuffer[0] = FT6336U_PERIODACTIVE;
    WRITE_TP_DATA(commandBuffer, 1);
    READ_TP_DATA(&buf_PERIODACTIVE,1);
    commandBuffer[0] = FT6336U_PERIODMONITOR;
    WRITE_TP_DATA(commandBuffer, 1);
    READ_TP_DATA(&buf_PERIODMONITOR,1);
    commandBuffer[0] = FT6336U_RADIAN_VALUE;
    WRITE_TP_DATA(commandBuffer, 1);
    READ_TP_DATA(&buf_RADIAN_VALUE,1);
    commandBuffer[0] = FT6336U_OFFSET_LEFT_RIGHT;
    WRITE_TP_DATA(commandBuffer, 1);
    READ_TP_DATA(&buf_OFFSET_LEFT_RIGHT,1);
    commandBuffer[0] = FT6336U_OFFSET_UP_DOWN;
    WRITE_TP_DATA(commandBuffer, 1);
    READ_TP_DATA(&buf_OFFSET_UP_DOWN,1);
    commandBuffer[0] = FT6336U_DISTANCE_LEFT_RIGHT;
    WRITE_TP_DATA(commandBuffer, 1);
    READ_TP_DATA(&buf_DISTANCE_LEFT_RIGHT,1);
    commandBuffer[0] = FT6336U_DISTANCE_UP_DOWN;
    WRITE_TP_DATA(commandBuffer, 1);
    READ_TP_DATA(&buf_DISTANCE_UP_DOWN,1);
    commandBuffer[0] = FT6336U_DISTANCE_ZOOM;
    WRITE_TP_DATA(commandBuffer, 1);
    READ_TP_DATA(&buf_DISTANCE_ZOOM,1);
    commandBuffer[0] = FT6336U_G_MODE;
    WRITE_TP_DATA(commandBuffer, 1);
    READ_TP_DATA(&buf_G_MODE,1);
    commandBuffer[0] = FT6336U_PWR_MODE;
    WRITE_TP_DATA(commandBuffer, 1);
    READ_TP_DATA(&buf_PWR_MODE,1);
    commandBuffer[0] = FT6336U_ID_G_VIRTUAL_KEY_THRES;
    WRITE_TP_DATA(commandBuffer, 1);
    READ_TP_DATA(&buf_ID_G_VIRTUAL_KEY_THRES,1);
    commandBuffer[0] = FT6336U_ID_G_FACTOR_Y_MODE;
    WRITE_TP_DATA(commandBuffer, 1);
    READ_TP_DATA(&buf_ID_G_FACTOR_Y_MODE,1);
    commandBuffer[0] = FT6336U_ID_G_FACE_DEC_MODE;
    WRITE_TP_DATA(commandBuffer, 1);
    READ_TP_DATA(&buf_ID_G_FACE_DEC_MODE,1);
    commandBuffer[0] = FT6336U_STATE;
    WRITE_TP_DATA(commandBuffer, 1);
    READ_TP_DATA(&buf_STATE,1);
    commandBuffer[0] = FT6336U_ID_G_SPEC_GESTRUE_ENABLE;
    WRITE_TP_DATA(commandBuffer, 1);
    READ_TP_DATA(&buf_ID_G_SPEC_GESTRUE_ENABLE,1);
    commandBuffer[0] = FT6336U_ID_G_SPEC_GESTURE_ENABLE_SIGN;
    WRITE_TP_DATA(commandBuffer, 1);
    READ_TP_DATA(&buf_ID_G_SPEC_GESTURE_ENABLE_SIGN,1);
}

/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
