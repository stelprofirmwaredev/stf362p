/*******************************************************************************
* @file    TP_FT6336U.h
* @date    2016/08/09
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef _TP_FT6336U_H
#define _TP_FT6336U_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>

#ifdef __ICCARM__
    #include "stm32f4xx_hal.h"
#endif



/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define TOUCHPANEL_X_SIZE           480
#define TOUCHPANEL_Y_SIZE           320

#define TOUCHPANEL_I2C_ADDRESS          (0x70<<0)
#define TOUCHPANEL_DATA_SIZE            10
#define TOUCHPANEL_READ_TIMEOUT         100         //100ms         //(HAL_MAX_DELAY-1)


#ifdef __ICCARM__
    #define RELEASE_RESET           HAL_GPIO_WritePin(TS_RESET_GPIO_Port, TS_RESET_Pin, GPIO_PIN_SET)
    #define PULL_RESET              HAL_GPIO_WritePin(TS_RESET_GPIO_Port, TS_RESET_Pin, GPIO_PIN_RESET)

    extern I2C_HandleTypeDef hTS_I2C;


    #define WRITE_TP_DATA(buffer,size)       HAL_I2C_Master_Transmit(&hTS_I2C, \
                                                        TOUCHPANEL_I2C_ADDRESS, \
                                                        buffer, \
                                                        size, \
                                                        TOUCHPANEL_READ_TIMEOUT)
    #define READ_TP_DATA(buffer,size)       HAL_I2C_Master_Receive(&hTS_I2C, \
                                                        TOUCHPANEL_I2C_ADDRESS, \
                                                        buffer, \
                                                        size, \
                                                        TOUCHPANEL_READ_TIMEOUT)
#else                       //#ifdef __ICCARM__
    #define RELEASE_RESET
    #define PULL_RESET
    #define WRITE_TP_DATA(buffer,size)
    #define READ_TP_DATA(buffer,size)
#endif



/*******************************************************************************
* Public structures definitions
*******************************************************************************/

/************************************************************/
/*  Enum that describe the register IDs of the FT6336U      */
/************************************************************/
typedef enum
{
    FT6336U_GEST_ID = 0x01,
    FT6336U_TD_STATUS,
    FT6336U_P1XH,
    FT6336U_P1XL,
    FT6336U_P1YH,
    FT6336U_P1YL,
    FT6336U_P1_WEIGHT,
    FT6336U_P1_MISC,
    FT6336U_P2XH,
    FT6336U_P2XL,
    FT6336U_P2YH,
    FT6336U_P2YL,
    FT6336U_P2_WEIGHT,
    FT6336U_P2_MISC,

    FT6336U_LAST_ID,

    FT6336U_TH_GROUP = 0x80,
    FT6336U_TH_DIFF = 0x85,
    FT6336U_CTRL = 0x86,
    FT6336U_TIMEENTERMONITOR = 0x87,
    FT6336U_PERIODACTIVE = 0x88,
    FT6336U_PERIODMONITOR = 0x89,
    FT6336U_RADIAN_VALUE = 0x91,
    FT6336U_OFFSET_LEFT_RIGHT = 0x92,
    FT6336U_OFFSET_UP_DOWN = 0x93,
    FT6336U_DISTANCE_LEFT_RIGHT = 0x94,
    FT6336U_DISTANCE_UP_DOWN = 0x95,
    FT6336U_DISTANCE_ZOOM = 0x97,
    FT6336U_G_MODE = 0xA4,
    FT6336U_PWR_MODE = 0xA5,
    FT6336U_ID_G_VIRTUAL_KEY_THRES = 0xA9,
    FT6336U_ID_G_FACTOR_Y_MODE = 0xAE,
    FT6336U_ID_G_FACE_DEC_MODE = 0xB0,
    FT6336U_STATE = 0xBC,
    FT6336U_ID_G_SPEC_GESTRUE_ENABLE = 0xD0,
    FT6336U_ID_G_SPEC_GESTURE_ENABLE_SIGN = 0xD1,
} FT633U_regId_t;

/************************************************************/
/*  Enum that describe the events of a touch action         */
/************************************************************/
typedef enum
{
    FT6336_EVENT_PRESS = 0x00,
    FT6336_EVENT_RELEASE,
    FT6336_EVENT_HOLD,
    FT6336_EVENT_NO_EVENT,
} FT6336U_event_t;

/************************************************************/
/*  registers structure of the FT6336U      */
/************************************************************/
typedef struct
{
    uint8_t Gesture_ID;
    uint8_t TD_Status;                  //bits [3:0] = Number of touch points
    uint8_t P1_XH;                      //bits [7:6] = Event flag; bits [3:0] = X pos MSB
    uint8_t P1_XL;
    uint8_t P1_YH;                      //bits [7:4] = Touch ID; bits [3:0] = Y pos MSB
    uint8_t P1_YL;
    uint8_t P1_Weight;
    uint8_t P1_Misc;                    //bits [7:4] = Touch Area
    uint8_t P2_XH;                      //bits [7:6] = Event flag; bits [3:0] = X pos MSB
    uint8_t P2_XL;
    uint8_t P2_YH;                      //bits [7:4] = Touch ID; bits [3:0] = Y pos MSB
    uint8_t P2_YL;
    uint8_t P2_Weight;
    uint8_t P2_Misc;                    //bits [7:4] = Touch Area
} FTxxxx_reg_t;


/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void TouchPanel_Init(void);
void TouchPanel_Read_XY_Position (uint8_t * buffer);

#endif /* _TP_FT6336U_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
