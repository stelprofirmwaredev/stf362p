/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2017, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    TP_FT7401.c
* @date    2017/02/01
* @authors Jean-Fran�ois Many
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>

#include ".\BSP\FT7401\inc\TP_FT7401.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/



/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
uint32_t readCount = 0;
/*******************************************************************************
* @brief  Touch Panel Read Register function (over I2C)
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/02/01
*******************************************************************************/
void TouchPanel_Read_XY_Position (uint8_t * buffer)
{
    readCount++;
    uint8_t commandBuffer = FT7401_GEST_ID;
    WRITE_TP_DATA(&commandBuffer, 1);
    READ_TP_DATA(buffer,FT7401_LAST_ID);
}

/*******************************************************************************
* @brief  TouchPanel Init function
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/02/01
*******************************************************************************/
void TouchPanel_Init(void)
{
    uint8_t commandBuffer[2];

    RELEASE_RESET;

    //Set the config for Touch Panel as per the Touch we had with the devkits
    commandBuffer[0] = FT7401_TH_GROUP;
    commandBuffer[1] = 0x1E;
    WRITE_TP_DATA(commandBuffer, 2);

    commandBuffer[0] = FT7401_TH_DIFF;
    commandBuffer[1] = 0xA0;
    WRITE_TP_DATA(commandBuffer, 2);

    commandBuffer[0] = FT7401_ID_G_VIRTUAL_KEY_THRES;
    commandBuffer[1] = 0x0F;
    WRITE_TP_DATA(commandBuffer, 2);

    commandBuffer[0] = FT7401_PERIODACTIVE;
    commandBuffer[1] = 0x0C;
    WRITE_TP_DATA(commandBuffer, 2);

    commandBuffer[0] = FT7401_PERIODMONITOR;
    commandBuffer[1] = 0x28;
    WRITE_TP_DATA(commandBuffer, 2);

    commandBuffer[0] = FT7401_ID_G_SPEC_GESTURE_ENABLE_SIGN;
    commandBuffer[1] = 0xFF;
    WRITE_TP_DATA(commandBuffer, 2);
}

/** Copyright(C) 2017 Stelpro Design, All Rights Reserved**/
