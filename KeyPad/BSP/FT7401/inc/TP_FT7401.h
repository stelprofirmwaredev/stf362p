/*******************************************************************************
* @file    TP_FT7401.h
* @date    2017/01/02
* @authors Jean-Fran�ois Many
* @brief
*******************************************************************************/

#ifndef _TP_FT7401_H
#define _TP_FT7401_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>

#ifdef __ICCARM__
    #include "stm32f4xx_hal.h"
#endif



/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define TOUCHPANEL_X_SIZE           320
#define TOUCHPANEL_Y_SIZE           480

#define TOUCHPANEL_I2C_ADDRESS          (0x70<<0)
#define TOUCHPANEL_DATA_SIZE            10
#define TOUCHPANEL_READ_TIMEOUT         100         //100ms         //(HAL_MAX_DELAY-1)


#ifdef __ICCARM__
    #define RELEASE_RESET           HAL_GPIO_WritePin(TS_RESET_GPIO_Port, TS_RESET_Pin, GPIO_PIN_SET)
    #define PULL_RESET              HAL_GPIO_WritePin(TS_RESET_GPIO_Port, TS_RESET_Pin, GPIO_PIN_RESET)

    extern I2C_HandleTypeDef hTS_I2C;


    #define WRITE_TP_DATA(buffer,size)       HAL_I2C_Master_Transmit(&hTS_I2C, \
                                                        TOUCHPANEL_I2C_ADDRESS, \
                                                        buffer, \
                                                        size, \
                                                        TOUCHPANEL_READ_TIMEOUT)
    #define READ_TP_DATA(buffer,size)       HAL_I2C_Master_Receive(&hTS_I2C, \
                                                        TOUCHPANEL_I2C_ADDRESS, \
                                                        buffer, \
                                                        size, \
                                                        TOUCHPANEL_READ_TIMEOUT)
#else                       //#ifdef __ICCARM__
    #define RELEASE_RESET
    #define PULL_RESET
    #define WRITE_TP_DATA(buffer,size)
    #define READ_TP_DATA(buffer,size)
#endif



/*******************************************************************************
* Public structures definitions
*******************************************************************************/

/************************************************************/
/*  Enum that describe the register IDs of the FT7401       */
/************************************************************/
typedef enum
{
    FT7401_GEST_ID = 0x01,
    FT7401_TD_STATUS,
    FT7401_P1XH,
    FT7401_P1XL,
    FT7401_P1YH,
    FT7401_P1YL,
    FT7401_P1_WEIGHT,
    FT7401_P1_MISC,
    FT7401_P2XH,
    FT7401_P2XL,
    FT7401_P2YH,
    FT7401_P2YL,
    FT7401_P2_WEIGHT,
    FT7401_P2_MISC,

    FT7401_LAST_ID,

    FT7401_TH_GROUP = 0x80,
    FT7401_TH_DIFF = 0x85,
    FT7401_CTRL = 0x86,
    FT7401_TIMEENTERMONITOR = 0x87,
    FT7401_PERIODACTIVE = 0x88,
    FT7401_PERIODMONITOR = 0x89,
    FT7401_RADIAN_VALUE = 0x91,
    FT7401_OFFSET_LEFT_RIGHT = 0x92,
    FT7401_OFFSET_UP_DOWN = 0x93,
    FT7401_DISTANCE_LEFT_RIGHT = 0x94,
    FT7401_DISTANCE_UP_DOWN = 0x95,
    FT7401_DISTANCE_ZOOM = 0x97,
    FT7401_G_MODE = 0xA4,
    FT7401_PWR_MODE = 0xA5,
    FT7401_ID_G_VIRTUAL_KEY_THRES = 0xA9,
    FT7401_ID_G_FACTOR_Y_MODE = 0xAE,
    FT7401_ID_G_FACE_DEC_MODE = 0xB0,
    FT7401_STATE = 0xBC,
    FT7401_ID_G_SPEC_GESTRUE_ENABLE = 0xD0,
    FT7401_ID_G_SPEC_GESTURE_ENABLE_SIGN = 0xD1,
} FT7401_regId_t;

/************************************************************/
/*  Enum that describe the events of a touch action         */
/************************************************************/
typedef enum
{
    FT7401_EVENT_PRESS = 0x00,
    FT7401_EVENT_RELEASE,
    FT7401_EVENT_HOLD,
    FT7401_EVENT_NO_EVENT,
} FT7401_event_t;

/************************************************************/
/*  registers structure of the FT7401                       */
/************************************************************/
typedef struct
{
    uint8_t Gesture_ID;
    uint8_t TD_Status;                  //bits [3:0] = Number of touch points
    uint8_t P1_XH;                      //bits [7:6] = Event flag; bits [3:0] = X pos MSB
    uint8_t P1_XL;
    uint8_t P1_YH;                      //bits [7:4] = Touch ID; bits [3:0] = Y pos MSB
    uint8_t P1_YL;
    uint8_t P1_Weight;
    uint8_t P1_Misc;                    //bits [7:4] = Touch Area
    uint8_t P2_XH;                      //bits [7:6] = Event flag; bits [3:0] = X pos MSB
    uint8_t P2_XL;
    uint8_t P2_YH;                      //bits [7:4] = Touch ID; bits [3:0] = Y pos MSB
    uint8_t P2_YL;
    uint8_t P2_Weight;
    uint8_t P2_Misc;                    //bits [7:4] = Touch Area
} FTxxxx_reg_t;


/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void TouchPanel_Init(void);
void TouchPanel_Read_XY_Position (uint8_t * buffer);

#endif /* _TP_FT7401_H */
/** Copyright(C) 2017 Stelpro Design, All Rights Reserved**/
