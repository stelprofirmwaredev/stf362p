/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2016 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "cmsis_os.h"

/* USER CODE BEGIN Includes */

#include ".\BSP\SDRAM\IS42S16100H\inc\IS42S16100H.h"
#include ".\HAL\inc\HAL_Flash.h"
#include ".\HAL\inc\HAL_WatchDog.h"
#include ".\HAL\inc\HAL_Console.h"
#include ".\HAL\inc\HAL_PowerManagement.h"

#include ".\Console\inc\THREAD_Console.h"
#include ".\Default\inc\THREAD_Default.h"
#include ".\Hmi\inc\THREAD_HMI.h"
#include ".\Sensors\inc\THREAD_Sensors.h"
#include ".\WiFi\inc\THREAD_WiFi.h"
#include ".\ZigBee\inc\THREAD_ZigBee.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\Load Management\inc\THREAD_LoadManagement.h"
#include "SystemClocks.h"

#include "HAL_ZigBeeInterface.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
SDRAM_HandleTypeDef hsdram1;
DMA2D_HandleTypeDef hdma2d;
TIM_HandleTypeDef htim2;
char* otaChunksChannel;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
static uint8_t ResetCause;
__no_init char ExceptionDiag[100];
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void Error_Handler(void);
static void MX_GPIO_Init(void);
static void MX_DMA2D_Init(void);
static void MX_TIM2_Init(void);
void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
static void DetermineResetCause (void);
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
/* USER CODE END 0 */

int main(void)
{
    DetermineResetCause();

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();
    HAL_EnableCompensationCell();

  /* Configure the system clock */
    SystemClock_Config();
    PowerManagement_Init(); //new
    
  /* Initialize all configured peripherals */
    MX_GPIO_Init();
    MX_DMA2D_Init();
    MX_TIM2_Init();

    HAL_FlashInit();

    WatchDogInit();

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
vTraceInitTraceData();
otaChunksChannel = vTraceStoreUserEventChannelName("OTA Chunks");

    InitThread_Default();
    InitThread_DB();
    InitThread_Console();

   /* definition and creation of hmiTask */
#ifndef PROXIMITY_TEST
    InitThread_HMI();
#endif

    /* definition and creation of sensorTask */
    InitThread_Sensor();

    /* definition and creation of loadTask */
    InitThread_LoadManagement();



    CONSOLE_EVENT_MESSAGE("Application Boot");

    /* Start scheduler */
    osKernelStart();

    while (1)
    {
    }
}

/** System Clock Configuration
*/



/* DMA2D init function */
static void MX_DMA2D_Init(void)
{
__DMA2D_CLK_ENABLE();
  hdma2d.Instance = DMA2D;
  hdma2d.Init.Mode = DMA2D_M2M;
  hdma2d.Init.ColorMode = DMA2D_OUTPUT_RGB565;
  hdma2d.Init.OutputOffset = 0;
  hdma2d.LayerCfg[1].InputOffset = 0;
  hdma2d.LayerCfg[1].InputColorMode = DMA2D_INPUT_RGB565;
  hdma2d.LayerCfg[1].AlphaMode = DMA2D_NO_MODIF_ALPHA;
  hdma2d.LayerCfg[1].InputAlpha = 0;
  if (HAL_DMA2D_Init(&hdma2d) != HAL_OK)
  {
    Error_Handler();
  }

  if (HAL_DMA2D_ConfigLayer(&hdma2d, 1) != HAL_OK)
  {
    Error_Handler();
  }

}

/* TIM2 init function */
static void MX_TIM2_Init(void)
{
  __TIM2_CLK_ENABLE();
  __GPIOA_CLK_ENABLE();

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;
//  TIM_OC_InitTypeDef sConfigOC;
//  TIM_Base_InitTypeDef sBaseConfig;

  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 2812;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 0;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }

//  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
//  {
//    Error_Handler();
//  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }

//  sConfigOC.OCMode = TIM_OCMODE_PWM1;
//  sConfigOC.Pulse = 687;
//  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
//  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
//  sConfigOC.OCFastMode = TIM_OCFAST_ENABLE;
//  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
//  {
//    Error_Handler();
//  }
//
//  sBaseConfig.ClockDivision = TIM_CLOCKDIVISION_DIV1;
//  sBaseConfig.CounterMode = TIM_COUNTERMODE_UP;
//  sBaseConfig.Period = 1373;
//  sBaseConfig.Prescaler = 1;
//  TIM_Base_SetConfig(htim2.Instance, &sBaseConfig);
//
//  HAL_TIM_MspPostInit(&htim2);
//
//  HAL_TIM_Base_Start(&htim2);
//  HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
}

/** Configure pins as
        * Analog
        * Input
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */

  /*Configure GPIO pin Output Level */

  /*Configure GPIO pin Output Level */
#ifdef DEVKIT_PLATFORM
  HAL_GPIO_WritePin(GPIOA, LED_ORANGE_Pin|LED_GREEN_Pin, GPIO_PIN_SET);
#endif /* DEVKIT_PLATFORM */
#ifdef PROXIMITY_TEST
  HAL_GPIO_WritePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin, GPIO_PIN_SET);
#endif

  /*Configure GPIO pin Output Level */
#ifdef DEVKIT_PLATFORM
  HAL_GPIO_WritePin(GPIOC, LED_RED_Pin|T_ACQ_ENABLE_Pin, GPIO_PIN_SET);
#else
  HAL_GPIO_WritePin(GPIOC, T_ACQ_ENABLE_Pin, GPIO_PIN_SET);
#endif /* DEVKIT_PLATFORM */

  /*Configure GPIO pin Output Level */



  /*Configure GPIO pins : BTN_UP_Pin BTN_LEFT_Pin */
//  GPIO_InitStruct.Pin = BTN_UP_Pin;
//  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
//  GPIO_InitStruct.Pull = GPIO_NOPULL;
//  GPIO_InitStruct.Alternate = GPIO_AF1_
//  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

#ifdef DEVKIT_PLATFORM
  /*Configure GPIO pins : BTN_UP_Pin BTN_LEFT_Pin */
  GPIO_InitStruct.Pin = BTN_LEFT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
#endif /* DEVKIT_PLATFORM */

  /*Configure GPIO pin : FLASH_SLAVE_SELECT_Pin */

#ifdef DEVKIT_PLATFORM
  /*Configure GPIO pin : PROXIMITY_DETECT_Pin */
  GPIO_InitStruct.Pin = PROXIMITY_DETECT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(PROXIMITY_DETECT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LED_ORANGE_Pin LED_GREEN_Pin WIFI_RST_Pin */
  GPIO_InitStruct.Pin = LED_ORANGE_Pin|LED_GREEN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : LED_RED_Pin T_ACQ_ENABLE_Pin */
  GPIO_InitStruct.Pin = LED_RED_Pin|T_ACQ_ENABLE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : BTN_RIGHT_Pin TS_IRQ_Pin */
  GPIO_InitStruct.Pin = BTN_RIGHT_Pin|TS_IRQ_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : BTN_ENTER_Pin */
  GPIO_InitStruct.Pin = BTN_ENTER_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(BTN_ENTER_GPIO_Port, &GPIO_InitStruct);
#else
  /*Configure GPIO pins : T_ACQ_ENABLE_Pin */
  GPIO_InitStruct.Pin = T_ACQ_ENABLE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : TS_IRQ_Pin */
  GPIO_InitStruct.Pin = TS_IRQ_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
#endif /* DEVKIT_PLATFORM */

#ifdef PROXIMITY_TEST
  /*Configure GPIO pins : LED_BLUE_Pin */
  GPIO_InitStruct.Pin = LED_BLUE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED_BLUE_GPIO_Port, &GPIO_InitStruct);
#endif
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2018/09/13
*******************************************************************************/
static void DetermineResetCause (void)
{
    ResetCause = 0;
    if (__HAL_RCC_GET_FLAG(RCC_FLAG_BORRST) != 0)
    {
        ResetCause |= 0x01;
    }
    if (__HAL_RCC_GET_FLAG(RCC_FLAG_PINRST) != 0)
    {
        ResetCause |= 0x02;
    }
    if (__HAL_RCC_GET_FLAG(RCC_FLAG_PORRST) != 0)
    {
        ResetCause |= 0x04;
    }
    if (__HAL_RCC_GET_FLAG(RCC_FLAG_SFTRST) != 0)
    {
        ResetCause |= 0x08;
    }
    if (__HAL_RCC_GET_FLAG(RCC_FLAG_IWDGRST) != 0)
    {
        ResetCause |= 0x10;
    }
    if (__HAL_RCC_GET_FLAG(RCC_FLAG_WWDGRST) != 0)
    {
        ResetCause |= 0x20;
    }
    if (__HAL_RCC_GET_FLAG(RCC_FLAG_LPWRRST) != 0)
    {
        ResetCause |= 0x40;
    }

    __HAL_RCC_CLEAR_RESET_FLAGS();
}

/*******************************************************************************
* @brief
* @inputs None
* @retval uint8_t ResetCause
* @author Jean-Fran�ois Many
* @date   2018/09/13
*******************************************************************************/
uint8_t GetResetCause(void)
{
    return ResetCause;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2019/06/07
*******************************************************************************/
void ClearResetCause(void)
{
    ResetCause = 0;
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler */
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */


#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */

/**
  * @}
*/

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
