/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  *
  * COPYRIGHT(c) 2016 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include ".\DB\inc\THREAD_DB.h"

/* USER CODE BEGIN Includes */
#include <string.h>
/* USER CODE END Includes */

/* Variables -----------------------------------------------------------------*/

/* USER CODE BEGIN Variables */
extern char ExceptionDiag[];
extern dbType_ThisThermostat_t thisThermostat;
static char rtos_str[9];
static uint8_t i_rtos;
static uint8_t rtos_chksum;
static uint8_t * rtos_chksumPointer;
/* USER CODE END Variables */

/* Function prototypes -------------------------------------------------------*/

/* USER CODE BEGIN FunctionPrototypes */
void vApplicationStackOverflowHook( TaskHandle_t xTask,
                                    signed char *pcTaskName );


/* USER CODE END FunctionPrototypes */

/* Hook prototypes */

/* USER CODE BEGIN Application */

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void vApplicationStackOverflowHook( TaskHandle_t xTask,
                                    signed char *pcTaskName )
{
    for (i_rtos = 0; i_rtos < 100; i_rtos++)
    {
        ExceptionDiag[i_rtos] = 0;
    }
    strcpy(ExceptionDiag, "StackOverflow: TaskName: ");
    strncat(ExceptionDiag, (char const*)pcTaskName, strlen((char const*)pcTaskName));
    strcat(ExceptionDiag, ", Address: 0x");
    sprintf((char*)rtos_str,"%x",xTask);
    strncat(ExceptionDiag, (char const*)rtos_str, strlen(rtos_str));

    rtos_chksumPointer = (uint8_t *)ExceptionDiag;

    rtos_chksum = 0xF0;
    for (i_rtos = 0; i_rtos < (strlen(ExceptionDiag)); i_rtos++)
    {
        rtos_chksum += rtos_chksumPointer[i_rtos];
    }
    sprintf((char*)rtos_str," 0x%02x", rtos_chksum);
    strncat(ExceptionDiag, (char const*)rtos_str, strlen(rtos_str));
    NVIC_SystemReset();
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void vApplicationMallocFailedHook(void)
{
    asm("nop");
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void OutOfMemory(void)
{
    for (i_rtos = 0; i_rtos < 100; i_rtos++)
    {
        ExceptionDiag[i_rtos] = 0;
    }
    strcpy(ExceptionDiag, "Reset due to Out Of Memory");

    rtos_chksumPointer = (uint8_t *)ExceptionDiag;

    rtos_chksum = 0xF0;
    for (i_rtos = 0; i_rtos < (strlen(ExceptionDiag)); i_rtos++)
    {
        rtos_chksum += rtos_chksumPointer[i_rtos];
    }
    sprintf((char*)rtos_str," 0x%02x", rtos_chksum);
    strncat(ExceptionDiag, (char const*)rtos_str, strlen(rtos_str));
    NVIC_SystemReset();
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void NoWeatherFeed(void)
{
    for (i_rtos = 0; i_rtos < 100; i_rtos++)
    {
        ExceptionDiag[i_rtos] = 0;
    }
    strcpy(ExceptionDiag, "Reset due to Weather Feed Timeout");

    rtos_chksumPointer = (uint8_t *)ExceptionDiag;

    rtos_chksum = 0xF0;
    for (i_rtos = 0; i_rtos < (strlen(ExceptionDiag)); i_rtos++)
    {
        rtos_chksum += rtos_chksumPointer[i_rtos];
    }
    sprintf((char*)rtos_str," 0x%02x", rtos_chksum);
    strncat(ExceptionDiag, (char const*)rtos_str, strlen(rtos_str));
    NVIC_SystemReset();
}


/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
