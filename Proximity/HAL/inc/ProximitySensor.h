/*******************************************************************************
* @file    HAL_ProximitySensor.h
* @date    2016/09/07
* @authors Jean-Fran�ois Many
* @brief
*******************************************************************************/

#ifndef HAL_ProximitySensor_H
#define HAL_ProximitySensor_H


/*******************************************************************************
* Includes
*******************************************************************************/


/*******************************************************************************
* Public constants definitions
*******************************************************************************/



/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void ProximitySensor_Init(void);
void HAL_ProximitySensorMonitoring(void);
U8BIT HAL_Proximity_GetPresence(void);
void HAL_Proximity_Recalibrate(void);
U16BIT HAL_Proximity_GetMinADCCount(void);
U16BIT HAL_Proximity_GetMaxADCCount(void);
void HAL_Proximity_ResetMinMaxADCCount(void);

#endif /* HAL_ProximitySensor_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
