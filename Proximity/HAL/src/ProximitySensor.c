/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/******************************************************************************
* @file    HAL_ProximitySensor.c
* @date    2016/09/07
* @authors Jean-Fran�ois Many
* @brief   Proximity Sensor module
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include "typedef.h"
#include "HAL_Interrupt.h"
#include "stm32f4xx_hal.h"
#include ".\HAL\inc\ProximitySensor.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\Console\inc\THREAD_Console.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#ifdef DEVKIT_PLATFORM
    #define I2C_CLOCK_SPEED         100000
    #define OWN_ADDRESS             0x01
    #define SENSOR_ADDRESS          0x39<<1 //Left shift 1 bit to make room for R/W bit (7-bit addressing)
    #define PS_HYSTERESIS           5
    #define PROXIMITY_READ_TIMEOUT  100     //100ms     //(HAL_MAX_DELAY-1)
#else
    #define ACQ_MAX                 1
    #ifdef BIS_CONTROLLER
        #define PROXIMITY_CHANNEL       ADC_CHANNEL_14
    #else
        #define PROXIMITY_CHANNEL       ADC_CHANNEL_2
    #endif

    #define PROXIMITY_READ_TIMEOUT (HAL_MAX_DELAY-1)
#endif /* DEVKIT_PLATFORM */

//Assuming an idle state at 50% of ADC (50% of 4095 = 2048)
#define PROXIMITY_HYSTERESIS_HIGH_SENSITIVITY   205 //205 / 2048 = 10% drop of signal
#define PROXIMITY_HYSTERESIS_MEDIUM_SENSITIVITY 512 //512 / 2048 = 25% drop of signal
#define PROXIMITY_HYSTERESIS_LOW_SENSITIVITY    683 //683 / 2048 = 33% drop of signal (Original Tux setting)

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
static U8BIT PresenceState;
static U16BIT PIR_Min_Count;
static U16BIT PIR_Max_Count;
#ifdef DEVKIT_PLATFORM
static I2C_HandleTypeDef Proximity_I2C;
static U8BIT ProximityCalibrationRequested;
static U8BIT first_time;
#else
static ADC_HandleTypeDef Proximity_ADC;
static S16BIT ProximitySensorReading;
static S16BIT average_proximity = 0;
#endif /* DEVKIT_PLATFORM */

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/


/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
#ifdef DEVKIT_PLATFORM
static void I2C_Init(void);
#endif /* DEVKIT_PLATFORM */
static void HAL_Proximity_SetPresence(uint8_t state);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/08/09
*******************************************************************************/
#ifdef DEVKIT_PLATFORM
static void I2C_Init(void)
{
    Proximity_I2C.Instance = I2C3;
    Proximity_I2C.Init.ClockSpeed = I2C_CLOCK_SPEED;
    Proximity_I2C.Init.DutyCycle = I2C_DUTYCYCLE_2;
    Proximity_I2C.Init.OwnAddress1 = OWN_ADDRESS;
    Proximity_I2C.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
    Proximity_I2C.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
    Proximity_I2C.Init.OwnAddress2 = 0;
    Proximity_I2C.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
    Proximity_I2C.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
    HAL_I2C_Init(&Proximity_I2C);
    ProximityCalibrationRequested = TRUE;
    first_time = TRUE;
}
#endif /* DEVKIT_PLATFORM */

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  Initalize the proximity sensor
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/09/07
*******************************************************************************/
void ProximitySensor_Init(void)
{
#ifdef DEVKIT_PLATFORM
    I2C_Init();
#else
    PresenceState = TRUE;

    /**Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
    */
    Proximity_ADC.Instance = ADC1;
    Proximity_ADC.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
    Proximity_ADC.Init.Resolution = ADC_RESOLUTION_12B;
    Proximity_ADC.Init.ScanConvMode = DISABLE;
    Proximity_ADC.Init.ContinuousConvMode = DISABLE;
    Proximity_ADC.Init.DiscontinuousConvMode = DISABLE;
    Proximity_ADC.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
    Proximity_ADC.Init.DataAlign = ADC_DATAALIGN_RIGHT;
    Proximity_ADC.Init.NbrOfConversion = 1;
    Proximity_ADC.Init.DMAContinuousRequests = DISABLE;
    Proximity_ADC.Init.EOCSelection = ADC_EOC_SINGLE_CONV;

    HAL_ADC_MspInit(&Proximity_ADC);

    HAL_ADC_Init(&Proximity_ADC);
#endif /* DEVKIT_PLATFORM */
    HAL_Proximity_ResetMinMaxADCCount();
}
#ifdef DEVKIT_PLATFORM
/*******************************************************************************
* @brief  Proximity Sensor monitoring
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/09/07
*******************************************************************************/
void HAL_ProximitySensorMonitoring(void)
{
    S16BIT proximity;
    static S16BIT idle_proximity;
    U8BIT buffer[2];
    static HAL_StatusTypeDef statusProx = HAL_OK;

    if (first_time == TRUE)
    {
        buffer[0] = 0x3F;
        buffer[1] = 0x06;
        statusProx |= HAL_I2C_Mem_Write(&Proximity_I2C, SENSOR_ADDRESS, 0x42, 1, &buffer[0], 1, PROXIMITY_READ_TIMEOUT);
        statusProx |= HAL_I2C_Mem_Write(&Proximity_I2C, SENSOR_ADDRESS, 0x41, 1, &buffer[1], 1, PROXIMITY_READ_TIMEOUT);
    }
    first_time = FALSE;

    statusProx |= HAL_I2C_Mem_Read(&Proximity_I2C, SENSOR_ADDRESS, 0x45, 1, &buffer[0], 1, PROXIMITY_READ_TIMEOUT);
    statusProx |= HAL_I2C_Mem_Read(&Proximity_I2C, SENSOR_ADDRESS, 0x44, 1, &buffer[1], 1, PROXIMITY_READ_TIMEOUT);
    if (statusProx == HAL_OK)
    {
        proximity = (buffer[0] << 8) | buffer[1];

        if (ProximityCalibrationRequested == TRUE)
        {
            idle_proximity = proximity;
        }
        ProximityCalibrationRequested = FALSE;

        if (proximity > (idle_proximity + PS_HYSTERESIS))
        {
            HAL_Proximity_SetPresence(TRUE);
            DBTHRD_SetData(DBTYPE_USER_ACTION,(void*)0, INDEX_DONT_CARE, DBCHANGE_WAKEUP_FROM_SLEEP);
#ifdef PROXIMITY_TEST
            HAL_GPIO_WritePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin, GPIO_PIN_SET);
#endif
        }
        else
        {
            HAL_Proximity_SetPresence(FALSE);
#ifdef PROXIMITY_TEST
            HAL_GPIO_WritePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin, GPIO_PIN_RESET);
#endif
        }
        idle_proximity = idle_proximity + (proximity - idle_proximity) / 5;
    }
    else
    {
        ProximitySensor_Init();
    }
}
#else
/*******************************************************************************
* @brief  Proximity Sensor monitoring
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/09/07
*******************************************************************************/
#pragma optimize = none
void HAL_ProximitySensorMonitoring(void)
{
    ADC_ChannelConfTypeDef sConfig;
    HAL_StatusTypeDef status = HAL_OK;

    /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
    */
    sConfig.Channel = PROXIMITY_CHANNEL;
    sConfig.Rank = 1;
    sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
    HAL_ADC_ConfigChannel(&Proximity_ADC, &sConfig);
    HAL_ADC_Start(&Proximity_ADC);
    status = HAL_ADC_PollForConversion(&Proximity_ADC, PROXIMITY_READ_TIMEOUT);
    if (status == HAL_OK)
    {
        HAL_ADC_GetValue(&Proximity_ADC);
        ProximitySensorReading = Proximity_ADC.Instance->DR;
    }
    else
    {
        asm("nop");
    }
    HAL_ADC_Stop(&Proximity_ADC);

    if (average_proximity == 0)
    {
        average_proximity = ProximitySensorReading;
    }
    else
    {
        average_proximity = average_proximity + (ProximitySensorReading - average_proximity) / 20;
    }

    if ((ProximitySensorReading < (average_proximity - PROXIMITY_HYSTERESIS_MEDIUM_SENSITIVITY)) || (ProximitySensorReading > (average_proximity + PROXIMITY_HYSTERESIS_MEDIUM_SENSITIVITY)))
    {
        HAL_Proximity_SetPresence(TRUE);
        DBTHRD_SetData(DBTYPE_USER_ACTION,(void*)0, INDEX_DONT_CARE, DBCHANGE_WAKEUP_FROM_SLEEP);
    }
    else
    {
        HAL_Proximity_SetPresence(FALSE);
    }

    if (ProximitySensorReading < PIR_Min_Count)
    {
        PIR_Min_Count = ProximitySensorReading;
    }

    if (ProximitySensorReading > PIR_Max_Count)
    {
        PIR_Max_Count = ProximitySensorReading;
    }
}
#endif /* DEVKIT_PLATFORM */

/*******************************************************************************
* @brief  Get Presence State
* @inputs None
* @retval U8BIT: PresenceState (TRUE/FALSE)
* @author Jean-Fran�ois Many
* @date   2016/09/07
*******************************************************************************/
U8BIT HAL_Proximity_GetPresence(void)
{
    return PresenceState;
}

/*******************************************************************************
* @brief  Force Proximity sensor recalibration
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/13
*******************************************************************************/
void HAL_Proximity_Recalibrate(void)
{
#ifdef DEVKIT_PLATFORM
    ProximityCalibrationRequested = TRUE;
#endif /* DEVKIT_PLATFORM */
}

/*******************************************************************************
* @brief  Set Presence State (and latched presence state)
* @inputs None
* @retval U8BIT: PresenceState (TRUE/FALSE)
* @author Jean-Fran�ois Many
* @date   2017/04/25
*******************************************************************************/
static void HAL_Proximity_SetPresence(uint8_t state)
{
    if ((PresenceState != state) && (state == TRUE))
    {
        CONSOLE_EVENT_MESSAGE("Proximity detected");
    }

    PresenceState = state;
}

U16BIT HAL_Proximity_GetMinADCCount(void)
{
    return PIR_Min_Count;
}

U16BIT HAL_Proximity_GetMaxADCCount(void)
{
    return PIR_Max_Count;
}

void HAL_Proximity_ResetMinMaxADCCount(void)
{
    PIR_Min_Count = 0xFFFF;
    PIR_Max_Count = 0;
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
