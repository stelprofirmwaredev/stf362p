/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    DB_OTAU.c
* @date    2016/09/20
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <string.h>
#include ".\DB\inc\DB_OTAU.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\Console\inc\THREAD_Console.h"
#include "..\..\common\crc\inc\CRC_Utility.h"

//#include ".\DB\inc\THREAD_DBDataTypes.h"

#include ".\HAL\inc\HAL_Flash.h"

#include "FlashLayout.h"

#include "stm32f4xx_hal.h"


#include "trcRecorder.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define EXTFLASH_OTAU_STARTADRESS               FLASH_BASE_INACTIVE_IMAGE


/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
static uint32_t OtauImageSize;
static uint8_t chunksBuffer[512];
static uint32_t dataCnt;
struct DataPointers
{
    uint16_t poR;
    uint16_t poW;
} dataPointers;

static uint32_t blockAddressStart = EXTFLASH_HOST_IMAGE_B_START;
static uint32_t currentExternalFlashAddress;

uint8_t NewImageDownloadComplete;


extern char* otaChunksChannel;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
void InitDbOtauModule (void)
{
    OtauImageSize = 0;
    dataCnt = 0;
    NewImageDownloadComplete = 0;
    blockAddressStart = EXTFLASH_HOST_IMAGE_B_START;
    currentExternalFlashAddress = blockAddressStart;

    vTracePrint(otaChunksChannel, "OTA Init");
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/21
*******************************************************************************/
void StartMcuOtauSession (uint32_t otauSize)
{
    loaderStates_t bootLoaderState = GetBootLoaderState();

    CONSOLE_EVENT_MESSAGE ("New OTA Session Starting ");
    switch (bootLoaderState)
    {
    case LOADER_STATE_1:
        CONSOLE_MESSAGE_APPEND_STRING ("on image \"B\"");
        blockAddressStart = EXTFLASH_HOST_IMAGE_B_START;
        break;

    case LOADER_STATE_5:
        CONSOLE_MESSAGE_APPEND_STRING ("on image \"A\"");
        blockAddressStart = EXTFLASH_HOST_IMAGE_A_START;
        break;

    case LOADER_INVALID_STATE:
        LoaderStateCorrupted_ResetToFactory();
        break;
        
    default:
        OtauImageSize = 0;
        return;
    }

    Erase_OTAU_Memory();
    currentExternalFlashAddress = blockAddressStart;
    OtauImageSize = otauSize;
    dataCnt = 0;
    NewImageDownloadComplete = 0;
    dataPointers.poR = 0;
    dataPointers.poW = 0;

    vTracePrintF(otaChunksChannel, "OTA New Session %d", OtauImageSize);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/20
*******************************************************************************/
void Erase_OTAU_Memory (void)
{
    uint8_t blockCnt = OTAUIMAGE_MAX_SIZE / 0x8000;      //Erase memory in 32K blocks

    for (uint8_t i = 0; i <= blockCnt; i++)
    {
        HAL_EraseFlash (blockAddressStart + ((uint32_t)i*0x8000),
                        FLASH_ERASE_BLOCK32);
    }

}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/20
*******************************************************************************/
#pragma optimize=none
otaErr_t Store_OTAU_Chunks (dbMcuOtauChunk_t * chunk)
{
    otaErr_t errStatus = OTAERR_OK;

    if (OtauImageSize > 0)
    {
        memcpy (&chunksBuffer[dataPointers.poW], chunk->data, chunk->size);
        dataPointers.poW += chunk->size;

        if (dataCnt != chunk->offset)
        {
            errStatus = OTAERR_BADALIGNMENT;
        }
        else
        {
            dataCnt += chunk->size;
            vTracePrintF(otaChunksChannel, "Chunk received %d", dataCnt);

            if (((dataPointers.poW - dataPointers.poR) >= 256) || (dataCnt >= OtauImageSize))
            {
                uint16_t datasize = (dataPointers.poW - dataPointers.poR);
                if (datasize >= 256)
                {
                    datasize = 256;
                }
                vTracePrintF(otaChunksChannel, "Chunk Stored %d", dataCnt);

                HAL_WriteFlashData(currentExternalFlashAddress, 
                                              datasize, 
                                              &chunksBuffer[dataPointers.poR]);
                currentExternalFlashAddress += datasize;
                dataPointers.poR += datasize;
                
                
#ifdef DEVKIT_PLATFORM
                HAL_GPIO_TogglePin(LED_RED_GPIO_Port, LED_RED_Pin);
#endif /* DEVKIT_PLATFORM */

                if (dataCnt >= OtauImageSize)
                {
                    vTracePrint(otaChunksChannel, "OTA Done");

                    if (0 != VerifyBlockCRC(blockAddressStart))
                    {
                        NewImageDownloadComplete = -1;      //crc error
                        CONSOLE_EVENT_MESSAGE ("CRC error on OTA download");
                    }
                    else
                    {
                        NewImageDownloadComplete = 1;
                        CONSOLE_EVENT_MESSAGE ("New OTA Image downloaded successfully");
                    }
                    
    #ifdef DEVKIT_PLATFORM
                    HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, GPIO_PIN_SET);
    #endif /* DEVKIT_PLATFORM */                    
                }
                else
                {
                    uint16_t remainingDataCnt = (dataPointers.poW - dataPointers.poR);
                    memcpy(chunksBuffer, &chunksBuffer[dataPointers.poR], remainingDataCnt);
                    dataPointers.poR = 0;
                    dataPointers.poW = dataPointers.poR + remainingDataCnt;
                }
            }
        }
    }
    return errStatus;
}

/*******************************************************************************
* @brief  Returns the status of the MCU OTAU Image download
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/20
*******************************************************************************/
DB_GETFUNC(GetMcuOtauImageDownloadStatus)
{
    dbMcuOtauDownloadStatus * downloadComplete;

    downloadComplete = (dbMcuOtauDownloadStatus*)pData;
    *downloadComplete = NewImageDownloadComplete;

    return DBERR_OK;
}

/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
