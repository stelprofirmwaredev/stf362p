/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    DB_NVM.c
* @date    2016/11/30
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include ".\DB\inc\DB_NVM.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\HAL\inc\HAL_Flash.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#ifdef FREERTOS_SUPPORT
    #include "trcRecorder.h"
    char* NVMChannel;
#endif


/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
static nvmPageDescr_t nvmPages [NVM_PAGES_AMOUNT];
extern dbType_AllThermostats_t ConnectedThermostats;
extern dbType_Residence_t CurrentResidence;
extern dbType_CtrlSettings_t ControllerSettings;
extern dbType_ThisThermostat_t thisThermostat;
extern dbType_Onboarding_t Onboarding;
extern dbType_Time_t SystemTime;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
CRC_HandleTypeDef hCrc;

nvmPageHandle_t hResidence_NVM = NVM_NOINIT_HANDLER;
nvmPageHandle_t hThermostat_NVM[THERMOSTAT_INSTANCES];
nvmPageHandle_t hOnboarding_NVM = NVM_NOINIT_HANDLER;
nvmPageHandle_t hDebug_NVM = NVM_NOINIT_HANDLER;

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
nvmPageHandle_t createNVMPage (nvmPageType_t type);
void initNvmPages (void);
void addNvmElementToPage (nvmPageHandle_t nvmHandle, void * pData, uint8_t size);
void nvmPull (nvmPageHandle_t nvmHandle);
void nvmPush (nvmPageHandle_t nvmHandle);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void nvmPush (nvmPageHandle_t nvmHandle)
{
    uint16_t paddingSize = 0;
    validationWord_t * validationWord;
    uint8_t * nvmNewData;
    uint8_t * nvmOldData;
    nvmElement_t * element;
    uint8_t heapRetry = 3;

    if (nvmHandle < NVM_PAGES_AMOUNT)
    {
        /************************************************************/
        /*  Add padding to align to sizeof(uint32_t)                */
        /*  required for the calculation of the CRC32 (peripheral)  */
        /************************************************************/
//        if (pageSize%(sizeof(validationWord_t)) != 0)
        {
            paddingSize = nvmPages[nvmHandle].pageSize%(sizeof(uint32_t));
        }

        while (heapRetry-- > 0)
        {
            /************************************************************/
            /*      Allocate memory for a copy of the flash             */
            /************************************************************/
            nvmNewData = pvPortMalloc(nvmPages[nvmHandle].pageSize + paddingSize + sizeof(validationWord_t));
            nvmOldData = pvPortMalloc(nvmPages[nvmHandle].pageSize + paddingSize + sizeof(validationWord_t));

            /************************************************************/
            /*      read data from flash                                */
            /************************************************************/
            if ((NULL != nvmNewData) && (NULL != nvmOldData))
            {
                uint8_t * copyOfNvmData;
                uint8_t rewriteStatus = 0;

                copyOfNvmData = nvmNewData;
                element = nvmPages[nvmHandle].nvmElement;
                while (element != NULL)
                {
                    uint8_t * u8ptr;
                    u8ptr = (uint8_t*)element->ptr_to_data;

                    for (uint8_t i = 0; i < element->size_of_data; i++)
                    {
                        *nvmNewData = *u8ptr;
                        u8ptr++;
                        nvmNewData++;
                    }
                    element = element->nextElement;
                }

                HAL_ReadFlashData (nvmPages[nvmHandle].pageStartAddress,
                                       nvmPages[nvmHandle].pageSize + paddingSize + sizeof(validationWord_t),
                                       nvmOldData);

                nvmNewData = copyOfNvmData;

                for (uint16_t i = 0; i < nvmPages[nvmHandle].pageSize; i++)
                {
                    if (nvmNewData[i] != nvmOldData[i])
                    {
                        /************************************************************/
                        /*      Compute CRC to check data integrity                 */
                        /************************************************************/
                        validationWord = (validationWord_t *)(nvmNewData + nvmPages[nvmHandle].pageSize + paddingSize);
                        *validationWord = HAL_CRC_Calculate(&hCrc,
                                                           (uint32_t*)nvmNewData,
                                                           nvmPages[nvmHandle].pageSize/sizeof(uint32_t));

                        nvmPages[nvmHandle].dataValidationWord = *validationWord;

                        HAL_EraseFlash (nvmPages[nvmHandle].pageStartAddress, FLASH_ERASE_SECTOR);
                        HAL_WriteFlashData (nvmPages[nvmHandle].pageStartAddress,
                                           nvmPages[nvmHandle].pageSize + paddingSize + sizeof(validationWord_t),
                                           nvmNewData);
                        rewriteStatus = 1;
                        break;
                    }
                }

                if (rewriteStatus == 0)
                {
        #ifdef FREERTOS_SUPPORT
                    vTracePrint(NVMChannel, "NVM page not updated:same data");
        #endif
                }
                vPortFree(nvmNewData);
                vPortFree(nvmOldData);
                break;              //exit while (heapRetry)
            }
            else
            {
                if (NULL != nvmNewData)
                {
                    vPortFree(nvmNewData);
                }

                if (NULL != nvmOldData)
                {
                    vPortFree(nvmOldData);
                }
#ifdef FREERTOS_SUPPORT
                vTracePrint(NVMChannel, "Heap full : suspending current task");
#endif
                vTaskDelay(pdMS_TO_TICKS(1));      //suspend current task until wifi q is has some free spaces

            }
        }
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void nvmPull (nvmPageHandle_t nvmHandle)
{
    uint16_t paddingSize = 0;
    validationWord_t validationWord;
    uint8_t * nvmData;
    nvmElement_t * element;

    if (nvmHandle < NVM_PAGES_AMOUNT)
    {
    /************************************************************/
    /*  Calculate total size of the nvm page                    */
    /************************************************************/


        /************************************************************/
        /*  Add padding to align to sizeof(uint32_t)                */
        /*  required for the calculation of the CRC32 (peripheral)  */
        /************************************************************/
//        if (nvmPages[nvmHandle].pageSize%(sizeof(uint32_t)) != 0)
        {
            paddingSize = nvmPages[nvmHandle].pageSize%(sizeof(uint32_t));
        }

        /************************************************************/
        /*      Allocate memory for a copy of the flash             */
        /************************************************************/
        nvmData = pvPortMalloc(nvmPages[nvmHandle].pageSize + paddingSize + sizeof(validationWord_t));

        /************************************************************/
        /*      read data from flash                                */
        /************************************************************/
        if (NULL != nvmData)
        {
            uint8_t * copyOfDataBuffer = nvmData;
            validationWord_t * pValidationWord;

            HAL_ReadFlashData (nvmPages[nvmHandle].pageStartAddress,
                               nvmPages[nvmHandle].pageSize + paddingSize + sizeof(validationWord_t),
                               nvmData);

            pValidationWord = (validationWord_t*)(nvmData + nvmPages[nvmHandle].pageSize + paddingSize);

            /************************************************************/
            /*      Compute CRC to check data integrity                 */
            /************************************************************/
            validationWord = HAL_CRC_Calculate(&hCrc,
                                               (uint32_t*)nvmData,
                                               nvmPages[nvmHandle].pageSize/sizeof(uint32_t));

            if (validationWord != *pValidationWord)
            {
                /****************************************************/
                /*      CRC not good; erase nvm page                */
                /****************************************************/
#ifdef FREERTOS_SUPPORT
                    vTracePrintF(NVMChannel, "Bad CRC on NVM page %d", nvmHandle);
#endif
                    HAL_EraseFlash (nvmPages[nvmHandle].pageStartAddress, FLASH_ERASE_SECTOR);
                    nvmPush(nvmHandle);
            }
            else
            {
                /****************************************************/
                /*  CRC is good; populate data with flash value     */
                /****************************************************/
                element = nvmPages[nvmHandle].nvmElement;
                while (element != NULL)
                {
                    uint8_t * u8ptr;
                    u8ptr = (uint8_t*)element->ptr_to_data;

                    for (uint8_t i = 0; i < element->size_of_data; i++)
                    {
                        *u8ptr = *nvmData;
                        u8ptr++;
                        nvmData++;
                    }
                    element = element->nextElement;
                }
            }
            vPortFree(copyOfDataBuffer);
        }
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
nvmPageHandle_t createNVMPage (nvmPageType_t type)
{
    nvmPageHandle_t freePage = NVM_NOINIT_HANDLER;
    nvmPagesAddr_t lastUsedPage = NVM_START_PAGE;

    for (uint8_t i = 0; i < NVM_PAGES_AMOUNT; i++)
    {
        if (nvmPages[i].pageStartAddress == 0)
        {
            freePage = i;
            break;
        }
    }

    if (freePage > 0)
    {
        lastUsedPage = (nvmPagesAddr_t)(nvmPages[freePage-1].pageStartAddress + NVMPAGE_SIZE);
    }

    nvmPages[freePage].pageStartAddress = lastUsedPage;
    nvmPages[freePage].pageType = type;
    nvmPages[freePage].nvmElement = NULL;

    return freePage;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void initNvmPages (void)
{
    for (uint8_t i = 0; i < NVM_PAGES_AMOUNT; i++)
    {
        nvmPages[i].pageStartAddress = (nvmPagesAddr_t)0;
        nvmPages[i].pageType = NVM_PAGE_NORMAL;
        nvmPages[i].nvmElement = NULL;
        nvmPages[i].dataValidationWord = 0xFFFFFFFF;
        nvmPages[i].pageSize = 0;
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void addNvmElementToPage (nvmPageHandle_t nvmHandle, void * pData, uint8_t size)
{
    nvmElement_t * newElement;
    newElement = pvPortMalloc(sizeof(nvmElement_t));

    if (newElement != NULL)
    {
        newElement->ptr_to_data = pData;
        newElement->size_of_data = size;
        newElement->nextElement = nvmPages[nvmHandle].nvmElement;

        nvmPages[nvmHandle].nvmElement = newElement;
        nvmPages[nvmHandle].pageSize += size;
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
DB_NOTIFICATION(UpdateNVMResidenceInformation)
{
#ifdef FREERTOS_SUPPORT
    vTracePrint(NVMChannel, "Pushing Residence Infos to NVM");
#endif
    nvmPush (hResidence_NVM);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
DB_NOTIFICATION(UpdateNVMThermostatInformation)
{
#ifdef FREERTOS_SUPPORT
    vTracePrintF(NVMChannel, "Pushing Stats %d Infos to NVM", instance);
#endif

    if (instance < THERMOSTAT_INSTANCES)
    {
        nvmPush (hThermostat_NVM[instance]);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
DB_NOTIFICATION(UpdateNVMOnboardingInformation)
{
#ifdef FREERTOS_SUPPORT
    vTracePrint(NVMChannel, "Pushing Onboarding Infos to NVM");
#endif
    nvmPush (hOnboarding_NVM);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void initResidenceNvmPage(void)
{
    hResidence_NVM = createNVMPage(NVM_PAGE_NORMAL);

    if (NVM_NOINIT_HANDLER != hResidence_NVM)
    {
        addNvmElementToPage (hResidence_NVM, &CurrentResidence.TemperatureFormat, sizeof(uint8_t));
        addNvmElementToPage (hResidence_NVM, &CurrentResidence.TimeFormat, sizeof(uint8_t));
        addNvmElementToPage (hResidence_NVM, &ControllerSettings.Language, sizeof(DisplayLang_t));
        addNvmElementToPage (hResidence_NVM, &thisThermostat.Backlight, sizeof(uint32_t));
        addNvmElementToPage (hResidence_NVM, &thisThermostat.OffMode, sizeof(uint8_t));
        addNvmElementToPage (hResidence_NVM, &thisThermostat.FloorType, sizeof(uint8_t));
        addNvmElementToPage (hResidence_NVM, &SystemTime.IsDstActive, sizeof(uint8_t));
        addNvmElementToPage (hResidence_NVM, &CurrentResidence.ScheduleMode, sizeof(uint8_t));
        addNvmElementToPage (hResidence_NVM, &CurrentResidence.WakeSetpoint, sizeof(uint16_t));
        addNvmElementToPage (hResidence_NVM, &CurrentResidence.LeaveSetpoint, sizeof(uint16_t));
        addNvmElementToPage (hResidence_NVM, &CurrentResidence.ReturnSetpoint, sizeof(uint16_t));
        addNvmElementToPage (hResidence_NVM, &CurrentResidence.SleepSetpoint, sizeof(uint16_t));
        addNvmElementToPage (hResidence_NVM, &CurrentResidence.FullSchedule, sizeof(FullSchedule_t));
    }

    nvmPull (hResidence_NVM);
    DBTHRD_SubscribeToNotification(DBTYPE_TEMPERATURE_FORMAT,
                                   UpdateNVMResidenceInformation,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification(DBTYPE_TIME_FORMAT,
                                   UpdateNVMResidenceInformation,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification(DBTYPE_LANGUAGE_SETTING,
                                   UpdateNVMResidenceInformation,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification(DBTYPE_BACKLIGHT_SETTING,
                                   UpdateNVMResidenceInformation,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification(DBTYPE_OFF_MODE_SETTING,
                                   UpdateNVMResidenceInformation,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification(DBTYPE_FLOOR_TYPE,
                                   UpdateNVMResidenceInformation,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification(DBTYPE_DST_ACTIVE,
                                   UpdateNVMResidenceInformation,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification(DBTYPE_SCHEDULE_MODE,
                                   UpdateNVMResidenceInformation,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification(DBTYPE_WAKE_SETPOINT,
                                   UpdateNVMResidenceInformation,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification(DBTYPE_LEAVE_SETPOINT,
                                   UpdateNVMResidenceInformation,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification(DBTYPE_RETURN_SETPOINT,
                                   UpdateNVMResidenceInformation,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification(DBTYPE_SLEEP_SETPOINT,
                                   UpdateNVMResidenceInformation,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification(DBTYPE_WAKE_SCHEDULE,
                                   UpdateNVMResidenceInformation,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification(DBTYPE_LEAVE_SCHEDULE,
                                   UpdateNVMResidenceInformation,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification(DBTYPE_RETURN_SCHEDULE,
                                   UpdateNVMResidenceInformation,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification(DBTYPE_SLEEP_SCHEDULE,
                                   UpdateNVMResidenceInformation,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void initThermostatNvmPage(void)
{

    for (uint8_t i = 0; i < THERMOSTAT_INSTANCES; i++)
    {
        hThermostat_NVM[i] = createNVMPage(NVM_PAGE_NORMAL);
        addNvmElementToPage (hThermostat_NVM[i],&ConnectedThermostats.Thermostat[i].AmbientSetpoint,sizeof(uint16_t));
        addNvmElementToPage (hThermostat_NVM[i],&ConnectedThermostats.Thermostat[i].FloorSetpoint,sizeof(uint16_t));
        addNvmElementToPage (hThermostat_NVM[i],&ConnectedThermostats.Thermostat[i].LockState,sizeof(uint8_t));
        addNvmElementToPage (hThermostat_NVM[i],&ConnectedThermostats.Thermostat[i].FloorMode,sizeof(uint8_t));
        addNvmElementToPage (hThermostat_NVM[i],&ConnectedThermostats.Thermostat[i].FloorLimit,sizeof(uint16_t));
        addNvmElementToPage (hThermostat_NVM[i],&ConnectedThermostats.Thermostat[i].RelayCycleCount, sizeof(uint32_t));

        nvmPull (hThermostat_NVM[i]);
    }

    DBTHRD_SubscribeToNotification(DBTYPE_AMBIENT_SETPOINT,
                                   UpdateNVMThermostatInformation,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification(DBTYPE_FLOOR_SETPOINT,
                                   UpdateNVMThermostatInformation,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification(DBTYPE_LOCK_STATE,
                                   UpdateNVMThermostatInformation,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification(DBTYPE_FLOOR_MODE,
                                   UpdateNVMThermostatInformation,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification(DBTYPE_FLOOR_LIMIT,
                                   UpdateNVMThermostatInformation,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification(DBTYPE_RELAY_CYCLE_COUNT,
                                   UpdateNVMThermostatInformation,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);

}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void initOnboardingPage(void)
{
    hOnboarding_NVM = createNVMPage(NVM_PAGE_NORMAL);

    if (NVM_NOINIT_HANDLER != hOnboarding_NVM)
    {
        addNvmElementToPage (hOnboarding_NVM, &Onboarding.OnboardingDone, sizeof(uint8_t));
    }

    nvmPull (hOnboarding_NVM);
    DBTHRD_SubscribeToNotification(DBTYPE_ONBOARDING_DONE,
                                   UpdateNVMOnboardingInformation,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void initDebugPage(void)
{
    hDebug_NVM = createNVMPage(NVM_PAGE_NORMAL);

    if (NVM_NOINIT_HANDLER != hDebug_NVM)
    {
#ifdef LOAD_CYCLING
        addNvmElementToPage (hDebug_NVM, &thisThermostat.LoadCyclingMode, sizeof(uint8_t));
        addNvmElementToPage (hDebug_NVM, &thisThermostat.LoadCyclingCount, sizeof(uint16_t));
#endif /* LOAD_CYCLING */
    }

    nvmPull (hDebug_NVM);
#ifdef LOAD_CYCLING
    DBTHRD_SubscribeToNotification(DBTYPE_LOAD_CYCLING_MODE,
                                   UpdateNVMDebugInformation,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
    DBTHRD_SubscribeToNotification(DBTYPE_LOAD_CYCLING_COUNT,
                                   UpdateNVMDebugInformation,
                                   DBCHANGE_ALLCHANGES,
                                   INDEX_DONT_CARE);
#endif /* LOAD_CYCLING */
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void NVM_Init (void)
{
    NVMChannel = vTraceStoreUserEventChannelName("NVM Events");
    hCrc.Instance = CRC;

    HAL_CRC_Init(&hCrc);

#ifdef FREERTOS_SUPPORT
    vTracePrint(NVMChannel, "NVM Init func");
#endif
    initNvmPages();

    /************************************************************/
    /************************************************************/
    /***             !!!!!!!! WARNING !!!!!!!!!               ***/
    /***                                                      ***/
    /***    When adding a new page to the NVM section,        ***/
    /***    keep in mind that the order of initialisation     ***/
    /***    is impacting directly the order of the storage    ***/
    /***    spaces.                                           ***/
    /***    So, the leprechauns should avoid changing the     ***/
    /***    initialization order just for fun.                ***/
    /************************************************************/
    /************************************************************/
    
    //need to add time check here since here is when NVM mem is checked and values loaded

    initResidenceNvmPage();
    initThermostatNvmPage();
    initOnboardingPage();
    initDebugPage();
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void NVM_PushAll (void)
{
    for (uint8_t i = 0; i < NVM_PAGES_AMOUNT; i++)
    {
        if (nvmPages[i].pageStartAddress != 0)
        {
            nvmPush(i);
        }
        else
        {
            break;
        }
    }
}



/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
