/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    DB_SetServices.c
* @date    2016/09/14
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include ".\DB\inc\THREAD_DBDataTypes.h"
#include ".\DB\inc\THREAD_DB.h"

#include ".\DB\classes\inc\class_Thermostats.h"
#include ".\DB\classes\inc\class_Residence.h"
#include ".\DB\classes\inc\class_Controller.h"
#include ".\DB\classes\inc\class_ThisThermostat.h"

#include "APP_SetpointManager.h"
/*******************************************************************************
*    Private constant definitions
*******************************************************************************/


/*******************************************************************************
*    Private variables definitions
*******************************************************************************/

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/


/*******************************************************************************
*    Table of services per datatype IDs
*******************************************************************************/
const setServiceFunc_t Set_ServicesTable[] =
{
    [DBTYPE_BOOTPROGRESSBAR]                            = 0,                              //
    [DBTYPE_ONBOARDING_DONE]                            = Set_OnboardingDone,             //
    [DBTYPE_TEMPERATURE_FORMAT]                         = Set_TemperatureFormat,          //
    [DBTYPE_TIME_FORMAT]                                = Set_TimeFormat,                 //
    [DBTYPE_SCHEDULE_MODE]                              = Set_ScheduleMode,               //
    [DBTYPE_ACTIVE_PGM]                                 = Set_ActiveProgram,              //
    [DBTYPE_WAKE_SETPOINT]                              = Set_WakeSetpoint,               //
    [DBTYPE_LEAVE_SETPOINT]                             = Set_LeaveSetpoint,              //
    [DBTYPE_RETURN_SETPOINT]                            = Set_ReturnSetpoint,             //
    [DBTYPE_SLEEP_SETPOINT]                             = Set_SleepSetpoint,              //
    [DBTYPE_WAKE_SCHEDULE]                              = Set_WakeSchedule,               //
    [DBTYPE_LEAVE_SCHEDULE]                             = Set_LeaveSchedule,              //
    [DBTYPE_RETURN_SCHEDULE]                            = Set_ReturnSchedule,             //
    [DBTYPE_SLEEP_SCHEDULE]                             = Set_SleepSchedule,              //
    [DBTYPE_DATE]                                       = Set_SystemDate,                 //
    [DBTYPE_LOCALTIME]                                  = Set_LocalTime,                  //
    [DBTYPE_DST_ACTIVE]                                 = Set_DstActive,                  //
    [DBTYPE_LANGUAGE_SETTING]                           = Set_Language,                   //
    [DBTYPE_OFF_MODE_SETTING]                           = Set_OffMode,                    //
    [DBTYPE_FLOOR_TYPE]                                 = Set_FloorType,                  //
    [DBTYPE_BACKLIGHT_SETTING]                          = Set_Backlight,                  //
    [DBTYPE_AMBIENT_TEMPERATURE_READING]                = Set_AmbientTemperatureReading,  //
    [DBTYPE_COMPENSATION_TEMPERATURE_READING]           = Set_CompTemperatureReading,     //
    [DBTYPE_HIGH_TEMPERATURE_READING]                   = Set_HighTemperatureReading,     //
    [DBTYPE_CONTROL_TEMPERATURE]                        = Set_ControlTemperature,         //
    [DBTYPE_CONTROLLER_VERSION]                         = Set_ControllerVersion,          //

    [DBTYPE_AMBIENT_TEMPERATURE]                        = Set_AmbientTemperature,         //
    [DBTYPE_AMBIENT_SETPOINT]                           = Set_AmbientSetpoint,            //
    [DBTYPE_FLOOR_TEMPERATURE]                          = Set_FloorTemperature,           //
    [DBTYPE_FLOOR_SETPOINT]                             = Set_FloorSetpoint,              //
    [DBTYPE_ACTIVE_ALERTS]                              = Set_ActiveAlerts,               //
    [DBTYPE_LOCK_STATE]                                 = Set_LockState,                  //
    [DBTYPE_FLOOR_MODE]                                 = Set_FloorMode,                  //
    [DBTYPE_FLOOR_LIMIT]                                = Set_FloorLimit,                 //
    [DBTYPE_HEAT_DEMAND]                                = Set_HeatDemand,                 //
    [DBTYPE_TSTAT_VERSION]                              = Set_TstatVersion,               //
    [DBTYPE_SERIAL_NUMBER]                              = Set_SerialNumber,               //
    [DBTYPE_RELAY_CYCLE_COUNT]                          = Set_RelayCycleCount,            //

    [DBTYPE_USER_ACTION]                                = 0,                              //
    [DBTYPE_MINUTE_CHANGE]                              = 0,                              //
    [DBTYPE_FLOOR_SETPOINT_CHANGE]                      = 0,
    [DBTYPE_AMBIENT_SETPOINT_CHANGE]                    = 0,
    [DBTYPE_RESIDENCE_STATE_CHANGE]                     = 0,
    [DBTYPE_AMBIENT_TEMPERATURE_REPORT]                 = 0,
    [DBTYPE_HEAT_DEMAND_REPORT]                         = 0,
#ifdef LOAD_CYCLING
    [DBTYPE_LOAD_CYCLING_MODE]                          = Set_LoadCyclingMode,
    [DBTYPE_LOAD_CYCLING_COUNT]                         = Set_LoadCyclingCount,
#endif /* LOAD_CYCLING */
};



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
dbErr_t UpdateSetpoint (uint16_t *dbSetpoint, TEMPERATURE_C_t newSetpoint, uint8_t isFloorSetpoint, uint8_t isLocalTstat)
{
    dbErr_t errStatus = DBERR_NOCHANGES;
    uint16_t maxSetpoint;
    FloorType_t floorType;

    maxSetpoint = MAXIMUM_SETPOINT;
    if (isFloorSetpoint != FALSE)
    {
        if (isLocalTstat != 0)
        {
            DBTHRD_GetData(DBTYPE_FLOOR_TYPE,(void*)&floorType, THIS_THERMOSTAT);
            if (floorType == FLOORTYPE_TILE)
            {
                maxSetpoint = MAXIMUM_FLOOR_SETPOINT;
            }
            else
            {
                maxSetpoint = MAXIMUM_ENGINEERED_FLOOR_SETPOINT;
            }
        }
        else
        {
            maxSetpoint = MAXIMUM_FLOOR_SETPOINT;
        }
    }
    if (*dbSetpoint != newSetpoint)
    {
        errStatus = DBERR_SETPOINTHASCHANGED;

        if (newSetpoint < MINIMUM_SETPOINT)
        {
            *dbSetpoint = MINIMUM_SETPOINT;
        }
        else if (newSetpoint > maxSetpoint )
        {
            *dbSetpoint = maxSetpoint;
        }
        else
        {
            *dbSetpoint = newSetpoint;
        }
    }
    //If setpoint is ambient, make sure the setpoint is not higher than 30�C
    //in case we switched from Floor mode to Ambient mode
    if (isFloorSetpoint == FALSE)
    {
        if (newSetpoint > MAXIMUM_SETPOINT )
        {
            *dbSetpoint = MAXIMUM_SETPOINT;
            errStatus = DBERR_SETPOINTHASCHANGED;
        }
    }

    return errStatus;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
dbErr_t CheckStringChange (uint8_t *dbString, char *newString)
{
    dbErr_t errStatus = DBERR_NOCHANGES;

    if (strcmp((char const*)dbString, newString) != 0)
    {
        errStatus = DBERR_STRINGHASCHANGED;
    }

    return errStatus;
}

/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
