/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    DB_InitServices.c
* @date    2016/09/14
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <string.h>
#include ".\DB\inc\THREAD_DB.h"
#include ".\DB\inc\THREAD_DBDataTypes.h"
#include ".\DB\inc\DB_NVM.h"
#include ".\DB\classes\inc\class_Thermostats.h"
#include "APP_SetpointManager.h"
#include "StringObjects.h"
#include ".\inc\StelproStat.h"
#include ".\sync_barrier\inc\SyncBarrier.h"
#include "MenuHandler.h"
#include "MenuTable.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
SemaphoreHandle_t sem_ClearDataBase = NULL;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
extern dbType_Residence_t           CurrentResidence;
extern dbType_AllThermostats_t      ConnectedThermostats;
extern dbType_CtrlSettings_t        ControllerSettings;
extern dbBootProgressBar_t          ProgressBar;
extern dbType_Date_t                SystemDate;
extern dbType_Time_t                SystemTime;
extern dbType_TemperatureReading_t  SensorReadings;
extern dbType_ThisThermostat_t      thisThermostat;

#include "trcRecorder.h"
extern char* dbEventChannel;


/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
void InitSystemTimeAndDate(void);
void InitThisThermostat(void);
void InitResidenceInformations (void);
void InitController(void);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
void InitData(void)
{
    sem_ClearDataBase = NULL;

    InitSystemTimeAndDate();

	InitResidenceInformations();
    InitController();

    InitThisThermostat();
}

/*******************************************************************************
* @brief  Clear the database after unregistration or on Reset to Default
* @inputs clearReason: reason why we clear the database (0: factory test, 1: reset to default, 2: unregistration)
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/12/16
*******************************************************************************/
void ClearDatabase(uint8_t clearReason)
{
    Sync_ClearTaskReady(BARRIER_TASK_DATABASE);

	InitResidenceInformations();
    InitSystemTimeAndDate();
    InitController();
    InitThisThermostat();

    NVM_PushAll();

    StelproStatInit();

    NVIC_SystemReset();
}

/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
