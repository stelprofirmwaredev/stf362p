/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    THREAD_DB.c
* @date    2016/09/13
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>

#include "cmsis_os.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\DB\inc\THREAD_DBDataTypes.h"
#include ".\DB\inc\DB_NVM.h"

#include ".\Console\inc\THREAD_Console.h"

#include "BootLoader_OTA_Control.h"

#include ".\sync_barrier\inc\SyncBarrier.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
typedef struct
{
    dbDataTypes_t subscribeToData;
    notificationServiceFunc_t notificationCB;
    dbChangeSource_t source;
    uint16_t instance;
} subscriptionData_t;

typedef struct
{
    dbDataTypes_t dataType;
    dbChangeSource_t source;
    uint64_t instance;
    dbErr_t status;
    notificationServiceFunc_t notificationCB;
} notificationData_t;

typedef struct notificationElement_s
{
    notificationData_t notification;
    struct notificationElement_s * nextNotification;
} notificationElement_t;

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/

notificationElement_t * currentNotification;
notificationElement_t * lastNotification;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
osThreadId dbTaskHandle;
osThreadId notificationTaskHandle;
QueueHandle_t dbGenericMsgQ;
QueueHandle_t dbNotificationMsgQ;
QueueHandle_t dbSetDataMsgQ;

TimerHandle_t OverheatingTimeout;
#ifdef FREERTOS_SUPPORT
    #include "trcRecorder.h"
    char* dbEventChannel;
#endif


extern const getServiceFunc_t Get_ServicesTable[];
extern const setServiceFunc_t Set_ServicesTable[];
extern void InitData(void);
void DBTHRD_NotifySubscriber (dbDataTypes_t dataType, dbErr_t status);

subscriptionData_t SubscriptionTable [DBTHRD_MAX_SUBSCRIPTION];

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
void StartNotificationTask(void const * argument);
void StartDBTask(void const * argument);
static void AddNotification (notificationData_t notifData);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/10/12
*******************************************************************************/
static void AddNotification (notificationData_t notifData)
{
    {
        notificationElement_t * newNotification = NULL;

        uint8_t retry = 100;
        while ((newNotification == NULL) && (retry-->0))
        {
            newNotification = pvPortMalloc(sizeof(notificationElement_t));
            if (newNotification == NULL)
            {
                osDelay(10);
            }
        }

        if (newNotification != NULL)
        {

            newNotification->notification = notifData;
            newNotification->nextNotification = NULL;

            if (currentNotification == NULL)
            {
                currentNotification = newNotification;
                lastNotification = currentNotification;
            }
            else
            {
                lastNotification->nextNotification = newNotification;
                lastNotification = lastNotification->nextNotification;
            }

//            xTaskNotify(notificationTaskHandle, 0, eNoAction);
        }
        else
        {
            vTracePrint(dbEventChannel, "Heap full. Cannot add notification");
        }
    }
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/10/12
*******************************************************************************/
susbscriptionHandle_t DBTHRD_SubscribeToNotification (dbDataTypes_t dataType,  notificationServiceFunc_t notifCB, dbChangeSource_t source, uint64_t instance)
{
    uint8_t i;
    susbscriptionHandle_t subscriptHandle = DBTHRD_NOTIFSUBSCRIPTION_FULL;

    for (i = 0; i < DBTHRD_MAX_SUBSCRIPTION; i++)
    {
        if (SubscriptionTable[i].subscribeToData == DB_NOSUBSCRIPTION)  //find first free table slot,
        {
            SubscriptionTable[i].notificationCB = notifCB;
            SubscriptionTable[i].subscribeToData = dataType;
            SubscriptionTable[i].source = source;
            SubscriptionTable[i].instance= instance;
            subscriptHandle = i;
            break;
        }
    }

    if (i >= DBTHRD_MAX_SUBSCRIPTION)
    {
        CONSOLE_RUNTIME_MESSAGE("Notification Table Full : ");
        CONSOLE_MESSAGE_APPEND_INTEGER (DBTHRD_MAX_SUBSCRIPTION);
        CONSOLE_MESSAGE_APPEND_STRING (" max");
    }

    return subscriptHandle;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/10/12
*******************************************************************************/
void DBTHRD_UnsubscribeToNotification (susbscriptionHandle_t handle)
{
    if (handle < DBTHRD_MAX_SUBSCRIPTION)
    {
        SubscriptionTable[handle].notificationCB = NULL;
        SubscriptionTable[handle].subscribeToData = DB_NOSUBSCRIPTION;
        SubscriptionTable[handle].source = DBCHANGE_NOCHANGES;
        SubscriptionTable[handle].instance = INDEX_DONT_CARE;
    }
}

void notifCallback_task(void const * argument)
{
    notificationElement_t * notif = (notificationElement_t *)argument;

    osThreadSetPriority(NULL, osPriorityHigh);

    notif->notification.notificationCB(notif->notification.status,
                                       notif->notification.instance,
                                       notif->notification.source);

    vPortFree(notif);
    vTaskDelete(NULL);
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/10/12
*******************************************************************************/
uint8_t DBTHRD_NotifySubscribers (notificationElement_t * notif)
{
    for (uint8_t i = 0; i < DBTHRD_MAX_SUBSCRIPTION; i++)
    {
        if (SubscriptionTable[i].subscribeToData == notif->notification.dataType)
        {
            if ((SubscriptionTable[i].source & notif->notification.source) != 0)
            {
                if ((SubscriptionTable[i].instance == notif->notification.instance) ||
                    (SubscriptionTable[i].instance == INDEX_DONT_CARE))
                {
                    if (SubscriptionTable[i].notificationCB != NULL)
                    {
                        notificationData_t * notif_data = NULL;

                        /*  The system needs to notify all subscribers.  To avoid re-notifying subscriber,
                            if one off them fail to be notified, the thread will loop here until memory
                            is available to notify the next subscriber. */
                        while (NULL == notif_data)
                        {
                            vTracePrintF(dbEventChannel, "Notify Data type %d to subscriber %d",
                                         notif->notification.dataType,
                                         i);
                            vTracePrintF(dbEventChannel, "notification address 0x%x", notif);

                            notif_data = pvPortMalloc(sizeof(notificationData_t));

                            if (NULL != notif_data)
                            {
                                notif->notification.notificationCB = SubscriptionTable[i].notificationCB;

                                *notif_data = notif->notification;
                                osThreadDef(notifCBTask, notifCallback_task,osPriorityNormal, 0, 512);
                                if (NULL != osThreadCreate(osThread(notifCBTask), notif_data))
                                {
                                    vTracePrintF(dbEventChannel, "Data type %d notified", notif->notification.dataType);
                                }
                                else
                                {
                                    vPortFree(notif_data);
                                    notif_data = NULL;
                                    vTracePrintF(dbEventChannel, "cannot create Nofication Task. will retry later...", notif->notification.dataType);
                                }
                            }
                            else
                            {
                                vTracePrint(dbEventChannel, "Heap full; will retry later");
                            }

                            /* if the notification fails, the thread will retry */
                            if (NULL == notif_data)
                            {
                                osDelay(5000);
                            }
                        }
                    }
                }
            }
        }
    }
    return 1;
}

/*******************************************************************************
* @brief  DB get service is used to retrieve the value of a data owned by
*           the DB module
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/14
*******************************************************************************/
dbErr_t DBTHRD_GetData(dbDataTypes_t dataType, void * pData, uint64_t index)
{
    dbErr_t errorStatus = DBERR_UNKNOWNSERVICE;
    if (dataType < DBTYPE_MAXDATATYPE)
    {
        if (Get_ServicesTable[dataType] != NULL)
        {
             errorStatus =  Get_ServicesTable[dataType](pData, index);
        }

        if (DBERR_INDEXOUTOFBOUND == errorStatus)
        {
            CONSOLE_EVENT_MESSAGE ("DB Get Index out of bound, type: ");
            CONSOLE_MESSAGE_APPEND_INTEGER(dataType);
            CONSOLE_MESSAGE_APPEND_STRING (", index: ");
            CONSOLE_MESSAGE_APPEND_INTEGER(index);
        }
    }

    return errorStatus;
}

/*******************************************************************************
* @brief  DB Set Services are used to change the value of a data own by the
*       data base module.  These service must never use a FreeRTOS service that
*       block the process on behalf of the DB Thread, because these services are
*       called by any other processes.
*       If the use of a FreeRTOS service that belongs to the DB Thread is needed,
*       a xNotify should be used.
*
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/14
*******************************************************************************/
dbErr_t DBTHRD_SetData_wMnuboDataPoint (dbDataTypes_t dataType, void * pData, uint64_t index, dbChangeSource_t source, void * mnuboDataPoint)
{
    dbErr_t errorStatus = DBERR_UNKNOWNSERVICE;
    notificationData_t notificationData;

    if (dataType < DBTYPE_MAXDATATYPE)
    {
        vTracePrintF(dbEventChannel, "Setting Data type %d", dataType);
        if (Set_ServicesTable[dataType] != NULL)
        {
            errorStatus = Set_ServicesTable[dataType](pData, index, source, mnuboDataPoint);
        }


        if (DBERR_NOCHANGES != errorStatus)
        {
            notificationData.dataType = dataType;
            notificationData.source = source;
            notificationData.instance = index;
            notificationData.status = errorStatus;
            AddNotification(notificationData);
        }

        if (DBERR_INDEXOUTOFBOUND == errorStatus)
        {
            CONSOLE_EVENT_MESSAGE ("DB Set Index out of bound, type: ");
            CONSOLE_MESSAGE_APPEND_INTEGER(dataType);
            CONSOLE_MESSAGE_APPEND_STRING (", index: ");
            CONSOLE_MESSAGE_APPEND_INTEGER(index);
        }
    }
    return errorStatus;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/13
*******************************************************************************/
void InitThread_DB(void)
{
    osThreadDef(dataBaseTask, StartDBTask,osPriorityNormal, 0, 512);
    dbTaskHandle = osThreadCreate(osThread(dataBaseTask), NULL);

    osThreadDef(notifTask, StartNotificationTask,osPriorityAboveNormal, 0, 256);
    notificationTaskHandle = osThreadCreate(osThread(notifTask), NULL);

#ifdef FREERTOS_SUPPORT
    dbEventChannel = vTraceStoreUserEventChannelName("Database Events");
#endif

    for (uint8_t i = 0; i < DBTHRD_MAX_SUBSCRIPTION; i++)
    {
        SubscriptionTable[i].notificationCB = NULL;
        SubscriptionTable[i].subscribeToData = DB_NOSUBSCRIPTION;
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/13
*******************************************************************************/
void StartDBTask(void const * argument)
{
    uint32_t taskNotifications;

#ifdef __ICCARM__
    dbGenericMsgQ = xQueueCreate (1, sizeof(uint32_t));
    configASSERT(dbGenericMsgQ);

    vQueueAddToRegistry(dbGenericMsgQ, "Generic DB Msg Q");
#endif


    /************************************************/
    /*  The order of execution of these functions   */
    /*  are critical and MUST NOT be changed.       */
    /*  The strategy is :                           */
    /*  1. Init default values to all data          */
    /*      structures                              */
    /*  2. Check NVM data integrity                 */
    /*  3. If NVM is OK, load data structures with  */
    /*      NVM data.                               */
    /*  4. Check database integrity for logic issue */
    {
        InitData();
        NVM_Init();
        Sync_TaskReady (BARRIER_TASK_DATABASE);
        Sync_Barrier((barriers_name_t)(BARRIER_TASK_NOTIFICATIONS));
    }
    /************************************************/

    for(;;)
    {
#ifdef __ICCARM__
        xTaskNotifyWait (0x0000,
                         0xffff,
                         &taskNotifications,
                         portMAX_DELAY);
        /*************** Thread Execution blocked here ***************/
        /**** Thread resume every 100ms or on Event Notifications ****/
#endif

	}
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/13
*******************************************************************************/
void StartNotificationTask(void const * argument)
{
    currentNotification = NULL;
    lastNotification = currentNotification;


    Sync_Barrier((barriers_name_t)(BARRIER_TASK_CONSOLE));


    Sync_TaskReady (BARRIER_TASK_NOTIFICATIONS);

    for (;;)
    {
        /* The wait delay need to be tailored in function of the type of
            performance needed :
            * Shorter delay => faster response, more heap required;
            * Longer delay => slower response, less heap required;      */
        xTaskNotifyWait (0x0000, 0x0000, NULL, pdMS_TO_TICKS(10));

        /*************** Thread Execution blocked here ***************/
        /****  Thread resume on notification ****/
        if (currentNotification != NULL)
        {
            notificationElement_t * nextNotif;

            if (DBTHRD_NotifySubscribers(currentNotification))
            {
                nextNotif = currentNotification->nextNotification;
                vPortFree(currentNotification);
                currentNotification = nextNotif;
            }
        }
    }
}

/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
