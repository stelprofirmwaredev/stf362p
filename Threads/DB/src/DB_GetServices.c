/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    DB_GetServices.c
* @date    2016/09/14
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <string.h>

#include ".\DB\inc\THREAD_DBDataTypes.h"
#include ".\DB\inc\THREAD_DB.h"

#include ".\DB\classes\inc\class_Thermostats.h"
#include ".\DB\classes\inc\class_Residence.h"
#include ".\DB\classes\inc\class_Controller.h"
#include ".\DB\classes\inc\class_ThisThermostat.h"


#include ".\DB\inc\DB_OTAU.h"
/*******************************************************************************
*    Private constant definitions
*******************************************************************************/


/*******************************************************************************
*    Private variables definitions
*******************************************************************************/


/*******************************************************************************
*    Public variables definitions
*******************************************************************************/

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/

/*******************************************************************************
*    Table of services per datatype IDs
*******************************************************************************/
const getServiceFunc_t Get_ServicesTable[] =
{
    [DBTYPE_BOOTPROGRESSBAR]                    = NULL,      //
    [DBTYPE_ONBOARDING_DONE]                    = Get_OnboardingDone,             //
    [DBTYPE_TEMPERATURE_FORMAT]                 = Get_TemperatureFormat,          //
    [DBTYPE_TIME_FORMAT]                        = Get_TimeFormat,                 //
    [DBTYPE_SCHEDULE_MODE]                      = Get_ScheduleMode,               //
    [DBTYPE_ACTIVE_PGM]                         = Get_ActiveProgram,              //
    [DBTYPE_WAKE_SETPOINT]                      = Get_WakeSetpoint,               //
    [DBTYPE_LEAVE_SETPOINT]                     = Get_LeaveSetpoint,              //
    [DBTYPE_RETURN_SETPOINT]                    = Get_ReturnSetpoint,             //
    [DBTYPE_SLEEP_SETPOINT]                     = Get_SleepSetpoint,              //
    [DBTYPE_WAKE_SCHEDULE]                      = Get_WakeSchedule,               //
    [DBTYPE_LEAVE_SCHEDULE]                     = Get_LeaveSchedule,              //
    [DBTYPE_RETURN_SCHEDULE]                    = Get_ReturnSchedule,             //
    [DBTYPE_SLEEP_SCHEDULE]                     = Get_SleepSchedule,              //
    [DBTYPE_DATE]                               = Get_SystemDate,                 //
    [DBTYPE_LOCALTIME]                          = Get_SystemTime,                 //
    [DBTYPE_DST_ACTIVE]                         = 0,                              //
    [DBTYPE_LANGUAGE_SETTING]                   = Get_Language,                   //
    [DBTYPE_OFF_MODE_SETTING]                   = Get_OffMode,                    //
    [DBTYPE_FLOOR_TYPE]                         = Get_FloorType,                  //
    [DBTYPE_BACKLIGHT_SETTING]                  = Get_Backlight,                  //
    [DBTYPE_AMBIENT_TEMPERATURE_READING]        = Get_AmbientTemperatureReading,  //
    [DBTYPE_COMPENSATION_TEMPERATURE_READING]   = Get_CompTemperatureReading,     //
    [DBTYPE_HIGH_TEMPERATURE_READING]           = Get_HighTemperatureReading,     //
    [DBTYPE_CONTROL_TEMPERATURE]                = Get_ControlTemperature,         //
    [DBTYPE_CONTROLLER_VERSION]                 = Get_ControllerVersion,          //

    [DBTYPE_AMBIENT_TEMPERATURE]                = Get_AmbientTemperature,         //
    [DBTYPE_AMBIENT_SETPOINT]                   = Get_AmbientSetpoint,            //
    [DBTYPE_FLOOR_TEMPERATURE]                  = Get_FloorTemperature,           //
    [DBTYPE_FLOOR_SETPOINT]                     = Get_FloorSetpoint,              //
    [DBTYPE_ACTIVE_ALERTS]                      = Get_ActiveAlerts,               //
    [DBTYPE_LOCK_STATE]                         = Get_LockState,                  //
    [DBTYPE_FLOOR_MODE]                         = Get_FloorMode,                  //
    [DBTYPE_FLOOR_LIMIT]                        = Get_FloorLimit,                 //
    [DBTYPE_HEAT_DEMAND]                        = Get_HeatDemand,                 //
    [DBTYPE_TSTAT_VERSION]                      = 0,                              //
    [DBTYPE_SERIAL_NUMBER]                      = Get_SerialNumber,               //
    [DBTYPE_RELAY_CYCLE_COUNT]                  = Get_RelayCycleCount,            //

    [DBTYPE_USER_ACTION]                        = 0,                              //
    [DBTYPE_MINUTE_CHANGE]                      = 0,                              //
    [DBTYPE_FLOOR_SETPOINT_CHANGE]              = 0,
    [DBTYPE_AMBIENT_SETPOINT_CHANGE]            = 0,
    [DBTYPE_RESIDENCE_STATE_CHANGE]             = 0,
    [DBTYPE_AMBIENT_TEMPERATURE_REPORT]         = 0,
    [DBTYPE_HEAT_DEMAND_REPORT]                 = 0,
#ifdef LOAD_CYCLING
    [DBTYPE_LOAD_CYCLING_MODE]                  = Get_LoadCyclingMode,
    [DBTYPE_LOAD_CYCLING_COUNT]                 = Get_LoadCyclingCount,
#endif /* LOAD_CYCLING */
};



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/



/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
