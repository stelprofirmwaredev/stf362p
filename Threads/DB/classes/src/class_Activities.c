/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    class_Activities.c
* @date    2017/02/06
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include ".\DB\classes\inc\class_Activities.h"

#include ".\DB\inc\THREAD_DBDataTypes.h"
#include ".\DB\inc\THREAD_DB.h"

#include "StringObjects.h"

#include ".\strings\inc\strings_tool.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
dbType_AllActivities_t       DefinedActivities;
extern uint8_t               PropertiesToUpdate[];



/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
extern uint8_t PropertyFetchInProgress;


/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  Initialize the activities in the database
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/17
*******************************************************************************/
void InitActivities(void)
{
    uint8_t activity;
    uint8_t group;

    for (activity = 0; activity < ACTIVITY_INSTANCES; activity++)
    {
        strcpy((char*)DefinedActivities.Activity[activity].ActivityName, "");
        DefinedActivities.Activity[activity].ActivityReadyAt = 0;
        DefinedActivities.Activity[activity].ActivityStartRequested = 0;
        DefinedActivities.Activity[activity].ActivityStartTime = ON_DEMAND;
        DefinedActivities.Activity[activity].ActivityWeekDays = 0;
        for (group = 0; group < GROUP_INSTANCES; group++)
        {
            DefinedActivities.Activity[activity].ActivitySetpointPerGroup[group] = 0;
        }
    }
}

/*******************************************************************************
* @brief  Set the Activity name
* @inputs pData: pointer to the Activity Name
* @inputs index: specified activity index
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/11/17
*******************************************************************************/
DB_SETFUNC(Set_ActivityName )
{
    dbErr_t status = DBERR_OK;
    char *activityName;

    if (index < ACTIVITY_INSTANCES)
    {
//        uint8_t update_analytic = 0;
        char previous_name[NAME_SIZE_AND_NULL];
        activityName = (char *)pData;

        strcpy(previous_name, (char const *)DefinedActivities.Activity[index].ActivityName);

        ConvertUTF8ToASCII(activityName,
                        (char*) DefinedActivities.Activity[index].ActivityName,
                        sizeof(DefinedActivities.Activity[index].ActivityName));

//        if (strcmp(previous_name, (char*) DefinedActivities.Activity[index].ActivityName))
//        {
//            update_analytic = 1;
//        }
//
//        if ((PropertyFetchInProgress == 0) && (0 != update_analytic))
//        {
//            hMnuboObject mnuboObject;
//            uint8_t mustSendDataPoint = 0;
//
//            if (NULL == mnuboDataPoint)
//            {
//                mnuboDataPoint = Mnubo_CreateNewDataPoint();
//                mustSendDataPoint = 1;
//            }
//
//            if (0 != mustSendDataPoint)
//            {
//                mnuboObject = Mnubo_AddObjectToDataPoint(mnuboDataPoint,
//                                                       (mnuboObjId_t)(MNUBO_OBJ_ACTIVITY_BASE+index));
//                Mnubo_AddDataToObject (mnuboObject, MNUBO_DATA_ACTIVITY_NAME);
//                ANALYTICS_SendDataPoint(mnuboDataPoint);
//            }
//        }
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    if (source == DBCHANGE_CLOUD)
    {
        PropertiesToUpdate[DBTYPE_ACTIVITY_NAME] = 0;
    }

    return status;
}

/*******************************************************************************
* @brief  Set the Activity start request
* @inputs pData: pointer to the Activity start request
* @inputs index: specified activity index
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/11/17
*******************************************************************************/
DB_SETFUNC(Set_ActivityStartRequest )
{
    dbErr_t status = DBERR_OK;
    uint8_t *activityStartRequest;
    uint8_t activityInstance;

    for (activityInstance = 0; activityInstance < ACTIVITY_INSTANCES; activityInstance++)
    {
        activityStartRequest = (uint8_t *)pData + activityInstance;

        DefinedActivities.Activity[activityInstance].ActivityStartRequested = *activityStartRequest;
    }

    if (source == DBCHANGE_CLOUD)
    {
        PropertiesToUpdate[DBTYPE_ACTIVITY_START] = 0;
    }

    return status;
}

/*******************************************************************************
* @brief  Set the Activity ready at
* @inputs pData: pointer to the Activity ready at
* @inputs index: specified activity index
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/11/17
*******************************************************************************/
DB_SETFUNC(Set_ActivityReadyAt )
{
    dbErr_t status = DBERR_OK;
    uint8_t *activityReadyAt;

    if (index < ACTIVITY_INSTANCES)
    {
        activityReadyAt = (uint8_t *)pData;

        DefinedActivities.Activity[index].ActivityReadyAt = *activityReadyAt;

//        if (PropertyFetchInProgress == 0)
//        {
//            hMnuboObject mnuboObject;
//            uint8_t mustSendDataPoint = 0;
//
//            if (NULL == mnuboDataPoint)
//            {
//                mnuboDataPoint = Mnubo_CreateNewDataPoint();
//                mustSendDataPoint = 1;
//            }
//
//            if (0 != mustSendDataPoint)
//            {
//                mnuboObject = Mnubo_AddObjectToDataPoint(mnuboDataPoint,
//                                                       (mnuboObjId_t)(MNUBO_OBJ_ACTIVITY_BASE+index));
//                Mnubo_AddDataToObject (mnuboObject, MNUBO_DATA_ACTIVITY_READY_AT);
//                ANALYTICS_SendDataPoint(mnuboDataPoint);
//            }
//        }
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    if (source == DBCHANGE_CLOUD)
    {
        PropertiesToUpdate[DBTYPE_ACTIVITY_READY_AT] = 0;
    }

    return status;
}

/*******************************************************************************
* @brief  Set the Activity setpoint per group
* @inputs pData: pointer to the Activity setpoint per group
* @inputs index: specified activity index
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/11/17
*******************************************************************************/
DB_SETFUNC(Set_ActivitySetpointPerGroup )
{
    dbErr_t status = DBERR_OK;
    Setpoint100_t *setpointPerGroup;
    uint8_t group;

    if (index < ACTIVITY_INSTANCES)
    {
        setpointPerGroup = (Setpoint100_t*)pData;

        //Save the setpoint for each group
        for (group = 0; group < GROUP_INSTANCES; group++)
        {
            DefinedActivities.Activity[index].ActivitySetpointPerGroup[group] = setpointPerGroup[group];
        }
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    if (source == DBCHANGE_CLOUD)
    {
        PropertiesToUpdate[DBTYPE_ACTIVITY_SETPOINT_PER_GROUP] = 0;
    }

    return status;
}

/*******************************************************************************
* @brief  Set the Activity start time
* @inputs pData: pointer to the Activity start time
* @inputs index: specified activity index
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/11/17
*******************************************************************************/
DB_SETFUNC(Set_ActivityStartTime )
{
    dbErr_t status = DBERR_OK;
    int16_t *activityStartTime;

    if (index < ACTIVITY_INSTANCES)
    {
        activityStartTime = (int16_t *)pData;

        DefinedActivities.Activity[index].ActivityStartTime = *activityStartTime;

//        if (PropertyFetchInProgress == 0)
//        {
//            hMnuboObject mnuboObject;
//            uint8_t mustSendDataPoint = 0;
//
//            if (NULL == mnuboDataPoint)
//            {
//                mnuboDataPoint = Mnubo_CreateNewDataPoint();
//                mustSendDataPoint = 1;
//            }
//
//            if (0 != mustSendDataPoint)
//            {
//                mnuboObject = Mnubo_AddObjectToDataPoint(mnuboDataPoint,
//                                                       (mnuboObjId_t)(MNUBO_OBJ_ACTIVITY_BASE+index));
//                Mnubo_AddDataToObject (mnuboObject, MNUBO_DATA_ACTIVITY_TIME);
//                ANALYTICS_SendDataPoint(mnuboDataPoint);
//            }
//        }
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    if (source == DBCHANGE_CLOUD)
    {
        PropertiesToUpdate[DBTYPE_ACTIVITY_START_TIME] = 0;
    }

    return status;
}

/*******************************************************************************
* @brief  Set the Activity week days
* @inputs pData: pointer to the Activity week days
* @inputs index: specified activity index
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/11/17
*******************************************************************************/
DB_SETFUNC(Set_ActivityWeekDays )
{
    dbErr_t status = DBERR_OK;
    uint8_t *activityWeekDays;

    if (index < ACTIVITY_INSTANCES)
    {
        activityWeekDays = (uint8_t*)pData;

        DefinedActivities.Activity[index].ActivityWeekDays = *activityWeekDays;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    if (source == DBCHANGE_CLOUD)
    {
        PropertiesToUpdate[DBTYPE_ACTIVITY_WEEK_DAYS] = 0;
    }

    return status;
}



/*******************************************************************************
* @brief  Get the Activity name
* @inputs pData: pointer to the Activity name
* @inputs index: specified activity index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/11/17
*******************************************************************************/
DB_GETFUNC(Get_ActivityName )
{
    dbErr_t status = DBERR_OK;
    char *activityName;

    if (index < ACTIVITY_INSTANCES)
    {
        activityName = (char *)pData;

        //Make sure the string is NULL terminated
        DefinedActivities.Activity[index].ActivityName[sizeof(DefinedActivities.Activity[index].ActivityName) - 1] = NULL;
        strcpy(activityName, (char const*)DefinedActivities.Activity[index].ActivityName);
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief  Get the Activity name
* @inputs pData: pointer to the Activity name
* @inputs index: specified activity index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/11/17
*******************************************************************************/
DB_GETFUNC(Get_ActivityNameUTF8 )
{
    dbErr_t status = DBERR_OK;
    char *activityName;

    if (index < ACTIVITY_INSTANCES)
    {
        activityName = (char *)pData;

        //Make sure the string is NULL terminated
        DefinedActivities.Activity[index].ActivityName[sizeof(DefinedActivities.Activity[index].ActivityName) - 1] = NULL;
        ConvertASCIIToUTF8 ((char*)DefinedActivities.Activity[index].ActivityName, activityName, strlen((char const*)DefinedActivities.Activity[index].ActivityName));
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief  Get the Activity start request
* @inputs pData: pointer to the Activity start request command
* @inputs index: specified activity index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/11/17
*******************************************************************************/
DB_GETFUNC(Get_ActivityStartRequest )
{
    dbErr_t status = DBERR_OK;
    uint8_t *activityStartRequest;

    if (index < ACTIVITY_INSTANCES)
    {
        activityStartRequest = (uint8_t *)pData;

        *activityStartRequest = DefinedActivities.Activity[index].ActivityStartRequested;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief  Get the Activity ready at
* @inputs pData: pointer to the Activity ready at setting
* @inputs index: specified activity index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/11/17
*******************************************************************************/
DB_GETFUNC(Get_ActivityReadyAt )
{
    dbErr_t status = DBERR_OK;
    uint8_t *activityReadyAt;

    if (index < ACTIVITY_INSTANCES)
    {
        activityReadyAt = (uint8_t *)pData;

        *activityReadyAt = DefinedActivities.Activity[index].ActivityReadyAt;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief  Get the Activity setpoint per group
* @inputs pData: pointer to the Activity setpoint per group
* @inputs index: specified activity index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/11/17
*******************************************************************************/
DB_GETFUNC(Get_ActivitySetpointPerGroup )
{
    dbErr_t status = DBERR_OK;
    Setpoint100_t *setpointPerGroup;
    uint8_t group;

    if (index < ACTIVITY_INSTANCES)
    {
        setpointPerGroup = (Setpoint100_t*)pData;

        for (group = 0; group < GROUP_INSTANCES; group++)
        {
            setpointPerGroup[group] = DefinedActivities.Activity[index].ActivitySetpointPerGroup[group];
        }
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief  Get the Activity start time
* @inputs pData: pointer to the Activity start time
* @inputs index: specified activity index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/11/17
*******************************************************************************/
DB_GETFUNC(Get_ActivityStartTime )
{
    dbErr_t status = DBERR_OK;
    uint16_t *activityStartTime;

    if (index < ACTIVITY_INSTANCES)
    {
        activityStartTime = (uint16_t *)pData;

        *activityStartTime = DefinedActivities.Activity[index].ActivityStartTime;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief  Get the Activity week days
* @inputs pData: pointer to the Activity week days
* @inputs index: specified activity index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/11/17
*******************************************************************************/
DB_GETFUNC(Get_ActivityWeekDays )
{
    dbErr_t status = DBERR_OK;
    uint8_t *activityWeekDays;

    if (index < ACTIVITY_INSTANCES)
    {
        activityWeekDays = (uint8_t *)pData;

        *activityWeekDays = DefinedActivities.Activity[index].ActivityWeekDays;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}



/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
