/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    class_ThisThermostat.c
* @date    2017/02/06
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include ".\DB\classes\inc\class_ThisThermostat.h"

#include ".\DB\inc\THREAD_DBDataTypes.h"
#include ".\DB\inc\THREAD_DB.h"

#include "common_types.h"
#include ".\strings\inc\strings_tool.h"
#include "APP_SetpointManager.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define MAX_RESET_COUNT 3

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
dbType_ThisThermostat_t thisThermostat;
dbType_TemperatureReading_t  SensorReadings;
dbType_Onboarding_t Onboarding;
extern dbType_AllThermostats_t      ConnectedThermostats;
extern U32BIT ProductSerialNumber;
extern uint8_t                      PropertiesToUpdate[];

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/




/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  Initialize the Temperature Readings (ambient and compensation) in the database
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/23
*******************************************************************************/
void InitThisThermostat(void)
{
    //uint16_t serial;

    Onboarding.OnboardingDone = 0;

    SensorReadings.AmbientTemperatureReading = DB_SENSOR_INVALID;
    SensorReadings.CompensationTemperatureReading = DB_SENSOR_INVALID;
    SensorReadings.ControlTemperature = DB_SENSOR_INVALID;

    thisThermostat.Backlight = BL_60_SEC;
	ConnectedThermostats.Thermostat[THIS_THERMOSTAT].AmbientTemperature = DB_SENSOR_INVALID;
    ConnectedThermostats.Thermostat[THIS_THERMOSTAT].DisplayedAmbientTemperature = DB_SENSOR_INVALID;
    ConnectedThermostats.Thermostat[THIS_THERMOSTAT].AmbientSetpoint = DEFAULT_HOME_SETPOINT;

    ConnectedThermostats.Thermostat[THIS_THERMOSTAT].FloorTemperature = DB_SENSOR_INVALID;
    ConnectedThermostats.Thermostat[THIS_THERMOSTAT].DisplayedFloorTemperature = DB_SENSOR_INVALID;
    ConnectedThermostats.Thermostat[THIS_THERMOSTAT].FloorSetpoint = DEFAULT_HOME_SETPOINT;


    ConnectedThermostats.Thermostat[THIS_THERMOSTAT].LockState = LOCK_OFF;
    ConnectedThermostats.Thermostat[THIS_THERMOSTAT].FloorMode = 0;
    ConnectedThermostats.Thermostat[THIS_THERMOSTAT].FloorLimit = 3000;

    ConnectedThermostats.Thermostat[THIS_THERMOSTAT].HeatDemand = 0;
    ConnectedThermostats.Thermostat[THIS_THERMOSTAT].DisplayedHeatDemand = 0;

    ConnectedThermostats.Thermostat[THIS_THERMOSTAT].TstatVersion = 0;

    for (uint8_t alert = 0; alert < ALERT_INSTANCES; alert++)
    {
        ConnectedThermostats.Thermostat[THIS_THERMOSTAT].Alerts.ActiveAlerts[alert] = 0;
    }
    ConnectedThermostats.Thermostat[THIS_THERMOSTAT].Alerts.LastAlertAction = DB_CLEAR_ALERT;
    ConnectedThermostats.Thermostat[THIS_THERMOSTAT].Alerts.LastAlertId = 0;
}


/*******************************************************************************
* @brief  Set the Ambient Temperature reading
* @inputs pData: pointer to the Ambient Temperature reading
* @inputs index: don't care
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/11/23
*******************************************************************************/
DB_SETFUNC(Set_AmbientTemperatureReading )
{
    int16_t *ambientTempReading;

    ambientTempReading = (int16_t *)pData;

    SensorReadings.AmbientTemperatureReading = *ambientTempReading;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Set the Compensation Temperature reading
* @inputs pData: pointer to the Compensation Temperature reading
* @inputs index: don't care
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/11/23
*******************************************************************************/
DB_SETFUNC(Set_CompTemperatureReading )
{
    int16_t *compTempReading;

    compTempReading = (int16_t *)pData;

    SensorReadings.CompensationTemperatureReading = *compTempReading;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Set the High Temperature reading
* @inputs pData: pointer to the High Temperature reading
* @inputs index: don't care
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2017/01/31
*******************************************************************************/
DB_SETFUNC(Set_HighTemperatureReading )
{
    int16_t *highTempReading;

    highTempReading = (int16_t *)pData;

    SensorReadings.HighTemperatureReading = *highTempReading;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Set the temperature used for the control calculations
* @inputs pData: pointer to the Control Temperature
* @inputs index: don't care
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/11/23
*******************************************************************************/
DB_SETFUNC(Set_ControlTemperature )
{
    int16_t *controlTemp;

    controlTemp = (int16_t *)pData;

    SensorReadings.ControlTemperature = *controlTemp;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Set the Off Mode status
* @inputs pData: pointer to the Off Mode status
* @inputs index: specified thermostat index (don't care)
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/05/03
*******************************************************************************/
DB_SETFUNC(Set_OffMode )
{
    uint8_t *offMode;

    offMode = (uint8_t *)pData;

    thisThermostat.OffMode = *offMode;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Set the Floor Type
* @inputs pData: pointer to the Floor Type
* @inputs index: specified thermostat index (don't care)
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/07/16
*******************************************************************************/
DB_SETFUNC(Set_FloorType )
{
    uint8_t *floorType;

    floorType = (uint8_t *)pData;

    thisThermostat.FloorType = *floorType;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Set the Backlight operation mode
* @inputs pData: pointer to the Backlight mode
* @inputs index: specified thermostat index (don't care)
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/14
*******************************************************************************/
DB_SETFUNC(Set_Backlight )
{
    uint32_t *backlight;

    backlight = (uint32_t *)pData;

    thisThermostat.Backlight = *backlight;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Set the Onboarding Done state
* @inputs pData: pointer to the Onboarding Done state
* @inputs index: specified thermostat index (don't care)
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2017/08/28
*******************************************************************************/
DB_SETFUNC(Set_OnboardingDone )
{
    uint8_t *onboardingDone;

    onboardingDone = (uint8_t *)pData;

    Onboarding.OnboardingDone = *onboardingDone;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Get the Backlight operation mode
* @inputs pData: pointer to the Backlight mode
* @inputs index: specified thermostat index (don't care)
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/14
*******************************************************************************/
DB_GETFUNC(Get_Backlight )
{
    uint32_t *backlight;

    backlight = (uint32_t *)pData;

    *backlight = thisThermostat.Backlight;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Get the Off Mode status
* @inputs pData: pointer to the Off Mode status
* @inputs index: specified thermostat index (don't care)
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/05/03
*******************************************************************************/
DB_GETFUNC(Get_OffMode )
{
    uint8_t *offMode;

    offMode = (uint8_t *)pData;

    *offMode = thisThermostat.OffMode;

    return DBERR_OK;
}


/*******************************************************************************
* @brief  Get the Floor Type
* @inputs pData: pointer to the Floor Type
* @inputs index: specified thermostat index (don't care)
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/07/16
*******************************************************************************/
DB_GETFUNC(Get_FloorType )
{
    uint8_t *floorType;

    floorType = (uint8_t *)pData;

    *floorType = thisThermostat.FloorType;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Get the Ambient Temperature sensor reading
* @inputs pData: pointer to the ambient temperature reading
* @inputs index: don't care
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/11/23
*******************************************************************************/
DB_GETFUNC(Get_AmbientTemperatureReading )
{
    int16_t *ambientTempReading;

    ambientTempReading = (int16_t*)pData;

    *ambientTempReading = SensorReadings.AmbientTemperatureReading;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Get the Compensation Temperature sensor reading
* @inputs pData: pointer to the compensation temperature reading
* @inputs index: don't care
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/11/23
*******************************************************************************/
DB_GETFUNC(Get_CompTemperatureReading )
{
    int16_t *compTempReading;

    compTempReading = (int16_t*)pData;

    *compTempReading = SensorReadings.CompensationTemperatureReading;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Get the High Temperature sensor reading
* @inputs pData: pointer to the high temperature reading
* @inputs index: don't care
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2017/01/31
*******************************************************************************/
DB_GETFUNC(Get_HighTemperatureReading )
{
    int16_t *highTempReading;

    highTempReading = (int16_t*)pData;

    *highTempReading = SensorReadings.HighTemperatureReading;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Get the temperature used for the control calculations
* @inputs pData: pointer to the control temperature
* @inputs index: don't care
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/11/23
*******************************************************************************/
DB_GETFUNC(Get_ControlTemperature )
{
    int16_t *controlTemp;

    controlTemp = (int16_t*)pData;

    *controlTemp = SensorReadings.ControlTemperature;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Get the Onboading Done state
* @inputs pData: pointer to the Onboarding Done flag
* @inputs index: specified thermostat index (don't care)
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2017/08/28
*******************************************************************************/
DB_GETFUNC(Get_OnboardingDone )
{
    uint8_t *onboardingDone;

    onboardingDone = (uint8_t *)pData;

    *onboardingDone = Onboarding.OnboardingDone;

    return DBERR_OK;
}

#ifdef LOAD_CYCLING
DB_GETFUNC(Get_LoadCyclingMode)
{
    uint8_t *loadCyclingMode;

    loadCyclingMode = (uint8_t *)pData;

    *loadCyclingMode = thisThermostat.LoadCyclingMode;

    return DBERR_OK;
}

DB_GETFUNC(Get_LoadCyclingCount)
{
    uint16_t *loadCyclingCount;

    loadCyclingCount = (uint16_t *)pData;

    *loadCyclingCount = thisThermostat.LoadCyclingCount;

    return DBERR_OK;
}

DB_SETFUNC(Set_LoadCyclingMode)
{
    uint8_t *loadCyclingMode;

    loadCyclingMode = (uint8_t *)pData;

    thisThermostat.LoadCyclingMode = *loadCyclingMode;

    return DBERR_OK;
}

DB_SETFUNC(Set_LoadCyclingCount)
{
    uint16_t *loadCyclingCount;

    loadCyclingCount = (uint16_t *)pData;

    thisThermostat.LoadCyclingCount = *loadCyclingCount;

    return DBERR_OK;
}
#endif /* LOAD_CYCLING */


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
