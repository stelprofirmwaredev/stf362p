/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    class_Residence.c
* @date    2017/02/06
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include ".\DB\classes\inc\class_Residence.h"
#include ".\DB\classes\inc\class_Thermostats.h"


#include ".\DB\inc\THREAD_DBDataTypes.h"
#include ".\DB\inc\THREAD_DB.h"

#include "common_types.h"
#include "APP_SetpointManager.h"
#include ".\strings\inc\strings_tool.h"
#include ".\HAL\inc\MnuboDataPoints.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
dbType_Residence_t           CurrentResidence;
extern uint8_t                      PropertiesToUpdate[];

extern TimerHandle_t WeatherFeedTimeout;
extern uint8_t weatherFeedExpirationCnt;
#ifdef FREERTOS_SUPPORT
    #include "trcRecorder.h"
    extern char* dbEventChannel;
#endif

susbscriptionHandle_t hUpdateAnalyticNotif = DBTHRD_NOTIFHANDLE_NOTSET;
/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
extern uint8_t PropertyFetchInProgress;


/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
dbErr_t UpdateSetpoint (uint16_t *dbSetpoint, TEMPERATURE_C_t newSetpoint, uint8_t isFloorSetpoint, uint8_t isLocalTstat);
DB_NOTIFICATION(classRD_UpdateAnalytics);


/*******************************************************************************
*    Private functions definitions
*******************************************************************************/

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2017/02/14
*******************************************************************************/
DB_NOTIFICATION(classRD_UpdateAnalytics)
{
//    dbType_Alerts_t currentAlert;
//
//    if (DBERR_CONTROLLERREGISTRATION_DONE == status)
//    {
//        hMnuboDataPoint newMnuboDataPoint;
//        hMnuboEvent mnuboEvent;
//
//        newMnuboDataPoint = Mnubo_CreateNewDataPoint();
//        Mnubo_AddEventToDataPoint(newMnuboDataPoint,
//                                       MNUBO_EVENT_UPDATE_RESIDENCE,
//                                       MNUBO_OBJ_RESIDENCE);
//
//        DBTHRD_GetData(DBTYPE_ACTIVE_ALERTS, (void*)&currentAlert, THIS_THERMOSTAT);
//        if (currentAlert.ActiveAlerts[0] != 0)
//        {
//            mnuboEvent = Mnubo_AddEventToDataPoint(newMnuboDataPoint,
//                                       MNUBO_EVENT_UPDATE_SET_ALERT,
//                                       MNUBO_OBJ_TSTAT_BASE);
//
//            Mnubo_AddDataToEvent (mnuboEvent, MNUBO_DATA_ALERT);
//        }
//
//        Mnubo_AddObjectToDataPoint ( newMnuboDataPoint,
//                                       MNUBO_OBJ_RESIDENCE);
//
//        classTH_AddAllActiveTstatToMnuboDatapoint(newMnuboDataPoint);
//
//        ANALYTICS_SendDataPoint(newMnuboDataPoint);
//
//    }
}


/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/15
*******************************************************************************/
void InitResidenceInformations (void)
{
	CurrentResidence.TemperatureFormat = DEGREE_C;
    CurrentResidence.TimeFormat = TIME_24H;
	CurrentResidence.DefaultSetpoint = DEFAULT_HOME_SETPOINT;
    CurrentResidence.ScheduleMode = MODE_PROG;
    CurrentResidence.ActiveProgram = PGM_WAKE;
    CurrentResidence.WakeSetpoint = 2100;
    CurrentResidence.LeaveSetpoint = 1600;
    CurrentResidence.ReturnSetpoint = 2100;
    CurrentResidence.SleepSetpoint = 1700;
    for (uint8_t pgm = 0; pgm < 20; pgm++)
    {
        CurrentResidence.FullSchedule.PgmEnabled[pgm] = TRUE;
        switch (pgm % 4)
        {
            case 0:
                CurrentResidence.FullSchedule.PgmStartTime[pgm] = 360;
                break;

            case 1:
                CurrentResidence.FullSchedule.PgmStartTime[pgm] = 480;
                break;

            case 2:
                CurrentResidence.FullSchedule.PgmStartTime[pgm] = 960;
                break;

            case 3:
                CurrentResidence.FullSchedule.PgmStartTime[pgm] = 1320;
                break;

            default:
                break;
        }
    }
    for (uint8_t pgm = 20; pgm < 28; pgm++)
    {
        if (((pgm % 4) == 0) || ((pgm % 4) == 3))
        {
            CurrentResidence.FullSchedule.PgmEnabled[pgm] = TRUE;
        }
        else
        {
            CurrentResidence.FullSchedule.PgmEnabled[pgm] = FALSE;
        }
        switch (pgm % 4)
        {
            case 0:
                CurrentResidence.FullSchedule.PgmStartTime[pgm] = 420;
                break;

            case 1:
                CurrentResidence.FullSchedule.PgmStartTime[pgm] = 540;
                break;

            case 2:
                CurrentResidence.FullSchedule.PgmStartTime[pgm] = 900;
                break;

            case 3:
                CurrentResidence.FullSchedule.PgmStartTime[pgm] = 1380;
                break;

            default:
                break;
        }
    }
}

/*******************************************************************************
* @brief  Set the Temperature format
* @inputs pData: pointer to the Temperature format
* @inputs index: specified thermostat index (don't care)
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/14
*******************************************************************************/
DB_SETFUNC(Set_TemperatureFormat )
{
    uint8_t *format;

    format = (uint8_t *)pData;

    CurrentResidence.TemperatureFormat = *format;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Set the Time format
* @inputs pData: pointer to the Time format
* @inputs index: specified thermostat index (don't care)
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/14
*******************************************************************************/
DB_SETFUNC(Set_TimeFormat )
{
    uint8_t *format;

    format = (uint8_t *)pData;

    CurrentResidence.TimeFormat = *format;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Set the Schedule mode
* @inputs pData: pointer to the schedule mode
* @inputs index: specified thermostat index (don't care)
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/07/31
*******************************************************************************/
DB_SETFUNC(Set_ScheduleMode )
{
    ScheduleModeEnum_t *mode;

    mode = (ScheduleModeEnum_t *)pData;

    CurrentResidence.ScheduleMode = *mode;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Set the Active Program
* @inputs pData: pointer to the active program
* @inputs index: specified thermostat index (don't care)
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/08/01
*******************************************************************************/
DB_SETFUNC(Set_ActiveProgram )
{
    Program_t *pgm;

    pgm = (Program_t *)pData;

    CurrentResidence.ActiveProgram = *pgm;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Set the Wake Setpoint
* @inputs pData: pointer to the wake setpoint
* @inputs index: specified thermostat index (don't care)
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/08/02
*******************************************************************************/
DB_SETFUNC(Set_WakeSetpoint )
{
    Setpoint100_t *setpoint;

    setpoint = (Setpoint100_t *)pData;

    CurrentResidence.WakeSetpoint = *setpoint;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Set the Leave Setpoint
* @inputs pData: pointer to the leave setpoint
* @inputs index: specified thermostat index (don't care)
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/08/02
*******************************************************************************/
DB_SETFUNC(Set_LeaveSetpoint )
{
    Setpoint100_t *setpoint;

    setpoint = (Setpoint100_t *)pData;

    CurrentResidence.LeaveSetpoint = *setpoint;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Set the Return Setpoint
* @inputs pData: pointer to the return setpoint
* @inputs index: specified thermostat index (don't care)
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/08/02
*******************************************************************************/
DB_SETFUNC(Set_ReturnSetpoint )
{
    Setpoint100_t *setpoint;

    setpoint = (Setpoint100_t *)pData;

    CurrentResidence.ReturnSetpoint = *setpoint;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Set the Sleep Setpoint
* @inputs pData: pointer to the sleep setpoint
* @inputs index: specified thermostat index (don't care)
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/08/02
*******************************************************************************/
DB_SETFUNC(Set_SleepSetpoint )
{
    Setpoint100_t *setpoint;

    setpoint = (Setpoint100_t *)pData;

    CurrentResidence.SleepSetpoint = *setpoint;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Set the Wake Schedule
* @inputs pData: pointer to the wake schedule
* @inputs index: don't care
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/08/06
*******************************************************************************/
DB_SETFUNC(Set_WakeSchedule )
{
    PgmSchedule_t *pgm_schedule;

    pgm_schedule = (PgmSchedule_t*)pData;

    for (uint8_t day = 0; day < 7; day++)
    {
        CurrentResidence.FullSchedule.PgmEnabled[day * 4] = pgm_schedule->Day[day].ScheduleEnabled;
        CurrentResidence.FullSchedule.PgmStartTime[day * 4] = pgm_schedule->Day[day].ScheduleTime;
    }

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Set the Leave Schedule
* @inputs pData: pointer to the leave schedule
* @inputs index: don't care
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/08/06
*******************************************************************************/
DB_SETFUNC(Set_LeaveSchedule )
{
    PgmSchedule_t *pgm_schedule;

    pgm_schedule = (PgmSchedule_t*)pData;

    for (uint8_t day = 0; day < 7; day++)
    {
        CurrentResidence.FullSchedule.PgmEnabled[day * 4 + 1] = pgm_schedule->Day[day].ScheduleEnabled;
        CurrentResidence.FullSchedule.PgmStartTime[day * 4 + 1] = pgm_schedule->Day[day].ScheduleTime;
    }

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Set the Return Schedule
* @inputs pData: pointer to the return schedule
* @inputs index: don't care
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/08/06
*******************************************************************************/
DB_SETFUNC(Set_ReturnSchedule )
{
    PgmSchedule_t *pgm_schedule;

    pgm_schedule = (PgmSchedule_t*)pData;

    for (uint8_t day = 0; day < 7; day++)
    {
        CurrentResidence.FullSchedule.PgmEnabled[day * 4 + 2] = pgm_schedule->Day[day].ScheduleEnabled;
        CurrentResidence.FullSchedule.PgmStartTime[day * 4 + 2] = pgm_schedule->Day[day].ScheduleTime;
    }

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Set the Sleep Schedule
* @inputs pData: pointer to the sleep schedule
* @inputs index: don't care
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/08/06
*******************************************************************************/
DB_SETFUNC(Set_SleepSchedule )
{
    PgmSchedule_t *pgm_schedule;

    pgm_schedule = (PgmSchedule_t*)pData;

    for (uint8_t day = 0; day < 7; day++)
    {
        CurrentResidence.FullSchedule.PgmEnabled[day * 4 + 3] = pgm_schedule->Day[day].ScheduleEnabled;
        CurrentResidence.FullSchedule.PgmStartTime[day * 4 + 3] = pgm_schedule->Day[day].ScheduleTime;
    }

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Get the temperature format
* @inputs pData: pointer to the temperature format
* @inputs index: specified thermostat index (don't care)
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/14
*******************************************************************************/
DB_GETFUNC(Get_TemperatureFormat )
{
    uint8_t *format;

    format = (uint8_t *)pData;

    *format = CurrentResidence.TemperatureFormat;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Get the time format
* @inputs pData: pointer to the time format
* @inputs index: specified thermostat index (don't care)
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/14
*******************************************************************************/
DB_GETFUNC(Get_TimeFormat )
{
    uint8_t *format;

    format = (uint8_t *)pData;

    *format = CurrentResidence.TimeFormat;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Get the schedule mode
* @inputs pData: pointer to the schedule mode
* @inputs index: specified thermostat index (don't care)
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/07/31
*******************************************************************************/
DB_GETFUNC(Get_ScheduleMode )
{
    ScheduleModeEnum_t *mode;

    mode = (ScheduleModeEnum_t *)pData;

    *mode = CurrentResidence.ScheduleMode;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Get the active program
* @inputs pData: pointer to the active program
* @inputs index: specified thermostat index (don't care)
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/08/01
*******************************************************************************/
DB_GETFUNC(Get_ActiveProgram )
{
    Program_t *pgm;

    pgm = (Program_t *)pData;

    *pgm = CurrentResidence.ActiveProgram;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Get the wake setpoint
* @inputs pData: pointer to the wake setpoint
* @inputs index: specified thermostat index (don't care)
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/08/02
*******************************************************************************/
DB_GETFUNC(Get_WakeSetpoint )
{
    Setpoint100_t *setpoint;

    setpoint = (Setpoint100_t *)pData;

    *setpoint = CurrentResidence.WakeSetpoint;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Get the leave setpoint
* @inputs pData: pointer to the leave setpoint
* @inputs index: specified thermostat index (don't care)
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/08/02
*******************************************************************************/
DB_GETFUNC(Get_LeaveSetpoint )
{
    Setpoint100_t *setpoint;

    setpoint = (Setpoint100_t *)pData;

    *setpoint = CurrentResidence.LeaveSetpoint;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Get the return setpoint
* @inputs pData: pointer to the return setpoint
* @inputs index: specified thermostat index (don't care)
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/08/02
*******************************************************************************/
DB_GETFUNC(Get_ReturnSetpoint )
{
    Setpoint100_t *setpoint;

    setpoint = (Setpoint100_t *)pData;

    *setpoint = CurrentResidence.ReturnSetpoint;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Get the sleep setpoint
* @inputs pData: pointer to the sleep setpoint
* @inputs index: specified thermostat index (don't care)
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/08/02
*******************************************************************************/
DB_GETFUNC(Get_SleepSetpoint )
{
    Setpoint100_t *setpoint;

    setpoint = (Setpoint100_t *)pData;

    *setpoint = CurrentResidence.SleepSetpoint;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Get the Wake Schedule
* @inputs pData: pointer to the wake schedule
* @inputs index: don't care
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/08/06
*******************************************************************************/
DB_GETFUNC(Get_WakeSchedule )
{
    PgmSchedule_t *pgm_schedule;

    pgm_schedule = (PgmSchedule_t*)pData;

    for (uint8_t day = 0; day < 7; day++)
    {
        pgm_schedule->Day[day].ScheduleEnabled = CurrentResidence.FullSchedule.PgmEnabled[day * 4];
        pgm_schedule->Day[day].ScheduleTime = CurrentResidence.FullSchedule.PgmStartTime[day * 4];
    }

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Get the Leave Schedule
* @inputs pData: pointer to the leave schedule
* @inputs index: don't care
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/08/06
*******************************************************************************/
DB_GETFUNC(Get_LeaveSchedule )
{
    PgmSchedule_t *pgm_schedule;

    pgm_schedule = (PgmSchedule_t*)pData;

    for (uint8_t day = 0; day < 7; day++)
    {
        pgm_schedule->Day[day].ScheduleEnabled = CurrentResidence.FullSchedule.PgmEnabled[day * 4 + 1];
        pgm_schedule->Day[day].ScheduleTime = CurrentResidence.FullSchedule.PgmStartTime[day * 4 + 1];
    }

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Get the Return Schedule
* @inputs pData: pointer to the return schedule
* @inputs index: don't care
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/08/06
*******************************************************************************/
DB_GETFUNC(Get_ReturnSchedule )
{
    PgmSchedule_t *pgm_schedule;

    pgm_schedule = (PgmSchedule_t*)pData;

    for (uint8_t day = 0; day < 7; day++)
    {
        pgm_schedule->Day[day].ScheduleEnabled = CurrentResidence.FullSchedule.PgmEnabled[day * 4 + 2];
        pgm_schedule->Day[day].ScheduleTime = CurrentResidence.FullSchedule.PgmStartTime[day * 4 + 2];
    }

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Get the Sleep Schedule
* @inputs pData: pointer to the sleep schedule
* @inputs index: don't care
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/08/06
*******************************************************************************/
DB_GETFUNC(Get_SleepSchedule )
{
    PgmSchedule_t *pgm_schedule;

    pgm_schedule = (PgmSchedule_t*)pData;

    for (uint8_t day = 0; day < 7; day++)
    {
        pgm_schedule->Day[day].ScheduleEnabled = CurrentResidence.FullSchedule.PgmEnabled[day * 4 + 3];
        pgm_schedule->Day[day].ScheduleTime = CurrentResidence.FullSchedule.PgmStartTime[day * 4 + 3];
    }

    return DBERR_OK;
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
