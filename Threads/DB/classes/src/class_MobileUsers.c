/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    class_MobileUsers.c
* @date    2017/02/06
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include ".\DB\classes\inc\class_MobileUsers.h"

#include ".\DB\inc\THREAD_DBDataTypes.h"
#include ".\DB\inc\THREAD_DB.h"

#include ".\strings\inc\strings_tool.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
dbType_AllUsers_t            RegisteredUsers;
extern uint8_t                      PropertiesToUpdate[];



/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2017/02/07
*******************************************************************************/
void InitMobileUser(void)
{
    for (uint8_t i = 0; i < USER_INSTANCES; i++)
    {
        RegisteredUsers.User[i].GeofencingMobileId[0] = NULL;
        RegisteredUsers.User[i].GeofencingMobileState = 1;
    }
}

/*******************************************************************************
* @brief  Set the Geofencing Mobile Id
* @inputs pData: pointer to the Geofencing Mobile Id
* @inputs index: specified user index
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
DB_SETFUNC(Set_GeofencingMobileId )
{
    dbErr_t status = DBERR_OK;

    if (index < USER_INSTANCES)
    {
        char *geofencingMobileId;
        
        geofencingMobileId = (char *)pData;

        stringNCopyWithNull((char*)RegisteredUsers.User[index].GeofencingMobileId,
                            geofencingMobileId,
                            sizeof(RegisteredUsers.User[index].GeofencingMobileId));
        
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    if (source == DBCHANGE_CLOUD)
    {
        PropertiesToUpdate[DBTYPE_GEOFENCING_MOBILE_ID] = 0;
    }

    return status;
}

/*******************************************************************************
* @brief  Set the Geofencing Mobile State
* @inputs pData: pointer to the Geofencing Mobile State
* @inputs index: specified user index
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
DB_SETFUNC(Set_GeofencingMobileState )
{
    dbErr_t status = DBERR_OK;
    uint8_t *geofencingMobileState;

    if (index < USER_INSTANCES)
    {
        geofencingMobileState = (uint8_t *)pData;

        RegisteredUsers.User[index].GeofencingMobileState = *geofencingMobileState;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    if (source == DBCHANGE_CLOUD)
    {
        PropertiesToUpdate[DBTYPE_GEOFENCING_MOBILE_STATE] = 0;
    }

    return status;
}


/*******************************************************************************
* @brief  Get the Geofencing Mobile Id
* @inputs pData: pointer to the Geofencing Mobile Id
* @inputs index: specified user index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
DB_GETFUNC(Get_GeofencingMobileId )
{
    dbErr_t status = DBERR_OK;
    char *geofencingMobileId;

    if (index < USER_INSTANCES)
    {
        geofencingMobileId = (char *)pData;

        //Make sure the string is NULL terminated
        RegisteredUsers.User[index].GeofencingMobileId[sizeof(RegisteredUsers.User[index].GeofencingMobileId) - 1] = NULL;
        strcpy(geofencingMobileId, (char const*)RegisteredUsers.User[index].GeofencingMobileId);
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief  Get the Geofencing Mobile State
* @inputs pData: pointer to the Geofencing Mobile State
* @inputs index: specified user index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
DB_GETFUNC(Get_GeofencingMobileState )
{
    dbErr_t status = DBERR_OK;
    uint8_t *geofencingMobileState;

    if (index < USER_INSTANCES)
    {
        geofencingMobileState = (uint8_t *)pData;

        *geofencingMobileState = RegisteredUsers.User[index].GeofencingMobileState;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
