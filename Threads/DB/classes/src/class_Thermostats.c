/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    class_Thermostats.c
* @date    2017/02/06
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include ".\DB\classes\inc\class_Thermostats.h"

#include ".\DB\inc\THREAD_DBDataTypes.h"
#include ".\DB\inc\THREAD_DB.h"

#include "APP_SetpointManager.h"
#include ".\HAL\inc\HAL_TemperatureAcquisition.h"
#include ".\strings\inc\strings_tool.h"
#include "tools.h"
#include "SwVersion.h"
/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define RELAY_CYCLE_COUNT_LIMIT     120000

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/


/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
dbType_AllThermostats_t      ConnectedThermostats;

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
dbErr_t UpdateSetpoint (uint16_t *dbSetpoint, TEMPERATURE_C_t newSetpoint, uint8_t isFloorSetpoint, uint8_t isLocalTstat);
dbErr_t CheckStringChange (uint8_t *dbString, char *newString);

static uint8_t GetHeatBars(uint8_t heatDemand);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  Set the FloorTemperature of the specified index
* @inputs pData: pointer to the Floor Temperature
* @inputs index: specified thermostat index
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/03/20
*******************************************************************************/
DB_SETFUNC(Set_FloorTemperature)
{
    int16_t *floorTemp;

    dbErr_t status = DBERR_OK;

    if (index < THERMOSTAT_INSTANCES)
    {
        floorTemp = (int16_t *)pData;

        ConnectedThermostats.Thermostat[index].FloorTemperature = *floorTemp;

        if (TEMP_IsSensorValid(ConnectedThermostats.Thermostat[index].FloorTemperature) == FALSE)
        {
            status = DBERR_INVALIDVALUE;
            ConnectedThermostats.Thermostat[index].DisplayedFloorTemperature =
                    ConnectedThermostats.Thermostat[index].FloorTemperature;
        }
        else
        {
            *floorTemp = TLS_Round((int16_t)*floorTemp,50);

            if (*floorTemp != ConnectedThermostats.Thermostat[index].DisplayedFloorTemperature)
            {
                status = DBERR_CHANGEABOVETHRESHOLD;
                ConnectedThermostats.Thermostat[index].DisplayedFloorTemperature = *floorTemp;
            }
        }
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief  Set the AmbientTemperature of the specified index
* @inputs pData: pointer to the Ambient Temperature
* @inputs index: specified thermostat index
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/09/29
*******************************************************************************/
DB_SETFUNC(Set_AmbientTemperature )
{
    int16_t *ambientTemp;

    dbErr_t status = DBERR_OK;

    if (index < THERMOSTAT_INSTANCES)
    {
        ambientTemp = (int16_t *)pData;

        ConnectedThermostats.Thermostat[index].AmbientTemperature = *ambientTemp;

        if (TEMP_IsSensorValid(ConnectedThermostats.Thermostat[index].AmbientTemperature) == FALSE)
        {
            status = DBERR_INVALIDVALUE;
            ConnectedThermostats.Thermostat[index].DisplayedAmbientTemperature =
                    ConnectedThermostats.Thermostat[index].AmbientTemperature;
        }
        else
        {
            *ambientTemp = TLS_Round((int16_t)*ambientTemp,50);

            if (*ambientTemp != ConnectedThermostats.Thermostat[index].DisplayedAmbientTemperature)
            {
                status = DBERR_CHANGEABOVETHRESHOLD;
                ConnectedThermostats.Thermostat[index].DisplayedAmbientTemperature = *ambientTemp;
            }
        }
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief  Set the AmbientSetpoint of the specified index
* @inputs pData: pointer to the Ambient Setpoint
* @inputs index: specified thermostat index
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/09/29
*******************************************************************************/
DB_SETFUNC(Set_FloorSetpoint )
{
    uint16_t *floorSetpoint;
    dbErr_t errStatus = DBERR_NOCHANGES;
    uint8_t localTstat;

    if (index < THERMOSTAT_INSTANCES)
    {
        floorSetpoint = (uint16_t *)pData;

        localTstat = 0;
        if (index == THIS_THERMOSTAT)
        {
            localTstat = 1;
        }
        errStatus  = UpdateSetpoint (&ConnectedThermostats.Thermostat[index].FloorSetpoint, *floorSetpoint, TRUE, localTstat);
    }
    else
    {
        errStatus = DBERR_INDEXOUTOFBOUND;
    }

    if ((errStatus == DBERR_SETPOINTHASCHANGED) && (index == THIS_THERMOSTAT))
    {
        DBTHRD_SetData(DBTYPE_USER_ACTION,(void*)0, INDEX_DONT_CARE, DBCHANGE_WAKEUP_FROM_SLEEP);
    }

    return errStatus;
}

/*******************************************************************************
* @brief  Set the AmbientSetpoint of the specified index
* @inputs pData: pointer to the Ambient Setpoint
* @inputs index: specified thermostat index
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/09/29
*******************************************************************************/
DB_SETFUNC(Set_AmbientSetpoint )
{
    uint16_t *ambientSetpoint;
    dbErr_t errStatus = DBERR_NOCHANGES;
    uint8_t localTstat;

    if (index < THERMOSTAT_INSTANCES)
    {
        ambientSetpoint = (uint16_t *)pData;

        localTstat = 0;
        if (index == THIS_THERMOSTAT)
        {
            localTstat = 1;
        }
        errStatus  = UpdateSetpoint (&ConnectedThermostats.Thermostat[index].AmbientSetpoint, *ambientSetpoint, FALSE, localTstat);

        if (DBERR_SETPOINTHASCHANGED == errStatus)
        {

        }
    }
    else
    {
        errStatus = DBERR_INDEXOUTOFBOUND;
    }

    if ((errStatus == DBERR_SETPOINTHASCHANGED) && (index == THIS_THERMOSTAT))
    {
        DBTHRD_SetData(DBTYPE_USER_ACTION,(void*)0, INDEX_DONT_CARE, DBCHANGE_WAKEUP_FROM_SLEEP);
    }

    return errStatus;
}

/*******************************************************************************
* @brief  Set the Heat Demand
* @inputs pData: pointer to the Heat Demand
* @inputs index: specified thermostat index
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
DB_SETFUNC(Set_HeatDemand )
{
    uint8_t *heatDemand;
    dbErr_t status = DBERR_OK;
    uint8_t displayedHeatBars;
    uint8_t newHeatBars;

    if (index < THERMOSTAT_INSTANCES)
    {
        heatDemand = (uint8_t *)pData;

        if (*heatDemand <= 100)
        {
            ConnectedThermostats.Thermostat[index].HeatDemand = *heatDemand;
        }
        else
        {
            ConnectedThermostats.Thermostat[index].HeatDemand = 0;
        }

        //Check current heat bars display
        displayedHeatBars = GetHeatBars(ConnectedThermostats.Thermostat[index].DisplayedHeatDemand);

        //Check new heat bars display
        newHeatBars = GetHeatBars(ConnectedThermostats.Thermostat[index].HeatDemand);

        if (displayedHeatBars != newHeatBars)
        {
            status = DBERR_CHANGEABOVETHRESHOLD;
            ConnectedThermostats.Thermostat[index].DisplayedHeatDemand =
                                ConnectedThermostats.Thermostat[index].HeatDemand;
        }
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief  Get the heat bars to display
* @inputs uint8_t: heat demand
* @retval uint8_t: number of heat bars to display
* @author Jean-Fran�ois Many
* @date 2017/12/05
*******************************************************************************/
static uint8_t GetHeatBars(uint8_t heatDemand)
{
    uint8_t heatBars = 0;

    if (heatDemand == 0)
    {
        heatBars = 0;
    }
    else if (heatDemand <= 20)
    {
        heatBars = 1;
    }
    else if (heatDemand <= 40)
    {
        heatBars = 2;
    }
    else if (heatDemand <= 60)
    {
        heatBars = 3;
    }
    else if (heatDemand <= 80)
    {
        heatBars = 4;
    }
    else
    {
        heatBars = 5;
    }
    return (heatBars);
}

/*******************************************************************************
* @brief  Set the Thermostat Version
* @inputs pData: pointer to the thermostat version
* @inputs index: thermostat instance
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2017/01/23
*******************************************************************************/
DB_SETFUNC(Set_TstatVersion )
{
    dbErr_t status = DBERR_OK;
    uint16_t *tstatVersion;

    if (index < THERMOSTAT_INSTANCES)
    {
        tstatVersion = (uint16_t *)pData;

        ConnectedThermostats.Thermostat[index].TstatVersion = *tstatVersion;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief  Set the Serial Number
* @inputs pData: pointer to the Serial Number
* @inputs index: thermostat instance
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2017/05/31
*******************************************************************************/
DB_SETFUNC(Set_SerialNumber )
{
    dbErr_t status = DBERR_NOCHANGES;
    char* serialNumber;

    if (index < THERMOSTAT_INSTANCES)
    {
        serialNumber = (char *)pData;

        status = CheckStringChange((uint8_t*)ConnectedThermostats.Thermostat[index].SerialNumber, serialNumber);

        if (status == DBERR_STRINGHASCHANGED)
        {
            stringNCopyWithNull(ConnectedThermostats.Thermostat[index].SerialNumber, serialNumber, sizeof(ConnectedThermostats.Thermostat[index].SerialNumber));
        }
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief  Set the Lock state
* @inputs pData: pointer to the Lock state
* @inputs index: specified thermostat index
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/14
*******************************************************************************/
DB_SETFUNC(Set_LockState )
{
    dbErr_t status = DBERR_OK;
    Lock_t *lock;

    if (index < THERMOSTAT_INSTANCES)
    {
        lock = (Lock_t *)pData;

        if (*lock < MAX_LOCK_STATE)
        {
            ConnectedThermostats.Thermostat[index].LockState = *lock;
        }
        else
        {
            status = DBERR_INVALIDVALUE;
        }
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief  Set the ActiveAlerts
* @inputs pData: pointer to the Active Alerts
* @inputs index: specified thermostat index
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/11
*******************************************************************************/
#define ALERT_NO_ACTION 0
#define ALERT_SET       1
#define ALERT_CLEAR     2
DB_SETFUNC(Set_ActiveAlerts )
{
    dbErr_t status = DBERR_NOCHANGES;
    uint8_t *activeAlerts;
    uint8_t i;
    uint8_t alertIsAlreadyActive;
    //uint8_t alertAction = ALERT_NO_ACTION;
//    hMnuboEvent mnuboEvent;
//    hMnuboObject mnuboObject;
//    uint8_t mustSendDataPoint = 0;

    if (index < THERMOSTAT_INSTANCES)
    {
        activeAlerts = (uint8_t *)pData;

        //Set an alert
        if (*activeAlerts & DB_SET_ALERT)
        {
            alertIsAlreadyActive = 0;
            for (i = 0; i < ALERT_INSTANCES; i++)
            {
                //Check if alert is already active
                if (ConnectedThermostats.Thermostat[index].Alerts.ActiveAlerts[i] == (*activeAlerts & 0x7F))
                {
                    alertIsAlreadyActive = 1;
                    break;
                }
            }
            if (alertIsAlreadyActive == 0)
            {
                for (i = 0; i < ALERT_INSTANCES; i++)
                {
                    if (ConnectedThermostats.Thermostat[index].Alerts.ActiveAlerts[i] == NULL)
                    {
                        ConnectedThermostats.Thermostat[index].Alerts.ActiveAlerts[i] = *activeAlerts & 0x7F;
                        break;
                    }
                }
                ConnectedThermostats.Thermostat[index].Alerts.LastAlertAction = DB_SET_ALERT;
                ConnectedThermostats.Thermostat[index].Alerts.LastAlertId = *activeAlerts & 0x7F;
//                alertAction = ALERT_SET;
                status = DBERR_OK;

                if (((*activeAlerts & 0x7F) == DBALERT_ZIGBEE_ERROR) || ((*activeAlerts & 0x7F) == DBALERT_NO_ZIGBEE_NETWORK))
                {
//                    InitZigBee();
                }
            }
        }
        //Clear an alert
        else
        {
            for (i = 0; i < ALERT_INSTANCES; i++)
            {
                if ((ConnectedThermostats.Thermostat[index].Alerts.ActiveAlerts[i] == (*activeAlerts & 0x7F)) || (DBALERT_MAX_ALERT == (*activeAlerts & 0x7F)))
                {
                    ConnectedThermostats.Thermostat[index].Alerts.ActiveAlerts[i] = 0;
                    ConnectedThermostats.Thermostat[index].Alerts.LastAlertAction = DB_CLEAR_ALERT;
                    ConnectedThermostats.Thermostat[index].Alerts.LastAlertId = *activeAlerts & 0x7F;
//                    alertAction = ALERT_CLEAR;
                    status = DBERR_OK;
                    break;
                }
            }
            //Reorder the alerts
            for (i = 0; i < (ALERT_INSTANCES - 1); i++)
            {
                if (ConnectedThermostats.Thermostat[index].Alerts.ActiveAlerts[i] == 0)
                {
                    ConnectedThermostats.Thermostat[index].Alerts.ActiveAlerts[i] = ConnectedThermostats.Thermostat[index].Alerts.ActiveAlerts[i+1];
                    ConnectedThermostats.Thermostat[index].Alerts.ActiveAlerts[i+1] = 0;
                }
            }
        }
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
DB_SETFUNC(Set_FloorMode )
{
    dbErr_t status = DBERR_NOCHANGES;
    uint8_t *floorMode;

    if (index < THERMOSTAT_INSTANCES)
    {
        floorMode = (uint8_t *)pData;

        if (ConnectedThermostats.Thermostat[index].FloorMode != *floorMode)
        {
            status = DBERR_STRINGHASCHANGED;
        }
        ConnectedThermostats.Thermostat[index].FloorMode = *floorMode;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
DB_SETFUNC(Set_FloorLimit )
{
    dbErr_t status = DBERR_NOCHANGES;
    uint16_t *floorLimit;

    if (index < THERMOSTAT_INSTANCES)
    {
        floorLimit = (uint16_t *)pData;

        if (ConnectedThermostats.Thermostat[index].FloorLimit != *floorLimit)
        {
            status = DBERR_STRINGHASCHANGED;
        }
        ConnectedThermostats.Thermostat[index].FloorLimit = *floorLimit;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
DB_SETFUNC(Set_RelayCycleCount )
{
    dbErr_t status = DBERR_NOCHANGES;
    uint32_t *relayCycleCount;
//    uint8_t alert;

    if (index < THERMOSTAT_INSTANCES)
    {
        relayCycleCount = (uint32_t *)pData;

        if (ConnectedThermostats.Thermostat[index].RelayCycleCount != *relayCycleCount)
        {
            status = DBERR_STRINGHASCHANGED;
        }
        ConnectedThermostats.Thermostat[index].RelayCycleCount = *relayCycleCount;
//TODO - If we want to set a Relay Wear Out alert based on Relay Cycle Count...
//        if (ConnectedThermostats.Thermostat[index].RelayCycleCount >= RELAY_CYCLE_COUNT_LIMIT)
//        {
//            alert = DB_SET_ALERT | DBALERT_RELAYS_WEARING_OUT;
//        }
//        else
//        {
//            alert = DB_CLEAR_ALERT | DBALERT_RELAYS_WEARING_OUT;
//        }
//        DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, index, source);
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}


#ifdef CALIBRATION_POWER_STEPS
#include <stdlib.h>
typedef struct
{
    uint32_t time;
    int8_t dutycycle;
} step_table_t;

TimerHandle_t CalibrationStepsTimer=0;
static uint8_t step = 0;
static uint8_t reload_timer = 1;
step_table_t step_table[] =
{
    {.time = (60*1000), .dutycycle = 50},
    {.time = (3*60*60*1000), .dutycycle = 0},
    {.time = (3*60*60*1000), .dutycycle = 20},
    {.time = (3*60*60*1000), .dutycycle = 100},
    {.time = 0, .dutycycle = 60},
};

/*******************************************************************************
*******************************************************************************/
void calibration_step (TimerHandle_t xTimer)
{
    step++;
    if (step_table[step].time > 0)
    {
        reload_timer = 1;
    }
}

/*******************************************************************************
* @brief  Get the Heat Demand
* @inputs pData: pointer to the Heat Demand
* @inputs index: specified thermostat index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
DB_GETFUNC(Get_HeatDemand )
{
    dbErr_t status = DBERR_OK;
    uint8_t *heatDemand;
    static uint8_t srand_done = 0;

    if (0 == srand_done)
    {
        srand(100);
        srand_done = 1;
    }

    if (index < THERMOSTAT_INSTANCES)
    {
        heatDemand = (uint8_t *)pData;

        *heatDemand = step_table[step].dutycycle;
        if ((*heatDemand > 15) && (*heatDemand < 85))
        {
            int8_t random_offset = 15 - (rand()%30);
            *heatDemand += random_offset;
        }
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    if (0 != reload_timer)
    {
        reload_timer = 0;
        if (0==CalibrationStepsTimer)
        {
            CalibrationStepsTimer = xTimerCreate ("Step Timer",
                                         (step_table[step].time),
                                         pdFALSE,               //periodic
                                         (void*)0,
                                         calibration_step);
            xTimerStart(CalibrationStepsTimer, 0);
        }
        else
        {
            xTimerChangePeriod(CalibrationStepsTimer, (step_table[step].time), 0);
        }
    }
    return status;
}

#else

/*******************************************************************************
* @brief  Get the Heat Demand
* @inputs pData: pointer to the Heat Demand
* @inputs index: specified thermostat index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/17
*******************************************************************************/
DB_GETFUNC(Get_HeatDemand )
{
    dbErr_t status = DBERR_OK;
    uint8_t *heatDemand;

    if (index < THERMOSTAT_INSTANCES)
    {
        heatDemand = (uint8_t *)pData;

        *heatDemand = ConnectedThermostats.Thermostat[index].HeatDemand;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}
#endif

/*******************************************************************************
* @brief  Get the Serial Number
* @inputs pData: pointer to the Serial Number
* @inputs index: Thermostat index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2017/05/31
*******************************************************************************/
DB_GETFUNC(Get_SerialNumber )
{
    dbErr_t status = DBERR_OK;
    char *serialNumber;

    if (index < THERMOSTAT_INSTANCES)
    {
        serialNumber = (char *)pData;
        stringNCopyWithNull(serialNumber, ConnectedThermostats.Thermostat[index].SerialNumber, sizeof(ConnectedThermostats.Thermostat[index].SerialNumber));
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief  Get the lock state
* @inputs pData: pointer to the lock state
* @inputs index: specified thermostat index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/14
*******************************************************************************/
DB_GETFUNC(Get_LockState )
{
    dbErr_t status = DBERR_OK;
    uint8_t *lock;

    if (index < THERMOSTAT_INSTANCES)
    {
        lock = (uint8_t *)pData;

        *lock = ConnectedThermostats.Thermostat[index].LockState;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief  Get the ActiveAlerts
* @inputs pData: pointer to the Active Alerts
* @inputs index: specified thermostat index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/11
*******************************************************************************/
DB_GETFUNC(Get_ActiveAlerts )
{
    dbErr_t status = DBERR_OK;
    dbType_Alerts_t* activeAlerts;

    if (index < THERMOSTAT_INSTANCES)
    {
        activeAlerts = (dbType_Alerts_t *)pData;
        *activeAlerts = ConnectedThermostats.Thermostat[index].Alerts;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief  Get the AmbientTemperature of the specified index
* @inputs pData: pointer to the Ambient Temperature buffer
* @inputs index: specified thermostat index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/03
*******************************************************************************/
DB_GETFUNC(Get_AmbientTemperature )
{
    dbErr_t status = DBERR_OK;
    TEMPERATURE_C_t *ambientTemp;

    if (index < THERMOSTAT_INSTANCES)
    {
        ambientTemp = (int16_t *)pData;

        *ambientTemp = ConnectedThermostats.Thermostat[index].AmbientTemperature;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

#ifdef CALIBRATION_SETPOINT_STEPS
/*******************************************************************************
* @brief  Get the AmbientSetpoint of the specified index
* @inputs pData: pointer to the Ambient Setpoint buffer
* @inputs index: specified thermostat index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/05
*******************************************************************************/
DB_GETFUNC(Get_AmbientSetpoint )
{
    dbErr_t status = DBERR_OK;
    int16_t *ambientSetpoint;

    if (index < THERMOSTAT_INSTANCES)
    {
        ambientSetpoint = (int16_t *)pData;

        if (xTaskGetTickCount() < (TickType_t)21600000)
        {
            *ambientSetpoint  = 1800;
        }
        else
        {
            *ambientSetpoint  = 2200;
        }
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}
#else           //CALIBRATION_SETPOINT_STEPS
/*******************************************************************************
* @brief  Get the AmbientSetpoint of the specified index
* @inputs pData: pointer to the Ambient Setpoint buffer
* @inputs index: specified thermostat index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/05
*******************************************************************************/
DB_GETFUNC(Get_AmbientSetpoint )
{
    dbErr_t status = DBERR_OK;
    int16_t *ambientSetpoint;

    if (index < THERMOSTAT_INSTANCES)
    {
        ambientSetpoint = (int16_t *)pData;

        *ambientSetpoint = ConnectedThermostats.Thermostat[index].AmbientSetpoint;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

#endif  //CALIBRATION_SETPOINT_STEPS

/*******************************************************************************
* @brief  Get the FloorTemperature of the specified index
* @inputs pData: pointer to the Floor Temperature buffer
* @inputs index: specified thermostat index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/03/20
*******************************************************************************/
DB_GETFUNC(Get_FloorTemperature)
{
    dbErr_t status = DBERR_OK;
    TEMPERATURE_C_t *floorTemp;

    if (index < THERMOSTAT_INSTANCES)
    {
        floorTemp = (int16_t *)pData;

        *floorTemp = ConnectedThermostats.Thermostat[index].FloorTemperature;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief  Get the FloorSetpoint of the specified index
* @inputs pData: pointer to the Floor Setpoint buffer
* @inputs index: specified thermostat index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2019/03/20
*******************************************************************************/
DB_GETFUNC(Get_FloorSetpoint)
{
    dbErr_t status = DBERR_OK;
    int16_t *floorSetpoint;

    if (index < THERMOSTAT_INSTANCES)
    {
        floorSetpoint = (int16_t *)pData;

        *floorSetpoint = ConnectedThermostats.Thermostat[index].FloorSetpoint;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
DB_GETFUNC(Get_FloorMode )
{
    dbErr_t status = DBERR_OK;
    uint8_t *floorMode;

    if (index < THERMOSTAT_INSTANCES)
    {
        floorMode = (uint8_t *)pData;

        *floorMode = ConnectedThermostats.Thermostat[index].FloorMode;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
DB_GETFUNC(Get_FloorLimit )
{
    dbErr_t status = DBERR_OK;
    uint16_t *floorLimit;

    if (index < THERMOSTAT_INSTANCES)
    {
        floorLimit = (uint16_t *)pData;

        *floorLimit = ConnectedThermostats.Thermostat[index].FloorLimit;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
DB_GETFUNC(Get_RelayCycleCount )
{
    dbErr_t status = DBERR_OK;
    uint8_t *relayCycleCount;

    if (index < THERMOSTAT_INSTANCES)
    {
        relayCycleCount = (uint8_t *)pData;

        *relayCycleCount = ConnectedThermostats.Thermostat[index].RelayCycleCount;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
