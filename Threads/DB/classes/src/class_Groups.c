/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    class_Groups.c
* @date    2017/02/06
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include ".\DB\classes\inc\class_Groups.h"

#include ".\DB\inc\THREAD_DBDataTypes.h"
#include ".\DB\inc\THREAD_DB.h"

#include ".\strings\inc\strings_tool.h"

#include "APP_SetpointManager.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
dbType_AllGroups_t              DefinedGroups;
extern uint8_t                  PropertiesToUpdate[];
extern dbType_Residence_t       CurrentResidence;


/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
extern uint8_t PropertyFetchInProgress;


/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
dbErr_t UpdateSetpoint (uint16_t *dbSetpoint, TEMPERATURE_C_t newSetpoint, uint8_t isFloorSetpoint, uint8_t isLocalTstat);
uint8_t isGroupFilledOnlyWithTstatInFloorMode(uint8_t groupIndex);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
uint8_t group_is_active(uint8_t group_id)
{
    if (strlen((char const*)DefinedGroups.Group[group_id].GroupName) > 0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/15
*******************************************************************************/
void InitGroups (void)
{
	for (uint8_t i = 0; i < GROUP_INSTANCES; i++)
	{
		if (i == 0)
		{
			strcpy((char*)DefinedGroups.Group[i].GroupName, "Central");
			DefinedGroups.Group[i].GroupMembers.MembersList = 1;
			DefinedGroups.Group[i].GroupMembers.NewestMember = 1;
		}
		else
		{
			strcpy((char*)DefinedGroups.Group[i].GroupName, '\0');
			DefinedGroups.Group[i].GroupMembers.MembersList = 0;
			DefinedGroups.Group[i].GroupMembers.NewestMember = 0;
		}
		DefinedGroups.Group[i].GroupSetpoint = DEFAULT_HOME_SETPOINT;
		DefinedGroups.Group[i].GroupHomeSetpoint = DEFAULT_HOME_SETPOINT;
		DefinedGroups.Group[i].GroupAwaySetpoint = DEFAULT_AWAY_SETPOINT;
		DefinedGroups.Group[i].GroupVacationSetpoint = DEFAULT_VACATION_SETPOINT;
		DefinedGroups.Group[i].GroupStandbySetpoint = DEFAULT_STANDBY_SETPOINT;
	}
}

/*******************************************************************************
* @brief  Set the Group Name
* @inputs pData: pointer to the Group Name
* @inputs index: group index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/25
*******************************************************************************/
DB_SETFUNC(Set_GroupName )
{
    dbErr_t status = DBERR_NOCHANGES;
    char *groupName;

    if (index < GROUP_INSTANCES)
    {
        char previous_name[NAME_SIZE_AND_NULL];
        groupName = (char *)pData;

        strcpy(previous_name, (char const *)DefinedGroups.Group[index].GroupName);
        ConvertUTF8ToASCII(groupName,
                           (char*) DefinedGroups.Group[index].GroupName,
                           sizeof(DefinedGroups.Group[index].GroupName));

        if (0 != strcmp(previous_name, (const char*)DefinedGroups.Group[index].GroupName))
        {
            status = DBERR_STRINGHASCHANGED;

//            if (PropertyFetchInProgress == 0)
//            {
//                hMnuboObject mnuboObject;
//                uint8_t mustSendDataPoint = 0;
//
//                if (NULL == mnuboDataPoint)
//                {
//                    mnuboDataPoint = Mnubo_CreateNewDataPoint();
//                    mustSendDataPoint = 1;
//                }
//
//                if (0 != mustSendDataPoint)
//                {
//                    mnuboObject = Mnubo_AddObjectToDataPoint(mnuboDataPoint,
//                                                           (mnuboObjId_t)(MNUBO_OBJ_GROUP_BASE+index));
//                    Mnubo_AddDataToObject (mnuboObject, MNUBO_DATA_GROUP_NAME);
//                    ANALYTICS_SendDataPoint(mnuboDataPoint);
//                }
//            }
        }
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    if (source == DBCHANGE_CLOUD)
    {
        PropertiesToUpdate[DBTYPE_GROUP_NAME] = 0;
    }

    return status;
}

/*******************************************************************************
* @brief  Set the Group Members
* @inputs pData: pointer to the Group Members
* @inputs index: group index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/25
*******************************************************************************/
DB_SETFUNC(Set_GroupMembers )
{
    dbErr_t errStatus = DBERR_NOCHANGES;
    dbType_GroupMembers_t *groupMembers;
    uint16_t setpoint;

    if (index < GROUP_INSTANCES)
    {
        groupMembers = (dbType_GroupMembers_t *)pData;

        if (DefinedGroups.Group[index].GroupMembers.MembersList != groupMembers->MembersList)
        {
            /****************************************************/
            /*  to determine which bit has change, XOR the old  */
            /*  and the new value, then AND with the new value. */
            /*  The remaining bits set are the bits added to the*/
            /*  group.  By doing so, we parse only the new      */
            /*  members, not the leaving members of the group.  */
            /****************************************************/
            uint32_t groupMask = groupMembers->MembersList ^ DefinedGroups.Group[index].GroupMembers.MembersList;
            groupMask &= groupMembers->MembersList;

            DefinedGroups.Group[index].GroupMembers = *groupMembers;

            for (uint8_t i = 0; i < THERMOSTAT_INSTANCES; i++)
            {
                if (0 != (groupMask & (1<<i)))
                {
                    DBTHRD_SetData(DBTYPE_TSTAT_PARENT_GROUPID,(void*)&index, i, source);
                    DBTHRD_GetData(DBTYPE_GROUP_SETPOINT, &setpoint, index);
                    DBTHRD_SetData(DBTYPE_AMBIENT_SETPOINT,(void*)&setpoint, i, source);
                    DBTHRD_SetData(DBTYPE_FLOOR_SETPOINT,(void*)&setpoint, i, source);
                }
            }
            errStatus = DBERR_OK;
        }
    }
    else
    {
        errStatus = DBERR_INDEXOUTOFBOUND;
    }

    if (source == DBCHANGE_CLOUD)
    {
        PropertiesToUpdate[DBTYPE_GROUP_MEMBERS] = 0;
    }

    return errStatus;
}

/*******************************************************************************
* @brief  Set the Group Setpoint
* @inputs pData: pointer to the Group Setpoint
* @inputs index: group index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/25
*******************************************************************************/
DB_SETFUNC(Set_GroupSetpoint )
{
    int16_t *groupSetpoint;
    dbErr_t errStatus = DBERR_NOCHANGES;
    uint32_t *membersList;
    membersList = &DefinedGroups.Group[index].GroupMembers.MembersList;
    dbType_Time_t localTime;
    dbType_Date_t dbDate;
    int16_t time;

    if (index < GROUP_INSTANCES)
    {
        groupSetpoint = (int16_t *)pData;

        errStatus  = UpdateSetpoint (&DefinedGroups.Group[index].GroupSetpoint, *groupSetpoint, isGroupFilledOnlyWithTstatInFloorMode(index), FALSE);

        if (DBERR_SETPOINTHASCHANGED == errStatus)
        {
//            hMnuboEvent mnuboEvent;
//            hMnuboObject mnuboObject;
//            uint8_t mustSendDataPoint = 0;
//            if (group_is_active(index) && (PropertyFetchInProgress == 0))
//            {
//                if (NULL == mnuboDataPoint)
//                {
//                    mnuboDataPoint = Mnubo_CreateNewDataPoint();
//                    mustSendDataPoint = 1;
//                }
//
//                mnuboEvent = Mnubo_AddEventToDataPoint(mnuboDataPoint,
//                                                       MNUBO_EVENT_UPDATE_GROUP_SETPOINT,
//                                                       (mnuboObjId_t)(MNUBO_OBJ_GROUP_BASE+index));
//                switch (source)
//                {
//                    case DBCHANGE_LOCAL:
//                        Mnubo_AddDataToEvent (mnuboEvent, MNUBO_DATA_TRIGGER_LOCAL);
//                        break;
//
//                    case DBCHANGE_CLOUD:
//                        Mnubo_AddDataToEvent (mnuboEvent, MNUBO_DATA_TRIGGER_CLOUD);
//                        break;
//
//                    case DBCHANGE_MODECHANGE:
//                        Mnubo_AddDataToEvent (mnuboEvent, MNUBO_DATA_TRIGGER_MODE_CHANGE);
//                        break;
//
//                    case DBCHANGE_ACTIVITY_LOCAL:
//                        Mnubo_AddDataToEvent (mnuboEvent, MNUBO_DATA_TRIGGER_ACTIVITY_LOCAL);
//                        break;
//
//                    case DBCHANGE_ACTIVITY_MOBILE:
//                        Mnubo_AddDataToEvent (mnuboEvent, MNUBO_DATA_TRIGGER_ACTIVITY_MOBILE);
//                        break;
//
//                    case DBCHANGE_ACTIVITY_SCHEDULED:
//                        Mnubo_AddDataToEvent (mnuboEvent, MNUBO_DATA_TRIGGER_ACTIVITY_SCHEDULED);
//                        break;
//
//                    default:
//                        break;
//                }
//                Mnubo_AddDataToEvent (mnuboEvent, MNUBO_DATA_GROUP_SETPOINT);
//
//
//                mnuboObject = Mnubo_AddObjectToDataPoint(mnuboDataPoint,
//                                                       (mnuboObjId_t)(MNUBO_OBJ_GROUP_BASE+index));
//                Mnubo_AddDataToObject (mnuboObject, MNUBO_DATA_GROUP_SETPOINT);
//                Mnubo_AddDataToObject (mnuboObject, MNUBO_DATA_GROUP_NAME);
//            }

            for (uint8_t i = 0; i < THERMOSTAT_INSTANCES; i++)
            {
                if ((*membersList & (1<<i)) != 0)
                {
                    /* using DBTHRD_SetData to force some notification to happen (NVM) */
                    DBTHRD_SetData_wMnuboDataPoint(DBTYPE_AMBIENT_SETPOINT,(void*)groupSetpoint, i, source, mnuboDataPoint);
                    DBTHRD_SetData_wMnuboDataPoint(DBTYPE_FLOOR_SETPOINT,(void*)groupSetpoint, i, source, mnuboDataPoint);
                }
            }

//            if (0 != mustSendDataPoint)
//            {
//                mnuboObject = Mnubo_AddObjectToDataPoint(mnuboDataPoint,
//                                                   (mnuboObjId_t)(MNUBO_OBJ_RESIDENCE));
//                Mnubo_AddDataToObject (mnuboObject, MNUBO_DATA_RESIDENCE_STATE);
//                Mnubo_AddDataToObject (mnuboObject, MNUBO_DATA_OUTDOOR_TEMPERATURE);
//                Mnubo_AddDataToObject (mnuboObject, MNUBO_DATA_OUTDOOR_HUMIDITY);
//                ANALYTICS_SendDataPoint(mnuboDataPoint);
//            }

            //Trigger the group setpoint change for the Wi-Fi
            DBTHRD_SetData(DBTYPE_GROUP_SETPOINT_CHANGE, 0, INDEX_DONT_CARE, source);
            //If the group setpoint was changed via the cloud, send back the new ambiant and floor setpoint
            if (source == DBCHANGE_CLOUD)
            {
                DBTHRD_SetData(DBTYPE_AMBIENT_SETPOINT_CHANGE, 0, INDEX_DONT_CARE, DBCHANGE_LOCAL);
                DBTHRD_SetData(DBTYPE_FLOOR_SETPOINT_CHANGE, 0, INDEX_DONT_CARE, DBCHANGE_LOCAL);
            }

            if (MODE_HOME == CurrentResidence.CurrentHomeState)
            {
                DBTHRD_SetData(DBTYPE_GROUP_HOME_SETPOINT,
                                (void*)&DefinedGroups.Group[index].GroupSetpoint,
                                index,
                                DBCHANGE_LOCAL);
            }

            DBTHRD_GetData(DBTYPE_LOCALTIME, (void*)&localTime, INDEX_DONT_CARE);
            if (localTime.IsTimeSet)
            {
                DBTHRD_GetData(DBTYPE_DATE, (void*)&dbDate, INDEX_DONT_CARE);
                //Switch DayOfWeek format for the one used in the Cloud (Monday to Sunday)
                if (dbDate.DayOfWeek == 0)
                {
                    dbDate.DayOfWeek = 7;
                }
                time = localTime.MinutesOfDay + (1440 * (dbDate.DayOfWeek - 1));
                DBTHRD_SetData(DBTYPE_GROUP_SETPOINT_CHANGE_TIME, (void*)&time, index, DBCHANGE_LOCAL);
            }
        }
        else
        {
            //Group setpoint did not change
            //Do not apply if the group setpoint change is made from mobile app
            //Since all group setpoints are received via mobile app,
            //we don't want a single group setpoint change to force all groups
            //to apply their setpoint to all their members
            //But if the source is local or an activity or a mode change, go ahead!
            if (source != DBCHANGE_CLOUD)
            {
                //The group setpoint did not change, but individual thermostat could be set to a different setpoint
                //So we transfer the command to the thermostat class with the current trigger
                for (uint8_t i = 0; i < THERMOSTAT_INSTANCES; i++)
                {
                    if ((*membersList & (1<<i)) != 0)
                    {
                        /* using DBTHRD_SetData to force some notification to happen (NVM) */
                        DBTHRD_SetData_wMnuboDataPoint(DBTYPE_AMBIENT_SETPOINT,(void*)groupSetpoint, i, source, mnuboDataPoint);
                        DBTHRD_SetData_wMnuboDataPoint(DBTYPE_FLOOR_SETPOINT,(void*)groupSetpoint, i, source, mnuboDataPoint);
                    }
                }
            }
        }
    }
    else
    {
        errStatus = DBERR_INDEXOUTOFBOUND;
    }

    if (source == DBCHANGE_CLOUD)
    {
        PropertiesToUpdate[DBTYPE_GROUP_SETPOINT] = 0;
    }

    return errStatus;
}

/*******************************************************************************
* @brief  Set the Group Home Setpoint
* @inputs pData: pointer to the Group Home Setpoint
* @inputs index: group index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/12/08
*******************************************************************************/
DB_SETFUNC(Set_GroupHomeSetpoint )
{
    int16_t *groupHomeSetpoint;
    dbErr_t errStatus = DBERR_NOCHANGES;

    if (index < GROUP_INSTANCES)
    {
        groupHomeSetpoint = (int16_t *)pData;

        errStatus  = UpdateSetpoint (&DefinedGroups.Group[index].GroupHomeSetpoint, *groupHomeSetpoint, isGroupFilledOnlyWithTstatInFloorMode(index), FALSE);
    }
    else
    {
        errStatus = DBERR_INDEXOUTOFBOUND;
    }

    if (source == DBCHANGE_CLOUD)
    {
        PropertiesToUpdate[DBTYPE_GROUP_HOME_SETPOINT] = 0;
    }

    return errStatus;
}

/*******************************************************************************
* @brief  Set the Group Away Setpoint
* @inputs pData: pointer to the Group Away Setpoint
* @inputs index: group index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/25
*******************************************************************************/
DB_SETFUNC(Set_GroupAwaySetpoint )
{
    int16_t *groupAwaySetpoint;
    dbErr_t errStatus = DBERR_NOCHANGES;

    if (index < GROUP_INSTANCES)
    {
        groupAwaySetpoint = (int16_t *)pData;

        errStatus  = UpdateSetpoint (&DefinedGroups.Group[index].GroupAwaySetpoint, *groupAwaySetpoint, isGroupFilledOnlyWithTstatInFloorMode(index), FALSE);

        if (MODE_AWAY == CurrentResidence.CurrentHomeState)
        {
            DBTHRD_SetData(DBTYPE_GROUP_SETPOINT,
                           (void*)&DefinedGroups.Group[index].GroupAwaySetpoint,
                           index,
                           DBCHANGE_LOCAL);
        }
    }
    else
    {
        errStatus = DBERR_INDEXOUTOFBOUND;
    }

    if (source == DBCHANGE_CLOUD)
    {
        PropertiesToUpdate[DBTYPE_GROUP_AWAY_SETPOINT] = 0;
    }

    return errStatus;
}

/*******************************************************************************
* @brief  Set the Group Vacation Setpoint
* @inputs pData: pointer to the Group Vacation Setpoint
* @inputs index: group index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/25
*******************************************************************************/
DB_SETFUNC(Set_GroupVacationSetpoint )
{
    int16_t *groupVacationSetpoint;
    dbErr_t errStatus = DBERR_NOCHANGES;

    if (index < GROUP_INSTANCES)
    {
        groupVacationSetpoint = (int16_t *)pData;

        errStatus  = UpdateSetpoint (&DefinedGroups.Group[index].GroupVacationSetpoint, *groupVacationSetpoint, isGroupFilledOnlyWithTstatInFloorMode(index), FALSE);

        if (MODE_VACATION == CurrentResidence.CurrentHomeState)
        {
            DBTHRD_SetData(DBTYPE_GROUP_SETPOINT,
                           (void*)&DefinedGroups.Group[index].GroupVacationSetpoint,
                           index,
                           DBCHANGE_LOCAL);
        }
    }
    else
    {
        errStatus = DBERR_INDEXOUTOFBOUND;
    }

    if (source == DBCHANGE_CLOUD)
    {
        PropertiesToUpdate[DBTYPE_GROUP_VACATION_SETPOINT] = 0;
    }

    return errStatus;
}

/*******************************************************************************
* @brief  Set the Group Standby Setpoint
* @inputs pData: pointer to the Group Standby Setpoint
* @inputs index: group index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/25
*******************************************************************************/
DB_SETFUNC(Set_GroupStandbySetpoint )
{
    int16_t *groupStandbySetpoint;
    dbErr_t errStatus = DBERR_NOCHANGES;

    if (index < GROUP_INSTANCES)
    {
        groupStandbySetpoint = (int16_t *)pData;

        errStatus  = UpdateSetpoint (&DefinedGroups.Group[index].GroupStandbySetpoint, *groupStandbySetpoint, isGroupFilledOnlyWithTstatInFloorMode(index), FALSE);

        if (MODE_STANDBY == CurrentResidence.CurrentHomeState)
        {
            DBTHRD_SetData(DBTYPE_GROUP_SETPOINT,
                           (void*)&DefinedGroups.Group[index].GroupStandbySetpoint,
                           index,
                           DBCHANGE_LOCAL);
        }
    }
    else
    {
        errStatus = DBERR_INDEXOUTOFBOUND;
    }

    if (source == DBCHANGE_CLOUD)
    {
        PropertiesToUpdate[DBTYPE_GROUP_STANDBY_SETPOINT] = 0;
    }

    return errStatus;
}

/*******************************************************************************
* @brief  Set the Group Setpoint Change Time
* @inputs pData: pointer to the Group Setpoint Change Time
* @inputs index: Group index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2017/05/08
*******************************************************************************/
DB_SETFUNC(Set_GroupSetpointChangeTime)
{
    dbErr_t status = DBERR_OK;
    int16_t *groupSetpointChangeTime;

    if (index < GROUP_INSTANCES)
    {
        groupSetpointChangeTime = (int16_t *)pData;

        DefinedGroups.Group[index].GroupSetpointChangeTime = *groupSetpointChangeTime;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    if (source == DBCHANGE_CLOUD)
    {
        PropertiesToUpdate[DBTYPE_GROUP_SETPOINT_CHANGE_TIME] = 0;
    }

    return status;
}



/*******************************************************************************
* @brief  Get the Group Information
* @inputs pData: pointer to the Group Info
* @inputs index: Group index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/25
*******************************************************************************/
DB_GETFUNC(Get_GroupInformation)
{
    dbType_Group_t *groupInfo;
    dbErr_t errStatus = DBERR_INVALIDVALUE;

    groupInfo = (dbType_Group_t *)pData;

    if (index < GROUP_INSTANCES)
    {
        *groupInfo = DefinedGroups.Group[index];
        errStatus = DBERR_OK;
    }
    else
    {
        errStatus = DBERR_INDEXOUTOFBOUND;
    }

    return errStatus;
}

/*******************************************************************************
* @brief  Get the Group Setpoint Change Time
* @inputs pData: pointer to the Group Setpoint Change Time
* @inputs index: Group index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2017/05/08
*******************************************************************************/
DB_GETFUNC(Get_GroupSetpointChangeTime)
{
    dbErr_t status = DBERR_OK;
    uint16_t *groupSetpointChangeTime;

    if (index < GROUP_INSTANCES)
    {
        groupSetpointChangeTime = (uint16_t *)pData;

        *groupSetpointChangeTime = DefinedGroups.Group[index].GroupSetpointChangeTime;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief  Get the Group Name
* @inputs pData: pointer to the Group Name
* @inputs index: Group index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/25
*******************************************************************************/
DB_GETFUNC(Get_GroupName )
{
    dbErr_t status = DBERR_OK;
    char *groupName;

    if (index < GROUP_INSTANCES)
    {
        groupName = (char *)pData;

        //Make sure the string is NULL terminated
        DefinedGroups.Group[index].GroupName[sizeof(DefinedGroups.Group[index].GroupName) - 1] = NULL;
        strcpy(groupName, (char const*)DefinedGroups.Group[index].GroupName);
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief  Get the Group Name
* @inputs pData: pointer to the Group Name
* @inputs index: Group index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/25
*******************************************************************************/
DB_GETFUNC(Get_GroupNameUTF8 )
{
    dbErr_t status = DBERR_OK;
    char *groupName;

    if (index < GROUP_INSTANCES)
    {
        groupName = (char *)pData;

        //Make sure the string is NULL terminated
        DefinedGroups.Group[index].GroupName[sizeof(DefinedGroups.Group[index].GroupName) - 1] = NULL;
        ConvertASCIIToUTF8 ((char*)DefinedGroups.Group[index].GroupName, groupName, strlen((char const*)DefinedGroups.Group[index].GroupName));
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief  Get the first Active Group
* @inputs pData:
* @inputs index: Group index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/25
*******************************************************************************/
DB_GETFUNC(Get_FirstActiveGroup)
{
    uint8_t *groupIndex;
    dbErr_t errStatus = DBERR_OK;

    groupIndex = (uint8_t*)pData;

    for (*groupIndex = 0; *groupIndex < GROUP_INSTANCES; *groupIndex++)
    {
        if (*DefinedGroups.Group[*groupIndex].GroupName != NULL)
        {
            break;
        }
    }

    if (*groupIndex >= GROUP_INSTANCES)
    {
        errStatus = DBERR_NOACTIVEGROUP;
    }

    return (errStatus);
}

/*******************************************************************************
* @brief  Get the Group Members
* @inputs pData: pointer to the Group Members
* @inputs index: Group index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/25
*******************************************************************************/
DB_GETFUNC(Get_GroupMembers )
{
    dbErr_t status = DBERR_OK;
    dbType_GroupMembers_t *groupMembers;

    if (index < GROUP_INSTANCES)
    {
        groupMembers = (dbType_GroupMembers_t *)pData;

        *groupMembers = DefinedGroups.Group[index].GroupMembers;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief  Get the Group Setpoint
* @inputs pData: pointer to the Group Setpoint
* @inputs index: Group index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/25
*******************************************************************************/
DB_GETFUNC(Get_GroupSetpoint )
{
    Setpoint100_t *groupSetpoint;
    dbErr_t errStatus = DBERR_OK;

    groupSetpoint = (uint16_t *)pData;

    if (index < GROUP_INSTANCES)
    {
        //Check if group is undefined
        if (DefinedGroups.Group[index].GroupName[0] == NULL)
        {
            errStatus = DBERR_UNDEFINEDGROUP;
        }
        //Check if group is empty
        else if (0 == DefinedGroups.Group[index].GroupMembers.MembersList)
        {
            errStatus = DBERR_EMPTYGROUP;
        }

        //Return the group setpoint no matter the error code
        *groupSetpoint = DefinedGroups.Group[index].GroupSetpoint;
    }
    else
    {
        errStatus = DBERR_INDEXOUTOFBOUND;
    }

    return errStatus;
}

/*******************************************************************************
* @brief  Get the Group Home Setpoint
* @inputs pData: pointer to the Group Home Setpoint
* @inputs index: Group index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/12/08
*******************************************************************************/
DB_GETFUNC(Get_GroupHomeSetpoint )
{
    dbErr_t status = DBERR_OK;
    uint16_t *groupHomeSetpoint;

    if (index < GROUP_INSTANCES)
    {
        groupHomeSetpoint = (uint16_t *)pData;

        *groupHomeSetpoint = DefinedGroups.Group[index].GroupHomeSetpoint;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief  Get the Group Away Setpoint
* @inputs pData: pointer to the Group Away Setpoint
* @inputs index: Group index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/25
*******************************************************************************/
DB_GETFUNC(Get_GroupAwaySetpoint )
{
    dbErr_t status = DBERR_OK;
    uint16_t *groupAwaySetpoint;

    if (index < GROUP_INSTANCES)
    {
        groupAwaySetpoint = (uint16_t *)pData;

        *groupAwaySetpoint = DefinedGroups.Group[index].GroupAwaySetpoint;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief  Get the Group Vacation Setpoint
* @inputs pData: pointer to the Group Vacation Setpoint
* @inputs index: Group index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/25
*******************************************************************************/
DB_GETFUNC(Get_GroupVacationSetpoint )
{
    dbErr_t status = DBERR_OK;
    uint16_t *groupVacationSetpoint;

    if (index < GROUP_INSTANCES)
    {
        groupVacationSetpoint = (uint16_t *)pData;

        *groupVacationSetpoint = DefinedGroups.Group[index].GroupVacationSetpoint;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief  Get the Group Standby Setpoint
* @inputs pData: pointer to the Group Standby Setpoint
* @inputs index: Group index
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/25
*******************************************************************************/
DB_GETFUNC(Get_GroupStandbySetpoint )
{
    dbErr_t status = DBERR_OK;
    uint16_t *groupStandbySetpoint;

    if (index < GROUP_INSTANCES)
    {
        groupStandbySetpoint = (uint16_t *)pData;

        *groupStandbySetpoint = DefinedGroups.Group[index].GroupStandbySetpoint;
    }
    else
    {
        status = DBERR_INDEXOUTOFBOUND;
    }

    return status;
}

/*******************************************************************************
* @brief Function that checks if all thermostat within the group are in floor mode
* @inputs groupIndex: group index of the group to scan
* @retval TRUE / FALSE
* @author Jean-Fran�ois Many
* @date 13/05/2019
*******************************************************************************/
uint8_t isGroupFilledOnlyWithTstatInFloorMode(uint8_t groupIndex)
{
    dbType_GroupMembers_t groupMembers;
    uint8_t tstat;
    uint8_t floorMode;

    DBTHRD_GetData(DBTYPE_GROUP_MEMBERS, &groupMembers, groupIndex);
    for (tstat = 0; tstat < THERMOSTAT_INSTANCES; tstat++)
    {
        //Check group membership
        if (groupMembers.MembersList & (1<<tstat))
        {
            DBTHRD_GetData(DBTYPE_FLOOR_MODE, &floorMode, tstat);
            if (floorMode != CONTROLMODE_FLOOR)
            {
                return FALSE;
            }
        }
    }
    return TRUE;
}

/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
