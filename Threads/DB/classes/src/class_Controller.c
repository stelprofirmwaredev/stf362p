/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    class_Controller.c
* @date    2017/02/06
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include ".\DB\classes\inc\class_Controller.h"

#include ".\DB\inc\THREAD_DBDataTypes.h"
#include ".\DB\inc\THREAD_DB.h"


#include ".\HAL\inc\AylaDefinitions.h"
#include "SwVersion.h"
#include "common_types.h"

#include ".\strings\inc\strings_tool.h"

#include ".\HAL\inc\MnuboDataPoints.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
dbType_Date_t                SystemDate;
dbType_Time_t                SystemTime;
dbType_CtrlSettings_t        ControllerSettings;



/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2017/02/07
*******************************************************************************/
void InitController(void)
{
    ControllerSettings.Language = LANG_FRANCAIS;
    ControllerSettings.ControllerVersion = SW_VER;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/15
*******************************************************************************/
void InitSystemTimeAndDate (void)
{ 
    SystemDate.Day = 1;
    SystemDate.Month = 1;
    SystemDate.Year = 2019;

    SystemTime.Hours = 0;
    SystemTime.Minutes = 0;
    SystemTime.Seconds = 0;
    SystemTime.IsTimeSet = 0; 
    SystemTime.MinutesOfDay = 0;
    SystemTime.TimeZone = 0;
}

/*******************************************************************************
* @brief  Set the Controller Version
* @inputs pData: pointer to the Controller Version
* @inputs index: don't care
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/20
*******************************************************************************/
DB_SETFUNC(Set_ControllerVersion )
{
    uint16_t *ctrlVersion;

    ctrlVersion = (uint16_t *)pData;

    ControllerSettings.ControllerVersion = *ctrlVersion;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Set the system date
* @inputs pData: pointer to the Default Setpoint
* @inputs index: specified thermostat index (Don't care)
* @inputs source: change source
* @retval dbErr_t: error code
* @author J-F Simard
* @date 2016/10/11
*******************************************************************************/
DB_SETFUNC(Set_SystemDate )
{
    dbType_Date_t * currentDate;
    currentDate = (dbType_Date_t *)pData;


    if ((currentDate->Day <= 31) && (currentDate->Day > 0))
    {
        SystemDate.Day = currentDate->Day;
    }

    if ((currentDate->Month <= 12) && (currentDate->Month > 0))
    {
        SystemDate.Month = currentDate->Month;
    }

    if ((currentDate->Year > 1900) && (currentDate->Year < 2100))
    {
        SystemDate.Year = currentDate->Year;
    }

    if (currentDate->DayOfWeek <= 7)
    {
        SystemDate.DayOfWeek = currentDate->DayOfWeek;
    }

    if (currentDate->DayOfYear < 366)
    {
        SystemDate.DayOfYear = currentDate->DayOfYear;
    }

    return (DBERR_OK);
}

/*******************************************************************************
* @brief
* @inputs pData: pointer to the Default Setpoint
* @inputs index: specified thermostat index (Don't care)
* @inputs source: change source
* @retval dbErr_t: error code
* @author J-F Simard
* @date 2016/10/11
*******************************************************************************/
DB_SETFUNC(Set_LocalTime )
{
    dbType_Time_t * currentTime;
    currentTime = (dbType_Time_t *)pData;

    if ((currentTime->Hours <= 23) && (currentTime->Minutes <= 59) && (currentTime->Seconds <= 59))
    {
        SystemTime.Hours = currentTime->Hours;
        SystemTime.Minutes = currentTime->Minutes;
        SystemTime.Seconds = currentTime->Seconds;
        SystemTime.MinutesOfDay = (uint16_t)(SystemTime.Hours*60) + SystemTime.Minutes;
    }

    return (DBERR_OK);
}

/*******************************************************************************
* @brief
* @inputs pData: pointer to the DST Active bit
* @inputs index: specified thermostat index (Don't care)
* @inputs source: change source
* @retval dbErr_t: error code
* @author J-F Simard
* @date 2016/10/11
*******************************************************************************/
DB_SETFUNC(Set_DstActive )
{
    dbType_Time_t * currentTime;
    currentTime = (dbType_Time_t *)pData;

    SystemTime.IsDstActive = currentTime->IsDstActive;

    return (DBERR_OK);
}

/*******************************************************************************
* @brief  Set the Language
* @inputs pData: pointer to the Language
* @inputs index: specified thermostat index (don't care)
* @inputs source: change source
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/14
*******************************************************************************/
DB_SETFUNC(Set_Language )
{
    DisplayLang_t *language;

    language = (DisplayLang_t *)pData;

    ControllerSettings.Language = *language;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Get the Language
* @inputs pData: pointer to the Language
* @inputs index: specified thermostat index (don't care)
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/14
*******************************************************************************/
DB_GETFUNC(Get_Language )
{
    uint8_t *language;

    language = (uint8_t *)pData;

    *language = ControllerSettings.Language;

    return DBERR_OK;
}


/*******************************************************************************
* @brief  Get the system Time
* @inputs pData:
* @inputs index: specified thermostat index (Don't care)
* @retval dbErr_t: error code
* @author J-F. Simard
* @date 2016/10/12
*******************************************************************************/
DB_GETFUNC(Get_SystemTime )
{
    dbType_Time_t * currentTime;
    currentTime = (dbType_Time_t *)pData;

    *currentTime = SystemTime;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Get the system Time
* @inputs pData:
* @inputs index: specified thermostat index (Don't care)
* @retval dbErr_t: error code
* @author J-F. Simard
* @date 2016/10/12
*******************************************************************************/
DB_GETFUNC(Get_SystemDate )
{
    dbType_Date_t * currentDate;
    currentDate = (dbType_Date_t *)pData;

    *currentDate = SystemDate;

    return DBERR_OK;
}

/*******************************************************************************
* @brief  Get the Controller Version
* @inputs pData: pointer to the Controller Version
* @inputs index: don't care
* @retval dbErr_t: error code
* @author Jean-Fran�ois Many
* @date 2016/10/20
*******************************************************************************/
DB_GETFUNC(Get_ControllerVersion )
{
    uint16_t *ctrlVersion;

    ctrlVersion = (uint16_t *)pData;

    *ctrlVersion = ControllerSettings.ControllerVersion;

    return DBERR_OK;
}

/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
