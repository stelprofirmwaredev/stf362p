/*******************************************************************************
* @file    class_Thermostats.h
* @date    2017/02/06
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef class_Thermostats_H
#define class_Thermostats_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include ".\DB\inc\THREAD_DB.h"



/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define DELETE_ONE_TERMOSTAT    0
#define DELETE_ALL_TERMOSTAT    1


/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
DB_SETFUNC(Set_AmbientTemperature );
DB_SETFUNC(Set_AmbientSetpoint );
DB_SETFUNC(Set_FloorTemperature);
DB_SETFUNC(Set_FloorSetpoint);
DB_SETFUNC(Set_ActiveAlerts );
DB_SETFUNC(Set_AmbientHumidity );
DB_SETFUNC(Set_LockState );
DB_SETFUNC(Set_SystemLock);
DB_SETFUNC(Set_LockSetpointLimit );
DB_SETFUNC(Set_HeatDemand );
DB_SETFUNC(Set_ThermostatName );
DB_SETFUNC(Set_ThermostatModel );
DB_SETFUNC(Set_ZigBeeStatDescriptor );
DB_SETFUNC(Set_LocalDailyConsumption );
DB_SETFUNC(Set_TstatVersion );
DB_SETFUNC(Set_ZigBeeVersion );
DB_SETFUNC(Set_TstatParentGroupId );
DB_SETFUNC(Set_SerialNumber );
DB_SETFUNC(Set_FloorMode );
DB_SETFUNC(Set_FloorLimit );
DB_SETFUNC(Set_RelayCycleCount );

DB_GETFUNC(Get_ZigBeeStatDescriptor);
DB_GETFUNC(Get_StatIndexFromZBExtAddr );
DB_GETFUNC(Get_StatIndexFromZBShortAddr );
DB_GETFUNC(Get_AmbientTemperature );
DB_GETFUNC(Get_AmbientSetpoint );
DB_GETFUNC(Get_FloorTemperature);
DB_GETFUNC(Get_FloorSetpoint);
DB_GETFUNC(Get_ActiveAlerts );
DB_GETFUNC(Get_AmbientHumidity );
DB_GETFUNC(Get_LockState );
DB_GETFUNC(Get_SystemLock);
DB_GETFUNC(Get_LockSetpointLimit );
DB_GETFUNC(Get_HeatDemand );
DB_GETFUNC(Get_ThermostatName );
DB_GETFUNC(Get_ThermostatNameUTF8 );
DB_GETFUNC(Get_ThermostatModel );
DB_GETFUNC(Get_ThermostatModelUTF8 );
DB_GETFUNC(Get_LocalDailyConsumption );
DB_GETFUNC(Get_ZigBeeVersion );
DB_GETFUNC(Get_TstatParentGroupId );
DB_GETFUNC(Get_SerialNumber );
DB_GETFUNC(Get_FloorMode );
DB_GETFUNC(Get_FloorLimit );
DB_GETFUNC(Get_RelayCycleCount );

void classTH_DeleteThermostat (uint8_t index, uint8_t uniqueOrGroup);
void classTH_AddAllActiveTstatToMnuboDatapoint (hMnuboDataPoint mnuboDataPoint);

#endif /* class_Thermostats_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
