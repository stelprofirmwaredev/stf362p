/*******************************************************************************
* @file    class_Residence.h
* @date    2017/02/06
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef class_Residence_H
#define class_Residence_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include ".\DB\inc\THREAD_DB.h"


/*******************************************************************************
* Public constants definitions
*******************************************************************************/



/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
DB_SETFUNC(Set_TemperatureFormat );
DB_SETFUNC(Set_TimeFormat );
DB_SETFUNC(Set_ScheduleMode );
DB_SETFUNC(Set_ActiveProgram );
DB_SETFUNC(Set_WakeSetpoint );
DB_SETFUNC(Set_LeaveSetpoint );
DB_SETFUNC(Set_ReturnSetpoint );
DB_SETFUNC(Set_SleepSetpoint );
DB_SETFUNC(Set_WakeSchedule );
DB_SETFUNC(Set_LeaveSchedule );
DB_SETFUNC(Set_ReturnSchedule );
DB_SETFUNC(Set_SleepSchedule );

DB_GETFUNC(Get_TemperatureFormat );
DB_GETFUNC(Get_TimeFormat );
DB_GETFUNC(Get_ScheduleMode );
DB_GETFUNC(Get_ActiveProgram );
DB_GETFUNC(Get_WakeSetpoint );
DB_GETFUNC(Get_LeaveSetpoint );
DB_GETFUNC(Get_ReturnSetpoint );
DB_GETFUNC(Get_SleepSetpoint );
DB_GETFUNC(Get_WakeSchedule );
DB_GETFUNC(Get_LeaveSchedule );
DB_GETFUNC(Get_ReturnSchedule );
DB_GETFUNC(Get_SleepSchedule );

#endif /* class_Residence_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
