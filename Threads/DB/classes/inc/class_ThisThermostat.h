/*******************************************************************************
* @file    class_ThisThermostat.h
* @date    2017/02/06
* @authors J-F.Simard
* @brief
*******************************************************************************/

#ifndef class_ThisThermostat_H
#define class_ThisThermostat_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include ".\DB\inc\THREAD_DB.h"



/*******************************************************************************
* Public constants definitions
*******************************************************************************/



/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
DB_SETFUNC(Set_ResetCount );
DB_SETFUNC(Set_WiFiModuleVersion );
DB_SETFUNC(Set_HeatMode );
DB_SETFUNC(Set_LobbyMode );
DB_SETFUNC(Set_RecoveryHeatingCycle );
DB_SETFUNC(Set_OffMode );
DB_SETFUNC(Set_FloorType );
DB_SETFUNC(Set_OpenWindowEnable );
DB_SETFUNC(Set_Backlight );
DB_SETFUNC(Set_PIR );
DB_SETFUNC(Set_AmbientTemperatureReading );
DB_SETFUNC(Set_CompTemperatureReading );
DB_SETFUNC(Set_HighTemperatureReading );
DB_SETFUNC(Set_ControlTemperature );
DB_SETFUNC(Set_OnboardingDone );
DB_SETFUNC(Set_Diagnostic );
DB_SETFUNC(Set_DebugLevel );
#ifdef LOAD_CYCLING
DB_SETFUNC(Set_LoadCyclingMode);
DB_SETFUNC(Set_LoadCyclingCount);
#endif /* LOAD_CYCLING */

DB_GETFUNC(Get_ResetCount );
DB_GETFUNC(Get_WiFiModuleVersion );
DB_GETFUNC(Get_HeatMode );
DB_GETFUNC(Get_LobbyMode );
DB_GETFUNC(Get_RecoveryHeatingCycle );
DB_GETFUNC(Get_OffMode );
DB_GETFUNC(Get_FloorType );
DB_GETFUNC(Get_OpenWindowEnable );
DB_GETFUNC(Get_Backlight );
DB_GETFUNC(Get_PIR );
DB_GETFUNC(Get_AmbientTemperatureReading );
DB_GETFUNC(Get_CompTemperatureReading );
DB_GETFUNC(Get_HighTemperatureReading );
DB_GETFUNC(Get_ControlTemperature );
DB_GETFUNC(Get_OnboardingDone );
DB_GETFUNC(Get_Diagnostic );
DB_GETFUNC(Get_DebugLevel );
#ifdef LOAD_CYCLING
DB_GETFUNC(Get_LoadCyclingMode);
DB_GETFUNC(Get_LoadCyclingCount);
#endif /* LOAD_CYCLING */

#endif /* class_ThisThermostat_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
