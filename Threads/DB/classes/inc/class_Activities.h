/*******************************************************************************
* @file    class_Activities.h
* @date    2017/02/06
* @authors J-F. Simard
* @brief 
*******************************************************************************/

#ifndef class_Activities_H
#define class_Activities_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include ".\DB\inc\THREAD_DB.h"



/*******************************************************************************
* Public constants definitions
*******************************************************************************/



/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
DB_SETFUNC(Set_ActivityName );
DB_SETFUNC(Set_ActivityStartRequest );
DB_SETFUNC(Set_ActivityReadyAt );
DB_SETFUNC(Set_ActivitySetpointPerGroup );
DB_SETFUNC(Set_ActivityStartTime );
DB_SETFUNC(Set_ActivityWeekDays );

DB_GETFUNC(Get_ActivityName );
DB_GETFUNC(Get_ActivityNameUTF8 );
DB_GETFUNC(Get_ActivityStartRequest );
DB_GETFUNC(Get_ActivityReadyAt );
DB_GETFUNC(Get_ActivitySetpointPerGroup );
DB_GETFUNC(Get_ActivityStartTime );
DB_GETFUNC(Get_ActivityWeekDays );


#endif /* class_Activities_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
