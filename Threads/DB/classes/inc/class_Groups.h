/*******************************************************************************
* @file    class_Groups.h
* @date    2017/02/06
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef class_Groups_H
#define class_Groups_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include ".\DB\inc\THREAD_DB.h"



/*******************************************************************************
* Public constants definitions
*******************************************************************************/



/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
DB_SETFUNC(Set_HomeState );
DB_SETFUNC(Set_GroupName );
DB_SETFUNC(Set_GroupMembers );
DB_SETFUNC(Set_GroupSetpoint );
DB_SETFUNC(Set_GroupHomeSetpoint );
DB_SETFUNC(Set_GroupAwaySetpoint );
DB_SETFUNC(Set_GroupVacationSetpoint );
DB_SETFUNC(Set_GroupStandbySetpoint );
DB_SETFUNC(Set_GroupSetpointChangeTime);

DB_GETFUNC(Get_GroupName );
DB_GETFUNC(Get_GroupNameUTF8 );
DB_GETFUNC(Get_GroupMembers );
DB_GETFUNC(Get_GroupSetpoint );
DB_GETFUNC(Get_GroupHomeSetpoint );
DB_GETFUNC(Get_GroupAwaySetpoint );
DB_GETFUNC(Get_GroupVacationSetpoint );
DB_GETFUNC(Get_GroupStandbySetpoint );
DB_GETFUNC(Get_GroupInformation);
DB_GETFUNC(Get_GroupSetpointChangeTime);

DB_GETFUNC(Get_FirstActiveGroup);

#endif /* class_Groups_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
