/*******************************************************************************
* @file    class_Controller.h
* @date    2017/02/06
* @authors J-F. Simard
* @brief 
*******************************************************************************/

#ifndef class_Controller_H
#define class_Controller_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include ".\DB\inc\THREAD_DB.h"



/*******************************************************************************
* Public constants definitions
*******************************************************************************/



/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
DB_SETFUNC(Set_ConnectivityStatus );
DB_SETFUNC(Set_ActiveProfile );
DB_SETFUNC(Set_ProfileSsid );
DB_SETFUNC(Set_SystemDate );
DB_SETFUNC(Set_LocalTime );
DB_SETFUNC(Set_TimeZone );
DB_SETFUNC(Set_DstActive );
DB_SETFUNC(Set_DstChange );
DB_SETFUNC(Set_DstValid );
DB_SETFUNC(Set_Language );
DB_SETFUNC(Set_ZigBeeJoinInfo );
DB_SETFUNC(Set_ZigBeeNetworkInfo );
DB_SETFUNC(Set_ZigBeeAssociationPermit );
DB_SETFUNC(Set_ControllerVersion );
DB_SETFUNC(Set_oemHostVersion );
DB_SETFUNC(Set_WiFiState );
DB_SETFUNC(Set_DSN );


DB_GETFUNC(Get_ConnectivityStatus );
DB_GETFUNC(Get_ActiveProfile );
DB_GETFUNC(Get_SystemTime );
DB_GETFUNC(Get_SystemDate );
DB_GETFUNC(Get_Language );
DB_GETFUNC(Get_ZigBeeJoinInfo );
DB_GETFUNC(Get_ZigBeeNetworkInfo );
DB_GETFUNC(Get_ZigBeeAssociationPermit );
DB_GETFUNC(Get_ControllerVersion );
DB_GETFUNC(Get_oemHostVersion );
DB_GETFUNC(Get_WiFiState );
DB_GETFUNC(Get_DSN );


#endif /* class_Controller_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
