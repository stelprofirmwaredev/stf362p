/*******************************************************************************
* @file    THREAD_DBDataTypes.h
* @date    2016/09/14
* @authors J-F. Simard
* @brief
*
*   March 7 2018:
*       - Add a new dbChangeSource_t type <DBCHANGE_WAKEUP_FROM_SLEEP> for
*           use with <DBTYPE_USER_ACTION> data type
*******************************************************************************/

#ifndef THREAD_DBDataTypes_H
#define THREAD_DBDataTypes_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>
#include "BootLoader_OTA_Control.h"
#include ".\HAL\inc\AylaDefinitions.h"

/*******************************************************************************
* Public constants definitions
*******************************************************************************/
typedef enum
{
    DBTYPE_BOOTPROGRESSBAR,
    DBTYPE_ONBOARDING_DONE,                 //9

    //Residence data
    DBTYPE_TEMPERATURE_FORMAT,
    DBTYPE_TIME_FORMAT,
    DBTYPE_SCHEDULE_MODE,
    DBTYPE_ACTIVE_PGM,
    DBTYPE_WAKE_SETPOINT,
    DBTYPE_LEAVE_SETPOINT,
    DBTYPE_RETURN_SETPOINT,
    DBTYPE_SLEEP_SETPOINT,
    DBTYPE_WAKE_SCHEDULE,
    DBTYPE_LEAVE_SCHEDULE,
    DBTYPE_RETURN_SCHEDULE,
    DBTYPE_SLEEP_SCHEDULE,
    DBTYPE_DATE,                            //20
    DBTYPE_LOCALTIME,
    DBTYPE_DST_ACTIVE,
    DBTYPE_LANGUAGE_SETTING,
    DBTYPE_BACKLIGHT_SETTING,
    DBTYPE_AMBIENT_TEMPERATURE_READING,
    DBTYPE_COMPENSATION_TEMPERATURE_READING,
    DBTYPE_HIGH_TEMPERATURE_READING,
    DBTYPE_CONTROL_TEMPERATURE,
    DBTYPE_CONTROLLER_VERSION,
    //Thermostats data
    DBTYPE_AMBIENT_TEMPERATURE,
    DBTYPE_AMBIENT_SETPOINT,
    DBTYPE_FLOOR_TEMPERATURE,
    DBTYPE_FLOOR_SETPOINT,                  //50
    DBTYPE_ACTIVE_ALERTS,
    DBTYPE_LOCK_STATE,
    DBTYPE_FLOOR_MODE,
    DBTYPE_FLOOR_LIMIT,
    DBTYPE_HEAT_DEMAND,
    DBTYPE_TSTAT_VERSION,
    DBTYPE_SERIAL_NUMBER,                   //67

    DBTYPE_USER_ACTION,                     //88
    DBTYPE_MINUTE_CHANGE,
    DBTYPE_AMBIENT_SETPOINT_CHANGE,         //90
    DBTYPE_RESIDENCE_STATE_CHANGE,
    DBTYPE_FLOOR_SETPOINT_CHANGE,

    //Periodic report
    DBTYPE_AMBIENT_TEMPERATURE_REPORT,      //98
    DBTYPE_HEAT_DEMAND_REPORT,

    DBTYPE_RELAY_CYCLE_COUNT,
    DBTYPE_OFF_MODE_SETTING,                //110
    DBTYPE_FLOOR_TYPE,

#ifdef LOAD_CYCLING
    DBTYPE_LOAD_CYCLING_MODE,
    DBTYPE_LOAD_CYCLING_COUNT,
#endif /* LOAD_CYCLING */

    DBTYPE_MAXDATATYPE,                     //112




    DB_NOSUBSCRIPTION = 0xFF,
} dbDataTypes_t;

typedef enum
{
    DBCHANGE_NOCHANGES                = 0x00,
    DBCHANGE_LOCAL                    = (1<<0),
    DBCHANGE_CLOUD                    = (1<<1),
    DBCHANGE_ZIGBEE                   = (1<<2),
    DBCHANGE_OPENWINDOW               = (1<<3),
    DBCHANGE_MODECHANGE               = (1<<4),
    DBCHANGE_GEOFENCING               = (1<<5),
    DBCHANGE_REGISTER                 = (1<<6),
    DBCHANGE_ACTIVITY_LOCAL           = (1<<7),
    DBCHANGE_ACTIVITY_MOBILE          = (1<<8),
    DBCHANGE_ACTIVITY_SCHEDULED       = (1<<9),
    DBCHANGE_GROUPCHANGE              = (1<<10),
    DBCHANGE_NEWSTATADDED             = (1<<11),
    DBCHANGE_WAKEUP_FROM_SLEEP        = (1<<12),
    DBCHANGE_NETWORK_IS_SET           = (1<<13),

    DBCHANGE_ALLCHANGES               = 0xFFFF,
} dbChangeSource_t;

typedef enum
{
    DBERR_OK                        = 0,
    DBERR_CHANGEABOVETHRESHOLD,
    DBERR_INVALIDVALUE,
    DBERR_UNKNOWNSERVICE,
    DBERR_GENERICERROR,
    DBERR_STATSLOTMAX,
    DBERR_EXISTING_STAT,
    DBERR_NONEXISTING_STAT,
    DBERR_REINIT_STAT,
    DBERR_NOCHANGES,
    DBERR_NOACTIVEGROUP,
    DBERR_SETPOINTHASCHANGED,
    DBERR_STRINGHASCHANGED,
    DBERR_CONTROLLERREGISTRATION_DONE,
    DBERR_UPDATED,
    DBERR_NEWTSTATADDED,
    DBERR_INDEXOUTOFBOUND,
    DBERR_EMPTYGROUP,
    DBERR_UNDEFINEDGROUP,
    DBERR_INVALIDGROUPID,
    DBERR_WRONGVERSION,
} dbErr_t;


#define INDEX_DONT_CARE 0xFF
#define THIS_THERMOSTAT 0

#define THERMOSTAT_INSTANCES    1
#define ALERT_INSTANCES         5

#define DB_SENSOR_INVALID   (int16_t)(0x8000)   //Value representing no sensor installed, or too cold, or too hot, or acquisition incomplete
#define DB_SENSOR_LOW       (int16_t)(0x7FFD)   //LOW Temperature below Minimum temperature
#define DB_SENSOR_HIGH      (int16_t)(0x7FFF)   //HIGH Temperature higher than Maximum temperature

#define MODEL_SIZE_AND_NULL         33
#define NAME_SIZE_AND_NULL          17
#define SERIALNUMBER_SIZE_AND_NULL  15

typedef enum
{
    DBALERT_NO_ALERT = 0,
    DBALERT_NO_ZIGBEE,
    DBALERT_OPEN_WINDOW,
    DBALERT_OVERHEAT,
    DBALERT_GFCI,
    DBALERT_NO_WIFI,
    DBALERT_OTA_PENDING,
    DBALERT_ZIGBEE_ERROR,
    DBALERT_NO_ZIGBEE_NETWORK,
    DBALERT_SERVER_PROBLEM,
    DBALERT_END_OF_LIFE,
    DBALERT_FLOOR_TEMP_LIMIT_REACHED,
    DBALERT_RELAYS_WEARING_OUT,
    DBALERT_NO_FLOOR_SENSOR_AF,
    DBALERT_NO_FLOOR_SENSOR_F,
    DBALERT_TIME_NOT_SET,
    DBALERT_MAX_ALERT
} AlertID_t;

#define NOT_JOINED          0
#define JOINING             1
#define JOIN_SUCCESS        2
#define JOIN_FAILURE        3

#define DB_CLEAR_ALERT  0x00
#define DB_SET_ALERT    0x80

#define ON_DEMAND       -1

#define NUMBER_OF_PROGRAMS  28  //4 programs per day

/*******************************************************************************
* Public structures definitions
*******************************************************************************/
typedef uint16_t Setpoint100_t;
typedef int16_t TEMPERATURE_C_t;     // in 100th of degree C


/************************* DBTYPE_CLOUDCONNECTIVITY  ************************/
/****************************************************************************/
typedef struct
{
    uint8_t isConnectedToCloud;
    uint8_t isRegistered;
    uint8_t signalStrength;             //rssi expressed in 0 to 255.  Must be interpreted as 0 to -255
} dbType_CloudConnectivity_t;

#define CLOUD_UNREGISTERED  0
#define CLOUD_REGISTERED    1
#define CLOUD_UNKNOWN       2

/*************************** DBTYPE_BOOTPROGRESSBAR  ************************/
/****************************************************************************/
typedef BootProgress_t dbBootProgressBar_t;



/*************************** DBTYPE_LANGUAGE_SETTING  ***********************/
/*************************** DBTYPE_BACKLIGHT_SETTING  **********************/
typedef enum {
    LANG_FRANCAIS,
    LANG_ENGLISH,
} DisplayLang_t;

typedef enum {
    BL_60_SEC = 60000,
    BL_300_SEC = 300000,
    BL_600_SEC = 600000,
    BL_1800_SEC = 1800000,
} BacklightTimeout_t;

typedef struct
{
    DisplayLang_t Language;
    uint16_t ControllerVersion;
} dbType_CtrlSettings_t;

/*************************** DBTYPE_AMBIENT_TEMPERATURE_READING  ************/
/*************************** DBTYPE_COMPENSATION_TEMPERATURE_READING  *******/
/*************************** DBTYPE_HIGH_TEMPERATURE_READING  ***************/
typedef struct
{
    int16_t AmbientTemperatureReading;
    int16_t CompensationTemperatureReading;
    int16_t HighTemperatureReading;
    int16_t ControlTemperature;
} dbType_TemperatureReading_t;


/*************************** DBTYPE_DATE  ***********************************/
/****************************************************************************/
typedef struct
{
    uint16_t Year;
    uint8_t  Month;
    uint8_t  Day;
    uint8_t  DayOfWeek;
    uint16_t DayOfYear;
} dbType_Date_t;

// constants representing the month
enum {
	MNTH_INVALID = 0,
	MNTH_JAN = 1,   /* January */
	MNTH_FEB = 2,   /* Februrary */
	MNTH_MAR = 3,   /* March */
	MNTH_APR = 4,   /* April */
	MNTH_MAY = 5,   /* May */
	MNTH_JUN = 6,   /* June */
	MNTH_JUL = 7,   /* July */
	MNTH_AUG = 8,   /* August */
	MNTH_SEP = 9,   /* September */
	MNTH_OCT = 10,  /* October */
	MNTH_NOV = 11,  /* November */
	MNTH_DEC = 12   /* December */
};

/*************************** DBTYPE_LOCALTIME  ******************************/
/*  The Set Service for DBTYPE_LOCALTIME uses the variable MinutesOfDay as  */
/*  input, then reassign Hours and Minutes                                  */
/*  The get Services for DBTYPE_LOCALTIME returns the content of the whole  */
/*  structure.                                                              */
/****************************************************************************/
typedef struct
{
    uint8_t Hours;
    uint16_t Minutes;
    uint8_t Seconds;
    uint16_t MinutesOfDay;

    uint16_t TimeZone;              //expressed in minute west of UTC (e.g. Montreal = 300minutes)
    uint16_t IsDstActive;

    uint8_t IsTimeSet;
} dbType_Time_t;

typedef dbType_Time_t RtcTime_t;            //for legacy
typedef dbType_Date_t RtcDate_t;            //for legacy


/*************************** DBTYPE_ACTIVE_ALERTS  **************************/
/****************************************************************************/
typedef struct
{
    uint8_t ActiveAlerts[ALERT_INSTANCES];
    uint8_t LastAlertId;
    uint8_t LastAlertAction;
} dbType_Alerts_t;

typedef struct
{
    uint8_t tstat;
    uint8_t alertId;
}dbType_AlertDisplay_t;

typedef struct
{
    uint8_t OnboardingDone;
} dbType_Onboarding_t;

typedef enum {
    MODE_PROG = 1,
    MODE_MANUAL,
    MODE_DEROGATION,
} ScheduleModeEnum_t;

typedef enum {
    PGM_WAKE = 1,
    PGM_LEAVE,
    PGM_RETURN,
    PGM_SLEEP,
} Program_t;

typedef struct
{
    uint8_t ScheduleEnabled;
    uint16_t ScheduleTime;
} ScheduleDay_t;

typedef struct
{
    ScheduleDay_t Day[7];
    uint16_t EditTime;
} PgmSchedule_t;

typedef struct
{
    uint8_t PgmEnabled[NUMBER_OF_PROGRAMS];
    uint16_t PgmStartTime[NUMBER_OF_PROGRAMS];
} FullSchedule_t;

/*************************** DBTYPE_TEMPERATURE_FORMAT  *********************/
/*************************** DBTYPE_TIME_FORMAT  ****************************/
/*************************** DBTYPE_CONTROLLER_VERSION  *********************/

typedef struct
{
    uint8_t TemperatureFormat;
    uint8_t TimeFormat;
    ScheduleModeEnum_t ScheduleMode;
    Program_t ActiveProgram;
    Setpoint100_t WakeSetpoint;
    Setpoint100_t LeaveSetpoint;
    Setpoint100_t ReturnSetpoint;
    Setpoint100_t SleepSetpoint;
    FullSchedule_t FullSchedule;
    Setpoint100_t DefaultSetpoint;
} dbType_Residence_t;

typedef struct
{
	uint32_t Backlight;
    uint8_t OffMode;
    uint8_t FloorType;
#ifdef LOAD_CYCLING
    uint8_t LoadCyclingMode;
    uint16_t LoadCyclingCount;
#endif /* LOAD_CYCLING */
} dbType_ThisThermostat_t;

/*************************** DBTYPE_AMBIENT_TEMPERATURE  ********************/
/*************************** DBTYPE_AMBIENT_SETPOINT  ***********************/
/*************************** DBTYPE_FLOOR_TEMPERATURE  **********************/
/*************************** DBTYPE_FLOOR_SETPOINT  *************************/
/*************************** DBTYPE_LOCK_STATE  *****************************/
/*************************** DBTYPE_LOCK_SETPOINT_LIMIT  ********************/
/*************************** DBTYPE_FLOOR_MODE  *****************************/
/*************************** DBTYPE_HEAT_DEMAND  ****************************/
/*************************** DBTYPE_TSTAT_VERSION  **************************/
/*************************** DBTYPE_SERIAL_NUMBER  **************************/

#define TEMPERATURE_DISPLAY_THRESHOLD           50

typedef enum {
    LOCK_UNKNOWN = 0,
	LOCK_OFF,
	LOCK_ON,
    MAX_LOCK_STATE,
} Lock_t;

typedef struct
{
    TEMPERATURE_C_t AmbientTemperature;
    TEMPERATURE_C_t DisplayedAmbientTemperature;
    Setpoint100_t AmbientSetpoint;
    TEMPERATURE_C_t FloorTemperature;
    TEMPERATURE_C_t DisplayedFloorTemperature;
    Setpoint100_t FloorSetpoint;
    dbType_Alerts_t Alerts;
    Lock_t LockState;
    uint8_t FloorMode;
    uint16_t FloorLimit;
    uint8_t HeatDemand;
    uint8_t DisplayedHeatDemand;
    uint16_t TstatVersion;
    char SerialNumber[SERIALNUMBER_SIZE_AND_NULL];
    uint32_t RelayCycleCount;
} dbType_Thermostat_t;

typedef struct
{
    dbType_Thermostat_t Thermostat[THERMOSTAT_INSTANCES];
} dbType_AllThermostats_t;


/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/



#endif /* THREAD_DBDataTypes_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
