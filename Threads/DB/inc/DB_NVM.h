/*******************************************************************************
* @file    DB_NVM.h
* @date    2016/11/30
* @authors J-F. Simard
* @brief 
*******************************************************************************/

#ifndef DB_NVM_H
#define DB_NVM_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>
#include "FlashLayout.h"

/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define NVM_NOINIT_HANDLER      0xFFFF


/*******************************************************************************
* Public structures definitions
*******************************************************************************/
typedef enum
{
    NVM_PAGE_NORMAL     = 0,
    NVM_PAGE_WEARLEVELING,
} nvmPageType_t;

typedef uint16_t nvmPageHandle_t;
typedef uint32_t validationWord_t;
typedef uint32_t nvmPagesAddr_t;
typedef struct nvmElement_s
{
    void * ptr_to_data;
    uint8_t size_of_data;
    struct nvmElement_s * nextElement;
} nvmElement_t;

typedef struct
{
    nvmPagesAddr_t pageStartAddress;
    nvmPageType_t pageType;
    uint16_t pageSize;
    nvmElement_t * nvmElement;
    validationWord_t dataValidationWord;
} nvmPageDescr_t;

/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void NVM_Init (void);
void NVM_PushAll (void);

#endif /* DB_NVM_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
