/*******************************************************************************
* @file    DB__OTAU.h
* @date    2016/09/20
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef DB__OTAU_H
#define DB__OTAU_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>

#include ".\DB\inc\THREAD_DB.h"
//#include ".\DB\inc\THREAD_DBDataTypes.h"

/*******************************************************************************
* Public constants definitions
*******************************************************************************/
typedef enum
{
    OTAERR_OK  = 0,
    OTAERR_BADALIGNMENT,
} otaErr_t;


/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
otaErr_t Store_OTAU_Chunks (dbMcuOtauChunk_t * chunk);
void Erase_OTAU_Memory (void);
void StartMcuOtauSession (uint32_t otauSize);
DB_GETFUNC(GetMcuOtauImageDownloadStatus);

#endif /* DB__OTAU_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
