/*******************************************************************************
* @file    THREAD_DB.h
* @date    2016/09/13
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef THREAD_DB_H
#define THREAD_DB_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>
#include ".\DB\inc\THREAD_DBDataTypes.h"
#include ".\HAL\inc\MnuboDataPoints.h"


#ifdef __ICCARM__
    #include "cmsis_os.h"
#endif

/*******************************************************************************
* Public constants definitions
*******************************************************************************/
typedef enum
{
    DBNOTIF_STOREOTAUCHUNK              =   (1<<0),
    DBNOTIF_STARTOTAUSESSION            =   (1<<1),
    DBNOTIF_ENDOFPROPERTYUPDATECHECK    =   (1<<2),
    DBNOTIF_SETDATA                     =   (1<<3),
    DBNOTIF_INITIALIZECLOUDDATA         =   (1<<4),
    DBNOTIF_CLEARRESIDENCEDATA          =   (1<<5),
} dbNotification_t;

#define WEATHERFEED_TIMEOUT             (4*60*60*1000)  //4 hours * 60 minutes/hour * 60 seconds/minute * 1000 ms/second
#define WEATHERFEED_TIMEOUT_CNT         10
#define OVERHEATING_TIMEOUT             (10*60*1000)    //10 minutes * 60 seconds/minute * 1000 ms/second

#define DBTHRD_MAX_SUBSCRIPTION         200
#define DBTHRD_NOTIFSUBSCRIPTION_FULL   0xFF
#define DBTHRD_NOTIFHANDLE_NOTSET       DBTHRD_NOTIFSUBSCRIPTION_FULL

#ifdef __ICCARM__
    extern osThreadId dbTaskHandle;


    #define DBTHRD_END_OF_PROPERTY_UPDATE_CHECK()     xTaskNotify(                \
                                                            dbTaskHandle,        \
                                                            DBNOTIF_ENDOFPROPERTYUPDATECHECK, \
                                                            eSetBits)

    #define DBTHRD_INITIALIZE_CLOUD_DATA()            xTaskNotify(                \
                                                            dbTaskHandle,        \
                                                            DBNOTIF_INITIALIZECLOUDDATA, \
                                                            eSetBits)

    #define DBTHRD_CLEAR_RESIDENCE_DATA()             xTaskNotify(                \
                                                            dbTaskHandle,        \
                                                            DBNOTIF_CLEARRESIDENCEDATA, \
                                                            eSetBits)

#else
    #define DBTHRD_END_OF_PROPERTY_UPDATE_CHECK()
#endif

#define CLEAR_DB_REASON_TEST_BENCH          0
#define CLEAR_DB_REASON_RESET_TO_DEFAULT    1
#define CLEAR_DB_REASON_RESET_UNREGISTER    2

/*******************************************************************************
* Public structures definitions
*******************************************************************************/

typedef dbErr_t (*setServiceFunc_t)(void*, uint64_t index, dbChangeSource_t source, mnuboDataPoint_t * mnuboDataPoint);
typedef dbErr_t (*getServiceFunc_t)(void*, uint64_t index);
typedef void (*notificationServiceFunc_t)(dbErr_t status, uint64_t instance, dbChangeSource_t source);
#define DB_NOTIFICATION(func_name)     void func_name(dbErr_t status, uint64_t instance,dbChangeSource_t source)
#define DB_GETFUNC(func_name)     dbErr_t func_name(void * pData, uint64_t index)
#define DB_SETFUNC(func_name)     dbErr_t func_name(void * pData, uint64_t index, dbChangeSource_t source, mnuboDataPoint_t * mnuboDataPoint)

typedef struct
{
    uint32_t offset;
    uint8_t size;
    uint8_t data[255];
} dbMcuOtauChunk_t;


typedef uint8_t susbscriptionHandle_t;




/*******************************************************************************
* Public variables declarations
*******************************************************************************/



/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void InitThread_DB(void);

dbErr_t DBTHRD_GetData(dbDataTypes_t dataType, void * pData, uint64_t index);
//dbErr_t DBTHRD_SetData(dbDataTypes_t dataType, void * pData, uint16_t index, dbChangeSource_t source);
dbErr_t DBTHRD_SetData_wMnuboDataPoint (dbDataTypes_t dataType, void * pData, uint64_t index, dbChangeSource_t source, void * mnuboDataPoint);
uint8_t DBTHRD_StartMcuOtauSession(uint32_t otauImageSize);
void DBTHRD_StoreOtauChunk (dbMcuOtauChunk_t * chunk);
susbscriptionHandle_t DBTHRD_SubscribeToNotification (dbDataTypes_t dataType,  notificationServiceFunc_t notifCB, dbChangeSource_t source, uint64_t instance);
void DBTHRD_UnsubscribeToNotification (susbscriptionHandle_t handle);

void EndOfPropertyUpdateCheck(void);
void UpdateCloudDatabase(void);
void ClearDatabase(uint8_t clearReason);
void SynchronizeCloud(void);

#define DBTHRD_SetData(dataType,pData,index,source)     DBTHRD_SetData_wMnuboDataPoint(  \
                                                                            dataType,   \
                                                                            pData,      \
                                                                            index,      \
                                                                            source,     \
                                                                            NULL)



#endif /* THREAD_DB_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
