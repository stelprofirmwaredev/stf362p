/*******************************************************************************
* @file    THREAD_Sensors.h
* @date    2016/08/25
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef THREAD_Sensors_H
#define THREAD_Sensors_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include "cmsis_os.h"


/*******************************************************************************
* Public constants definitions
*******************************************************************************/



/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/
extern osThreadId sensorsTaskHandle;
#ifdef FREERTOS_SUPPORT
    #include "trcRecorder.h"
    extern char* sensorEventChannel;
#endif


/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void StartSensorsTask(void const * argument);
void InitThread_Sensor(void);
void SetCurrentSensingRequest(uint8_t request);
void SetCurrentSensingPermit(uint8_t permit);
uint8_t IsCurrentSensingPermit(void);
void SetVoltageSensingRequest(uint8_t request);
void SetVoltageSensingPermit(uint8_t permit);
uint8_t IsVoltageSensingPermit(void);

#endif /* THREAD_Sensors_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
