/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    THREAD_Sensors.c
* @date    2016/08/25
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include "typedef.h"
#include ".\Sensors\inc\THREAD_Sensors.h"
#include ".\HAL\inc\HAL_TemperatureAcquisition.h"
#include ".\HAL\inc\TemperatureCompensation.h"
#include ".\HAL\inc\TemperatureControl.h"
#include "cmsis_os.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\sync_barrier\inc\SyncBarrier.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#ifdef FREERTOS_SUPPORT
    #include "trcRecorder.h"
    char* sensorEventChannel;
#endif
#define DELAY_10_SECONDS    100 //(10s / 100ms)
#define DELAY_5_MINUTES     30  //(5min (300 sec) / 10s)

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
osThreadId sensorsTaskHandle;

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/10/25
*******************************************************************************/
void InitThread_Sensor(void)
{
    osThreadDef(sensorsTask, StartSensorsTask, osPriorityBelowNormal, 0, 256);
    sensorsTaskHandle = osThreadCreate(osThread(sensorsTask), NULL);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void StartSensorsTask(void const * argument)
{
    static U8BIT cnt_10s = DELAY_10_SECONDS;

    Sync_Barrier((barriers_name_t)(BARRIER_TASK_DEFAULT));

    TemperatureSensor_Init();
    TMP_Init();
    CTL_Init();

#ifdef FREERTOS_SUPPORT
    sensorEventChannel = vTraceStoreUserEventChannelName("Sensor Events");
#endif

    Sync_TaskReady (BARRIER_TASK_SENSORS);



    /* Infinite loop */
    for(;;)
    {
        osDelay(100);
        cnt_10s++;
        if (cnt_10s >= DELAY_10_SECONDS)
        {
            cnt_10s = 0;
            HAL_TemperatureAcquisitionMonitoring();
        }
    }
}

/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
