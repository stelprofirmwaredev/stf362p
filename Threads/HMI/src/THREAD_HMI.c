/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    THREAD_HMI.c
* @date    2016/08/25
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include "cmsis_os.h"

#include ".\HMI\inc\THREAD_HMI.h"
#include ".\Console\inc\THREAD_Console.h"


#include ".\HAL\inc\Display.h"
#include ".\HAL\inc\HAL_Keypad.h"
#include ".\inc\StelproStat.h"

#include "OsTask.h"

#include "BootLoader_OTA_Control.h"

#include "SwVersion.h"
#include ".\sync_barrier\inc\SyncBarrier.h"
#include "APP_Display.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#ifdef FREERTOS_SUPPORT
    #include "trcRecorder.h"
    char* hmiEventChannel;
#endif


/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
extern uint32_t RAM_OFFSET;


/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
osThreadId hmiTaskHandle;



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/10/14
*******************************************************************************/
void InitThread_HMI(void)
{
        /* definition and creation of defaultTask */
    osThreadDef(hmiTask, StartHmiTask, osPriorityHigh, 0, 512);
    hmiTaskHandle = osThreadCreate(osThread(hmiTask), NULL);
#ifdef FREERTOS_SUPPORT
    hmiEventChannel = vTraceStoreUserEventChannelName("HMI Events");
#endif
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void StartHmiTask(void const * argument)
{
    loaderStates_t bootLoaderState = GetBootLoaderState();

    switch (bootLoaderState)
    {
    case LOADER_STATE_1:
    case LOADER_STATE_2:
    case LOADER_STATE_7:
    case LOADER_STATE_8:
        RAM_OFFSET = EXTFLASH_IMAGELIB_A_START;
        break;

    case LOADER_STATE_3:
    case LOADER_STATE_4:
    case LOADER_STATE_5:
    case LOADER_STATE_6:
        RAM_OFFSET = EXTFLASH_IMAGELIB_B_START;
        break;

    default:
        LoaderStateCorrupted_ResetToFactory();
        break;
    }

    
    DisplayInit();
    KeyPadInit();
    StelproStatInit();

    CONSOLE_LOG_MESSAGE("Application version ");
    CONSOLE_MESSAGE_APPEND_INTEGER(SW_VER_MAJOR);
    CONSOLE_MESSAGE_APPEND_STRING(".");
    CONSOLE_MESSAGE_APPEND_INTEGER(SW_VER_MINOR);
    CONSOLE_MESSAGE_APPEND_STRING(BUILD_INFO);
    
    Sync_TaskReady (BARRIER_TASK_HMI);
    Sync_Barrier((barriers_name_t)(BARRIER_TASK_DATABASE));

  /* Infinite loop */
    for(;;)
    {
        OS_DoEvents();
        osDelay(1);
        OS_HandleTickInterrupt();
    }
}



/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
