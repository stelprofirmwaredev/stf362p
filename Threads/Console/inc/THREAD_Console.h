/*******************************************************************************
* @file    THREAD_Console.h
* @date    2016/08/25
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef THREAD_Console_H
#define THREAD_Console_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include "cmsis_os.h"
#include ".\HAL\inc\HAL_Console.h"

/*******************************************************************************
* Public constants definitions
*******************************************************************************/
#define THREAD_CONSOLE_TX_QUEUE_NUMBER      5      //holds up to 5 messages string

#define CONSOLE_MESSAGE_QUEUE_WAIT                  (20/portTICK_PERIOD_MS)

#define CONSOLE_NOTIFICATION_SEND_NEW_MESSAGE   (1<<0)
#define CONSOLE_NOTIFICATION_MSG_SENT           (1<<1)

/*******************************************************************************
* Public structures definitions
*******************************************************************************/


/*******************************************************************************
* Public variables declarations
*******************************************************************************/
extern osThreadId consoleTaskHandle;


/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void StartConsoleTask(void const * argument);
void InitThread_Console(void);


#endif /* THREAD_Console_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
