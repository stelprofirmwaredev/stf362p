/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    THREAD_Console.c
* @date    2016/08/25
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <ctype.h>

#include ".\Console\inc\THREAD_Console.h"
#include ".\Console\inc\console_IncomingMsg.h"
#include "cmsis_os.h"
#include ".\HAL\inc\HAL_Console.h"
#include ".\console\inc\console.h"
#include ".\HAL\inc\app_test_bench.h"

#include ".\sync_barrier\inc\SyncBarrier.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
#define CONSOLE_MAX_STORED_MESSAGE      5
SemaphoreHandle_t tx_console_msg_cnt;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
osThreadId consoleTaskHandle;

extern uint8_t InTestMode;

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void InitThread_Console(void)
{
    osThreadDef(consoleTask, StartConsoleTask, osPriorityNormal, 0, 256);
    consoleTaskHandle = osThreadCreate(osThread(consoleTask), NULL);
    
    tx_console_msg_cnt = xSemaphoreCreateCounting(CONSOLE_MAX_STORED_MESSAGE,
                                                  CONSOLE_MAX_STORED_MESSAGE);
    configASSERT(tx_console_msg_cnt);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
#pragma optimize=none
void StartConsoleTask(void const * argument)
{
    static char cmdBuf[CMD_BUF_SIZE + 1];             // Additional space for end-of-string
#ifndef DEVKIT_PLATFORM
    GPIO_PinState TestLockPinState;
#endif /* DEVKIT_PLATFORM */

    consoleProperties_t consoleInterface =
    {
        .processFunc = processConsoleCommand,
        .cmdBuf = cmdBuf,
        .cmdBufIdx = 0,
    };

    InitConsole();

    Sync_TaskReady (BARRIER_TASK_CONSOLE);
    Sync_Barrier((barriers_name_t)(BARRIER_TASK_DATABASE));

    /* Infinite loop */
    for(;;)
    {
        uint32_t notifications;

        xTaskNotifyWait(0,-1,&notifications,pdMS_TO_TICKS(1000));

        if ((notifications & CONSOLE_NOTIFICATION_MSG_SENT) != 0)
        {
            FreeLastSentConsoleMsg();
        }

//        if ((notifications & CONSOLE_NOTIFICATION_SEND_NEW_MESSAGE) != 0)
        {
            SendNextConsoleMsg();
        }
        
        {
            uint8_t incomingCharacter;
            while (0 != GetConsoleNextCharacter(&incomingCharacter))
            {
#ifndef DEVKIT_PLATFORM
                TestLockPinState = HAL_GPIO_ReadPin(TestLock_GPIO_Port, TestLock_Pin);
                if (TestLockPinState == 0)
                {
                    if (InTestMode == 0)
                    {
                        if (incomingCharacter == APP_TBCH_ENABLE_TEST_MODE)
                        {
                            InTestMode = 1;
                        }
                    }
                }
                else
                {
                    InTestMode = 0;
                }
#endif /* DEVKIT_PLATFORM */
                if (InTestMode == 0)
                {
                    consoleRx(incomingCharacter, &consoleInterface);
                }
                else
                {
#ifndef DEVKIT_PLATFORM
                    if (TestLockPinState == 0)
                    {
                        ProcessTestCommand(incomingCharacter);
                        if ((incomingCharacter == APP_TBCH_DISABLE_TEST_MODE) || (incomingCharacter == APP_TBCH_SET_DEFAULT))
                        {
                            InTestMode = 0;
                        }
                    }
                    else
                    {
                        InTestMode = 0;
                    }
#endif /* DEVKIT_PLATFORM */
                }
            }
        }
    }
}



/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
