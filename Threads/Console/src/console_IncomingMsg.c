/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2017, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    console_IncomingMsg.c
* @date    2017/04/21
* @authors J-F. Simard
* @brief   
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include ".\Console\inc\console_IncomingMsg.h"
#include ".\console\inc\console.h"



/*******************************************************************************
*    Private constant definitions
*******************************************************************************/



/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
const ConsoleCommand_t IncomingConsoleMsg [] =
{
    
    
    
    /*  Last entry, must be kept all at NULL    */
    {
        .name = NULL,
        .fmt = NULL,
        .handler = NULL,
    },    
};


/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/**************************************************************************//**
\brief Processes command

\param[in] Str - string with command
******************************************************************************/
void processConsoleCommand(char *Str)
{
    ScanStack_t stk;


    if (!tokenizeStr(Str, &stk))
        return;

    if (stk.top == stk.args)                        // No command name
        return;

    /** Seek for a matching command */
    for (uint8_t i = 0; i < (sizeof(IncomingConsoleMsg)/sizeof(ConsoleCommand_t)); i++)   
    {
        if (strcmp(stk.args[0].str, IncomingConsoleMsg[i].name) == 0)    // Command match
        {
            if (unpackArgs(&IncomingConsoleMsg[i], &stk))
            {
                if (IncomingConsoleMsg[i].handler != NULL)
                {
                    IncomingConsoleMsg[i].handler(stk.args + 1);
                }
            }
            return;
        }
    }
}


/** Copyright(C) 2017 Stelpro Design, All Rights Reserved**/
