/*******************************************************************************
* @file    THREAD_LoadManagement.h
* @date    2017/01/04
* @authors Jean-Fran�ois Many
* @brief
*******************************************************************************/

#ifndef THREAD_LoadManagement_H
#define THREAD_LoadManagement_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include "cmsis_os.h"


/*******************************************************************************
* Public constants definitions
*******************************************************************************/



/*******************************************************************************
* Public structures definitions
*******************************************************************************/



/*******************************************************************************
* Public variables declarations
*******************************************************************************/
extern osThreadId loadTaskHandle;
#ifdef FREERTOS_SUPPORT
    #include "trcRecorder.h"
    extern char* loadEventChannel;
#endif


/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void StartLoadManagementTask(void const * argument);
void InitThread_LoadManagement(void);
void LOADTHRD_UpdateDailyConsumption(void);

#endif /* THREAD_LoadManagement_H */
/** Copyright(C) 2017 Stelpro Design, All Rights Reserved**/
