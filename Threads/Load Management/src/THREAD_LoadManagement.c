/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2017, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    THREAD_LoadManagement.c
* @date    2017/01/04
* @authors Jean-Fran�ois Many
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include "typedef.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\WiFi\inc\THREAD_WiFi.h"
#include ".\Load Management\inc\THREAD_LoadManagement.h"
#include ".\HAL\inc\HAL_LoadManagement.h"
#include "cmsis_os.h"
#include ".\sync_barrier\inc\SyncBarrier.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#ifdef FREERTOS_SUPPORT
    #include "trcRecorder.h"
    char* loadEventChannel;
#endif


/*******************************************************************************
*    Private variables definitions
*******************************************************************************/


/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
osThreadId loadTaskHandle;



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/01/04
*******************************************************************************/
void InitThread_LoadManagement(void)
{
    osThreadDef(loadTask, StartLoadManagementTask, osPriorityHigh, 0, 512);
    loadTaskHandle = osThreadCreate(osThread(loadTask), NULL);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void StartLoadManagementTask(void const * argument)
{
    uint32_t taskNotifications;
#ifdef FREERTOS_SUPPORT
    loadEventChannel = vTraceStoreUserEventChannelName("Load Events");
#endif
    Sync_Barrier((barriers_name_t)(BARRIER_TASK_DEFAULT));

    LoadManagement_Init();

    Sync_TaskReady (BARRIER_TASK_LOADMNG);

    /* Infinite loop */
    for(;;)
    {
#ifdef __ICCARM__
        xTaskNotifyWait (0x0000,
                         0xffff,
                         &taskNotifications,
                         portMAX_DELAY);
        /*************** Thread Execution blocked here ***************/
        /**** Thread resume on Event Notifications ****/
#endif
    }
}

/** Copyright(C) 2017 Stelpro Design, All Rights Reserved**/
