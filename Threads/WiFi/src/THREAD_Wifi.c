/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    THREAD_WiFi.c
* @date    2016/08/25
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <string.h>

#include ".\WiFi\inc\THREAD_Wifi.h"
#include ".\HAL\inc\HAL_CloudInterface.h"
#include ".\HAL\inc\HAL_WiFiTypes.h"
#include ".\Application\inc\WiFi_Notifications.h"
#include ".\sync_barrier\inc\SyncBarrier.h"
#include ".\HAL\inc\AylaProperties.h"

#include ".\DB\inc\THREAD_DB.h"
#include ".\strings\inc\strings_tool.h"

#ifdef __ICCARM__
    #include "cmsis_os.h"
#endif

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#define RX_PRIORITY 0
#define TX_PRIORITY 1

static uint8_t direction_priority = RX_PRIORITY;

typedef struct wifi_SendProperty_s
{
    WIFITYPE_t propType;
    uint8_t index;
    dbChangeSource_t source;
    struct wifi_SendProperty_s * nextPropToSend;
}wifi_SendProperty_t;

typedef struct wifi_FetchProperty_s
{
    WIFITYPE_t propType;
    struct wifi_FetchProperty_s * nextPropToFetch;
}wifi_FetchProperty_t;

typedef struct
{
    uint8_t buffer[384];
    uint16_t packetLength;
}wifi_rx_data_t;

typedef struct
{
	uint8_t eventType;
	union
	{
		void (* voidPtrParamCb)(void*) ;
		void (* genericNoParamCb)(void) ;
		void (* registrationCb)(RegisterStatus_t,uint8_t*) ;
		wifi_joinNetworks_t wifiNetworkInfo;
		uint8_t scanNumber;
	};
} wifiNotifQ_t;

#define MAX_ATTEMPTS    400
#define MAX_SPI_TIMEOUT 3

#define WIFI_SUCCESS    0
#define WIFI_PENDING    1
#define WIFI_ERROR      -1
#define WIFI_TIMEOUT    2

#define WIFI_RX_MSG_MAX     10

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
wifi_state_machine_t WiFiSerialState;
SemaphoreHandle_t wifi_rx_msg_cnt;
uint8_t wifiSemaphoreTaken = 0;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
#ifdef __ICCARM__
    osThreadId wifiTaskHandle;
    osThreadId sendPropertyTaskHandle;
#endif


#ifdef FREERTOS_SUPPORT
    #include "trcRecorder.h"
    char* wifiEventChannel;
#endif

TimerHandle_t RadioOTATimeout;
propertiesToSend_t propertiesToSend[MAX_PROPERTY_TYPE];
extern const prop_t propTable[];

/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
uint8_t HAL_IsMessagePending(void);
static void OnRadioOTATimeout( TimerHandle_t xTimer );
extern void OutOfMemory(void);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
static void OnRadioOTATimeout( TimerHandle_t xTimer )
{
    PULL_WIFI_RESET_LINE();
    osDelay ( 100 );
    RELEASE_WIFI_RESET_LINE();
}


/*******************************************************************************
*    Public functions definitions
*******************************************************************************/

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/13
*******************************************************************************/
void InitThread_WiFi(void)
{
#ifdef __ICCARM__

    /* Initialization code - create the channel label for a VTracePrintF*/
    osThreadDef(wifiTask, StartWifiTask, osPriorityNormal, 0, 256);
    wifiTaskHandle = osThreadCreate(osThread(wifiTask), NULL);

    /* Initialization code - create the channel label for a VTracePrintF*/
    osThreadDef(sendPropertyTask, SendPropertyTask, osPriorityNormal, 0, 256);
    sendPropertyTaskHandle = osThreadCreate(osThread(sendPropertyTask), NULL);

#endif
#ifdef FREERTOS_SUPPORT
    wifiEventChannel = vTraceStoreUserEventChannelName("WiFi Events");
#endif

    //InitAnalytics();
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/13
*******************************************************************************/
void WIFITHRD_CancelWiFiScan (void)
{
    /* TODO : Cancel an active wifi scan  */
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void WIFITHRD_StartWiFiScan (void (*callback)(void*))
{
	HAL_SCAN_WIFI_NETWORK(callback);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void WIFITHRD_StopWiFiScan (void)
{
	HAL_STOP_WIFI_SCAN();
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void WIFITHRD_JoinNetwork (uint8_t scanNumber, uint8_t* pKey, void (* joinCallBack)(WiFiJoinStatus_t))
{
	wifi_joinNetworks_t wifiNetworkInfo;

    wifiNetworkInfo.scanId = scanNumber;

    stringNCopyWithNull ((char*)wifiNetworkInfo.key, (char*)pKey, sizeof(wifiNetworkInfo.key));

    wifiNetworkInfo.joinNetworkcB = joinCallBack;

	HAL_JoinWiFiNetworks(&wifiNetworkInfo);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void WIFITHRD_GetRSSI (void)
{
    HAL_GetSignalStrength();
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void WIFITHRD_GetWiFiVersion (void)
{
    HAL_GetWiFiModuleVersion();
}

/*******************************************************************************
* @brief  Ask the WiFi thread to perform a factory reset of the WiFi module
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2017/02/14
*******************************************************************************/
void WIFITHRD_FactoryReset (void)
{
    HAL_FactoryReset();
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void WIFITHRD_GetRegistrationToken (void (* registrationCallBack)(RegisterStatus_t, uint8_t*))
{
	HAL_GetRegistrationToken(registrationCallBack);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void WIFITHRD_GenerateNewRegistrationToken (void)
{
	HAL_GenerateNewRegistrationToken();
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void WIFITHRD_GetRegistrationStatus (void (* registrationCallBack)(RegisterStatus_t, uint8_t*))
{
	HAL_GetRegistrationStatus(registrationCallBack);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date 2016/09/19
*******************************************************************************/
void WIFITHRD_GetCurrentNetwork (void (*currentNetworkcB)(void))
{
	HAL_GetCurrentNetwork(currentNetworkcB);
}

/*******************************************************************************
* @brief Send a property to the WiFi module
* @inputs type: WIFITYPE property
* @inputs index: property instance index
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/03
*******************************************************************************/
void WIFITHRD_SendPropToWiFi(WIFITYPE_t type, uint8_t index, dbChangeSource_t source)
{
    dbType_CloudConnectivity_t cloudInfo;

    DBTHRD_GetData( DBTYPE_CLOUDCONNECTIVITY, (void*)&cloudInfo, INDEX_DONT_CARE);

    if (cloudInfo.isConnectedToCloud != 0)
    {
		HAL_SendProperty(type, &index, &source);

    #ifdef FREERTOS_SUPPORT
        vTracePrint(wifiEventChannel, "Queuing new property to send");
    #endif
    }
}

/*******************************************************************************
* @brief Get all "To Device" properties from the cloud
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/10/27
*******************************************************************************/
void WIFITHRD_GetAllProperties (void)
{
    dbType_CloudConnectivity_t cloudInfo;

    DBTHRD_GetData( DBTYPE_CLOUDCONNECTIVITY, (void*)&cloudInfo, INDEX_DONT_CARE);

    if (cloudInfo.isConnectedToCloud != 0)
    {
        HAL_GetAllProperties();
    }
}

/*******************************************************************************
* @brief Get all "To Device" properties from the cloud
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/24
*******************************************************************************/
void WIFITHRD_GetSpecificProperty (WIFITYPE_t wifiType)
{
    dbType_CloudConnectivity_t cloudInfo;

    DBTHRD_GetData( DBTYPE_CLOUDCONNECTIVITY, (void*)&cloudInfo, INDEX_DONT_CARE);

    if (cloudInfo.isConnectedToCloud != 0)
    {
        char string_buffer[100] = "Queuing new property to fetch: ";

		HAL_GetSpecificProperty(wifiType);

        strcat(string_buffer,(char const*)propTable[wifiType].propertyName);
        vTracePrint(wifiEventChannel, string_buffer);
    }
}

/*******************************************************************************
* @brief Update Local Time starting with Time zone
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/11/25
*******************************************************************************/
void WIFITHRD_UpdateTime(void)
{
	HAL_UpdateTime();
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/12/12
*******************************************************************************/
void WIFITHRD_GetSecurity(uint8_t scanResultNumber)
{
    HAL_GetSecurity(scanResultNumber);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/12/12
*******************************************************************************/
void WIFITHRD_GetSSID(uint8_t scanResultNumber)
{
	HAL_GetSSID(scanResultNumber);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/12/12
*******************************************************************************/
void WIFITHRD_PollWiFiScanReady(TimerHandle_t xTimer)
{
    HAL_PollWiFiScanReady();
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date 2016/12/12
*******************************************************************************/
void WIFITHRD_SnapShotScanResult(void)
{
	HAL_SnapShotScanResult();
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F Simard
* @date 2017/01/13
*******************************************************************************/
void WIFITHRD_ResetWifi (void)
{
    HAL_ResetCloudInterface();
}


void wifiRxParsing(void const * argument)
{
    wifi_rx_data_t * rx_data = (wifi_rx_data_t *)argument;

    osThreadSetPriority(NULL, osPriorityHigh);

    HAL_ParseReceivedPacket(rx_data->buffer, rx_data->packetLength);

    vPortFree(rx_data);
    xSemaphoreGive(wifi_rx_msg_cnt);
    wifiSemaphoreTaken--;

    //Message received, switch direction priority
    direction_priority = TX_PRIORITY;

    vTaskDelete(NULL);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void SendPropertyTask(void const * argument)
{
    static uint8_t propType = 0;
    uint8_t sendStatus;
    for (;;)
    {
        vTaskDelay(pdMS_TO_TICKS(10));       //let the RTOS serve lower priority tasks

        //Do a round robin on all wifi property types
        if (propertiesToSend[propType].sendRequest == 1)
        {
            sendStatus = propTable[propType].sendFunction((void*)&propertiesToSend[propType].index, propertiesToSend[propType].source);
            if (sendStatus == 0)
            {
                propertiesToSend[propType].sendRequest = 0;
            }
        }
        propType++;
        if (propType >= MAX_PROPERTY_TYPE)
        {
            propType = 0;
        }
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
#pragma optimize=none
void StartWifiTask(void const * argument)
{
    uint8_t buffer[384];
    uint16_t packetLength;

    /**********************************************************/
    /*  The WIFI task must register its notifications before  */
    /*  initializing the serial interface, otherwise, some of */
    /*  the data manipulation (DB) can be done without having */
    /*  the wifi notification being registered.               */
    /**********************************************************/
    Sync_Barrier((barriers_name_t)(BARRIER_TASK_DATABASE));
    RegisterWiFiNotifications();

    InitCloudInterface();
    direction_priority = RX_PRIORITY;


    RadioOTATimeout = xTimerCreate ("Radio OTA Timeout",
                                       pdMS_TO_TICKS( RADIO_OTA_TIMEOUT ),
                                       pdFALSE,     //one-shot
                                       (void*)0,
                                       OnRadioOTATimeout);
    configASSERT(RadioOTATimeout);

    wifi_rx_msg_cnt = xSemaphoreCreateCounting(WIFI_RX_MSG_MAX,WIFI_RX_MSG_MAX);
    wifiSemaphoreTaken = 0;
    configASSERT(wifi_rx_msg_cnt);

    Sync_TaskReady (BARRIER_TASK_WIFI);
    /* Infinite loop */
    for(;;)
    {
		if (HAL_IS_WIFIRADIO_READY() != 0)
		{
            switch (WiFiSerialState)
            {
                /************************************************************/
                /*  State IDLE :                                            */
                /*  In state idle, the system monitor the INTR pin for      */
                /*  incoming messages and the WifiMsgQ for outgoing messages*/
                /*                                                          */
                /*  Alternatively, in case where the INTR pin would not be  */
                /*  reliable, it is possible to monitor the ATTENTION for   */
                /*  incoming messages.  To enable this feature, the lines   */
                /*  needs to be uncommented.                                */
                /************************************************************/
                case WIFI_IDLE:
                    vTaskDelay(pdMS_TO_TICKS(10));       //let the RTOS serve lower priority tasks
#ifdef FREERTOS_SUPPORT
                    vTracePrint(wifiEventChannel, "Wifi State : IDLE");
#endif
                    switch (direction_priority)
                    {
                        case RX_PRIORITY:
                            if (HAL_POLL_RADIO_FOR_NEW_MESSAGE() != 0)
                            {
                                WiFiSerialState = WIFI_RX_WAIT;
                            }

                            /************************************************/
                            /*  Uncomment the following lines to enable the */
                            /*  ATTENTION bit monitoring                    */
//                            else if (HAL_IsMessagePending() == 0)
//                            {
//                                WiFiSerialState = WIFI_RX;
//#ifdef FREERTOS_SUPPORT
//                                vTracePrint(wifiEventChannel, "Transition to Wifi RX from Attention bit");
//#endif
//                            }
                            /************************************************/

                            else if (1 == HAL_isWifiMsgToTx())
                            {
                                WiFiSerialState = WIFI_TX_START;
                            }
                            else
                            {
                                ;
                            }
                            break;

                        case TX_PRIORITY:
                            if (1 == HAL_isWifiMsgToTx())
                            {
                                WiFiSerialState = WIFI_TX_START;
                            }
                            else if (HAL_POLL_RADIO_FOR_NEW_MESSAGE() != 0)
                            {
                                WiFiSerialState = WIFI_RX_WAIT;
                            }
                            else
                            {
                                ;
                            }
                            break;

                        default:
                            break;
                    }
                    break;

                /************************************************************/
                /*  State RX WAIT :                                         */
                /*  Intermediary state that allow to "double check" the     */
                /*  availability of an incoming message by polling the      */
                /*  ATTENTION bit.                                          */
                /*  In the case where the ATTENTION bit is not set, the     */
                /*  state machine will return to the IDLE state.            */
                /************************************************************/
                case WIFI_RX_WAIT:
#ifdef FREERTOS_SUPPORT
                    vTracePrint(wifiEventChannel, "Wifi State : RX WAIT");
#endif
                    int8_t status;
                    static uint8_t timeout_counter = 0;

                    status = HAL_IsMessagePending();
                    if (WIFI_SUCCESS == status)
                    {
                        WiFiSerialState = WIFI_RX;
                        timeout_counter = 0;
                    }
                    else
                    {
                        WiFiSerialState = WIFI_IDLE;
                        /*  2 : radio in timeout */
                        /*  reset wi-fi module */
                        if (status == WIFI_TIMEOUT)
                        {
                            timeout_counter++;
                            if (timeout_counter >= MAX_SPI_TIMEOUT)
                            {
                                //Wi-Fi module did not answer in time
                                HAL_RESET_WIFI();
                                direction_priority = RX_PRIORITY;
                            }
                        }
                        else
                        {
                            timeout_counter = 0;
                        }
                    }
                    break;

                /************************************************************/
                /*  State RX :                                              */
                /*  Once in this state, the reception of the message from   */
                /*  the radio module starts in a defined sequence :         */
                /*                                                          */
                /*  1. Send the start command                               */
                /*      This step ends when the radio module replies with   */
                /*      the same start command (0xF1)                       */
                /*      Per experimentation, once this command has been sent*/
                /*      once after the assertion of the INTR pin,           */
                /*      the latter is deasserted, thus, the state machine   */
                /*      must exclusively retry sending the start command    */
                /*      until it receives the start response.               */
                /*      A fall back process using 400 retries has been      */
                /*      installed, just in case...                          */
                /*      Note : the busy state is also monitored in this step*/
                /*  2. The packet length is received from the radio module  */
                /*  3. The DMA/SPI is configured to receives the packet     */
                /*      according to the length received previously         */
                /************************************************************/
                case WIFI_RX:
                    {
                        //rx_retry initialized to a large number for the SPI
                        static uint16_t rx_retry = 400;
                        uint8_t status;
                        static uint8_t timeout_counter = 0;

                        vTracePrint(wifiEventChannel, "Wifi State : RX");
                        status = HAL_READ_MESSAGE_FROM_RADIO(buffer, &packetLength);
                        if (WIFI_SUCCESS == status)
                        {
                            //SPI communication success, rx_retry is reduced for ressource allocation
                            rx_retry = 10;
                            timeout_counter = 0;
                            osThreadId * threadId = NULL;
                            do
                            {
                                wifi_rx_data_t * wifi_rx_data;
                                wifi_rx_data = pvPortMalloc(sizeof(wifi_rx_data_t));

                                if (NULL != wifi_rx_data)
                                {
                                    memcpy(wifi_rx_data->buffer, buffer, packetLength);
                                    wifi_rx_data->packetLength = packetLength;
                                    if (pdTRUE == xSemaphoreTake(wifi_rx_msg_cnt,pdMS_TO_TICKS(5000) ))  //each message takes one resource
                                    {
                                        wifiSemaphoreTaken++;
                                        osThreadDef(wifiRxParsingTask, wifiRxParsing,osPriorityNormal, 0, 256);
                                        threadId = osThreadCreate(osThread(wifiRxParsingTask), wifi_rx_data);
                                        if (NULL == threadId)
                                        {
                                            vPortFree(wifi_rx_data);
                                            xSemaphoreGive(wifi_rx_msg_cnt);
                                            wifiSemaphoreTaken--;
                                            vTracePrint(wifiEventChannel, "cannot create rx parsing thread");
                                            rx_retry--;
                                            vTaskDelay(pdMS_TO_TICKS(5000));       //let the RTOS serve lower priority tasks
                                        }
                                    }
                                    else
                                    {
                                        //No more RX ressources available
                                        rx_retry--;
                                    }
                                }
                                else
                                {
                                    vTracePrint(wifiEventChannel, "cannot allocate rx memory");
                                    rx_retry--;
                                    vTaskDelay(pdMS_TO_TICKS(5000));       //let the RTOS serve lower priority tasks
                                }
                            }
                            while ((threadId == NULL) && (rx_retry > 0));

                            WiFiSerialState = WIFI_IDLE;
                            if (rx_retry == 0)
                            {
                                //If we exited with error, switch the priority and maybe force a wifi reset
                                if (direction_priority != TX_PRIORITY)
                                {
                                    direction_priority = TX_PRIORITY;
                                }
                                else
                                {
                                    //If the direction priority was already TX
                                    //which means we have nothing to send and we don't have memory left
                                    //"Houston we have a problem..."
                                    //Force a thermostat reset!
                                    OutOfMemory();
                                }
                            }
                            //Restore rx_retry to its nominal value
                            rx_retry = 400;
                        }
                        else
                        {
                            //SPI peripheral in error
                            if (status == WIFI_TIMEOUT)
                            {
                                timeout_counter++;
                                if (timeout_counter >= MAX_SPI_TIMEOUT)
                                {
                                    //Wi-Fi module did not answer in time
                                    HAL_RESET_WIFI();
                                    direction_priority = RX_PRIORITY;
                                }
                            }
                            else
                            {
                                timeout_counter = 0;
                                vTracePrint(wifiEventChannel, "Start master in not received");
                                if (rx_retry--)
                                {
                                    WiFiSerialState = WIFI_RX;
                                    vTaskDelay(pdMS_TO_TICKS(1));       //let the RTOS serve lower priority tasks
                                }
                                else
                                {
                                    HAL_ResetCloudInterface();
                                    direction_priority = RX_PRIORITY;
                                    WiFiSerialState = WIFI_IDLE;
                                }
                            }
                        }
                    }
                    break;

                /************************************************************/
                /*  State TX START :                                        */
                /*  Once in this state, the transmission of the message to  */
                /*  the radio module starts in a defined sequence :         */
                /*                                                          */
                /*  1. Wait not busy (and ADS busy for data operations)     */
                /*      If the radio module is busy (and/or adsbusy), the   */
                /*      state machine returns to the IDLE state to serve    */
                /*      a probable incomming message. (return status -1)    */
                /*  2. Send the start command + length of packet (%8)       */
                /*  3. Wait for busy bit indicating that the radio has      */
                /*      accepted the command (return status 1)              */
                /*  4. Send the exact packet length.                        */
                /*  5. Send the packet                                      */
                /*  6. Send the padding bytes to fit a %8                   */
                /*                                                          */
                /*  At this point, the return status is 0, indicating the   */
                /*  end of the sequence.                                    */
                /************************************************************/
                case WIFI_TX_START:
                    {
                        int8_t status;
                        static uint8_t timeout_counter = 0;
                        static uint16_t retry = 400;
#ifdef FREERTOS_SUPPORT
                        vTracePrint(wifiEventChannel, "Wifi State : TX START");
#endif
                        status = HAL_SendCommand();

                        switch (status)
                        {
                        /* Send sequence ended normally */
                        case WIFI_SUCCESS:
                            WiFiSerialState = WIFI_TX_END;
                            retry = 400;
                            timeout_counter = 0;
                            break;
                        /* waiting the acceptance of the start command */
                        case WIFI_PENDING:
                            if (retry--)
                            {
                                WiFiSerialState = WIFI_TX_START;
                            }
                            else
                            {
#ifdef FREERTOS_SUPPORT
                                vTracePrint(wifiEventChannel, "Wifi State : Tx retry exceeded maximum");
#endif
                                WiFiSerialState = WIFI_IDLE;
                            }
                            timeout_counter = 0;
                            break;
                        /* radio module is busy and cannot serve the tx request */
                        case WIFI_ERROR:
#ifdef FREERTOS_SUPPORT
                            vTracePrint(wifiEventChannel, "Wifi State : radio is busy; cannot serve TX");
#endif
                            timeout_counter = 0;
                            WiFiSerialState = WIFI_IDLE;
                            //If TX cannot be served, switch direction priority
                            direction_priority = RX_PRIORITY;
                            break;
                        /*  2 : radio in timeout */
                        /*  reset wi-fi module */
                        case WIFI_TIMEOUT:
                            timeout_counter++;
                            if (timeout_counter >= MAX_SPI_TIMEOUT)
                            {
                                //Wi-Fi module did not answer in time
                                HAL_RESET_WIFI();
                                direction_priority = RX_PRIORITY;
                            }
                            break;
                        default:
                            WiFiSerialState = WIFI_IDLE;
                            timeout_counter = 0;
                            break;
                        }
                    }
                    break;

                /************************************************************/
                /*  State TX END :                                          */
                /*  This state wait the confirmation of the ending of the   */
                /*  transmission                                            */
                /*  If the transmission ends normally (return status 0),    */
                /*  the message is removed from the Q, else, the message is */
                /*  kept and will be resent later.                          */
                /************************************************************/
                case WIFI_TX_END:
                    {
                        int8_t status;
                        static uint8_t timeout_counter = 0;
                        status = HAL_isWiFiTxComplete();

#ifdef FREERTOS_SUPPORT
                        vTracePrint(wifiEventChannel, "Wifi State : TX END");
#endif

                        //Message transmitted, change direction priority
                        direction_priority = RX_PRIORITY;

                        switch (status)
                        {
                        /*  0 : tx completed        */
                        /*  msg sent without problems.  clear message from q    */
                        case WIFI_SUCCESS:
                            HAL_FreeWiFiQ();
                            timeout_counter = 0;
                            WiFiSerialState = WIFI_IDLE;
                            break;
                        /* -1 : error while tx      */
                        /*  retry last message on next passage  */
                        case WIFI_ERROR:
                            timeout_counter = 0;
                            WiFiSerialState = WIFI_IDLE;
                            break;
                        /*  1 : wait ending tx           */
                        case WIFI_PENDING:
                            timeout_counter = 0;
                            vTaskDelay(pdMS_TO_TICKS(10));       //let the RTOS serve lower priority tasks
                            break;
                        /*  2 : radio in timeout */
                        /*  reset wi-fi module */
                        case WIFI_TIMEOUT:
                            timeout_counter++;
                            if (timeout_counter >= MAX_SPI_TIMEOUT)
                            {
                                //Wi-Fi module did not answer in time
                                HAL_RESET_WIFI();
                            }
                            break;

                        default:
                            break;
                        }
                    }
                    break;

                default:
                    break;
            }
		}
        else
        {
#ifdef FREERTOS_SUPPORT
            vTracePrint(wifiEventChannel, "Wifi not ready: Went to sleep?");
#endif
            if (1 == HAL_isWifiMsgToTx())
            {
                HAL_RESET_WIFI();
                direction_priority = RX_PRIORITY;
            }
            vTaskDelay(pdMS_TO_TICKS(10000));       //let the RTOS serve lower priority tasks
        }
    }
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
