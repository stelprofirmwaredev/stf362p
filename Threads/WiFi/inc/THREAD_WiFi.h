/*******************************************************************************
* @file    THREAD_WiFi.h
* @date    2016/08/25
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef THREAD_WIFI_H
#define THREAD_WIFI_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include ".\HAL\inc\HAL_WiFiTypes.h"
#include ".\HAL\inc\HAL_CloudInterface.h"
#include ".\DB\inc\THREAD_DB.h"


#ifdef __ICCARM__
    #include "cmsis_os.h"
#endif

/*******************************************************************************
* Public constants definitions
*******************************************************************************/
typedef enum
{
    WIFITHRD_STARTNEWSCAN                           = 1,
    WIFITHRD_SENDPROPERTY,
    WIFITHRD_JOINNETWORK,
    WIFITHRD_GETACTIVENETWORK,
    WIFITHRD_GETREGISTRATIONSTATUS,
    WIFITHRD_GETREGISTRATIONTOKEN,
    WIFITHRD_GETALLPROPERTIES,
    WIFITHRD_GETSPECIFICPROPERTY,
    WIFITHRD_UPDATETIME,
    WIFITHRD_STOPSCAN,
    WIFITHRD_GETSECURITY,
    WIFITHRD_GETSSID,
    WIFITHRD_POLLWIFISCANREADY,
    WIFITHRD_SNAPSHOTSCANRESULT,
    WIFITHRD_GENERATENEWREGTOKEN,
} wifiThread_notifications_t;

typedef enum
{
    WIFI_IDLE = 0,
    WIFI_RX_WAIT,
    WIFI_RX,
    WIFI_TX_WAIT,
    WIFI_TX_START,
    WIFI_TX_RESEND,
    WIFI_TX_END
} wifi_state_machine_t;

#define WIFI_DELAY_BETWEEN_SEND_PROPERTY            500         //in ms; to short, the ADS returns
                                                                //busy status

/*******************************************************************************
* Public structures definitions
*******************************************************************************/

/*******************************************************************************
* Public variables declarations
*******************************************************************************/


/*******************************************************************************
* Public functions declarations
*******************************************************************************/
void StartWifiTask(void const * argument);
void SendPropertyTask(void const * argument);
void WIFITHRD_StartWiFiScan (void (*callback)(void*));
void WIFITHRD_StopWiFiScan (void);
void WIFITHRD_JoinNetwork (uint8_t scanNumber, uint8_t* pKey, void (* joinCallBack)(WiFiJoinStatus_t));
void WIFITHRD_GetRegistrationToken (void (* registrationCallBack)(RegisterStatus_t, uint8_t*));
void WIFITHRD_GetRegistrationStatus (void (* registrationCallBack)(RegisterStatus_t, uint8_t*));
void WIFITHRD_GetRSSI (void);
void WIFITHRD_GetWiFiVersion (void);
void InitThread_WiFi(void);
void WIFITHRD_CancelWiFiScan (void);
void WIFITHRD_GetCurrentNetwork (void (*currentNetworkcB)(void));
void WIFITHRD_SendPropToWiFi(WIFITYPE_t type, uint8_t index, dbChangeSource_t source);
void HAL_GetRegistrationToken (void (*regTokencB)(RegisterStatus_t, uint8_t*));
void HAL_GetRegistrationStatus (void (*regStatuscB)(RegisterStatus_t, uint8_t*));
void WIFITHRD_GetAllProperties (void);
void HAL_GetAllProperties(void);
void HAL_GetSpecificProperty(WIFITYPE_t wifiType);
void WIFITHRD_GetSpecificProperty (WIFITYPE_t wifiType);
void WIFITHRD_UpdateTime(void);
void HAL_UpdateTime(void);
void WIFITHRD_GetSecurity(uint8_t scanResultNumber);
void WIFITHRD_GetSSID(uint8_t scanResultNumber);
void WIFITHRD_PollWiFiScanReady(TimerHandle_t xTimer);
void WIFITHRD_SnapShotScanResult(void);
void HAL_GetSecurity(uint8_t scanResultNumber);
void HAL_GetSSID(uint8_t scanResultNumber);
void HAL_PollWiFiScanReady(void);
void HAL_SnapShotScanResult(void);
void WIFITHRD_GenerateNewRegistrationToken (void);
void WIFITHRD_ResetWifi (void);
void HAL_SetWiFiState(wifi_state_machine_t state);
void HAL_GetSignalStrength (void);
void HAL_GetWiFiModuleVersion (void);
void WIFITHRD_FactoryReset (void);
void HAL_FactoryReset(void);
void HAL_ResetWiFiInitStateMachine(void);
#endif /* THREAD_WIFI_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
