/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    THREAD_ZigBee.c
* @date    2016/08/25
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <string.h>

#include ".\ZigBee\inc\THREAD_ZigBee.h"
#include ".\Application\inc\ZigBeeStateMachines.h"
#include ".\Application\inc\ZigBee_ClustersEmulation.h"
#include ".\console\inc\console.h"

#include ".\HAL\BitCloud\inc\HAL_BitCloud_ConsoleInterface.h"
#include ".\HAL\BitCloud\inc\HAL_OutgoingBitCloudMsg.h"

#include ".\Console\inc\THREAD_Console.h"

#include "cmsis_os.h"

#include "HAL_ZigBeeInterface.h"
#include "ZigBee_Notifications.h"

#include ".\DB\inc\THREAD_DB.h"

#include "BootLoader_OTA_Control.h"
#include ".\console\inc\console.h"
#include ".\Console\inc\THREAD_Console.h"
#include ".\sync_barrier\inc\SyncBarrier.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#ifdef FREERTOS_SUPPORT
    #include "trcRecorder.h"
    char* zbEventChannel;
#endif

#define PERMIT_JOIN_30_MINUTES  (30 * 60)
#define PERMIT_JOIN_3_MINUTES   (3 * 60)
#define PERMIT_JOIN_DISABLED    (0)

#define ZB_MODULE_EXPIRATION_TIME   (60*1000)    //x * 1000ms
#define ZB_MODULE_RESET_CNT_MAX     5           //5 reset inside <ZB_MODULE_EXPIRATION_TIME> amount of time

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
osThreadId zigBeeTaskHandle;
osThreadId zigBeeSMTaskHandle;
QueueHandle_t  zigBeeQhandle;
TimerHandle_t ZigBeeTstatTimeout;
TimerHandle_t ZigBeePingTrigger;
TimerHandle_t ZigBeePingTimeout;
TimerHandle_t ZigBeeModuleProvisionalTimer;
TimerHandle_t bBitCloudPeriodicRx;

uint32_t statsToRemove;
static uint8_t zibBeeModuleResetCnt;
uint8_t keepSameNetwork = 0;
/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
extern void OnZigBeeTstatTimeout( TimerHandle_t xTimer );
extern void OnZigBeePingTrigger( TimerHandle_t xTimer );
extern void OnZigBeePingTimeout( TimerHandle_t xTimer );
extern uint8_t isZigBeeModuleUnusable(void);
extern uint8_t* GetMacAddress();
extern uint8_t* GetInstallCode();

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
void StartZigBeeTask(void const * argument);
void ZigBeeSMTask(void const * argument);


/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief  Initilize the ZigBee thread
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/09/19
*******************************************************************************/
void InitThread_ZigBee(void)
{
    /* definition and creation of zigBeeTask */
    osThreadDef(zigBeeTask, StartZigBeeTask, osPriorityAboveNormal, 0, 1024);
    zigBeeTaskHandle = osThreadCreate(osThread(zigBeeTask), NULL);

    osThreadDef(zigBeeStateMachineTask, ZigBeeSMTask, osPriorityAboveNormal, 0, 1024);
    zigBeeSMTaskHandle = osThreadCreate(osThread(zigBeeStateMachineTask), NULL);
}

///*******************************************************************************
//* @brief  Join a ZigBee network
//* @inputs pJoinMode : pointer to ZigBee join mode (auto or specific)
//* @inputs pChannel : pointer to the specific ZigBee channel
//* @retval None
//* @author Jean-Fran�ois Many
//* @date   2016/09/19
//*******************************************************************************/
//void ZBTHRD_JoinZigBeeNetwork(JoinNetwork_t* pJoinMode, NetworkChannel_t* pChannel)
//{
//
//}

/*******************************************************************************
* @brief  remove a zigbee stat from the network
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/10/29
*******************************************************************************/
void ZBTHRD_RemoveZBStat(uint8_t statIndex)
{
    ZGB_PostEventsWithStatId (ZBEVENT_REMOVESTAT, statIndex);
}

/*******************************************************************************
* @brief  Put the zigbee network in association mode
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/10/29
*******************************************************************************/
void ZBTHRD_AddZBStat(uint8_t startStop)
{
    if (0 == startStop)
    {
        ZGB_PostEvents(ZBEVENT_STOPADDTSTAT);
    }
    else
    {
        ZGB_PostEvents(ZBEVENT_ADDTSTAT);
    }
}

/*******************************************************************************
* @brief  Form a ZigBee network
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/10/29
*******************************************************************************/
void ZBTHRD_FormZigBeeNetwork(void)
{
    ZGB_PostEvents(ZBEVENT_STARTINITIALIZATION);
}

/*******************************************************************************
* @brief  Close a ZigBee network
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/11/01
*******************************************************************************/
void ZBTHRD_CloseZigBeeNetwork(void)
{
}

/*******************************************************************************
* @brief  ReOpen a ZigBee network
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/11/04
*******************************************************************************/
void ZBTHRD_ReOpenZigBeeNetwork(void)
{
}

/*******************************************************************************
* @brief  Extend the Permit Join period
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/11/08
*******************************************************************************/
void ZBTHRD_ExtendPermitJoin(void)
{

}

/*******************************************************************************
* @brief  Reconfigure a ZigBee thermostat
* @inputs index: Thermostat index
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/04/06
*******************************************************************************/
void ZBTHRD_ReconfigureZBStat(uint8_t statIndex)
{
    ZGB_PostEventsWithStatId(ZBEVENT_RECONFIGURE, statIndex);
}

/*******************************************************************************
* @brief  Rediscover a ZigBee thermostat if the short address is unknown
* @inputs shortAddress: Thermostat's short address
* @retval None
* @author Jean-Fran�ois Many
* @date   2018/01/30
*******************************************************************************/
void ZBTHRD_RediscoverZBStat(uint16_t shortAddress)
{
    SendIeeeAddrReq(shortAddress);
}

/*******************************************************************************
* @brief  request a factory new reset of the zigbee radio
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/11/08
*******************************************************************************/
void ZBTHRD_ResetRadioToFactoryNew(void)
{
    ZGB_PostEvents(ZBEVENT_RESETRADIO_TO_FACTORYNEW);
}

/*******************************************************************************
* @brief  request a soft reset of the zigbee radio
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2016/11/08
*******************************************************************************/
void ZBTHRD_ResetRadio(void)
{
    ZGB_PostEvents(ZBEVENT_RESETRADIO);
}

/*******************************************************************************
* @brief  Reinitialize the ZigBee UART
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/03/10
*******************************************************************************/
void ZBTHRD_ReinitUart(void)
{
    ZGB_PostEvents(ZBEVENT_RESET_ZIGBEEMODULE);
}

/*******************************************************************************
* @brief  Trigger an update for the ZigBee module
* @inputs None
* @retval None
* @author Jean-Fran?ois Many
* @date   2017/07/12
*******************************************************************************/
void ZBTHRD_UpdateZigBeeModule(void)
{
    ZGB_PostEvents(ZBEVENT_UPDATEZIGBEEMODULE);
}

/*******************************************************************************
* @brief  ZigBee module stack version received
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2017/09/25
*******************************************************************************/
void ZBTHRD_StackVersionReceived(void)
{
    ZGB_PostEvents(ZBEVENT_STACKVERSIONRECEIVED);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void zigBeeEndOfProvisionalTime( TimerHandle_t xTimer )
{
    zibBeeModuleResetCnt = 1;
    if (0 == isZigBeeModuleUnusable())
    {
        DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,&(uint8_t){DB_CLEAR_ALERT | DBALERT_ZIGBEE_ERROR}, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    }
    else
    {
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void ZigBeeModuleResetOccurs (void)
{
    if (1 <= zibBeeModuleResetCnt)
    {
        if (NULL == ZigBeeModuleProvisionalTimer)
        {
            ZigBeeModuleProvisionalTimer = xTimerCreate ("ZigBee Provisional Timer",
                                               pdMS_TO_TICKS( ZB_MODULE_EXPIRATION_TIME ),
                                               pdFALSE,     //one-shot
                                               (void*)0,
                                               zigBeeEndOfProvisionalTime);

        }
        if (NULL != ZigBeeModuleProvisionalTimer)
        {
            xTimerStart(ZigBeeModuleProvisionalTimer, 0);
        }
    }
    else if (ZB_MODULE_RESET_CNT_MAX <= zibBeeModuleResetCnt)
    {
        DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,&(uint8_t){DB_SET_ALERT | DBALERT_ZIGBEE_ERROR}, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    }
    else
    {
        zibBeeModuleResetCnt++;
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author Jean-Fran�ois Many
* @date   2017/03/10
*******************************************************************************/
void ZBTHRD_KeepSameNetwork(uint8_t keep)
{
    keepSameNetwork = keep;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
uint8_t getKeepSameNetwork (void)
{
    return keepSameNetwork;
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void ZBTHRD_IdentifyQuery(void)
{
    StartFindAndBind();
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void ZBTHRD_SetUseOfInstallCode(void)
{
    ZbSetUseOfInstallCode();
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void ZBTHRD_ClearUseOfInstallCode(void)
{
    ZbClearUseOfInstallCode();
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void ZBTHRD_SetInstallCode(void)
{
    uint8_t* pInstallCode;
    uint8_t* pMacAddress;

    pInstallCode = GetInstallCode();
    pMacAddress = GetMacAddress();
    ZbSetInstallCode(pMacAddress, pInstallCode);
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
#pragma optimize = none
void StartZigBeeTask(void const * argument)
{
    extern zbSerialMsg_t * zbMsg_Head;
    extern zbSerialMsg_t * zbMsg_Tail;
    static char cmdBuf[CMD_BUF_SIZE + 1];             // Additional space for end-of-string
#ifdef DEVKIT_PLATFORM
#else
    GPIO_InitTypeDef GPIO_InitStruct;
#endif

    consoleProperties_t zigBeeInterface =
    {
        .processFunc = processZBCommand,
        .cmdBuf = cmdBuf,
        .cmdBufIdx = 0,
    };

    Sync_Barrier((barriers_name_t)(BARRIER_TASK_DATABASE));

    statsToRemove = 0;

    /******************************************************************/

#ifdef FREERTOS_SUPPORT
    zbEventChannel = vTraceStoreUserEventChannelName("ZigBee Events");
#endif

    ZigBeePingTrigger = xTimerCreate ("ZigBee Ping Trigger",
                                       pdMS_TO_TICKS( ZIGBEE_PING_PERIOD ),
                                       pdTRUE,     //auto-reload
                                       (void*)0,
                                       OnZigBeePingTrigger);
    configASSERT(ZigBeePingTrigger);

    ZigBeePingTimeout = xTimerCreate ("ZigBee Ping Timeout",
                                       pdMS_TO_TICKS( ZIGBEE_PING_TIMEOUT ),
                                       pdFALSE,     //one-shot
                                       (void*)0,
                                       OnZigBeePingTimeout);
    configASSERT(ZigBeePingTimeout);

    bBitCloudPeriodicRx = xTimerCreate ("BitCloud Rx Event",
                                       pdMS_TO_TICKS(10),
                                       pdTRUE,          //Auto reload
                                       (void*)0,
                                       ZigBeeUartRx_PeriodicBufferRead);

    ZigBeeTstatTimeout = xTimerCreate ("ZigBee Tstat Timeout",
                                       pdMS_TO_TICKS( ZIGBEE_TSTAT_TIMEOUT ),
                                       pdTRUE,     //auto-reload
                                       (void*)0,
                                       OnZigBeeTstatTimeout);

    xTimerStart(ZigBeeTstatTimeout, 0);     //configASSERT embedded in xTimerStart
    xTimerStart(bBitCloudPeriodicRx, 0);    //configASSERT embedded in xTimerStart

#ifdef DEVKIT_PLATFORM
#else
    GPIO_InitStruct.Pin = ZIGBEE_BYPASS_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(ZIGBEE_BYPASS_GPIO_Port, &GPIO_InitStruct);
#endif

    InitZigBeeStateMachines();
    RegisterZigBeeNotifications();

    zibBeeModuleResetCnt = 0;
    ZigBeeModuleProvisionalTimer = NULL;

    Sync_TaskReady (BARRIER_TASK_ZIGBEE);

    /* Infinite loop */
    for(;;)
    {
        zigBeeNotifID_t notifId, notifMask;

        xTaskNotifyWait(0,0xffffffff,(uint32_t*)&notifId,portMAX_DELAY);

//        vTracePrintF(zbEventChannel,"zigbee notif 0x%x",notifId);
        notifMask = (zigBeeNotifID_t)0x1;
        while (NULL != notifId)
        {
            switch (notifId & notifMask)
            {
            /***************************************************/
            /***************************************************/
            case ZBTHRD_INCOMING_MSG:
                if (0 != isZigbeeUpAndRunning())
                {
                    uint8_t incomingByteCnt;
                    char incomingBuffer[CONSOLE_RX_BUFFER_LENGTH];

                    incomingByteCnt = HAL_ReadUART ((uint8_t*)incomingBuffer);
                    for (uint8_t i = 0;i < incomingByteCnt; i++)
                    {
                        if (0 != isInUpdateZbModule())
                        {
                            RxBootloaderMsg(incomingBuffer[i]);

                        }
                        else
                        {
                            consoleRx(incomingBuffer[i], &zigBeeInterface);
                        }
                    }
                }
                break;

            /***************************************************/
            /***************************************************/
            case ZBTHRD_MSG_TIMEOUT:
                ResendLastMsg();
                break;

            /***************************************************/
            /***************************************************/
            case ZBTHRD_MSG_SENT:
                zbMsg_Head->msgState = ZBMSG_SENT;
                FreeLastSentMsg(0,ZB_INMSG_DONT_CARE);
                SendNextMsg();
                break;

            /***************************************************/
            /***************************************************/
            case ZBTHRD_SENDNEXTMSG:
            case ZBTHRD_OUTGOING_MSG:
                SendNextMsg();
                break;

            /***************************************************/
            /***************************************************/
            default:
                break;
            }

            notifId &= ~notifMask;
            notifMask <<= 1;
        }
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
void ZigBeeSMTask(void const * argument)
{
    for (;;)
    {
        xTaskNotifyWait(0,0xffffffff,NULL,pdMS_TO_TICKS(10));
        ExecuteEvent();
    }
}


/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
