/*******************************************************************************
* @file    THREAD_ZigBee.h
* @date    2016/08/25
* @authors J-F. Simard
* @brief
*******************************************************************************/

#ifndef THREAD_ZIGBEE_H
#define THREAD_ZIGBEE_H


/*******************************************************************************
* Includes
*******************************************************************************/
#include <stdint.h>
#include "cmsis_os.h"

/*******************************************************************************
* Public constants definitions
*******************************************************************************/

typedef enum
{
    ZBTHRD_INCOMING_MSG         = (1<<0),
    ZBTHRD_REINITUART           = (1<<1),
    ZBTHRD_OUTGOING_MSG         = (1<<2),
    ZBTHRD_MSG_SENT             = (1<<3),
    ZBTHRD_MSG_TIMEOUT          = (1<<4),
    ZBTHRD_SENDNEXTMSG          = (1<<5),
    ZBTHRD_EVENT                = (1<<6),
} zigBeeNotifID_t;

/*******************************************************************************
* Public structures definitions
*******************************************************************************/
typedef struct
{
    zigBeeNotifID_t notifId;
    uint32_t data;         //data use depends on the notification type
} zigBeeNotification_t;



/*******************************************************************************
* Public variables declarations
*******************************************************************************/

/*******************************************************************************
* Public functions declarations
*******************************************************************************/
//void ZBTHRD_JoinZigBeeNetwork(JoinNetwork_t* pJoinMode, NetworkChannel_t* pChannel);
void ZBTHRD_FormZigBeeNetwork(void);
void InitThread_ZigBee(void);
void ZBTHRD_CloseZigBeeNetwork(void);
void ZBTHRD_ReOpenZigBeeNetwork(void);
void ZBTHRD_ExtendPermitJoin(void);
void ZBTHRD_RemoveZBStat(uint8_t statIndex);
void ZBTHRD_AddZBStat(uint8_t startStop);
void ZBTHRD_ResetRadioToFactoryNew(void);
void ZBTHRD_ResetRadio(void);
void ZBTHRD_ReinitUart(void);
void ZBTHRD_ReconfigureZBStat(uint8_t statIndex);
void ZBTHRD_UpdateZigBeeModule(void);
void ZBTHRD_NotifyNewEvent(void);
uint8_t getKeepSameNetwork (void);
void ZBTHRD_KeepSameNetwork(uint8_t keep);
void ZBTHRD_IdentifyQuery(void);
void ZBTHRD_SetUseOfInstallCode(void);
void ZBTHRD_ClearUseOfInstallCode(void);
void ZBTHRD_SetInstallCode(void);
void ZBTHRD_StackVersionReceived(void);
void ZBTHRD_RediscoverZBStat(uint16_t shortAddress);

#endif /* THREAD_ZIGBEE_H */
/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
