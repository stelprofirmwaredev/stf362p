/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2016, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    THREAD_Default.c
* @date    2016/08/25
* @authors J-F. Simard
* @brief
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include <stdint.h>

#include "cmsis_os.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_rtc.h" //new

#include ".\Default\inc\THREAD_Default.h"
#include ".\Console\inc\THREAD_Console.h"
#include ".\sync_barrier\inc\SyncBarrier.h"


#include "BootLoader_OTA_Control.h"
#include ".\HAL\inc\HAL_WatchDog.h"
#include ".\inc\FlashOps.h"
#include ".\WiFi\inc\THREAD_WiFi.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\Load Management\inc\THREAD_LoadManagement.h"
#include "APP_SetpointManager.h"
#include "Program_Tracking.h"


/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
#ifdef FREERTOS_SUPPORT
    #include "trcRecorder.h"
    char* rtcEventChannel;
#endif

#define DEFAULTTHRD_RTC_WKUP    (1<<0)
#define SYNC_TIME               (180) //3h AM
#define SUNDAY                  7

/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
TimerHandle_t ProvisionalFirmwareTimer;
RTC_HandleTypeDef RTC_Handle;

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/
osThreadId defaultTaskHandle;
extern dbType_Time_t                SystemTime;


/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/
static void ProvisionalFirmwareTimerCB( TimerHandle_t xTimer );
static void RTC_Init(void);
static DB_NOTIFICATION(OnCalendarDateUpdate);
static DB_NOTIFICATION(OnTimeUpdate);
static void UpdateDBCalendar (void);
static int8_t GetDaylightSavingOffset(RtcTime_t time);
static void EnableBkUpDomainAccess(void);

/*******************************************************************************
*    Private functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/10/14
*******************************************************************************/
static void ProvisionalFirmwareTimerCB( TimerHandle_t xTimer )
{
    BootLoader_ClearProvisionalState();
    xTimerDelete(ProvisionalFirmwareTimer, 0);
}


/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/10/24
*******************************************************************************/
static void RTC_Init(void)
{
  RTC_TimeTypeDef sTime;
  RTC_DateTypeDef sDate;

    /**Initialize RTC and set the Time and Date
    */
  RTC_Handle.Instance = RTC;
  
  //access to bckdomain needs to be enabled if bkdomain reset (calendar not set)
  if(__HAL_RTC_ALARM_GET_FLAG(&RTC_Handle, RTC_FLAG_INITS) == 0)
  {
     EnableBkUpDomainAccess(); //new
  }
  
  //init and alarms/wkup timer always needs to be set
  RTC_Handle.Init.HourFormat = RTC_HOURFORMAT_24;
  RTC_Handle.Init.AsynchPrediv = 127; 
  RTC_Handle.Init.SynchPrediv = 255; 
  RTC_Handle.Init.OutPut = RTC_OUTPUT_DISABLE/*RTC_OUTPUT_WAKEUP*/;
  RTC_Handle.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  RTC_Handle.Init.OutPutType = RTC_OUTPUT_TYPE_PUSHPULL;
  if (HAL_RTC_Init(&RTC_Handle) != HAL_OK)
  {
//    Error_Handler();
  }

  //if calendar set, then values stored so no reset to for RTC time and date
  //only needs to advice display that time is set
  if(__HAL_RTC_ALARM_GET_FLAG(&RTC_Handle, RTC_FLAG_INITS) == 1)
  {
      dbType_Time_t localTime;
      dbType_Date_t localDate;
      
      HAL_RTC_GetTime(&RTC_Handle, &sTime, RTC_FORMAT_BIN);
      HAL_RTC_GetDate(&RTC_Handle, &sDate, RTC_FORMAT_BIN);
      
      localTime.Hours = sTime.Hours;
      localTime.Minutes = sTime.Minutes;
      localTime.Seconds = sTime.Seconds;
      localTime.MinutesOfDay = (localTime.Hours * 60) + localTime.Minutes;
      DBTHRD_SetData(DBTYPE_LOCALTIME, (void*)&localTime, INDEX_DONT_CARE, DBCHANGE_LOCAL);
       
      localDate.Day = sDate.Date;
      localDate.DayOfWeek = sDate.WeekDay;
      localDate.Month = sDate.Month;
      localDate.Year = sDate.Year + 2000;
      DBTHRD_SetData(DBTYPE_DATE, (void*)&localDate, INDEX_DONT_CARE, DBCHANGE_LOCAL);
  }
  else
  {
    sTime.Hours = 0x0;
    sTime.Minutes = 0x0;
    sTime.Seconds = 0x0;
    sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
    sTime.StoreOperation = RTC_STOREOPERATION_RESET;
    if (HAL_RTC_SetTime(&RTC_Handle, &sTime, RTC_FORMAT_BIN) != HAL_OK)
    {
  //    Error_Handler();
    }

    sDate.WeekDay = RTC_WEEKDAY_MONDAY;
    sDate.Month = RTC_MONTH_JANUARY;
    sDate.Date = 0x1;
    sDate.Year = 0x0;

    if (HAL_RTC_SetDate(&RTC_Handle, &sDate, RTC_FORMAT_BIN) != HAL_OK)
    {
  //    Error_Handler();
    }
  }
  HAL_RTC_DeactivateAlarm(&RTC_Handle, RTC_ALARM_A);
  HAL_RTC_DeactivateAlarm(&RTC_Handle, RTC_ALARM_B);


  /************************************************************/
  /*  Configure WakeUp timer at 1 second.                     */
  /*  DO NOT rely on this event for a precise event, as the   */
  /*  combination of the interrupt priority and the thread    */
  /*  priority causes delay in the service of this event      */
  /************************************************************/
  __HAL_RTC_WAKEUPTIMER_CLEAR_FLAG(&RTC_Handle, RTC_FLAG_WUTF);

  if (HAL_RTCEx_SetWakeUpTimer_IT(&RTC_Handle, 0, RTC_WAKEUPCLOCK_CK_SPRE_16BITS) != HAL_OK)
  {
//    Error_Handler();
  }
  
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author S. Pereira
* @date   2019/10/04
*******************************************************************************/
static void EnableBkUpDomainAccess(void){
  __HAL_RCC_PWR_CLK_ENABLE();
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_RTC_CONFIG(RCC_RTCCLKSOURCE_LSE); //used to select clk source for RTC if not done already
  __HAL_RCC_RTC_ENABLE();
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/10/24
*******************************************************************************/
static DB_NOTIFICATION(OnCalendarDateUpdate)
{
    RTC_DateTypeDef sDate;
    dbType_Date_t localDate;

    DBTHRD_GetData(DBTYPE_DATE, (void*)&localDate, INDEX_DONT_CARE);


    sDate.Month = localDate.Month;
    sDate.Date = localDate.Day;
    sDate.Year = localDate.Year-2000;
    sDate.WeekDay = localDate.DayOfWeek;

    if (HAL_RTC_SetDate(&RTC_Handle, &sDate, RTC_FORMAT_BIN) != HAL_OK)
    {
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/10/24
*******************************************************************************/
static DB_NOTIFICATION(OnTimeUpdate)
{
    RTC_TimeTypeDef sTime;
    dbType_Time_t localTime;
    ScheduleModeEnum_t scheduleMode;
    uint8_t alert;

    DBTHRD_GetData(DBTYPE_LOCALTIME, (void*)&localTime, INDEX_DONT_CARE);

    sTime.Hours = localTime.Hours;
    sTime.Minutes = localTime.Minutes;
    sTime.Seconds = localTime.Seconds;
    sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
    sTime.StoreOperation = RTC_STOREOPERATION_RESET;
    if (HAL_RTC_SetTime(&RTC_Handle, &sTime, RTC_FORMAT_BIN) != HAL_OK)
    {
    }
    if (SystemTime.IsTimeSet == 0)
    {
        SystemTime.IsTimeSet = 1;
        alert = DB_CLEAR_ALERT | DBALERT_TIME_NOT_SET;
        DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    }
    //If in schedule mode and not in derogation
    DBTHRD_GetData(DBTYPE_SCHEDULE_MODE,(void*)&scheduleMode, THIS_THERMOSTAT);
    if (scheduleMode == MODE_PROG)
    {
        FindActiveProgram();
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/10/24
*******************************************************************************/
static void UpdateDBCalendar (void)
{
    dbType_Time_t localTime;
    dbType_Date_t localDate;
    RTC_TimeTypeDef sTime;
    RTC_DateTypeDef sDate;
    static uint8_t lastMinute = 0;
    static uint8_t lastDay = 0;
    static uint8_t initialCheck = 1; 
    int8_t dstOffet = 0;
    uint8_t alert;

    HAL_RTC_GetTime(&RTC_Handle, &sTime, RTC_FORMAT_BIN);
    HAL_RTC_GetDate(&RTC_Handle, &sDate, RTC_FORMAT_BIN);

    DBTHRD_GetData(DBTYPE_LOCALTIME, (void*)&localTime, NULL);

    if (localTime.IsTimeSet)
    {
        localTime.Hours = sTime.Hours;
        localTime.Minutes = sTime.Minutes;
        localTime.Seconds = sTime.Seconds;
        localTime.MinutesOfDay = (localTime.Hours * 60) + localTime.Minutes;

        if (lastMinute != localTime.Minutes)
        {
            if (localTime.IsDstActive != 0)
            {
                dstOffet = GetDaylightSavingOffset(localTime);
                if (dstOffet != 0)
                {
                    localTime.Hours += dstOffet;
                }
            }
        }

        DBTHRD_SetData(DBTYPE_LOCALTIME, (void*)&localTime, INDEX_DONT_CARE, DBCHANGE_LOCAL);

        if (lastMinute != localTime.Minutes)
        {
            DBTHRD_SetData(DBTYPE_MINUTE_CHANGE,(void*)0, INDEX_DONT_CARE, DBCHANGE_LOCAL);
        }
        lastMinute = localTime.Minutes;

        localDate.Day = sDate.Date;
        localDate.DayOfWeek = sDate.WeekDay;
        localDate.Month = sDate.Month;
        localDate.Year = sDate.Year + 2000;

        if (lastDay != localDate.Day)
        {
            if (initialCheck == 0)
            {
                DBTHRD_SetData(DBTYPE_DATE, (void*)&localDate, INDEX_DONT_CARE, DBCHANGE_LOCAL);
            }
            lastDay = localDate.Day;
        }

        initialCheck = 0;

#ifdef FREERTOS_SUPPORT
        vTracePrint(rtcEventChannel, "Calendar update from RTC");
#endif
    }
    else
    {
        alert = DB_SET_ALERT | DBALERT_TIME_NOT_SET;
        DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    }
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
static int8_t GetDaylightSavingOffset(RtcTime_t time)
{
    int8_t DstOffset = 0;
    dbType_Date_t localDate;
    static uint8_t dstSetbackApplied = 0;

    //Get Date
    DBTHRD_GetData(DBTYPE_DATE, (void*)&localDate, THIS_THERMOSTAT);

    if (time.IsTimeSet)
    {
        //DST change only occurs at 2h00 AM
        if ((time.Hours == 2) && (time.Minutes == 0))
        {
            //DST change only occurs on Sunday
            if (localDate.DayOfWeek == SUNDAY)
            {
                //DST change only occurs on March or November
                if ((localDate.Month == 3) || (localDate.Month == 11))
                {
                    //During March, DST will occur on 2nd Sunday of the month
                    if (localDate.Month == 3)
                    {
                        //The second Sunday of March will always be between the 8th and 14th inclusive
                        if ((localDate.Day >= 8) && (localDate.Day <= 14))
                        {
                            //Add one hour
                            DstOffset = 1;
                        }
                        //We are not in November, so we can cancel the DST setback applied flag
                        dstSetbackApplied = 0;
                    }
                    //During November, DST will occur on 1st Sunday of the month
                    else
                    {
                        //The first Sunday of November will always be between the 1st and 7th inclusive
                        if ((localDate.Day >= 1) && (localDate.Day <= 7))
                        {
                            //Apply the change if the setback was not already done today
                            if (dstSetbackApplied == 0)
                            {
                                //Substract one hour
                                DstOffset = -1;
                            }
                            //DST setback is now applied, make sure we don't apply it again when we hit the 2h00 AM barrier once more today
                            dstSetbackApplied = 1;
                        }
                        else
                        {
                            //We are not in the first week of November, so we can cancel the DST setback applied flag
                            dstSetbackApplied = 0;
                        }
                    }
                }
                else
                {
                    //We are not in November, so we can cancel the DST setback applied flag
                    dstSetbackApplied = 0;
                }
            }
            else
            {
                //We are not on Sunday, so we can cancel the DST setback applied flag
                dstSetbackApplied = 0;
            }
        }
        else
        {
            //If we are now passed 2h00AM (which means there won't be another dst change)
            if ((time.Hours >= 3) || ((time.Hours == 2) && (time.Minutes != 0)))
            {
                //Cancel the DST setback applied flag
                dstSetbackApplied = 0;
            }
        }
    }
    else
    {
        //Time is not set, so we can cancel the DST setback applied flag
        dstSetbackApplied = 0;
    }

    return DstOffset;
}

/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author J-F. Simard
* @date   2016/10/14
*******************************************************************************/
void InitThread_Default(void)
{
        /* definition and creation of defaultTask */
    osThreadDef(defaultTask, StartDefaultTask, osPriorityLow, 0, 256);
    defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);
#ifdef FREERTOS_SUPPORT
    rtcEventChannel = vTraceStoreUserEventChannelName("RTC Events");
#endif
}

/*******************************************************************************
* @brief
* @inputs None
* @retval None
* @author
* @date
*******************************************************************************/
#pragma optimize=none
void StartDefaultTask(void const * argument)
{
    uint32_t taskNotifications;
    loaderStates_t bootLoaderState = GetBootLoaderState();

    Sync_Barrier((barriers_name_t)(BARRIER_TASK_CONSOLE));


    RTC_Init();

    DBTHRD_SubscribeToNotification (DBTYPE_LOCALTIME,
                                    OnTimeUpdate,
                                    DBCHANGE_LOCAL,
                                    INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification (DBTYPE_DATE,
                                    OnCalendarDateUpdate,
                                    DBCHANGE_LOCAL,
                                    INDEX_DONT_CARE);



    if ((LOADER_STATE_3 == bootLoaderState) || (LOADER_STATE_7 == bootLoaderState))
    {
        ProvisionalFirmwareTimer = xTimerCreate (
                                       "ProvisionFirmwareTimer",
                                       PROVISIONAL_DELAY,
                                       pdFALSE,         //one-shot
                                       (void*)0,
                                       ProvisionalFirmwareTimerCB);

        xTimerStart(ProvisionalFirmwareTimer, 0);
    }
    else
    {
        if (LOADER_INVALID_STATE == bootLoaderState)
        {
            LoaderStateCorrupted_ResetToFactory();
        }
    }

    Sync_TaskReady (BARRIER_TASK_DEFAULT);

    /* Infinite loop */
    for(;;)
    {
#ifdef __ICCARM__
        xTaskNotifyWait (0x0000,
                         0xffff,
                         &taskNotifications,
                         pdMS_TO_TICKS(25));
        /*************** Thread Execution blocked here ***************/
        /**** Thread resume every 25ms or on Event Notifications ****/
#endif
        RefreshWatchDog();

                    /********** Event Notification : RTC Wake up *********/
        if (taskNotifications&DEFAULTTHRD_RTC_WKUP)
        {
            UpdateDBCalendar ();
        }
    }
}


/**
  * @brief  Initializes the RTC MSP.
  * @param  hrtc: pointer to a RTC_HandleTypeDef structure that contains
  *                the configuration information for RTC.
  * @retval None
  */
void HAL_RTC_MspInit(RTC_HandleTypeDef* hrtc)
{
  __HAL_RCC_RTC_ENABLE();
    HAL_NVIC_SetPriority(RTC_WKUP_IRQn, 10, 0);
    HAL_NVIC_EnableIRQ(RTC_WKUP_IRQn);
}


/**
  * @brief  Wake Up Timer callback at 1 second period.
  *
  *  DO NOT rely on this event for a precise event, as the
  *  combination of the interrupt priority and the thread
  *  priority causes random delays in the service of this event
  *
  * @param  RTC_Handle: pointer to a RTC_HandleTypeDef structure that contains
  *                the configuration information for RTC.
  * @retval None
  */
void HAL_RTCEx_WakeUpTimerEventCallback(RTC_HandleTypeDef *RTC_Handle)
{
    BaseType_t xHigherPriorityTaskWoken;

    xHigherPriorityTaskWoken = pdFALSE;
    xTaskNotifyFromISR (defaultTaskHandle,
                        DEFAULTTHRD_RTC_WKUP,
                        eSetBits,
                        &xHigherPriorityTaskWoken);
    portYIELD_FROM_ISR (xHigherPriorityTaskWoken);

#ifdef FREERTOS_SUPPORT
        vTracePrint(rtcEventChannel, "WakeUp Interrupt");
#endif

}

/** Copyright(C) 2016 Stelpro Design, All Rights Reserved**/
