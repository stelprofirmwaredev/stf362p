///////////////////////////////////////////////////////////////////////////////
// Application Control
//
// Goal: Calculate and provide ambient temperature
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __TEMPERATURECOMPENSATION_H_
#define __TEMPERATURECOMPENSATION_H_

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "typedef.h"
#include "common_types.h"
#include "OsTask.h"
#include ".\DB\inc\THREAD_DBDataTypes.h"

///////////////////////////////////////////////////////////////////////////////
// Estimate parameters
///////////////////////////////////////////////////////////////////////////////
#define TMP_MAX_COMP_TEMP       m_DegC(10)
#define TMP_DEFAULT_COMP_TEMP   m_DegC(4)

#define TEMP_ACQ_DELAY              MS_TO_OSTICK(10000)

//
//! Temperature Compensation parameters
// \typedef <CompensationParam_t>
//
typedef struct {
    float ConstA;
    float ConstB;
    float ConstC;
} CompensationParam_t;

///////////////////////////////////////////////////////////////////////////////
// Estimate Data
// This structure is included in the header file to allow logging function
// to access internal data by a an accessor function
///////////////////////////////////////////////////////////////////////////////
//
//! Estimate data for an ambient and a compensation sensor
// \typedef <TemperatureData_t>
//
typedef struct {
    //External data
    TEMPERATURE_C_t         sensorAmbient;                  //Latest sensors value
    TEMPERATURE_C_t         sensorComp;
    TEMPERATURE_C_t         sensorTriac;
    CompensationParam_t     *compParam;                     //Pointer to estimate parameters

    //Output
    TEMPERATURE_C_t         displayTemp;                    //UI temperature
    TEMPERATURE_C_t         controlTemp;                    //Control temperature

}TemperatureData_t;


///////////////////////////////////////////////////////////////////////////////
// Temperature acquisition Functions
///////////////////////////////////////////////////////////////////////////////
//Action performed once acquisition is completed
OSTASK_DEF (OnSensorsAdcComplete);
void TMP_Init(void);
//Begin temperature acquisition
//Set callback on acquisition complete

//Retreive estimated temp
void TMP_GetDisplayedTemperature(TEMPERATURE_C_t * estimated);

//Retreive control temp
void TMP_GetControlTemp(TEMPERATURE_C_t * control);

CompensationParam_t TMP_GetEstimateParam(void);
int8u TMP_IsSensorValid(TEMPERATURE_C_t sensor);
int8u TMP_GetCompensationChecksum (void);

#endif

