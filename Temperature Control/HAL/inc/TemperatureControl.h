///////////////////////////////////////////////////////////////////////////////
// Application Control
//
// Goal: Calculate and provide ambient temperature
//       Calculate and provide heat demand request
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __TEMPERATURECONTROL_H_
#define __TEMPERATURECONTROL_H_

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////

#include "typedef.h"
#include "common_types.h"
#include "OsTask.h"

///////////////////////////////////////////////////////////////////////////////
// Control parameters and Control data
// These structures are included in the header file to allow logging function
// to access internal data by a an accessor function, giving a code size
// reduction
///////////////////////////////////////////////////////////////////////////////
typedef enum
{
    CTLEVENT_NOEVENT = 0,
    CTLEVENT_SP_CHANGE_UP,
    CTLEVENT_SP_CHANGE_DOWN,
}ControlEvents_t;

typedef struct {
    int8u Kp;
    int8u Ki;
    int8u TauIntegral;
    int8s PropTreshold;
} CtlParam_t;

//Control data
typedef struct{
    //External data
    TEMPERATURE_C_t     ControlTemperature;             //actual temp
    TEMPERATURE_C_t     Setpoint;                       //setpoint
    CtlParam_t const FLASH_MEM * Param;                 //parameters for control
    ControlEvents_t     ControlEvents;
    //output
    int8u               HeatDemand;                     //Heat Demand in percent 0-100%
    TEMPERATURE_C_t     Error;                          //Control Error
    //private
    int32s              IntegralError;                  //Integral Error X 100 X 1000
    int8s               PI_Prop;                        //PI Proportional power
    int8s               PI_Int;                         //PI Integral power
}ControlData_t;


#define CTL_MAX_ERROR       m_DegC(10)
#define INTEGRAL_SCALE      1000


///////////////////////////////////////////////////////////////////////////////
// Control functions
///////////////////////////////////////////////////////////////////////////////
//Init heat demand module
void CTL_Init(void);

//CTL_OnEstimateUpdate called periodicly at every 10 seconds
//or on ambient indoor temperature update
void CTL_OnEstimateUpdate(void);

void CTL_OnSetpointChange(uint8_t fromCloud);        //Callled on setpoint change

void CTL_SetHeatDemand(int8u heatDemand);

int16u CTL_GetControlBand(void);

//Retreive control data log
const ControlData_t * CTL_GetTempControlLog(void);

CtlParam_t CTL_GetTempControlParam(int8u shortcycle);
void CTL_SetTempControlParam(CtlParam_t param, int8u shortcycle);
int8u CTL_GetControlChecksum (void);

#endif

