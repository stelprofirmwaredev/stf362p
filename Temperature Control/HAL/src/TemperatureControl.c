/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       TemperatureControl.c

    \brief      Temperature control task of the thermostat

    \author     Jean-Fran�ois Many
                J-F. Simard

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the implementation of the temperature control of the
    thermostat.

*******************************************************************************/

/*-------------------------------------------------------------------------------
    INCLUDE
-------------------------------------------------------------------------------*/
#include "typedef.h"

#include "common_types.h"
#include ".\DB\inc\THREAD_DB.h"

#include "OsTask.h"

#include ".\HAL\inc\HAL_LoadManagement.h"
#include ".\HAL\inc\TemperatureControl.h"

#ifdef SETPOINT_TO_DUTYCYCLE
    #include "APP_SetpointManager.h"
#endif

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// Heat demand Control
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
/*-------------------------------------------------------------------------------
    DEFINITIONS
-------------------------------------------------------------------------------*/
#define TAU_INTEGRAL 90
#define MAX_INTEGRAL (TAU_INTEGRAL * 100)

#define CONTROLLER_UPDATE_TICK          10          //expressed in seconds

#define STEP_1_DELAY    180//30 minutes (x10s)
#define STEP_2_DELAY    360//60 minutes (x10s)
#define STEP_3_DELAY    540//90 minutes (x10s)
#define STEP_4_DELAY    720//120 minutes (x10s)

/*-------------------------------------------------------------------------------
    DATA
-------------------------------------------------------------------------------*/
//Control parameters for Fan with medium cycle
CtlParam_t ControlParamsRelayCycle = {  /* Fan Base medium Cycles */
    33,             // Kp (% / �C);
    5,             // Ki (% / �C);
    15,             // TauIntegral (minutes);
    33,             // Proportionnal treshold (% heat demand)
};

//Control Data variable
static ControlData_t Control;

/*-------------------------------------------------------------------------------
    DECLARATIONS
-------------------------------------------------------------------------------*/
//Main PI control function
void CTL_DoPIControl(ControlData_t * ctl, ControlEvents_t event);

//PI individual function
//Error = Setpoint - ControlTemperature
TEMPERATURE_C_t CTL_CalculateError(TEMPERATURE_C_t control_temp, TEMPERATURE_C_t setpoint, TEMPERATURE_C_t maxerror);

//IntegralError = (LastIntegral + Error) / TAU I
int32s CTL_CalculateIntegralError(TEMPERATURE_C_t error, int32s integralError, int8u tauI, int8u gain);

//Percent = Proportional + Integral
int8u CTL_ComputeHeatDemand(int8s prop, int8s integral, ControlEvents_t event, int8s propTreshold, int8u gain);

//Proportional = (Error / band) * 100
int8s CTL_Error2Prop(TEMPERATURE_C_t error, int8u band);

//Adjustment done on setpoint change
void CTL_AdjustOnSetpointChange(ControlData_t * ctl);

static DB_NOTIFICATION(OnControlTemperatureUpdate);
static DB_NOTIFICATION(OnSetpointChange);
static int8u IsSensorValid(TEMPERATURE_C_t sensor);

/*-------------------------------------------------------------------------------
    IMPLEMENTATION
-------------------------------------------------------------------------------*/
/*******************************************************************************
*   \function : void CTL_Init(void){
*	\brief :    Initialize the heat demand algorithm variables
*	\param :    None
*	\return :   None
********************************************************************************/
void CTL_Init(void){

    Control.Error = 0;          //Control Error
    Control.HeatDemand = 0;     //Heat Demand in percent 0-100%
    Control.IntegralError = 0;
    Control.PI_Prop = 0;
    Control.PI_Int = 0;

    DBTHRD_SubscribeToNotification (DBTYPE_CONTROL_TEMPERATURE,
                                    OnControlTemperatureUpdate,
                                    DBCHANGE_LOCAL,
                                    INDEX_DONT_CARE);

    DBTHRD_SubscribeToNotification (DBTYPE_AMBIENT_SETPOINT,
                                    OnSetpointChange,
                                    (dbChangeSource_t)(DBCHANGE_ALLCHANGES&~(DBCHANGE_CLOUD)),
                                    THIS_THERMOSTAT);

    DBTHRD_SubscribeToNotification (DBTYPE_FLOOR_SETPOINT,
                                    OnSetpointChange,
                                    (dbChangeSource_t)(DBCHANGE_ALLCHANGES&~(DBCHANGE_CLOUD)),
                                    THIS_THERMOSTAT);

    DBTHRD_SubscribeToNotification (DBTYPE_FLOOR_MODE,
                                    OnSetpointChange,
                                    DBCHANGE_ALLCHANGES,
                                    THIS_THERMOSTAT);

    DBTHRD_SubscribeToNotification (DBTYPE_OFF_MODE_SETTING,
                                    OnSetpointChange,
                                    DBCHANGE_LOCAL,
                                    THIS_THERMOSTAT);
}

/*******************************************************************************
*   \function : DB_NOTIFICATION(OnControlTemperatureUpdate)
*	\brief :    When a new temperature is acquired, update heat demand
*	\param :    None
*	\return :   None
********************************************************************************/
static DB_NOTIFICATION(OnControlTemperatureUpdate)
{
#ifdef POWER_STEPS
    static uint16_t step_time;
#endif /* POWER_STEPS */
    uint8_t floorMode;

    //Get control temperature
    DBTHRD_GetData(DBTYPE_CONTROL_TEMPERATURE,(void*)&Control.ControlTemperature, INDEX_DONT_CARE);
    DBTHRD_GetData(DBTYPE_FLOOR_MODE,(void*)&floorMode, THIS_THERMOSTAT);
    if (floorMode == CONTROLMODE_FLOOR)
    {
        //Get Setpoint
        DBTHRD_GetData(DBTYPE_FLOOR_SETPOINT,(void*)&Control.Setpoint, THIS_THERMOSTAT);
    }
    else
    {
        //Get Setpoint
        DBTHRD_GetData(DBTYPE_AMBIENT_SETPOINT,(void*)&Control.Setpoint, THIS_THERMOSTAT);
    }

    //Select control parameters
    Control.Param = &ControlParamsRelayCycle;

#ifdef PCT_HEAT
#else
#ifdef POWER_STEPS
    if (step_time < STEP_4_DELAY)
    {
        step_time++;
    }
    if (step_time < STEP_1_DELAY)
    {
        Control.HeatDemand = 0;
    }
    else if (step_time < STEP_2_DELAY)
    {
        Control.HeatDemand = 25;
    }
    else if (step_time < STEP_3_DELAY)
    {
        Control.HeatDemand = 50;
    }
    else if (step_time < STEP_4_DELAY)
    {
        Control.HeatDemand = 75;
    }
    else
    {
        Control.HeatDemand = 100;
    }
    DBTHRD_SetData(DBTYPE_HEAT_DEMAND,(void*)&Control.HeatDemand, THIS_THERMOSTAT, DBCHANGE_LOCAL);
#else
    CTL_DoPIControl(&Control, CTLEVENT_NOEVENT);
#ifndef LOAD_CYCLING
    DBTHRD_SetData(DBTYPE_HEAT_DEMAND,(void*)&Control.HeatDemand, THIS_THERMOSTAT, DBCHANGE_LOCAL);
#endif /* LOAD_CYCLING */
#endif /* POWER_STEPS */
#endif /* PCT_HEAT */
}

/*******************************************************************************
*   \function : DB_NOTIFICATION(OnSetpointChange)
*	\brief :    When a the setpoint is changed
*	\param :    None
*	\return :   None
********************************************************************************/
static DB_NOTIFICATION(OnSetpointChange)
{
    CTL_OnSetpointChange(FALSE);
}

//***********************************************************************************
///	\brief
///
///	\param  None.
///	\return TRUE if any sensor is invalid, FALSE if all sensors are valid.
//***********************************************************************************
static int8u IsSensorValid(TEMPERATURE_C_t sensor)
{
    switch (sensor)
    {
    case DB_SENSOR_INVALID:
    case DB_SENSOR_HIGH:
    case DB_SENSOR_LOW:
        return FALSE;

    default:
        return TRUE;
    }
}

/*******************************************************************************
*   \function : void CTL_OnSetpointChange(uint8_t fromCloud){
*	\brief :    Action to take when a change of setpoint occured
*	\param :    fromCloud: new setpoint received from cloud (TRUE or FALSE)
*	\return :   None
********************************************************************************/
void CTL_OnSetpointChange(uint8_t fromCloud)
{
    static TEMPERATURE_C_t oldControlSetpoint = 0;
    ControlEvents_t event = CTLEVENT_NOEVENT;
    TEMPERATURE_C_t temperature;
    uint8_t floorMode;

    DBTHRD_GetData(DBTYPE_FLOOR_MODE,(void*)&floorMode, THIS_THERMOSTAT);
    if ((floorMode == CONTROLMODE_AMBIENT) || (floorMode == CONTROLMODE_AMBIENTFLOOR))
    {
        //Get Setpoint
        DBTHRD_GetData(DBTYPE_AMBIENT_SETPOINT,(void*)&Control.Setpoint, THIS_THERMOSTAT);
        //Get Temperature
        DBTHRD_GetData(DBTYPE_AMBIENT_TEMPERATURE,(void*)&temperature, THIS_THERMOSTAT);
    }
    else if (floorMode == CONTROLMODE_FLOOR)
    {
        //Get Setpoint
        DBTHRD_GetData(DBTYPE_FLOOR_SETPOINT,(void*)&Control.Setpoint, THIS_THERMOSTAT);
        //Get Temperature
        DBTHRD_GetData(DBTYPE_FLOOR_TEMPERATURE,(void*)&temperature, THIS_THERMOSTAT);
    }
    else
    {
        oldControlSetpoint = Control.Setpoint;
    }

    if (temperature != DB_SENSOR_INVALID)
    {
        //Setpoint has increased
        if (Control.Setpoint > oldControlSetpoint)
        {
            //Setpoint is above temperature
            if (Control.Setpoint > temperature)
            {
                event = CTLEVENT_SP_CHANGE_UP;
            }
        }
        else if (Control.Setpoint < oldControlSetpoint)
        {
            event = CTLEVENT_SP_CHANGE_DOWN;
        }
    }

#ifdef PCT_HEAT
#else
#ifdef POWER_STEPS
#else
    CTL_DoPIControl(&Control,event);
#ifndef LOAD_CYCLING
    DBTHRD_SetData(DBTYPE_HEAT_DEMAND,(void*)&Control.HeatDemand, THIS_THERMOSTAT, DBCHANGE_LOCAL);
#endif /* LOAD_CYCLING */
#endif /* POWER_STEPS */
#endif /* PCT_HEAT */

	LoadManagement_StartNewCycle(fromCloud);           //Trig a new cycle
    oldControlSetpoint = Control.Setpoint;
}

/*******************************************************************************
//  \function : void CTL_SetHeatDemand(int8u){
//	\brief :    Return latest calculated heat demand
//	\param :    None
//	\return :   int8u latest heat demand
********************************************************************************/
void CTL_SetHeatDemand(int8u heatDemand)
{
    Control.HeatDemand = heatDemand;
    DBTHRD_SetData(DBTYPE_HEAT_DEMAND,(void*)&Control.HeatDemand, THIS_THERMOSTAT, DBCHANGE_LOCAL);
}

/*******************************************************************************
//  \function : void CTL_DoPIControl(ControlData_t * ctl){
//	\brief :    Calculate heat demand using a PI algorithm
*	\param :    ControlData_t * ctl = Control data structure containing latest values
*               for input variables: ControlTemperature, Setpoint, * Param
//	\return :   None
********************************************************************************/
void CTL_DoPIControl(ControlData_t * ctl, ControlEvents_t event)
{
    //Get Error
    ctl->Error = CTL_CalculateError(ctl->ControlTemperature, ctl->Setpoint, CTL_MAX_ERROR);
    //Get proportional
    ctl->PI_Prop = CTL_Error2Prop(ctl->Error, ctl->Param->Kp);
    //Compute Integral Error
    ctl->IntegralError = CTL_CalculateIntegralError(ctl->Error, ctl->IntegralError, ctl->Param->TauIntegral, ctl->Param->Ki);
    //Get Integral
    ctl->PI_Int = CTL_Error2Prop((TEMPERATURE_C_t)(ctl->IntegralError/INTEGRAL_SCALE), ctl->Param->Ki);
#ifdef LOAD_CYCLING
#elif defined(SETPOINT_TO_DUTYCYCLE)

    ctl->HeatDemand = ((100*(ctl->Setpoint-MINIMUM_SETPOINT))/(MAXIMUM_SETPOINT-MINIMUM_SETPOINT));
#else
    //Get heat percent
    ctl->HeatDemand = CTL_ComputeHeatDemand(ctl->PI_Prop, ctl->PI_Int, event, ctl->Param->PropTreshold, ctl->Param->Ki);
#endif /* LOAD_CYCLING */
}


/*******************************************************************************
//  \function : int8u CTL_ComputeHeatDemand(int8s prop, int16s integral, int16u tau)
//	\brief :    Convert proportional and integral to percent, verify boundaries
//	\param :    int8s prop = proportional
//              int16s integral = integral
//              int16u tau = Tau of integral
//	\return :   int8u percent value
********************************************************************************/
int8u CTL_ComputeHeatDemand(int8s prop, int8s integral, ControlEvents_t event, int8s propTreshold, int8u gain)
{
    int16s percent;
    static ControlEvents_t activeEvent=CTLEVENT_NOEVENT;
    uint8_t offMode;

    DBTHRD_GetData(DBTYPE_OFF_MODE_SETTING,(void*)&offMode, THIS_THERMOSTAT);

    //In Off mode, cut the heat and reset the integral
    if (offMode != 0)
    {
        activeEvent = CTLEVENT_NOEVENT;
        Control.IntegralError = 0;
        percent = 0;
    }
    else
    {
        if (event != CTLEVENT_NOEVENT)
        {
            activeEvent = event;
        }

        switch (activeEvent)
        {
        case CTLEVENT_SP_CHANGE_UP:
            //If setpoint is above temperature on a setpoint change, heat at 100%
            if (prop > 0)
            {
                percent = 100;
            }
            else
            {
                //Temperature crossed the setpoint after a setpoint change:
                //return to normal mode
                activeEvent = CTLEVENT_NOEVENT;
                //...and set the integral to 60%
                Control.IntegralError = (((int32s)60*INTEGRAL_SCALE) * 100) / gain;
                percent = prop + Control.IntegralError;
            }
            break;

        case CTLEVENT_SP_CHANGE_DOWN:
            //If setpoint is below temperature on a setpoint change, heat at 0%
            if (prop < 0)
            {
                percent = 0;
            }
            else
            {
                //Temperature crossed the setpoint, return to normal mode
                activeEvent = CTLEVENT_NOEVENT;
                //...and set the integral to 60%
                Control.IntegralError = (((int32s)60*INTEGRAL_SCALE) * 100) / gain;
                percent = prop + Control.IntegralError;
            }
            break;

        case CTLEVENT_NOEVENT:
            //Force heating if temperature is 3�C below setpoint
            if(prop >= (propTreshold * 3))
            {
                percent = 100;
            }
            //Stop heating if temperature is 1�C above setpoint
            else if(prop <= -propTreshold)
            {
                percent = 0;
            }
            else
            {
                percent = prop + integral;
            }
            break;
        default:
            break;
        }

        Control.ControlEvents = activeEvent;
        //TODO: Consid�rer remplacer ces protections par un temps minimum On et Off
        if(percent < 0)
        {
            percent = 0;
        }
        else if(percent > 100)
        {
            percent = 100;
        }
    }
    return((int8u)percent);
}

/*******************************************************************************
*   \function : int32s CTL_CalculateIntegralError(TEMPERATURE_C_t error, int32s integralError, CtlParam_t FLASH_MEM const * param)
*	\brief :    Compute integral Error out of current Error
*	\param :    TEMPERATURE_C_t error = current temperature error
*               int32s integralError = last integral value
*               CtlParam_t FLASH_MEM const * param = parameter structure that contains integral TAU
*	\return :   int32s new integral value
********************************************************************************/
int32s CTL_CalculateIntegralError(TEMPERATURE_C_t error, int32s integralError, int8u tauI, int8u gain)
{
    int32s maxIntError;

    if (gain > 0)
    {
        maxIntError = (((int32s)100*INTEGRAL_SCALE) * 100) / gain;  // gain(K) : %/�C => 5 = 0.05/�C => 5/100 = 0.05
                                                                    // % = error * gain(K) = error * K/100 (integer value)
                                                                    // error = (1/%)*K/100
                                                                    // error = % * 100/K
    }

    if (IsSensorValid(error))
    {
        if (tauI > 0)
        {
            integralError += ((int32s)error * INTEGRAL_SCALE) / ((tauI * 60)/CONTROLLER_UPDATE_TICK);

            if (integralError >= maxIntError)       // >= 100%
            {
                integralError = maxIntError;        // 100%
            }
//            else if (integralError <= -maxIntError)  // <= -100%
//            {
//                integralError = -maxIntError;        // -100%
//            }
            else if (integralError <= 0)  // <= -100%
            {
                integralError = 0;        // -100%
            }
            else
            {
                ;           //do nothing
            }
        }
    }

    return (integralError);
}


/*******************************************************************************
//  \function : static TEMPERATURE_C_t CTL_CalculateError(TEMPERATURE_C_t control_temp, TEMPERATURE_C_t setpoint, TEMPERATURE_C_t maxerror);
//	\brief :    Convert a setpoint and control_temp differences in error
//	\param :    TEMPERATURE_C_t control_temp = actual temperature (int16s)
//              TEMPERATURE_C_t setpoint = name says it all
//              TEMPERATURE_C_t maxerror = maximum error (usually band)
//	\return :   signed error from -maxerror to maxerror
********************************************************************************/
TEMPERATURE_C_t CTL_CalculateError(TEMPERATURE_C_t control_temp, TEMPERATURE_C_t setpoint, TEMPERATURE_C_t maxerror)
{
#ifdef FLOOR
    TEMPERATURE_C_t floor_limit;
    TEMPERATURE_C_t floor_sensor;
    ControlMode_t floorMode;
#endif
    TEMPERATURE_C_t error = DB_SENSOR_INVALID;

    if (IsSensorValid(maxerror))
    {
        if (IsSensorValid(setpoint))
        {
            switch (control_temp)
            {
            case DB_SENSOR_INVALID:
                error = -maxerror;
                break;
            case DB_SENSOR_LOW:
                error = maxerror;
                break;
            case DB_SENSOR_HIGH:
                error = -maxerror;
                break;
            default:
                error = setpoint - control_temp;       //Get Error
                break;
            }
        }
        else
        {
            error = -maxerror;
        }

#ifdef FLOOR
        DBTHRD_GetData(DBTYPE_FLOOR_LIMIT,(void*)&floor_limit, THIS_THERMOSTAT);
        DBTHRD_GetData(DBTYPE_HIGH_TEMPERATURE_READING,(void*)&floor_sensor, THIS_THERMOSTAT);
        DBTHRD_GetData(DBTYPE_FLOOR_MODE,(void*)&floorMode, THIS_THERMOSTAT);
        //If floor sensor valid and not in ambient mode
        if ((IsSensorValid(floor_sensor)) && (floorMode != CONTROLMODE_AMBIENT))
        {
            uint8_t alert;
            //If floor temperature exceeds the floor limit
            if (floor_sensor > floor_limit)
            {
                //Turn off the heat
                error = -maxerror;
                alert = DB_SET_ALERT | DBALERT_FLOOR_TEMP_LIMIT_REACHED;
            }
            else
            {
                alert = DB_CLEAR_ALERT | DBALERT_FLOOR_TEMP_LIMIT_REACHED;
            }
            DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
        }
#endif

        //Limit error to +/- max error
        if(error < -maxerror)
        {
            error = -maxerror;
        }
        else if(error > maxerror)
        {
            error = maxerror;
        }
    }

    return(error);
}

/*******************************************************************************
//  \function : int8s CTL_Error2Prop(int16s error, int16u band){
//	\brief :    Convert error and a band to a proportionnal percent
//	\param :    error = temperature error (int16s)
//              band =  proportionnal band in unsigned temp scale (int16u)
//	\return :   percent -100 to +100
********************************************************************************/
int8s CTL_Error2Prop(TEMPERATURE_C_t error, int8u gain)
{
    int16s prop = 0;                  //Use unsigned maths

    if (IsSensorValid(error))
    {
        prop = (int16s) (((int32s) error * gain) / 100);
        if (prop > 100)
        {
            prop = 100;
        }
        else if (prop < -100)
        {
            prop = -100;
        }
        else
        {
            ;       // nichts tun
        }
    }
    return ((int8s)prop);
}

/*******************************************************************************
//  \function :
//	\brief :
//	\return :
********************************************************************************/
uint8_t CTL_IsInSetpointChange(void)
{
    return (Control.ControlEvents != CTLEVENT_NOEVENT);
}

/*******************************************************************************
//  \function :
//	\brief :
//	\return :
********************************************************************************/
CtlParam_t CTL_GetTempControlParam(int8u shortcycle)
{
    return(ControlParamsRelayCycle);
}

/*******************************************************************************
//  \function :
//	\brief :
//	\return :
********************************************************************************/
void CTL_SetTempControlParam(CtlParam_t param, int8u shortcycle)
{
    ControlParamsRelayCycle = param;
}

/*******************************************************************************
//  \function : const ControlData_t * CTL_GetTempControlLog(void)
//	\brief :    Return the log data for the control module
//	\return :   pointer to control module log data
********************************************************************************/
const ControlData_t * CTL_GetTempControlLog(void)
{
    return(&Control);
}

//***********************************************************************************
///	\brief
///
///	\param  None.
///	\return
//***********************************************************************************
int8u CTL_GetControlChecksum (void)
{
    int8u * chksumPointer;
    int8u chksum = 0xf0;
    int8u i;

    chksumPointer = (int8u *)&ControlParamsRelayCycle;

    for (i = 0;i < (sizeof(CtlParam_t)); i++)
    {
        chksum += chksumPointer[i];
    }

    return (chksum);
}
