/****************************************************************************/
/*                                                                          */
/*               Stelpro Confidential and Proprietary                       */
/*                                                                          */
/* This work contains valuable confidential and proprietary information.    */
/* Disclosure, use or reproduction outside of Stelpro is prohibited         */
/* except as authorized in writing. This unpublished work is protected by   */
/* the laws of Canada and other countries.                                  */
/*                                                                          */
/*                  Copyright 2014, Stelpro Inc.                            */
/*                         All rights reserved.                             */
/*                                                                          */
/* This material is being furnished in confidence by Stelpro Inc.           */
/*                                                                          */
/****************************************************************************/

/*******************************************************************************
    \file       TemperatureControl.c

    \brief      Temperature control task of the thermostat

    \author     Jean-Fran�ois Many

********************************************************************************
    \section Info   Module Information

    Description:

    This file contains the implementation of the temperature control of the
    thermostat.

*******************************************************************************/

/*-------------------------------------------------------------------------------
    INCLUDE
-------------------------------------------------------------------------------*/
#include "typedef.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\HAL\inc\C828_DebugLog.h"
#include ".\HAL\inc\TemperatureCompensation.h"

#include ".\HAL\inc\Display.h"

/*-------------------------------------------------------------------------------
    DEFINITIONS
-------------------------------------------------------------------------------*/
#define DISPLAY_HYSTERESIS  m_DegC(0.13) //Highly scientific value
#define MAX_ACQ_NB          90

#define MIN_SENSOR_DIFF         0.0       //minimun error allowed for RT3-RT1(based LAB2C data)
#define MAX_SENSOR_DIFF         15        //maximum error allowed for RT3-RT1(based LAB2C data)
#define ROOM_TEMP_FILTER        3       //x in acq period (x * 10sec); 3 = 3*10 = 30sec.

#define MAX_BACKLIGHT_OFFSET    0.3
#define MIN_BACKLIGHT_OFFSET    0
#define TAU_BACKLIGHT_CORRECTION    72     //expressed acq period (10s);
                                            // must reach MAX_BACKLIGHT_OFFSET in 60minutes(3600 sec)
                                            // in a exponential way.
                                            // 3600sec => 5 TAU ; 3600/5 = 720/10sec/period = 72

#define DROOP_CONST_A         0.0036
#define DROOP_CONST_B         0.043
/*-------------------------------------------------------------------------------
    DATA
-------------------------------------------------------------------------------*/
//Long cycle is used for Relays
CompensationParam_t CompensationParams_LongCycle = {
    0,             //float ConstA
    0.5499,             //float ConstB
    2.4246,             //float ConstC
};


static TemperatureData_t Temperature_Sensors;

/*-------------------------------------------------------------------------------
    DECLARATIONS
-------------------------------------------------------------------------------*/
static DB_NOTIFICATION(OnTemperatureUpdate);
//Convert sensors temperature to estimated ambient
static void ConvertTemperatureSensors(TemperatureData_t * tempData);

//Internal function to convert temperature

//Compensation
TEMPERATURE_C_t ComputeSensorCompensation(TemperatureData_t * tempData);

//Display Hysteresis
TEMPERATURE_C_t ApplyDisplayHysteresis(TEMPERATURE_C_t previous, TEMPERATURE_C_t now, TEMPERATURE_C_t hysteresis);

extern uint8_t CTL_IsInSetpointChange(void);
/*-------------------------------------------------------------------------------
    IMPLEMENTATION
-------------------------------------------------------------------------------*/
/*******************************************************************************
*   \function : void TMP_Init(void){
*   \brief :    Reset value of temperatures, enable sensors
*	\param :    None
*	\return :   None
********************************************************************************/
void TMP_Init(void)
{
    Temperature_Sensors.sensorAmbient = DB_SENSOR_INVALID;
    Temperature_Sensors.sensorComp = DB_SENSOR_INVALID;
    Temperature_Sensors.sensorTriac = DB_SENSOR_INVALID;
    Temperature_Sensors.displayTemp = DB_SENSOR_INVALID;
    Temperature_Sensors.controlTemp = DB_SENSOR_INVALID;

    DBTHRD_SubscribeToNotification (DBTYPE_AMBIENT_TEMPERATURE_READING,
                                    OnTemperatureUpdate,
                                    DBCHANGE_LOCAL,
                                    INDEX_DONT_CARE);
}


/*******************************************************************************
*   \function : void OnSensorsAdcComplete(void){
*	\brief :    Get sensor temperature and converts to estimated temp
*	\param :    None
*	\return :   None
********************************************************************************/
static DB_NOTIFICATION(OnTemperatureUpdate)
{
    ControlMode_t floorMode;

    //Get sensors temperature
    Temperature_Sensors.sensorAmbient = DB_SENSOR_INVALID;
    Temperature_Sensors.sensorComp = DB_SENSOR_INVALID;
    Temperature_Sensors.sensorTriac = DB_SENSOR_INVALID;

    DBTHRD_GetData(DBTYPE_AMBIENT_TEMPERATURE_READING,(void*)&Temperature_Sensors.sensorAmbient, INDEX_DONT_CARE);
    DBTHRD_GetData(DBTYPE_COMPENSATION_TEMPERATURE_READING,(void*)&Temperature_Sensors.sensorComp, INDEX_DONT_CARE);
    DBTHRD_GetData(DBTYPE_HIGH_TEMPERATURE_READING,(void*)&Temperature_Sensors.sensorTriac, INDEX_DONT_CARE);

    Temperature_Sensors.compParam = &CompensationParams_LongCycle;

    //Perform estimate mathematics
    ConvertTemperatureSensors(&Temperature_Sensors);

    DBTHRD_GetData(DBTYPE_FLOOR_MODE,(void*)&floorMode, THIS_THERMOSTAT);
    if ((floorMode == CONTROLMODE_AMBIENT) || (floorMode == CONTROLMODE_AMBIENTFLOOR))
    {
        DBTHRD_SetData(DBTYPE_AMBIENT_TEMPERATURE,(void*)&Temperature_Sensors.displayTemp, THIS_THERMOSTAT, DBCHANGE_LOCAL);
#ifdef FLOOR
        DBTHRD_SetData(DBTYPE_FLOOR_TEMPERATURE,(void*)&Temperature_Sensors.sensorTriac, THIS_THERMOSTAT, DBCHANGE_LOCAL);
#endif
    }
    else if (floorMode == CONTROLMODE_FLOOR)
    {
        DBTHRD_SetData(DBTYPE_FLOOR_TEMPERATURE,(void*)&Temperature_Sensors.displayTemp, THIS_THERMOSTAT, DBCHANGE_LOCAL);
    }
    DBTHRD_SetData(DBTYPE_CONTROL_TEMPERATURE,(void*)&Temperature_Sensors.controlTemp, INDEX_DONT_CARE, DBCHANGE_LOCAL);

    InitiateNewLogEntry();
}

/*******************************************************************************
*   \function : void ConvertTemperatureSensors(TemperatureData_t * tempData){
*	\brief :    Convert sensor temperatures to ambient temperature (display and control)
*	\param :    TemperatureData_t * tempData = pointer to structure containing updated inputs
*               list of inputs are:
*               sensorAmbient, sensorComp, * param, percentlatch, heaton
*	\return :   TemperatureData_t * tempData with updated displayTemp and controlTemp
********************************************************************************/
void ConvertTemperatureSensors(TemperatureData_t * tempData)
{
    TEMPERATURE_C_t sensor_ambient              = tempData->sensorAmbient;        //Sensor ambient
#ifdef FLOOR
    TEMPERATURE_C_t sensor_floor                = tempData->sensorTriac;    //Floor sensor is replacing the triac sensor
    uint8_t alert;
#endif
    TEMPERATURE_C_t compensation;
    ControlMode_t floorMode;

    ////////////////////////////////////////////////////////////////////////////
    //Process ambient sensor
    ////////////////////////////////////////////////////////////////////////////
    DBTHRD_GetData(DBTYPE_FLOOR_MODE,(void*)&floorMode, THIS_THERMOSTAT);
    if ((floorMode == CONTROLMODE_AMBIENT) || (floorMode == CONTROLMODE_AMBIENTFLOOR))
    {
        alert = DB_CLEAR_ALERT | DBALERT_NO_FLOOR_SENSOR_F;
        DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
        switch(sensor_ambient){
        case DB_SENSOR_INVALID:                    //Sensor defective -> system OFF
        case DB_SENSOR_HIGH:                       //Sensor HI
        case DB_SENSOR_LOW:                        //Sensor LO
            tempData->controlTemp = sensor_ambient;
            tempData->displayTemp = tempData->controlTemp;              //Save display temp
            break;

        //Sensor ambient in normal operation
        default:
            ////////////////////////////////////////////////////////////////////////
            //Compensation - Internal heating
            ////////////////////////////////////////////////////////////////////////
            compensation = ComputeSensorCompensation(tempData);
            //Apply compensated temperature
            tempData->controlTemp = compensation;

            ////////////////////////////////////////////////////////////////////////////
            //Displayed temp
            ////////////////////////////////////////////////////////////////////////////
            //Apply hysteresis
            tempData->displayTemp = ApplyDisplayHysteresis(tempData->displayTemp, tempData->controlTemp, DISPLAY_HYSTERESIS);

            break;
        }
#ifdef FLOOR
        if (floorMode == CONTROLMODE_AMBIENTFLOOR)
        {
            //In floor thermostat, check the floor sensor in ambient-floor mode, to alert the user that the floor protection is inactive when the floor sensor is missing
            if (sensor_floor == DB_SENSOR_INVALID)
            {
                alert = DB_SET_ALERT | DBALERT_NO_FLOOR_SENSOR_AF;
            }
            else
            {
                alert = DB_CLEAR_ALERT | DBALERT_NO_FLOOR_SENSOR_AF;
            }
            DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
        }
        else
        {
            alert = DB_CLEAR_ALERT | DBALERT_NO_FLOOR_SENSOR_AF;
            DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
        }
#endif
    }
#ifdef FLOOR
    else if (floorMode == CONTROLMODE_FLOOR)
    {
        alert = DB_CLEAR_ALERT | DBALERT_NO_FLOOR_SENSOR_AF;
        DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
        switch(sensor_floor){
        case DB_SENSOR_INVALID:                    //Sensor defective -> system OFF
            alert = DB_SET_ALERT | DBALERT_NO_FLOOR_SENSOR_F;
            DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
        case DB_SENSOR_HIGH:                       //Sensor HI
        case DB_SENSOR_LOW:                        //Sensor LO
            tempData->controlTemp = sensor_floor;
            tempData->displayTemp = tempData->controlTemp;              //Save display temp
            break;

        //Sensor floor in normal operation
        default:
            //Apply floor sensor directly, no compensation
            tempData->controlTemp = sensor_floor;

            ////////////////////////////////////////////////////////////////////////////
            //Displayed temp
            ////////////////////////////////////////////////////////////////////////////
            //Apply hysteresis
            tempData->displayTemp = ApplyDisplayHysteresis(tempData->displayTemp, tempData->controlTemp, DISPLAY_HYSTERESIS);

            alert = DB_CLEAR_ALERT | DBALERT_NO_FLOOR_SENSOR_F;
            DBTHRD_SetData(DBTYPE_ACTIVE_ALERTS,(void*)&alert, THIS_THERMOSTAT, DBCHANGE_LOCAL);
            break;
        }
    }
#endif
    else
    {
        tempData->controlTemp = DB_SENSOR_INVALID;
        tempData->displayTemp = tempData->controlTemp;
    }
}

/*******************************************************************************
*   \function : TEMPERATURE_C_t ComputeSensorCompensation(TemperatureData_t * tempData)
*	\brief :    Calculate a compensation influence from a second sensor
*	\param :    TemperatureData_t * tempData
*	\return :   TEMPERATURE_C_t for compensation influence range from 0�C to 50�C
********************************************************************************/
#ifndef WIN32
#pragma optimize=none
#endif /* WIN32 */
TEMPERATURE_C_t ComputeSensorCompensation(TemperatureData_t * tempData)
{
    float compensation = DB_SENSOR_INVALID;
    static uint8_t pastHeatDemand[MAX_ACQ_NB];
    static uint8_t nb_acq = 0;
    static uint8_t active_sample = 0;
    uint16_t heatDemandCumul;
    uint8_t heatDemand;
    uint8_t i;
    float sensor_RT1;
    float sensor_RT3;
    static float roomTemp = -100;
    float roomTemp_tmp;
    static float backlight_correction = 0;
    float Droop_Correction;

    TEMPERATURE_C_t compSensor1     = tempData->sensorAmbient;
    TEMPERATURE_C_t compSensor2     = tempData->sensorComp;

    if (TMP_IsSensorValid(compSensor2) && TMP_IsSensorValid(compSensor1))
    {
        float sensor_diff;
        sensor_RT1 = ((float)compSensor1/100);
        sensor_RT3 = ((float)compSensor2/100);
        DBTHRD_GetData(DBTYPE_HEAT_DEMAND,(void*)&heatDemand, THIS_THERMOSTAT);

        /* Room temp calculation based on the value of RT1 and RT3  */
        sensor_diff = sensor_RT3 - sensor_RT1;
        if (sensor_diff < MIN_SENSOR_DIFF)
        {
            sensor_diff = MIN_SENSOR_DIFF;
        }
        else if (sensor_diff > MAX_SENSOR_DIFF)
        {
            sensor_diff = MAX_SENSOR_DIFF;
        }
        else
        {
            ;
        }

        roomTemp_tmp = sensor_RT1 - ((tempData->compParam->ConstA * sensor_diff * sensor_diff) +
                                (tempData->compParam->ConstB * sensor_diff) +
                                 tempData->compParam->ConstC);

        /*  Average of the last MAX_ACQ_NB Heat demand samples  */
        if (nb_acq < MAX_ACQ_NB)
        {
            nb_acq++;
        }

        pastHeatDemand[active_sample++] = heatDemand;
        if (active_sample >= MAX_ACQ_NB)
        {
            active_sample = 0;
        }
        heatDemandCumul = 0;
        for (i = 0; i < nb_acq; i++)
        {
            heatDemandCumul += pastHeatDemand[i];
        }
        heatDemand = heatDemandCumul / nb_acq;

        /* Droop correction, based on the delta of "Boule Noire" and "Stratis 5" of LAB2C
            and the duty cycle applied to the baseboard, from 0-100 */

        Droop_Correction = (DROOP_CONST_A * (float)heatDemand) + DROOP_CONST_B;

        roomTemp_tmp -= Droop_Correction;


        /*  Backlight correction; when the backlight is ON, the resulting calculated room
            temperature is offset of ~0.3�C.  It take near 60 minutes to reach the maximum
            offset  */
        if (0 != DSP_Backlight_GetBacklightState())
        {
            backlight_correction += (MAX_BACKLIGHT_OFFSET - backlight_correction)/TAU_BACKLIGHT_CORRECTION;
        }
        else
        {
            backlight_correction += (MIN_BACKLIGHT_OFFSET - backlight_correction)/TAU_BACKLIGHT_CORRECTION;
        }

        roomTemp_tmp -= backlight_correction;

        /* Filtering of the calculated room temperature     */
        if (roomTemp == -100)       //not init
        {
            roomTemp = roomTemp_tmp;
        }
        else
        {
            roomTemp += (roomTemp_tmp - roomTemp) / ROOM_TEMP_FILTER;
        }

        compensation = roomTemp*100;
    }

    return((TEMPERATURE_C_t)compensation);
}

/*******************************************************************************
*   \function : TEMPERATURE_C_t ApplyDisplayHysteresis(TEMPERATURE_C_t previous, TEMPERATURE_C_t now, TEMPERATURE_C_t hysteresis){
*	\brief :    Apply an hysteresis to a new temperature versus a previous one
*	\param :    TEMPERATURE_C_t previous = previous temperature
*               TEMPERATURE_C_t now = new temperature
*               TEMPERATURE_C_t hysteresis = temperature difference needed to select "now" value
*	\return :   TEMPERATURE_C_t with hysteresis applied, either new or previous temperature
********************************************************************************/
TEMPERATURE_C_t ApplyDisplayHysteresis(TEMPERATURE_C_t previous, TEMPERATURE_C_t now, TEMPERATURE_C_t hysteresis)
{
    TEMPERATURE_C_t final = previous;
    //Apply hysteresis
    if(TMP_IsSensorValid(previous) && TMP_IsSensorValid(now) && TMP_IsSensorValid(hysteresis))
    {
        if (hysteresis < 0)
        {
            hysteresis = -hysteresis;
        }
        if(now >= (previous + hysteresis) || now <= (previous - hysteresis) )
        {        //Verify hysteresis
            final = now;     //Save new value
        }
    }
    else
    {
        final = now;     //Save new value
    }
    return(final);
}


/*******************************************************************************
*   \function : void TMP_GetDisplayedTemperature(TEMPERATURE_C_t * estimated){
*	\brief :    Get the Estimated temperature
*	\param :    TEMPERATURE_C_t * estimated = filled with estimated temp
*	\return :   None
********************************************************************************/
void TMP_GetDisplayedTemperature(TEMPERATURE_C_t * estimated)
{
    if(estimated != NULL){
        *estimated = Temperature_Sensors.displayTemp;
    }
}


/*******************************************************************************
*   \function : void TMP_GetControlTemp(TEMPERATURE_C_t * control){
*	\brief :    Get the control temperature
*	\param :    EMPERATURE_C_t * controlTemp = filled with control temp
*	\return :   None
********************************************************************************/
void TMP_GetControlTemp(TEMPERATURE_C_t * control)
{
    if(control != NULL){
        *control = Temperature_Sensors.controlTemp;
    }
}

//***********************************************************************************
///	\brief
///
///	\param  None.
///	\return TRUE if any sensor is invalid, FALSE if all sensors are valid.
//***********************************************************************************
int8u TMP_IsSensorValid(TEMPERATURE_C_t sensor)
{
    switch (sensor)
    {
    case DB_SENSOR_INVALID:
    case DB_SENSOR_HIGH:
    case DB_SENSOR_LOW:
        return FALSE;

    default:
        return TRUE;
    }
}


//***********************************************************************************
///	\brief
///
///	\param  None.
///	\return
//***********************************************************************************
CompensationParam_t TMP_GetEstimateParam(void)
{
    return CompensationParams_LongCycle;
}


//***********************************************************************************
///	\brief
///
///	\param  None.
///	\return
//***********************************************************************************
int8u TMP_GetCompensationChecksum (void)
{
    int8u * chksumPointer;
    int8u chksum = 0xf0;
    int8u i;

    chksumPointer = (int8u *)&CompensationParams_LongCycle;

    for (i = 0;i < (sizeof(CompensationParam_t)); i++)
    {
        chksum += chksumPointer[i];
    }

    return (chksum);
}
