/*****************************************************************************/
/*                                                                           */
/*               Stelpro Confidential and Proprietary                        */
/*                                                                           */
/* This work contains valuable confidential and proprietary information.     */
/* Disclosure, use or reproduction outside of Stelpro is prohibited          */
/* except as authorized in writing. This unpublished work is protected by    */
/* the laws of Canada and other countries.                                   */
/*                                                                           */
/*                  Copyright 2015, Stelpro Design Inc.                      */
/*                         All rights reserved.                              */
/*                                                                           */
/* This material is being furnished in confidence by Stelpro Design Inc.     */
/*                                                                           */
/*****************************************************************************/

/*******************************************************************************
* @file    C828_DebugLog.c
* @date    2017/02/28
* @authors Jean-Fran�ois Many
* @brief   C828 Debug Log module
*******************************************************************************/

/*******************************************************************************
*    Includes
*******************************************************************************/
#include "typedef.h"
#include "common_types.h"
#include <string.h>
#include ".\HAL\inc\HAL_CurrentSensing.h"
#include ".\DB\inc\THREAD_DB.h"
#include ".\HAL\inc\C828_DebugLog.h"
#include ".\Console\inc\THREAD_Console.h"
#include ".\HAL\inc\Display.h"
#include ".\HAL\inc\HAL_TemperatureAcquisition.h"
#include ".\HAL\inc\HAL_LoadManagement.h"

/*******************************************************************************
*    Private constant definitions
*******************************************************************************/
typedef struct
{
    uint8_t                     logstart;
    uint8_t                     hours;
    uint8_t                     minutes;
    uint8_t                     seconds;
    int16_t                     ambiantTemp;
    int16_t                     compTemp;
    int16_t                     triacTemp;
    int16_t                     setpoint;
    int16_t                     displayTemp;
    int16_t                     controlTemp;
    uint8_t                     heatDemand;
    Lock_t                      lock;
    uint8_t                     alert;//may contain up to 5 alerts
    uint8_t                     backlightState;
    uint16_t                    ambiantCount;
    uint16_t                    compCount;
    uint16_t                    triacCount;
} C828DebugLog_t;


/*******************************************************************************
*    Private variables definitions
*******************************************************************************/
static uint8_t HeaderSent = FALSE;
static char log_buffer[256];

/*******************************************************************************
*    Public variables definitions
*******************************************************************************/



/*******************************************************************************
*    Private functions prototypes
*******************************************************************************/



/*******************************************************************************
*    Private functions definitions
*******************************************************************************/



/*******************************************************************************
*    Public functions definitions
*******************************************************************************/
void InitiateNewLogEntry(void)
{
    static uint8_t cnt = 3;
    C828DebugLog_t LogBuffer;
    RtcTime_t time;
    dbType_Alerts_t currentAlert;
    uint8_t floorMode;

    DBTHRD_GetData(DBTYPE_LOCALTIME, (void*)&time, THIS_THERMOSTAT);
    DBTHRD_GetData(DBTYPE_AMBIENT_TEMPERATURE_READING,(void*)&LogBuffer.ambiantTemp, THIS_THERMOSTAT);
    DBTHRD_GetData(DBTYPE_COMPENSATION_TEMPERATURE_READING,(void*)&LogBuffer.compTemp, THIS_THERMOSTAT);
    DBTHRD_GetData(DBTYPE_HIGH_TEMPERATURE_READING,(void*)&LogBuffer.triacTemp, THIS_THERMOSTAT);

    DBTHRD_GetData(DBTYPE_FLOOR_MODE,(void*)&floorMode, THIS_THERMOSTAT);
    if (floorMode == CONTROLMODE_FLOOR)
    {
        DBTHRD_GetData(DBTYPE_FLOOR_SETPOINT,(void*)&LogBuffer.setpoint, THIS_THERMOSTAT);
        DBTHRD_GetData(DBTYPE_FLOOR_TEMPERATURE,(void*)&LogBuffer.displayTemp, THIS_THERMOSTAT);
    }
    else
    {
        DBTHRD_GetData(DBTYPE_AMBIENT_SETPOINT,(void*)&LogBuffer.setpoint, THIS_THERMOSTAT);
        DBTHRD_GetData(DBTYPE_AMBIENT_TEMPERATURE,(void*)&LogBuffer.displayTemp, THIS_THERMOSTAT);
    }
    DBTHRD_GetData(DBTYPE_CONTROL_TEMPERATURE,(void*)&LogBuffer.controlTemp, THIS_THERMOSTAT);
    DBTHRD_GetData(DBTYPE_HEAT_DEMAND,(void*)&LogBuffer.heatDemand, THIS_THERMOSTAT);
    DBTHRD_GetData(DBTYPE_LOCK_STATE,(void*)&LogBuffer.lock, THIS_THERMOSTAT);
    DBTHRD_GetData(DBTYPE_ACTIVE_ALERTS,(void*)&currentAlert, THIS_THERMOSTAT);
    LogBuffer.backlightState = DSP_Backlight_GetBacklightState();

    TEMP_GetADCCount(&LogBuffer.ambiantCount, &LogBuffer.compCount, &LogBuffer.triacCount);

    if (HeaderSent == FALSE)
    {
        CONSOLE_LOG_MESSAGE(",LogStart,");
        CONSOLE_MESSAGE_APPEND_STRING("Hours,");
        CONSOLE_MESSAGE_APPEND_STRING("Minutes,");
        CONSOLE_MESSAGE_APPEND_STRING("Seconds,");
        CONSOLE_MESSAGE_APPEND_STRING("AmbientTemp,");
        CONSOLE_MESSAGE_APPEND_STRING("CompTemp,");
        CONSOLE_MESSAGE_APPEND_STRING("TriacTemp,");
        CONSOLE_MESSAGE_APPEND_STRING("Setpoint,");
        CONSOLE_MESSAGE_APPEND_STRING("DisplayTemp,");
        CONSOLE_MESSAGE_APPEND_STRING("ControlTemp,");
        CONSOLE_MESSAGE_APPEND_STRING("HeatDemand,");
        CONSOLE_MESSAGE_APPEND_STRING("Lock,");
        CONSOLE_MESSAGE_APPEND_STRING("Alerts,");
        CONSOLE_MESSAGE_APPEND_STRING("Backlight,");
        CONSOLE_MESSAGE_APPEND_STRING("AmbientCount,");
        CONSOLE_MESSAGE_APPEND_STRING("CompensationCount,");
        CONSOLE_MESSAGE_APPEND_STRING("TriacCount,");
    }
    HeaderSent = TRUE;
    if (cnt)
    {
        cnt--;
    }

    memset(log_buffer, 0, 256);

    if (!cnt)
    {
        cnt = 3;
        sprintf(log_buffer,",LogStart30,");
    }
    else
    {
        sprintf(log_buffer,",LogStart10,");
    }

    sprintf(&log_buffer[12],"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",
                        time.Hours,
                        time.Minutes,
                        time.Seconds,
                        LogBuffer.ambiantTemp,
                        LogBuffer.compTemp,
                        LogBuffer.triacTemp,
                        LogBuffer.setpoint,
                        LogBuffer.displayTemp,
                        LogBuffer.controlTemp,
                        LogBuffer.heatDemand,
                        LogBuffer.lock,
                        currentAlert.ActiveAlerts[0],
                        LogBuffer.backlightState,
                        LogBuffer.ambiantCount,
                        LogBuffer.compCount,
                        LogBuffer.triacCount);

    CONSOLE_LOG_MESSAGE(log_buffer);
}


/** Copyright(C) 2017 Stelpro Design, All Rights Reserved**/
